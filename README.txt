Using the EX2GK program
Author: Aaron Ho (10/01/2018)

For first time users, it is STRONGLY RECOMMENDED to use the GUI
developed for the corresponding machine to be analyzed. Note that
these GUIs are developed to be executed on the local computing
clusters of the machine in question.

Machine-specific GUIs are found in EX2GK/guis/<machine>/
    Due to implementation of computing clusters and machine-specific
    options, each machine requires its own unique GUI


Otherwise, the script run_EX2GK.py in EX2GK/scripts/general/ provides
a command line interface to the extraction tool. This script
requires three input files:
    shotlist.txt       Contains the shot number and time window
    argdict.txt        Contains the execution setting specifications
    getlist.txt        Contains the data fields to be extracted

Ideology: Use pre-specified getlist.txt in EX2GK/data/<machine>/ if the
          full fitting routine is to be performed. If tool is only
          intended to return raw data, then getlist.txt can be
          truly user-defined.


Machine-specific data files are found in EX2GK/data/<machine>/
    This folder contains default inputs for the EX2GK code for
    each machine, due to variations in data access and format

Machine-specific scripts are found in EX2GK/scripts/<machine>/
    This folder contains commonly used executable Python scripts for
    each machine (or in general), ideally copied to working directory for
    modification and use

Machine-specific tools are found in EX2GK/tools/<machine>/
    This folder contains commonly used Python functions for each
    machine (or in general), ideally imported into custom Python
    scripts for use
