#!/bin/env bash

#
# Name:
# 	public-user-install - install for personal use in new directory.
#
# Usage:
#	public-user-install <to>
#
# Parameters:
#       to   - target directory for local install (do not use repository directory!).
#


script=`basename $0`

if [ "$1" = "" ] ; then
  echo "Usage: $script <to>"
  exit 1
fi

if [ -f "$1" ] ; then
  echo "$script: $2 is a file"
  exit 1
fi

echo "$script: installing EX2GK to $1"

#
# Files and folders in repo to link.
#

link="docs LICENSE setup.cfg EX2GK Makefile pyproject.toml setup.py MANIFEST.in README.txt tests"

#
# Make target directory.
#

echo "$script: mkdir -p $1"
mkdir -p $1

#
# Create links.
#

for x in $link
do
  linkpath="$(readlink -f $x)"
  if [ -e $linkpath ] ; then
    echo "$script: ln -s $linkpath $1/$x"
    ln -s $linkpath $1/$x
  else
    echo "$script: $linkpath not found"
  fi
done

exit 0
