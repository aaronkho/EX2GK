"""Packaging settings."""

import site
import sys
site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

from codecs import open
from os.path import abspath, dirname, join
from subprocess import call

from setuptools import Command, find_packages, setup

this_dir = abspath(dirname(__file__))
with open(join(this_dir, 'README.txt'), encoding='utf-8') as file:
    long_description = file.read()


class RunTests(Command):
    """Run all tests."""
    description = 'run tests'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        """Run all tests!"""
        errno = call(['py.test'])
        raise SystemExit(errno)

setup(
    name = 'EX2GK',
    version = '1.7.0',
    description = 'Tool to extract experimental data for gyrokinetic code sampling.',
    long_description = long_description,
    url = 'https://gitlab.com/aaronkho/EX2GK',
    author = 'Aaron Ho',
    author_email = 'a.ho@differ.nl',
    license = 'MIT',
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'Topic :: Utilities',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.10'
    ],
    packages = find_packages(exclude=['docs', 'tests*']),
    install_requires = ['numpy>=1.12',
                        'scipy>=0.17',
                        'matplotlib>=3.0',
                        'pandas>=0.18'
                       ],
    python_requires= ">=3.5",
    extras_require = {
        'gui-qt4': ['PyQt4', 'setuptools', 'ipython'],
        'gui-qt5': ['PyQt5', 'setuptools', 'sip', 'PyQt5_sip', 'ipython'],
        'test': ['coverage', 'pytest', 'pytest-cov'],
#        'qualikiz': ['qualikiz_tools'],
#        'jet': ['jetto-pythontools']
    },
    scripts = ['EX2GK/scripts/general/run_EX2GK.py',
               'EX2GK/scripts/general/make_qualikiz_point.py',
               'EX2GK/scripts/jet/create_from_jetto.py',
               'EX2GK/scripts/jet/tabular_qualikiz_from_jetto.py',
               'EX2GK/scripts/jet/jetto_gamhistory.py',
               'EX2GK/scripts/jet/generate_tw_from_shot.py',
               'EX2GK/guis/jet/EX2GK_JET.py',
               'EX2GK/guis/jet/GPR1D_JET.py',
               'EX2GK/guis/jet/jet_exp_viewer.py'
              ],
    cmdclass = {'test': RunTests},
)
