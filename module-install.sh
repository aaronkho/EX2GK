#!/bin/env bash

#
# Name:
# 	module-install - install for personal use in new directory.
#
# Usage:
#	module-install
#


script=`basename $0`
target="$PWD/site-packages"

echo "$script: installing EX2GK to $target"

pip install --target=$target setuptools>=62.6.0
pip install --target=$target -r requirements.txt --no-deps --no-cache-dir .

exit 0
