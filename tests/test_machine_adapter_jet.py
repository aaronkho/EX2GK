#!/usr/bin/env python

import pytest
import copy
import numpy as np
from operator import itemgetter

from EX2GK.machine_adapters.jet import time_selector as tjet, process_jet as pjet, standardize_jet as sjet, fit_settings_jet as fjet, adapter as ajet
from EX2GK.tools.general import classes, nested_operations

@pytest.mark.jet
@pytest.mark.usefixtures("sample_jet_raw_data", "sample_jet_processed_data", "sample_jet_processed_data_equilibrium_level", "sample_jet_processed_data_uncorrected_level")
class TestJETAdapterDataProcessor(object):

    debug_flag = True

    @pytest.fixture(scope="class")
    def output_container(self):
        return {}

    @pytest.fixture(scope="class")
    def variables_tested(self):
        return []

    @pytest.mark.jet_process
    def test_process_generic_transfer(self, sample_jet_raw_data, sample_jet_processed_data_equilibrium_level, output_container, variables_tested):
        output_container = pjet.transfer_generic_data(
            sample_jet_raw_data,
            newstruct=output_container,
            userz=True,
            gpcoord=False,
            useshift=True,
            usenscale=True,
            useece=False,
            absece=False,
            useqedge=False,
            uselmode=False,
            usenesmooth=False,
            pureraw=False,
            usesom=False,
            userefl=False,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_equilibrium_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_process_equilibrium_definition(self, sample_jet_raw_data, sample_jet_processed_data_equilibrium_level, output_container, variables_tested):
        output_container = pjet.define_equilibrium(
            sample_jet_raw_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_equilibrium_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_process_elm_filter_definition(self, sample_jet_raw_data, sample_jet_processed_data_equilibrium_level, output_container, variables_tested):
        jet_output_container = pjet.define_time_filter(
            sample_jet_raw_data,
            newstruct=output_container,
            threshold=7.732e12,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_equilibrium_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_unpack_general_data(self, sample_jet_raw_data, sample_jet_processed_data_equilibrium_level, output_container, variables_tested):
        output_container = pjet.unpack_general_data(
            sample_jet_raw_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_equilibrium_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_unpack_config_data(self, sample_jet_raw_data, sample_jet_processed_data_equilibrium_level, output_container, variables_tested):
        output_container = pjet.unpack_config_data(
            sample_jet_raw_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_equilibrium_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_unpack_coordinate_data(self, sample_jet_raw_data, sample_jet_processed_data_equilibrium_level, output_container, variables_tested):
        output_container = pjet.unpack_coord_data(
            sample_jet_raw_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_equilibrium_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_coordinate_correction(self, sample_jet_processed_data_uncorrected_level, output_container, variables_tested):
        output_container = pjet.calculate_coordinate_systems(
            output_container,
            cdebug=False,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_uncorrected_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_unpack_profile_data(self, sample_jet_raw_data, sample_jet_processed_data_uncorrected_level, output_container, variables_tested):
        output_container = pjet.unpack_profile_data(
            sample_jet_raw_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_uncorrected_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_unpack_source_data(self, sample_jet_raw_data, sample_jet_processed_data_uncorrected_level, output_container, variables_tested):
        output_container = pjet.unpack_source_data(
            sample_jet_raw_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data_uncorrected_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_data_correction(self, sample_jet_raw_data, sample_jet_processed_data, output_container, variables_tested):
        output_container = pjet.apply_data_corrections(
            output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        tested_here.extend(["HRTS","LIDR","ECR","ECM","CX","MAGN"])
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_processed_data, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_process
    def test_jet_process_pytest_suite_completeness(self, sample_jet_processed_data, variables_tested):
        for key in sample_jet_processed_data:
            assert key in variables_tested

@pytest.mark.jet
@pytest.mark.usefixtures("sample_jet_processed_data", "sample_jet_formatted_diagnostic_data", "sample_jet_formatted_source_data", "sample_jet_standardized_data_separated_level", "sample_jet_standardized_data_combined_level", "sample_jet_standardized_data_bounded_level", "sample_jet_standardized_data")
class TestJETAdapterDataStandardizer(object):

    debug_flag = True

    @pytest.fixture(scope="class")
    def output_container(self):
        return {}

    @pytest.fixture(scope="class")
    def variables_tested(self):
        return []

    @pytest.mark.jet_standardize
    def test_standardize_diagnostics(self, sample_jet_processed_data, sample_jet_formatted_diagnostic_data):
        diagnostics_container = sjet.standardize_diagnostic_data(
            sample_jet_processed_data,
            newstruct=None,
            useexp=True,
            fdebug=self.debug_flag
        )
        assert nested_operations.compare(diagnostics_container, sample_jet_formatted_diagnostic_data, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_source(self, sample_jet_processed_data, sample_jet_formatted_source_data):
        source_container = sjet.standardize_source_data(
            sample_jet_processed_data,
            newstruct=None,
            useexp=True,
            fdebug=self.debug_flag
        )
        assert nested_operations.compare(source_container, sample_jet_formatted_source_data, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_generic_transfer(self, sample_jet_processed_data, sample_jet_standardized_data_separated_level, output_container, variables_tested):
        output_container = sjet.transfer_generic_data(
            sample_jet_processed_data,
            newstruct=output_container,
            usepol=False,
            usexeb=False,
            useexp=True,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_standardized_data_separated_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_diagnostic_data(self, sample_jet_formatted_diagnostic_data, sample_jet_standardized_data_separated_level, output_container, variables_tested):
        error_multipliers = {
            "NE": None,
            "TE": None,
            "NI": None,
            "TI": None,
            "NIMP": None,
            "TIMP": None,
            "AF": 0.9,
            "Q": None
        }
        output_container = sjet.filter_standardized_diagnostic_data(
            sample_jet_formatted_diagnostic_data,
            newstruct=output_container,
            userscales=error_multipliers,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_standardized_data_separated_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_edge_data_merge(self, sample_jet_standardized_data_combined_level, output_container, variables_tested):
        output_container = sjet.combine_edge_diagnostic_data(output_container, fdebug=self.debug_flag)
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        tested_here.extend([
            "NEDIAG","NIDIAG","TEDIAG","TIDIAG","AFDIAG","TIMPDIAG","QDIAG","NIMPLIST",
            "NERAW","NERAWEB","NERAWX","NERAWXEB","NEDMAP",
            "TERAW","TERAWEB","TERAWX","TERAWXEB","TEDMAP",
            "TIMPRAW","TIMPRAWEB","TIMPRAWX","TIMPRAWXEB","TIMPDMAP",
            "AFRAW","AFRAWEB","AFRAWX","AFRAWXEB","AFDMAP",
            "QRAW","QRAWEB","QRAWX","QRAWXEB","QDMAP",
            "IQRAW","IQRAWEB","IQRAWX","IQRAWXEB","IQDMAP"
	])
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_standardized_data_combined_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_boundary_definition(self, sample_jet_standardized_data_bounded_level, output_container, variables_tested):
        output_container = sjet.add_profile_boundary_data(
            output_container,
            pastete=True,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        tested_here.extend([
            "NEDIAG","NIDIAG","TEDIAG","TIDIAG","AFDIAG","TIMPDIAG","QDIAG",
            "NERAW","NERAWEB","NERAWX","NERAWXEB","NEDMAP",
            "TERAW","TERAWEB","TERAWX","TERAWXEB","TEDMAP",
            "TIMPRAW","TIMPRAWEB","TIMPRAWX","TIMPRAWXEB","TIMPDMAP",
            "AFRAW","AFRAWEB","AFRAWX","AFRAWXEB","AFDMAP",
            "QRAW","QRAWEB","QRAWX","QRAWXEB","QDMAP",
            "IQRAW","IQRAWEB","IQRAWX","IQRAWXEB","IQDMAP"
	])
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_standardized_data_bounded_level, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_source_data(self, sample_jet_formatted_source_data, sample_jet_standardized_data, output_container, variables_tested):
        output_container = sjet.transfer_standardized_source_data(
            sample_jet_formatted_source_data,
            newstruct=output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_standardized_data, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_standardize_derivative_constraint_definition(self, sample_jet_standardized_data, output_container, variables_tested):
        output_container = sjet.add_derivative_constraint_data(
            output_container,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        local_modification = {}
        for key in tested_here:
            local_modification[key] = output_container[key]
        assert nested_operations.compare(local_modification, sample_jet_standardized_data, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_standardize
    def test_jet_standardize_pytest_suite_completeness(self, sample_jet_standardized_data, variables_tested):
        for key in sample_jet_standardized_data:
            assert key in variables_tested

@pytest.mark.jet
@pytest.mark.usefixtures("sample_jet_standardized_data", "sample_jet_finished_data")
class TestJETAdapterFitSettings(object):

    debug_flag = True

    @pytest.fixture(scope="class")
    def variables_tested(self):
        return []

    @pytest.mark.jet_fit_settings
    def test_fit_settings_definition(self, sample_jet_standardized_data, sample_jet_finished_data, variables_tested):
        output_container = fjet.generate_fit_kernel_settings(
            sample_jet_standardized_data,
            fopt=True,
            fdebug=self.debug_flag
        )
        tested_here = [key for key in output_container if key not in variables_tested]
        variables_tested.extend(tested_here)
        assert nested_operations.compare(output_container, sample_jet_finished_data, ignore_keys=["UPDATED"], fdebug=self.debug_flag)

    @pytest.mark.jet_fit_settings
    def test_jet_fit_settings_pytest_suite_completeness(self, sample_jet_finished_data, variables_tested):
        for key in sample_jet_finished_data:
            assert key in variables_tested


@pytest.mark.jet
@pytest.mark.usefixtures("sample_jet_preclass_data", "sample_jet_meta_class", "sample_jet_coordinate_class", "sample_jet_zero_class", "sample_jet_raw_class", "sample_jet_equilibrium_class", "sample_jet_settings_class", "sample_jet_flag_class", "sample_time_window_class_entry")
class TestJETAdapterClassInsertion(object):

    debug_flag = True

    @pytest.fixture(scope="class")
    def output_container(self):
        return []

    @pytest.mark.jet_insertion
    def test_jet_populate_all_classes(self, sample_jet_preclass_data, output_container):
        test_class = ajet.populate_classes(sample_jet_preclass_data, fdebug=self.debug_flag)
        assert isinstance(test_class, classes.EX2GKTimeWindow)
        assert test_class.isReady()
        output_container.append(test_class)

    @pytest.mark.jet_insertion
    def test_jet_meta_class_equivalence(self, sample_jet_meta_class, output_container):
        assert "META" in output_container[0]
        assert isinstance(output_container[0]["META"], classes.EX2GKMetaContainer)
        assert output_container[0]["META"] == sample_jet_meta_class

    @pytest.mark.jet_insertion
    def test_jet_coordinate_class_equivalence(self, sample_jet_coordinate_class, output_container):
        assert "CD" in output_container[0]
        assert isinstance(output_container[0]["CD"], classes.EX2GKCoordinateContainer)
        assert output_container[0]["CD"] == sample_jet_coordinate_class

    @pytest.mark.jet_insertion
    def test_jet_zero_class_equivalence(self, sample_jet_zero_class, output_container):
        assert "ZD" in output_container[0]
        assert isinstance(output_container[0]["ZD"], classes.EX2GKZeroDimensionalContainer)
        assert output_container[0]["ZD"] == sample_jet_zero_class

    @pytest.mark.jet_insertion
    def test_jet_raw_class_equivalence(self, sample_jet_raw_class, output_container):
        assert "RD" in output_container[0]
        assert isinstance(output_container[0]["RD"], classes.EX2GKRawDataContainer)
        assert output_container[0]["RD"] == sample_jet_raw_class

    @pytest.mark.jet_insertion
    def test_jet_equilibrium_class_equivalence(self, sample_jet_equilibrium_class, output_container):
        assert "ED" in output_container[0]
        assert isinstance(output_container[0]["ED"], classes.EX2GKEquilibriumDataContainer)
        assert output_container[0]["ED"] == sample_jet_equilibrium_class

    @pytest.mark.jet_insertion
    def test_jet_settings_class_equivalence(self, sample_jet_settings_class, output_container):
        assert "FSI" in output_container[0]
        assert isinstance(output_container[0]["FSI"], classes.EX2GKFitSettingsContainer)
        assert output_container[0]["FSI"] == sample_jet_settings_class

    @pytest.mark.jet_insertion
    def test_jet_flag_class_equivalence(self, sample_jet_flag_class, output_container):
        assert "FLAG" in output_container[0]
        assert isinstance(output_container[0]["FLAG"], classes.EX2GKFlagContainer)
        assert output_container[0]["FLAG"] == sample_jet_flag_class

    @pytest.mark.jet_insertion
    def test_jet_class_entry_equivalence(self, sample_time_window_class_entry, output_container):
        assert output_container[0] == sample_time_window_class_entry
