#!/usr/bin/env python

import pytest
import numpy as np
from operator import itemgetter

from EX2GK.tools.general import phystools

@pytest.mark.phystools
@pytest.mark.usefixtures("single_elm_signal", "equipartition_power_data", "heating_power_data", "kinetic_energy_data")
class TestMainPipeline(object):

    passing_sigma_criterion = 2.0

    def test_burst_detector(self, single_elm_signal):
        threshold = 0.75 * np.nanstd(single_elm_signal["signal"])
        (spike_start, spike_duration, analyzed_vector) = phystools.elm_detector(
            single_elm_signal["time"],
            single_elm_signal["signal"],
            ratio_threshold=1.5,
            end_criterion=0.8,
            delta_threshold=threshold
        )
        assert len(spike_start) > 0
        assert len(spike_duration) > 0
        assert np.all(analyzed_vector == single_elm_signal["signal"])
        assert np.isclose(spike_start[0], single_elm_signal["elm"][0])
        assert np.isclose(spike_duration[0], single_elm_signal["elm"][1])

    def test_compute_electron_ion_exchange_power(self, equipartition_power_data):
        (exchange_power, exchange_power_error) = phystools.calc_equipartition_power(
            equipartition_power_data["electron_temperature"],
            equipartition_power_data["electron_density"],
            ts2raw=equipartition_power_data["ion_temperature"],
            ns2raw=equipartition_power_data["ion_density"],
            as2=equipartition_power_data["ion_mass"],
            zs2=equipartition_power_data["ion_charge"],
            debye=equipartition_power_data["debye_length"] # used in collisionality calculation
        )
        assert np.all(np.isclose(exchange_power, equipartition_power_data["exchange_power"]))

    def test_heating_power_consistency_check(self, heating_power_data):
        (result, total_integrated_power) = phystools.heating_power_test(
             heating_power_data["volume"],
             heating_power_data["electron_heating"],
             heating_power_data["ion_heating"],
             srceraweb=heating_power_data["electron_heating_error"],
             srciraweb=heating_power_data["ion_heating_error"],
             totalpow=heating_power_data["total_heating_power"],
             totalpoweb=heating_power_data["total_heating_power_error"],
             passwidth=self.passing_sigma_criterion
        )
        # Stored internal as HTEST flag
        assert result
        assert np.isclose(total_integrated_power, heating_power_data["deposited_power"])

    def test_equipartition_power_consistency_check(self, heating_power_data, equipartition_power_data):
        (result, index_of_max_error, exchange_power_at_max, exchange_power_error_at_max, available_power_at_max, available_power_error_at_max) = phystools.equipartition_test(
            heating_power_data["volume"],
            equipartition_power_data["exchange_power"],
            heating_power_data["electron_heating"],
            heating_power_data["ion_heating"],
            equipartition_power_data["exchange_power_error"],
            heating_power_data["electron_heating_error"],
            heating_power_data["ion_heating_error"],
            passwidth=self.passing_sigma_criterion
        )
        # Stored internally as QTEST flag
        assert result
        assert index_of_max_error == 98
        assert np.isclose(exchange_power_at_max, equipartition_power_data["limiting_exchange_power"])
        assert np.isclose(exchange_power_error_at_max, equipartition_power_data["limiting_exchange_power_error"])
        assert np.isclose(available_power_at_max, equipartition_power_data["limiting_available_power"])
        assert np.isclose(available_power_error_at_max, equipartition_power_data["limiting_available_power_error"])

    def test_energy_content_consistency_check(self, kinetic_energy_data):
        (result, main_energy, main_energy_error, thermal_energy, thermal_energy_error, total_energy, total_energy_error) = phystools.energy_content_test(
            kinetic_energy_data["volume"],
            kinetic_energy_data["electron_pressure"],
            kinetic_energy_data["electron_pressure_error"],
            kinetic_energy_data["ion_pressure"],
            kinetic_energy_data["ion_pressure_error"],
            kinetic_energy_data["impurity_pressure"],
            kinetic_energy_data["impurity_pressure_error"],
            kinetic_energy_data["fast_ion_pressure"],
            kinetic_energy_data["fast_ion_pressure_error"],
            kinetic_energy_data["total_energy"],
            kinetic_energy_data["total_energy_error"],
            passwidth=self.passing_sigma_criterion
        )
        # Stored internally as ETEST flag
        assert result
        assert np.isclose(main_energy, kinetic_energy_data["main_species_energy"])
        assert np.isclose(main_energy_error, kinetic_energy_data["main_species_energy_error"])
        assert np.isclose(thermal_energy, kinetic_energy_data["thermal_energy"])
        assert np.isclose(thermal_energy_error, kinetic_energy_data["thermal_energy_error"])
        assert np.isclose(total_energy, kinetic_energy_data["total_energy"])
        assert np.isclose(total_energy_error, kinetic_energy_data["total_energy_error"])
