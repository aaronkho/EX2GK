#!/usr/bin/env python

import pytest
import copy
import numpy as np
from operator import itemgetter

from EX2GK.tools.general import classes, nested_operations

@pytest.mark.main_pipeline
@pytest.mark.usefixtures("sample_time_window_class_entry", "sample_time_window_settings", "sample_time_window_class_primary_stage", "sample_time_window_class_secondary_stage", "sample_time_window_class_tertiary_stage", "sample_time_window_class_poly_fitted", "sample_time_window_class_gpr_fitted", "sample_time_window_class_gpr_post_processed")
class TestMainPipeline(object):

    debug_flag = True

    def test_class_entry_readiness(self, sample_time_window_class_entry):
        assert sample_time_window_class_entry.isReady()

    def test_class_primary_fit(self, sample_time_window_class_entry, sample_time_window_settings, sample_time_window_class_primary_stage):
        newrho = np.linspace(sample_time_window_settings["CSOMIN"], sample_time_window_settings["CSOMAX"], sample_time_window_settings["CSON"])
        sample_time_window_class_entry["FLAG"].setFlag("LINFI", sample_time_window_settings["LINFIFLAG"])
        sample_time_window_class_entry["FLAG"].setFlag("LINSRC", sample_time_window_settings["LINSRCFLAG"])
        sample_time_window_class_entry.fitPrimaryProfiles(newrho, sample_time_window_settings["CSO"], sample_time_window_settings["CSOP"])
        assert sample_time_window_class_entry.isFitted()
        assert sample_time_window_class_entry == sample_time_window_class_primary_stage

    def test_class_secondary_fit(self, sample_time_window_class_entry, sample_time_window_class_secondary_stage):
        sample_time_window_class_entry.fitSecondaryProfiles()
        assert sample_time_window_class_entry.isFitted()
        assert sample_time_window_class_entry == sample_time_window_class_secondary_stage

    def test_class_tertiary_fit(self, sample_time_window_class_entry, sample_time_window_class_tertiary_stage):
        sample_time_window_class_entry.fitTertiaryProfiles()
        assert sample_time_window_class_entry.isFitted()
        assert sample_time_window_class_entry == sample_time_window_class_tertiary_stage

    def test_class_source_fit(self, sample_time_window_class_entry, sample_time_window_class_poly_fitted):
        sample_time_window_class_entry.fitSourceProfiles()
        assert sample_time_window_class_entry.isFitted()
        assert sample_time_window_class_entry == sample_time_window_class_poly_fitted

    def test_class_post_processing(self, sample_time_window_settings, sample_time_window_class_gpr_fitted, sample_time_window_class_gpr_post_processed):
        sample_time_window_class_gpr_fitted["META"].setQualityCheckPassCriteria(sample_time_window_settings["QCPASS"])
        sample_time_window_class_gpr_fitted.postProcess()
        assert sample_time_window_class_gpr_fitted == sample_time_window_class_gpr_post_processed
