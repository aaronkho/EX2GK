#!/usr/bin/env python

import pytest
import copy
import numpy as np
from operator import itemgetter

from EX2GK.code_adapters.qualikiz import adapter as aqlk
from EX2GK.tools.general import classes, nested_operations

@pytest.mark.qualikiz
@pytest.mark.usefixtures("sample_time_window_class_gpr_post_processed", "sample_time_window_class_gpr_fitted_qualikiz_output")
class TestQuaLiKizAdapter(object):

    debug_flag = True

    def test_complete_class_conversion(self, sample_time_window_class_gpr_post_processed, sample_time_window_class_gpr_fitted_qualikiz_output):
        style = 'jetto'
        output = aqlk.calc_QuaLiKiz_inputs(sample_time_window_class_gpr_post_processed, use_def=style, fdebug=self.debug_flag)
        assert nested_operations.compare(output, sample_time_window_class_gpr_fitted_qualikiz_output, ignore_keys=["UPDATED"], fdebug=self.debug_flag)
