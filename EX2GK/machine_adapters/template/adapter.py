# Template adapter file for data access - this should be the only imported file for any given adapter!
# Developer: Aaron Ho - 14/11/2017

# Required imports
import os
import sys
import copy
import re

# Internal package imports
#from EX2GK.machine_adapters.<device> import foo as bar


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via extras argument
def get_data(namelist):
    """
    <machine>-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides a single entry point into the Python scripts
    developed to access and process experimental data. For ease of use,
    only this file should be imported into any external script and only
    this function should be called to access the desired data.

    All arguments should be provided as a Python dictionary object via
    the argument namelist. The possible fields are listed below, and
    any other fields in the dictionary are ignored.
        <key>  =  <dtype>. <short_desc>.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: list. List of Python dictionary objects with data corresponding to the level of processing specified with outlevel argument.
    """
    device = "N/A"
    argdict = dict()
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

    # Containers for output data
    outlist  = None

#    <implementation-sepcific processing commands go here>

    return outlist


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def reset_fit_settings(inputdata,namelist):
    """
    <machine>-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides capability to reset the Gaussian process
    fit settings for re-fitting after data manipulation.

    :arg inputdata: dict. Python dictionary object with fully processed data according to the standardized adapter format.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: dict. Python dictionary object identical to input object except with GP fit settings added and / or reset to defaults.
    """
    outdata = None
    argdict = dict()
    if isinstance(inputdata,dict):
        outdata = copy.deepcopy(inputdata)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

#    <implementation-sepcific processing commands go here>

    return outdata


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def get_time_traces(namelist):
    """
    <machine>-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides capability to extract time traces
    needed for time window selection.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: dict. Python dictionary object with standardized time trace data.
    """
    outdata = None
    argdict = dict()
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

#    <implementation-sepcific processing commands go here>

    return outdata
