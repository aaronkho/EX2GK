# Script with functions to grab discharge data from AUG server via Python3 dd module
# Developer: Aaron Ho - 30/01/2018

# Required imports
import os
import sys
from operator import itemgetter
import numpy as np
import re
import copy
import datetime
import pickle
from scipy.interpolate import interp1d

# Required non-standard Python packages
from EX2GK.machine_adapters.aug import dd3 as dd


def getsig(shot,ti,tf,sfname,dfield,tfield=None,xfield=None,pfield=None,favg=True,edition=None,uid=None,nsplit=None):
    """
    AUG-SPECIFIC FUNCTION
    Obtain desired signal / data via Python3 version of dd module, with optional
    averaging, from a single data field over the specified time window. A 
    typical application would need to call this function multiple times to
    grab all the required data. This function does not check for valid inputs,
    instead outputting a blank numpy array if the inputs are invalid.

    :arg shot: int. Shot number within the AUG data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on AUG data system.

    :arg tf: float. End of data extraction time window, reference to time vector on AUG data system.

    :arg sfname: str. Name of shotfile from which data is to be taken.

    :arg dfield: str. Name of data field within chosen shot file to be extracted.

    :arg tfield: str. Optional name of time base corresponding to extracted data field.

    :arg xfield: str or list. Optional name(s) of area base corresponding to extracted data field.

    :arg pfield: str. Optional name of parameter when extracting from a parameter set in shot file.

    :kwarg favg: bool. Flag to toggle averaging of the extracted data over the specified time window.

    :kwarg edition: int. Optional edition number of data to be extracted within the AUG data system, default is 0.

    :kwarg uid: str. Optional user ID tag for accessing private entries within the AUG data system, default is AUGD.

    :kwarg nsplit: int. Optional number of equal time windows to split data into, default is 1.

    :returns: (array, array, array, float, float, int).
        Extracted data, vector of spatial or other coordinate points corresponding to extracted data, vector of
        time coordinate points corresponding to extracted data, time of first data point found, time of last
        data point found, number of data points found in terms of the time coordinate.
    """
    siglist = None
    xvec = None
    tvec = None
    timei = None
    timef = None
    ntp = 0
    data = None
    uuid = 'AUGD'
    if isinstance(uid,str):
        uuid = uid
    seq = 0
    if isinstance(edition,(float,int)) and int(edition) > 0:
        seq = int(edition)
    numt = int(nsplit) if isinstance(nsplit,(float,int)) and int(nsplit) > 0 else 1

    existflag = False
    try:
        sf = dd.shotfile(sfname,shot,edition=seq,experiment=uuid)
        existflag = True
    except:
        data = np.array([])

    if existflag:
        if isinstance(pfield,str):
            try:
                data = sf.GetParameter(dfield,pfield)
                if isinstance(data,(int,np.int16,np.int32,np.int64)):
                    data = np.array([int(data)])
                elif isinstance(data,(float,np.float16,np.float32,np.float64)):
                    data = np.array([float(data)])
                elif isinstance(data,str):
                    data = np.array([data])
                elif isinstance(data,(list,tuple)):
                    data = np.array(data)
                elif not isinstance(data,np.ndarray):
                    data = np.array([])
            except:
                data = None
        if data is None:
            try:
                data = sf.GetSignal(dfield)
                if isinstance(data,(list,tuple)):
                    data = np.array(data)
                elif not isinstance(data,np.ndarray):
                    data = np.array([])
            except:
                data = None
        if data is None:
            try:
                data = sf.GetAreabase(dfield)
                if isinstance(data,(list,tuple)):
                    data = np.array(data)
                elif not isinstance(data,np.ndarray):
                    data = np.array([])
            except:
                data = None
        if data is None:
            try:
                data = sf.GetTimebase(dfield)
                if isinstance(data,(list,tuple)):
                    data = np.array(data)
                elif not isinstance(data,np.ndarray):
                    data = np.array([])
            except:
                data = None
        if data is None:
            data = np.array([])

    if data.size > 0:
        if data.size < numt:
            print('   Requested signal %s/%s from shot #%10d does not have enough data for %3d time windows.' % (sfname,dfield,shot,numt))
            numt = 1
        if isinstance(tfield,str):
            try:
                tvec = sf.GetTimebase(tfield)
                if isinstance(tvec,(list,tuple)):
                    tvec = np.array(tvec)
                elif not isinstance(tvec,np.ndarray):
                    tvec = None
            except:
                tvec = None
            if tvec is None:
                try:
                    tvec = sf.GetSignal(tfield)
                    if isinstance(tvec,(list,tuple)):
                        tvec = np.array(tvec)
                    elif not isinstance(tvec,np.ndarray):
                        tvec = None
                except:
                    tvec = None
            if tvec is None:
                print('   Requested time base %s for signal %s/%s of shot #%10d is empty.' % (tfield,sfname,dfield,shot))
        if isinstance(xfield,str):
            try:
                xvec = sf.GetAreabase(xfield)
                if isinstance(xvec,(list,tuple)):
                    xvec = np.array(xvec)
                elif not isinstance(xvec,np.ndarray):
                    xvec = None
            except:
                xvec = None
            if xvec is None:
                try:
                    xvec = sf.GetSignal(xfield)
                    if isinstance(xvec,(list,tuple)):
                        xvec = np.array(xvec)
                    elif not isinstance(xvec,np.ndarray):
                        xvec = None
                except:
                    xvec = None
            if xvec is None:
                print('   Requested area base %s for signal %s/%s of shot #%10d is empty.' % (xfield,sfname,dfield,shot))
        elif isinstance(xfield,(list,tuple)):
            xvec = []
            for ii in np.arange(0,len(xfield)):
                temp = None
                try:
                    temp = sf.GetAreabase(xfield[ii])
                    if isinstance(temp,(list,tuple)):
                        temp = np.array(temp)
                    elif not isinstance(temp,np.ndarray):
                        temp = None
                except:
                    temp = None
                if temp is None:
                    try:
                        temp = sf.GetSignal(xfield[ii])
                        if isinstance(temp,(list,tuple)):
                            temp = np.array(temp)
                        elif not isinstance(temp,np.ndarray):
                            temp = None
                    except:
                        temp = None
                if temp is not None:
                    xvec.append(copy.deepcopy(temp))
                else:
                    print('   One of the requested area bases %s for signal %s/%s of shot #%10d is empty.' % (xfield[ii],sfname,dfield,shot))
            if len(xvec) != len(xfield):
                xvec = None
    else:
        print('   Requested signal %s/%s of shot #%10d is empty.' % (sfname,dfield,shot))

    if tvec is not None:
        siglist = []
        timei = tvec[0] if ti is None else ti
        timef = tvec[-1] if tf is None else tf
        deltat = float(timef - timei) / float(numt)
        for ii in np.arange(0,numt):
            siginfo = dict()
            signal = None
            itimei = timei + float(ii) * deltat if deltat > 1.0e-6 else timei
            idx = np.all([(tvec >= timei),(tvec <= timef)],axis=0)    # boolean array flagging desired data points
            siginfo["T1"] = float(itimei)
            siginfo["T2"] = float(itimei + deltat)

            # Automatically apply average over specified time window
            if np.any(idx):
                ivec = np.where(idx)[0]
                if xvec is None:
                    dshape = np.array(data.shape)
                    taxisv = np.where(dshape == tvec.size)[0]
                    if len(taxisv) == 0:
                        taxisv = np.where(dshape > 1000)[0]
                    taxis = taxisv[0] if len(taxisv) > 0 else None
                    if taxis is not None and taxis != 0:
                        data = np.swapaxes(data,0,taxis)
                    if favg:
                        signal = np.nanmean(np.take(data,ivec,axis=0),axis=0)
                    else:
                        signal = np.take(data,ivec,axis=0)
                elif isinstance(xvec,(list,tuple)):
                    dshape = np.array(data.shape)
                    taxisv = np.where(dshape == tvec.size)[0]
                    bigflag = False
                    if len(taxisv) == 0:
                        taxisv = np.where(dshape > 1000)[0]
                        bigflag = True
                    taxis = taxisv[0] if len(taxisv) > 0 else None
                    if taxis is not None and taxis != 0:
                        data = np.swapaxes(data,0,taxis)
                    xorder = []
                    for xx in np.arange(0,len(xvec)):
                        xdimlim = 1
                        xshape = np.array(xvec[xx].shape)
                        xaxisv = np.where(xshape == data.shape[0])[0]
                        fxt = True if len(xaxisv) > 0 else False
                        if fxt:
                            xaxis = xaxisv[0]
                            if xaxis != 0:
                                xvec[xx] = np.swapaxes(xvec[xx],0,xaxis)
                            if bigflag:
                                xvec[xx] = xvec[xx][:tvec.size]
                            xdimlim = 2
                        if xvec[xx].ndim > xdimlim:
                            for zz in np.arange(xdimlim-1,len(xvec[xx].shape)):
                                ddshape = np.array(data.shape[1:])
                                daxis = np.where(ddshape == xvec[xx].shape[zz])[0]
                                if len(daxis) > 0 and zz < daxis[0]:
                                    xvec[xx] = np.swapaxes(xvec[xx],zz,daxis[0])
                        elif data.ndim > 2:
                            ddshape = np.array(data.shape[1:])
                            xord = np.where(ddshape == xvec[xx].shape[xdimlim-1])[0]
                            if len(xord) > 0:
                                xorder.append(xord[0])
                            else:
                                xorder.append(xx)
                        if fxt and favg:
                            xvec[xx] = np.nanmean(np.take(xvec[xx],ivec,axis=0),axis=0)
                        elif fxt:
                            xvec[xx] = np.take(xvec[xx],ivec,axis=0)
                    if bigflag:
                        data = data[:tvec.size]
                    temp = copy.deepcopy(xvec)
                    for jj in np.arange(0,len(xorder)):
                        xvec[xorder[jj]] = temp[jj]
                    if favg:
                        signal = np.nanmean(np.take(data,ivec,axis=0),axis=0)
                    else:
                        signal = np.take(data,ivec,axis=0)
                elif isinstance(xvec,np.ndarray):
                    fxt = True if data.shape == xvec.shape else False
                    dshape = np.array(data.shape)
                    taxisv = np.where(dshape == tvec.size)[0]
                    bigflag = False
                    if len(taxisv) == 0:
                        taxisv = np.where(dshape > 1000)[0]
                        bigflag = True
                    taxis = taxisv[0] if len(taxisv) > 0 else None
                    if taxis is not None and taxis != 0:
                        data = np.swapaxes(data,0,taxis)
                    xdimlim = 1
                    xshape = np.array(xvec.shape)
                    xaxisv = np.where(xshape == data.shape[0])[0]
                    fxt = True if len(xaxisv) > 0 else False
                    if fxt:
                        xaxis = xaxisv[0]
                        if xaxis != 0:
                            xvec = np.swapaxes(xvec,0,xaxis)
                        if bigflag:
                            xvec = xvec[:tvec.size]
                        xdimlim = 2
                    if xvec.ndim > xdimlim:
                        for zz in np.arange(xdimlim-1,len(xvec.shape)):
                            ddshape = np.array(data.shape[1:])
                            daxis = np.where(ddshape == xvec.shape[zz])[0]
                            if len(daxis) > 0 and zz < daxis[0]:
                                xvec = np.swapaxes(xvec,zz,daxis[0])
                    if bigflag:
                        data = data[:tvec.size]
                    if favg:
                        signal = np.nanmean(np.take(data,ivec,axis=0),axis=0)
                        if fxt:
                            xvec = np.nanmean(np.take(xvec,ivec,axis=0),axis=0)
                    else:
                        signal = np.take(data,ivec,axis=0)
                        if fxt:
                            xvec = np.take(xvec,ivec,axis=0)
                siginfo["DATA"] = np.array(signal,dtype=np.float64)
                siginfo["XVEC"] = None
                if isinstance(xvec,(list,tuple)) and len(xvec) > 1:
                    rvec = copy.deepcopy(xvec[0])
                    zvec = copy.deepcopy(xvec[1])
                    if not favg:
                        rvec = rvec.squeeze()
                        zvec = zvec.squeeze()
                        rzt = 0
                        nxdata = nxdata = siginfo["DATA"].shape[1] if siginfo["DATA"].ndim > 1 else 1
                        if rvec.ndim == 1 and rvec.size != nxdata:
                            rzt = rvec.size
                        elif zvec.ndim == 1 and zvec.size != nxdata:
                            rzt = zvec.size
                        elif rvec.ndim > 1:
                            raxis = 0
                            ridxv = np.where(np.array(list(rvec.shape)) == nxdata)[0]
                            raxis = 1 if len(ridxv) > 0 and ridxv[0] == 0 else 0
                            rzt = rvec.shape[raxis]
                        elif zvec.ndim > 1:
                            zaxis = 0
                            zidxv = np.where(np.array(list(zvec.shape)) == nxdata)[0]
                            zaxis = 1 if len(zidxv) > 0 and zidxv[0] == 0 else 0
                            rzt = zvec.shape[zaxis]
                        rvec = np.atleast_2d(rvec)
                        zvec = np.atleast_2d(zvec)
                        if rvec.shape[0] != rzt and rvec.shape[1] == rzt:
                            rvec = np.transpose(rvec)
                        if zvec.shape[0] != rzt and zvec.shape[1] == rzt:
                            zvec = np.transpose(zvec)
#                        ridxv2 = np.where(np.array(list(rvec.shape)) == rzt)[0]
#                        if len(ridxv2) > 0:
#                            rvec = np.nanmean(rvec,axis=ridxv2[0]) if rvec.ndim > 1 else np.nanmean(rvec)
#                        zidxv2 = np.where(np.array(list(zvec.shape)) == rzt)[0]
#                        if len(zidxv2) > 0:
#                            zvec = np.nanmean(zvec,axis=zidxv2[0]) if zvec.ndim > 1 else np.nanmean(zvec)
                    siginfo["RVEC"] = np.array(rvec,dtype=np.float64)
                    siginfo["ZVEC"] = np.array(zvec,dtype=np.float64)
                elif isinstance(xvec,np.ndarray):
                    if not favg:
                        xvec = np.atleast_2d(xvec)
                    siginfo["XVEC"] = np.array(xvec,dtype=np.float64)
                siginfo["TVEC"] = np.array(tvec[idx],dtype=np.float64)
                siginfo["NAVG"] = siginfo["TVEC"].size
                if favg:
                    siginfo["TVEC"] = np.nanmean(siginfo["TVEC"])
            else:
                siginfo["DATA"] = None
                siginfo["XVEC"] = None
                siginfo["TVEC"] = None
                siginfo["NAVG"] = None
                print('   Requested time slice [%8.5f, %8.5f] not in %s/%s of shot #%10d.' % (timei,timef,sfname,dfield,shot))
            siglist.append(copy.deepcopy(siginfo))
    elif data.dtype == 'S1':
        tempd = np.char.array(data).transpose()
        tempo = []
        for ii in np.arange(0,tempd.shape[0]):
            sval = ''.join(tempd[ii,:].astype(str).tolist())
            tempo.append(sval.strip())
        siglist = []
        timei = tvec[0] if ti is None else ti
        timef = tvec[-1] if tf is None else tf
        deltat = float(timef - timei) / float(numt)
        tmid = timei + 0.5 * deltat
        for ii in np.arange(0,numt):
            if len(tempo) > 0:
                siginfo = dict()
                siginfo["DATA"] = np.array(tempo)
                siginfo["XVEC"] = None
                siginfo["TVEC"] = np.array([tmid],dtype=np.float64)
                siginfo["NAVG"] = 1
                siglist.append(siginfo)
            tmid = tmid + deltat
    if siglist is not None and len(siglist) == 0:
        print('   Requested signal %s/%s of shot #%10d is empty.' % (sfname,dfield,shot))
        siglist = None

    return siglist


def get_data_with_file(shot,ti,tf,datafile,ttw=None):
    """
    AUG-SPECIFIC FUNCTION
    Obtains the base shot data for given shot / times, where base shot data
    must be specified in a text file.

    :arg shot: int. Shot number within the AUG data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on AUG data system.

    :arg tf: float. End of data extraction time window, reference to time vector on AUG data system.

    :arg datafile: str. Name of file containing the shotfile name and field information to be extracted, along with averaging flags.

    :kwarg ttw: int. Integer flag indicating the plasma phase which this time window classifies as, for post-processing.

    :returns: dict. Implementation-specific object containing all data extracted and computed by this function.
    """
    snum = None
    timei = None
    timef = None
    twtype = -1
    if isinstance(shot,(int,float)) and int(shot) > 0:
        snum = int(shot)
    if isinstance(ti,(int,float)):
        timei = float(ti)
    if isinstance(tf,(int,float)):
        timef = float(tf)
    if isinstance(ttw,(float,int)) and int(ttw) >= 0:
        twtype = int(ttw)

    # Itemgetter objects extract desired elements from tuples, i.e. function outputs
    #   Specified here are common itemgetter configurations used in this function
    grab1 = itemgetter(0)        # Used with getsig to acquire singular data
    grab3 = itemgetter(0,1,2)    # Used with getsig to acquire data and radial coords and time coords

    RL = None
    if isinstance(snum,int) and datafile is not None and os.path.isfile(datafile):
        fields = dict()
        tfields = dict()
        xfields = dict()
        pfields = dict()
        flags = dict()
        edits = dict()
        uids = dict()
        with open(datafile,'r') as ff:
            for line in ff:
                target = 1
                ufields = line.strip().split()
                if len(ufields) > target and not re.search('^#',ufields[0]) and not re.search('^#',ufields[1]):
                    usfn = ufields[0].upper()
                    usff = ufields[1]
                    utag = "/".join([usfn,usff])
                    cline = False
                    target = target + 1

                    usft = None
                    if len(ufields) > target and not re.search('^#',ufields[target]) and not re.match('^-+$',ufields[target]):
                        usft = ufields[target]
                    elif len(ufields) > target and re.search('^#',ufields[target]):
                        cline = True
                    target = target + 1

                    usfx = None
                    if len(ufields) > target and not re.search('^#',ufields[target]) and not re.match('^-+$',ufields[target]):
                        if re.search(',',ufields[target]):
                            usfx = ufields[target].split(',')
                        else:
                            usfx = ufields[target]
                    elif len(ufields) > target and re.search('^#',ufields[target]):
                        cline = True
                    target = target + 1

                    tusft = usft if usft is not None else ''
                    tusfx = ''
                    if isinstance(usfx,(list,tuple)):
                        tusfx = "|".join(usfx)
                    elif usfx is not None:
                        tusfx = usfx
                    utag = "/".join([utag,tusft,tusfx])
                    umap = "|".join([usfn,usff,tusft,tusfx])

                    if len(ufields) > target and not re.search('^#',ufields[target]) and not re.match('^-+$',ufields[target]):
                        umap = ufields[target].upper()
                    elif len(ufields) > target and re.search('^#',ufields[target]):
                        cline = True
                    target = target + 1

                    uavg = True
                    usingle = False
                    if len(ufields) > target and not re.search('^#',ufields[target]) and not cline:
                        if int(ufields[target]) == 1:
                            uavg = False
                        elif int(ufields[target]) == 2:
                            uavg = False
                            usingle = True
                    elif len(ufields) > target and re.search('^#',ufields[target]):
                        cline = True
                    target = target + 1

                    uedit = None
                    if len(ufields) > target and not re.search('^#',ufields[target]) and not cline:
                        if re.search('[0-9]+',ufields[target]) and int(ufields[target]) >= 0:
                            uedit = int(ufields[target])
                    elif len(ufields) > target and re.search('^#',ufields[target]):
                        cline = True
                    target = target + 1

                    uuid = None
                    if len(ufields) > target and not re.search('^#',ufields[target]) and not cline:
                        uuid = ufields[target].lower()
                    elif len(ufields) > target and re.search('^#',ufields[target]):
                        cline = True
                    target = target + 1

                    fields[utag] = umap
                    tfields[utag] = usft if not usingle else None
                    xfields[utag] = usfx if not usingle else None
                    pfields[utag] = usft if usingle else None
                    flags[utag] = (uavg,usingle)
                    edits[utag] = uedit
                    uids[utag] = uuid

        if RL is None:
            RL = []
            RD = dict()
            RD["DEVICE"] = "AUG"
            RD["INTERFACE"] = "DD3"
            RD["SHOT"] = snum
            RD["SHOTPHASE"] = twtype
            RD["WINDOW"] = 0
            RD["MAP"] = fields
            RD["FLAGS"] = flags
            RD["LAST_UPDATE"] = datetime.date
            deltat = float(timef - timei) / float(ntw)
            for jj in np.arange(0,ntw):
                RD["T1"] = timei + float(jj) * deltat
                RD["T2"] = RD["T1"] + deltat
                RL.append(copy.deepcopy(RD))

        # Loop to check data from AUGD system before full query - TODO!

        # Loop to grab data from AUG data system under the specified shot files and field names
        for (key,tag) in fields.items():
            name = key.split('/')
            avgflag = flags[key][0]
            oneflag = flags[key][1]
#            print(name[0],name[1],tfields[key],xfields[key],pfields[key],avgflag,oneflag)
            if oneflag:
                siglist = getsig(snum,None,None,name[0],name[1],pfield=pfields[key],edition=edits[key],uid=uids[key])
                for jj in np.arange(0,ntw):
                    RL[jj][tag] = siglist[0]["DATA"] if siglist is not None else None
            else:
                siglist = getsig(snum,timei,timef,name[0],name[1],tfield=tfields[key],xfield=xfields[key],favg=avgflag,edition=edits[key],uid=uids[key])
                for jj in np.arange(0,ntw):
                    RL[jj][tag] = siglist[jj]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[jj]["XVEC"] if siglist is not None else None
                    if siglist is not None and "RVEC" in siglist[jj]:
                        RL[jj]["R_"+tag] = siglist[jj]["RVEC"]
                        if "Z_"+tag not in RL[jj]:
                            RL[jj]["Z_"+tag] = None
                    if siglist is not None and "ZVEC" in siglist[jj]:
                        RL[jj]["Z_"+tag] = siglist[jj]["ZVEC"]
                        if "R_"+tag not in RL[jj]:
                            RL[jj]["R_"+tag] = None
                    RL[jj]["T_"+tag] = siglist[jj]["TVEC"] if siglist is not None else None

    return RL


def get_data_with_dict(shot,ti,tf,datadict,ttw=None,nwindows=None):
    """
    AUG-SPECIFIC FUNCTION
    Obtains the base shot data for given shot / times, where base shot data
    must be specified in a text file.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg datadict: dict. Python dictionary containing the DDA and DTYPE information to be extracted as keys, along with a list of settings as values.

    :kwarg ttw: int. Integer flag indicating the plasma phase which this time window classifies as, for post-processing.

    :kwarg nwindows: int. Optional number of equal time windows to split data into, default is 1.

    :returns: dict. Implementation-specific object containing all data extracted and computed by this function.
    """
    snum = None
    timei = None
    timef = None
    indict = None
    twtype = -1
    ntw = 1
    if isinstance(shot,(int,float)) and int(shot) > 0:
        snum = int(shot)
    if isinstance(ti,(int,float)):
        timei = float(ti)
    if isinstance(tf,(int,float)):
        timef = float(tf)
    if isinstance(datadict,dict):
        indict = datadict
    if isinstance(ttw,(float,int)) and int(ttw) >= 0:
        twtype = int(ttw)
    if isinstance(nwindows,(int,float)) and int(nwindows) > 0:
        ntw = int(nwindows)

    RL = None
    if isinstance(snum,int) and isinstance(indict,dict):
        fields = dict()
        tfields = dict()
        xfields = dict()
        pfields = dict()
        flags = dict()
        edits = dict()
        uids = dict()
        trees = dict()
        for key, value in indict.items():
            if isinstance(key,str) and isinstance(value,(list,tuple)):
                usfn = key.split('/')[0].upper()
                usff = key.split('/')[1]
                utag = "/".join([usfn,usff])
                cline = False
                target = 0

                usft = None
                if len(value) > target and not re.search('^#',value[target]) and not re.match('^-+$',value[target]):
                    usft = value[target]
                elif len(value) > target and re.search('^#',value[target]):
                    cline = True
                target = target + 1

                usfx = None
                if len(value) > target and not re.search('^#',value[target]) and not re.match('^-+$',value[target]):
                    if re.search(',',value[target]):
                        usfx = value[target].split(',')
                    else:
                        usfx = value[target]
                elif len(value) > target and re.search('^#',value[target]):
                    cline = True
                target = target + 1

                tusft = usft if usft is not None else ''
                tusfx = ''
                if isinstance(usfx,(list,tuple)):
                    tusfx = "|".join(usfx)
                elif usfx is not None:
                    tusfx = usfx
                utag = "/".join([utag,tusft,tusfx])
                umap = "|".join([usfn,usff,tusft,tusfx])

                if len(value) > target and not re.search('^#',value[target]) and not re.match('^-+$',value[target]):
                    umap = value[target].upper()
                elif len(value) > target and re.search('^#',value[target]):
                    cline = True
                target = target + 1

                uavg = True
                usingle = False
                if len(value) > target and not re.search('^#',value[target]) and not cline:
                    if int(value[target]) == 1:
                        uavg = False
                    elif int(value[target]) == 2:
                        uavg = False
                        usingle = True
                elif len(value) > target and re.search('^#',value[target]):
                    cline = True
                target = target + 1

                uedit = None
                if len(value) > target and not re.search('^#',value[target]) and not cline:
                    if re.search('[0-9]+',value[target]) and int(value[target]) >= 0:
                        uedit = int(value[target])
                elif len(value) > target and re.search('^#',value[target]):
                    cline = True
                target = target + 1

                uuid = None
                if len(value) > target and not re.search('^#',value[target]) and not cline:
                    uuid = value[target].lower()
                elif len(value) > target and re.search('^#',value[target]):
                    cline = True
                target = target + 1

                fields[utag] = umap
                tfields[utag] = usft if not usingle else None
                xfields[utag] = usfx if not usingle else None
                pfields[utag] = usft if usingle else None
                flags[utag] = (uavg,usingle)
                edits[utag] = uedit
                uids[utag] = uuid

                if usfn not in trees:
                    trees[usfn] = utag

        if RL is None:
            RL = []
            RD = dict()
            RD["DEVICE"] = "AUG"
            RD["INTERFACE"] = "DD3"
            RD["SHOT"] = snum
            RD["SHOTPHASE"] = twtype
            RD["WINDOW"] = 0
            RD["MAP"] = fields
            RD["FLAGS"] = flags
            RD["LAST_UPDATE"] = datetime.date
            deltat = float(timef - timei) / float(ntw)
            for jj in np.arange(0,ntw):
                RD["T1"] = timei + float(jj) * deltat
                RD["T2"] = RD["T1"] + deltat
                RL.append(copy.deepcopy(RD))

        # Loop to check data from AUGD system before full query - TODO!
#        glist = []
#        for key, value in trees.items():
#            name = fields[value].split('|')
#            avgflag = flags[value][0]
#            oneflag = flags[value][1]
#            if oneflag:
#               siglist = getsig(snum,None,None,name[0],name[1],pfield=pfields[key],favg=True,edition=edits[key],uid=uids[key])
#            else:
#                siglist = getsig(snum,timei,timef,name[0],name[1],tfield=tfields[key],xfield=xfields[key],favg=avgflag,edition=edits[key],uid=uids[key])

        # Loop to grab data from AUGD system under the specified shotfiles and data fields
        for (key,tag) in fields.items():
            name = key.split('/')
            avgflag = flags[key][0]
            oneflag = flags[key][1]
            if oneflag:
                siglist = getsig(snum,None,None,name[0],name[1],pfield=pfields[key],favg=True,edition=edits[key],uid=uids[key],nsplit=None)
                for jj in np.arange(0,ntw):
                    RL[jj][tag] = siglist[0]["DATA"] if siglist is not None else None
            else:
                siglist = getsig(snum,timei,timef,name[0],name[1],tfield=tfields[key],xfield=xfields[key],favg=avgflag,edition=edits[key],uid=uids[key],nsplit=ntw)
                for jj in np.arange(0,ntw):
                    RL[jj][tag] = siglist[jj]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[jj]["XVEC"] if siglist is not None else None
                    if siglist is not None and "RVEC" in siglist[jj]:
                        RL[jj]["R_"+tag] = siglist[jj]["RVEC"]
                        if "Z_"+tag not in RL[jj]:
                            RL[jj]["Z_"+tag] = None
                    if siglist is not None and "ZVEC" in siglist[jj]:
                        RL[jj]["Z_"+tag] = siglist[jj]["ZVEC"]
                        if "R_"+tag not in RL[jj]:
                            RL[jj]["R_"+tag] = None
                    RL[jj]["T_"+tag] = siglist[jj]["TVEC"] if siglist is not None else None

    return RL
