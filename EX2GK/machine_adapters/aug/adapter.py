# Adapter file for AUG data access - this should be the only imported file outside this directory
# Developer: Aaron Ho - 30/01/2018

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools
gmod2 = None
gmod3 = None
try:
    from EX2GK.machine_adapters.aug import extract_aug_dd as gmod2
except ImportError:
    gmod2 = None
try:
    from EX2GK.machine_adapters.aug import extract_aug_dd3 as gmod3
except ImportError:
    gmod3 = None
from EX2GK.machine_adapters.aug import process_aug as pmod
from EX2GK.machine_adapters.aug import standardize_aug as smod
from EX2GK.machine_adapters.aug import fit_settings_aug as kmod
from EX2GK.machine_adapters.aug import time_selector as tmod


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via extras argument
def get_data(namelist):
    """
    AUG-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides a single entry point into the adapter scripts
    developed to access and process AUG data. For ease of use, only
    this file should be imported into any user-defined script and only
    this function should be used to extract the desired data.

    The output container should be in the same generalized format and
    structure as other implementations, minus the kernel selection and
    fit settings for the GPR1D profile fitting tool. The concept is
    that any user-defined scripts defined strictly on the data
    structure here should be portable to any other implementation.

    An extra namelist can be provided via a Python dictionary object
    into the argument extras. The possible fields are listed below, and
    any other fields in the dictionary are ignored by these scripts.
        SHOTINFO      =  str. Contains shot number, time start, and time end, space-separated.
        INTERFACE     =  str. Name of requested data access interface to be used.
        DATAFILE      =  str. Name of file containing shotfile and field info, with averaging flags.
        DATADICT      =  dict. Python dictionary containing shotfile and field info, with time base and area base info.
        OUTPUTLEVEL   =  int. 1 = raw data, 2 = proc. raw data, 3 = std. data, 4 = with opt. GP settings.
        SHOTPHASE     =  int. Phase of the discharge, used in processing decisions.
        SELECTEQ      =  str. Equilibrium selection, used in processing decisions.
        GPCOORDFLAG   =  bool. Unify coordinate systems via GP fitting.
        RAWCOORDFLAG  =  bool. Keep raw coordinate data after unification.
        CDEBUG        =  bool. Print out raw coordinate data to files, for debugging.
        RHOEBFLAG     =  bool. Provide calculated rho errors in output, not meaningful at this moment.
        POLFLAG       =  bool. Express standardized profiles with respect to rho_pol_norm.
        MIXDIAGFLAG   =  bool. Combine all ne and Te diagnostics if available.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: dict. Python dictionary object with data corresponding to the level of processing specified with outlevel argument.
    """
    device = "AUG"
    argdict = dict()
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

    # Containers for output data
    outdata  = None

    shot = None
    ti = None
    tf = None
    iname = 'dd3'
    dfile = None
    ddict = None
    outlvl = 0
    pickledir = None
    if "SHOTINFO" in argdict and isinstance(argdict["SHOTINFO"],str):
        expstr = argdict["SHOTINFO"].strip()
        expinfo = expstr.split()
        if len(expinfo) > 0:
            shot = int(float(expinfo[0]))
        if len(expinfo) > 1:
            ti = float(expinfo[1])
        if len(expinfo) > 2:
            tf = float(expinfo[2])
    else:
        raise TypeError("Experimental information must be passed as a space-separable string.")
    if "INTERFACE" in argdict and isinstance(argdict["INTERFACE"],str):
        iname = argdict["INTERFACE"]
    if "DATAFILE" in argdict and isinstance(argdict["DATAFILE"],str) and os.path.isfile(argdict["DATAFILE"]):
        dfile = argdict["DATAFILE"]
    elif "DATADICT" in argdict and isinstance(argdict["DATADICT"],dict) and argdict["DATADICT"]:
        ddict = copy.deepcopy(argdict["DATADICT"])
    else:
        print("Data extraction specification not provided or invalid, defaulting to preset fields.")
    if "OUTPUTLEVEL" in argdict and isinstance(argdict["OUTPUTLEVEL"],(int,float)):
        outlvl = int(argdict["OUTPUTLEVEL"])
    if "SHOTPHASE" in argdict and isinstance(argdict["SHOTPHASE"],(int,float)) and int(argdict["SHOTPHASE"]) >= 0:
        ttw = int(argdict["SHOTPHASE"])
    fdebug = "DEBUGFLAG" in argdict and argdict["DEBUGFLAG"]

    # Note for scripting: Data extraction is designed to produce fatal error if 'shot' is not a non-zero positive integer.
    twlist = None
    if isinstance(dfile,str) and os.path.isfile(dfile):
        if re.match(r'^dd2$',iname,flags=re.IGNORECASE):
            twlist = gmod2.get_data_with_file(shot,ti,tf,dfile,ttw=ttw)
        elif re.match(r'^dd3$',iname,flags=re.IGNORECASE):
            twlist = gmod3.get_data_with_file(shot,ti,tf,dfile,ttw=ttw)
        else:
            raise ImportError("%s-specific adapter for %s data access interface not found." % (device.upper(),iname.upper()))
    elif isinstance(ddict,dict):
        nwindows = argdict["NWINDOWS"] if "NWINDOWS" in argdict and isinstance(argdict["NWINDOWS"],(int,float)) else 1
        if re.match(r'^dd2$',iname,flags=re.IGNORECASE):
            twlist = gmod2.get_data_with_dict(shot,ti,tf,ddict,ttw=ttw,nwindows=nwindows)
        elif re.match(r'^dd3$',iname,flags=re.IGNORECASE):
            twlist = gmod3.get_data_with_dict(shot,ti,tf,ddict,ttw=ttw,nwindows=nwindows)
        else:
            raise ImportError("%s-specific adapter for %s data access interface not found." % (device.upper(),iname.upper()))

    if isinstance(twlist,list):

        outlist = []
        for jj in np.arange(0,len(twlist)):

            datadict = twlist[jj]
            outobj = None
 
            if pmod is not None and isinstance(datadict,dict) and (outlvl == 0 or outlvl > 1):

                if "MAP" in datadict and datadict["MAP"] is not None:

                    eqselect = argdict["SELECTEQ"].lower() if "SELECTEQ" in argdict and isinstance(argdict["SELECTEQ"],str) else None
                    gpc = argdict["GPCOORDFLAG"] if "GPCOORDFLAG" in argdict else False
                    cdebug = True if "CDEBUG" in argdict and argdict["CDEBUG"] else False

                    # Unpack the necessary data for the next processing steps, discards all other raw data unless save flags specified
                    tempdict = pmod.transfer_generic_data(datadict,newstruct=None,gpcoord=gpc,fdebug=fdebug)
                    tempdict = pmod.define_equilibrium(datadict,newstruct=tempdict,equilibrium=eqselect,fdebug=fdebug)
                    tempdict = pmod.unpack_general_data(datadict,newstruct=tempdict,fdebug=fdebug)
                    tempdict = pmod.unpack_coord_data(datadict,newstruct=tempdict,fdebug=fdebug)
                    tempdict = pmod.unpack_profile_data(datadict,newstruct=tempdict,fdebug=fdebug)
#                    tempdict = pmod.unpack_source_data(datadict,newstruct=tempdict,fdebug=fdebug)
                    tempdict = pmod.calculate_coordinate_systems(tempdict,cdebug=cdebug,fdebug=fdebug)

                    if isinstance(tempdict,dict):
                        datadict = copy.deepcopy(tempdict)

                else:
                    datadict = None

            if smod is not None and kmod is not None and datadict is not None and (outlvl == 0 or outlvl > 2):

                # Initialize required classes
                MC = classes.EX2GKMetaContainer()
                CC = classes.EX2GKCoordinateContainer()
                ZC = classes.EX2GKZeroDimensionalContainer()
                RC = classes.EX2GKRawDataContainer()
                KC = classes.EX2GKFitSettingsContainer()
                FC = classes.EX2GKFlagContainer()

                pf = argdict["POLFLAG"] if "POLFLAG" in argdict else False
                fxeb = argdict["RHOEBFLAG"] if "RHOEBFLAG" in argdict else False

                ebmults = dict()
                ebmults["NE"] = argdict["NEEBMULT"] if "NEEBMULT" in argdict else None
                ebmults["TE"] = argdict["TEEBMULT"] if "TEEBMULT" in argdict else None
                ebmults["NI"] = argdict["NIEBMULT"] if "NIEBMULT" in argdict else None
                ebmults["TI"] = argdict["TIEBMULT"] if "TIEBMULT" in argdict else None
                ebmults["NIMP"] = argdict["NIMPEBMULT"] if "NIMPEBMULT" in argdict else None
                ebmults["TIMP"] = argdict["TIMPEBMULT"] if "TIMPEBMULT" in argdict else None
                ebmults["AF"] = argdict["AFEBMULT"] if "AFEBMULT" in argdict else None
                ebmults["Q"] = argdict["QEBMULT"] if "QEBMULT" in argdict else None

                # Separates and formats the diagnostic data for filtering
                diagdict = smod.standardize_diagnostic_data(datadict,newstruct=None,fdebug=fdebug)
                srcdict = smod.standardize_source_data(datadict,newstruct=None,fdebug=fdebug)

                stddict = smod.transfer_generic_data(datadict,newstruct=None,usepol=pf,usexeb=fxeb,fdebug=fdebug)
                stddict = smod.filter_standardized_diagnostic_data(diagdict,newstruct=stddict,userscales=ebmults,fdebug=fdebug)
                stddict = smod.combine_edge_diagnostic_data(stddict,fdebug=fdebug)
                stddict = smod.add_profile_boundary_data(stddict,fdebug=fdebug)
                stddict = smod.transfer_standardized_source_data(srcdict,newstruct=stddict,fdebug=fdebug)
                stddict = smod.add_derivative_constraint_data(stddict,fdebug=fdebug)

                stddict = kmod.generate_fit_kernel_settings(stddict,fdebug=fdebug)

                # Transfer generic metadata
                device = stddict["DEVICE"] if "DEVICE" in stddict else "AUG"
                interface = stddict["INTERFACE"] if "INTERFACE" in stddict and stddict["INTERFACE"] is not None else "UNKNOWN"
                shotnum = stddict["SHOT"] if "SHOT" in stddict else None
                t1 = stddict["T1"] if "T1" in stddict else None
                t2 = stddict["T2"] if "T2" in stddict else None
                window = stddict["WINDOW"] if "WINDOW" in stddict and stddict["WINDOW"] is not None else -1
                shotphase = stddict["SHOTPHASE"] if "SHOTPHASE" in stddict and stddict["SHOTPHASE"] is not None else -1
                eqsrc = stddict["EQLIST"][0].upper() if "EQLIST" in stddict and len(stddict["EQLIST"]) > 0 else "UNKNOWN"
                wallmat = stddict["WALLMAT"] if "WALLMAT" in stddict else "UNKNOWN"
                contmat = stddict["CONTMAT"] if "CONTMAT" in stddict else "UNKNOWN"
                pconfig = 1       # Assumed divertor configuration if not specified

                MC.setRequiredMetaData(device,interface,shotnum,t1,t2,window,shotphase,eqsrc,wallmat,contmat,pconfig)
                if "FH" in stddict or "FD" in stddict or "FT" in stddict:
                    mtags = []
                    fracs = np.array([])
                    fracebs = np.array([])
                    for material in ["H","D","T"]:
                        if "F"+material in stddict:
                            mtags.append(material)
                            fracs = np.hstack((fracs,stddict["F"+material]))
                            fracebs = np.hstack((fracebs,stddict["F"+material+"EB"]))
                    idxmax = np.where(fracs >= np.nanmax(fracs))[0][0]
                    MC.addIonMetaData(name=mtags[idxmax],weight=fracs[idxmax])
                    for matid in np.arange(0,len(mtags)):
                        if matid != idxmax and fracs[matid] >= 0.1:
                            MC.addIonMetaData(name=mtags[matid],weight=fracs[matid])
                        ZC.addData("FRAC"+mtags[matid].upper(),float(fracs[matid]),float(fracebs[matid]))
                elif "PLASMAT" in stddict and isinstance(stddict["PLASMAT"],str):
                    MC.addIonMetaData(name=stddict["PLASMAT"],weight=1.0)
                    ZC.addData("FRAC"+stddict["PLASMAT"].upper(),1.0,0.0)
                else:
                    MC.addIonMetaData(name="D",weight=1.0)         # Assumed deuterium plasma if data not given
                    ZC.addData("FRACD",1.0,0.0)

                ebmkeys = ["NE","TE","NI","TI","NIMP","TIMP","AF","Q"]
                for item in ebmkeys:
                    if item+"EBMULT" in stddict and stddict[item+"EBMULT"] is not None:
                        MC.addValue(item.upper()+"EBMULT",stddict[item+"EBMULT"])

                if fdebug:
                    print("aug - adapter.py: %s populated." % (type(MC).__name__))

                csdict = {"POLFLUXN": "PSIPOLN", "POLFLUX": "PSIPOL", "RHOPOLN": "RHOPOLN", "TORFLUXN": "PSITORN", "TORFLUX": "PSITOR", "RHOTORN": "RHOTORN", \
                          "RMAJORO": "RMAJORO", "RMAJORI": "RMAJORI", "RMINORO": "RMINORO", "RMINORI": "RMINORI", "FSVOL": "FSVOL", "FSAREA": "FSAREA"}
                if "CS_BASE" in stddict and stddict["CS_BASE"] is not None and stddict["CS_BASE"] in csdict:
                    if "CS_"+stddict["CS_BASE"] in stddict and stddict["CS_"+stddict["CS_BASE"]] is not None:
                        jbase = stddict["CJ_BASE"] if "CJ_BASE" in stddict else stddict["CS_BASE"]
                        CS = classes.EX2GKCoordinateSystem()
                        CS.addCoordinate(csdict[stddict["CS_BASE"]],stddict["CS_"+stddict["CS_BASE"]],cs_prefix="")
                        for key in csdict:
                            if not re.match(r'^'+stddict["CS_BASE"]+r'$',key,flags=re.IGNORECASE) and "CS_"+key in stddict and stddict["CS_"+key] is not None:
                                jvec = stddict["CJ_"+jbase+"_"+key].copy() if "CJ_"+jbase+"_"+key in stddict else None
                                evec = stddict["CE_"+key].copy() if "CE_"+key in stddict else None
                                CS.addCoordinate(csdict[key],stddict["CS_"+key],jvec,evec,cs_prefix="")
                        prefix = "C"
                        if prefix+"CS_BASE" in stddict and stddict[prefix+"CS_BASE"] is not None and stddict[prefix+"CS_BASE"] in csdict:
                            jbase = stddict[prefix+"CJ_BASE"] if prefix+"CJ_BASE" in stddict else stddict[prefix+"CS_BASE"]
                            if "CS_BASECORR" in stddict and stddict["CS_BASECORR"] is not None:
                                jvec = stddict["CJ_"+jbase+"_BASECORR"].copy() if "CJ_"+jbase+"_BASECORR" in stddict else None
                                evec = stddict["CE_BASECORR"].copy() if "CE_BASECORR" in stddict else None
                                CS.addConversionCoordinate(prefix,stddict["CS_BASECORR"],jvec,evec,cs_suffix="")
                            elif "CS_BASECORRO" in stddict and stddict["CS_BASECORRO"] is not None and "CS_BASECORRI" in stddict and stddict["CS_BASECORRI"] is not None:
                                jvec = stddict["CJ_"+jbase+"_BASECORRO"].copy() if "CJ_"+jbase+"_BASECORRO" in stddict else None
                                evec = stddict["CE_BASECORRO"].copy() if "CE_BASECORRO" in stddict else None
                                CS.addConversionCoordinate(prefix,stddict["CS_BASECORRO"],jvec,evec,cs_suffix="O")
                                jvec = stddict["CJ_"+jbase+"_BASECORRI"].copy() if "CJ_"+jbase+"_BASECORRI" in stddict else None
                                evec = stddict["CE_BASECORRI"].copy() if "CE_BASECORRI" in stddict else None
                                CS.addConversionCoordinate(prefix,stddict["CS_BASECORRI"],jvec,evec,cs_suffix="I")
                            CC.addCoordinateSystemObject(CS)
                            CCS = classes.EX2GKCoordinateSystem()
                            CCS.addCoordinate(csdict[stddict[prefix+"CS_BASE"]],stddict[prefix+"CS_"+stddict[prefix+"CS_BASE"]],cs_prefix=prefix)
                            for key in csdict:
                                if not re.match(r'^'+stddict[prefix+"CS_BASE"]+r'$',key,flags=re.IGNORECASE) and prefix+"CS_"+key in stddict and stddict[prefix+"CS_"+key] is not None:
                                    jvec = stddict[prefix+"CJ_"+jbase+"_"+key].copy() if prefix+"CJ_"+jbase+"_"+key in stddict else None
                                    evec = stddict[prefix+"CE_"+key].copy() if prefix+"CE_"+key in stddict else None
                                    CCS.addCoordinate(csdict[key],stddict[prefix+"CS_"+key],jvec,evec,cs_prefix=prefix)
                            CC.addCoordinateSystemObject(CCS)
                        else:
                            CC.addCoordinateSystemObject(CS)

                if fdebug:
                    print("aug - adapter.py: %s populated." % (type(CC).__name__))

                # Transfer 0D data
                rmag = float(stddict["RMAG"]) if "RMAG" in stddict and stddict["RMAG"] is not None else 3.0
                rmageb = float(stddict["RMAGEB"]) if "RMAGEB" in stddict and stddict["RMAGEB"] is not None else None
                zmag = float(stddict["ZMAG"]) if "ZMAG" in stddict and stddict["ZMAG"] is not None else 0.0
                zmageb = float(stddict["ZMAGEB"]) if "ZMAGEB" in stddict and stddict["ZMAGEB"] is not None else None
                bmag = float(stddict["BMAG"]) if "BMAG" in stddict and stddict["BMAG"] is not None else 3.0
                bmageb = float(stddict["BMAGEB"] + 0.04) if "BMAGEB" in stddict and stddict["BMAGEB"] is not None else 0.04
                ip = float(stddict["IP"]) if "IP" in stddict and stddict["IP"] is not None else 3.0e6
                ipeb = float(stddict["IPEB"]) if "IPEB" in stddict else None
                ZC.addRequiredData(rmag,zmag,ip,bmag,rmageb,zmageb,ipeb,bmageb)

                rgeo = float(stddict["RGEO"]) if "RGEO" in stddict and stddict["RGEO"] is not None else 2.96
                rgeoeb = float(stddict["RGEOEB"]) if "RGEOEB" in stddict and stddict["RGEOEB"] is not None else None
                zgeo = float(stddict["ZGEO"]) if "ZGEO" in stddict and stddict["ZGEO"] is not None else 0.0
                zgeoeb = float(stddict["ZGEOEB"]) if "ZGEOEB" in stddict and stddict["ZGEOEB"] is not None else None
                bvgeo = float(stddict["BVAC"]) if "BVAC" in stddict and stddict["BVAC"] is not None else None
                bvgeoeb = float(stddict["BVACEB"]) if "BVACEB" in stddict and stddict["BVACEB"] is not None else None
                ZC.addData("RGEO",rgeo,rgeoeb)
                ZC.addData("ZGEO",zgeo,zgeoeb)
                ZC.addData("BVGEO",bvgeo,bvgeoeb)

                if "RMAJSHIFT" in stddict and stddict["RMAJSHIFT"] is not None:
                    rshift = float(stddict["RMAJSHIFT"])
                    rshifteb = float(stddict["RMAJSHIFTEB"]) if "RMAJSHIFTEB" in stddict and stddict["RMAJSHIFTEB"] is not None else 0.0
                    ZC.addData("RSHIFT",rshift,rshifteb)

                if "NESCALE" in stddict and stddict["NESCALE"] is not None:
                    nescale = float(stddict["NESCALE"])
                    nescaleeb = float(stddict["NESCALEEB"]) if "NESCALEEB" in stddict and stddict["NESCALEEB"] is not None else 0.0
                    ZC.addData("NESCALE",nescale,nescaleeb)

                for src in ["NBI","ICRH","ECRH","LH","OHM"]:
                    if "P"+src in stddict and stddict["P"+src] is not None:
                        pi = float(stddict["P"+src])
                        pieb = float(stddict["P"+src+"EB"]) if "P"+src+"EB" in stddict and stddict["P"+src+"EB"] is not None else 0.0
                        ZC.addData("POWI"+src,pi,pieb)
                        if "L"+src in stddict and stddict["L"+src] is not None:
                            pl = float(stddict["L"+src])
                            pleb = float(stddict["L"+src+"EB"]) if "L"+src+"EB" in stddict and stddict["L"+src+"EB"] is not None else 0.0
                            ZC.addData("POWL"+src,pl,pleb)

                if "TWD" in stddict and stddict["TWD"] is not None:
                    wp = float(stddict["TWD"])
                    wpeb = float(stddict["TWDEB"]) if "TWDEB" in stddict and stddict["TWDEB"] is not None else 0.05 * wp
                    ZC.addData("WEXP",wp,wpeb)

                if "BVAC" in stddict and stddict["BVAC"] is not None:
                    ZC.addData("BVAC",float(stddict["BVAC"]),0.0)
                if "VAVG" in stddict and stddict["VAVG"] is not None:
                    ZC.addData("VAVG",float(stddict["VAVG"]),0.0)
                if "BETAND" in stddict and stddict["BETAND"] is not None:
                    ZC.addData("BETANEXP",float(stddict["BETAND"]),0.0)
                if "BETAPD" in stddict and stddict["BETAPD"] is not None:
                    ZC.addData("BETAPEXP",float(stddict["BETAPD"]),0.0)

                # Select lowest of two Z-effective measurements as standard flat Z-effective estimate
                wwflag = True if re.match(r'^W$',wallmat,flags=re.IGNORECASE) else False
                fzeff = 0.0
                fzeffeb = 0.0
                if "ZEFV" in stddict and stddict["ZEFV"] is not None and (fzeff < 1.0 or stddict["ZEFV"] < fzeff):
                    fzeff = float(stddict["ZEFV"])
                    fzeffeb = float(stddict["ZEFVEB"]) if "ZEFVEB" in stddict and stddict["ZEFVEB"] is not None else 0.1 * fzeff
                if "ZEFH" in stddict and stddict["ZEFH"] is not None and (fzeff < 1.0 or stddict["ZEFH"] < fzeff):
                    fzeff = float(stddict["ZEFH"])
                    fzeffeb = float(stddict["ZEFHEB"]) if "ZEFHEB" in stddict and stddict["ZEFHEB"] is not None else 0.1 * fzeff
                if fzeff < 1.0:
                    fzeff = 1.5
                    fzeffeb = 0.1
                ZC.addData("FZEFF",fzeff,fzeffeb)
                FC.setFlag("FLATZEFF",True)

                if fdebug:
                    print("aug - adapter.py: %s populated." % (type(ZC).__name__))

                # Specify impurity-related quantities, defined using assumptions and known limitations
                zz1 = 5.0
                zlim1 = 1.2
                zz2 = None
                # Possible source of higher Z impurities in CW campaigns, although N, O, or Ne are also likely candidates, Ar chosen for lowest fuel dilution
                if fzeff > zlim1:
                    zz2 = 74.0

                prefix_list = CC.getPrefixes()
                prefix = prefix_list[1] if len(prefix_list) > 1 and stddict["PSICORRFLAG"] else prefix_list[0]
#                MC.addValue("CSPUSED",prefix)

                varconv = {"NE": "NE", "TE": "TE", "NI": "NI", "TI": "TI", "NIMP": "NIMP", "TIMP": "TIMP", "AF": "AFTOR", "Q": "Q", \
                           "S": "SNI", "QE": "STE", "QI": "STI", "TAU": "SP", "J": "J", "NFI": "NFI", "WFI": "WFI", "ZEFF": "ZEFF", "IQ": "IOTA" }

                # Specify raw data for kinetic profiles, to be fitted in main routine
                cstemp = stddict["CS_DIAG"] if "CS_DIAG" in stddict and stddict["CS_DIAG"] is not None else "RHOTORN"
                csd = itemgetter(0)(ptools.define_coordinate_system(cstemp))
                if "NERAW" in stddict and stddict["NERAW"].size > 0:
                    newname = varconv["NE"]
                    RC.addRawData(newname,stddict["NERAWX"],stddict["NERAW"],stddict["NEDIAG"],stddict["NEDMAP"],stddict["NERAWXEB"],stddict["NERAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DNERAW" in stddict and stddict["DNERAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DNERAWX"],stddict["DNERAW"],stddict["DNERAWXEB"],stddict["DNERAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                if "TERAW" in stddict and stddict["TERAW"].size > 0:
                    newname = varconv["TE"]
                    RC.addRawData(newname,stddict["TERAWX"],stddict["TERAW"],stddict["TEDIAG"],stddict["TEDMAP"],stddict["TERAWXEB"],stddict["TERAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DTERAW" in stddict and stddict["DTERAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DTERAWX"],stddict["DTERAW"],stddict["DTERAWXEB"],stddict["DTERAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                if "TIRAW" in stddict and stddict["TIRAW"].size > 0:
                    newname = varconv["TI"]
                    RC.addRawData(newname,stddict["TIRAWX"],stddict["TIRAW"],stddict["TIDIAG"],stddict["TIDMAP"],stddict["TIRAWXEB"],stddict["TIRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DTIRAW" in stddict and stddict["DTIRAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DTIRAWX"],stddict["DTIRAW"],stddict["DTIRAWXEB"],stddict["DTIRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                if "AFRAW" in stddict and stddict["AFRAW"].size > 0:
                    newname = varconv["AF"]
                    RC.addRawData(newname,stddict["AFRAWX"],stddict["AFRAW"],stddict["AFDIAG"],stddict["AFDMAP"],stddict["AFRAWXEB"],stddict["AFRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DAFRAW" in stddict and stddict["DAFRAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DAFRAWX"],stddict["DAFRAW"],stddict["DAFRAWXEB"],stddict["DAFRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                for ii in np.arange(0,stddict["NUMIMP"]):
                    itag = "%d" % (ii+1)
                    if "NIMP"+itag+"RAW" in stddict and stddict["NIMP"+itag+"RAW"].size > 0:
                        newname = varconv["NIMP"]
                        MC.addImpurityMetaData(stddict["ZIMP"+itag+"RAW"],stddict["AIMP"+itag+"RAW"])
                        ltag = "%d" % (MC["NUMIMP"])
                        RC.addRawData(newname+ltag,stddict["NIMP"+itag+"RAWX"],stddict["NIMP"+itag+"RAW"],stddict["NIMP"+itag+"DIAG"],stddict["NIMP"+itag+"DMAP"], \
                                      stddict["NIMP"+itag+"RAWXEB"],stddict["NIMP"+itag+"RAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                        if "DNIMP"+itag+"RAW" in stddict and stddict["DNIMP"+itag+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+ltag,stddict["DNIMP"+itag+"RAWX"],stddict["DNIMP"+itag+"RAW"],stddict["DNIMP"+itag+"RAWXEB"],stddict["DNIMP"+itag+"RAWEB"], \
                                                       raw_x_coord=csd,raw_x_coord_prefix=prefix)
                        if zz1 is not None and np.abs(stddict["ZIMP"+itag+"RAW"] - zz1) < 0.5:
                            zz1 = zz2
                            zlim1 = None
                            zz2 = None
                if "TIMPRAW" in stddict and stddict["TIMPRAW"].size > 0:
                    newname = varconv["TIMP"]
                    RC.addRawData(newname,stddict["TIMPRAWX"],stddict["TIMPRAW"],stddict["TIMPDIAG"],stddict["TIMPDMAP"],stddict["TIMPRAWXEB"],stddict["TIMPRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DTIMPRAW" in stddict and stddict["DTIMPRAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DTIMPRAWX"],stddict["DTIMPRAW"],stddict["DTIMPRAWXEB"],stddict["DTIMPRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                if "QRAW" in stddict and stddict["QRAW"].size > 0:
                    newname = varconv["Q"]
                    RC.addRawData(newname,stddict["QRAWX"],stddict["QRAW"],stddict["QDIAG"],stddict["QDMAP"],stddict["QRAWXEB"],stddict["QRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DQRAW" in stddict and stddict["DQRAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DQRAWX"],stddict["DQRAW"],stddict["DQRAWXEB"],stddict["DQRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                if "IQRAW" in stddict and stddict["IQRAW"].size > 0:
                    newname = varconv["IQ"]
                    RC.addRawData(newname,stddict["IQRAWX"],stddict["IQRAW"],stddict["IQDIAG"],stddict["IQDMAP"],stddict["IQRAWXEB"],stddict["IQRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)
                    if "DIQRAW" in stddict and stddict["DIQRAW"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["DIQRAWX"],stddict["DIQRAW"],stddict["DIQRAWXEB"],stddict["DIQRAWEB"],raw_x_coord=csd,raw_x_coord_prefix=prefix)

                # Pass pre-fitted profiles, typically from a more trusted fitting routine (IDA in this case)
                if "NETRUSTED" in stddict and stddict["NETRUSTED"].size > 0:
                    newname = varconv["NE"] + "_"
                    RC.addRawData(newname,stddict["NETRUSTEDRHOTN"],stddict["NETRUSTED"],stddict["NETRUSTEDDIAG"],stddict["NETRUSTEDDMAP"],stddict["NETRUSTEDRHOTNEB"],stddict["NETRUSTEDEB"],raw_x_coord="RHOTORN",raw_x_coord_prefix=prefix)
                    if "NETRUSTEDGRAD" in stddict and stddict["NETRUSTEDGRAD"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["NETRUSTEDRHOTN"],stddict["NETRUSTEDGRAD"],stddict["NETRUSTEDRHOTNEB"],stddict["NETRUSTEDGRADEB"],raw_x_coord="RHOTORN",raw_x_coord_prefix=prefix)
                if "TETRUSTED" in stddict and stddict["TETRUSTED"].size > 0:
                    newname = varconv["TE"] + "_"
                    RC.addRawData(newname,stddict["TETRUSTEDRHOTN"],stddict["TETRUSTED"],stddict["TETRUSTEDDIAG"],stddict["TETRUSTEDDMAP"],stddict["TETRUSTEDRHOTNEB"],stddict["TETRUSTEDEB"],raw_x_coord="RHOTORN",raw_x_coord_prefix=prefix)
                    if "TETRUSTEDGRAD" in stddict and stddict["TETRUSTEDGRAD"].size > 0:
                        RC.addDerivativeConstraint(newname,stddict["TETRUSTEDRHOTN"],stddict["TETRUSTEDGRAD"],stddict["TETRUSTEDRHOTNEB"],stddict["TETRUSTEDGRADEB"],raw_x_coord="RHOTORN",raw_x_coord_prefix=prefix)

                # Specify assumed impurities, defined using assumptions and known limitations
                if zz1 is not None:
                    MC.addFillerImpurityMetaData(zz1,weight=1.0)
                else:
                    MC.addFillerImpurityMetaData(54.0,weight=1.0)    # Use Xe if all suspected candidates are measured, chosen for low fuel dilution
                if zlim1 is not None and zz2 is not None:
                    MC.addFillerImpurityLimitMetaData(zlim1,index=1)
                    MC.addFillerImpurityMetaData(zz2,weight=1.0)

                if fdebug:
                    print("aug - adapter.py: %s kinetic profiles populated." % (type(RC).__name__))

                # Specify raw data for source profiles, to be fitted in main routine
                cstemp = stddict["CS_SRC"] if "CS_SRC" in stddict and stddict["CS_SRC"] is not None else "RHOTORN"
                css = itemgetter(0)(ptools.define_coordinate_system(cstemp))
                for src in ["NBI","ICRH","ECRH","LH","OHM"]:
                    if "S"+src+"RAW" in stddict and stddict["S"+src+"RAW"].size > 0:
                        newname = varconv["S"]
                        xerr = stddict["S"+src+"RAWXEB"] if "S"+src+"RAWXEB" in stddict and stddict["S"+src+"RAWXEB"] is not None else np.zeros(stddict["S"+src+"RAWX"].shape)
                        yerr = stddict["S"+src+"RAWEB"] if "S"+src+"RAWEB" in stddict and stddict["S"+src+"RAWEB"] is not None else np.zeros(stddict["S"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["S"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["S"+src+"RAWX"],stddict["S"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DS"+src+"RAW" in stddict and stddict["DS"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DS"+src+"RAWX"],stddict["DS"+src+"RAW"],stddict["DS"+src+"RAWXEB"],stddict["DS"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "QE"+src+"RAW" in stddict and stddict["QE"+src+"RAW"].size > 0:
                        newname = varconv["QE"]
                        xerr = stddict["QE"+src+"RAWXEB"] if "QE"+src+"RAWXEB" in stddict and stddict["QE"+src+"RAWXEB"] is not None else np.zeros(stddict["QE"+src+"RAWX"].shape)
                        yerr = stddict["QE"+src+"RAWEB"] if "QE"+src+"RAWEB" in stddict and stddict["QE"+src+"RAWEB"] is not None else np.zeros(stddict["QE"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["QE"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["QE"+src+"RAWX"],stddict["QE"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DQE"+src+"RAW" in stddict and stddict["DQE"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DQE"+src+"RAWX"],stddict["DQE"+src+"RAW"],stddict["DQE"+src+"RAWXEB"],stddict["DQE"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "QI"+src+"RAW" in stddict and stddict["QE"+src+"RAW"].size > 0:
                        newname = varconv["QI"]
                        xerr = stddict["QI"+src+"RAWXEB"] if "QI"+src+"RAWXEB" in stddict and stddict["QI"+src+"RAWXEB"] is not None else np.zeros(stddict["QI"+src+"RAWX"].shape)
                        yerr = stddict["QI"+src+"RAWEB"] if "QI"+src+"RAWEB" in stddict and stddict["QI"+src+"RAWEB"] is not None else np.zeros(stddict["QI"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["QI"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["QI"+src+"RAWX"],stddict["QI"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DQI"+src+"RAW" in stddict and stddict["DQI"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DQI"+src+"RAWX"],stddict["DQI"+src+"RAW"],stddict["DQI"+src+"RAWXEB"],stddict["DQI"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "TAU"+src+"RAW" in stddict and stddict["TAU"+src+"RAW"].size > 0:
                        newname = varconv["TAU"]
                        xerr = stddict["TAU"+src+"RAWXEB"] if "TAU"+src+"RAWXEB" in stddict and stddict["TAU"+src+"RAWXEB"] is not None else np.zeros(stddict["TAU"+src+"RAWX"].shape)
                        yerr = stddict["TAU"+src+"RAWEB"] if "TAU"+src+"RAWEB" in stddict and stddict["TAU"+src+"RAWEB"] is not None else np.zeros(stddict["TAU"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["TAU"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["TAU"+src+"RAWX"],stddict["TAU"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DTAU"+src+"RAW" in stddict and stddict["DTAU"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DTAU"+src+"RAWX"],stddict["DTAU"+src+"RAW"],stddict["DTAU"+src+"RAWXEB"],stddict["DTAU"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "J"+src+"RAW" in stddict and stddict["J"+src+"RAW"].size > 0:
                        newname = varconv["J"]
                        xerr = stddict["J"+src+"RAWXEB"] if "J"+src+"RAWXEB" in stddict and stddict["J"+src+"RAWXEB"] is not None else np.zeros(stddict["J"+src+"RAWX"].shape)
                        yerr = stddict["J"+src+"RAWEB"] if "J"+src+"RAWEB" in stddict and stddict["J"+src+"RAWEB"] is not None else np.zeros(stddict["J"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["J"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["J"+src+"RAWX"],stddict["J"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DJ"+src+"RAW" in stddict and stddict["DJ"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DJ"+src+"RAWX"],stddict["DJ"+src+"RAW"],stddict["DJ"+src+"RAWXEB"],stddict["DJ"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "NFI"+src+"RAW" in stddict and stddict["NFI"+src+"RAW"].size > 0:
                        newname = varconv["NFI"]
                        xerr = stddict["NFI"+src+"RAWXEB"] if "NFI"+src+"RAWXEB" in stddict and stddict["NFI"+src+"RAWXEB"] is not None else np.zeros(stddict["NFI"+src+"RAWX"].shape)
                        yerr = stddict["NFI"+src+"RAWEB"] if "NFI"+src+"RAWEB" in stddict and stddict["NFI"+src+"RAWEB"] is not None else np.zeros(stddict["NFI"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["NFI"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["NFI"+src+"RAWX"],stddict["NFI"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DNFI"+src+"RAW" in stddict and stddict["DNFI"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DNFI"+src+"RAWX"],stddict["DNFI"+src+"RAW"],stddict["DNFI"+src+"RAWXEB"],stddict["DNFI"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "WFI"+src+"RAW" in stddict and stddict["WFI"+src+"RAW"].size > 0:
                        newname = varconv["WFI"]
                        xerr = stddict["WFI"+src+"RAWXEB"] if "WFI"+src+"RAWXEB" in stddict and stddict["WFI"+src+"RAWXEB"] is not None else np.zeros(stddict["WFI"+src+"RAWX"].shape)
                        yerr = stddict["WFI"+src+"RAWEB"] if "WFI"+src+"RAWEB" in stddict and stddict["WFI"+src+"RAWEB"] is not None else np.zeros(stddict["WFI"+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        dmap = [0] * stddict["WFI"+src+"RAWX"].size
                        RC.addRawData(newname+src,stddict["WFI"+src+"RAWX"],stddict["WFI"+src+"RAW"],diag,dmap,xerr,yerr,raw_x_coord=css,raw_x_coord_prefix=prefix)
                        if "DWFI"+src+"RAW" in stddict and stddict["DWFI"+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src,stddict["DWFI"+src+"RAWX"],stddict["DWFI"+src+"RAW"],stddict["DWFI"+src+"RAWXEB"],stddict["DWFI"+src+"RAWEB"], \
                                                       raw_x_coord=css,raw_x_coord_prefix=prefix)
                    if "AFI1"+src in stddict and stddict["AFI1"+src] is not None and "ZFI1"+src in stddict and stddict["ZFI1"+src] is not None:
                        MC.addFastIonMetaData(stddict["ZFI1"+src],stddict["AFI1"+src],weight=0.01)
                        ntag = "%d" % MC["NUMFI"]
                        MC.addData("SRCFI"+ntag,src)
                    if "AFI2"+src in stddict and stddict["AFI2"+src] is not None and "ZFI2"+src in stddict and stddict["ZFI2"+src] is not None:
                        MC.addFastIonMetaData(stddict["ZFI2"+src],stddict["AFI2"+src],weight=0.01)
                        ntag = "%d" % MC["NUMFI"]
                        MC.addData("SRCFI"+ntag,src)

                if fdebug:
                    print("aug - adapter.py: %s source profiles populated." % (type(RC).__name__))

                for key in stddict:
                    newname = None
                    newtag = None
                    mm = re.match(r'^([A-Z]+)([0-9]*)GPSET$',key)
                    if mm and mm.group(1).upper() in varconv:
                        newname = varconv[mm.group(1).upper()]
                        newtag = mm.group(2).upper()
                    elif mm:
                        for src in ["NBI","ICRH","ECRH","LH","OHM"]:
                            mmm = re.match(r'^([A-Z]+)'+src+r'$',mm.group(1).upper())
                            if mmm and mmm.group(1).upper() in varconv:
                                newname = varconv[mmm.group(1).upper()]
                                newtag = src+mm.group(2).upper()
                    if isinstance(stddict[key],list) and newname is not None:
                        FL = classes.EX2GKFitSettingsOrderedList()
                        for ii in np.arange(0,len(stddict[key])):
                            if "FTYPE" in stddict[key][ii] and isinstance(stddict[key][ii]["FTYPE"],str):
                                if stddict[key][ii]["FTYPE"].upper() == "GPR":
#                                    stddict[key][ii]["FIT_ONAME"] = 5
#                                    stddict[key][ii]["ERR_ONAME"] = 5
                                    FS = classes.EX2GKGPFitSettings()
                                    FS.defineFitKernelSettings(stddict[key][ii]["FIT_KNAME"],stddict[key][ii]["FIT_KPARS"],stddict[key][ii]["FIT_REGPAR"], \
                                                               stddict[key][ii]["FIT_ONAME"],stddict[key][ii]["FIT_OPARS"],stddict[key][ii]["FIT_EPS"], \
                                                               stddict[key][ii]["FIT_NRES"],stddict[key][ii]["FIT_KBNDS"])
                                    FS.defineErrorKernelSettings(stddict[key][ii]["ERR_KNAME"],stddict[key][ii]["ERR_KPARS"],stddict[key][ii]["ERR_REGPAR"], \
                                                                 stddict[key][ii]["ERR_ONAME"],stddict[key][ii]["ERR_OPARS"],stddict[key][ii]["ERR_EPS"], \
                                                                 stddict[key][ii]["ERR_NRES"],stddict[key][ii]["ERR_KBNDS"])
                                    FL.append(FS)
                                elif stddict[key][ii]["FTYPE"].upper() == "POLY":
                                    FS = classes.EX2GKPolyFitSettings()
                                    FS.definePolyFitSettings(stddict[key][ii]["DEGREE"],stddict[key][ii]["XANCHORS"],stddict[key][ii]["YANCHORS"],stddict[key][ii]["OMITCST"],stddict[key][ii]["OMITLIN"])
                                    FL.append(FS)
                                elif stddict[key][ii]["FTYPE"].upper() == "LININT":
                                    pass
                        KC.addFitSettingsListObject(newname+newtag,FL)

                if fdebug:
                    print("aug - adapter.py: %s populated." % (type(KC).__name__))

                # Transfer processing flags as metadata
                FC.setFlag("GPCOORD","GPCOORDFLAG" in stddict and stddict["GPCOORDFLAG"])
                FC.setFlag("RSHIFT","RSHIFTFLAG" in stddict and stddict["RSHIFTFLAG"])
                FC.setFlag("IDA","IDAFLAG" in stddict and stddict["IDAFLAG"])

                if fdebug:
                    print("aug - adapter.py: %s populated." % (type(FC).__name__))

                outobj = classes.EX2GKTimeWindow(meta=MC,cd=CC,zd=ZC,rd=RC,fsi=KC,flag=FC)

                if fdebug:
                    print("aug - adapter.py: %s populated." % (type(outobj).__name__))
                    print("aug - adapter.py: get_data() completed!")

            else:
                outobj = copy.deepcopy(datadict)

            outlist.append(outobj)

    return outlist


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def get_time_traces(namelist):
    """
    AUG-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides capability to extract time traces
    needed for time window selection.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: dict. Python dictionary object with standardized time trace data.
    """
    outdata  = None
    argdict = dict()
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

    shot = None
    if "SHOTINFO" in argdict and isinstance(argdict["SHOTINFO"],str):
        expstr = argdict["SHOTINFO"].strip()
        expinfo = expstr.split()
        if len(expinfo) > 0:
            shot = int(float(expinfo[0]))

    if tmod is not None and shot is not None:
        outdata = tmod.get_time_data(shot)

    return outdata
