# Script with functions to estimate good time windows from JET traces via MDSplus
# Developer: Aaron Ho - 19/01/2018
#    Additional help for MDSplus can be found at http://www.mdsplus.org/

# Required imports
import os
import sys
import numpy as np
import re
import copy
import datetime
import pickle
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# Required non-standard Python packages
from EX2GK.tools.general import proctools as ptools
from EX2GK.machine_adapters.aug import extract_aug_dd3 as ext

np_types = (np.int8,np.int16,np.int32,np.int64,np.uint8,np.uint16,np.uint32,np.uint64,np.float16,np.float32,np.float64)


def get_time_data(shot):
    """
    JET-SPECIFIC FUNCTION

    Gathers the necessary data for determining time windows,
    which can be inspected manually or placed into an
    automated routine.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :returns: dict. Implementation-specific object containing all time trace data extracted by this function.
    """
    snum = None
    ti = None
    tf = None
    if isinstance(shot,(int,float)) and int(shot) > 0:
        snum = int(shot)
    else:
        raise TypeError("Shot number argument must be a positive integer.")

    # Uses EFIT/XIP data to determine if PPF exists, use its closest (ti,tf) as actual (ti,tf)
    sigd = ext.getsig(snum,ti,tf,'FPC','IpiFP','TIMEF',None,favg=False)
    ip = sigd[0]["DATA"] if sigd is not None else None
    tip = sigd[0]["TVEC"] if sigd is not None else None

    RD = None
    if ip is not None and tip is not None:

        RD = dict()
        RD["DEVICE"] = "AUG"
        RD["INTERFACE"] = "DD3"
        RD["SHOT"] = snum
        RD["IP"] = ip
        RD["TIP"] = tip

        # Extraction of required data
        sigd = ext.getsig(snum,ti,tf,'TOT','P_OH','time',None,favg=False)
        RD["OHM"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TOHM"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'NIS','PNI','T-B',None,favg=False)
        RD["NBI"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TNBI"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'ICP','PICRN','T-B',None,favg=False)
        RD["ICRH"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TICRH"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'ECS','PECRH','T-B',None,favg=False)
        RD["ECRH"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TECRH"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'TOT','Wmhd','time',favg=False)
        RD["WMHD"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TWMHD"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'TOT','Wth','time',favg=False)
        RD["WTH"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TWTH"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'ZES','Zeff','TimeBase',None,favg=False)
        RD["ZEFS"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TZEFS"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'TOT','Zeff','time',None,favg=False)
        RD["ZEFT"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TZEFT"] = sigd[0]["TVEC"] if sigd is not None else None
        sigd = ext.getsig(snum,ti,tf,'ENR','NRATE','Timebase',None,favg=False)
        RD["NEUT"] = sigd[0]["DATA"] if sigd is not None else None
        RD["TNEUT"] = sigd[0]["TVEC"] if sigd is not None else None

    return RD


def generate_time_windows(shot,nslices=None,do_plot=False):
    """
    AUG-SPECIFIC FUNCTION
    Automatically select interesting time windows in ramp-up, flat-top, and 
    ramp-down phases. Time windows of interest defined as having the desired
    behaviour of the plasma current as well as not spanning an abrupt change in
    the heating power scheme. If possible, the algorithm prioritizes windows
    containing non-ohmic heating sources before those with purely ohmic heating.
    
    Only recommended as an initial guess as this implementation still picks many
    erroneous / useless windows!!!

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :kwarg nslices: array. Number of time windows to be selected in order of ramp-up phase, flat-top phase, ramp-down phase.

    :kwarg do_plot: bool. Flag to toggle automatic plotting of extracted data and selected time windows.

    :returns: dict. Implementation-specific object containing all raw data extracted and computed by this function.
    """
    snum = None
    ns = np.array([2,3,2])             # Ramp-up, flat-top, ramp-down
    tw = np.array([0.1,0.5,0.1])
    pdir = ''
    if isinstance(shot,(int,float)) and int(shot) > 0:
        snum = int(shot)
    else:
        raise TypeError("Shot number argument must be a positive integer.")
    if isinstance(nslices,(list,tuple)):
        if len(nslices) > 0 and isinstance(nslices[0],(int,float)) and int(nslices[0]) >= 0:
            ns[0] = int(nslices[0])
        if len(nslices) > 1 and isinstance(nslices[1],(int,float)) and int(nslices[1]) >= 0:
            ns[1] = int(nslices[1])
        if len(nslices) > 2 and isinstance(nslices[2],(int,float)) and int(nslices[2]) >= 0:
            ns[2] = int(nslices[2])
    elif isinstance(nslices,np.ndarray):
        if nslices.size > 0 and isinstance(nslices[0],(int,float)) and int(nslices[0]) >= 0:
            ns[0] = int(nslices[0])
        if nslices.size > 1 and isinstance(nslices[1],(int,float)) and int(nslices[1]) >= 0:
            ns[1] = int(nslices[1])
        if nslices.size > 2 and isinstance(nslices[2],(int,float)) and int(nslices[2]) >= 0:
            ns[2] = int(nslices[2])
    elif isinstance(nslices,(int,float)) and int(nslices) > 0:
        ns = np.array([0,int(nslices),0])
    if pickle_dir and isinstance(pickle_dir,str):
        pdir = pickle_dir
        if not pdir.endswith('/'):
            pdir = pdir + '/'

    RD = get_time_data(snum)          # Output container (dict)
    if RD is not None:

        RD["TW"] = []                 # Time windows stored here

        # Attempts to discern the boundaries of the flat-top phase, defining the ramp-up phase as all
        # times before it and ramp-down phase as all times after it
        tbnd = RD["TIP"][0] + 5.0
        navg = 5
        mavg = ptools.moving_average(RD["IP"],navg)
        tavg = ptools.moving_average(RD["TIP"],navg)
        tcheck = np.where(RD["TIP"] >= tbnd)[0]
        ifta = 0
        if tcheck.size > 0:
            ifta = tcheck[0]
        else:
            tbnd = RD["TIP"][0] + 0.5
            ifta = np.where(RD["TIP"] >= tbnd)[0][0]
        iftb = RD["TIP"].size - 1
        jj = 0
        while ifta < (iftb - navg) and np.abs(RD["IP"][ifta] / RD["IP"][iftb] - 1.0) > 1.0e-2 and jj < 3:
            if np.abs(RD["IP"][ifta + navg] / RD["IP"][ifta] - 1.0) > 1.0e-2:
                ifta = ifta + 1
                jj = 0
            else:
                jj = jj + 1
            if np.abs(RD["IP"][iftb - navg] / RD["IP"][iftb] - 1.0) > 1.0e-2:
                iftb = iftb - 1
                jj = 0
            else:
                jj = jj + 1
        tfton = RD["TIP"][ifta]    # Flat-top start time
        tftoff = RD["TIP"][iftb]   # Flat-top end time

        # Future expansion to consider time derivative of plasma current as a condition
        tfilt = np.all([tavg >= RD["TIP"][0],tavg <= RD["TIP"][-1]],axis=0)
        RD["DIPDT"] = np.diff(np.abs(mavg[tfilt])) / np.diff(tavg[tfilt])
        RD["TDIPDT"] = tavg[tfilt][:-1] + np.diff(tavg[tfilt]) / 2.0
        itvec = np.linspace(RD["TIP"][0],RD["TIP"][-1],1000)
        dipfunc = interp1d(RD["TDIPDT"],RD["DIPDT"],bounds_error=False,fill_value='extrapolate')
        lindipdt = dipfunc(itvec)

        # Attempt to identify the times when the ECRH system is switched on and off, as well as abruptly changed
        tecon = None                # ECRH power start time
        tecoff = None               # ECRH power end time
        ecidx = np.where(RD["ECRH"] > 1.0e4)[0] if RD["ECRH"] is not None else np.array([])
        if ecidx.size > 0:
            bidx = np.where(RD["TECRH"] >= RD["TECRH"][ecidx[0]] + 0.1)[0][0]
            tecon = RD["TECRH"][bidx]
            eidx = np.where(RD["TECRH"] >= RD["TECRH"][ecidx[-1]] - 0.1)[0][0]
            tecoff = RD["TECRH"][eidx]
        tecswitch = np.array([]) if tecon is None else np.hstack((tecon,tecoff))
        if tecswitch.size > 0:
            navg = 10
            secrh = np.diff(ptools.moving_average(np.cumsum(RD["ECRH"] * 1.0e-6),navg))[int((navg + 1) / 2):-int(navg / 2)]
            tsecrh = ptools.moving_average(RD["TECRH"],navg)[int((navg + 1) / 2):-int(navg / 2)]
            tsecrh = (tsecrh[:-1] + tsecrh[1:]) / 2.0
            ecrhfunc = interp1d(tsecrh,secrh,bounds_error=False,fill_value='extrapolate')
            RD["SECRH"] = ecrhfunc(itvec)

        # Attempt to identify the times when the ICRH system is switched on and off, as well as abruptly changed
        ticon = None                # ICRH power start time
        ticoff = None               # ICRH power end time
        icidx = np.where(RD["ICRH"] > 1.0e3)[0] if RD["ICRH"] is not None else np.array([])
        if icidx.size > 0:
            bidx = np.where(RD["TICRH"] >= RD["TICRH"][icidx[0]] + 0.1)[0][0]
            ticon = RD["TICRH"][bidx]
            eidx = np.where(RD["TICRH"] >= RD["TICRH"][icidx[-1]] - 0.1)[0][0]
            ticoff = RD["TICRH"][eidx]
        ticswitch = np.array([]) if ticon is None else np.array([ticon])
        if ticswitch.size > 0:
            navg = 10
            sicrh = np.diff(ptools.moving_average(np.cumsum(RD["ICRH"] * 1.0e-6),navg))[int((navg + 1) / 2):-int(navg / 2)]
            tsicrh = ptools.moving_average(RD["TICRH"],navg)[int((navg + 1) / 2):-int(navg / 2)]
            tsicrh = (tsicrh[:-1] + tsicrh[1:]) / 2.0
            icrhfunc = interp1d(tsicrh,sicrh,bounds_error=False,fill_value='extrapolate')
            RD["SICRH"] = icrhfunc(itvec)
            ics = np.diff(ptools.moving_average(np.cumsum(RD["ICRH"] * 1.0e-6),navg))[:-navg+1]
            bidx = np.where(RD["TICRH"] >= RD["TICRH"][icidx[0]] + 0.1)[0][0]
            eidx = np.where(RD["TICRH"] >= RD["TICRH"][icidx[-1]] - 0.1)[0][0]
            opow = ics[bidx]
            npow = -1.0
            jj = 0
            kk = 0
            for ii in np.arange(bidx,eidx+1):
                if jj > navg and (ics[ii] - opow) >= 0.5:
                    jj = 0
                    npow = ics[ii]
                    reld = 0.5 / (ics[ii] - opow)
                    kk = ii - int(reld * navg / 2)
                if jj == navg and (ics[ii] - npow) < 0.1:
                    ticswitch = np.hstack((ticswitch,RD["TICRH"][kk]))
                    opow = ics[ii]
                    npow = -1.0
                jj = jj + 1
            ticswitch = np.hstack((ticswitch,ticoff))

        # Attempt to identify the times when the NBI system is switched on and off, as well as abruptly changed
        tnbon = None                # NBI power start time
        tnboff = None               # NBI power end time
        nbidx = np.where(RD["NBI"] > 1.0e5)[0] if RD["NBI"] is not None else np.array([])
        if nbidx.size > 0:
            bidx = np.where(RD["TNBI"] >= RD["TNBI"][nbidx[0]] + 0.1)[0][0]
            tnbon = RD["TNBI"][bidx]
            eidx = np.where(RD["TNBI"] >= RD["TNBI"][nbidx[-1]] - 0.1)[0][0]
            tnboff = RD["TNBI"][eidx]
        tnbswitch = np.array([]) if tnbon is None else np.array([tnbon])
        if tnbswitch.size > 0:
            navg = 10
            snbi = np.diff(ptools.moving_average(np.cumsum(RD["NBI"] * 1.0e-6),navg))[int((navg + 1) / 2):-int(navg / 2)]
            tsnbi = ptools.moving_average(RD["TNBI"],navg)[int((navg + 1) / 2):-int(navg / 2)]
            tsnbi = (tsnbi[:-1] + tsnbi[1:]) / 2.0
            nbifunc = interp1d(tsnbi,snbi,bounds_error=False,fill_value='extrapolate')
            RD["SNBI"] = nbifunc(itvec)
            nbs = np.diff(ptools.moving_average(np.cumsum(RD["NBI"] * 1.0e-6),navg))[:-navg+1]
            bidx = np.where(RD["TNBI"] >= RD["TNBI"][nbidx[0]] + 0.1)[0][0]
            eidx = np.where(RD["TNBI"] >= RD["TNBI"][nbidx[-1]] - 0.1)[0][0]
            opow = nbs[bidx]
            npow = -1.0
            jj = 0
            kk = 0
            for ii in np.arange(bidx,eidx+1):
                if jj > navg and (nbs[ii] - opow) >= 0.5:
                    jj = 0
                    npow = nbs[ii]
                    reld = 0.5 / (nbs[ii] - opow)
                    kk = ii - int(reld * navg / 2)
                if jj == navg and (nbs[ii] - npow) < 0.1:
                    tnbswitch = np.hstack((tnbswitch,RD["TNBI"][kk]))
                    opow = nbs[ii]
                    npow = -1.0
                jj = jj + 1
            tnbswitch = np.hstack((tnbswitch,tnboff))

        # Time window selection for ramp-up phase, accounting for plasma development (via Z-effective),
        # heating system behaviour and minimizing time window overlap / bunching
        trampup = RD["TIP"][0] + 1.0
        rutvec = np.hstack((tecswitch + 0.51,ticswitch + 0.21,tnbswitch + 0.11))
        dtmin = (tfton - trampup) / float(ns[0] + 2)
        lasttime = -1.0e5
        olasttime = lasttime
        nadded = 0
        # First pass looks slightly ahead of all identified heating source switching times, most likely to be interesting
        for ii in np.arange(0,rutvec.size):
            if rutvec[ii] <= (tfton - 0.2) and (rutvec[ii] - lasttime) > dtmin and nadded < ns[0]:
                dipfilt = np.all([itvec >= rutvec[ii],itvec <= (rutvec[ii] + 0.2)],axis=0)
                adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                ipflag = True if adip is not None and adip > 5.0e4 else False
                azeff = None
                if RD["ZEFS"] is not None:
                    zefsfilt = np.all([RD["TZEFS"] >= rutvec[ii],RD["TZEFS"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFS"][zefsfilt]) if np.any(zefsfilt) else None
                elif RD["ZEFT"] is not None:
                    zeftfilt = np.all([RD["TZEFT"] >= rutvec[ii],RD["TZEFT"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFT"][zeftfilt]) if np.any(zeftfilt) else None
                zeffflag = True if azeff is not None and azeff < 4.0 else False
                ecflag = True
                for tt in tecswitch:
                    if ecflag:
                        ecflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii] + 0.21)],axis=0) else True
                icflag = True
                for tt in ticswitch:
                    if icflag:
                        icflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii] + 0.21)],axis=0) else True
                nbflag = True
                for tt in tnbswitch:
                    if nbflag:
                        nbflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii] + 0.21)],axis=0) else True
                if ipflag and zeffflag and ecflag and icflag and nbflag:
                    tfilt = np.all([itvec >= rutvec[ii],itvec <= (rutvec[ii] + 0.2)],axis=0)
                    ecpower = 0.0
                    if "SECRH" in RD:
                        ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                        if ecpower < 0.0:
                            ecpower = 0.0
                    icpower = 0.0
                    if "SICRH" in RD:
                        icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                        if icpower < 0.0:
                            icpower = 0.0
                    nbpower = 0.0
                    if "SNBI" in RD:
                        nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                        if nbpower < 0.0:
                            nbpower = 0.0
                    timewindow = (rutvec[ii],rutvec[ii] + 0.2,0,ecpower,icpower,nbpower)
                    RD["TW"].append(timewindow)
                    lasttime = rutvec[ii] + 0.2
                    nadded = nadded + 1
        rlasttime = lasttime
        # Second pass looks specifically at the entire period which ECRH is on, if present in ramp-up phase
        if tecswitch.size > 0:
            tmin = np.nanmin(tecswitch) + 0.51
            tmax = np.nanmax(tecswitch)
            rutvec = np.arange(tmin,tmax,0.1)
            for ii in np.arange(0,rutvec.size - 2):
                if rutvec[ii] <= (tfton - 0.2) and (rutvec[ii] - lasttime) > dtmin and nadded < ns[0]:
                    dipfilt = np.all([itvec >= rutvec[ii],itvec <= rutvec[ii+2]],axis=0)
                    adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                    ipflag = True if adip is not None and adip > 5.0e4 else False
                    azeff = None
                    if RD["ZEFS"] is not None:
                        zefsfilt = np.all([RD["TZEFS"] >= rutvec[ii],RD["TZEFS"] <= (rutvec[ii] + 0.2)],axis=0)
                        azeff = np.mean(RD["ZEFS"][zefsfilt]) if np.any(zefsfilt) else None
                    elif RD["ZEFT"] is not None:
                        zeftfilt = np.all([RD["TZEFT"] >= rutvec[ii],RD["TZEFT"] <= (rutvec[ii] + 0.2)],axis=0)
                        azeff = np.mean(RD["ZEFT"][zeftfilt]) if np.any(zeftfilt) else None
                    zeffflag = True if azeff is not None and azeff < 4.0 else False
                    ecflag = True
                    for tt in tecswitch:
                        if ecflag:
                            ecflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii+2] + 0.01)],axis=0) else True
                    icflag = True
                    for tt in ticswitch:
                        if icflag:
                            icflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii+2] + 0.01)],axis=0) else True
                    nbflag = True
                    for tt in tnbswitch:
                        if nbflag:
                            nbflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii+2] + 0.01)],axis=0) else True
                    twflag = True
                    for tw in RD["TW"]:
                        if twflag:
                            twaflag = False if np.all([tw[0] >= rutvec[ii],tw[0] <= (rutvec[ii+2] + 0.01 + dtmin)],axis=0) else True
                            twbflag = False if np.all([tw[1] >= (rutvec[ii] - 0.01 - dtmin),tw[1] <= rutvec[ii+2]],axis=0) else True
                            twflag = False if not twaflag or not twbflag else True
                    if ipflag and zeffflag and ecflag and icflag and nbflag and twflag:
                        tfilt = np.all([itvec >= rutvec[ii],itvec <= rutvec[ii+2]],axis=0)
                        ecpower = 0.0
                        if "SECRH" in RD:
                            ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                            if ecpower < 0.0:
                                ecpower = 0.0
                        icpower = 0.0
                        if "SICRH" in RD:
                            icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                            if icpower < 0.0:
                                icpower = 0.0
                        nbpower = 0.0
                        if "SNBI" in RD:
                            nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                            if nbpower < 0.0:
                                nbpower = 0.0
                        timewindow = (rutvec[ii],rutvec[ii+2],0,ecpower,icpower,nbpower)
                        RD["TW"].append(timewindow)
                        lasttime = rutvec[ii+2]
                        nadded = nadded + 1
            rlasttime = np.nanmax([rlasttime,lasttime],axis=0)
        lasttime = olasttime
        # Third pass scans the whole phase, allowing selection of purely ohmic time windows (last pass)
        rutvec = np.arange(trampup,tfton - 0.1,0.1)
        for ii in np.arange(0,rutvec.size - 2):
            if (rutvec[ii] - lasttime) > dtmin and nadded < ns[0]:
                dipfilt = np.all([itvec >= rutvec[ii],itvec <= rutvec[ii+2]],axis=0)
                adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                ipflag = True if adip is not None and adip > 5.0e4 else False
                azeff = None
                if RD["ZEFS"] is not None:
                    zefsfilt = np.all([RD["TZEFS"] >= rutvec[ii],RD["TZEFS"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFS"][zefsfilt]) if np.any(zefsfilt) else None
                elif RD["ZEFT"] is not None:
                    zeftfilt = np.all([RD["TZEFT"] >= rutvec[ii],RD["TZEFT"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFT"][zeftfilt]) if np.any(zeftfilt) else None
                zeffflag = True if azeff is not None and azeff < 4.0 else False
                ecrhflag = True
                for tt in tecswitch:
                    if ecflag:
                        ecflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii+2] + 0.01)],axis=0) else True
                icflag = True
                for tt in ticswitch:
                    if icflag:
                        icflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii+2] + 0.01)],axis=0) else True
                nbflag = True
                for tt in tnbswitch:
                    if nbflag:
                        nbflag = False if np.all([tt >= rutvec[ii],tt <= (rutvec[ii+2] + 0.01)],axis=0) else True
                twflag = True
                for tw in RD["TW"]:
                    if twflag:
                        twaflag = False if np.all([tw[0] >= rutvec[ii],tw[0] <= (rutvec[ii+2] + 0.01 + dtmin)],axis=0) else True
                        twbflag = False if np.all([tw[1] >= (rutvec[ii] - 0.01 - dtmin),tw[1] <= rutvec[ii+2]],axis=0) else True
                        twflag = False if not twaflag or not twbflag else True
                if ipflag and zeffflag and ecflag and icflag and nbflag and twflag:
                    tfilt = np.all([itvec >= rutvec[ii],itvec <= rutvec[ii+2]],axis=0)
                    ecpower = 0.0
                    if "SECRH" in RD:
                        ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                        if ecpower < 0.0:
                            ecpower = 0.0
                    icpower = 0.0
                    if "SICRH" in RD:
                        icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                        if icpower < 0.0:
                            icpower = 0.0
                    nbpower = 0.0
                    if "SNBI" in RD:
                        nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                        if nbpower < 0.0:
                            nbpower = 0.0
                    timewindow = (rutvec[ii],rutvec[ii+2],0,ecpower,icpower,nbpower)
                    RD["TW"].append(timewindow)
                    lasttime = rutvec[ii+2]
                    nadded = nadded + 1
        olasttime = np.nanmax([rlasttime,lasttime],axis=0)

        # Time window selection for flat-top phase, accounting for heating system behaviour and minimizing time
        # window overlap / bunching
        lasttime = RD["TIP"][0]
        fttvec = np.hstack((tfton + 2.01,tecswitch + 1.01,ticswitch + 1.01,tnbswitch + 1.01))
        dtmin = (tftoff - tfton) / (float(ns[1] + 4))
        nadded = 0
        # First pass looks slightly ahead of all identified heating source switching times, most likely to be in steady-state
        for ii in np.arange(0,fttvec.size):
            if fttvec[ii] > (tfton + 0.5) and fttvec[ii] < (tftoff - 0.6) and (fttvec[ii] - lasttime) > dtmin and nadded < ns[1]:
                dipfilt = np.all([itvec >= fttvec[ii],itvec <= (fttvec[ii] + 0.5)],axis=0)
                adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                ipflag = True if adip is not None and np.abs(adip) < 1.0e4 else False
                adwp = None
                if RD["WTH"] is not None:
                    wtfilt = np.all([RD["TWTH"] >= (fttvec[ii] - 0.5),RD["TWTH"] <= (fttvec[ii] + 1.0)],axis=0)
                    adwp = np.mean(np.diff(RD["WTH"][wtfilt])) if np.any(wtfilt) else None
                if RD["WMHD"] is not None:
                    wmfilt = np.all([RD["TWMHD"] >= (fttvec[ii] - 0.5),RD["TWMHD"] <= (fttvec[ii] + 1.0)],axis=0)
                    adwp = np.mean(np.diff(RD["WMHD"][wmfilt])) if np.any(wmfilt) else None
                ssflag = True if adwp is not None and np.abs(adwp) < 1.0e5 else False
                ecflag = True
                for tt in tecswitch:
                    if ecflag:
                        ecflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii] + 0.55)],axis=0) else True
                icflag = True
                for tt in ticswitch:
                    if icflag:
                        icflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii] + 0.55)],axis=0) else True
                nbflag = True
                for tt in tnbswitch:
                    if nbflag:
                        nbflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii] + 0.55)],axis=0) else True
                if ipflag and ssflag and ecflag and icflag and nbflag:
                    tfilt = np.all([itvec >= fttvec[ii],itvec <= (fttvec[ii] + 0.5)],axis=0)
                    ecpower = 0.0
                    if "SECRH" in RD:
                        ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                        if ecpower < 0.0:
                            ecpower = 0.0
                    icpower = 0.0
                    if "SICRH" in RD:
                        icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                        if icpower < 0.0:
                            icpower = 0.0
                    nbpower = 0.0
                    if "SNBI" in RD:
                        nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                        if nbpower < 0.0:
                            nbpower = 0.0
                    timewindow = (fttvec[ii],fttvec[ii] + 0.5,1,ecpower,icpower,nbpower)
                    RD["TW"].append(timewindow)
                    lasttime = fttvec[ii] + 0.5
                    nadded = nadded + 1
        rlasttime = lasttime
        # Second pass looks specifically at the entire period which ICRH and / or NBI is on, if present in flat-top phase
        if ticswitch.size > 0 or tnbswitch.size > 0:
            tmin = np.nanmin(np.hstack((ticswitch,tnbswitch)))
            tmax = np.nanmax(np.hstack((ticswitch,tnbswitch)))
            fttvec = np.arange(tmin,tmax,0.1)
            for ii in np.arange(0,fttvec.size - 5):
                if (fttvec[ii] - lasttime) > dtmin and nadded < ns[1]:
                    dipfilt = np.all([itvec >= fttvec[ii],itvec <= fttvec[ii+5]],axis=0)
                    adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                    ipflag = True if adip is not None and np.abs(adip) < 1.0e4 else False
                    adwp = None
                    if RD["WTH"] is not None:
                        wtfilt = np.all([RD["TWTH"] >= (fttvec[ii] - 0.5),RD["TWTH"] <= (fttvec[ii] + 1.0)],axis=0)
                        adwp = np.mean(np.diff(RD["WTH"][wtfilt])) if np.any(wtfilt) else None
                    if RD["WMHD"] is not None:
                        wmfilt = np.all([RD["TWMHD"] >= (fttvec[ii] - 0.5),RD["TWMHD"] <= (fttvec[ii] + 1.0)],axis=0)
                        adwp = np.mean(np.diff(RD["WMHD"][wmfilt])) if np.any(wmfilt) else None
                    ssflag = True if adwp is not None and np.abs(adwp) < 1.0e5 else False
                    ecflag = True
                    for tt in tecswitch:
                        if ecflag:
                            ecflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii+5] + 0.05)],axis=0) else True
                    icflag = True
                    for tt in ticswitch:
                        if icflag:
                            icflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii+5] + 0.05)],axis=0) else True
                    nbflag = True
                    for tt in tnbswitch:
                        if nbflag:
                            nbflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii+5] + 0.05)],axis=0) else True
                    twflag = True
                    for tw in RD["TW"]:
                        if twflag:
                            twaflag = False if np.all([tw[0] >= fttvec[ii],tw[0] <= (fttvec[ii+5] + 0.05 + dtmin)],axis=0) else True
                            twbflag = False if np.all([tw[1] >= (fttvec[ii] - 0.05 - dtmin),tw[1] <= fttvec[ii+5]],axis=0) else True
                            twflag = False if not twaflag or not twbflag else True
                    if ipflag and ssflag and ecflag and icflag and nbflag and twflag:
                        tfilt = np.all([itvec >= fttvec[ii],itvec <= fttvec[ii+5]],axis=0)
                        ecpower = 0.0
                        if "SECRH" in RD:
                            ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                            if ecpower < 0.0:
                                ecpower = 0.0
                        icpower = 0.0
                        if "SICRH" in RD:
                            icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                            if icpower < 0.0:
                                icpower = 0.0
                        nbpower = 0.0
                        if "SNBI" in RD:
                            nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                            if nbpower < 0.0:
                                nbpower = 0.0
                        timewindow = (fttvec[ii],fttvec[ii+5],1,ecpower,icpower,nbpower)
                        RD["TW"].append(timewindow)
                        lasttime = fttvec[ii+5]
                        nadded = nadded + 1
            rlasttime = np.nanmax([rlasttime,lasttime],axis=0)
        lasttime = olasttime
        # Third pass scans the whole phase, allowing selection of purely ohmic time windows (last pass)
        fttvec = np.arange(tfton + 2.0,tftoff,0.1)
        for ii in np.arange(0,fttvec.size - 5):
            if (fttvec[ii] - lasttime) > dtmin and nadded < ns[1]:
                dipfilt = np.all([itvec >= fttvec[ii],itvec <= fttvec[ii+5]],axis=0)
                adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                ipflag = True if adip is not None and np.abs(adip) < 1.0e4 else False
                adwp = None
                if RD["WTH"] is not None:
                    wtfilt = np.all([RD["TWTH"] >= (fttvec[ii] - 0.5),RD["TWTH"] <= (fttvec[ii] + 1.0)],axis=0)
                    adwp = np.mean(np.diff(RD["WTH"][wtfilt])) if np.any(wtfilt) else None
                if RD["WMHD"] is not None:
                    wmfilt = np.all([RD["TWMHD"] >= (fttvec[ii] - 0.5),RD["TWMHD"] <= (fttvec[ii] + 1.0)],axis=0)
                    adwp = np.mean(np.diff(RD["WMHD"][wmfilt])) if np.any(wmfilt) else None
                ssflag = True if adwp is not None and np.abs(adwp) < 1.0e5 else False
                ecflag = True
                for tt in tecswitch:
                    if ecflag:
                        ecflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii+5] + 0.05)],axis=0) else True
                icflag = True
                for tt in ticswitch:
                    if icflag:
                        icflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii+5] + 0.05)],axis=0) else True
                nbflag = True
                for tt in tnbswitch:
                    if nbflag:
                        nbflag = False if np.all([tt >= (fttvec[ii] - 0.5),tt <= (fttvec[ii+5] + 0.05)],axis=0) else True
                twflag = True
                for tw in RD["TW"]:
                    if twflag:
                        twaflag = False if np.all([tw[0] >= fttvec[ii],tw[0] <= (fttvec[ii+5] + 0.05 + dtmin)],axis=0) else True
                        twbflag = False if np.all([tw[1] >= (fttvec[ii] - 0.05 - dtmin),tw[1] <= fttvec[ii+5]],axis=0) else True
                        twflag = False if not twaflag or not twbflag else True
                if ipflag and ssflag and ecflag and icflag and nbflag and twflag:
                    tfilt = np.all([itvec >= fttvec[ii],itvec <= fttvec[ii+5]],axis=0)
                    ecpower = 0.0
                    if "SECRH" in RD:
                        ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                        if ecpower < 0.0:
                            ecpower = 0.0
                    icpower = 0.0
                    if "SICRH" in RD:
                        icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                        if icpower < 0.0:
                            icpower = 0.0
                    nbpower = 0.0
                    if "SNBI" in RD:
                        nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                        if nbpower < 0.0:
                            nbpower = 0.0
                    timewindow = (fttvec[ii],fttvec[ii+5],1,ecpower,icpower,nbpower)
                    RD["TW"].append(timewindow)
                    lasttime = fttvec[ii+5]
                    nadded = nadded + 1
        lasttime = np.nanmax([rlasttime,lasttime],axis=0)

        # Time window selection for ramp-down phase, accounting for heating system behaviour and minimizing time
        # window overlap / bunching
        olasttime = lasttime
        trampdown = tftoff + 1.5
        rdtvec = np.hstack((trampdown + 0.01,tecswitch + 0.01,ticswitch + 0.01,tnbswitch + 0.01))
        rdtvec = np.arange(trampdown,RD["TIP"][-1],0.1)
        dtmin = (RD["TIP"][-1] - trampdown) / float(ns[2] + 2)
        nadded = 0
        # First pass looks slightly ahead of all identified heating source switching times, most likely to be interesting
        for ii in np.arange(0,rdtvec.size):
            if rdtvec[ii] >= (tftoff + 1.0) and (rdtvec[ii] - lasttime) > dtmin and nadded < ns[2]:
                dipfilt = np.all([itvec >= rdtvec[ii],itvec <= (rdtvec[ii] + 0.2)],axis=0)
                adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                ipflag = True if adip is not None and adip < -1.0e4 else False
                azeff = None
                if RD["ZEFS"] is not None:
                    zefsfilt = np.all([RD["TZEFS"] >= rutvec[ii],RD["TZEFS"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFS"][zefsfilt]) if np.any(zefsfilt) else None
                elif RD["ZEFT"] is not None:
                    zeftfilt = np.all([RD["TZEFT"] >= rutvec[ii],RD["TZEFT"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFT"][zeftfilt]) if np.any(zeftfilt) else None
                zeffflag = True if azeff is not None and azeff < 4.0 else False
                ecflag = True
                for tt in tecswitch:
                    if ecflag:
                        ecflag = False if np.all([tt >= rdtvec[ii],tt <= (rdtvec[ii] + 0.21)],axis=0) else True
                icflag = True
                for tt in ticswitch:
                    if icflag:
                        icflag = False if np.all([tt >= rdtvec[ii],tt <= (rdtvec[ii] + 0.21)],axis=0) else True
                nbflag = True
                for tt in tnbswitch:
                    if nbflag:
                        nbflag = False if np.all([tt >= rdtvec[ii],tt <= (rdtvec[ii] + 0.21)],axis=0) else True
                if ipflag and zeffflag and ecflag and icflag and nbflag:
                    tfilt = np.all([itvec >= rdtvec[ii],itvec <= (rdtvec[ii] + 0.2)],axis=0)
                    ecpower = 0.0
                    if "SECRH" in RD:
                        ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                        if ecpower < 0.0:
                            ecpower = 0.0
                    icpower = 0.0
                    if "SICRH" in RD:
                        icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                        if icpower < 0.0:
                            icpower = 0.0
                    nbpower = 0.0
                    if "SNBI" in RD:
                        nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                        if nbpower < 0.0:
                            nbpower = 0.0
                    timewindow = (rdtvec[ii],rdtvec[ii] + 0.2,2,ecpower,icpower,nbpower)
                    RD["TW"].append(timewindow)
                    lasttime = rdtvec[ii] + 0.2
                    nadded = nadded + 1
        rlasttime = lasttime
        lasttime = olasttime
        # Second pass scans the whole phase, allowing selection of purely ohmic time windows (last pass)
        rdtvec = np.arange(trampdown,RD["TIP"][-1],0.1)
        for ii in np.arange(0,rdtvec.size - 2):
            if (rdtvec[ii] - lasttime) > dtmin and nadded < ns[2]:
                dipfilt = np.all([itvec >= rdtvec[ii],itvec <= rdtvec[ii+2]],axis=0)
                adip = np.mean(lindipdt[dipfilt]) if np.any(dipfilt) else None
                ipflag = True if adip is not None and adip < -1.0e4 else False
                azeff = None
                if RD["ZEFS"] is not None:
                    zefsfilt = np.all([RD["TZEFS"] >= rutvec[ii],RD["TZEFS"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFS"][zefsfilt]) if np.any(zefsfilt) else None
                elif RD["ZEFT"] is not None:
                    zeftfilt = np.all([RD["TZEFT"] >= rutvec[ii],RD["TZEFT"] <= (rutvec[ii] + 0.2)],axis=0)
                    azeff = np.mean(RD["ZEFT"][zeftfilt]) if np.any(zeftfilt) else None
                zeffflag = True if azeff is not None and azeff < 4.0 else False
                ecflag = True
                for tt in tecswitch:
                    if ecflag:
                        ecflag = False if np.all([tt >= rdtvec[ii],tt <= (rdtvec[ii+2] + 0.01)],axis=0) else True
                icflag = True
                for tt in ticswitch:
                    if icflag:
                        icflag = False if np.all([tt >= rdtvec[ii],tt <= (rdtvec[ii+2] + 0.01)],axis=0) else True
                nbflag = True
                for tt in tnbswitch:
                    if nbflag:
                        nbflag = False if np.all([tt >= rdtvec[ii],tt <= (rdtvec[ii+2] + 0.01)],axis=0) else True
                twflag = True
                for tw in RD["TW"]:
                    if twflag:
                        twaflag = False if np.all([tw[0] >= rdtvec[ii],tw[0] <= (rdtvec[ii+2] + 0.01 + dtmin)],axis=0) else True
                        twbflag = False if np.all([tw[1] >= (rdtvec[ii] - 0.01 - dtmin),tw[1] <= rdtvec[ii+2]],axis=0) else True
                        twflag = False if not twaflag or not twbflag else True
                if ipflag and zeffflag and ecflag and icflag and nbflag and twflag:
                    tfilt = np.all([itvec >= rdtvec[ii],itvec <= rdtvec[ii+2]],axis=0)
                    ecpower = 0.0
                    if "SECRH" in RD:
                        ecpower = np.mean(RD["SECRH"][tfilt]) if np.any(tfilt) else 0.0
                        if ecpower < 0.0:
                            ecpower = 0.0
                    icpower = 0.0
                    if "SICRH" in RD:
                        icpower = np.mean(RD["SICRH"][tfilt]) if np.any(tfilt) else 0.0
                        if icpower < 0.0:
                            icpower = 0.0
                    nbpower = 0.0
                    if "SNBI" in RD:
                        nbpower = np.mean(RD["SNBI"][tfilt]) if np.any(tfilt) else 0.0
                        if nbpower < 0.0:
                            nbpower = 0.0
                    timewindow = (rdtvec[ii],rdtvec[ii+2],2,ecpower,icpower,nbpower)
                    RD["TW"].append(timewindow)
                    lasttime = rdtvec[ii+2]
                    nadded = nadded + 1

#        for tw in RD["TW"]:
#            print(tw[0],tw[1],tw[2])

        # Optional plotting done here
        if do_plot:
            if not os.path.exists('./TPlots/'):
                os.makedirs('./TPlots/')

            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.plot(RD["TIP"],np.abs(RD["IP"])*1.0e-6,label=r'I_p [MA]')
            if RD["BMAG"] is not None and RD["TBMAG"] is not None:
                ax.plot(RD["TBMAG"],np.abs(RD["BMAG"]),label=r'B_{mag} [T]')
            if RD["WMHD"] is not None and RD["TWMHD"] is not None:
                ax.plot(RD["TWMHD"],RD["WMHD"]*1.0e-6,label=r'W_{mhd} [MJ]')
            if RD["TEAX"] is not None and RD["TTEAX"] is not None:
                ax.plot(RD["TWTH"],RD["WTH"]*1.0e-6,label=r'W_{th} [MJ]')
            if RD["ZEFS"] is not None and RD["TZEFS"] is not None:
                ax.plot(RD["TZEFS"],RD["ZEFS"],label=r'Z_{eff}')
            elif RD["ZEFT"] is not None and RD["TZEFT"] is not None:
                ax.plot(RD["TZEFT"],RD["ZEFT"],label=r'Z_{eff}')
            (ymin,ymax) = ax.get_ylim()
            ccs = ['k','b','r','g','m','c','y']
            lss = ['-.','--',':','-']
            phase = 0
            ii = [0] * len(ns)
            for tw in RD["TW"]:
                phase = int(tw[2])
                ax.plot([tw[0],tw[0]],[ymin,ymax],color=ccs[int(ii[phase])],ls=lss[phase])
                ax.plot([tw[1],tw[1]],[ymin,ymax],color=ccs[int(ii[phase])],ls=lss[phase])
                ii[phase] = int(ii[phase]) + 1
            ax.set_ylim(ymin,ymax)
            ax.set_ylabel(r'Assorted Values')
            ax.set_xlabel(r'Time [s]')
            plt.legend(loc='best')
            fig.savefig('./TPlots/Auto_T' + str(snum) + '_basic.png')
            plt.close()

            fig = plt.figure()
            ax = fig.add_subplot(111)
            sc = 1.0e-3 if np.mean(np.abs(RD["OHM"])) < 1.0e4 else 1.0e-6
            unit = 'kW' if np.mean(np.abs(RD["OHM"])) < 1.0e4 else 'MW'
            ax.plot(RD["TOHM"],RD["OHM"]*sc,label=r'P_{ohm} ['+unit+']')
            if RD["ICRH"] is not None:
                ax.plot(RD["TICRH"],RD["ICRH"]*1.0e-6,label=r'P_{IC} [MW]')
            if RD["NBI"] is not None:
                ax.plot(RD["TNBI"],RD["NBI"]*1.0e-6,label=r'P_{NBI} [MW]')
            if RD["ECRH"] is not None:
                ax.plot(RD["TECRH"],RD["ECRH"]*1.0e-6,label=r'P_{EC} [MW]')
            (ymin,ymax) = ax.get_ylim()
            ccs = ['k','b','r','g','m','c','y']
            lss = ['-.','--',':','-']
            ii = np.zeros((len(ns),))
            for tw in RD["TW"]:
                phase = int(tw[2])
                ax.plot([tw[0],tw[0]],[ymin,ymax],color=ccs[int(ii[phase])],ls=lss[phase])
                ax.plot([tw[1],tw[1]],[ymin,ymax],color=ccs[int(ii[phase])],ls=lss[phase])
                ii[phase] = int(ii[phase]) + 1
            ax.set_ylim(ymin,ymax)
            ax.set_ylabel(r'Assorted Values')
            ax.set_xlabel(r'Time [s]')
            plt.legend(loc='best')
            fig.savefig('./TPlots/Auto_T' + str(snum) + '_power.png')
            plt.close()

        # Save extracted shot data into a "pickle" file
        if pdir:
            if not os.path.isdir(pdir):
                os.makedirs(pdir)
            snstr = "%06d" % (int(snum))
            with open(pdir+'tS'+snstr+'.p','rb') as bf:
                pickle.dump(RD,bf,protocol=3)

    return RD


def make_printable(twdict):
    """
    JET-SPECIFIC FUNCTION
    Takes the custom time window storage container and
    converts it into a list of printable strings,
    formatted for use as 'shotlist.txt' input file for
    the EX2GK program.

    :arg twdict: dict. Custom time window storage object generated by this file

    :returns: list. List of printable strings representing the time windows given
    """
    RD = None
    if isinstance(twdict,dict):
        RD = copy.deepcopy(twdict)
    phaselist = ["Ramp-up","Flat-top","Ramp-down"]

    TL = None
    if isinstance(RD,dict) and "TW" in RD:
        TL = []
        for ii in np.arange(0,len(RD["TW"])):
            tw = RD["TW"][ii]
            nn = tw[2]
            if nn == 0:
                nn = 1
            elif nn == 1:
                nn = 0
            TL.append("%10d%10.4f%10.4f%4d #   T%d - %s, EC: %8.4f MW, IC: %8.4f MW, NB: %8.4f MW" % (RD["SHOT"],tw[0],tw[1],nn,ii+1,phaselist[tw[2]],tw[3],tw[4],tw[5]))

    return TL
