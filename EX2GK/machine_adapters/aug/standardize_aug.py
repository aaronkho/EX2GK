# Script with functions to standardize extracted JET PPF data structure for final representation
# Developer: Aaron Ho - 10/07/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools

def transfer_generic_data(procdata,newstruct=None,usepol=False,usexeb=False,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Transfers any crucial data fields which require little to no
    additional processing from the raw extracted data structure into
    a new processed data structure. This concept prevents the
    accumulation of unnecessary data in the final data structure.

    :arg procdata: dict. Implementation-specific object containing unified coordinate system and extraneous processed data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :returns: dict. Implementation-specific object with general metadata transferred.
    """
    PD = None
    SD = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()

    if PD is not None:

        # Transfers generic metadata
        SD["DEVICE"] = PD["DEVICE"] if "DEVICE" in PD else "JET"
        SD["INTERFACE"] = PD["INTERFACE"] if "INTERFACE" in PD else "UNKNOWN"
        SD["SHOT"] = int(PD["SHOT"]) if "SHOT" in PD else None
        SD["T1"] = float(PD["T1"]) if "T1" in PD else None
        SD["T2"] = float(PD["T2"]) if "T2" in PD else None
        SD["SHOTPHASE"] = int(PD["SHOTPHASE"]) if "SHOTPHASE" in PD else -1
        SD["WINDOW"] = PD["WINDOW"] if "WINDOW" in PD else -1

        SD["GPCOORDFLAG"] = True if "GPCOORDFLAG" in PD and PD["GPCOORDFLAG"] else False
#        SD["RSHIFTFLAG"] = True if "RSHIFTFLAG" in PD and PD["RSHIFTFLAG"] else False
        SD["WALLMAT"] = PD["WALLMAT"] if "WALLMAT" in PD and isinstance(PD["WALLMAT"],str) else "UNKNOWN"
        SD["CONTMAT"] = PD["CONTMAT"] if "CONTMAT" in PD and isinstance(PD["CONTMAT"],str) else "UNKNOWN"

        SD["POLFLAG"] = True if usepol else False
        SD["RHOEBFLAG"] = True if usexeb else False
        SD["IDAFLAG"] = True if "IDAFLAG" in PD and PD["IDAFLAG"] else False

        SD["EQLIST"] = PD["EQLIST"] if "EQLIST" in PD and isinstance(PD["EQLIST"],list) and len(PD["EQLIST"]) > 0 else ['efit']
        for key in PD:
            if re.match(r'^C?C[SJE]_.+$',key):
                SD[key] = copy.deepcopy(PD[key])
#        SD["PFNMAP"] = copy.deepcopy(PD["PFNMAP"]) if "PFNMAP" in PD and PD["PFNMAP"] is not None else None
#        SD["RMAP"] = copy.deepcopy(PD["RMAP"]) if "RMAP" in PD and PD["RMAP"] is not None and PD["PFNMAP"] is not None else None
#        SD["ZMAP"] = copy.deepcopy(PD["ZMAP"]) if "ZMAP" in PD and PD["ZMAP"] is not None and PD["PFNMAP"] is not None else None

        # Required values with prescribed defaults
        SD["RMAG"] = float(PD["RMAG"]) if "RMAG" in PD and PD["RMAG"] is not None else 3.0
        SD["RMAGEB"] = float(PD["RMAGEB"]) if "RMAGEB" in PD and PD["RMAGEB"] is not None else None
        SD["ZMAG"] = float(PD["ZMAG"]) if "ZMAG" in PD and PD["ZMAG"] is not None else 0.0
        SD["ZMAGEB"] = float(PD["ZMAGEB"]) if "ZMAGEB" in PD and PD["ZMAGEB"] is not None else None
        SD["RGEO"] = float(PD["RGEO"]) if "RGEO" in PD and PD["RGEO"] is not None else 2.96
        SD["RGEOEB"] = float(PD["RGEOEB"]) if "RGEOEB" in PD and PD["RGEOEB"] is not None else None
        SD["ZGEO"] = float(PD["ZGEO"]) if "ZGEO" in PD and PD["ZGEO"] is not None else SD["ZMAG"]
        SD["ZGEOEB"] = float(PD["ZGEOEB"]) if "ZGEOEB" in PD and PD["ZGEOEB"] is not None else SD["ZMAGEB"]
        SD["IP"] = float(PD["IP"]) if "IP" in PD and PD["IP"] is not None else 3.0e6
        SD["IPEB"] = float(PD["IPEB"]) if "IPEB" in PD and PD["IPEB"] is not None else 4.0e4

        SD["BVAC"] = float(PD["BVAC"]) if "BVAC" in PD and PD["BVAC"] is not None else None
        SD["BVACEB"] = float(PD["BVACEB"]) if "BVACEB" in PD and PD["BVACEB"] is not None else None
        SD["RMAJSHIFT"] = float(PD["RMAJSHIFT"]) if "RMAJSHIFT" in PD and PD["RMAJSHIFT"] is not None else None
        SD["RMAJSHIFTEB"] = float(PD["RMAJSHIFTEB"]) if "RMAJSHIFTEB" in PD and PD["RMAJSHIFTEB"] is not None else None
        SD["NESCALE"] = float(PD["HRTSC"]) if "HRTSC" in PD and PD["HRTSC"] is not None else None
        SD["NESCALEEB"] = float(PD["HRTSCEB"]) if "HRTSCEB" in PD and PD["HRTSCEB"] is not None else None
        SD["BMAG"] = float(PD["BMAG"]) if "BMAG" in PD and PD["BMAG"] is not None else None
        SD["BMAGEB"] = float(PD["BMAGEB"]) if "BMAGEB" in PD and PD["BMAGEB"] is not None else None
        SD["VAVG"] = float(np.mean(PD["VLOOP"])) if "VLOOP" in PD and PD["VLOOP"] is not None else None
        SD["VAVGEB"] = None

        # These are not strictly necessary, most quantities used flux surface averaged magnetic fields, which are approximated by B_mag
#        btovec = 1.0 / btfunc(SD["CS_RMAJORO"]) if btfunc is not None else None
#        btivec = 1.0 / btfunc(SD["CS_RMAJORI"]) if btfunc is not None else None
#        if "CCS_BASE" in SD and SD["CCS_BASE"] is not None:
#            cbtovec = 1.0 / btfunc(SD["CCS_RMAJORO"]) if btfunc is not None and "CCS_RMAJORO" in SD and SD["CCS_RMAJORO"] is not None else None
#            cbtivec = 1.0 / btfunc(SD["CCS_RMAJORI"]) if btfunc is not None and "CCS_RMAJORI" in SD and SD["CCS_RMAJORI"] is not None else None

        SD["PNBI"] = float(PD["PNBI"]) if "PNBI" in PD and PD["PNBI"] is not None else None
        SD["PNBIEB"] = float(PD["PNBIEB"]) if "PNBIEB" in PD and PD["PNBIEB"] is not None else None
        SD["LNBI"] = float(PD["LNBI"]) if "LNBI" in PD and PD["LNBI"] is not None else None
        SD["LNBIEB"] = float(PD["LNBIEB"]) if "LNBIEB" in PD and PD["LNBIEB"] is not None else None
        SD["PICRH"] = float(PD["PICRH"]) if "PICRH" in PD and PD["PICRH"] is not None else None
        SD["PICRHEB"] = float(PD["PICRHEB"]) if "PICRHEB" in PD and PD["PICRHEB"] is not None else None
        SD["LICRH"] = float(PD["LICRH"]) if "LICRH" in PD and PD["LICRH"] is not None else None
        SD["LICRHEB"] = float(PD["LICRHEB"]) if "LICRHEB" in PD and PD["LICRHEB"] is not None else None
        SD["PECRH"] = float(PD["PECRH"]) if "PECRH" in PD and PD["PECRH"] is not None else None
        SD["PECRHEB"] = float(PD["PECRHEB"]) if "PECRHEB" in PD and PD["PECRHEB"] is not None else None
        SD["LECRH"] = float(PD["LECRH"]) if "LECRH" in PD and PD["LECRH"] is not None else None
        SD["LECRHEB"] = float(PD["LECRHEB"]) if "LECRHEB" in PD and PD["LECRHEB"] is not None else None
        SD["PLH"] = float(PD["PLH"]) if "PLH" in PD and PD["PLH"] is not None else None
        SD["PLHEB"] = float(PD["PLHEB"]) if "PLHEB" in PD and PD["PLHEB"] is not None else None
        SD["LLH"] = float(PD["LLH"]) if "LLH" in PD and PD["LLH"] is not None else None
        SD["LLHEB"] = float(PD["LLHEB"]) if "LLHEB" in PD and PD["LLHEB"] is not None else None
        SD["POHM"] = float(PD["POHM"]) if "POHM" in PD and PD["POHM"] is not None else None
        SD["POHMEB"] = float(PD["POHMEB"]) if "POHMEB" in PD and PD["POHMEB"] is not None else None
        SD["LOHM"] = float(PD["LOHM"]) if "LOHM" in PD and PD["LOHM"] is not None else None
        SD["LOHMEB"] = float(PD["LOHMEB"]) if "LOHMEB" in PD and PD["LOHMEB"] is not None else None

        SD["ZEFF"] = float(PD["ZEFF"]) if "ZEFF" in PD and PD["ZEFF"] is not None else None
        SD["ZEFFEB"] = float(PD["ZEFFEB"]) if "ZEFFEB" in PD and PD["ZEFFEB"] is not None else None

        SD["TWD"] = float(PD["TWD"]) if "TWD" in PD and PD["TWD"] is not None else None
        SD["TWDEB"] = float(PD["TWDEB"]) if "TWDEB" in PD and PD["TWDEB"] is not None else None
        SD["BETAND"] = float(PD["BETAND"]) if "BETAND" in PD and PD["BETAND"] is not None else None
        SD["BETANDEB"] = float(PD["BETANDEB"]) if "BETANDEB" in PD and PD["BETANDEB"] is not None else None
        SD["BETAPD"] = float(PD["BETAPD"]) if "BETAPD" in PD and PD["BETAPD"] is not None else None
        SD["BETAPDEB"] = float(PD["BETAPDEB"]) if "BETAPDEB" in PD and PD["BETAPDEB"] is not None else None
        SD["NEUT"] = float(PD["NEUT"]) if "NEUT" in PD and PD["NEUT"] is not None else None
        SD["NEUTEB"] = float(PD["NEUTEB"]) if "NEUTEB" in PD and PD["NEUTEB"] is not None else None

        hfrac = float(PD["HFRAC"]) if "HFRAC" in PD and PD["HFRAC"] is not None and PD["HFRAC"] > 0.0 else 0.0
        dfrac = float(PD["DFRAC"]) if "DFRAC" in PD and PD["DFRAC"] is not None and PD["DFRAC"] > 0.0 else 0.001
        tfrac = float(PD["TFRAC"]) if "TFRAC" in PD and PD["TFRAC"] is not None and PD["TFRAC"] > 0.0 else 0.0
        hfraceb = float(PD["HFRACEB"]) if "HFRACEB" in PD and PD["HFRACEB"] is not None else 0.0
        dfraceb = float(PD["DFRACEB"]) if "DFRACEB" in PD and PD["DFRACEB"] is not None else 0.0
        tfraceb = float(PD["TFRACEB"]) if "TFRACEB" in PD and PD["TFRACEB"] is not None else 0.0
        fraclist = np.array([dfrac,hfrac,tfrac])
        total = np.sum(fraclist)
        if hfrac > 0.0 and hfrac < total:
            SD["FH"] = hfrac / total
            SD["FHEB"] = hfraceb / total
        if dfrac > 0.0 and dfrac < total:
            SD["FD"] = dfrac / total
            SD["FDEB"] = dfraceb / total
        if tfrac > 0.0 and tfrac < total:
            SD["FT"] = tfrac / total
            SD["FTEB"] = tfraceb / total

        plasmats = ['D','H','T']
        fraceblist = np.array([dfraceb,hfraceb,tfraceb])
        idxmax = np.where(fraclist >= np.nanmax(fraclist))[0][0]
        SD["PLASMAT"] = plasmats[idxmax]

    if fdebug:
        print("standardize_aug.py: transfer_generic_data() completed.")

    return SD


def convert_coords(datadict,datavec,csin,csout,prein=None,preout=None,cdebug=False):
    """
    Automated conversion between the unified coordinate systems defined
    in the pre-processing step, simply by providing the standardized
    names of the input and output coordinate system. Linearly
    interpolates between the standardized 101 point grid and provides
    the associated Jacobians as well.

    For any coordinate systems with applied corrections or biases, see
    the implementation-specific adapter files for relevant names. For
    conversion from uncorrected to corrected systems, by convention, the
    output coordinate system should have the letter C appended to the
    beginning. For conversion between corrected systems, simply use the
    argument corrflag with the standard names. Note that these corrected
    systems may come with an additional radial coordinate error due to
    the statistics used to formulate the applied correction.

    Standardized coordinate system names
        POLFLUXN = Normalized poloidal flux
        POLFLUX  = Absolute poloidal flux
        RHOPOLN  = Normalized poloidal rho (sqrt POLFLUXN)
        TORFLUXN = Normalized toroidal flux
        TORFLUX  = Absolute toroidal flux
        RHOTORN  = Normalized toroidal rho (sqrt TORFLUXN)
        RMAJORO  = Absolute outer major radius at height of magnetic axis
        RMAJORI  = Absolute inner major radius at height of magnetic axis
        RMINORO  = Absolute outer minor radius at height of magnetic axis
        RMINORI  = Absolute inner minor radius at height of magnetic axis
        FSVOL    = Flux surface volume
        FSAREA   = Flux surface surface area

    :arg datadict: dict. Object containing unified coordinate system information.

    :arg datavec: array. Vector of radial points corresponding to input coordinate system.

    :arg csin: str. Standardized name of input coordinate system.

    :arg csout: str. Standardized name of output coordinate system.

    :kwarg prein: str. Standardized prefix for input coordinate system. I for interface, C for corrected.

    :kwarg preout: str. Standardized prefix for output coordinate system. I for interface, C for corrected.

    :returns: (array, array, array).
        Vector of radial points corresponding to the output coordinate system, vector
        of Jacobians corresponding to the conversion between input and output
        coordinate systems at the radial points, vector of radial errors corresponding
        to the conversion between input and output coordinate systems.
    """
    cslist = ['RHOTORN','TORFLUX','TORFLUXN', \
              'RHOPOLN','POLFLUX','POLFLUXN', \
              'RMAJORO','RMAJORI', \
              'RMINORO','RMINORI', \
              'FSVOL','FSAREA']

    dobj = None
    dvec = None
    ics = None         # Input coordinate system label
    ocs = None         # Output coordinate system label
    if isinstance(datadict,dict):
        dobj = datadict
    if isinstance(datavec,(float,int,np.int8,np.int16,np.int32,np.int64,np.uint8,np.uint16,np.uint32,np.uint64,np.float16,np.float32,np.float64)):
        dvec = np.array([datavec]).flatten()
    elif isinstance(datavec,(list,tuple)):
        dvec = np.array(datavec).flatten()
    elif isinstance(datavec,np.ndarray):
        dvec = datavec.flatten()
    for cs in cslist:
        if re.match(r'^'+cs+r'$',csin,flags=re.IGNORECASE):
            ics = cs
        if re.match(r'^'+cs+r'$',csout,flags=re.IGNORECASE):
            ocs = cs

    # Standardized coordinate label prefixes
    csp = 'CS'       # Coordinate system prefix
    cjp = 'CJ'       # Coordinate Jacobian prefix
    cep = 'CE'       # Coordinate error prefix

    cvec = None       # Output coordinate vector
    evec = None       # Output error vector
    jvec = None       # Output Jacobian vector

    if isinstance(dobj,dict) and ics is not None and ocs is not None:

        # Determine existence and usage of corrected coordinate systems
        icp = prein if "CS_PREFIXES" in dobj and prein in dobj["CS_PREFIXES"] else ""
        ocp = preout if "CS_PREFIXES" in dobj and preout in dobj["CS_PREFIXES"] else ""

        if cdebug:
            print(icp+csp+"_"+ics,ocp+csp+"_"+ocs)

        # Simplify process if input and output coordinate systems are identical
        if re.match(ics,ocs,flags=re.IGNORECASE) and icp == ocp:
            cvec = dvec.copy()
            jvec = np.ones(cvec.shape)
            evec = np.zeros(cvec.shape)

        else:

            # Check existence of input and output coordinate systems
            cics = None
            cocs = None
            if isinstance(dobj,dict) and ics is not None and ocs is not None:
                if icp+csp+"_"+ics not in dobj:
                    ics = None
                if ocp+csp+"_"+ocs not in dobj:
                    ocs = None

                # Additional treatment needed for conversion between corrected and uncorrected systems
                if icp != ocp:

                    # Check existence of conversion coordinate system
                    cfull = False
                    if icp+csp+"_BASECORRO" in dobj and dobj[icp+csp+"_BASECORRO"] is not None and icp+csp+"_BASECORRI" in dobj and dobj[icp+csp+"_BASECORRI"] is not None:
                        cflag = True
                        cfull = True
                    elif ocp+csp+"_BASECORRO" in dobj and dobj[ocp+csp+"_BASECORRO"] is not None and ocp+csp+"_BASECORRI" in dobj and dobj[ocp+csp+"_BASECORRI"] is not None:
                        cflag = True
                        cfull = True
                    elif (icp+csp+"_BASECORR" in dobj and dobj[icp+csp+"_BASECORR"] is not None) or (ocp+csp+"_BASECORR" in dobj and dobj[ocp+csp+"_BASECORR"] is not None):
                        cflag = True

                    if cflag:
                        if cfull:
                            ice = "I" if ics.endswith("I") else "O"
                            oce = "I" if ocs.endswith("I") else "O"
                            if icp+csp+"_BASECORR"+ice in dobj and dobj[icp+csp+"_BASECORR"+ice] is not None:
                                cics = "BASECORR"+ice
                                cocs = dobj[ocp+csp+"_BASE"] if ocp+csp+"_BASE" in dobj else None
                            elif ocp+csp+"_BASECORR"+oce in dobj and dobj[icp+csp+"_BASECORR"+ice] is not None:
                                cocs = "BASECORR"+oce
                                cics = dobj[icp+csp+"_BASE"] if icp+csp+"_BASE" in dobj else None
                        else:
                            if icp+csp+"_BASECORR" in dobj and dobj[icp+csp+"_BASECORR"] is not None:
                                cics = "BASECORR"
                                cocs = dobj[ocp+csp+"_BASE"] if ocp+csp+"_BASE" in dobj else None
                            elif ocp+csp+"_BASECORR" in dobj and dobj[ocp+csp+"_BASECORR"] is not None:
                                cocs = "BASECORR"
                                cics = dobj[icp+csp+"_BASE"] if icp+csp+"_BASE" in dobj else None
                    else:
                        icp = ""
                        ocp = ""

            else:
                ics = None
                ocs = None

            # Extract base coordinate for Jacobians
            ijb = None
            ojb = None
            if icp+cjp+"_BASE" in dobj and dobj[icp+cjp+"_BASE"] in cslist:
                ijb = dobj[icp+cjp+"_BASE"]
            if ocp+cjp+"_BASE" in dobj and dobj[ocp+cjp+"_BASE"] in cslist:
                ojb = dobj[ocp+cjp+"_BASE"]

            if ics is not None and ocs is not None and ijb is not None and ojb is not None:

                # Containers for coordinate system vectors
                civec = dobj[icp+csp+"_"+ics]
                ccivec = None
                ccovec = None
                covec = dobj[ocp+csp+"_"+ocs]

                # Containers for coordinate system Jacobian vectors
                jivec = dobj[icp+cjp+"_"+ijb+"_"+ics]
                jcivec = None
                jcovec = None
                jovec = dobj[ocp+cjp+"_"+ojb+"_"+ocs]

                # Containers for coordinate system errors
                ecvec = None
                eovec = dobj[ocp+cep+"_"+ocs]

                # Additional vectors needed for conversion between corrected and uncorrected systems
                if cics is not None and cocs is not None:

                    ccivec = dobj[icp+csp+"_"+cics]
                    ccovec = dobj[ocp+csp+"_"+cocs]
                    jcivec = dobj[icp+cjp+"_"+ijb+"_"+cics]
                    jcovec = dobj[ocp+cjp+"_"+ojb+"_"+cocs]
                    if re.match(r'^BASECORR[IO]?$',cics,flags=re.IGNORECASE):
                        ecvec = dobj[icp+cep+"_"+cics]
                    else:
                        ecvec = dobj[ocp+cep+"_"+cocs]

                if cdebug:
                    print(civec)
                    print(covec)

                # Double-pass needed for conversion between corrected and uncorrected systems, otherwise just single-pass
                if ccivec is not None and ccovec is not None and jcivec is not None and jcovec is not None and ecvec is not None:
                    (cvec,jvec1,evec1) = ptools.convert_base_coords(dvec,civec,ccivec,jivec,jcivec,ecvec)
                    (cvec,jvec2,evec2) = ptools.convert_base_coords(cvec,ccovec,covec,jcovec,jovec,eovec)
                    jvec = jvec1 * jvec2 if cvec is not None and jvec1 is not None and jvec2 is not None else None
                    evec = np.sqrt(np.power(evec1,2.0) + np.power(evec2,2.0)) if cvec is not None and evec1 is not None and evec2 is not None else None
                else:
                    (cvec,jvec,evec) = ptools.convert_base_coords(dvec,civec,covec,jivec,jovec,ecvec)

                if cdebug:
                    print(dvec)
                    print(cvec)
                    print(jvec)
                    print(evec)

                if cvec is None or jvec is None or evec is None:
                    cvec = None
                    jvec = None
                    evec = None

    return (cvec,jvec,evec)


def process_averaged_profile(procdata,profdata,quantity,newstruct=None):
    """
    AUG-SPECIFIC FUNCTION
    Processes and formats raw profile data from pre-processing step into a
    standardized profile structure for the generalized fitting routine.

    :arg procdata: dict. Implementation-specific object containing unified coordinate system and extraneous processed data.

    :arg profdata: dict. Implementation-specific object containing only the processed plasma profile data.

    :arg quantity: str. The specific quantity represented by the input plasma profile data, used to flag type of processing.

    :kwarg newstruct: dict. Optional object in which averaged quantity data from a given diagnostic will be stored.

    :returns: dict. Formatted object containing the processed profile data (and fit parameters).
    """
    PD = None
    FD = None
    tag = None
    DD = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(profdata,dict):
        FD = profdata
    if isinstance(quantity,str):
        tag = quantity
    if isinstance(newstruct,dict):
        DD = newstruct

    mapflag = False
    if PD is not None and FD is not None and tag is not None and tag in FD and FD[tag] is not None:

        # Copying essential data over to the new structure
        if not isinstance(DD,dict):
            DD = dict()
        DD["VAL"] = FD[tag].copy()
        DD["VALEB"] = FD[tag+"EB"].copy() if tag+"EB" in FD and FD[tag+"EB"] is not None else None
        DD["N"] = FD["N"+tag].copy() if "N"+tag in FD and FD["N"+tag] is not None else None
        if DD["VAL"].size < 1:
            DD = None
        elif "D"+tag in FD and FD["D"+tag] is not None:
            DD["DVAL"] = FD["D"+tag].copy()
            DD["DVALEB"] = FD["D"+tag+"EB"].copy() if "D"+tag+"EB" in FD and FD["D"+tag+"EB"] is not None else None

        # Checking if psi map is provided for conversion from (r,z) to normalized poloidal flux
        if "PFNMAP" in PD and PD["PFNMAP"] is not None and "RMAP" in PD and PD["RMAP"] is not None and "ZMAP" in PD and PD["ZMAP"] is not None:
            mapflag = True

    if DD is not None and PD is not None:

        # Define the radial position of the magnetic axis for processing of the diagnostic data
        cdebug = False
        rmag = PD["RMAG"] if "RMAG" in PD else None
        if rmag is None:
            psimin = np.amin(np.abs(PD["CS_POLFLUXN"]))
            aidx = np.where(np.abs(PD["CS_POLFLUXN"]) == psimin)[0][0]
            rmag = np.average([PD["CS_RMAJORO"][aidx],PD["CS_RMAJORI"][aidx]])
        if rmag is None:
            rmag = 1.7

        if "R"+tag in FD and FD["R"+tag] is not None and "Z"+tag in FD and FD["Z"+tag] is not None and mapflag:
            rvec = float(FD["R"+tag]) if FD["R"+tag].size == 1 else FD["R"+tag].copy()
            zvec = float(FD["Z"+tag]) if FD["Z"+tag].size == 1 else FD["Z"+tag].copy()
            DD["PFN"] = ptools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],rvec,zvec)
#            pfilt = np.all([DD["OPFN"] >= -1.1,DD["OPFN"] <= 1.1],axis=0)
#            DD["OPFN"] = DD["OPFN"][pfilt]
#            DD["VAL"] = DD["VAL"][pfilt]
#            DD["VALEB"] = DD["VALEB"][pfilt]
#            if "DVAL" in DD and DD["DVAL"] is not None:
#                DD["DVAL"] = DD["DVAL"][pfilt]
#            if "DVALEB" in DD and DD["DVALEB"] is not None:
#                DD["DVALEB"] = DD["DVALEB"][pfilt]
#            DD["N"] = DD["N"][pfilt]
            DD["PFNEB"] = np.zeros(DD["PFN"].shape)
            pswap = np.where((DD["PFN"][:-1] * DD["PFN"][1:]) < 0.0)[0]
            if len(pswap) == 0:
                pdiff = np.diff(DD["PFN"])
                pswap = np.where((pdiff[:-1] * pdiff[1:]) < 0.0)[0]
            pidx = pswap[0] + 1 if len(pswap) > 0 else 0
            testvec = np.arange(0,DD["PFN"].size)
            ofhfs = (testvec < pidx)
            rmaj = np.full(DD["PFN"].shape,np.NaN)
            if not np.all(ofhfs):
                (rmajo,dummyj,dummye) = convert_coords(PD,np.abs(DD["PFN"][np.invert(ofhfs)]),"POLFLUXN","RMAJORO",prein="",preout="",cdebug=cdebug)
                rmaj[np.invert(ofhfs)] = rmajo
            if np.any(ofhfs):
                (rmaji,dummyj,dummye) = convert_coords(PD,np.abs(DD["PFN"][ofhfs]),"POLFLUXN","RMAJORI",prein="",preout="",cdebug=cdebug)
                rmaj[ofhfs] = rmaji
            DD["RMAJ"] = rmaj.copy()
            fhfs = (DD["RMAJ"] < rmag)
            DD["AXID"] = np.where(np.invert(fhfs))[0][0] if np.any(fhfs) and not np.all(fhfs) else 0
            DD["RHOPN"] = np.sqrt(np.abs(DD["PFN"]))
            pfilt = np.invert(DD["RHOPN"] == 0.0)
            DD["RHOPNEB"] = np.zeros(DD["RHOPN"].shape)
            if "RHOPNEB" in FD and FD["RHOPNEB"] is not None:
                if FD["RHOPNEB"].size == 1:
                    DD["RHOPNEB"] = np.full(DD["RHOPN"].shape,FD["RHOPNEB"][0])
                elif FD["RHOPNEB"].size == DD["RHOPN"].size:
                    DD["RHOPNEB"] = FD["RHOPNEB"].squeeze()
            DD["RHOPNEB"][pfilt] = np.sqrt(np.power(DD["RHOPNEB"][pfilt],2.0) + np.power(0.5 * DD["PFNEB"][pfilt] / DD["RHOPN"][pfilt],2.0))
            (DD["TFN"],dummyj,pteb) = convert_coords(PD,np.abs(DD["PFN"]),"POLFLUXN","TORFLUXN",prein="",preout="",cdebug=cdebug)
            DD["TFNEB"] = np.sqrt(np.power(DD["PFNEB"],2.0) + np.power(pteb,2.0))
            DD["TFN"][DD["TFN"] < 1.0e-4] = 0.0   # Enforces true zero at magnetic axis
            DD["RHOTN"] = np.sqrt(np.abs(DD["TFN"]))
            tfilt = np.invert(DD["RHOTN"] == 0.0)
            DD["RHOTNEB"] = np.full(DD["RHOTN"].shape,0.002)
            DD["RHOTNEB"][tfilt] = 0.5 * DD["TFNEB"][tfilt] / DD["RHOTN"][tfilt]
            if np.any(fhfs):
                DD["PFN"][fhfs] = -DD["PFN"][fhfs]       # pol_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOPN"][fhfs] = -DD["RHOPN"][fhfs]   # rho_pol from -1 to 1 (-1 is inner LCFS)
                DD["TFN"][fhfs] = -DD["TFN"][fhfs]       # tor_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOTN"][fhfs] = -DD["RHOTN"][fhfs]   # rho_tor from -1 to 1 (-1 is inner LCFS)

        # For ??? at AUG
        elif "PFN"+tag in FD and FD["PFN"+tag] is not None:
            DD["PFN"] = FD["PFN"+tag].copy()
            DD["PFNEB"] = np.zeros(DD["PFN"].shape)
            pswap = np.where((DD["PFN"][:-1] * DD["PFN"][1:]) < 0.0)[0]
            if len(pswap) == 0:
                pdiff = np.diff(DD["PFN"])
                pswap = np.where((pdiff[:-1] * pdiff[1:]) < 0.0)[0]
            pidx = pswap[0] + 1 if len(pswap) > 0 else 0
            testvec = np.arange(0,DD["PFN"].size)
            ofhfs = (testvec < pidx)
            rmaj = np.full(DD["PFN"].shape,np.NaN)
            if not np.all(ofhfs):
                (rmajo,dummyj,dummye) = convert_coords(PD,np.abs(DD["PFN"][np.invert(ofhfs)]),"POLFLUXN","RMAJORO",prein="",preout="",cdebug=cdebug)
                rmaj[np.invert(ofhfs)] = rmajo
            if np.any(ofhfs):
                (rmaji,dummyj,dummye) = convert_coords(PD,np.abs(DD["PFN"][ofhfs]),"POLFLUXN","RMAJORI",prein="",preout="",cdebug=cdebug)
                rmaj[ofhfs] = rmaji
            DD["RMAJ"] = rmaj.copy()
            fhfs = (DD["RMAJ"] < rmag)
            DD["AXID"] = np.where(np.invert(fhfs))[0][0] if np.any(fhfs) else 0
            DD["RHOPN"] = np.sqrt(np.abs(DD["PFN"]))
            pfilt = np.invert(DD["RHOPN"] == 0.0)
            DD["RHOPNEB"] = np.zeros(DD["RHOPN"].shape)
            if "RHOPNEB" in FD and FD["RHOPNEB"] is not None:
                if FD["RHOPNEB"].size == 1:
                    DD["RHOPNEB"] = np.full(DD["RHOPN"].shape,FD["RHOPNEB"][0])
                elif FD["RHOPNEB"].size == DD["RHOPN"].size:
                    DD["RHOPNEB"] = FD["RHOPNEB"].squeeze()
            DD["RHOPNEB"][pfilt] = np.sqrt(np.power(DD["RHOPNEB"][pfilt],2.0) + np.power(0.5 * DD["PFNEB"][pfilt] / DD["RHOPN"][pfilt],2.0))
            (DD["TFN"],dummyj,pteb) = convert_coords(PD,np.abs(DD["PFN"]),"POLFLUXN","TORFLUXN",prein="",preout="",cdebug=cdebug)
            DD["TFNEB"] = np.sqrt(np.power(DD["PFNEB"],2.0) + np.power(pteb,2.0))
            DD["TFN"][DD["TFN"] < 1.0e-4] = 0.0   # Enforces true zero at magnetic axis
            DD["RHOTN"] = np.sqrt(np.abs(DD["TFN"]))
            tfilt = np.invert(DD["RHOTN"] == 0.0)
            DD["RHOTNEB"] = np.full(DD["RHOTN"].shape,0.002)
            DD["RHOTNEB"][tfilt] = 0.5 * DD["TFNEB"][tfilt] / DD["RHOTN"][tfilt]
            if np.any(fhfs):
                DD["PFN"][fhfs] = -DD["PFN"][fhfs]       # pol_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOPN"][fhfs] = -DD["RHOPN"][fhfs]   # rho_pol from -1 to 1 (-1 is inner LCFS)
                DD["TFN"][fhfs] = -DD["TFN"][fhfs]       # tor_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOTN"][fhfs] = -DD["RHOTN"][fhfs]   # rho_tor from -1 to 1 (-1 is inner LCFS)

    return DD


def standardize_diagnostic_data(procdata,newstruct=None,fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Converts all profiles from the various diagnostic data into
    the same coordinate system and format for improved clarity
    during further processing and profile fitting.

    :arg procdata: dict. Implementation-specific object containing processed extracted data.

    :kwarg newstruct: dict. Optional object in which formatted data will be stored.

    :returns: dict. Formatted object containing all profile data for additional processing.
    """
    PD = None
    DD = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(newstruct,dict):
        DD = newstruct
    else:
        DD = dict()

    if PD is not None:
        DD["POLFLAG"] = True if "POLFLAG" in PD and PD["POLFLAG"] else False

        DD["IDAFLAG"] = False
        diag = "IDA"
        quantities = ["NE","TE"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD,PD[diag],quantity=dtag)
                    if tempd is not None:
                        DD[diag][dtag] = tempd
                        DD["IDAFLAG"] = True

        # Gives absolute preference to IDA profiles if present, only electron density and temperature
        allflag = True    # Overrides preference for testing
        if allflag or not DD["IDAFLAG"]:
            diags = ["VTNC","VTNE","VTAD","VTAE","VTAF"]
            quantities = ["NE","TE"]
            for diag in diags:
                if diag in PD and PD[diag] is not None:
                    DD[diag] = dict()
                    for dtag in quantities:
                        if dtag in PD[diag] and PD[diag][dtag] is not None:
                            tempd = process_averaged_profile(PD,PD[diag],quantity=dtag)
                            if tempd is not None:
                                DD[diag][dtag] = tempd

            diag = "LIN"
            quantities = ["NE"]
            if diag in PD and PD[diag] is not None:
                DD[diag] = dict()
                for dtag in quantities:
                    if dtag in PD[diag] and PD[diag][dtag] is not None:
                        tempd = process_averaged_profile(PD,PD[diag],quantity=dtag)
                        if tempd is not None:
                            DD[diag][dtag] = tempd

            diag = "DCN"
            quantities = ["NE"]
            if diag in PD and PD[diag] is not None:
                DD[diag] = dict()
                for dtag in quantities:
                    if dtag in PD[diag] and PD[diag][dtag] is not None:
                        tempd = process_averaged_profile(PD,PD[diag],quantity=dtag)
                        if tempd is not None:
                            DD[diag][dtag] = tempd

            diag = "RMD"
            quantities = ["TE"]
            if diag in PD and PD[diag] is not None:
                DD[diag] = dict()
                for dtag in quantities:
                    if dtag in PD[diag] and PD[diag][dtag] is not None:
                        tempd = process_averaged_profile(PD,PD[diag],quantity=dtag)
                        if tempd is not None:
                            DD[diag][dtag] = tempd

        # The charge exchange diagnostic contains large amount of separate fields at AUG
        diag = "CX"
        quantities = ["TI","AF"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for key in PD[diag]:
                DD[diag][key] = dict()
                for dtag in quantities:
                    if dtag in PD[diag][key] and PD[diag][key][dtag] is not None:
                        tempd = process_averaged_profile(PD,PD[diag][key],quantity=dtag)
                        if tempd is not None:
                            DD[diag][key][dtag] = copy.deepcopy(tempd)

                            # Specify measured impurity, assume Ne if not given
                            if "Z" not in DD[diag][key]:
                                DD[diag][key]["A"] = float(PD[diag][key]["A"]) if "A" in PD[diag][key] and PD[diag][key]["A"] is not None else 20.0
                                DD[diag][key]["Z"] = float(PD[diag][key]["Z"]) if "Z" in PD[diag][key] and PD[diag][key]["Z"] is not None else 10.0

        diag = "MAGN"
        quantities = ["Q"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD,PD[diag],quantity=dtag)
                    if tempd is not None:
                        DD[diag][dtag] = tempd
                        DD[diag][dtag]["EQTYPE"] = PD[diag]["EQTYPE"] if "EQTYPE" in PD[diag] else "UNKNOWN"

    if fdebug:
        print("standardize_aug.py: standardize_diagnostic_data() completed.")

    return DD


def get_standardized_profile_data(diagdata,hfsflag=False,symflag=False):
    """
    JET-SPECIFIC FUNCTION
    Extracts the raw diagnostic data from the input object with the
    desired radial coordinate vector. This function is nearly
    general but requires the diagnostic data fields to be specified
    in a certain way.

    :arg diagdata: dict. Formatted object containing raw diagnostic profile data.

    :kwarg hfsflag: bool. Flag which toggle usage of high-field side data.

    :returns: (array, array, array, array).
        Vector of radial points, vector of errors in radial points corresponding to the
        radial points, vector of specified quantity from diagnostic data corresponding
        to the radial points, vector of errors in specified quantity from diagnostic
        data corresponding to the radial points.
    """
    DD = None
    if isinstance(diagdata,dict):
        DD = diagdata

    xval = None
    xerr = None
    yval = None
    yerr = None
    if DD is not None and "VAL" in DD and DD["VAL"] is not None and "AXID" in DD and DD["AXID"] is not None:
        idx = DD["AXID"]
        if "POLFLAG" in DD and DD["POLFLAG"] and "RHOPN" in RD:
            xval = -DD["RHOPN"][idx::-1] if hfsflag else DD["RHOPN"][idx:]
            if symflag:
                xval = np.abs(DD["RHOPN"])
            xerr = np.zeros(xval.shape)
            if "RHOPNEB" in DD and DD["RHOPNEB"] is not None:
                xerr = DD["RHOPNEB"][idx::-1] if hfsflag else DD["RHOPNEB"][idx:]
                if symflag:
                    xerr = np.abs(DD["RHOPNEB"])
        elif "RHOTN" in DD:
            xval = -DD["RHOTN"][idx::-1] if hfsflag else DD["RHOTN"][idx:]
            if symflag:
                xval = np.abs(DD["RHOTN"])
            xerr = np.zeros(xval.shape)
            if "RHOTNEB" in DD and DD["RHOTNEB"] is not None:
                xerr = DD["RHOTNEB"][idx::-1] if hfsflag else DD["RHOTNEB"][idx:]
                if symflag:
                    xerr = np.abs(DD["RHOTNEB"])
        yval = DD["VAL"][idx::-1] if hfsflag else DD["VAL"][idx:]
        if symflag:
            yval = DD["VAL"].copy()
        yerr = np.zeros(yval.shape)
        if "VALEB" in DD and DD["VALEB"] is not None:
            yerr = DD["VALEB"][idx::-1] if hfsflag else DD["VALEB"][idx:]
            if symflag:
                yerr = np.abs(DD["VALEB"])

    return (xval,xerr,yval,yerr)


def add_filtered_profile_data(tag,newstruct=None,xval=None,xerr=None,yval=None,yerr=None):
    """
    STANDARDIZED FUNCTION
    Adds input data into standardized fields for the specified
    processed profiles, creating these fields if they do not
    already exist.

    If an object is passed in via the newstruct argument, this
    function first checks whether or not the fields already
    exist in that object. If so, the new data is appended to
    the existing data.

    :arg tag: str. The specific quantity represented by the input plasma profile data, used to flag type of processing.

    :kwarg newstruct: dict. Optional object in which new fields and formatted data will be stored.

    :kwarg xval: array. Vector of radial points.

    :kwarg xerr: array. Vector of errors in radial points corresponding to the radial points.

    :kwarg yval: array. Vector of specified quantity from diagnostic data corresponding to the radial points.

    :kwarg yerr: array. Vector of errors in specified quantity from diagnostic data corresponding to the radial points.

    :returns: dict. Object with identical structure as input object except with input profile appended to standard fields.
    """
    otag = None
    SD = None
    if isinstance(tag,str) and tag:
        otag = tag.upper()
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()

    xx = np.array([])
    xe = np.array([])
    yy = np.array([])
    ye = np.array([])
    if isinstance(xval,(list,tuple,np.ndarray)):
        xx = np.array(xval).flatten()
    elif isinstance(xval,(int,float)):
        xx = np.array([float(xval)])
    if isinstance(xerr,(list,tuple,np.ndarray)):
        xe = np.array(xerr).flatten()
    elif isinstance(xerr,(int,float)):
        xe = np.array([float(xerr)])
    if isinstance(yval,(list,tuple,np.ndarray)):
        yy = np.array(yval).flatten()
    elif isinstance(yval,(int,float)):
        yy = np.array([float(yval)])
    if isinstance(yerr,(list,tuple,np.ndarray)):
        ye = np.array(yerr).flatten()
    elif isinstance(yerr,(int,float)):
        ye = np.array([float(yerr)])

    idx = None
    if otag is not None:
        idx = 0
        if otag+"RAW" not in SD:
            SD[otag+"RAW"] = np.array([])
        if otag+"RAWEB" not in SD:
            SD[otag+"RAWEB"] = np.array([])
        if otag+"RAWX" not in SD:
            SD[otag+"RAWX"] = np.array([])
        if otag+"RAWXEB" not in SD:
            SD[otag+"RAWXEB"] = np.array([])
        if otag+"DMAP" not in SD:
            SD[otag+"DMAP"] = np.array([],dtype=np.int64)
        elif SD[otag+"DMAP"].size > 0:
            idx = np.rint(np.nanmax(SD[otag+"DMAP"])) + 1

        if xx.size > 0 and xx.size == xe.size and xx.size == yy.size and xe.size == ye.size:
            SD[otag+"RAW"] = np.hstack((SD[otag+"RAW"],yy))
            SD[otag+"RAWEB"] = np.hstack((SD[otag+"RAWEB"],ye))
            SD[otag+"RAWX"] = np.hstack((SD[otag+"RAWX"],xx))
            SD[otag+"RAWXEB"] = np.hstack((SD[otag+"RAWXEB"],xe))
            SD[otag+"DMAP"] = np.hstack((SD[otag+"DMAP"],np.full(xx.shape,idx)))

    return SD


def filter_ne(diagdata,newstruct=None,edgeflag=False,hfsflag=False,symflag=False,ftflag=True,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed electron density profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed electron density profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ftflag: bool. Flag to specify that input profile belongs to a time window within the flat-top phase.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "NE"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag,symflag=symflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Region-specific filters
        # Technically does nothing since Li-beam diagnostic at JET is not currently coupled to this tool
        if edgeflag:
            status = 0       # Dummy operation just to conserve structure
        else:
            # NE data in core can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                cfilt = (np.abs(darr[0,:]) <= 0.7)
                vallim = 0.7 * np.mean(darr[2,:][cfilt]) if np.any(cfilt) else 0.0
                rfilt = (np.abs(darr[0,:]) <= 0.7)
                lfilt = (darr[2,:] < vallim)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

            # NE data near pedestal can be corrupted with large errors due to presence of ELMs
#            if darr.size > 0:
#                efilt = np.all([np.abs(darr[0,:]) >= 0.75,np.abs(darr[0,:]) <= 0.9,darr[2,:] < 1.0e18,darr[3,:] > 1.0e18],axis=0)
#                if np.any(efilt):
#                    darr[3,:][efilt] = 0.7 * darr[3,:][efilt]

            # NE data near edge can be corrupted with large errors, reduce errors to retain use of information
#            if darr.size > 0:
#                pfilt = np.all([np.abs(darr[0,:]) >= 0.9,darr[2,:] < 1.0e18,darr[3,:] > 1.0e18],axis=0)
#                if np.any(pfilt):
#                    darr[3,:][pfilt] = 0.75 * darr[2,:][pfilt]

            # NE data with extremely large errors are typically salvageable
#            if darr.size > 0:
#                lfilt = (darr[3,:] > darr[2,:])
#                ufilt = (darr[2,:] > 1.0e19)
#                darr = ptools.custom_filter_2d(darr,filters=[lfilt,ufilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_te(diagdata,newstruct=None,edgeflag=False,hfsflag=False,symflag=False,ftflag=True,trustflag=False,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed electron temperature profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed electron temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ftflag: bool. Flag to specify that input profile belongs to a time window within the flat-top phase.

    :kwarg trustflag: bool. Flag to specify that input profile is high-quality data, employs reduced filtering.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "TE"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag,symflag=symflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Region-specific filters
        # Although no edge-specific TE diagnostics at JET, this setting is also used for ECE measurements due to better quality data
        if edgeflag or trustflag:
            # Edge TE data at edge can be corrupted with large values in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) >= 0.95)
                ufilt = (darr[2,:] > 1000)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,rfilt],operation='and',finvert=True)

        else:
            # Core TE data in core can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 0.7)
                vallim = 0.5 * np.mean(darr[2,:][rfilt]) if np.any(rfilt) else 0.0
                lfilt = (darr[2,:] < vallim)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

            # Core TE data at edge can be corrupted with large values in automated pre-processing routines
#            if darr.size > 0:
#                rfilt = (np.abs(darr[0,:]) >= 0.95)
#                ufilt = (darr[2,:] > 1000)
#                darr = ptools.custom_filter_2d(darr,filters=[ufilt,rfilt],operation='and',finvert=True)

            # Core TE error at pedestal top greatly influenced by ELMs, ELM filtering not useful due to limited time resolution
#            if darr.size > 0:
#                rfilt = np.all([np.abs(darr[0,:]) >= 0.7,np.abs(darr[0,:]) <= 0.95],axis=0)
#                efilt = (darr[3,:] / darr[2,:] >= 0.3)
#                pfilt = np.all([rfilt,efilt],axis=0)
#                if np.any(pfilt):
#                    darr[3,:][pfilt] = 0.4 * darr[3,:][pfilt]

            # Core TE data near edge can be corrupted with large errors, reduce errors to retain use of information
#            if darr.size > 0:
#                pfilt = np.all([np.abs(darr[0,:]) >= 0.9,darr[2,:] < 5.0e2,darr[3,:] > 1.0e3],axis=0)
#                if np.any(pfilt):
#                    darr[3,:][pfilt] = 0.75 * darr[2,:][pfilt]

            # Core TE data with extremely large errors are usually corrupted data
#            if darr.size > 0:
#                lfilt = (darr[3,:] > darr[2,:])
#                ufilt = (darr[2,:] > 500)
#                darr = ptools.custom_filter_2d(darr,filters=[lfilt,ufilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_ti(diagdata,newstruct=None,edgeflag=False,hfsflag=False,symflag=False,ampflag=False,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed ion temperature profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed ion temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ampflag: bool. Flag to specify artifical amplification factor of errors, usually based on wall material.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "TI"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag,symflag=symflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # TI data can be corrupted with extremely high values
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.0e6,upper_equality=False,indices=[2])

        # Region-specific filters
        if edgeflag:
            # Edge TI sometimes corrupted with large values due to stray radiation, filtered using reported error
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,upper_bound=1000,upper_equality=False,indices=[3])

            # Edge TI data inside pedestal can be corrupted with small values
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 0.85)
                vallim = 0.5 * np.mean(darr[2,:][rfilt]) if np.any(rfilt) else 0.0
                lfilt = (darr[2,:] < vallim)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

            # Edge TI error at pedestal top greatly influenced by ELMs, ELM filtering not useful due to limited time resolution
#            if darr.size > 0:
#                rfilt = np.all([np.abs(darr[0,:]) >= 0.8,np.abs(darr[0,:]) <= 0.9],axis=0)
#                efilt = (darr[3,:] >= 0.25 * darr[2,:])
#                pfilt = np.all([rfilt,efilt],axis=0)
#                if np.any(pfilt):
#                    darr[3,:][pfilt] = 0.4 * darr[3,:][pfilt]

            # Edge TI error suspected to be underestimated, especially away from the edge
#            if darr.size > 0:
#                rfilt = (darr[3,:] < 0.2 * darr[2,:])
#                if np.any(rfilt):
#                    efac = 0.3 # 0.5 if ampflag else 0.3
#                    darr[3,:][rfilt] = darr[3,:][rfilt] + np.abs(0.975 - darr[0,:][rfilt]) * efac * darr[2,:][rfilt]
        else:
            # Core TI error known to be underestimated where it overlaps with edge measurements
#            if darr.size > 0:
#                rfilt = np.all([darr[0,:] >= 0.7,darr[3,:] < 0.2 * darr[2,:]],axis=0)
#                if np.any(rfilt):
#                    efac = 0.175 if ampflag else 0.2
#                    darr[3,:][rfilt] = darr[3,:][rfilt] + efac * darr[2,:][rfilt]

            # Core TI error can be overestimated in the central core
#            if darr.size > 0:
#                rfilt = np.all([darr[0,:] < 0.6,darr[3,:] > 0.10 * darr[2,:]],axis=0)
#                if np.any(rfilt):
#                    efac = 0.7
#                    darr[3,:][rfilt] = (darr[0,:][rfilt] + 0.6) * efac * darr[3,:][rfilt]

            # Core TI error can be underestimated in the central core
#            if darr.size > 0:
#                rfilt = np.all([darr[0,:] < 0.7,darr[3,:] < 0.05 * darr[2,:]],axis=0)
#                if np.any(rfilt):
#                    efac = 0.3
#                    darr[3,:][rfilt] = 2.0 * darr[3,:][rfilt]

            # Core TI data can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 1.05)
                lfilt = (darr[2,:] < 50)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_timp(diagdata,newstruct=None,edgeflag=False,hfsflag=False,symflag=False,ampflag=False,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed impurity ion temperature profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed impurity ion temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ampscale: bool. 

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "TIMP"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag,symflag=symflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # TIMP data can be corrupted with extremely high values
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.0e6,upper_equality=False,indices=[2])

        # Region-specific filters
        if edgeflag:
            # Edge TIMP sometimes corrupted with large values due to stray radiation, filtered using reported error
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,upper_bound=1000,upper_equality=False,indices=[3])

            # Edge TIMP data inside pedestal can be corrupted with small values
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 0.85)
                vallim = 0.5 * np.mean(darr[2,:][rfilt]) if np.any(rfilt) else 0.0
                lfilt = (darr[2,:] < vallim)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

            # Edge TIMP error at pedestal top greatly influenced by ELMs, ELM filtering not useful due to limited time resolution
#            if darr.size > 0:
#                rfilt = np.all([np.abs(darr[0,:]) >= 0.8,np.abs(darr[0,:]) <= 0.95],axis=0)
#                efilt = (darr[3,:] >= 0.25 * darr[2,:])
#                pfilt = np.all([rfilt,efilt],axis=0)
#                if np.any(pfilt):
#                    darr[3,:][pfilt] = 0.4 * darr[3,:][pfilt]

            # Edge TIMP error suspected to be underestimated, especially away from the edge
#            if darr.size > 0:
#                rfilt = (darr[3,:] < 0.2 * darr[2,:])
#                if np.any(rfilt):
#                    efac = 0.3 # 0.5 if ampflag else 0.3
#                    darr[3,:][rfilt] = darr[3,:][rfilt] + np.abs(0.975 - darr[0,:][rfilt]) * efac * darr[2,:][rfilt]
        else:
            # Core TIMP measurements near separatrix are typically of very poor quality
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,upper_bound=0.9,upper_equality=True,indices=[0])

            # Core TIMP error known to be underestimated, especially closer to the edge
#            if darr.size > 0:
#                rfilt = np.all([darr[0,:] >= 0.7,darr[3,:] < 0.2 * darr[2,:]],axis=0)
#                if np.any(rfilt):
#                    efac = 0.175 if ampflag else 0.2
#                    darr[3,:][rfilt] = darr[3,:][rfilt] + efac * darr[2,:][rfilt]

            # Core TIMP error can be overestimated in the central core
#            if darr.size > 0:
#                rfilt = np.all([darr[0,:] < 0.6,darr[3,:] > 0.125 * darr[2,:]],axis=0)
#                if np.any(rfilt):
#                    efac = 0.7
#                    darr[3,:][rfilt] = (darr[0,:][rfilt] + 0.6) * efac * darr[3,:][rfilt]

            # Core TIMP error can be underestimated in the central core
#            if darr.size > 0:
#                rfilt = np.all([darr[0,:] < 0.7,darr[3,:] < 0.05 * darr[2,:]],axis=0)
#                if np.any(rfilt):
#                    efac = 0.3
#                    darr[3,:][rfilt] = 2.0 * darr[3,:][rfilt]

            # Core TIMP data can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 1.05)
                lfilt = (darr[2,:] < 50)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_af(diagdata,newstruct=None,edgeflag=False,hfsflag=False,symflag=False,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed rotational angular frequency profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed rotational angular frequency profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "AF"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag,symflag=symflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Artificially augment errors that seem unreasonably small
        if darr.size > 0:
            rfilt = (darr[3,:] < 0.05 * np.abs(darr[2,:]))
            if np.any(rfilt):
                darr[3,:][rfilt] = darr[3,:][rfilt] + 0.05 * np.abs(darr[2,:][rfilt])

        # Region-specific filters
        if edgeflag:
            # Edge AF data away from the edge tend to be untrustworthy
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,lower_bound=0.8,lower_equality=True,indices=[0])
        else:
            status = 0       # Dummy operation just to conserve structure

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_nimp(diagdata,newstruct=None,zimp=None,edgeflag=False,hfsflag=False,symflag=False,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed impurity ion density profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed impurity ion density profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    zz = 10.0
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(zimp,(float,int)) and float(zimp) >= 1.0:
        zz = float(zimp)
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "NIMP"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag,symflag=symflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Region-specific filters
        if edgeflag:
            # Edge NIMP data inside core region is typically untrustworthy due to high noise levels
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,lower_bound=0.8,upper_bound=1.0,lower_equality=False,upper_equality=True,indices=[0])

            # Edge NIMP data can be corrupted by zeros, filtered using flexible limit based on impurity charge
            if darr.size > 0:
                lb = 1.0e17 / zimp
                darr = ptools.bounded_filter_2d(darr,lower_bound=lb,lower_equality=True,indices=[2])
        else:
            # Core NIMP data can contain unreasonably large values, filtered using flexible limit based on impurity charge
            if darr.size > 0:
                ub = 1.0e18 / zimp
                rfilt = (np.abs(darr[0,:]) > 0.8)
                ufilt = (darr[2,:] >= ub)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,rfilt],operation='and',finvert=True)

            # Central core NIMP data can contain large values from neoclassical transport, hard to account for in fitting
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,lower_bound=0.05,lower_equality=True,indices=[0])

        # Error adjustment - too small error bars yields unreasonable profiles or causes numerical instability in GP fit
        if darr.size > 0:
            lfilt = (darr[3,:] < 3.0e17 / zimp)
            if np.any(lfilt):
                darr[3,:][lfilt] = darr[3,:][lfilt] + 1.0e17 / zimp

        # Error adjustment - too large error bars are generated by previous step for low density, low-Z impurities
        if darr.size > 0:
            lfilt = (darr[3,:] > 0.3 * darr[2,:])
            if np.any(lfilt):
                darr[3,:][lfilt] = 0.3 * darr[2,:][lfilt]

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            SD = add_filtered_profile_data(dtag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_q(diagdata,newstruct=None,conflag=False,hfsflag=False,corrflag=False,userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed safety factor profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg conflag: bool. Flag to specify that input profile comes from a constrained equilibrium calculation.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg corrflag: bool. Flag to specify that input profile is corrected data, with the bias correction applied.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale,(int,float)) and float(userscale) >= 0.0:
        sc = float(userscale)
    dtag = "Q" if not corrflag else "CQ"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Addition of heuristic Q error if it came from a constrained equilibrium or is unreasonably small
        if darr.size > 0:
            if conflag or np.average(darr[3,:]) <= 0.05:
                darr[3,:] = darr[3,:] + 0.05

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.00,upper_equality=True,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten() * sc
            SD = add_filtered_profile_data(dtag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 1

    return (SD,status)


def filter_standardized_diagnostic_data(diagdata,newstruct=None,userscales=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Applies pre-defined filters to the raw diagnostic data, optimized
    such to improve the quality of the fitting routine.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg userscales: dict. Optional object containing user-defined error multipliers for the various quantities.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    DD = None
    SD = None
    MD = dict()
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscales,dict):
        MD = userscales
    fe = 0

    SD["NEDIAG"] = None
    SD["NIDIAG"] = None
    SD["TEDIAG"] = None
    SD["TIDIAG"] = None
    SD["AFDIAG"] = None
    SD["TIMPDIAG"] = None
    SD["QDIAG"] = None
    SD["NIMPLIST"] = []

    SD["ETIDIAG"] = None
    SD["EAFDIAG"] = None
    SD["ETIMPDIAG"] = None
    SD["ENIMPLIST"] = []

    SD["NEEBMULT"] = MD["NE"] if "NE" in MD else None
    SD["TEEBMULT"] = MD["TE"] if "TE" in MD else None
    SD["NIEBMULT"] = MD["NI"] if "NI" in MD else None
    SD["TIEBMULT"] = MD["TI"] if "TI" in MD else None
    SD["NIMPEBMULT"] = MD["NIMP"] if "NIMP" in MD else None
    SD["TIMPEBMULT"] = MD["TIMP"] if "TIMP" in MD else None
    SD["AFEBMULT"] = MD["AF"] if "AF" in MD else None
    SD["QEBMULT"] = MD["Q"] if "Q" in MD else None

    if DD is not None:
        SD["CS_DIAG"] = "RHOPOLN" if DD["POLFLAG"] else "RHOTORN"
        ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

        code = "IDA"
        ttag = "TRUSTED"
        if code in DD and DD[code] is not None:
            if "NE" in DD[code] and DD[code]["NE"] is not None:
                SD["NE"+ttag+"RHOTN"] = DD[code]["NE"]["RHOTN"].copy()
                SD["NE"+ttag+"RHOTNEB"] = DD[code]["NE"]["RHOTNEB"].copy()
                SD["NE"+ttag] = DD[code]["NE"]["VAL"].copy()
                SD["NE"+ttag+"EB"] = DD[code]["NE"]["VALEB"].copy()
                SD["NE"+ttag+"GRAD"] = DD[code]["NE"]["DVAL"].copy() if "DVAL" in DD[code]["NE"] else None
                SD["NE"+ttag+"GRADEB"] = DD[code]["NE"]["DVALEB"].copy() if "DVALEB" in DD[code]["NE"] else None
                SD["NE"+ttag+"DIAG"] = ["IDA"]
                SD["NE"+ttag+"DMAP"] = [0] * len(DD[code]["NE"]["VAL"])
            if "TE" in DD[code] and DD[code]["TE"] is not None:
                SD["TE"+ttag+"RHOTN"] = DD[code]["TE"]["RHOTN"].copy()
                SD["TE"+ttag+"RHOTNEB"] = DD[code]["TE"]["RHOTNEB"].copy()
                SD["TE"+ttag] = DD[code]["TE"]["VAL"].copy()
                SD["TE"+ttag+"EB"] = DD[code]["TE"]["VALEB"].copy()
                SD["TE"+ttag+"GRAD"] = DD[code]["TE"]["DVAL"].copy() if "DVAL" in DD[code]["TE"] else None
                SD["TE"+ttag+"GRADEB"] = DD[code]["TE"]["DVALEB"].copy() if "DVALEB" in DD[code]["TE"] else None
                SD["TE"+ttag+"DIAG"] = ["IDA"]
                SD["TE"+ttag+"DMAP"] = [0] * len(DD[code]["TE"]["VAL"])

        diag = "VTNC"
        if diag in DD and DD[diag] is not None and fe == 0:
            nstatus = 0
            tstatus = 0
            if "NE" in DD[diag] and DD[diag]["NE"] is not None:
                (SD,nstatus) = filter_ne(DD[diag]["NE"],newstruct=SD,ftflag=ftflag,userscale=SD["NEEBMULT"])
                if nstatus != 0:
                    if SD["NEDIAG"] is None:
                        SD["NEDIAG"] = []
                    SD["NEDIAG"].append(diag)
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD,tstatus) = filter_te(DD[diag]["TE"],newstruct=SD,ftflag=ftflag,userscale=SD["TEEBMULT"])
                if tstatus != 0:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)

        diag = "VTNE"
        if diag in DD and DD[diag] is not None and fe == 0:
            nstatus = 0
            tstatus = 0
            if "NE" in DD[diag] and DD[diag]["NE"] is not None:
                (SD,nstatus) = filter_ne(DD[diag]["NE"],newstruct=SD,ftflag=ftflag,edgeflag=True,userscale=SD["NEEBMULT"])
                if nstatus != 0:
                    if SD["NEDIAG"] is None:
                        SD["NEDIAG"] = []
                    SD["NEDIAG"].append(diag)
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD,tstatus) = filter_te(DD[diag]["TE"],newstruct=SD,ftflag=ftflag,edgeflag=True,userscale=SD["TEEBMULT"])
                if tstatus != 0:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)

        diags = ["VTAD","VTAE","VTAF"]
        for diag in diags:
            if diag in DD and DD[diag] is not None and fe == 0:
                nstatus = 0
                tstatus = 0
                if "NE" in DD[diag] and DD[diag]["NE"] is not None:
                    (SD,nstatus) = filter_ne(DD[diag]["NE"],newstruct=SD,ftflag=ftflag,edgeflag=True,userscale=SD["NEEBMULT"])
                    if nstatus != 0:
                        if SD["NEDIAG"] is None:
                            SD["NEDIAG"] = []
                        SD["NEDIAG"].append(diag)
                if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                    (SD,tstatus) = filter_te(DD[diag]["TE"],newstruct=SD,ftflag=ftflag,edgeflag=True,userscale=SD["TEEBMULT"])
                    if tstatus != 0:
                        if SD["TEDIAG"] is None:
                            SD["TEDIAG"] = []
                        SD["TEDIAG"].append(diag)

        diag = "RMD"
        if diag in DD and DD[diag] is not None and fe == 0:
            tstatus = 0
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD,tstatus) = filter_te(DD[diag]["TE"],newstruct=SD,ftflag=ftflag,trustflag=True,userscale=SD["TEEBMULT"])
                if tstatus != 0:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)

        diag = "CX"
        if diag in DD and DD[diag] is not None:
            edgetihere = False
            for key in DD[diag]:
                if re.match('^C[MP]Z$',key,flags=re.IGNORECASE):
                    use_this = True
                    # CMZ edge diagnostic trusted more than all others
                    if "CMZ" in DD[diag] and DD[diag]["CMZ"] is not None:
                        use_this = True if key == "CMZ" else False
                    if use_this:
                        if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None and np.abs(float(DD[diag][key]["Z"]) - 1.0) < 0.1:
                            if "TI" in DD[diag][key]:
                                (SD,status) = filter_ti(DD[diag][key]["TI"],newstruct=SD,edgeflag=True,userscale=SD["TIMPEBMULT"])
                                if status != 0:
                                    if SD["ETIDIAG"] is None:
                                        SD["ETIDIAG"] = []
                                    SD["ETIDIAG"].append(key.upper())
                            if "AF" in DD[diag][key]:
                                (SD,status) = filter_af(DD[diag][key]["AF"],newstruct=SD,edgeflag=True,userscale=SD["AFEBMULT"])
                                if status != 0:
                                    if SD["EAFDIAG"] is None:
                                        SD["EAFDIAG"] = []
                                    SD["EAFDIAG"].append(key.upper())
                        else:
                            if "TI" in DD[diag][key]:
                                (SD,status) = filter_timp(DD[diag][key]["TI"],newstruct=SD,edgeflag=True,userscale=SD["TIMPEBMULT"])
                                if status != 0:
                                    if SD["ETIMPDIAG"] is None:
                                        SD["ETIMPDIAG"] = []
                                    SD["ETIMPDIAG"].append(key.upper())
                            if "AF" in DD[diag][key]:
                                (SD,status) = filter_af(DD[diag][key]["AF"],newstruct=SD,edgeflag=True,userscale=SD["AFEBMULT"])
                                if status != 0:
                                    if SD["EAFDIAG"] is None:
                                        SD["EAFDIAG"] = []
                                    SD["EAFDIAG"].append(key.upper())
                            if "NIMP" in DD[diag][key]:
                                idx = len(SD["ENIMPLIST"])
                                if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None:
                                    for jj in np.arange(0,len(SD["ENIMPLIST"])):
                                        if np.abs(SD["ENIMPLIST"][jj]["ZIMP"] - DD[diag][key]["Z"]) < 0.01:
                                            idx = jj
                                temp = None if idx >= len(SD["ENIMPLIST"]) else copy.deepcopy(SD["ENIMPLIST"][idx])
                                (ND,nstatus) = filter_nimp(DD[diag][key]["NIMP"],newstruct=temp,zimp=float(DD[diag][key]["Z"]),edgeflag=True,userscale=SD["NIMPEBMULT"])
                                if nstatus != 0:
                                    ND["AIMP"] = float(DD[diag][key]["A"])
                                    ND["ZIMP"] = float(DD[diag][key]["Z"])
                                    if idx >= len(SD["ENIMPLIST"]):
                                        ND["NIMPDIAG"] = []
                                        ND["NIMPDIAG"].append(key.upper())
                                        SD["ENIMPLIST"].append(ND)
                                    else:
                                        ND["NIMPDIAG"] = SD["ENIMPLIST"][idx]["NIMPDIAG"]
                                        ND["NIMPDIAG"].append(key.upper())
                                        SD["ENIMPLIST"][idx] = copy.deepcopy(ND)
            for key in DD[diag]:
                if not re.match('^C[MP]Z$',key,flags=re.IGNORECASE):
                    use_this = True
                    # CEZ core diagnostic trusted more than all others
                    if "CEZ" in DD[diag] and DD[diag]["CEZ"] is not None:
                        use_this = True if key == "CEZ" else False
                    if use_this:
                        if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None and np.abs(float(DD[diag][key]["Z"]) - 1.0) < 0.1:
                            if "TI" in DD[diag][key] and DD[diag][key]["TI"] is not None:
                                (SD,tstatus) = filter_ti(DD[diag][key]["TI"],newstruct=SD,userscale=SD["TIEBMULT"])
                                if tstatus != 0:
                                    if SD["TIDIAG"] is None:
                                        SD["TIDIAG"] = []
                                    SD["TIDIAG"].append(key.upper())
                            if "AF" in DD[diag][key] and DD[diag][key]["AF"] is not None:
                                (SD,fstatus) = filter_af(DD[diag][key]["AF"],newstruct=SD,userscale=SD["AFEBMULT"])
                                if fstatus != 0:
                                    if SD["AFDIAG"] is None:
                                        SD["AFDIAG"] = []
                                    SD["AFDIAG"].append(key.upper())
                        else:
                            if "TI" in DD[diag][key] and DD[diag][key]["TI"] is not None:
#                                fcw = False if "WALLMAT" in SD and re.match(r'^C$',SD["WALLMAT"],flags=re.IGNORECASE) else True
                                (SD,tstatus) = filter_timp(DD[diag][key]["TI"],newstruct=SD,userscale=SD["TIMPEBMULT"])
                                if tstatus != 0:
                                    if SD["TIMPDIAG"] is None:
                                        SD["TIMPDIAG"] = []
                                    SD["TIMPDIAG"].append(key.upper())
                            if "AF" in DD[diag][key] and DD[diag][key]["AF"] is not None:
                                (SD,fstatus) = filter_af(DD[diag][key]["AF"],newstruct=SD,userscale=SD["AFEBMULT"])
                                if fstatus != 0:
                                    if SD["AFDIAG"] is None:
                                        SD["AFDIAG"] = []
                                    SD["AFDIAG"].append(key.upper())
                            if "NIMP" in DD[diag][key] and DD[diag][key]["NIMP"] is not None:
                                idx = len(SD["NIMPLIST"])
                                if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None:
                                    for jj in np.arange(0,len(SD["NIMPLIST"])):
                                        if np.abs(SD["NIMPLIST"][jj]["ZIMP"] - DD[diag][key]["Z"]) < 0.01:
                                            idx = jj
                                temp = None if idx >= len(SD["NIMPLIST"]) else copy.deepcopy(SD["NIMPLIST"][idx])
                                (ND,nstatus) = filter_nimp(DD[diag][key]["NIMP"],newstruct=temp,zimp=float(DD[diag][key]["Z"]),userscale=SD["NIMPEBMULT"])
                                if nstatus != 0:
                                    ND["AIMP"] = float(DD[diag][key]["A"])
                                    ND["ZIMP"] = float(DD[diag][key]["Z"])
                                    if idx >= len(SD["NIMPLIST"]):
                                        ND["NIMPDIAG"] = []
                                        ND["NIMPDIAG"].append(key.upper())
                                        SD["NIMPLIST"].append(ND)
                                    else:
                                        ND["NIMPDIAG"] = SD["NIMPLIST"][idx]["NIMPDIAG"]
                                        ND["NIMPDIAG"].append(key.upper())
                                        SD["NIMPLIST"][idx] = copy.deepcopy(ND)

        diag = "MAGN"
        if diag in DD and DD[diag] is not None:
            if "Q" in DD[diag] and DD[diag]["Q"] is not None:
                (SD,qstatus) = filter_q(DD[diag]["Q"],newstruct=SD,userscale=SD["QEBMULT"])
                if qstatus != 0:
                    if SD["QDIAG"] is None:
                        SD["QDIAG"] = []
                    SD["QDIAG"].append(DD[diag]["Q"]["EQTYPE"].upper())

            if "QDIAG" in SD and SD["QDIAG"] is not None and len(SD["QDIAG"]) > 0:
                SD["IQRAWX"] = copy.deepcopy(SD["QRAWX"])
                SD["IQRAWXEB"] = copy.deepcopy(SD["QRAWXEB"])
                SD["IQRAW"] = 1.0 / SD["QRAW"]
                SD["IQRAWEB"] = SD["QRAWEB"] / np.power(SD["QRAW"],2.0)
                SD["IQDIAG"] = copy.deepcopy(SD["QDIAG"])
                SD["IQDMAP"] = copy.deepcopy(SD["QDMAP"])

    if fdebug:
        print("standardize_aug.py: filter_standardized_diagnostic_data() completed.")

    return SD


def combine_edge_diagnostic_data(stddata,fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Combines any edge and core diagnostic profile data of the same quantity,
    applying any additional checks and cross-diagnostic filters.

    :arg stddata: dict. Standardized object containing separated core and edge profiles.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    SD = None
    if isinstance(stddata,dict):
        SD = stddata

    if SD is not None:

        if "ETIRAW" in SD and SD["ETIRAW"].size > 0:
            if "TIRAW" not in SD:
                # Pasting of TE core data segment yields better estimation of TI profile if only edge data exists
                emptyte = True
                if "TERAW" in SD and SD["TERAW"].size > 0:
                    cfilt = np.all([SD["TERAWX"] >= 0.1,SD["TERAWX"] <= 0.6],axis=0)
                    if np.any(cfilt):
                        emptyte = False
                        SD["TIRAWX"] = SD["TERAWX"][cfilt][::2]
                        SD["TIRAWXEB"] = SD["TERAWXEB"][cfilt][::2]
                        SD["TIRAW"] = SD["TERAW"][cfilt][::2]
                        SD["TIRAWEB"] = (1.6 - SD["TERAWX"][cfilt][::2]) * SD["TERAWEB"][cfilt][::2]
                        SD["TIDIAG"] = []
                        SD["TIDMAP"] = np.full(SD["TIRAWX"].shape,len(SD["TIDIAG"])).flatten()
                        SD["TIDIAG"].append("TICC")
                if emptyte:
                    SD["TIRAW"] = np.array([])
                    SD["TIRAWEB"] = np.array([])
                    SD["TIRAWX"] = np.array([])
                    SD["TIRAWXEB"] = np.array([])
                    SD["TIDMAP"] = np.array([])
                    SD["TIDIAG"] = []
            SD["TIRAW"] = np.hstack((SD["TIRAW"],SD["ETIRAW"]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"],SD["ETIRAWEB"]))
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"],SD["ETIRAWX"]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"],SD["ETIRAWXEB"]))
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"],SD["ETIDMAP"] + len(SD["TIDIAG"])))
            SD["TIDIAG"].extend(SD["ETIDIAG"])

        if "ETIMPRAW" in SD and SD["ETIMPRAW"].size > 0:
            if "TIMPRAW" not in SD:
                # Pasting of TE core data segment yields better estimation of TI profile if only edge data exists
                emptyte = True
                if "TERAW" in SD and SD["TERAW"].size > 0:
                    cfilt = np.all([SD["TERAWX"] >= 0.1,SD["TERAWX"] <= 0.6],axis=0)
                    if np.any(cfilt):
                        emptyte = False
                        SD["TIMPRAWX"] = SD["TERAWX"][cfilt][::2]
                        SD["TIMPRAWXEB"] = SD["TERAWXEB"][cfilt][::2]
                        SD["TIMPRAW"] = SD["TERAW"][cfilt][::2]
                        SD["TIMPRAWEB"] = (1.6 - SD["TERAWX"][cfilt][::2]) * SD["TERAWEB"][cfilt][::2]
                        SD["TIMPDIAG"] = []
                        SD["TIMPDMAP"] = np.full(SD["TIMPRAWX"].shape,len(SD["TIMPDIAG"])).flatten()
                        SD["TIMPDIAG"].append("TICC")
                if emptyte:
                    SD["TIMPRAW"] = np.array([])
                    SD["TIMPRAWEB"] = np.array([])
                    SD["TIMPRAWX"] = np.array([])
                    SD["TIMPRAWXEB"] = np.array([])
                    SD["TIMPDMAP"] = np.array([])
                    SD["TIMPDIAG"] = []
            SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"],SD["ETIMPRAW"]))
            SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"],SD["ETIMPRAWEB"]))
            SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"],SD["ETIMPRAWX"]))
            SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"],SD["ETIMPRAWXEB"]))
            SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"],SD["ETIMPDMAP"] + len(SD["TIMPDIAG"])))
            SD["TIMPDIAG"].extend(SD["ETIMPDIAG"])

        if "EAFRAW" in SD and SD["EAFRAW"].size > 0 and "AFRAW" in SD:
            # Only include edge angular frequency measurements if they are not excessively large
            if SD["AFRAW"].size > 0 and np.sqrt(np.mean(np.power(SD["AFRAW"],2.0))) > np.sqrt(np.mean(np.power(SD["EAFRAW"],2.0))):
                if "AFDIAG" not in SD or SD["AFDIAG"] is None:
                    SD["AFDIAG"] = []
                SD["AFRAW"] = np.hstack((SD["AFRAW"],SD["EAFRAW"]))
                SD["AFRAWEB"] = np.hstack((SD["AFRAWEB"],SD["EAFRAWEB"]))
                SD["AFRAWX"] = np.hstack((SD["AFRAWX"],SD["EAFRAWX"]))
                SD["AFRAWXEB"] = np.hstack((SD["AFRAWXEB"],SD["EAFRAWXEB"]))
                SD["AFDMAP"] = np.hstack((SD["AFDMAP"],SD["EAFDMAP"] + len(SD["AFDIAG"])))
                SD["AFDIAG"].extend(SD["EAFDIAG"])

        for ii in np.arange(0,len(SD["ENIMPLIST"])):
            if "NIMPRAW" in SD["ENIMPLIST"][ii] and SD["ENIMPLIST"][ii]["NIMPRAW"] is not None:
                for jj in np.arange(0,len(SD["NIMPLIST"])):
                    if "NIMPRAW" in SD["NIMPLIST"][jj] and SD["NIMPLIST"][jj]["NIMPRAW"] is not None:
                        # Core charge exchange measurements at edge usually polluted with bad impurity measurements, edge is cleaner
                        if SD["NIMPLIST"][jj]["NIMPRAW"].size > 0 and np.abs(SD["ENIMPLIST"][ii]["ZIMP"] - SD["NIMPLIST"][jj]["ZIMP"]) < 0.01:
                            cfilt = (SD["NIMPLIST"][jj]["NIMPRAW"] >= 0.5 * np.mean(SD["ENIMPLIST"][ii]["NIMPRAW"]))
                            SD["NIMPLIST"][jj]["NIMPRAW"] = SD["NIMPLIST"][jj]["NIMPRAW"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPRAWEB"] = SD["NIMPLIST"][jj]["NIMPRAWEB"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPRAWX"] = SD["NIMPLIST"][jj]["NIMPRAWX"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPRAWXEB"] = SD["NIMPLIST"][jj]["NIMPRAWXEB"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPDMAP"] = SD["NIMPLIST"][jj]["NIMPDMAP"][cfilt] if np.any(cfilt) else np.array([])

                for jj in np.arange(0,len(SD["NIMPLIST"])):
                    # Perhaps edge information should only be used if corresponding core information exists...
                    if "NIMPRAW" in SD["NIMPLIST"][jj] and SD["NIMPLIST"][jj]["NIMPRAW"] is not None:
                        if np.abs(SD["ENIMPLIST"][ii]["ZIMP"] - SD["NIMPLIST"][jj]["ZIMP"]) < 0.01:
                            SD["NIMPLIST"][jj]["NIMPRAW"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAW"],SD["ENIMPLIST"][ii]["NIMPRAW"]))
                            SD["NIMPLIST"][jj]["NIMPRAWEB"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAWEB"],SD["ENIMPLIST"][ii]["NIMPRAWEB"]))
                            SD["NIMPLIST"][jj]["NIMPRAWX"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAWX"],SD["ENIMPLIST"][ii]["NIMPRAWX"]))
                            SD["NIMPLIST"][jj]["NIMPRAWXEB"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAWXEB"],SD["ENIMPLIST"][ii]["NIMPRAWXEB"]))
                            SD["NIMPLIST"][jj]["NIMPDMAP"] = np.hstack((SD["NIMPLIST"][jj]["NIMPDMAP"],SD["ENIMPLIST"][ii]["NIMPDMAP"] + len(SD["NIMPLIST"][jj]["NIMPDIAG"])))
                            SD["NIMPLIST"][jj]["NIMPDIAG"].extend(SD["ENIMPLIST"][ii]["NIMPDIAG"])

        # RAWEDGEFLAG option not documented as is unsupported - keeps filtered edge diagnostic data in output dict if True
        if not ("RAWEDGEFLAG" in SD and SD["RAWEDGEFLAG"]):
            dlist = ["ETIRAW","ETIRAWEB","ETIRAWX","ETIRAWXEB","ETIDMAP", \
                     "EAFRAW","EAFRAWEB","EAFRAWX","EAFRAWXEB","EAFDMAP", \
                     "ETIMPRAW","ETIMPRAWEB","ETIMPRAWX","ETIMPRAWXEB","ETIMPDMAP", \
                     "ENIMPLIST"]
            SD = ptools.delete_fields_from_dict(SD,fields=dlist)

    if fdebug:
        print("standardize_aug.py: combine_edge_diagnostic_data() completed.")

    return SD


def add_profile_boundary_data(stddata,fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Inserts appropriate boundary constraints on the various profiles
    at the separatrix in order improve the robustness of the GP fit
    routine and to achieve more meaningful fits.

    :arg stddata: dict. Standardized object containing the processed profiles.

    :returns: dict. Object with identical structure as input object except with profile boundary constraints added to standard fields.
    """
    SD = None
    if isinstance(stddata,dict):
        SD = stddata

    ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

    ###  The following section adds profile boundary constraints for more robust GP fits
    if SD is not None:
#        if "NERAW" in SD and SD["NERAW"].size > 0 and np.nanmin(np.abs(SD["NERAWX"] - 1.0)) > 0.05:
#            idx = np.where((1.0 - SD["NERAWX"]) > 0.0)[0][-1]
#            eneval = 0.01 / (1.0 - SD["NERAWX"][idx]) * SD["NERAW"][idx]
#            SD["NERAWX"] = np.hstack((SD["NERAWX"],0.99))
#            SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"],0.0))
#            SD["NERAW"] = np.hstack((SD["NERAW"],eneval))
#            SD["NERAWEB"] = np.hstack((SD["NERAWEB"],0.3 * eneval))

        # 3-point NE boundary constraint based on data near rho=1.0 yields most robust results for both L- and H-mode time windows
        edgene = 3.0e18
        if "NERAW" in SD and SD["NERAW"].size > 0:
            efilt = np.all([SD["NERAWX"] >= 0.9,SD["NERAWX"] <= 1.0],axis=0)
            edgene = 3.0e18
            edgeneeb = 1.5e18
            if np.any(efilt):
                nextemp = SD["NERAWX"][efilt]
                netemp = SD["NERAW"][efilt]
                idx = np.where(nextemp >= np.nanmax(nextemp))[0][0]
                if netemp[idx] < 1.0e19:
                    edgene = 0.8 * netemp[idx]
            sfilt = np.all([SD["NERAWX"] >= 0.975,SD["NERAWX"] <= 1.0],axis=0)
            if np.any(sfilt):
                neavg = np.nanmean(SD["NERAW"][sfilt])
                if edgene < 0.4 * neavg:
                    edgene = 0.4 * neavg
                    edgeneeb = 0.5 * edgene
            SD["NERAWX"] = np.hstack((SD["NERAWX"],[0.98,1.00,1.02]))
            SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"],[0.0,0.0,0.0]))
            SD["NERAW"] = np.hstack((SD["NERAW"],[edgene,edgene,edgene]))
            SD["NERAWEB"] = np.hstack((SD["NERAWEB"],[2.0*edgeneeb,1.5*edgeneeb,1.0*edgeneeb]))
            SD["NEDMAP"] = np.hstack((SD["NEDMAP"],[len(SD["NEDIAG"])] * 3))
            SD["NEDIAG"].append("NEBC")

        # Pasting of TE edge data as TI profile constraint yields better resolution of TI pedestal
        #     Shown to improve boundary condition of integrated modelling
        if "TERAW" in SD and SD["TERAW"].size > 0 and "TIRAW" in SD and SD["TIRAW"].size > 3:
            efilt = np.all([SD["TERAWX"] >= 0.85,SD["TERAWX"] <= 1.0],axis=0)
            nume = np.count_nonzero(efilt)
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"],SD["TERAWX"][efilt]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"],SD["TERAWXEB"][efilt]))
            SD["TIRAW"] = np.hstack((SD["TIRAW"],SD["TERAW"][efilt]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"],SD["TERAWEB"][efilt]))
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"],[len(SD["TIDIAG"])] * nume))
            SD["TIDIAG"].append("TIBC")

        # Pasting of TE edge data as TIMP profile constraint yields better resolution of TIMP pedestal
        #     Shown to improve boundary condition of integrated modelling
        if "TERAW" in SD and SD["TERAW"].size > 0 and "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            efilt = np.all([SD["TERAWX"] >= 0.85,SD["TERAWX"] <= 1.0],axis=0)
            nume = np.count_nonzero(efilt)
            SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"],SD["TERAWX"][efilt]))
            SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"],SD["TERAWXEB"][efilt]))
            SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"],SD["TERAW"][efilt]))
            SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"],SD["TERAWEB"][efilt]))
            SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"],[len(SD["TIMPDIAG"])] * nume))
            SD["TIMPDIAG"].append("TZBC")

        # Determination of TE and TI and TIMP boundary constraint based on data near rho=1.0
        edgete = 0
        edgeti = np.NaN
        edgeteeb = 0
        edgetieb = np.NaN
        if "TERAW" in SD and SD["TERAW"].size > 0:
            efilt = np.all([SD["TERAWX"] >= 0.9,SD["TERAWX"] <= 1.0],axis=0)
            idx = np.where(SD["TERAWX"][efilt] >= np.nanmax(SD["TERAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgete = 0.7 * SD["TERAW"][efilt][idx] if idx is not None else 100
            edgeteeb = np.abs(2.0 * SD["TERAWEB"][efilt][idx]) if idx is not None else 50
        if "TIRAW" in SD and SD["TIRAW"].size > 0:
            efilt = np.all([SD["TIRAWX"] >= 0.9,SD["TIRAWX"] <= 1.0],axis=0)
            idx = np.where(SD["TIRAWX"][efilt] >= np.nanmax(SD["TIRAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgeti = 0.7 * SD["TIRAW"][efilt][idx] if idx is not None else 100
            edgetieb = np.abs(3.0 * SD["TIRAWEB"][efilt][idx]) if idx is not None else 50
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            efilt = np.all([SD["TIMPRAWX"] >= 0.9,SD["TIMPRAWX"] <= 1.0],axis=0)
            idx = np.where(SD["TIMPRAWX"][efilt] >= np.nanmax(SD["TIMPRAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgeti = 0.7 * SD["TIMPRAW"][efilt][idx] if idx is not None else 100
            edgetieb = np.abs(3.0 * SD["TIMPRAWEB"][efilt][idx]) if idx is not None else 50
        edget = np.nanmax([edgete,edgeti])
        if edget > 500:
            edget = 300
        elif "RMAJSHIFT" in SD and SD["RMAJSHIFT"] is not None and SD["RMAJSHIFT"] > 0.0 and edget < 100:
            edget = 100
        edgeteb = np.nanmin([0.5 * edget,np.nanmax([edgeteeb,edgetieb])])

        # 3-point combined TE and TI boundary constraint yields most robust results for L- and H-mode time windows
        if "TERAW" in SD and SD["TERAW"].size > 0:
            SD["TERAWX"] = np.hstack((SD["TERAWX"],[0.98,1.00,1.02]))
            SD["TERAWXEB"] = np.hstack((SD["TERAWXEB"],[0.0,0.0,0.0]))
            SD["TERAW"] = np.hstack((SD["TERAW"],[edget,edget,edget]))
            SD["TERAWEB"] = np.hstack((SD["TERAWEB"],[2.0*edgeteb,1.5*edgeteb,1.0*edgeteb]))
            SD["TEDMAP"] = np.hstack((SD["TEDMAP"],[len(SD["TEDIAG"])] * 3))
            SD["TEDIAG"].append("TEBC")
        if "TIRAW" in SD and SD["TIRAW"].size > 3:
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"],[0.98,1.00,1.02]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"],[0.0,0.0,0.0]))
            SD["TIRAW"] = np.hstack((SD["TIRAW"],[edget,edget,edget]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"],[1.5*edgeteb,1.25*edgeteb,1.0*edgeteb]))
            bcidx = len(SD["TIDIAG"])
            for ii in np.arange(0,len(SD["TIDIAG"])):
                if re.match('^TIBC$',SD["TIDIAG"][ii]):
                    bcidx = ii
            if bcidx >= len(SD["TIDIAG"]):
                SD["TIDIAG"].append("TIBC")
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"],[bcidx,bcidx,bcidx]))
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"],[0.98,1.00,1.02]))
            SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"],[0.0,0.0,0.0]))
            SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"],[edget,edget,edget]))
            SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"],[1.5*edgeteb,1.25*edgeteb,1.0*edgeteb]))
            bcidx = len(SD["TIMPDIAG"])
            for ii in np.arange(0,len(SD["TIMPDIAG"])):
                if re.match('^TZBC$',SD["TIMPDIAG"][ii]):
                    bcidx = ii
            if bcidx >= len(SD["TIMPDIAG"]):
                SD["TIMPDIAG"].append("TZBC")
            SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"],[bcidx,bcidx,bcidx]))

        # Single point AF boundary constraint based on aggregate slope yields most robust results over many plasma regimes
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
#            zfilt = np.all([SD["AFRAW"] == 0.0,SD["AFRAWEB"] <= 0.0],axis=0)
#            SD["AFRAWEB"][zfilt] = 500
            afmax = np.nanmax(np.abs(SD["AFRAW"]))
            edgeaf = 0
            edgeafeb = 0.075 * afmax
            efilt = (SD["AFRAWX"] >= 0.9)
            cfilt = np.all([SD["AFRAWX"] > 0.0,SD["AFRAWX"] <= 0.5],axis=0)
            if np.any(efilt) and np.any(cfilt):
                afe = np.mean(SD["AFRAW"][efilt])
                afc = np.mean(SD["AFRAW"][cfilt])
                if (afe * afc) < 0.0:
                    rhoe = np.mean(SD["AFRAWX"][efilt])
                    rhoc = np.mean(SD["AFRAWX"][cfilt])
                    edgeaf = 0.8 * ((1.02 - rhoe) * (afc - afe) / (rhoc - rhoe) + afe)
                    edgeafeb = np.abs(0.25 * edgeaf)
            SD["AFRAWX"] = np.hstack((SD["AFRAWX"],1.02))
            SD["AFRAWXEB"] = np.hstack((SD["AFRAWXEB"],0.0))
            SD["AFRAW"] = np.hstack((SD["AFRAW"],edgeaf))
            SD["AFRAWEB"] = np.hstack((SD["AFRAWEB"],edgeafeb))
            SD["AFDMAP"] = np.hstack((SD["AFDMAP"],[len(SD["AFDIAG"])]))
            SD["AFDIAG"].append("AFBC")

        # Some assumptions for the impurity density filtering - needs a more robust selection
        fzeff = 0.0
        if "ZEFV" in SD and SD["ZEFV"] is not None and (fzeff < 1.0 or SD["ZEFV"] < fzeff):
            fzeff = float(SD["ZEFV"])
        if "ZEFH" in SD and SD["ZEFH"] is not None and (fzeff < 1.0 or SD["ZEFH"] < fzeff):
            fzeff = float(SD["ZEFH"])
        if fzeff < 1.0:
            fzeff = 1.25 if re.match(r'^Be$',SD["WALLMAT"],flags=re.IGNORECASE) else 2.0
        zi = 1.0

        # Single point NIMP boundary constraint, dependent on impurity charge, provides sufficient robustness
        idx = 0
        for ii in np.arange(0,len(SD["NIMPLIST"])):
            if "NIMPRAW" in SD["NIMPLIST"][ii] and SD["NIMPLIST"][ii]["NIMPRAW"] is not None and SD["NIMPLIST"][ii]["NIMPRAW"].size > 0:
                idx = idx + 1
                itag = ("%d" % (idx))
                edgenimp = 0.5 * edgene * (fzeff - zi) / (SD["NIMPLIST"][ii]["ZIMP"] * (SD["NIMPLIST"][ii]["ZIMP"] - fzeff))
                edgenimpeb = 0.4 * (edgenimp + 2.0e16 / SD["NIMPLIST"][ii]["ZIMP"])
                SD["NIMP"+itag+"RAWX"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAWX"],1.02))
                SD["NIMP"+itag+"RAWXEB"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAWXEB"],0.0))
                SD["NIMP"+itag+"RAW"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAW"],edgenimp))
                SD["NIMP"+itag+"RAWEB"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAWEB"],edgenimpeb))
                SD["NIMP"+itag+"DMAP"] = np.hstack((SD["NIMPLIST"][ii]["NIMPDMAP"],[len(SD["NIMPLIST"][ii]["NIMPDIAG"])]))
                SD["NIMP"+itag+"DIAG"] = SD["NIMPLIST"][ii]["NIMPDIAG"]
                SD["NIMP"+itag+"DIAG"].append("NXBC")
                SD["AIMP"+itag+"RAW"] = SD["NIMPLIST"][ii]["AIMP"]
                SD["ZIMP"+itag+"RAW"] = SD["NIMPLIST"][ii]["ZIMP"]
        SD["NUMIMP"] = len(SD["NIMPLIST"])

        # Single point Q boundary constraint based on edge data provides sufficient robustness, max. value of 4
        if "QRAW" in SD and SD["QRAW"].size > 0:
            bfilt = np.all([SD["QRAWX"] >= 0.01,SD["QRAWX"] <= 0.99],axis=0)
            edgeq = np.nanmax((4.0,1.2 * np.nanmax(SD["QRAW"][bfilt])))
            edgeqeb = 0.5
            SD["QRAWX"] = np.hstack((SD["QRAWX"],1.02))
            SD["QRAWXEB"] = np.hstack((SD["QRAWXEB"],0.0))
            SD["QRAW"] = np.hstack((SD["QRAW"],edgeq))
            SD["QRAWEB"] = np.hstack((SD["QRAWEB"],edgeqeb))
            SD["QDMAP"] = np.hstack((SD["QDMAP"],len(SD["QDIAG"])))
            SD["QDIAG"].append("QBC")
        if "IQRAW" in SD and SD["IQRAW"].size > 0:
            bfilt = np.all([SD["IQRAWX"] >= 0.01,SD["IQRAWX"] <= 0.99],axis=0)
            edgeq = np.nanmin((0.25,np.nanmin(SD["IQRAW"][bfilt] / 1.2)))
            edgeqeb = 0.02
            SD["IQRAWX"] = np.hstack((SD["IQRAWX"],1.02))
            SD["IQRAWXEB"] = np.hstack((SD["IQRAWXEB"],0.0))
            SD["IQRAW"] = np.hstack((SD["IQRAW"],edgeq))
            SD["IQRAWEB"] = np.hstack((SD["IQRAWEB"],edgeqeb))
            SD["IQDMAP"] = np.hstack((SD["IQDMAP"],len(SD["IQDIAG"])))
            SD["IQDIAG"].append("IQBC")

        dlist = ["NIMPLIST","ENIMPLIST"]
        SD = ptools.delete_fields_from_dict(SD,fields=dlist)

    if fdebug:
        print("standardize_aug.py: add_profile_boundary_data() completed.")

    return SD


def standardize_source_data(procdata,newstruct=None,fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Transfers source profile data fields from the implementation-
    specific object to the standardized object, with interpolation,
    ensuring that the standardized naming and format conventions are
    retained.

    :arg procdata: dict. Implementation-specific object containing processed source profile data.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :returns: dict. Object with identical structure as input object except with source profiles interpolated and transferred.
    """
    PD = dict()
    QD = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(newstruct,dict):
        QD = newstruct
    else:
        QD = dict()

    if PD is not None:

        QD["RAWSRCFLAG"] = True if "RAWSRCFLAG" in PD and PD["RAWSRCFLAG"] else False

        # Transfer neutral beam heating source data
        src = "NBI"
        qtag = "NBP2"
        quantities = ["QE","QI","S","J","TAU","NFIA","WFIA"]
        if qtag in PD and PD[qtag] is not None:
            QD[src] = dict()
            for dtag in quantities:
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD,PD[qtag],quantity=dtag)
                    if tempd is not None:
                        QD[src][dtag] = tempd
            if "AFI1" in PD[qtag] and "ZFI1" in PD[qtag]:
                QD[src]["AFI1"] = PD[qtag]["AFI1"] if PD[qtag]["AFI1"] is not None and PD[qtag]["AFI1"] >= 1.0 else 2.0
                QD[src]["ZFI1"] = PD[qtag]["ZFI1"] if PD[qtag]["ZFI1"] is not None and PD[qtag]["ZFI1"] >= 1.0 else 1.0
            if QD[src]:
                QD[src]["CODE"] = PD[qtag]["CODE"]
            else:
                del QD[src]

        # Transfer ion cyclotron heating source data
        src = "ICRH"
        qtag = "PION"
        quantities = ["QE","QI","NFIA","WFIA"]
        if qtag in PD and PD[qtag] is not None:
            QD[src] = dict()
            for dtag in quantities:
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD,PD[qtag],quantity=dtag)
                    if tempd is not None:
                        QD[src][dtag] = tempd
            if "AFI1" in PD[qtag] and "ZFI1" in PD[qtag]:
                QD[src]["AFI1"] = PD[qtag]["AFI1"] if PD[qtag]["AFI1"] is not None and PD[qtag]["AFI1"] >= 1.0 else 2.0
                QD[src]["ZFI1"] = PD[qtag]["ZFI1"] if PD[qtag]["ZFI1"] is not None and PD[qtag]["ZFI1"] >= 1.0 else 1.0
            if "AFI2" in PD[qtag] and "ZFI2" in PD[qtag]:
                QD[src]["AFI2"] = PD[qtag]["AFI2"] if PD[qtag]["AFI2"] is not None and PD[qtag]["AFI2"] >= 1.0 else 1.0
                QD[src]["ZFI2"] = PD[qtag]["ZFI2"] if PD[qtag]["ZFI2"] is not None and PD[qtag]["ZFI2"] >= 1.0 else 1.0

            # Subtracts NBI heat source profiles from PION, if NBI profiles are also extracted
            #    PION uses NBI profiles to calculate synergistic heating effects and includes base NBI contribution in output
            if QD[src]:
                QD[src]["CODE"] = PD[qtag]["CODE"]
                if "NBI" in QD and "QE" in QD["NBI"] and "QE" in QD[src]:
                    QD[src]["QE"]["VAL"] = QD[src]["QE"]["VAL"] - QD["NBI"]["QE"]["VAL"]
                    zfilt = (QD[src]["QE"]["VAL"] <= 0.0)
                    if np.any(zfilt):
                        QD[src]["QE"]["VAL"][zfilt] = 0.0
                if "NBI" in QD and "QI" in QD["NBI"] and "QI" in QD[src]:
                    QD[src]["QI"]["VAL"] = QD[src]["QI"]["VAL"] - QD["NBI"]["QI"]["VAL"]
                    zfilt = (QD[src]["QI"]["VAL"] <= 0.0)
                    if np.any(zfilt):
                        QD[src]["QI"]["VAL"][zfilt] = 0.0
            else:
                del QD[src]

        # Transfer ohmic heating source data
        src = "OHM"
        qtag = "OHM"
        quantities = ["QE","QI","J"]
        if qtag in PD and PD[qtag] is not None:
            QD[src] = dict()
            for dtag in quantities:
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD,PD[qtag],quantity=dtag)
                    if tempd is not None:
                        QD[src][dtag] = tempd
            if QD[src]:
                QD[src]["CODE"] = PD[qtag]["CODE"]
            else:
                del QD[src]

        # Remove rho=0 point in NBI source profiles calculated by PENCIL, spurious value due to zero volume in code, messes with fitting
        if "NBI" in QD and re.match(r'^NBP2$',QD["NBI"]["CODE"],flags=re.IGNORECASE):
            quantities = ["QE","QI","S","J","TAU","NFIA","WFIA"]
            for dtag in quantities:
                if dtag in QD["NBI"] and QD["NBI"][dtag]["VAL"] is not None:
                    QD["NBI"][dtag]["OPFN"] = QD["NBI"][dtag]["OPFN"][1:]
                    QD["NBI"][dtag]["VAL"] = QD["NBI"][dtag]["VAL"][1:]
                    QD["NBI"][dtag]["VALEB"] = QD["NBI"][dtag]["VALEB"][1:] if QD["NBI"][dtag]["VALEB"] is not None else None
                    QD["NBI"][dtag]["N"] = QD["NBI"][dtag]["N"][1:] if QD["NBI"][dtag]["N"] is not None else None
                    QD["NBI"][dtag]["PFN"] = QD["NBI"][dtag]["PFN"][1:]
                    QD["NBI"][dtag]["PFNEB"] = QD["NBI"][dtag]["PFNEB"][1:]
                    QD["NBI"][dtag]["RHOPN"] = QD["NBI"][dtag]["RHOPN"][1:]
                    QD["NBI"][dtag]["RHOPNEB"] = QD["NBI"][dtag]["RHOPNEB"][1:]
                    QD["NBI"][dtag]["TFN"] = QD["NBI"][dtag]["TFN"][1:]
                    QD["NBI"][dtag]["TFNEB"] = QD["NBI"][dtag]["TFNEB"][1:]
                    QD["NBI"][dtag]["RHOTN"] = QD["NBI"][dtag]["RHOTN"][1:]
                    QD["NBI"][dtag]["RHOTNEB"] = QD["NBI"][dtag]["RHOTNEB"][1:]
                    QD["NBI"][dtag]["AXID"] = QD["NBI"][dtag]["AXID"] - 1 if QD["NBI"][dtag]["AXID"] != 0 else 0

        if not ("RAWSRCFLAG" in QD and QD["RAWSRCFLAG"]):
            dlist = ["NBP2","PION","OHM"]
            PD = ptools.delete_fields_from_dict(PD,fields=dlist)

    if fdebug:
        print("standardize_aug.py: standardize_source_data() completed.")

    return QD


def transfer_standardized_source_data(srcdata,newstruct=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Applies pre-defined filters to the raw diagnostic data, optimized
    such to improve the quality of the fitting routine.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    QD = None
    SD = None
    if isinstance(srcdata,dict):
        QD = srcdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    fe = 0

    if QD is not None:
        SD["CS_SRC"] = "RHOPOLN"

        src = "NBI"
        if src in QD and QD[src] is not None:
            gflag = False
            if "QE" in QD[src] and QD[src]["QE"] is not None:
                SD["QE"+src+"RAW"] = QD[src]["QE"]["VAL"].copy()
                SD["QE"+src+"RAWEB"] = QD[src]["QE"]["VALEB"].copy()
                SD["QE"+src+"RAWX"] = QD[src]["QE"]["RHOPN"].copy()
                gflag = True
            if "QI" in QD[src] and QD[src]["QI"] is not None:
                SD["QI"+src+"RAW"] = QD[src]["QI"]["VAL"].copy()
                SD["QI"+src+"RAWEB"] = QD[src]["QI"]["VALEB"].copy()
                SD["QI"+src+"RAWX"] = QD[src]["QI"]["RHOPN"].copy()
                gflag = True
            if "S" in QD[src] and QD[src]["S"] is not None:
                SD["S"+src+"RAW"] = QD[src]["S"]["VAL"].copy()
                SD["S"+src+"RAWEB"] = QD[src]["S"]["VALEB"].copy()
                SD["S"+src+"RAWX"] = QD[src]["S"]["RHOPN"].copy()
                gflag = True
            if "J" in QD[src] and QD[src]["J"] is not None:
                SD["J"+src+"RAW"] = QD[src]["J"]["VAL"].copy()
                SD["J"+src+"RAWEB"] = QD[src]["J"]["VALEB"].copy()
                SD["J"+src+"RAWX"] = QD[src]["J"]["RHOPN"].copy()
                gflag = True
            if "TAU" in QD[src] and QD[src]["TAU"] is not None:
                SD["TAU"+src+"RAW"] = QD[src]["TAU"]["VAL"].copy()
                SD["TAU"+src+"RAWEB"] = QD[src]["TAU"]["VALEB"].copy()
                SD["TAU"+src+"RAWX"] = QD[src]["TAU"]["RHOPN"].copy()
                gflag = True
            if "NFIA" in QD[src] and QD[src]["NFIA"] is not None:
                SD["NFI"+src+"RAW"] = QD[src]["NFIA"]["VAL"].copy()
                SD["NFI"+src+"RAWEB"] = QD[src]["NFIA"]["VALEB"].copy()
                SD["NFI"+src+"RAWX"] = QD[src]["NFIA"]["RHOPN"].copy()
                gflag = True
            if "WFIA" in QD[src] and QD[src]["WFIA"] is not None:
                SD["WFI"+src+"RAW"] = QD[src]["WFIA"]["VAL"].copy()
                SD["WFI"+src+"RAWEB"] = QD[src]["WFIA"]["VALEB"].copy()
                SD["WFI"+src+"RAWX"] = QD[src]["WFIA"]["RHOPN"].copy()
                gflag = True
            if gflag and "AFI1" in QD[src] and "ZFI1" in QD[src]:
                SD["AFI1_"+src] = float(QD[src]["AFI1"]) if QD[src]["AFI1"] is not None else None
                SD["ZFI1_"+src] = float(QD[src]["ZFI1"]) if QD[src]["ZFI1"] is not None else None
            if gflag:
                SD[src+"CODE"] = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"

        src = "ICRH"
        pionflag = False
        if src in QD and QD[src] is not None:
            gflag = False
            if "QE" in QD[src] and QD[src]["QE"] is not None:
                SD["QE"+src+"RAW"] = QD[src]["QE"]["VAL"].copy()
                SD["QE"+src+"RAWEB"] = QD[src]["QE"]["VALEB"].copy()
                SD["QE"+src+"RAWX"] = QD[src]["QE"]["RHOPN"].copy()
                gflag = True
            if "QI" in QD[src] and QD[src]["QI"] is not None:
                SD["QI"+src+"RAW"] = QD[src]["QI"]["VAL"].copy()
                SD["QI"+src+"RAWEB"] = QD[src]["QI"]["VALEB"].copy()
                SD["QI"+src+"RAWX"] = QD[src]["QI"]["RHOPN"].copy()
                gflag = True
#            if "QFI1" in QD[src] and QD[src]["QFI1"] is not None:
#                SD["QFI1"+src+"RAW"] = QD[src]["QFI1"]["VAL"].copy()
#                SD["QFI1"+src+"RAWEB"] = QD[src]["QFI1"]["VALEB"].copy()
#                SD["QFI1"+src+"RAWX"] = QD[src]["QFI1"]["RHOPN"].copy()
#                gflag = True
#            if "QFI2" in QD[src] and QD[src]["QFI2"] is not None:
#                SD["QFI2"+src+"RAW"] = QD[src]["QFI2"]["VAL"].copy()
#                SD["QFI2"+src+"RAWEB"] = QD[src]["QFI2"]["VALEB"].copy()
#                SD["QFI2"+src+"RAWX"] = QD[src]["QFI2"]["RHOPN"].copy()
#                gflag = True
            if "NFIA" in QD[src] and QD[src]["NFIA"] is not None:
                SD["NFI"+src+"RAW"] = QD[src]["NFIA"]["VAL"].copy()
                SD["NFI"+src+"RAWEB"] = QD[src]["NFIA"]["VALEB"].copy()
                SD["NFI"+src+"RAWX"] = QD[src]["NFIA"]["RHOPN"].copy()
                gflag = True
            if "WFIA" in QD[src] and QD[src]["WFIA"] is not None:
                SD["WFI"+src+"RAW"] = QD[src]["WFIA"]["VAL"].copy()
                SD["WFI"+src+"RAWEB"] = QD[src]["WFIA"]["VALEB"].copy()
                SD["WFI"+src+"RAWX"] = QD[src]["WFIA"]["RHOPN"].copy()
                gflag = True
            if gflag and "AFI1" in QD[src] and "ZFI1" in QD[src]:
                SD["AFI1_"+src] = float(QD[src]["AFI1"]) if QD[src]["AFI1"] is not None else None
                SD["ZFI1_"+src] = float(QD[src]["ZFI1"]) if QD[src]["ZFI1"] is not None else None
            if gflag and "AFI2" in QD[src] and "ZFI2" in QD[src]:
                SD["AFI2_"+src] = float(QD[src]["AFI2"]) if QD[src]["AFI2"] is not None else None
                SD["ZFI2_"+src] = float(QD[src]["ZFI2"]) if QD[src]["ZFI2"] is not None else None
            if gflag:
                SD[src+"CODE"] = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"

        src = "LH"
        if src in QD and QD[src] is not None:
            gflag = False
            if gflag:
                SD[src+"CODE"] = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
            print("This should not be printed at AUG.")

        src = "OHM"
        if src in QD and QD[src] is not None:
            gflag = False
            if "QE" in QD[src] and QD[src]["QE"] is not None:
                SD["QE"+src+"RAW"] = QD[src]["QE"]["VAL"].copy()
                SD["QE"+src+"RAWEB"] = QD[src]["QE"]["VALEB"].copy()
                SD["QE"+src+"RAWX"] = QD[src]["QE"]["RHOPN"].copy()
                gflag = True
            if "QI" in QD[src] and QD[src]["QI"] is not None:
                SD["QI"+src+"RAW"] = QD[src]["QI"]["VAL"].copy()
                SD["QI"+src+"RAWEB"] = QD[src]["QI"]["VALEB"].copy()
                SD["QI"+src+"RAWX"] = QD[src]["QI"]["RHOPN"].copy()
                gflag = True
            if "J" in QD[src] and QD[src]["J"] is not None:
                SD["J"+src+"RAW"] = QD[src]["J"]["VAL"].copy()
                SD["J"+src+"RAWEB"] = QD[src]["J"]["VALEB"].copy()
                SD["J"+src+"RAWX"] = QD[src]["J"]["RHOPN"].copy()
                gflag = True
            if gflag:
                SD[src+"CODE"] = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"

    if fdebug:
        print("standardize_aug.py: transfer_standardized_source_data() completed.")

    return SD


def add_derivative_constraint_data(stddata,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Inserts appropriate derivative constraints on the various profiles
    in order improve the robustness of the GP fit routine and to
    achieve more meaningful fits.

    :arg stddata: dict. Standardized object containing the processed profiles and GPR1D settings.

    :returns: dict. Standardized object with derivative constraints for profile to be fitted inserted.
    """
    SD = None
    if isinstance(stddata,dict):
        SD = stddata

    ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

    ###  The following section adds profile derivative constraints for more robust GP fits
    if SD is not None and "CS_DIAG" in SD and re.match('^RHO((TOR)|(POL))N?$',SD["CS_DIAG"],flags=re.IGNORECASE):

        SD["SYMMETRIC"] = True

        # Zero NE derivative constraint at rho_tor = 0.0, axis of symmetry
        if "NERAW" in SD and SD["NERAW"].size > 0:
            SD["DNERAWX"] = np.array([0.0])
            SD["DNERAWXEB"] = np.array([0.0])
            SD["DNERAW"] = np.array([0.0])
            SD["DNERAWEB"] = np.array([0.0])

        # Zero TE derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TERAW" in SD and SD["TERAW"].size > 0:
            SD["DTERAWX"] = np.array([0.0])
            SD["DTERAWXEB"] = np.array([0.0])
            SD["DTERAW"] = np.array([0.0])
            SD["DTERAWEB"] = np.array([0.0])

        # Zero TI derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TIRAW" in SD and SD["TIRAW"].size > 3:
            # Additional derivative constraint if there is no inner core data, rho < 0.2
            cfilt = np.all([SD["TIRAWX"] >= 0.0,SD["TIRAWX"] <= 0.2],axis=0)
            uidx = np.where(SD["TIRAW"] == np.nanmax(SD["TIRAW"]))[0][0]
            lidx = np.where(SD["TIRAW"] == np.nanmin(SD["TIRAW"]))[0][0]
            corecon = None
#            corecon = float(0.3 * (SD["TIRAW"][uidx] - SD["TIRAW"][lidx]) / (SD["TIRAWX"][uidx] - SD["TIRAWX"][lidx])) if not np.any(cfilt) else None
            dcxvec = np.array([0.1]) if corecon is not None else np.array([])
            dcyvec = np.array([corecon]) if corecon is not None else np.array([])
            dcevec = np.array([0.3 * np.abs(corecon)]) if corecon is not None else np.array([])
            SD["DTIRAWX"] = np.hstack((0.0,dcxvec))
            SD["DTIRAWXEB"] = np.hstack((0.0,np.zeros(dcxvec.shape)))
            SD["DTIRAW"] = np.hstack((0.0,dcyvec))
            SD["DTIRAWEB"] = np.hstack((0.0,dcevec))

        # Zero AF derivative constraint at rho_tor = 0.0, axis of symmetry
        # Extra derivative constraint at inner-most point to avoid high core shear in GP fit due to lack of data
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
            # This filter works since the radial coordinate with JET data is always rhopol or rhotor
            iidx = np.where(SD["AFRAWX"] == np.nanmin(SD["AFRAWX"]))[0][0]
            edgecon = None
#            edgecon = float(-SD["AFRAW"][iidx]) / 2.0 if SD["AFRAWX"][iidx] > 0.2 else None
            dcxvec = np.array([0.2]) if edgecon is not None else np.array([])
            dcyvec = np.array([edgecon]) if edgecon is not None else np.array([])
            dcevec = np.array([0.25 * np.abs(edgecon)]) if edgecon is not None else np.array([])
            SD["DAFRAWX"] = np.hstack((0.0,dcxvec))
            SD["DAFRAWXEB"] = np.hstack((0.0,np.zeros(dcxvec.shape)))
            SD["DAFRAW"] = np.hstack((0.0,dcyvec))
            SD["DAFRAWEB"] = np.hstack((0.0,dcevec))

        # Zero NIMP derivative constraint at rho_tor = 0.0, axis of symmetry
        for ii in np.arange(0,SD["NUMIMP"]):
            itag = "%d" % (ii+1)
            if "NIMP"+itag+"RAW" in SD and SD["NIMP"+itag+"RAW"].size > 0:
                SD["DNIMP"+itag+"RAWX"] = np.array([0.0])
                SD["DNIMP"+itag+"RAWXEB"] = np.array([0.0])
                SD["DNIMP"+itag+"RAW"] = np.array([0.0])
                SD["DNIMP"+itag+"RAWEB"] = np.array([0.0])

        # Zero TIMP derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            # Additional derivative constraint if there is no inner core data, rho <= 0.2
            cfilt = np.all([SD["TIMPRAWX"] >= 0.0,SD["TIMPRAWX"] <= 0.2],axis=0)
            uidx = np.where(SD["TIMPRAW"] == np.nanmax(SD["TIMPRAW"]))[0][0]
            lidx = np.where(SD["TIMPRAW"] == np.nanmin(SD["TIMPRAW"]))[0][0]
            corecon = None
#            corecon = float(0.3 * (SD["TIMPRAW"][uidx] - SD["TIMPRAW"][lidx]) / (SD["TIMPRAWX"][uidx] - SD["TIMPRAWX"][lidx])) if not np.any(cfilt) else None
            # Additional derivative constraint if there is no core data, rho <= 0.5
            cfilt = np.all([SD["TIMPRAWX"] >= 0.0,SD["TIMPRAWX"] <= 0.5],axis=0)
            uidx = np.where(SD["TIMPRAW"] == np.nanmax(SD["TIMPRAW"]))[0][0]
            lidx = np.where(SD["TIMPRAW"] == np.nanmin(SD["TIMPRAW"]))[0][0]
            corecon2 = None
#            corecon2 = float(0.9 * (SD["TIMPRAW"][uidx] - SD["TIMPRAW"][lidx]) / (SD["TIMPRAWX"][uidx] - SD["TIMPRAWX"][lidx])) if not np.any(cfilt) else None
            dcxvec = np.array([0.1]) if corecon is not None else np.array([])
            dcyvec = np.array([corecon]) if corecon is not None else np.array([])
            dcevec = np.array([0.3 * np.abs(corecon)]) if corecon is not None else np.array([])
            if corecon2 is not None:
                dcxvec = np.hstack((dcxvec,0.4))
                dcyvec = np.hstack((dcyvec,corecon2))
                dcevec = np.hstack((2.0 * dcevec,0.2 * corecon2))
            SD["DTIMPRAWX"] = np.hstack((0.0,dcxvec))
            SD["DTIMPRAWXEB"] = np.hstack((0.0,np.zeros(dcxvec.shape)))
            SD["DTIMPRAW"] = np.hstack((0.0,dcyvec))
            SD["DTIMPRAWEB"] = np.hstack((0.0,dcevec))

        # Zero Q derivative constraint at rho_tor = 0.0, axis of symmetry
        if "QRAW" in SD and SD["QRAW"].size > 0:
            SD["DQRAWX"] = np.array([0.0])
            SD["DQRAWXEB"] = np.array([0.0])
            SD["DQRAW"] = np.array([0.0])
            SD["DQRAWEB"] = np.array([0.0])
        if "IQRAW" in SD and SD["IQRAW"].size > 0:
            SD["DIQRAWX"] = np.array([0.0])
            SD["DIQRAWXEB"] = np.array([0.0])
            SD["DIQRAW"] = np.array([0.0])
            SD["DIQRAWEB"] = np.array([0.0])

        # Zero derivative constraint for all source profiles at rho_tor = 0.0, axis of symmetry
        sources = ["NBI","ICRH","ECRH","LH","OHM"]
        for src in sources:
            if "QE"+src+"RAW" in SD and SD["QE"+src+"RAW"].size > 0:
                SD["DQE"+src+"RAWX"] = np.array([0.0])
                SD["DQE"+src+"RAWXEB"] = np.array([0.0])
                SD["DQE"+src+"RAW"] = np.array([0.0])
                SD["DQE"+src+"RAWEB"] = np.array([0.0])
            if "QI"+src+"RAW" in SD and SD["QI"+src+"RAW"].size > 0:
                SD["DQI"+src+"RAWX"] = np.array([0.0])
                SD["DQI"+src+"RAWXEB"] = np.array([0.0])
                SD["DQI"+src+"RAW"] = np.array([0.0])
                SD["DQI"+src+"RAWEB"] = np.array([0.0])
            if "S"+src+"RAW" in SD and SD["S"+src+"RAW"].size > 0:
                SD["DS"+src+"RAWX"] = np.array([0.0])
                SD["DS"+src+"RAWXEB"] = np.array([0.0])
                SD["DS"+src+"RAW"] = np.array([0.0])
                SD["DS"+src+"RAWEB"] = np.array([0.0])
            if "J"+src+"RAW" in SD and SD["J"+src+"RAW"].size > 0:
                SD["DJ"+src+"RAWX"] = np.array([0.0])
                SD["DJ"+src+"RAWXEB"] = np.array([0.0])
                SD["DJ"+src+"RAW"] = np.array([0.0])
                SD["DJ"+src+"RAWEB"] = np.array([0.0])
            if "TAU"+src+"RAW" in SD and SD["TAU"+src+"RAW"].size > 0:
                SD["DTAU"+src+"RAWX"] = np.array([0.0])
                SD["DTAU"+src+"RAWXEB"] = np.array([0.0])
                SD["DTAU"+src+"RAW"] = np.array([0.0])
                SD["DTAU"+src+"RAWEB"] = np.array([0.0])
            if "NFI"+src+"RAW" in SD and SD["NFI"+src+"RAW"].size > 0:
                SD["DNFI"+src+"RAWX"] = np.array([0.0])
                SD["DNFI"+src+"RAWXEB"] = np.array([0.0])
                SD["DNFI"+src+"RAW"] = np.array([0.0])
                SD["DNFI"+src+"RAWEB"] = np.array([0.0])
            if "WFI"+src+"RAW" in SD and SD["WFI"+src+"RAW"].size > 0:
                SD["DWFI"+src+"RAWX"] = np.array([0.0])
                SD["DWFI"+src+"RAWXEB"] = np.array([0.0])
                SD["DWFI"+src+"RAW"] = np.array([0.0])
                SD["DWFI"+src+"RAWEB"] = np.array([0.0])

    if fdebug:
        print("standardize_aug.py: add_derivative_constraint_data() completed.")

    return SD
