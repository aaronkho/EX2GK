# Script with functions to define optimized kernel settings in standardized format
# Developer: Aaron Ho - 14/11/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle

# Internal package imports
from EX2GK.tools.general import proctools as ptools


def add_ne_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting electron density profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for electron density profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "NE"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For NE pedestal modelling
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [5.0e-1,3.0e-1,1.0e-1,8.0e-2,1.0e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,2.0e-1,7.0e-2,7.0e-2],[1.0e0,5.0e-1,2.0e-1,2.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 1.0
            GPS["FIT_ONAME"] = 'adam'
            GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
            GPS["FIT_EPSPAR"] = 1.0e-2
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [1.0e-1,1.0e-1,6.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[1.0e0,1.0e0,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 10.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 1.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [1.0e-1,1.0e-1,6.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[2.0e-1,1.0e-1,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 10.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_te_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting electron temperature profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for electron temperature profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "TE"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For TE pedestal modelling, pedestal is present in data but small
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [5.0e-1,3.0e-1,1.0e-1,1.0e-1,1.0e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,2.0e-1,7.0e-2,7.0e-2],[1.0e0,5.0e-1,2.0e-1,2.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 1.0
            GPS["FIT_ONAME"] = 'adam'
            GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
            GPS["FIT_EPSPAR"] = 1.0e-2
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,5.0e-1,8.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[1.0e0,1.0e0,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 10.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 1.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [2.0e-1,5.0e-1,8.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 10.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_ti_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting ion temperature profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for ion temperature profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "TI"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For TI pedestal modelling, pedestal is nearly imperceptible in the data but theoretically expected
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [5.0e-1,3.0e-1,1.0e-1,1.0e-1,1.0e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,2.0e-1,7.0e-2,7.0e-2],[1.0e0,5.0e-1,2.0e-1,2.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 1.0
            GPS["FIT_ONAME"] = 'adam'
            GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
            GPS["FIT_EPSPAR"] = 1.0e-2
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,1.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 12.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 5.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,1.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 12.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_nimp_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting impurity ion density profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for impurity ion density profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "NIMP"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For NIMP pedestal modelling, more for consistency rather than necessity
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [5.0e-1,9.0e-1,6.0e-1,1.0e-1,1.0e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,2.0e-1,7.0e-2,7.0e-2],[1.0e0,5.0e-1,2.0e-1,2.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 2.0
            GPS["FIT_ONAME"] = 'adam'
            GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
            GPS["FIT_EPSPAR"] = 1.0e-2
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 10.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 5.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 10.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_vt_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting toroidal flow velocity profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for toroidal flow velocity profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    # Note that this function is not really used, as AF is the poloidally symmetric quantity related to this
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "VT"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For VT pedestal modelling, more for consistency rather than necessity
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [5.0e-1,3.0e-1,1.0e-1,1.0e-1,1.0e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,2.0e-1,7.0e-2,7.0e-2],[1.0e0,5.0e-1,2.0e-1,2.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 4.0
            GPS["FIT_ONAME"] = 'adam'
            GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
            GPS["FIT_EPSPAR"] = 1.0e-2
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 10.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 5.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 10.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_af_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting angular frequency profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for angular frequency profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "AF"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For AF pedestal modelling, more for consistency rather than necessity
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [5.0e-1,3.0e-1,1.0e-1,1.0e-1,1.0e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,2.0e-1,7.0e-2,7.0e-2],[1.0e0,5.0e-1,2.0e-1,2.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 4.0
            GPS["FIT_ONAME"] = 'adam'
            GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
            GPS["FIT_EPSPAR"] = 1.0e-2
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 10.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 5.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 10.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_q_kernels(newstruct=None,option=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Adds GPR1D-specific kernel selections and settings optimized
    for fitting safety factor profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :kwarg option: int. Choice of kernel settings to add for safety factor profile fitting.

    :returns: dict. Object with identical structure as input object except with GPR1D settings appended to standard fields.
    """
    KD = None
    opt = None
    if isinstance(newstruct,dict):
        KD = newstruct
    else:
        KD = dict()
    if isinstance(option,(float,int)) and int(option) >= 0:
        opt = int(option)
    dtag = "Q"
    status = 0

    if KD is not None:
        KD[dtag+"GP_SETTINGS"] = []

        # For unconstrained EFIT Q profile
        if opt == 1:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [1.0e0,7.2e-1,5.0e-1,1.0e-1,1.01e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e-1,1.0e-2],[1.0e1,1.0e0,1.0e0,1.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 1.0
            GPS["FIT_ONAME"] = 'adadelta'
            GPS["FIT_OPARS"] = [1.0e-3,0.5]
            GPS["FIT_EPSPAR"] = 1.0e-3
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 5.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # For constrained EFIT Q profile
        elif opt == 2:
            GPS = dict()
            GPS["FIT_KNAME"] = "GwIG"
            GPS["FIT_KPARS"] = [1.0e0,3.0e-1,1.0e-1,1.0e-1,1.01e0,6.0e-1]
            GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e-1,1.0e-2],[1.0e1,1.0e0,1.0e0,1.0e-1]])
            GPS["FIT_RESTART"] = 4
            GPS["FIT_REGPAR"] = 1.0
            GPS["FIT_ONAME"] = 'adadelta'
            GPS["FIT_OPARS"] = [1.0e-3,0.5]
            GPS["FIT_EPSPAR"] = 1.0e-3
            GPS["ERR_KNAME"] = "RQ"
            GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
            GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
            GPS["ERR_RESTART"] = 4
            GPS["ERR_REGPAR"] = 5.0
            GPS["ERR_ONAME"] = 'adadelta'
            GPS["ERR_OPARS"] = [1.0e-3,0.5]
            GPS["ERR_EPSPAR"] = 1.0e-2
            KD[dtag+"GP_SETTINGS"].append(GPS)

        # Default back-up kernel, most generally robust selection but lacks specific features
        GPS = dict()
        GPS["FIT_KNAME"] = "RQ"
        GPS["FIT_KPARS"] = [5.0e-1,1.0e-1,3.0e0]
        GPS["FIT_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-2,1.0e0],[1.0e0,1.0e0,1.0e1]])
        GPS["FIT_RESTART"] = 4
        GPS["FIT_REGPAR"] = 5.0
        GPS["FIT_ONAME"] = 'adam'
        GPS["FIT_OPARS"] = [1.0e-2,0.4,0.8]
        GPS["FIT_EPSPAR"] = 1.0e-2
        GPS["ERR_KNAME"] = "RQ"
        GPS["ERR_KPARS"] = [2.0e-1,2.0e-1,4.0e0]
        GPS["ERR_KBOUNDS"] = np.atleast_2d([[1.0e-1,1.0e-1,1.0e0],[5.0e-1,5.0e-1,1.0e1]])
        GPS["ERR_RESTART"] = 4
        GPS["ERR_REGPAR"] = 10.0
        GPS["ERR_ONAME"] = 'adadelta'
        GPS["ERR_OPARS"] = [1.0e-3,0.5]
        GPS["ERR_EPSPAR"] = 1.0e-2
        KD[dtag+"GP_SETTINGS"].append(GPS)

    return KD


def add_fit_kernel_settings(stddata,newstruct=None):
    """
    JET-SPECIFIC FUNCTION
    Selects the GPR1D fit settings options to be added to the input
    object depending on the profile data that is available and the
    characteristics of this data.

    :arg stddata: dict. Standardized object containing the processed profiles.

    :kwarg newstruct: dict. Optional object in which standardized fit settings will be stored.

    :returns: dict. Standardized object with fit settings for each available profile inserted.
    """
    SD = None
    GD = None
    if isinstance(stddata,dict):
        SD = stddata
    if isinstance(newstruct,dict):
        GD = newstruct
    else:
        GD = SD

    if SD is not None and "CS_DIAG" in SD and re.match('^RHO((TOR)|(POL))N?$',SD["CS_DIAG"],flags=re.IGNORECASE):
        gflag = False
        ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

        ped_kopt = 0
        if "NERAW" in SD and SD["NERAW"].size > 0:
            xx = SD["NERAWX"]
            xe = SD["NERAWXEB"]
            yy = SD["NERAW"]
            ye = SD["NERAWEB"]
            efilt = (xx >= 0.8)
            bdiff = 0.0
            if np.any(efilt) and yy[efilt].size > 1:
                peddata = np.vstack((xx[efilt].flatten(),yy[efilt].flatten()))
                peddata = peddata[:,np.argsort(peddata[0,:])]
                bdiff = np.nanmin(np.diff(peddata[1,:].flatten()) / np.diff(peddata[0,:].flatten()))
            xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
            ymax = yy[xmin[0]] if xmin.size > 0 else 7.0e19
            oval = np.nanmax(xx[efilt]) if np.any(efilt) else None
            idxo = np.where(xx >= oval)[0][0] if oval is not None else -1
            nflag = True if gflag or ftflag else False
            if bdiff <= -ymax or nflag or yy[idxo] > 1.5e19:
                ped_kopt = 1
                gflag = True

        if "TERAW" in SD and SD["TERAW"].size > 0:
            xx = SD["TERAWX"]
            xe = SD["TERAWXEB"]
            yy = SD["TERAW"]
            ye = SD["TERAWEB"]
            efilt = (xx >= 0.8)
            bdiff = 0.0
            if np.any(efilt) and yy[efilt].size > 1:
                peddata = np.vstack((xx[efilt].flatten(),yy[efilt].flatten()))
                peddata = peddata[:,np.argsort(peddata[0,:])]
                bdiff = np.nanmin(np.diff(peddata[1,:].flatten()) / np.diff(peddata[0,:].flatten()))
            xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
            ymax = yy[xmin[0]] if xmin.size > 0 else 5.0e3
            oval = np.nanmax(xx[efilt]) if np.any(efilt) else None
            idxo = np.where(xx >= oval)[0][0] if oval is not None else -1
            tflag = True if gflag and ftflag else False
            if bdiff <= -ymax or tflag or yy[idxo] > 5.0e2:
                ped_kopt = 1
                gflag = True

        ped_kopt = 1   # Temporary for testing AUG CX adapter!!!
        GD["CS_DIAG"] = SD["CS_DIAG"]
        if "NERAW" in SD and SD["NERAW"].size > 0:
            GD = add_ne_kernels(GD,option=ped_kopt)
        if "TERAW" in SD and SD["TERAW"].size > 0:
            GD = add_te_kernels(GD,option=ped_kopt)
        if "TIRAW" in SD and SD["TIRAW"].size > 0:
            GD = add_ti_kernels(GD,option=ped_kopt)
        if "NIMPRAW" in SD and SD["NIMPRAW"].size > 0:
            GD = add_nimp_kernels(GD,option=ped_kopt)
#        if "VTRAW" in SD and SD["VTRAW"].size > 0:
#            GD = add_vt_kernels(GD,option=ped_kopt)
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
            GD = add_af_kernels(GD,option=ped_kopt)
        if "QRAW" in SD and SD["QRAW"].size > 0:
            GD = add_q_kernels(GD,option=ped_kopt)

    return GD


def add_derivative_constraint_data(gpstddata):
    """
    JET-SPECIFIC FUNCTION
    Inserts appropriate derivative constraints on the various profiles
    in order improve the robustness of the GP fit routine and to
    achieve more meaningful fits.

    :arg gpstddata: dict. Standardized object containing the processed profiles and GPR1D settings.

    :returns: dict. Standardized object with derivative constraints for profile to be fitted inserted.
    """
    GD = None
    if isinstance(gpstddata,dict):
        GD = gpstddata

    ftflag = True if "SHOTPHASE" in GD and GD["SHOTPHASE"] <= 0 else False

    ###  The following section adds profile derivative constraints for more robust GP fits
    if GD is not None and "CS_DIAG" in GD and re.match('^RHO((TOR)|(POL))N?$',GD["CS_DIAG"],flags=re.IGNORECASE):

        # Zero NE derivative constraint at rho_tor = 0.0, axis of symmetry
        if "NEGP_SETTINGS" in GD and isinstance(GD["NEGP_SETTINGS"],(list,tuple)):
            for ii in np.arange(0,len(GD["NEGP_SETTINGS"])):
                GD["NEGP_SETTINGS"][ii]["DX"] = np.array([0.0])
                GD["NEGP_SETTINGS"][ii]["DY"] = np.array([0.0])
                GD["NEGP_SETTINGS"][ii]["DYEB"] = np.array([0.0])

        # Zero TE derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TEGP_SETTINGS" in GD and isinstance(GD["TEGP_SETTINGS"],(list,tuple)):
            for ii in np.arange(0,len(GD["TEGP_SETTINGS"])):
                GD["TEGP_SETTINGS"][ii]["DX"] = np.array([0.0])
                GD["TEGP_SETTINGS"][ii]["DY"] = np.array([0.0])
                GD["TEGP_SETTINGS"][ii]["DYEB"] = np.array([0.0])

        # Zero TI derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TIGP_SETTINGS" in GD and isinstance(GD["TIGP_SETTINGS"],(list,tuple)):
            for ii in np.arange(0,len(GD["TIGP_SETTINGS"])):
                GD["TIGP_SETTINGS"][ii]["DX"] = np.array([0.0])
                GD["TIGP_SETTINGS"][ii]["DY"] = np.array([0.0])
                GD["TIGP_SETTINGS"][ii]["DYEB"] = np.array([0.0])

        # Zero AF derivative constraint at rho_tor = 0.0, axis of symmetry
        # Extra derivative constraint at inner-most point to avoid high core shear in GP fit due to lack of data
        if "AFGP_SETTINGS" in GD and isinstance(GD["AFGP_SETTINGS"],(list,tuple)):
            # This filter works since the radial coordinate with JET data is always rhopol or rhotor
            iidx = np.where(GD["AFRAWX"] == np.nanmin(GD["AFRAWX"]))[0][0]
            edcon = float(-GD["AFRAW"][iidx]) / 2.0 if GD["AFRAWX"][iidx] > 0.2 else None
            for ii in np.arange(0,len(GD["AFGP_SETTINGS"])):
                GD["AFGP_SETTINGS"][ii]["DX"] = np.array([0.0]) if edcon is None else np.array([0.0,0.2])
                GD["AFGP_SETTINGS"][ii]["DY"] = np.array([0.0]) if edcon is None else np.array([0.0,edcon])
                GD["AFGP_SETTINGS"][ii]["DYEB"] = np.array([0.0]) if edcon is None else np.array([0.0,0.25 * np.abs(edcon)])

        # Zero NIMP derivative constraint at rho_tor = 0.0, axis of symmetry
        if "NIMPGP_SETTINGS" in GD and isinstance(GD["NIMPGP_SETTINGS"],(list,tuple)):
            for ii in np.arange(0,len(GD["NIMPGP_SETTINGS"])):
                GD["NIMPGP_SETTINGS"][ii]["DX"] = np.array([0.0])
                GD["NIMPGP_SETTINGS"][ii]["DY"] = np.array([0.0])
                GD["NIMPGP_SETTINGS"][ii]["DYEB"] = np.array([0.0])

        # Zero Q derivative constraint at rho_tor = 0.0, axis of symmetry
        if "QGP_SETTINGS" in GD and isinstance(GD["QGP_SETTINGS"],(list,tuple)):
            for ii in np.arange(0,len(GD["QGP_SETTINGS"])):
                GD["QGP_SETTINGS"][ii]["DX"] = np.array([0.0])
                GD["QGP_SETTINGS"][ii]["DY"] = np.array([0.0])
                GD["QGP_SETTINGS"][ii]["DYEB"] = np.array([0.0])

    return GD


def define_fit_settings(stddata,extra_info=None,pickle_dir=None):
    """
    AUG-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Main call function to add optimized fit settings to the input
    data structure, for use in the GPR1D porfile fitting tool.

    The output container should be in the same generalized format and
    structure as other implementations, including the kernel selection
    and fit settings for the GPR1D profile fitting tool. The concept
    is that any user-defined scripts defined strictly on the data
    structure here should be portable to any other implementation,
    provided that the fit settings have been optimized for that
    implementation.

    :arg stddata: dict. Standardized object containing processed profile data.

    :kwarg extra_info: dict. Generic container for implementation-specific parameters and options.

    :kwarg pickle_dir: str. Directory in which to save the extracted and computed data into a Python pickle file, specifying this toggles saving.

    :returns: dict. Object identical in structure to input object, except containing standardized fit settings and constraints for fitting.
    """
    SD = None
    GD = None
    infodict = dict()
    pdir = ''
    if isinstance(stddata,dict):
        SD = stddata
    elif isinstance(stddata,str) and os.path.isfile(stddata):
        SD = ptools.unpickle_this(stddata)
    else:
        raise TypeError('Invalid format for input standardized data structure.')
    if isinstance(extra_info,dict):
        infodict = copy.deepcopy(extra_info)
    if pickle_dir and isinstance(pickle_dir,str):
        pdir = pickle_dir
        if not pdir.endswith('/'):
            pdir = pdir + '/'

    if SD is not None:

        # Note that the default structure here is actually the input dict, not a new dict.
        GD = add_fit_kernel_settings(SD,newstruct=None)
        GD = add_derivative_constraint_data(GD)

        GD["LAST_UPDATE"] = datetime.date

        # Save extracted shot data into a "pickle" file
        if pdir:
            if "TWNUM" in infodict and isinstance(infodict["TWNUM"],(int,float)) and int(infodict["TWNUM"]) > 0:
                twstr = "_T%02d" % (int(infodict["TWNUM"]))
            snstr = "%06d" % (int(GD["SHOT"]))
            stf = ptools.pickle_this(GD,'S'+snstr+twstr+'.p',pdir)
            if stf != 0:
                print("Error encountered in pickling process... recommend to do manually.")

    return GD
