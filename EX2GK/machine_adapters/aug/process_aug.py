# Script with functions to process extracted AUG data into intermediate custom structure
# Developer: Aaron Ho - 30/01/2018

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
import warnings
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz

# Internal package imports
from EX2GK.tools.general import proctools as ptools, phystools as ftools

def transfer_generic_data(rawdata,newstruct=None,gpcoord=False,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Transfers any crucial data fields which require little to no
    additional processing from the raw extracted data structure into
    a new processed data structure. This concept prevents the
    accumulation of unnecessary data in the final data structure.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg gpcoord: bool. Optional flag to toggle GPR fitting of coordinate system vectors.

    :kwarg fdebug: bool. Optional flag to print statements useful for debugging.

    :returns: dict. Implementation-specific object with general metadata transferred.
    """
    RD = None
    PD = None
    if isinstance(rawdata,dict):
        RD = rawdata
    if isinstance(newstruct,dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None:

        # Transfers generic metadata
        PD["DEVICE"] = RD["DEVICE"] if "DEVICE" in RD else "AUG"
        PD["INTERFACE"] = RD["INTERFACE"] if "INTERFACE" in RD else "UNKNOWN"
        PD["SHOT"] = int(RD["SHOT"]) if "SHOT" in RD else None
        PD["T1"] = float(RD["T1"]) if "T1" in RD else None
        PD["T2"] = float(RD["T2"]) if "T2" in RD else None
        PD["SHOTPHASE"] = int(RD["SHOTPHASE"]) if "SHOTPHASE" in RD else -1
        PD["WINDOW"] = RD["WINDOW"] if "WINDOW" in RD else -1

        PD["GPCOORDFLAG"] = True if gpcoord else False

        PD["WALLMAT"] = 'C'

    if fdebug:
        print("process_aug.py: transfer_generic_data() completed.")

    return PD


def define_equilibrium(rawdata,newstruct=None,equilibrium=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Specifies the equilibrium to be used based on a pre-defined
    hierarchy, placing the equilibrium selected by the user at
    the highest preference if supplied.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg equilibrium: str. Optional flag to select the equilibrium to be used for futher processing.

    :kwarg fdebug: bool. Optional flag to print statements useful for debugging.

    :returns: dict. Implementation-specific object with equilibrium selection defined.
    """
    RD = None
    PD = None
    if isinstance(rawdata,dict):
        RD = rawdata
    if isinstance(newstruct,dict):
        PD = newstruct
    else:
        PD = dict()
    eqlist = ['IDE','EQH','EQI','FPP']          # This list is in order of descending preference, do not tamper with it!

    if RD is not None and "MAP" in RD:
        m = RD["MAP"]
        eqgood = []
        reqvars = ['PFM/time/Ri|Zj','TFLx/time/PFL','Qpsi/time/PFL','Vol/time/PFL','Area/time/PFL','SSQ/time/']
        for ii in np.arange(0,len(eqlist)):
            gflag = True
            for var in reqvars:
                if gflag:
                    if not (eqlist[ii]+'/'+var in m and RD[m[eqlist[ii]+'/'+var]] is not None):
                        gflag = False
            if gflag:
#                print(eqlist[ii])
                eqgood.append(eqlist[ii])

        if 'IDE' in eqgood:
            PD["EQLIST"] = ['IDE']
        elif isinstance(equilibrium,str) and re.match(r'^all$',equilibrium,flags=re.IGNORECASE):
            PD["EQLIST"] = eqlist
        elif isinstance(equilibrium,str) and equilibrium.lower() in eqlist and equilibrium.lower() in eqgood:
            PD["EQLIST"] = [equilibrium.lower()]
        elif len(eqgood) > 0:
            PD["EQLIST"] = [eqgood[0]]
        if "EQLIST" not in PD:
            raise ValueError("Required coordinate system data not found.")

    if fdebug:
        print("process_aug.py: define_equilibrium() completed.")

    return PD


def unpack_general_data(rawdata,newstruct=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Unpacks the required general data from the raw extracted data container,
    pre-processes it with statistical averaging if necessary, and stores it
    in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg fdebug: bool. Optional flag to print statements useful for debugging.

    :returns: dict. Implementation-specific object with processed general data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata,dict):
        RD = rawdata
    if isinstance(newstruct,dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None

        warnings.filterwarnings("ignore",category=FutureWarning)

        # Specification of which magnetic equilibrium entry to use, default is standard FPP
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['FPP']

        # Plasma current
        vals = RD[m['FPC/IpiFP']] if 'FPC/IpiFP' in m else None
        (PD["IP"],PD["IPEB"],stdmean,navgmap) = ptools.filtered_average(vals)
        PD["IPEB"] = np.sqrt(np.power(PD["IPEB"],2.0) + np.power(0.01 * PD["IP"] + 1.0e4,2.0)) if PD["IP"] is not None and PD["IPEB"] is not None else None

        # NBI power and error
        vals = RD[m['NIS/PNI/T-B/']] if 'NIS/PNI/T-B' in m else None
        errs = 0.1 * vals / 1.96 if vals is not None else None                         # 10% error, assumed reported at 95% confidence
        (PD["PNBI"],PD["PNBIEB"],stdmean,navgmap) = ptools.filtered_average(vals,value_errors=errs)

        # NBI power loss and error
        vals = RD[m['TOT/SHINE_TH/time/']] if 'TOT/SHINE_TH/time/' in m else None
        (PD["LNBI"],PD["LNBIEB"],stdmean,navgmap) = ptools.filtered_average(vals)

        # NBI power per PINI, used for separation of CX measurement locations due to beam switching
        if 'NIS/PNIQ/T-B/' in m and RD[m['NIS/PNIQ/T-B/']] is not None:
            PD["TNBI"] = RD["T_"+m['NIS/PNIQ/T-B/']].squeeze()
            PD["NBIQ1"] = RD[m['NIS/PNIQ/T-B/']][:,0,0].squeeze()
            PD["NBIQ2"] = RD[m['NIS/PNIQ/T-B/']][:,1,0].squeeze()
            PD["NBIQ3"] = RD[m['NIS/PNIQ/T-B/']][:,2,0].squeeze()
            PD["NBIQ4"] = RD[m['NIS/PNIQ/T-B/']][:,3,0].squeeze()
            PD["NBIQ5"] = RD[m['NIS/PNIQ/T-B/']][:,0,1].squeeze()
            PD["NBIQ6"] = RD[m['NIS/PNIQ/T-B/']][:,1,1].squeeze()
            PD["NBIQ7"] = RD[m['NIS/PNIQ/T-B/']][:,2,1].squeeze()
            PD["NBIQ8"] = RD[m['NIS/PNIQ/T-B/']][:,3,1].squeeze()

        # ICRH power and error
        vals = RD[m['ICP/PICRH/T-B/']] if 'ICP/PICRH/T-B/' in m else None         # 25% error, assumed reported at 95% confidence           
        (PD["PICRH"],PD["PICRHEB"],stdmean,navgmap) = ptools.filtered_average(vals,percent_errors=0.125)

        # ICRH power loss and error
        vals = RD[m['ICP/PICRFl/T-B/']] if 'ICP/PICRFl/T-B/' in m else None
        (PD["LICRH"],PD["LICRHEB"],stdmean,navgmap) = ptools.filtered_average(vals)

        # ECRH power and error
        vals = RD[m['ECS/PECRH/T-B/']] if 'ECS/PECRH/T-B/' in m else None         # 10% error, assumed reported at 95% confidence
        (PD["PECRH"],PD["PECRHEB"],stdmean,navgmap) = ptools.filtered_average(vals,value_errors=errs)

        # ECRH power loss and error
        PD["LECRH"] = 0.0 if PD["PECRH"] is not None else None
        PD["LECRHEB"] = 0.0 if PD["PECRH"] is not None else None

        # Ohmic power
        vals = RD[m['TOT/P_OH/time/']] if 'TOT/P_OH/time/' in m else None
        (PD["POHM"],PD["POHMEB"],stdmean,navgmap) = ptools.filtered_average(vals)

        # Line-averaged Z-effective
        vals = RD[m['ZES/axk-H1/TimeBase/']] if 'ZES/axk-H1/TimeBase/' in m else None
        (PD["ZEFA"],PD["ZEFAEB"],stdmean,nz) = ptools.filtered_average(vals)              # This one is recommended
        vals = RD[m['ZES/Zeff/TimeBase/']] if 'ZES/Zeff/TimeBase/' in m else None
        (PD["ZEFF"],PD["ZEFFEB"],stdmean,nz) = ptools.filtered_average(vals)

        # Total stored plasma energy, calculated using equilibrium code
        vals = RD[m['TOT/Wmhd/time/']] if 'TOT/Wmhd/time/' in m else None
        (PD["TWE"],PD["TWEEB"],stdmean,ntwe) = ptools.filtered_average(vals)

        # Total stored plasma energy, calculated using magnetic measurements
        vals = RD[m['BMA/Wdia/time/']] if 'BMA/Wdia/time/' in m else None
        (PD["TWD"],PD["TWDEB"],stdmean,ntwd) = ptools.filtered_average(vals)

        # Total beta, calculated using equilibrium code
        vals = RD[m['TOT/beta_N/time/']] if 'TOT/beta_N/time/' in m else None
        (PD["BETANE"],dummy,stdmean,nbetane) = ptools.filtered_average(vals)

        # Total beta, calculated using magnetic measurements
        PD["BETAND"] = None

        # Poloidal beta, calculated using equilibrium code
        PD["BETAPE"] = None

        # Poloidal beta, calculated using magnetic measurements
        vals = RD[m['BMA/betdia/time/']] if 'BMA/betdia/time/' in m else None
        (PD["BETAPD"],dummy,stdmean,nbetane) = ptools.filtered_average(vals)

        # Toroidal magnetic field at magnetic axis
        vals = RD[m['FPC/BTF/TIMEF/']] if 'FPC/BTF/TIMEF/' in m else None
        (PD["BMAG"],PD["BMAGEB"],stdmean,nbvac) = ptools.filtered_average(vals)

        # Toroidal magnetic field without plasma - signal not known so taking field @ magnetic axis
        PD["BVAC"] = PD["BMAG"]
        PD["BVACEB"] = PD["BMAGEB"]

        # Total neutron rate
        if 'ENR/NRATE_II/Timebase/' in m and RD[m['ENR/NRATE_II/Timebase/']] is not None:
            (PD["NEUT"],PD["NEUTEB"],stdmean,nnt) = ptools.filtered_average(RD[m['ENR/NRATE_II/Timebase/']],percent_errors=0.01)     # Assumed 1% error
        elif 'ENR/NRATE/Timebase/' in m and RD[m['ENR/NRATE_II/Timebase/']] is not None:
            (PD["NEUT"],PD["NEUTEB"],stdmean,nnt) = ptools.filtered_average(RD[m['ENR/NRATE/Timebase/']],percent_errors=0.01)        # Assumed 1% error
        else:
            PD["NEUT"] = None
            PD["NEUTEB"] = None

        # Primary plasma composition
        totrate = 0.0
        totrateeb = 0.0
        vals = RD[m['UVS/H_tot/time/']] if 'UVS/H_tot/time/' in m else None
        (hrate,hrateeb,stdmean,nhr) = ptools.filtered_average(vals)
        if hrate is not None and hrate > 1.0e3:
            totrate = totrate + np.abs(hrate)
            totrateeb = np.sqrt(np.power(totrateeb,2.0) + np.power(hrateeb,2.0))
        vals = RD[m['UVS/D_tot/time/']] if 'UVS/D_tot/time/' in m else None
        (drate,drateeb,stdmean,ndr) = ptools.filtered_average(vals)
        if drate is not None and drate > 1.0e3:
            totrate = totrate + np.abs(drate)
            totrateeb = np.sqrt(np.power(totrateeb,2.0) + np.power(drateeb,2.0))
        trate = None
        trateeb = None
        if trate is not None and trate > 1.0e3:
            totrate = totrate + np.abs(trate)
            totrateeb = np.sqrt(np.power(totrateeb,2.0) + np.power(trateeb,2.0))
        vals = RD[m['UVS/He_tot/time/']] if 'UVS/He_tot/time/' in m else None
        (herate,herateeb,stdmean,nher) = ptools.filtered_average(vals)
        if herate is not None and herate > 1.0e3:
            totrate = totrate + np.abs(herate)
            totrateeb = np.sqrt(np.power(totrateeb,2.0) + np.power(herateeb,2.0))

        PD["HFRAC"] = np.abs(hrate / totrate) if hrate is not None and hrate > 1.0e3 else None
        PD["HFRACEB"] = PD["HFRAC"] * totrateeb / totrate if PD["HFRAC"] is not None else None
        PD["DFRAC"] = np.abs(drate / totrate) if drate is not None and drate > 1.0e3 else None
        PD["DFRACEB"] = PD["DFRAC"] * totrateeb / totrate if PD["DFRAC"] is not None else None
        PD["TFRAC"] = np.abs(trate / totrate) if trate is not None and trate > 1.0e3 else None
        PD["TFRACEB"] = PD["TFRAC"] * totrateeb / totrate if PD["TFRAC"] is not None else None
        PD["HEFRAC"] = np.abs(herate / totrate) if herate is not None and herate > 1.0e3 else None
        PD["HEFRACEB"] = PD["HEFRAC"] * totrateeb / totrate if PD["HEFRAC"] is not None else None

    if fdebug:
        print("process_aug.py: unpack_general_data() completed.")

    return PD


def unpack_coord_data(rawdata,newstruct=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Unpacks the required coordinate data from the raw extracted data container,
    pre-processes it with statistical averaging if necessary, and stores it in
    another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :returns: dict. Implementation-specific object with processed coordinate data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata,dict):
        RD = rawdata
    if isinstance(newstruct,dict):
        PD = newstruct
    else:
        PD = dict()

    logfitpar = 1.0e-5        # Factor to avoid computing logarithm of zero

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None

        warnings.filterwarnings("ignore",category=FutureWarning)

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['FPP']
        qsf = eqlist[0].upper() if len(eqlist) > 0 else None

        # Radial coordinate of magnetic axis
        tag = 'SSQ/time/'
        ntag = 'SSQnam//'
        stag = "Rmag"
        vtag = "RMAG"         # R coord of magnetic axis
        PD[vtag] = None
        for ii in np.arange(0,len(eqlist)):
            sidx = 14
            if eqlist[ii]+'/'+ntag in m and RD[m[eqlist[ii]+'/'+ntag]] is not None:
                idxv = np.where(RD[m[eqlist[ii]+'/'+ntag]] == stag)[0]
                sidx = idxv[0] if len(idxv) > 0 else sidx
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]

        # Vertical coordinate of magnetic axis
        tag = 'SSQ/time/'
        ntag = 'SSQnam//'
        stag = "Zmag"
        vtag = "ZMAG"         # Z coord of magnetic axis
        PD[vtag] = None
        for ii in np.arange(0,len(eqlist)):
            sidx = 15
            if eqlist[ii]+'/'+ntag in m and RD[m[eqlist[ii]+'/'+ntag]] is not None:
                idxv = np.where(RD[m[eqlist[ii]+'/'+ntag]] == stag)[0]
                sidx = idxv[0] if len(idxv) > 0 else sidx
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]

        # Radial coordinate of X-point
        tag = 'SSQ/time/'
        ntag = 'SSQnam//'
        stag = "Rxpu"
        vtag = "RXP"
        PD[vtag] = None
        for ii in np.arange(0,len(eqlist)):
            sidx = 4
            if eqlist[ii]+'/'+ntag in m and RD[m[eqlist[ii]+'/'+ntag]] is not None:
                idxv = np.where(RD[m[eqlist[ii]+'/'+ntag]] == stag)[0]
                sidx = idxv[0] if len(idxv) > 0 else sidx
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]

        # Vertical coordinate of X-point
        tag = 'SSQ/time/'
        ntag = 'SSQnam//'
        stag = "Zxpu"
        vtag = "ZXP"
        PD[vtag] = None
        for ii in np.arange(0,len(eqlist)):
            sidx = 5
            if eqlist[ii]+'/'+ntag in m and RD[m[eqlist[ii]+'/'+ntag]] is not None:
                idxv = np.where(RD[m[eqlist[ii]+'/'+ntag]] == stag)[0]
                sidx = idxv[0] if len(idxv) > 0 else sidx
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]][sidx]

        # Normalized poloidal flux as a function of R coord and Z coord
        tag = 'PFM/time/Ri|Zj'
        vtag = "PFXMAP"        # Poloidal flux map
        rtag = "RMAP"
        ztag = "ZMAP"
        ntag = "PFNMAP"
        vtaga = "FBND"       # PSI at plasma boundary
        vtagb = "FAXS"       # PSI at magnetic axis
        PD[vtag] = None
        PD[rtag] = None
        PD[ztag] = None
        PD[ntag] = None
        for ii in np.arange(0,len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                (PD[rtag],PD[ztag]) = np.meshgrid(RD["Z_"+m[eqlist[ii]+'/'+tag]],RD["R_"+m[eqlist[ii]+'/'+tag]])
                # Finding polodial flux at magnetic axis and LCFS, used to undo normalization
                if "RXP" in PD and PD["RXP"] is not None and "ZXP" in PD and PD["ZXP"] is not None:
                    temp = ftools.rz2pf(PD[vtag],PD[rtag],PD[ztag],PD["RXP"],PD["ZXP"])
                    PD[vtaga] = float(temp)
                if "RMAG" in PD and PD["RMAG"] is not None and "ZMAG" in PD and PD["ZMAG"] is not None:
                    temp = ftools.rz2pf(PD[vtag],PD[rtag],PD[ztag],PD["RMAG"],PD["ZMAG"])
                    PD[vtagb] = float(temp)
                if vtaga in PD and vtagb in PD and PD[vtaga] is not None and PD[vtagb] is not None and "RMAG" in PD and PD["RMAG"] is not None:
                    PD[ntag] = (PD[vtagb] - PD[vtag]) / (PD[vtagb] - PD[vtaga])
                    hfilt = (PD[rtag] < PD["RMAG"])
                    PD[ntag][hfilt] = -PD[ntag][hfilt]
            if len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                PD[eqtag][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                (PD[eqtag][rtag],PD[eqtag][ztag]) = np.meshgrid(RD["Z_"+m[eqlist[ii]+'/'+tag]],RD["R_"+m[eqlist[ii]+'/'+tag]])
                # Finding polodial flux at magnetic axis and LCFS, used to undo normalization
                if "RXP" in PD[eqtag] and PD[eqtag]["RXP"] is not None and "ZXP" in PD[eqtag] and PD[eqtag]["ZXP"] is not None:
                    temp = ftools.rz2pf(PD[eqtag][vtag],PD[eqtag][rtag],PD[eqtag][ztag],PD[eqtag]["RXP"],PD[eqtag]["ZXP"])
                    PD[eqtag][vtaga] = float(temp)
                if "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None and "ZMAG" in PD[eqtag] and PD[eqtag]["ZMAG"] is not None:
                    temp = ftools.rz2pf(PD[eqtag][vtag],PD[eqtag][rtag],PD[eqtag][ztag],PD[eqtag]["RMAG"],PD[eqtag]["ZMAG"])
                    PD[eqtag][vtagb] = float(temp)
                if vtaga in PD[eqtag] and vtagb in PD[eqtag] and PD[eqtag][vtaga] is not None and PD[eqtag][vtagb] is not None and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                    PD[eqtag][ntag] = (PD[eqtag][vtagb] - PD[eqtag][vtag]) / (PD[eqtag][vtagb] - PD[eqtag][vtaga])
                    hfilt = (PD[eqtag][rtag] < PD[eqtag]["RMAG"])
                    PD[eqtag][ntag][hfilt] = -PD[eqtag][ntag][hfilt]

        # Major radius as a function of normalized poloidal flux
        vtaga = "PFNI"
        vtagb = "PFNO"
        xtaga = "RHF2PFN"
        xtagb = "RLF2PFN"
        PD[vtaga] = None
        PD[vtagb] = None
        PD[xtaga] = None
        PD[xtagb] = None
        for ii in np.arange(0,len(eqlist)):
            if PD["PFNMAP"] is not None and (PD[vtaga] is None or PD[vtagb] is None):
                rmag = float(PD["RMAG"]) if PD["RMAG"] is not None else 1.7
                zmag = float(PD["ZMAG"]) if PD["ZMAG"] is not None else 0.05
                PD[xtaga] = np.linspace(0.75,rmag,100)[::-1]
                PD[vtaga] = ftools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],PD[xtaga],zmag)
                nfilt = np.all([PD[vtaga] >= -1.0,PD[vtaga] < 0.0],axis=0)
                PD[xtaga] = np.hstack((rmag,PD[xtaga][nfilt]))
                PD[vtaga] = np.hstack((1.0e-3,np.abs(PD[vtaga][nfilt])))
                PD[xtagb] = np.linspace(rmag,2.67,100)
                PD[vtagb] = ftools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],PD[xtagb],zmag)
                nfilt = np.all([PD[vtagb] > 0.0,PD[vtagb] <= 1.0],axis=0)
                PD[xtagb] = np.hstack((rmag,PD[xtagb][nfilt]))
                PD[vtagb] = np.hstack((1.0e-3,np.abs(PD[vtagb][nfilt])))
            if len(eqlist) > 1 and PD[eqlist[ii].upper()]["PFNMAP"] is not None:
                eqtag = eqlist[ii].upper()
                rmag = float(PD[eqtag]["RMAG"]) if PD[eqtag]["RMAG"] is not None else 1.7
                zmag = float(PD[eqtag]["ZMAG"]) if PD[eqtag]["ZMAG"] is not None else 0.05
                PD[eqtag][xtaga] = np.linspace(0.75,rmag,100)[::-1]
                PD[eqtag][vtaga] = ftools.rz2pf(PD[eqtag]["PFNMAP"],PD[eqtag]["RMAP"],PD[eqtag]["ZMAP"],PD[eqtag][xtaga],zmag)
                nfilt = np.all([PD[eqtag][vtaga] >= -1.0,PD[eqtag][vtaga] < 0.0],axis=0)
                PD[eqtag][xtaga] = np.hstack((rmag,PD[eqtag][xtaga][nfilt]))
                PD[eqtag][vtaga] = np.hstack((1.0e-3,np.abs(PD[eqtag][vtaga][nfilt])))
                PD[eqtag][xtagb] = np.linspace(rmag,2.67,100)
                PD[eqtag][vtagb] = ftools.rz2pfx(PD[eqtag]["PFNMAP"],PD[eqtag]["RMAP"],PD[eqtag]["ZMAP"],PD[eqtag][xtagb],zmag)
                nfilt = np.all([PD[eqtag][vtagb] > 0.0,PD[eqtag][vtagb] <= 1.0],axis=0)
                PD[eqtag][xtagb] = np.hstack((rmag,PD[eqtag][xtagb][nfilt]))
                PD[eqtag][vtagb] = np.hstack((1.0e-3,np.abs(PD[eqtag][vtagb][nfilt])))

        # Safety factor profile
        tag = 'Qpsi/time/PFL'
        vtag = "Q"            # Safety factor
        xtag = "QPFX"
        ntag = "QPFN"
        for ii in np.arange(0,len(eqlist)):
            eqtag = eqlist[ii].upper()
            PD[vtag+eqtag] = RD[m[eqlist[ii]+'/'+tag]] if eqlist[ii]+'/'+tag in m else None
            PD[xtag+eqtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag+eqtag] is not None else None
            if PD[xtag+eqtag] is not None:
                axid = np.where(np.diff(PD[xtag+eqtag]) < 0.0)[0][0]
                PD[vtag+eqtag] = PD[vtag+eqtag][axid::-1]
                PD[xtag+eqtag] = PD[xtag+eqtag][axid::-1]
            if PD["FBND"] is not None and PD["FAXS"] is not None:
                PD[ntag+eqtag] = (PD["FAXS"] - PD[xtag+eqtag]) / (PD["FAXS"] - PD["FBND"])
                nfilt = np.all([PD[ntag+eqtag] >= 0.0,PD[ntag+eqtag] <= 1.0],axis=0)
                PD[xtag+eqtag] = PD[xtag+eqtag][nfilt]
                PD[vtag+eqtag] = PD[vtag+eqtag][nfilt]
                PD[ntag+eqtag] = PD[ntag+eqtag][nfilt]
            PD[vtag+"EB"+eqtag] = np.zeros(PD[vtag+eqtag].shape) if PD[vtag+eqtag] is not None else None
            PD["N"+vtag+eqtag] = None
            if len(eqlist) > 1:
                PD[eqtag][vtag] = PD[vtag+eqtag].copy() if PD[vtag+eqtag] is not None else None
                PD[eqtag][xtag] = PD[xtag+eqtag].copy() if PD[vtag+eqtag] is not None else None
                PD[eqtag][ntag] = PD[ntag+eqtag].copy() if PD[vtag+eqtag] is not None else None
#                PD[eqtag][vtag+"EB"] = PD[vtag+"EB"+eqtag].copy() if PD[vtag+"EB"+eqtag] is not None else None
#                PD[eqtag]["N"+vtag] = PD["N"+vtag+eqtag].copy() if PD["N"+vtag+eqtag] is not None else None

        # Toroidal flux profile, used to define toroidal flux corrdinates
        tag = 'TFLx/time/PFL'
        vtag = "TFX"         # Toroidal magnetic flux(PSI_norm)
        xtag = "PFX2TFX"
        ntag = "PFN2TFX"
        PD[vtag] = None
        PD[xtag] = None
        PD[ntag] = None
        for ii in np.arange(0,len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                if PD[xtag] is not None:
                    axid = np.where(np.diff(PD[xtag]) < 0.0)[0][0]
                    PD[vtag] = PD[vtag][axid::-1]
                    PD[xtag] = PD[xtag][axid::-1]
                if PD["FBND"] is not None and PD["FAXS"] is not None:
                    PD[ntag] = (PD["FAXS"] - PD[xtag]) / (PD["FAXS"] - PD["FBND"])
                    nfilt = np.all([PD[ntag] >= 0.0,PD[ntag] <= 1.0],axis=0)
                    PD[xtag] = np.hstack((PD["FAXS"],PD[xtag][nfilt]))
                    PD[vtag] = np.hstack((0.0,PD[vtag][nfilt]))
                    PD[ntag] = np.hstack((0.0,PD[ntag][nfilt]))
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                PD[eqtag][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqtag][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqtag][vtag] is not None else None
                if PD[eqtag][xtag] is not None:
                    axid = np.where(np.diff(PD[xtag]) < 0.0)[0][0]
                    PD[eqtag][vtag] = PD[eqtag][vtag][axid::-1]
                    PD[eqtag][xtag] = PD[eqtag][xtag][axid::-1]
                if PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag][ntag] = (PD[eqtag]["FAXS"] - PD[eqtag][xtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])
                    nfilt = np.all([PD[eqtag][ntag] >= 0.0,PD[eqtag][ntag] <= 1.0],axis=0)
                    PD[eqtag][xtag] = np.hstack((PD[eqtag]["FAXS"],PD[eqtag][xtag][nfilt]))
                    PD[eqtag][vtag] = np.hstack((0.0,PD[eqtag][vtag][nfilt]))
                    PD[eqtag][ntag] = np.hstack((0.0,PD[eqtag][ntag][nfilt]))
        # If toroidal flux profile is unavailable, uses poloidal flux profile in combination with safety factor profile to calculate it
        if PD["TFX"] is None and qdda is not None and PD["FBND"] is not None and PD["FAXS"] is not None:
            qval = PD["Q"+qdda].copy()
            qerr = PD["QEB"+qdda].copy()
            qpfn = PD["QPFN"+qdda].copy()
            pfntemp = qpfn.copy()
            pfntemp[pfntemp < 0.0] = 0.0
            tfxtemp = np.zeros(pfntemp.shape)
            psitemp = qpfn * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
            logq = np.log(qval + logfitpar)
            pqfunc = interp1d(psitemp,logq,kind='cubic',bounds_error=False)
            testpsi = np.linspace(PD["FAXS"],PD["FBND"] - 1.0e-5,1001)
            qtemp = np.exp(pqfunc(testpsi)) - logfitpar
            for pp in np.arange(1,pfntemp.size):
                idxs = np.where(testpsi >= psitemp[pp])[0]
                jj = idxs[0]+1 if idxs.size > 0 and idxs[0]+1 < testpsi.size else None
                tfxtemp[pp] = np.trapz(qtemp[:jj],testpsi[:jj]) if jj is not None else np.trapz(qtemp,testpsi)
            PD[vtag] = -2.0 * np.pi * tfxtemp
            PD[xtag] = pfntemp
        if len(eqlist) > 1:
            for ii in np.arange(0,len(eqlist)):
                eqtag = eqlist[ii].upper()
                if PD[eqtag]["TFX"] is None and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    qval = PD[eqtag]["Q"].copy()
                    qerr = PD[eqtag]["QEB"].copy()
                    qpfn = PD[eqtag]["QPFN"].copy()
                    pfntemp = qpfn.copy()
                    pfntemp[pfntemp < 0.0] = 0.0
                    tfntemp = np.zeros(pfntemp.shape)
                    psitemp = qpfn * (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"]) + PD[eqtag]["FAXS"]
                    logq = np.log(qval + logfitpar)
                    pqfunc = interp1d(psitemp,logq,kind='cubic',bounds_error=False)
                    testpsi = np.linspace(PD[eqtag]["FAXS"],PD[eqtag]["FBND"] - 1.0e-5,1001)
                    qtemp = np.exp(pqfunc(testpsi)) - logfitpar
                    for pp in np.arange(1,pfntemp.size):
                        idxs = np.where(testpsi >= psitemp[pp])[0] if PD[eqtag]["FBND"] >= PD[eqtag]["FAXS"] else np.where(testpsi <= psitemp[pp])[0]
                        jj = idxs[0]+1 if idxs.size > 0 and idxs[0]+1 < testpsi.size else None
                        tfntemp[pp] = np.trapz(qtemp[:jj],testpsi[:jj]) if jj is not None else np.trapz(qtemp,testpsi)
                    PD[eqtag][vtag] = -2.0 * np.pi * tfntemp
                    PD[eqtag][xtag] = pfntemp

        # Flux surface volume as a function of normalized poloidal flux
        tag = 'Vol/time/PFL'
        vtag = "VOL"         # Flux surface volume(PSI_norm)
        dtag = "DVOL"        # Derivative with respect to PSI_norm
        xtag = "PFX2VOL"
        ntag = "PFN2VOL"
        PD[vtag] = None
        PD[dtag] = None
        PD[xtag] = None
        PD[ntag] = None
        for ii in np.arange(0,len(eqlist)):
            if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]][::2]
                PD[dtag] = RD[m[eqlist[ii]+'/'+tag]][1::2]
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                while PD[dtag].size < PD[xtag].size:
                    PD[dtag] = np.hstack((PD[dtag],np.NaN))
                if PD[xtag] is not None:
                    axid = np.where(np.diff(PD[xtag]) < 0.0)[0][0]
                    PD[vtag] = PD[vtag][axid::-1]
                    PD[xtag] = PD[xtag][axid::-1]
                if PD["FBND"] is not None and PD["FAXS"] is not None:
                    PD[ntag] = (PD["FAXS"] - PD[xtag]) / (PD["FAXS"] - PD["FBND"])
                    nfilt = np.all([PD[ntag] >= 0.0,PD[ntag] <= 1.0],axis=0)
                    PD[xtag] = np.hstack((PD["FAXS"],PD[xtag][nfilt]))
                    PD[vtag] = np.hstack((0.0,PD[vtag][nfilt]))
                    PD[ntag] = np.hstack((0.0,PD[ntag][nfilt]))
            if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]][::2]
                PD[eqlist[ii].upper()][dtag] = RD[m[eqlist[ii]+'/'+tag]][1::2]
                PD[eqlist[ii].upper()][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqlist[ii].upper()][vtag] is not None else None
                while PD[eqlist[ii].upper()][dtag].size < PD[eqlist[ii].upper()][xtag].size:
                    PD[eqlist[ii].upper()][dtag] = np.hstack((PD[eqlist[ii].upper()][dtag],np.NaN))
                if PD[eqtag][xtag] is not None:
                    axid = np.where(np.diff(PD[xtag]) < 0.0)[0][0]
                    PD[eqtag][vtag] = PD[eqtag][vtag][axid::-1]
                    PD[eqtag][xtag] = PD[eqtag][xtag][axid::-1]
                if PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag][ntag] = (PD[eqtag]["FAXS"] - PD[eqtag][xtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])
                    nfilt = np.all([PD[eqtag][ntag] >= 0.0,PD[eqtag][ntag] <= 1.0],axis=0)
                    PD[eqtag][xtag] = np.hstack((PD[eqtag]["FAXS"],PD[eqtag][xtag][nfilt]))
                    PD[eqtag][vtag] = np.hstack((0.0,PD[eqtag][vtag][nfilt]))
                    PD[eqtag][ntag] = np.hstack((0.0,PD[eqtag][ntag][nfilt]))

        # Flux surface area as a function of normalized poloidal flux
        tag = 'Area/time/PFL'
        vtag = "AREA"        # Flux surface surface area(PSI_norm)
        dtag = "DAREA"       # Derivative with respect to PSI_norm
        xtag = "PFX2AREA"
        ntag = "PFN2AREA"
        PD[vtag] = None
        PD[dtag] = None
        PD[xtag] = None
        PD[ntag] = None
        for ii in np.arange(0,len(eqlist)):
            if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]][::2]
                PD[dtag] = RD[m[eqlist[ii]+'/'+tag]][1::2]
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                while PD[dtag].size < PD[xtag].size:
                    PD[dtag] = np.hstack((PD[dtag],np.NaN))
                if PD[xtag] is not None:
                    axid = np.where(np.diff(PD[xtag]) < 0.0)[0][0]
                    PD[vtag] = PD[vtag][axid::-1]
                    PD[xtag] = PD[xtag][axid::-1]
                if PD["FBND"] is not None and PD["FAXS"] is not None:
                    PD[ntag] = (PD["FAXS"] - PD[xtag]) / (PD["FAXS"] - PD["FBND"])
                    nfilt = np.all([PD[ntag] >= 0.0,PD[ntag] <= 1.0],axis=0)
                    PD[xtag] = np.hstack((PD["FAXS"],PD[xtag][nfilt]))
                    PD[vtag] = np.hstack((0.0,PD[vtag][nfilt]))
                    PD[ntag] = np.hstack((0.0,PD[ntag][nfilt]))
            if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]][::2]
                PD[eqlist[ii].upper()][dtag] = RD[m[eqlist[ii]+'/'+tag]][1::2]
                PD[eqlist[ii].upper()][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqlist[ii].upper()][vtag] is not None else None
                while PD[eqlist[ii].upper()][dtag].size < PD[eqlist[ii].upper()][xtag].size:
                    PD[eqlist[ii].upper()][dtag] = np.hstack((PD[eqlist[ii].upper()][dtag],np.NaN))
                if PD[eqtag][xtag] is not None:
                    axid = np.where(np.diff(PD[xtag]) < 0.0)[0][0]
                    PD[eqtag][vtag] = PD[eqtag][vtag][axid::-1]
                    PD[eqtag][xtag] = PD[eqtag][xtag][axid::-1]
                if PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag][ntag] = (PD[eqtag]["FAXS"] - PD[eqtag][xtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])
                    nfilt = np.all([PD[eqtag][ntag] >= 0.0,PD[eqtag][ntag] <= 1.0],axis=0)
                    PD[eqtag][xtag] = np.hstack((PD[eqtag]["FAXS"],PD[eqtag][xtag][nfilt]))
                    PD[eqtag][vtag] = np.hstack((0.0,PD[eqtag][vtag][nfilt]))
                    PD[eqtag][ntag] = np.hstack((0.0,PD[eqtag][ntag][nfilt]))

    if fdebug:
        print("process_aug.py: unpack_coord_data() completed.")

    return PD


def unpack_profile_data(rawdata,newstruct=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Unpacks the required plasma profile data from the raw extracted data
    container, pre-processes it with statistical averaging if necessary,
    and stores it in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :returns: dict. Implementation-specific object with processed plasma profile data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata,dict):
        RD = rawdata
    if isinstance(newstruct,dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None
        ftflag = False if "SHOTPHASE" in RD and RD["SHOTPHASE"] > 0 else True

        warnings.filterwarnings("ignore",category=FutureWarning)

        # Specification of which magnetic equilibrium entry to use, default is standard FPP
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['FPP']
        qsf = eqlist[0].upper() if len(eqlist) > 0 else None

        # IDA Bayesian profile reconstruction - electron density and temperature
        IDA = None
        if 'IDA/Te/time/rhop' in m and RD[m['IDA/Te/time/rhop']] is not None:
            if IDA is None:
                IDA = dict()
            vals = RD[m['IDA/Te/time/rhop']]                                                   # Electron temp. (rho_pol)
            errs = RD[m['IDA/Te_unc/time/rhop']] if 'IDA/Te_unc/time/rhop' in m else None      # 1 sigma (see ISIS)
            (IDA["TE"],IDA["TEEB"],IDA["TEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e1)
            pfnvec = RD["X_"+m['IDA/Te/time/rhop']].copy()
            if pfnvec.ndim > 1:
                pfnvec = itemgetter(0)(ptools.filtered_average(RD["X_"+m['IDA/Te/time/rhop']]))
            IDA["PFNTE"] = np.power(pfnvec,2.0)
            navgmap = np.atleast_2d(navgmap)
            IDA["NTE"] = np.sum(navgmap.astype(int),axis=0)
            if 'IDA/dTe_dr/time/rhop' in m and RD[m['IDA/dTe_dr/time/rhop']] is not None:
                vals = RD[m['IDA/dTe_dr/time/rhop']]
                errs = 0.1 * vals if vals is not None else None
                (IDA["DTE"],IDA["DTEEB"],IDA["DTEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs)
                pfnvec = RD["X_"+m['IDA/dTe_dr/time/rhop']].copy()
                if pfnvec.ndim > 1:
                    pfnvec = itemgetter(0)(ptools.filtered_average(RD["X_"+m['IDA/dTe_dr/time/rhop']]))
                IDA["PFNDTE"] = np.power(pfnvec,2.0)
                navgmap = np.atleast_2d(navgmap)
                IDA["NDTE"] = np.sum(navgmap.astype(int),axis=0)
        if 'IDA/ne/time/rhop' in m and RD[m['IDA/ne/time/rhop']] is not None:
            if IDA is None:
                IDA = dict()
            vals = RD[m['IDA/ne/time/rhop']]                                                   # Electron density (R)
            errs = RD[m['IDA/ne_unc/time/rhop']] if 'IDA/ne_unc/time/rhop' in m else None      # 1 sigma (see ISIS)
            (IDA["NE"],IDA["NEEB"],IDA["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            pfnvec = RD["X_"+m['IDA/ne/time/rhop']].copy()
            if pfnvec.ndim > 1:
                pfnvec = itemgetter(0)(ptools.filtered_average(RD["X_"+m['IDA/ne/time/rhop']]))
            IDA["PFNNE"] = np.power(pfnvec,2.0)
            navgmap = np.atleast_2d(navgmap)
            IDA["NNE"] = np.sum(navgmap.astype(int),axis=0)
            if 'IDA/dne_dr/time/rhop' in m and RD[m['IDA/dne_dr/time/rhop']] is not None:
                vals = RD[m['IDA/dne_dr/time/rhop']]
                errs = 0.1 * vals if vals is not None else None
                (IDA["DNE"],IDA["DNEEB"],IDA["DNEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs)
                pfnvec = RD["X_"+m['IDA/dne_dr/time/rhop']].copy()
                if pfnvec.ndim > 1:
                    pfnvec = itemgetter(0)(ptools.filtered_average(RD["X_"+m['IDA/dne_dr/time/rhop']]))
                IDA["PFNDNE"] = np.power(pfnvec,2.0)
                navgmap = np.atleast_2d(navgmap)
                IDA["NDNE"] = np.sum(navgmap.astype(int),axis=0)
        PD["IDA"] = IDA

        # Lithium beam diagnostic - edge electron density
        LIN = None
        if 'LIN/ne/time/R|Z' in m and RD[m['LIN/ne/time/R|Z']] is not None:
            if LIN is None:
                LIN = dict()
            vals = RD[m['LIN/ne/time/R|Z']]
            errs = RD[m['LIN/ne_unc/time/R|Z']] if 'LIN/ne_unc/time/R|Z' in m else None     # Assumed 1 sigma
            (LIN["NE"],LIN["NEEB"],LIN["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            rvals = RD["R_"+m['LIN/ne/time/R|Z']].copy()
            zvals = RD["Z_"+m['LIN/ne/time/R|Z']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['LIN/ne/time/R|Z']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['LIN/ne/time/R|Z']]))
            LIN["RNE"] = rvals.copy()
            LIN["ZNE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            LIN["NNE"] = np.sum(navgmap.astype(int),axis=0)
        PD["LIN"] = LIN

        # Inversion of interferometry lines - electron density
        DCN = None
        if 'DCR/profile/time_c/' in m and RD[m['DCR/profile/time_c/']] is not None:
            if DCN is None:
                DCN = dict()
            vals = RD[m['DCR/profile/time_c/']]
            errs = 0.1 * vals if vals is not None else None            # Taken from script of Teobaldo Luda
            (DCN["NE"],DCN["NEEB"],DCN["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            DCN["PFNNE"] = np.power(np.linspace(0,1.1,15),2.0)         # Taken from script of Teobaldo Luda
            navgmap = np.atleast_2d(navgmap)
            DCN["NNE"] = np.sum(navgmap.astype(int),axis=0)
        if DCN is None and 'DLP/ne/time/rp' in m and RD[m['DLP/ne/time/rp']] is not None:
            if DCN is None:
                DCN = dict()
            vals = RD[m['DLP/ne/time/rp']]
            errs = 0.1 * vals if vals is not None else None            # Assumed 10% error
            (DCN["NE"],DCN["NEEB"],DCN["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            rhopvec = RD["X_"+m['DLP/ne/time/rp']].copy()
            if rhopvec.ndim > 1:
                rhopvec = itemgetter(0)(ptools.filtered_average(RD["X_"+m['DLP/ne/time/rp']]))
            DCN["PFNNE"] = np.power(rhopvec,2.0)
            navgmap = np.atleast_2d(navgmap)
            DCN["NNE"] = np.sum(navgmap.astype(int),axis=0)
        if DCN is None and 'DPR/ne/time/rp' in m and RD[m['DPR/ne/time/rp']] is not None:
            if DCN is None:
                DCN = dict()
            vals = RD[m['DPR/ne/time/rp']]
            errs = 0.1 * vals if vals is not None else None            # Assumed 10% error
            (DCN["NE"],DCN["NEEB"],DCN["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            rhopvec = RD["X_"+m['DPR/ne/time/rp']].copy()
            if rhopvec.ndim > 1:
                rhopvec = itemgetter(0)(ptools.filtered_average(RD["X_"+m['DPR/ne/time/rp']]))
            DCN["PFNNE"] = np.power(rhopvec,2.0)
            navgmap = np.atleast_2d(navgmap)
            DCN["NNE"] = np.sum(navgmap.astype(int),axis=0)
        PD["DCN"] = DCN

        # Core Thomson scattering - electron density and temperature
        VTNC = None
        if 'VTA/Te_c/TIM_CORE/R_core|Z_core' in m and RD[m['VTA/Te_c/TIM_CORE/R_core|Z_core']] is not None:
            if VTNC is None:
                VTNC = dict()
            vals = RD[m['VTA/Te_c/TIM_CORE/R_core|Z_core']]
            errs = RD[m['VTA/SigTe_c/TIM_CORE/R_core|Z_core']] if 'VTA/SigTe_c/TIM_CORE/R_core|Z_core' in m else None     # 1 sigma
            (VTNC["TE"],VTNC["TEEB"],VTNC["TEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTA/Te_c/TIM_CORE/R_core|Z_core']].copy()
            zvals = RD["Z_"+m['VTA/Te_c/TIM_CORE/R_core|Z_core']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTA/Te_c/TIM_CORE/R_core|Z_core']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTA/Te_c/TIM_CORE/R_core|Z_core']]))
            VTNC["RTE"] = rvals.copy()
            VTNC["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTNC["NTE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTA/Ne_c/TIM_CORE/R_core|Z_core' in m and RD[m['VTA/Ne_c/TIM_CORE/R_core|Z_core']] is not None:
            if VTNC is None:
                VTNC = dict()
            vals = RD[m['VTA/Ne_c/TIM_CORE/R_core|Z_core']]
            errs = RD[m['VTA/SigNe_c/TIM_CORE/R_core|Z_core']] if 'VTA/SigNe_c/TIM_CORE/R_core|Z_core' in m else None     # 1 sigma
            (VTNC["NE"],VTNC["NEEB"],VTNC["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            rvals = RD["R_"+m['VTA/Ne_c/TIM_CORE/R_core|Z_core']]
            zvals = RD["Z_"+m['VTA/Ne_c/TIM_CORE/R_core|Z_core']]
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTA/Ne_c/TIM_CORE/R_core|Z_core']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTA/Ne_c/TIM_CORE/R_core|Z_core']]))
            VTNC["RNE"] = rvals.copy()
            VTNC["ZNE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTNC["NNE"] = np.sum(navgmap.astype(int),axis=0)
        PD["VTNC"] = VTNC

        # Edge Thomson scattering - electron density and temperature
        VTNE = None
        if 'VTA/Te_e/TIM_EDGE/R_edge|Z_edge' in m and RD[m['VTA/Te_e/TIM_EDGE/R_edge|Z_edge']] is not None:
            if VTNE is None:
                VTNE = dict()
            vals = RD[m['VTA/Te_e/TIM_EDGE/R_edge|Z_edge']]
            errs = RD[m['VTA/SigTe_e/TIM_EDGE/R_edge|Z_edge']] if 'VTA/SigTe_e/TIM_EDGE/R_edge|Z_edge' in m else None     # 1 sigma
            (VTNE["TE"],VTNE["TEEB"],VTNE["TEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTA/Te_e/TIM_EDGE/R_edge|Z_edge']].copy()
            zvals = RD["Z_"+m['VTA/Te_e/TIM_EDGE/R_edge|Z_edge']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTA/Te_e/TIM_EDGE/R_edge|Z_edge']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTA/Te_e/TIM_EDGE/R_edge|Z_edge']]))
            VTNE["RTE"] = rvals.copy()
            VTNE["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTNE["NTE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTA/Ne_e/TIM_EDGE/R_edge|Z_edge' in m and RD[m['VTA/Ne_e/TIM_EDGE/R_edge|Z_edge']] is not None:
            if VTNE is None:
                VTNE = dict()
            vals = RD[m['VTA/Ne_e/TIM_EDGE/R_edge|Z_edge']]
            errs = RD[m['VTA/SigNe_e/TIM_EDGE/R_edge|Z_edge']] if 'VTA/SigNe_e/TIM_EDGE/R_edge|Z_edge' in m else None     # 1 sigma
            (VTNE["NE"],VTNE["NEEB"],VTNE["NEEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
            rvals = RD["R_"+m['VTA/SigNe_e/TIM_EDGE/R_edge|Z_edge']].copy()
            zvals = RD["Z_"+m['VTA/SigNe_e/TIM_EDGE/R_edge|Z_edge']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTA/Ne_e/TIM_EDGE/R_edge|Z_edge']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTA/Ne_e/TIM_EDGE/R_edge|Z_edge']]))
            VTNE["RNE"] = rvals.copy()
            VTNE["ZNE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTNE["NNE"] = np.sum(navgmap.astype(int),axis=0)
        PD["VTNE"] = VTNE

        # Old Thomson scattering, assumed core (3 port options, not sure if used simultaneously or not) - electron density and temperature
        VTAD = None
        VTAE = None
        VTAF = None
        if 'VTN/Td/TIME/Rd|Zd' in m and RD[m['VTN/Td/TIME/Rd|Zd']] is not None:
            if VTAD is None:
                VTAD = dict()
            vals = RD[m['VTN/Td/TIME/Rd|Zd']]
            lvals = RD[m['VTN/Tdlow/TIME/Rd|Zd']] if 'VTA/Tdlow/TIME/Rd|Zd' in m else None                                # Assumed 1 sigma
            uvals = RD[m['VTN/Tdupp/TIME/Rd|Zd']] if 'VTA/Tdupp/TIME/Rd|Zd' in m else None                                # Assumed 1 sigma
            (VTAD["TE"],VTAD["TEEB"],VTAD["TEEM"],navgmap) = ptools.filtered_average(vals,lower_values=lvals,upper_values=uvals,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTN/Td/TIME/Rd|Zd']].copy()
            zvals = RD["Z_"+m['VTN/Td/TIME/Rd|Zd']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTN/Td/TIME/Rd|Zd']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTN/Td/TIME/Rd|Zd']]))
            VTAD["RTE"] = rvals.copy()
            VTAD["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTAD["NTE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTN/Nd/TIME/Rd|Zd' in m and RD[m['VTN/Nd/TIME/Rd|Zd']] is not None:
            if VTAD is None:
                VTAD = dict()
            vals = RD[m['VTN/Nd/TIME/Rd|Zd']]
            lvals = RD[m['VTN/Ndlow/TIME/Rd|Zd']] if 'VTA/Nelow/TIME/Rd|Zd' in m else None                                # Assumed 1 sigma
            uvals = RD[m['VTN/Ndupp/TIME/Rd|Zd']] if 'VTA/Neupp/TIME/Rd|Zd' in m else None                                # Assumed 1 sigma
            (VTAD["NE"],VTAD["NEEB"],VTAD["NEEM"],navgmap) = ptools.filtered_average(vals,lower_values=lvals,upper_values=uvals,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTN/Nd/TIME/Rd|Zd']].copy()
            zvals = RD["Z_"+m['VTN/Nd/TIME/Rd|Zd']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTN/Nd/TIME/Rd|Zd']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTN/Nd/TIME/Rd|Zd']]))
            VTAD["RNE"] = rvals.copy()
            VTAD["ZNE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTAD["NNE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTN/Te/TIME/Re|Ze' in m and RD[m['VTN/Te/TIME/Re|Ze']] is not None:
            if VTAE is None:
                VTAE = dict()
            vals = RD[m['VTN/Te/TIME/Re|Ze']]
            lvals = RD[m['VTN/Telow/TIME/Re|Ze']] if 'VTA/Telow/TIME/Re|Ze' in m else None                                # Assumed 1 sigma
            uvals = RD[m['VTN/Teupp/TIME/Re|Ze']] if 'VTA/Teupp/TIME/Re|Ze' in m else None                                # Assumed 1 sigma
            (VTAE["TE"],VTAE["TEEB"],VTAE["TEEM"],navgmap) = ptools.filtered_average(vals,lower_values=lvals,upper_values=uvals,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTN/Te/TIME/Re|Ze']].copy()
            zvals = RD["Z_"+m['VTN/Te/TIME/Re|Ze']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTN/Te/TIME/Re|Ze']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTN/Te/TIME/Re|Ze']]))
            VTAE["RTE"] = rvals.copy()
            VTAE["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTAE["NTE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTN/Ne/TIME/Re|Ze' in m and RD[m['VTN/Ne/TIME/Re|Ze']] is not None:
            if VTAE is None:
                VTAE = dict()
            vals = RD[m['VTN/Ne/TIME/Re|Ze']]
            lvals = RD[m['VTN/Nelow/TIME/Re|Ze']] if 'VTA/Nelow/TIME/Re|Ze' in m else None                                # Assumed 1 sigma
            uvals = RD[m['VTN/Neupp/TIME/Re|Ze']] if 'VTA/Neupp/TIME/Re|Ze' in m else None                                # Assumed 1 sigma
            (VTAE["NE"],VTAE["NEEB"],VTAE["NEEM"],navgmap) = ptools.filtered_average(vals,lower_values=lvals,upper_values=uvals,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTN/Ne/TIME/Re|Ze']].copy()
            zvals = RD["Z_"+m['VTN/Ne/TIME/Re|Ze']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTN/Ne/TIME/Re|Ze']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTN/Ne/TIME/Re|Ze']]))
            VTAE["RNE"] = rvals.copy()
            VTAE["ZNE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTAE["NNE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTN/Tf/TIME/Rf|Zf' in m and RD[m['VTN/Tf/TIME/Rf|Zf']] is not None:
            if VTAF is None:
                VTAF = dict()
            vals = RD[m['VTN/Tf/TIME/Rf|Zf']]
            lvals = RD[m['VTN/Tflow/TIME/Rf|Zf']] if 'VTA/Tflow/TIME/Rf|Zf' in m else None                                # Assumed 1 sigma
            uvals = RD[m['VTN/Tfupp/TIME/Rf|Zf']] if 'VTA/Tfupp/TIME/Rf|Zf' in m else None                                # Assumed 1 sigma
            (VTAF["TE"],VTAF["TEEB"],VTAF["TEEM"],navgmap) = ptools.filtered_average(vals,lower_values=lvals,upper_values=uvals,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTN/Tf/TIME/Rf|Zf']].copy()
            zvals = RD["Z_"+m['VTN/Tf/TIME/Rf|Zf']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTN/Tf/TIME/Rf|Zf']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTN/Tf/TIME/Rf|Zf']]))
            VTAF["RTE"] = rvals.copy()
            VTAF["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTAF["NTE"] = np.sum(navgmap.astype(int),axis=0)
        if 'VTN/Nf/TIME/Rf|Zf' in m and RD[m['VTN/Nf/TIME/Rf|Zf']] is not None:
            if VTAF is None:
                VTAF = dict()
            vals = RD[m['VTN/Nf/TIME/Rf|Zf']]
            lvals = RD[m['VTN/Nflow/TIME/Rf|Zf']] if 'VTA/Nflow/TIME/Rf|Zf' in m else None                                # Assumed 1 sigma
            uvals = RD[m['VTN/Nfupp/TIME/Rf|Zf']] if 'VTA/Nfupp/TIME/Rf|Zf' in m else None                                # Assumed 1 sigma
            (VTAF["NE"],VTAF["NEEB"],VTAF["NEEM"],navgmap) = ptools.filtered_average(vals,lower_values=lvals,upper_values=uvals,lower_bounds=1.0e1)
            rvals = RD["R_"+m['VTN/Nf/TIME/Rf|Zf']].copy()
            zvals = RD["Z_"+m['VTN/Nf/TIME/Rf|Zf']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['VTN/Nf/TIME/Rf|Zf']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['VTN/Nf/TIME/Rf|Zf']]))
            VTAF["RNE"] = rvals.copy()
            VTAF["ZNE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            VTAF["NNE"] = np.sum(navgmap.astype(int),axis=0)
        PD["VTAD"] = VTAD
        PD["VTAE"] = VTAE
        PD["VTAF"] = VTAF

        # Electron cyclotron emission at AUG - electron temperature
        RMD = None
        if 'RMD/Trad-A/time-A/R-A|z-A' in m and RD[m['RMD/Trad-A/time-A/R-A|z-A']] is not None:
            if RMD is None:
                RMD = dict()
            vals = RD[m['RMD/Trad-A/time-A/R-A|z-A']]                                   # No error estimate
            (RMD["TE"],RMD["TEEB"],RMD["TEEM"],navgmap) = ptools.filtered_average(vals,value_errors=None,lower_bounds=1.0e1)
            rvals = RD["R_"+m['RMD/Trad-A/time-A/R-A|z-A']].copy()
            zvals = RD["Z_"+m['RMD/Trad-A/time-A/R-A|z-A']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['RMD/Trad-A/time-A/R-A|z-A']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['RMD/Trad-A/time-A/R-A|z-A']]))
            RMD["RTE"] = rvals.copy()
            RMD["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            RMD["NTE"] = np.sum(navgmap.astype(int),axis=0)
        PD["RMD"] = RMD

        # Old electron cyclotron emission at AUG - electron temperature
        CEC = None
        if 'CEC/Trad-A/time-A/R-A|z-A' in m and RD[m['CEC/Trad-A/time-A/R-A|z-A']] is not None:
            if CEC is None:
                CEC = dict()
            vals = RD[m['CEC/Trad-A/time-A/R-A|z-A']]                                   # No error estimate
            (CEC["TE"],CEC["TEEB"],CEC["TEEM"],navgmap) = ptools.filtered_average(vals,value_errors=None,lower_bounds=1.0e1)
            rvals = RD["R_"+m['CEC/Trad-A/time-A/R-A|z-A']].copy()
            zvals = RD["Z_"+m['CEC/Trad-A/time-A/R-A|z-A']].copy()
            if rvals.ndim > 1:
                rvals = itemgetter(0)(ptools.filtered_average(RD["R_"+m['CEC/Trad-A/time-A/R-A|z-A']]))
            if zvals.ndim > 1:
                zvals = itemgetter(0)(ptools.filtered_average(RD["Z_"+m['CEC/Trad-A/time-A/R-A|z-A']]))
            CEC["RTE"] = rvals.copy()
            CEC["ZTE"] = zvals.copy()
            navgmap = np.atleast_2d(navgmap)
            CEC["NTE"] = np.sum(navgmap.astype(int),axis=0)
        PD["CEC"] = CEC

        # Charge exchange spectroscopy system at AUG - ion temperature, toroidal angular frequency, toroidal velocity, and impurity density
        CX = None
        tags = ['CE','CM','CA','CC','CH','CN','CO','CU','CZ']
        for kk in np.arange(0,len(tags)):
            tag1 = tags[kk]+'Z'
            tag2 = tags[kk]+'S'
            rz = 'time/R|z'
            rzt = 'time/R_time|z_time'
            cflag = True if (tag1+'/Ti_c/'+rz in m and RD[m[tag1+'/Ti_c/'+rz]] is not None) or (tag1+'/Ti_c/'+rzt in m and RD[m[tag1+'/Ti_c/'+rzt]] is not None) else False
            ctag = '_c' if cflag else ''
            tflag = True if tag1+'/Ti'+ctag+'/'+rzt in m and RD[m[tag1+'/Ti'+ctag+'/'+rzt]] is not None else False
            ttag = rzt if tflag else rz
            if tag1+'/Ti'+ctag+'/'+ttag in m and RD[m[tag1+'/Ti'+ctag+'/'+ttag]] is not None:
                if CX is None:
                    CX = dict()
                CXD = dict()

                if re.match(r'C.',tags[kk]):   # Errors given as 1 sigma for all CX at AUG
                    ti = RD[m[tag1+'/Ti'+ctag+'/'+ttag]].copy()
                    tieb = RD[m[tag1+'/err_Ti'+ctag+'/'+ttag]].copy() if tag1+'/err_Ti'+ctag+'/'+ttag in m and RD[m[tag1+'/err_Ti'+ctag+'/'+ttag]] is not None else None
                    rti = RD["R_"+m[tag1+'/Ti'+ctag+'/'+ttag]].copy()
                    zti = RD["Z_"+m[tag1+'/Ti'+ctag+'/'+ttag]].copy()

                    # Filtering CX data when NBI lines are swapped mid-discharge
                    ivec = np.where(np.full((ti.shape[0],),True))[0]
                    iivec  = np.where(np.invert(ivec))[0]
                    tempv = rti.copy()
                    if tempv.shape[0] != ti.shape[0] and tempv.shape[1] == ti.shape[0]:
                        tempv = np.transpose(tempv)
                    if tempv.shape[0] == ti.shape[0]:
                        xdim = tempv.ndim
                        for nd in np.arange(1,xdim):
                            tempv = np.take(tempv,0,axis=1)
                        idx = np.isfinite(tempv)
                        if tflag:
                            drvec = np.diff(tempv)
                            if np.any(np.abs(drvec) > 1.0e-4):
                                idx = (tempv < np.mean(tempv))
                        ivec = np.where(idx)[0]
                        iivec = np.where(np.invert(idx))[0]

                    if rti.shape == zti.shape:
                        zti[rti == 0.0] = np.NaN
                        rti[rti == 0.0] = np.NaN
                    sfilt = None
                    if tag1+'/fit_stat/'+ttag in m and RD[m[tag1+'/fit_stat/'+ttag]] is not None:
                        sfilt = (RD[m[tag1+'/fit_stat/'+ttag]] < 1)
                        ti[sfilt] = np.NaN
                        tieb[sfilt] = np.NaN
                        if rti.shape == ti.shape:
                            rti[sfilt] = np.NaN
                        if zti.shape == ti.shape:
                            zti[sfilt] = np.NaN
                    vals = np.take(ti,ivec,axis=0)
                    errs = np.take(tieb,ivec,axis=0) if tieb is not None else None
                    (CXD["TI"],CXD["TIEB"],CXD["TIEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e1,upper_bounds=1.0e7)
                    navgmap = np.atleast_2d(navgmap)
                    CXD["NTI"] = np.sum(navgmap.astype(int),axis=0)
                    rvals = rti.copy() if CXD["TI"] is not None else None
                    if rvals is not None and rti.shape[0] == ti.shape[0]:
                        rvals = np.take(rti,ivec,axis=0)
                    rerrs = np.zeros(rvals.shape)
                    CXD["RTI"] = itemgetter(0)(ptools.filtered_average(rvals,value_errors=rerrs))
                    zvals = zti.copy() if CXD["TI"] is not None else None
                    if zvals is not None and zti.shape[0] == ti.shape[0]:
                        zvals = np.take(zti,ivec,axis=0)
                    zerrs = np.zeros(zvals.shape)
                    CXD["ZTI"] = itemgetter(0)(ptools.filtered_average(zvals,value_errors=zerrs))
#                    CXD["PFNTI"] = None
#                    if "PFNMAP" in PD and PD["PFNMAP"] is not None and CXD["TI"] is not None:
#                        CXD["PFNTI"] = ftools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],CXD["RTI"],CXD["ZTI"])
#                        pfilt = np.all([CXD["PFNTI"] >= -1.1,CXD["PFNTI"] <= 1.1],axis=0)
#                        CXD["PFNTI"] = CXD["PFNTI"][pfilt]
#                        CXD["TI"] = CXD["TI"][pfilt]
#                        CXD["TIEB"] = CXD["TIEB"][pfilt]
#                        CXD["NTI"] = CXD["NTI"][pfilt]
#                        CXD["RTI"] = CXD["RTI"][pfilt]
#                        CXD["ZTI"] = CXD["ZTI"][pfilt]
                    if tflag:
                        vals2 = np.take(ti,iivec,axis=0)
                        errs2 = np.take(tieb,iivec,axis=0) if tieb is not None else None
                        (CXD["TI2"],CXD["TIEB2"],CXD["TIEM2"],navgmap2) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e1,upper_bounds=1.0e7)
                        navgmap2 = np.atleast_2d(navgmap2)
                        CXD["NTI2"] = np.sum(navgmap2.astype(int),axis=0)
                        rvals2 = rti.copy() if CXD["TI2"] is not None else None
                        if rvals2 is not None and rti.shape[0] == ti.shape[0]:
                            rvals2 = np.take(rti,iivec,axis=0)
                        rerrs2 = np.zeros(rvals2.shape)
                        CXD["RTI2"] = itemgetter(0)(ptools.filtered_average(rvals2,value_errors=rerrs2))
                        zvals2 = zti.copy() if CXD["TI2"] is not None else None
                        if zvals2 is not None and zti.shape[0] == ti.shape[0]:
                            zvals2 = np.take(zti,iivec,axis=0)
                        zerrs2 = np.zeros(zvals2.shape)
                        CXD["ZTI2"] = itemgetter(0)(ptools.filtered_average(zvals2,value_errors=zerrs2))
#                        CXD["PFNTI2"] = None
#                        if "PFNMAP" in PD and PD["PFNMAP"] is not None and CXD["TI2"] is not None:
#                            CXD["PFNTI2"] = ftools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],CXD["RTI2"],CXD["ZTI2"])
#                            pfilt = np.all([CXD["PFNTI2"] >= -1.1,CXD["PFNTI2"] <= 1.1],axis=0)
#                            CXD["PFNTI2"] = CXD["PFNTI2"][pfilt]
#                            CXD["TI2"] = CXD["TI2"][pfilt]
#                            CXD["TIEB2"] = CXD["TIEB2"][pfilt]
#                            CXD["NTI2"] = CXD["NTI2"][pfilt]
#                            CXD["RTI2"] = CXD["RTI2"][pfilt]
#                            CXD["ZTI2"] = CXD["ZTI2"][pfilt]
                    if tag1+'/vrot/'+ttag in m and RD[m[tag1+'/vrot/'+ttag]] is not None:
                        vt = RD[m[tag1+'/vrot/'+ttag]].copy()
                        vteb = RD[m[tag1+'/err_vrot/'+ttag]].copy() if tag1+'/err_vrot/'+ttag in m and RD[m[tag1+'/err_vrot/'+ttag]] is not None else None
                        rvt = RD["R_"+m[tag1+'/vrot/'+ttag]].copy()
                        zvt = RD["Z_"+m[tag1+'/vrot/'+ttag]].copy()
                        if rvt.shape == zvt.shape:
                            zvt[rvt == 0.0] = np.NaN
                            rvt[rvt == 0.0] = np.NaN
                        if vt.shape == ti.shape and sfilt is not None:
                            vt[sfilt] = np.NaN
                            vteb[sfilt] = np.NaN
                            if rvt.shape == vt.shape:
                                rvt[sfilt] = np.NaN
                            if zvt.shape == vt.shape:
                                zvt[sfilt] = np.NaN
                        vals = np.take(vt,ivec,axis=0)
                        errs = np.take(vteb,ivec,axis=0) if vteb is not None else None
                        (vtu,vtueb,vtuem,navgumap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e-5)
                        (vtl,vtleb,vtlem,navglmap) = ptools.filtered_average(vals,value_errors=errs,upper_bounds=-1.0e-5)
                        navgumap = np.atleast_2d(navgumap)
                        navglmap = np.atleast_2d(navglmap)
                        nvtu = np.sum(navgumap.astype(int),axis=0)
                        nvtl = np.sum(navglmap.astype(int),axis=0)
                        ufilt = (nvtu == 0)
                        vtu[ufilt] = 0.0
                        vtueb[ufilt] = 0.0
                        vtuem[ufilt] = 0.0
                        lfilt = (nvtl == 0)
                        vtl[lfilt] = 0.0
                        vtleb[lfilt] = 0.0
                        vtlem[lfilt] = 0.0
                        nfilt = np.invert(np.all([ufilt,lfilt],axis=0))
                        CXD["VT"] = np.full(nfilt.shape,np.NaN)
                        CXD["VTEB"] = np.full(nfilt.shape,np.NaN)
                        CXD["VTEM"] = np.full(nfilt.shape,np.NaN)
                        if np.any(nfilt):
                            CXD["VT"][nfilt] = (vtu[nfilt] * nvtu[nfilt] + vtl[nfilt] * nvtl[nfilt]) / (nvtu[nfilt] + nvtl[nfilt])
                            CXD["VTEB"][nfilt] = np.sqrt((np.power(vtueb[nfilt],2.0) * nvtu[nfilt] + np.power(vtleb[nfilt],2.0) * nvtl[nfilt]) / (nvtu[nfilt] + nvtl[nfilt]))
                            CXD["VTEM"][nfilt] = np.sqrt((np.power(vtuem[nfilt],2.0) * nvtu[nfilt] + np.power(vtlem[nfilt],2.0) * nvtl[nfilt]) / (nvtu[nfilt] + nvtl[nfilt]))
                        CXD["NVT"] = nvtu + nvtl
                        rvals = rvt.copy() if CXD["VT"] is not None else None
                        if rvals is not None and rvt.shape[0] == vt.shape[0]:
                            rvals = np.take(rvt,ivec,axis=0)
                        rerrs = np.zeros(rvals.shape)
                        CXD["RVT"] = itemgetter(0)(ptools.filtered_average(rvals,value_errors=rerrs))
                        zvals = zvt.copy() if CXD["VT"] is not None else None
                        if zvals is not None and zvt.shape[0] == vt.shape[0]:
                            zvals = np.take(zvt,ivec,axis=0)
                        zerrs = np.zeros(zvals.shape)
                        CXD["ZVT"] = itemgetter(0)(ptools.filtered_average(zvals,value_errors=zerrs))
#                        CXD["PFNVT"] = None
#                        if "PFNMAP" in PD and PD["PFNMAP"] is not None and CXD["VT"] is not None:
#                            CXD["PFNVT"] = ftools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],CXD["RVT"],CXD["ZVT"])
#                            pfilt = np.all([CXD["PFNVT"] >= -1.1,CXD["PFNVT"] <= 1.1],axis=0)
#                            CXD["PFNVT"] = CXD["PFNVT"][pfilt]
#                            CXD["VT"] = CXD["VT"][pfilt]
#                            CXD["VTEB"] = CXD["VTEB"][pfilt]
#                            CXD["NVT"] = CXD["NVT"][pfilt]
#                            CXD["RVT"] = CXD["RVT"][pfilt]
#                            CXD["ZVT"] = CXD["ZVT"][pfilt]
                        if tflag:
                            vals2 = np.take(vt,iivec,axis=0)
                            errs2 = np.take(vteb,iivec,axis=0) if vteb is not None else None
                            (vtu2,vtueb2,vtuem2,navgumap2) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e-5)
                            (vtl2,vtleb2,vtlem2,navglmap2) = ptools.filtered_average(vals,value_errors=errs,upper_bounds=-1.0e-5)
                            navgumap2 = np.atleast_2d(navgumap2)
                            navglmap2 = np.atleast_2d(navglmap2)
                            nvtu2 = np.sum(navgumap2.astype(int),axis=0)
                            nvtl2 = np.sum(navglmap2.astype(int),axis=0)
                            ufilt = (nvtu2 == 0)
                            vtu2[ufilt] = 0.0
                            vtueb2[ufilt] = 0.0
                            vtuem2[ufilt] = 0.0
                            lfilt = (nvtl2 == 0)
                            vtl2[lfilt] = 0.0
                            vtleb2[lfilt] = 0.0
                            vtlem2[lfilt] = 0.0
                            nfilt = np.invert(np.all([ufilt,lfilt],axis=0))
                            CXD["VT2"] = np.full(nfilt.shape,np.NaN)
                            CXD["VTEB2"] = np.full(nfilt.shape,np.NaN)
                            CXD["VTEM2"] = np.full(nfilt.shape,np.NaN)
                            if np.any(nfilt):
                                CXD["VT2"][nfilt] = (vtu2[nfilt] * nvtu2[nfilt] + vtl2[nfilt] * nvtl2[nfilt]) / (nvtu2[nfilt] + nvtl2[nfilt])
                                CXD["VTEB2"][nfilt] = np.sqrt((np.power(vtueb2[nfilt],2.0) * nvtu2[nfilt] + np.power(vtleb2[nfilt],2.0) * nvtl2[nfilt]) / (nvtu2[nfilt] + nvtl2[nfilt]))
                                CXD["VTEM2"][nfilt] = np.sqrt((np.power(vtuem2[nfilt],2.0) * nvtu2[nfilt] + np.power(vtlem2[nfilt],2.0) * nvtl2[nfilt]) / (nvtu2[nfilt] + nvtl2[nfilt]))
                            CXD["NVT2"] = nvtu2 + nvtl2
                            rvals2 = rvt.copy() if CXD["VT2"] is not None else None
                            if rvals2 is not None and rvt.shape[0] == vt.shape[0]:
                                rvals2 = np.take(rvt,iivec,axis=0)
                            rerrs2 = np.zeros(rvals2.shape)
                            CXD["RVT2"] = itemgetter(0)(ptools.filtered_average(rvals2,value_errors=rerrs2))
                            zvals2 = zvt.copy() if CXD["VT2"] is not None else None
                            if zvals2 is not None and zvt.shape[0] == vt.shape[0]:
                                zvals2 = np.take(zvt,iivec,axis=0)
                            zerrs2 = np.zeros(zvals2.shape)
                            CXD["ZVT2"] = itemgetter(0)(ptools.filtered_average(zvals2,value_errors=zerrs2))
#                            CXD["PFNVT2"] = None
#                            if "PFNMAP" in PD and PD["PFNMAP"] is not None and CXD["VT2"] is not None:
#                                CXD["PFNVT2"] = ftools.rz2pf(PD["PFNMAP"],PD["RMAP"],PD["ZMAP"],CXD["RVT2"],CXD["ZVT2"])
#                                pfilt = np.all([CXD["PFNVT2"] >= -1.1,CXD["PFNVT2"] <= 1.1],axis=0)
#                                CXD["PFNVT2"] = CXD["PFNVT2"][pfilt]
#                                CXD["VT2"] = CXD["VT2"][pfilt]
#                                CXD["VTEB2"] = CXD["VTEB2"][pfilt]
#                                CXD["NVT2"] = CXD["NVT2"][pfilt]
#                                CXD["RVT2"] = CXD["RVT2"][pfilt]
#                                CXD["ZVT2"] = CXD["ZVT2"][pfilt]
                        CXD["AF"] = CXD["VT"] / CXD["RVT"] if CXD["RVT"] is not None and CXD["VT"] is not None else None
                        CXD["AFEB"] = CXD["VTEB"] / CXD["RVT"] if CXD["RVT"] is not None and CXD["VTEB"] is not None else None
                        CXD["AFEM"] = CXD["VTEM"] / CXD["RVT"] if CXD["RVT"] is not None and CXD["VTEM"] is not None else None
                        CXD["RAF"] = CXD["RVT"].copy() if CXD["RVT"] is not None else None
                        CXD["ZAF"] = CXD["ZVT"].copy() if CXD["ZVT"] is not None else None
                        CXD["NAF"] = CXD["NVT"].copy()
#                        CXD["PFNAF"] = CXD["PFNVT"].copy() if CXD["PFNVT"] is not None else None
                        if tflag:
                            CXD["AF2"] = CXD["VT2"] / CXD["RVT2"] if CXD["RVT2"] is not None and CXD["VT2"] is not None else None
                            CXD["AFEB2"] = CXD["VTEB2"] / CXD["RVT2"] if CXD["RVT2"] is not None and CXD["VTEB2"] is not None else None
                            CXD["AFEM2"] = CXD["VTEM2"] / CXD["RVT2"] if CXD["RVT2"] is not None and CXD["VTEM2"] is not None else None
                            CXD["RAF2"] = CXD["RVT2"].copy() if CXD["RVT2"] is not None else None
                            CXD["ZAF2"] = CXD["ZVT2"].copy() if CXD["ZVT2"] is not None else None
                            CXD["NAF2"] = CXD["NVT2"].copy()
#                            CXD["PFNAF2"] = CXD["PFNVT2"].copy() if CXD["PFNVT2"] is not None else None
                    ptag = '_plc' if tag2+'/nimp_plc/time/R|z' in m else ''
                    if tag2+'/nimp'+ptag+'/time/R|z' in m and RD[m[tag2+'/nimp'+ptag+'/time/R|z']] is not None:
                        nimp = RD[m[tag2+'/nimp'+ptag+'/time/R|z']].copy()
                        nimpeb = RD[m[tag2+'/err_nimp/time/R|z']].copy() if tag2+'/err_nimp/time/R|z' in m and RD[m[tag2+'/err_nimp/time/R|z']] is not None else None
                        rnimp = RD["R_"+m[tag1+'/nimp'+ptag+'/time/R|z']].copy()
                        znimp = RD["Z_"+m[tag1+'/nimp'+ptag+'/time/R|z']].copy()
                        if rnimp.shape == znimp.shape:
                            znimp[rnimp == 0.0] = np.NaN
                            rnimp[rnimp == 0.0] = np.NaN
                        if nimp.shape == ti.shape and sfilt is not None:
                            nimp[sfilt] = np.NaN
                            nimpeb[sfilt] = np.NaN
                            if rnimp.shape == nimp.shape:
                                rnimp[sfilt] = np.NaN
                            if znimp.shape == nimp.shape:
                                znimp[sfilt] = np.NaN
                        vals = np.take(nimp,ivec,axis=0)
                        errs = np.take(nimpeb,ivec,axis=0) if vteb is not None else None
                        (CXD["NIMP"],CXD["NIMPEB"],CXD["NIMPEM"],navgmap) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
                        navgmap = np.atleast_2d(navgmap)
                        CXD["NNIMP"] = np.sum(navgmap.astype(int),axis=0)
                        rvals = rnimp.copy() if CXD["NIMP"] is not None else None
                        if rvals is not None and rnimp.shape[0] == nimp.shape[0]:
                            rvals = np.take(rnimp,ivec,axis=0)
                        rerrs = np.zeros(rvals.shape)
                        CXD["RNIMP"] = itemgetter(0)(ptools.filtered_average(rvals,value_errors=rerrs))
                        zvals = znimp.copy() if CXD["NIMP"] is not None else None
                        if zvals is not None and znimp.shape[0] == nimp.shape[0]:
                            zvals = np.take(znimp,ivec,axis=0)
                        zerrs = np.zeros(zvals.shape)
                        CXD["ZNIMP"] = itemgetter(0)(ptools.filtered_average(zvals,value_errors=zerrs))
                        if tflag:
                            vals2 = np.take(nimp,iivec,axis=0)
                            errs2 = np.take(nimpeb,iivec,axis=0) if nimpeb is not None else None
                            (CXD["NIMP2"],CXD["NIMPEB2"],CXD["NIMPEM2"],navgmap2) = ptools.filtered_average(vals,value_errors=errs,lower_bounds=1.0e10)
                            navgmap2 = np.atleast_2d(navgmap2)
                            CXD["NNIMP2"] = np.sum(navgmap2.astype(int),axis=0)
                            rvals2 = rnimp.copy() if CXD["NIMP2"] is not None else None
                            if rvals2 is not None and rnimp.shape[0] == nimp.shape[0]:
                                rvals2 = np.take(rnimp,iivec,axis=0)
                            rerrs2 = np.zeros(rvals2.shape)
                            CXD["RNIMP2"] = itemgetter(0)(ptools.filtered_average(rvals2,value_errors=rerrs2))
                            zvals2 = znimp.copy() if CXD["NIMP2"] is not None else None
                            if zvals2 is not None and znimp.shape[0] == nimp.shape[0]:
                                zvals2 = np.take(znimp,iivec,axis=0)
                            zerrs2 = np.zeros(zvals2.shape)
                            CXD["ZNIMP2"] = itemgetter(0)(ptools.filtered_average(zvals2,value_errors=zerrs2))
                    CXD["A"] = None
                    CXD["Z"] = None
                    if tag2+'/Info/imp_Z/' in m and RD[m[tag2+'/Info/imp_Z/']] is not None:
                        CXD["Z"] = float(RD[m[tag2+'/Info/imp_Z/']])
                        (zname,za,zz) = ptools.define_ion_species(CXD["Z"])
                        CXD["A"] = float(za)
                    elif tag1+'/LineInfo/Z/' in m and RD[m[tag1+'/LineInfo/Z/']] is not None:
                        CXD["Z"] = float(RD[m[tag1+'/LineInfo/Z/']])
                        (zname,za,zz) = ptools.define_ion_species(CXD["Z"])
                        CXD["A"] = float(za)
                    if CXD["A"] is None or CXD["Z"] is None:
                        CXD["A"] = 11.0
                        CXD["Z"] = 5.0
                if CX is not None and CXD:
                    CX[tags[kk].upper()+'Z'] = CXD
        PD["CX"] = CX

        # Magnetic field reconstruction from array of magnetic sensors at AUG - safety factor
        MAGN = None
        if qsf is not None and "Q"+qsf in PD:
            MAGN = dict()
            MAGN["Q"] = PD["Q"+qsf]
            MAGN["QEB"] = PD["QEB"+qsf]
            MAGN["PFN"] = PD["QPFN"+qsf]
            MAGN["NQ"] = PD["NQ"+qsf]
            MAGN["QSF"] = qsf
#        else:
#            vals = None
#            for ii in np.arange(0,len(eqlist)):
#                if vals is None:
#                    MAGN = dict()
#                    vals = RD[m[eqlist[ii]+'/q']] if eqlist[ii]+'/q' in m else None
#                    verrs = np.zeros(vals.shape) if vals is not None else None
#                    (MAGN["Q"],MAGN["QEB"],MAGN["QEM"],navgmap) = ptools.filtered_average(vals,value_errors=verrs)
#                    MAGN["PFN"] = RD["X_"+m[eqlist[ii]+'/q']] if vals is not None else None
#                    navgmap = np.atleast_2d(navgmap)
#                    MAGN["NQ"] = np.count_nonzero(navgmap,axis=0).flatten() if vals is not None else None
#                    MAGN["DDA"] = eqlist[ii].upper() if vals is not None else None
        PD["MAGN"] = MAGN

    if fdebug:
        print("process_aug.py: unpack_profile_data() completed.")

    return PD


def unpack_source_data(rawdata,newstruct=None,fdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Unpacks the required power deposition profile data from the raw extracted 
    data container, pre-processes it with statistical averaging if necessary,
    and stores it in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :returns: dict. Implementation-specific object with processed power profile data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata,dict):
        RD = rawdata
    if isinstance(newstruct,dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None
#        fullflag = True if "SELECTEQ" in RD and re.match(r'^all$',RD["SELECTEQ"],flags=re.IGNORECASE) else False

        warnings.filterwarnings("ignore",category=FutureWarning)

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['efit']
#        if fullflag:
#            for ii in np.arange(0,len(eqlist)):
#                if eqlist[ii].upper() not in PD:
#                    PD[eqlist[ii].upper()] = dict()

        # NBI heat source profile to electrons - calculated by PENCIL in chain2
        PD["NBQE"] = RD[m['nbp2/ge']] if 'nbp2/ge' in m else None
        PD["PFNNBQE"] = RD["X_"+m['nbp2/ge']] if PD["NBQE"] is not None else None

        # NBI heat source profile to ions - calculated by PENCIL in chain2
        PD["NBQI"] = RD[m['nbp2/gi']] if 'nbp2/gi' in m else None
        PD["PFNNBQI"] = RD["X_"+m['nbp2/gi']] if PD["NBQE"] is not None else None

        # NBI particle source profile - calculated by PENCIL in chain2
        PD["NBSFI"] = RD[m['nbp2/sv']] if 'nbp2/sv' in m else None
        PD["PFNNBSFI"] = RD["X_"+m['nbp2/sv']] if PD["NBSFI"] is not None else None

        # NBI fast ion source profile - calculated by PENCIL in chain2
        PD["NFI"] = RD[m['nbp2/nf']] if 'nbp2/nf' in m else None
        PD["PFNNFI"] = RD["X_"+m['nbp2/nf']] if PD["NFI"] is not None else None

        # RF heat source density profile to ion species 1 - calculated by PION in chain2
        PD["PDI1"] = RD[m['pion/pd1']] if 'pion/pd1' in m else None
        PD["PPDI1"] = RD["X_"+m['pion/pd1']] if PD["PDI1"] is not None else None

        # RF heat source density profile to ion species 2 - calculated by PION in chain2
        PD["PDI2"] = RD[m['pion/pd2']] if 'pion/pd2' in m else None
        PD["PPDI2"] = RD["X_"+m['pion/pd2']] if PD["PDI2"] is not None else None

        # RF heat source density profile to impurity species - calculated by PION in chain2
        PD["PDIMP"] = RD[m['pion/pdim']] if 'pion/pdim' in m else None
        PD["PPDIMP"] = RD["X_"+m['pion/pdim']] if PD["PDIMP"] is not None else None

        # RF heat source density profile to electrons - calculated by PION in chain2
        PD["PDE"] = RD[m['pion/pde']] if 'pion/pde' in m else None
        PD["PPDE"] = RD["X_"+m['pion/pde']] if PD["PDE"] is not None else None

        # RF heat source profile to electrons via collisions - calculated by PION in chain2
        PD["PDCE"] = RD[m['pion/pdce']] if 'pion/pdce' in m else None
        PD["PPDCE"] = RD["X_"+m['pion/pdce']] if PD["PDCE"] is not None else None

        # RF heat source profile to ions via collisions - calculated by PION in chain2
        PD["PDCI"] = RD[m['pion/pdci']] if 'pion/pdci' in m else None
        PD["PPDCI"] = RD["X_"+m['pion/pdci']] if PD["PDCI"] is not None else None

        # Flux surface averaged current density as a function of normalized polodial flux
        tag = 'jsur'
        vtag = "JSURF"        # Flux surface averaged current density(PSI_norm)
        xtag = "PJSURF"
        PD[vtag] = None
        PD[xtag] = None
        for ii in np.arange(0,len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqlist[ii].upper()][vtag] is not None else None

        # Flux surface average loop voltage as a function of normalized poloidal flux
        vtag = "VLOOP"        # Loop voltage(PSI_norm) - calculated from time derivative of PSI
        xtag = "PVLOOP"
        PD[vtag] = None
        PD[xtag] = None
        for ii in np.arange(0,len(eqlist)):
            if PD[vtag] is None:
                fbndl = None
                tbndl = None
                faxsl = None
                taxsl = None
                psi = None
                xpsi = None
                tpsi = None
                tag = 'fbnd'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    fbndl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    tbndl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if fbndl is not None else None
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    fbndl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                tag = 'faxs'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    faxsl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    taxsl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if faxsl is not None else None
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    faxsl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                tag = 'psno'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    psi = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                    xpsi = RD["X_"+m[eqlist[ii]+'/'+tag]] if psi is not None else None
                    tpsi = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if psi is not None else None
                if fbndl is not None and faxsl is not None and psi is not None:
                    fbnd = None
                    faxs = None
                    if fbndl.size > 1:
                        fbndfunc = interp1d(tbndl,fbndl,bounds_error=False,fill_value='extrapolate')
                        fbnd = fbndfunc(tpsi)
                    else:
                        fbnd = np.ones(tpsi.shape) * fbndl[0]
                    if faxsl.size > 1:
                        faxsfunc = interp1d(taxsl,faxsl,bounds_error=False,fill_value='extrapolate')
                        faxs = faxsfunc(tpsi)
                    else:
                        faxs = np.ones(tpsi.shape) * faxsl[0]
                    fpsi = np.zeros(psi.shape)
                    for pp in np.arange(0,fpsi.shape[0]):
                        fpsi[pp,:] = psi[pp,:] * (fbnd[pp] - faxs[pp]) + faxs[pp]
                    vloop = np.diff(fpsi,axis=0)
                    for qq in np.arange(0,vloop.shape[1]):
                        vloop[:,qq] = vloop[:,qq] / np.diff(tpsi)
                    verrs = np.zeros(vloop.shape)
                    (PD[vtag],PD[vtag+"EB"],PD[vtag+"EM"],nv) = ptools.filtered_average(vloop,value_errors=verrs)
                    PD[xtag] = xpsi.copy() if PD[vtag] is not None else None
            if len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                fbndl = None
                tbndl = None
                faxsl = None
                taxsl = None
                psi = None
                xpsi = None
                tpsi = None
                tag = 'fbnd'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    fbndl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    tbndl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if fbndl is not None else None
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    fbndl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                tag = 'faxs'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    faxsl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    taxsl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if faxsl is not None else None
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    faxsl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                tag = 'psno'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    psi = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                    xpsi = RD["X_"+m[eqlist[ii]+'/'+tag]] if psi is not None else None
                    tpsi = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if psi is not None else None
                if fbndl is not None and faxsl is not None and psi is not None:
                    fbnd = None
                    faxs = None
                    if fbndl.size > 1:
                        fbndfunc = interp1d(tbndl,fbndl,bounds_error=False,fill_value='extrapolate')
                        fbnd = fbndfunc(tpsi)
                    else:
                        fbnd = np.ones(tpsi.shape) * fbndl[0]
                    if faxsl.size > 1:
                        faxsfunc = interp1d(taxsl,faxsl,bounds_error=False,fill_value='extrapolate')
                        faxs = faxsfunc(tpsi)
                    else:
                        faxs = np.ones(tpsi.shape) * faxsl[0]
                    fpsi = np.zeros(psi.shape)
                    for pp in np.arange(0,fpsi.shape[0]):
                        fpsi[pp,:] = psi[pp,:] * (fbnd[pp] - faxs[pp]) + faxs[pp]
                    vloop = np.diff(fpsi,axis=0)
                    for qq in np.arange(0,vloop.shape[1]):
                        vloop[:,qq] = vloop[:,qq] / np.diff(tpsi)
                    verrs = np.zeros(vloop.shape)
                    (PD[eqtag][vtag],PD[eqtag][vtag+"EB"],PD[eqtag][vtag+"EM"],nv) = ptools.filtered_average(vloop,value_errors=verrs)
                    PD[eqtag][xtag] = xpsi.copy() if PD[eqtag][vtag] is not None else None

    if fdebug:
        print("process_aug.py: unpack_source_data() completed.")

    return PD


def calc_coords_with_bspline(procdata,cdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Calculates the relations between the various radial coordinate systems to
    be used for further processing. Interpolates all systems to be on the same
    101 point base uniformly spread in normalized poloidal flux coordinate,
    such that conversion between systems can be done easily with linear
    interpolations hereafter. Additionally computes the necessary Jacobians
    in order to convert derivatives with respect to one coordinate system into
    derivative with respect to another coordinate system.

    This function uses smoothed cubic B-splines to perform the necessary
    interpolations, with special care taken to not introduce discontinuities
    in the second derivative of the individual splines. This ensures
    smoothness in the first derivatives to be used for the coordinate
    Jacobians.

    Normalized poloidal flux was chosen to be the base coordinate in this
    implementation simply because most profile information in the AUG database
    system was already expressed with respect to this coordinate. This reduces
    the overall conversion errors attached to the data.

    Coordinates computed and associated names
        POLFLUXN = Normalized poloidal flux
        POLFLUX  = Absolute poloidal flux
        RHOPOLN  = Normalized poloidal rho (sqrt POLFLUXN)
        TORFLUXN = Normalized toroidal flux
        TORFLUX  = Absolute toroidal flux
        RHOTORN  = Normalized toroidal rho (sqrt TORFLUXN)
        RMAJORO  = Absolute outer major radius at height of magnetic axis
        RMAJORI  = Absolute inner major radius at height of magnetic axis
        RMINORO  = Absolute outer minor radius at height of magnetic axis
        RMINORI  = Absolute inner minor radius at height of magnetic axis
        FSVOL    = Flux surface volume
        FSAREA   = Flux surface surface area

    Prefix guide
        CS = Coordinate system
        CJ = Coordinate Jacobian
        CE = Coordinate error

    :arg procdata: dict. Implementation-specific object containing processed radial coordinate data.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :returns: dict. Identical to input object with unified coordinate system data inserted.
    """
    PD = None
    if isinstance(procdata,dict):
        PD = procdata

    if PD is not None:

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqdda = PD["EQLIST"][0].upper()
        eqlist = PD["EQLIST"] if len(PD["EQLIST"]) > 1 else []

        # Print coordinate system data from equilibrium, for debugging purposes
        if cdebug:
            debugdir = './coord_debug/'
            if not os.path.isdir(debugdir):
                os.makedirs(debugdir)
            if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
                with open(debugdir+'tfx.txt','w') as ff1:
                    for ii in np.arange(0,PD["TFX"].size):
                        ff1.write("   %15.6e   %15.6e\n" % (PD["PFN2TFX"][ii],PD["TFX"][ii]))
            if "PFNO" in PD and "PFNI" in PD and PD["PFNO"] is not None and PD["PFNI"] is not None:
                with open(debugdir+'pfn.txt','w') as ff2:
                    psin = np.hstack((PD["PFNI"][:0:-1],PD["PFNO"]))
                    rmaj = np.hstack((PD["RHF2PFN"][:-1],PD["RLF2PFN"]))
                    for ii in np.arange(0,psin.size):
                        ff2.write("   %15.6e   %15.6e\n" % (rmaj[ii],psin[ii]))
            if "VOL" in PD and PD["VOL"] is not None:
                with open(debugdir+'vol.txt','w') as ff3:
                    for ii in np.arange(0,PD["VOL"].size):
                        ff3.write("   %15.6e   %15.6e\n" % (PD["PFN2VOL"][ii],PD["VOL"][ii]))
            if "AREA" in PD and PD["AREA"] is not None:
                with open(debugdir+'area.txt','w') as ff4:
                    for ii in np.arange(0,PD["AREA"].size):
                        ff4.write("   %15.6e   %15.6e\n" % (PD["PFN2AREA"][ii],PD["AREA"][ii]))

        # Definition of the base coordinate system
        csb = "POLFLUXN"
        cjb = "POLFLUXN"
        PD["CS_PREFIXES"] = [""]
        PD["CS_BASE"] = csb
        PD["CJ_BASE"] = cjb
        PD["CS_"+csb] = np.linspace(0.0,1.0,101)
        PD["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD["CS_"+csb].shape)
        PD["CE_"+csb] = np.zeros(PD["CS_"+csb].shape)

        # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
        PD["CS_RHOPOLN"] = np.sqrt(PD["CS_POLFLUXN"])
        PD["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD["CS_RHOPOLN"]

        # Calculating absolute poloidal flux coordinate
        if "FBND" in PD and "FAXS" in PD and PD["FBND"] is not None and PD["FAXS"] is not None:
            PD["CS_POLFLUX"] = PD["CS_POLFLUXN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
            PD["CJ_"+cjb+"_POLFLUX"] = np.ones(PD["CS_"+csb].shape) / (PD["FBND"] - PD["FAXS"])
            PD["CE_POLFLUX"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_POLFLUX"] = None
            PD["CJ_"+cjb+"_POLFLUX"] = None
            PD["CE_POLFLUX"] = None

        # Calculating absolute toroidal flux coordinate and related coordinates
        if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
            xdata = PD["PFN2TFX"]
            ydata = PD["TFX"]
            spvals = splrep(xdata,ydata)
            PD["CS_POLFLUX"] = splev(xdata,spvals,der=0)
            PD["CJ_"+cjb+"_POLFLUX"] = 1.0 / splev(xdata,spvals,der=1)
            PD["CE_POLFLUX"] = np.zeros(xdata.shape)
        else:
            PD["CS_TORFLUX"] = None
            PD["CS_TORFLUXN"] = None
            PD["CS_RHOTORN"] = None
            PD["CJ_"+cjb+"_TORFLUX"] = None
            PD["CJ_"+cjb+"_TORFLUXN"] = None
            PD["CJ_"+cjb+"_RHOTORN"] = None
            PD["CE_TORFLUX"] = None
            PD["CE_TORFLUXN"] = None
            PD["CE_RHOTORN"] = None

        # Calculating absolute major and minor radius coordinates and related coordinates
        if "PFNO" in PD and "PFNI" in PD and PD["PFNO"] is not None and PD["PFNI"] is not None:
            xdata = np.hstack((PD["PFNI"][:0:-1],PD["PFNO"]))
            ydata = np.hstack((PD["RHF2PFN"][:-1],PD["RLF2PFN"]))
#            spvals = splrep(xdata,ydata)
#            PD["CS_POLFLUX"] = splev(xdata,spvals,der=0)
#            PD["CJ_"+cjb+"_POLFLUX"] = 1.0 / splev(xdata,spvals,der=1)
#            PD["CE_POLFLUX"] = np.zeros(xdata.shape)
        else:
            PD["CS_RMAJORO"] = None
            PD["CS_RMAJORI"] = None
            PD["CJ_"+cjb+"_RMAJORO"] = None
            PD["CJ_"+cjb+"_RMAJORI"] = None
            PD["CE_RMAJORO"] = None
            PD["CE_RMAJORI"] = None
            PD["CS_RMINORO"] = None
            PD["CS_RMINORI"] = None
            PD["CJ_"+cjb+"_RMINORO"] = None
            PD["CJ_"+cjb+"_RMINORI"] = None
            PD["CE_RMINORO"] = None
            PD["CE_RMINORI"] = None

        # Calculating flux surface volume coordinate
        if "VOL" in PD and PD["VOL"] is not None:
            xdata = PD["PDVOL"]
            ydata = PD["VOL"]
            spvals = splrep(xdata,ydata)
            PD["CS_FSVOL"] = splev(xdata,spvals,der=0)
            PD["CJ_"+cjb+"_FSVOL"] = 1.0 / splev(xdata,spvals,der=1)
            PD["CE_FSVOL"] = np.zeros(xdata.shape)
        else:
            PD["CS_FSVOL"] = None
            PD["CJ_"+cjb+"_FSVOL"] = None
            PD["CE_FSVOL"] = None

        # Calculating flux surface surface area coordinate
        if "AREA" in PD and PD["AREA"] is not None:
            xdata = PD["PDAREA"]
            ydata = PD["AREA"]
            spvals = splrep(xdata,ydata)
            PD["CS_FSAREA"] = splev(xdata,spvals,der=0)
            PD["CJ_"+cjb+"_FSAREA"] = 1.0 / splev(xdata,spvals,der=1)
            PD["CE_FSAREA"] = np.zeros(xdata.shape)
        else:
            PD["CS_FSAREA"] = None
            PD["CJ_"+cjb+"_FSAREA"] = None
            PD["CE_FSAREA"] = None

        # Performs identical coordinate system fitting for all magnetic equilibria, if specified, and stores it in a separate index
        for ii in np.arange(0,len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqtag in PD and PD[eqtag] is not None:

                # Definition of the base coordinate system
                PD[eqtag]["CS_BASE"] = csb
                PD[eqtag]["CJ_BASE"] = cjb
                PD[eqtag]["CS_"+csb] = np.linspace(0.0,1.0,101)
                PD[eqtag]["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD[eqtag]["CS_"+csb].shape)
                PD[eqtag]["CE_"+csb] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
                PD[eqtag]["CS_RHOPOLN"] = np.sqrt(PD[eqtag]["CS_POLFLUXN"])
                PD[eqtag]["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD[eqtag]["CS_RHOPOLN"]
                PD[eqtag]["CE_RHOPOLN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating absolute poloidal flux coordinate
                if "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag]["CS_POLFLUX"] = PD[eqtag]["CS_POLFLUXN"] * (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"]) + PD[eqtag]["FAXS"]
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = np.ones(PD[eqtag]["CS_POLFLUXN"].shape) / (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"])
                    PD[eqtag]["CE_POLFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_POLFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = None
                    PD[eqtag]["CE_POLFLUX"] = None

                # Calculating absolute toroidal flux coordinate and related coordinates
                if "TFX" in PD[eqtag] and "PFN2TFX" in PD[eqtag] and PD[eqtag]["TFX"] is not None and PD[eqtag]["PFN2TFX"] is not None:
                    xdata = PD[eqtag]["PFN2TFX"]
                    ydata = PD[eqtag]["TFX"]
                    spvals = splrep(xdata,ydata)
                    PD[eqtag]["CS_POLFLUX"] = splev(xdata,spvals,der=0)
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = 1.0 / splev(xdata,spvals,der=1)
                    PD[eqtag]["CE_POLFLUX"] = np.zeros(xdata.shape)
                else:
                    PD[eqtag]["CS_TORFLUX"] = None
                    PD[eqtag]["CS_TORFLUXN"] = None
                    PD[eqtag]["CS_RHOTORN"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUXN"] = None
                    PD[eqtag]["CJ_"+cjb+"_RHOTORN"] = None
                    PD[eqtag]["CE_TORFLUX"] = None
                    PD[eqtag]["CE_TORFLUXN"] = None
                    PD[eqtag]["CE_RHOTORN"] = None

                # Calculating absolute major and minor radius coordinates and related coordinates
                if "PFNO" in PD[eqtag] and "PFNI" in PD[eqtag] and PD[eqtag]["PFNO"] is not None and PD[eqtag]["PFNI"] is not None:
                    xdata = np.hstack((PD[eqtag]["PFNI"][:0:-1],PD[eqtag]["PFNO"]))
                    ydata = np.hstack((PD[eqtag]["RHF2PFN"][:-1],PD[eqtag]["RLF2PFN"]))
#                    spvals = splrep(xdata,ydata)
#                    PD[eqtag]["CS_POLFLUX"] = splev(xdata,spvals,der=0)
#                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = 1.0 / splev(xdata,spvals,der=1)
#                    PD[eqtag]["CE_POLFLUX"] = np.zeros(xdata.shape)
                else:
                    PD[eqtag]["CS_RMAJORO"] = None
                    PD[eqtag]["CS_RMAJORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = None
                    PD[eqtag]["CE_RMAJORO"] = None
                    PD[eqtag]["CE_RMAJORI"] = None
                    PD[eqtag]["CS_RMINORO"] = None
                    PD[eqtag]["CS_RMINORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = None
                    PD[eqtag]["CE_RMINORO"] = None
                    PD[eqtag]["CE_RMINORI"] = None

                # Calculating flux surface volume coordinate
                if "VOL" in PD[eqtag] and PD[eqtag]["VOL"] is not None:
                    xdata = PD[eqtag]["PDVOL"]
                    ydata = PD[eqtag]["VOL"]
                    spvals = splrep(xdata,ydata)
                    PD[eqtag]["CS_FSVOL"] = splev(xdata,spvals,der=0)
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = 1.0 / splev(xdata,spvals,der=1)
                    PD[eqtag]["CE_FSVOL"] = np.zeros(xdata.shape)
                else:
                    PD[eqtag]["CS_FSVOL"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = None
                    PD[eqtag]["CE_FSVOL"] = None

                # Calculating flux surface surface area coordinate
                if "AREA" in PD[eqtag] and PD[eqtag]["AREA"] is not None:
                    xdata = PD[eqtag]["PDAREA"]
                    ydata = PD[eqtag]["AREA"]
                    spvals = splrep(xdata,ydata)
                    PD[eqtag]["CS_FSAREA"] = splev(xdata,spvals,der=0)
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = 1.0 / splev(xdata,spvals,der=1)
                    PD[eqtag]["CE_FSAREA"] = np.zeros(xdata.shape)
                else:
                    PD[eqtag]["CS_FSAREA"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = None
                    PD[eqtag]["CE_FSAREA"] = None

    return PD


def calc_coords_with_gp(procdata,cdebug=False):
    """
    AUG-SPECIFIC FUNCTION
    Calculates the relations between the various radial coordinate systems to
    be used for further processing. Interpolates all systems to be on the same
    101 point base uniformly spread in normalized poloidal flux coordinate,
    such that conversion between systems can be done easily with linear
    interpolations hereafter. Additionally computes the necessary Jacobians
    in order to convert derivatives with respect to one coordinate system into
    derivative with respect to another coordinate system.

    This function uses the Gaussian process regression tool, GPR1D, to perform
    the necessary interpolations. This algorithm takes longer than traditional
    methods but has yielded more useable results without tweaking the free
    parameters of the fit, especially in the Jacobians.

    Normalized poloidal flux was chosen to be the base coordinate in this
    implementation simply because most profile information in the AUG database
    system was already expressed with respect to this coordinate. This reduces
    the overall conversion errors attached to the data.

    Coordinates computed and associated names
        POLFLUXN = Normalized poloidal flux
        POLFLUX  = Absolute poloidal flux
        RHOPOLN  = Normalized poloidal rho (sqrt POLFLUXN)
        TORFLUXN = Normalized toroidal flux
        TORFLUX  = Absolute toroidal flux
        RHOTORN  = Normalized toroidal rho (sqrt TORFLUXN)
        RMAJORO  = Absolute outer major radius at height of magnetic axis
        RMAJORI  = Absolute inner major radius at height of magnetic axis
        RMINORO  = Absolute outer minor radius at height of magnetic axis
        RMINORI  = Absolute inner minor radius at height of magnetic axis
        FSVOL    = Flux surface volume
        FSAREA   = Flux surface surface area

    Prefix guide
        CS_ = Coordinate system
        CJ_ = Coordinate Jacobian
        CC_ = Coordinate correction
        CE_ = Coordinate error

    :arg procdata: dict. Implementation-specific object containing processed radial coordinate data.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :returns: dict. Identical to input object with unified coordinate system data inserted.
    """
    PD = None
    if isinstance(procdata,dict):
        PD = procdata

    grabfit = itemgetter(0)
    grabGPR = itemgetter(0,4)

    if PD is not None:

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else []
        qdda = eqlist[0].upper() if len(eqlist) > 0 else None

        # Print coordinate system data from equilibrium, for debugging purposes
        if cdebug:
            debugdir = './coord_debug/'
            if not os.path.isdir(debugdir):
                os.makedirs(debugdir)
            if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
                with open(debugdir+'tfx.txt','w') as ff1:
                    for ii in np.arange(0,PD["TFX"].size):
                        ff1.write("   %15.6e   %15.6e\n" % (PD["PFN2TFX"][ii],PD["TFX"][ii]))
            if "PFNO" in PD and "PFNI" in PD and PD["PFNO"] is not None and PD["PFNI"] is not None:
                with open(debugdir+'pfn.txt','w') as ff2:
                    psin = np.hstack((PD["PFNI"][:0:-1],PD["PFNO"]))
                    rmaj = np.hstack((PD["RHF2PFN"][:0:-1],PD["RLF2PFN"]))
                    for ii in np.arange(0,psin.size):
                        ff2.write("   %15.6e   %15.6e\n" % (rmaj[ii],psin[ii]))
            if "VOL" in PD and PD["VOL"] is not None:
                with open(debugdir+'vol.txt','w') as ff3:
                    for ii in np.arange(0,PD["VOL"].size):
                        ff3.write("   %15.6e   %15.6e\n" % (PD["PFN2VOL"][ii],PD["VOL"][ii]))
            if "AREA" in PD and PD["AREA"] is not None:
                with open(debugdir+'area.txt','w') as ff4:
                    for ii in np.arange(0,PD["AREA"].size):
                        ff4.write("   %15.6e   %15.6e\n" % (PD["PFN2AREA"][ii],PD["AREA"][ii]))

        CSO = ptools.gp.GaussianProcessRegression1D()       # Initializing custom object for coordinate system fitting

        # Definition of the base coordinate system
        csb = "POLFLUXN"
        cjb = "POLFLUXN"
        PD["CS_PREFIXES"] = [""]
        PD["CS_BASE"] = csb
        PD["CJ_BASE"] = cjb
        PD["CS_"+csb] = np.linspace(0.0,1.0,101)
        PD["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD["CS_"+csb].shape)
        PD["CE_"+csb] = np.zeros(PD["CS_"+csb].shape)

        # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
        PD["CS_RHOPOLN"] = np.sqrt(PD["CS_POLFLUXN"])
        PD["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD["CS_RHOPOLN"]

        # Calculating absolute poloidal flux coordinate
        if "FBND" in PD and "FAXS" in PD and PD["FBND"] is not None and PD["FAXS"] is not None:
            PD["CS_POLFLUX"] = PD["CS_POLFLUXN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
            PD["CJ_"+cjb+"_POLFLUX"] = np.ones(PD["CS_"+csb].shape) / (PD["FBND"] - PD["FAXS"])
            PD["CE_POLFLUX"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_POLFLUX"] = None
            PD["CJ_"+cjb+"_POLFLUX"] = None
            PD["CE_POLFLUX"] = None

        # Calculating absolute toroidal flux coordinate and related coordinates
        if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Noise_Kernel(1.0e-3))
            (PD["CS_TORFLUX"],nkk) = \
                  grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"],kernel=kk,xdata=PD["PFN2TFX"],ydata=PD["TFX"],yerr='None',epsilon=1.0e-1,method='adadelta',spars=[1.0e-2,0.9]))
            PD["CS_TORFLUX"][0] = 0.0
            npfn = np.linspace(0.0,1.0,11)
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(npfn,kernel=nkk,xdata=PD["PFN2TFX"],ydata=PD["TFX"],epsilon='None',do_drv=True))
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,3.0e-1,1.0e0),ptools.gp.Noise_Kernel(1.0e-3))
            PD["CJ_"+cjb+"_TORFLUX"] = \
                  grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"],kernel=kk,xdata=npfn,ydata=1.0/gpdy,epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
            PD["CE_TORFLUX"] = np.zeros(PD["CS_"+csb].shape)
            tbnd = PD["TBND"] if "TBND" in PD and PD["TBND"] is not None and np.abs(PD["TBND"]) > 0.0 else PD["CS_TORFLUX"][-1]
            PD["CS_TORFLUXN"] = PD["CS_TORFLUX"] / tbnd
            PD["CJ_"+cjb+"_TORFLUXN"] = PD["CJ_"+cjb+"_TORFLUX"] * tbnd
            PD["CE_TORFLUXN"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RHOTORN"] = np.sqrt(PD["CS_TORFLUXN"])
            PD["CJ_"+cjb+"_RHOTORN"] = 2.0 * PD["CS_RHOTORN"] * PD["CJ_"+cjb+"_TORFLUXN"]
            PD["CE_RHOTORN"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_TORFLUX"] = None
            PD["CS_TORFLUXN"] = None
            PD["CS_RHOTORN"] = None
            PD["CJ_"+cjb+"_TORFLUX"] = None
            PD["CJ_"+cjb+"_TORFLUXN"] = None
            PD["CJ_"+cjb+"_RHOTORN"] = None
            PD["CE_TORFLUX"] = None
            PD["CE_TORFLUXN"] = None
            PD["CE_RHOTORN"] = None

        # Calculating absolute major and minor radius coordinates and related coordinates
        if "PFNO" in PD and "PFNI" in PD and PD["PFNO"] is not None and PD["PFNI"] is not None:
            psin = np.hstack((PD["PFNI"][:0:-1],PD["PFNO"]))
            rmaj = np.hstack((PD["RHF2PFN"][:0:-1],PD["RLF2PFN"]))
            nrmj = np.linspace(np.nanmin(rmaj),np.nanmax(rmaj),301)
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,5.0e-2,1.0e0),ptools.gp.Noise_Kernel(1.0e-2))
            (gpy,nkk) = grabGPR(CSO._GaussianProcessRegression1D__basic_fit(nrmj,kernel=kk,xdata=rmaj,ydata=psin,epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
            idx = np.where(np.diff(gpy[:-1]) * np.diff(gpy[1:]) <= 0.0)[0][0] + 1
            if "RMAG" in PD and PD["RMAG"] is not None:
                if np.abs(nrmj[idx] - PD["RMAG"]) > 1.0e-2:
                    PD["RMAG"] = float(nrmj[idx])
            else:
                PD["RMAG"] = float(nrmj[idx])
            rmjifunc = interp1d(gpy[:idx+1],nrmj[:idx+1],bounds_error=False,fill_value='extrapolate')
            rmjofunc = interp1d(gpy[idx:],nrmj[idx:],bounds_error=False,fill_value='extrapolate')
            PD["CS_RMAJORO"] = rmjofunc(PD["CS_POLFLUXN"])
            PD["CS_RMAJORO"][0] = PD["RMAG"]
            PD["CS_RMAJORI"] = rmjifunc(PD["CS_POLFLUXN"])
            PD["CS_RMAJORI"][0] = PD["RMAG"]
            rgeotemp = (PD["CS_RMAJORO"][-1] + PD["CS_RMAJORI"][-1]) / 2.0
            if "RGEO" in PD and PD["RGEO"] is not None:
                if np.abs(rgeotemp - PD["RGEO"]) > 1.0e-2:
                    PD["RGEO"] = float(rgeotemp)
            else:
                PD["RGEO"] = float(rgeotemp)
            nrmj = np.hstack((PD["CS_RMAJORI"][:0:-1],PD["CS_RMAJORO"]))
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(nrmj,kernel=nkk,xdata=rmaj,ydata=psin,epsilon='None',do_drv=True))
            PD["CJ_"+cjb+"_RMAJORO"] = gpdy[PD["CS_POLFLUXN"].size-1:]
            PD["CJ_"+cjb+"_RMAJORO"][0] = 0.0
            PD["CJ_"+cjb+"_RMAJORI"] = gpdy[PD["CS_POLFLUXN"].size-1::-1]
            PD["CJ_"+cjb+"_RMAJORI"][0] = 0.0
            PD["CE_RMAJORO"] = np.zeros(PD["CS_"+csb].shape)
            PD["CE_RMAJORI"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RMINORO"] = np.abs(PD["CS_RMAJORO"] - PD["CS_RMAJORO"][0])
            PD["CS_RMINORI"] = np.abs(PD["CS_RMAJORI"] - PD["CS_RMAJORI"][0])
            PD["CJ_"+cjb+"_RMINORO"] = PD["CJ_"+cjb+"_RMAJORO"]
            PD["CJ_"+cjb+"_RMINORI"] = -PD["CJ_"+cjb+"_RMAJORI"]
            PD["CE_RMINORO"] = np.zeros(PD["CS_"+csb].shape)
            PD["CE_RMINORI"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_RMAJORO"] = None
            PD["CS_RMAJORI"] = None
            PD["CJ_POLFLUXN_RMAJORO"] = None
            PD["CJ_POLFLUXN_RMAJORI"] = None
            PD["CE_RMAJORO"] = None
            PD["CE_RMAJORI"] = None
            PD["CS_RMINORO"] = None
            PD["CS_RMINORI"] = None
            PD["CJ_POLFLUXN_RMINORO"] = None
            PD["CJ_POLFLUXN_RMINORI"] = None
            PD["CE_RMINORO"] = None
            PD["CE_RMINORI"] = None

        # Calculating flux surface volume coordinate
        if "VOL" in PD and PD["VOL"] is not None:
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Linear_Kernel(1.0e1),ptools.gp.Noise_Kernel(1.0e-3))
            (PD["CS_FSVOL"],nkk) = \
                  grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"],kernel=kk,xdata=PD["PFN2VOL"],ydata=PD["VOL"],epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"],kernel=nkk,xdata=PD["PFN2VOL"],ydata=PD["VOL"],epsilon='None',do_drv=True))
            PD["CJ_"+cjb+"_FSVOL"] = 1.0 / gpdy
            PD["CE_FSVOL"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_FSVOL"] = None
            PD["CJ_"+cjb+"_FSVOL"] = None
            PD["CE_FSVOL"] = None

        # Calculating flux surface surface area coordinate
        if "AREA" in PD and PD["AREA"] is not None:
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Linear_Kernel(1.0e1),ptools.gp.Noise_Kernel(1.0e-3))
            (PD["CS_FSAREA"],nkk) = \
                  grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"],kernel=kk,xdata=PD["PFN2AREA"],ydata=PD["AREA"],epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"],kernel=nkk,xdata=PD["PFN2AREA"],ydata=PD["AREA"],epsilon='None',do_drv=True))
            PD["CJ_"+cjb+"_FSAREA"] = 1.0 / gpdy
            PD["CE_FSAREA"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_FSAREA"] = None
            PD["CJ_"+cjb+"_FSAREA"] = None
            PD["CE_FSAREA"] = None

        # Performs identical coordinate system fitting for all magnetic equilibria, if specified, and stores it in a separate index
        for ii in np.arange(0,len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqtag in PD and PD[eqtag] is not None:

                # Definition of the base coordinate system
                PD[eqtag]["CS_BASE"] = csb
                PD[eqtag]["CJ_BASE"] = cjb
                PD[eqtag]["CS_"+csb] = np.linspace(0.0,1.0,101)
                PD[eqtag]["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD[eqtag]["CS_"+csb].shape)
                PD[eqtag]["CE_"+csb] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
                PD[eqtag]["CS_RHOPOLN"] = np.sqrt(PD[eqtag]["CS_POLFLUXN"])
                PD[eqtag]["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD[eqtag]["CS_RHOPOLN"]
                PD[eqtag]["CE_RHOPOLN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating absolute poloidal flux coordinate
                if "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag]["CS_POLFLUX"] = PD[eqtag]["CS_POLFLUXN"] * (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"]) + PD[eqtag]["FAXS"]
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = np.ones(PD[eqtag]["CS_POLFLUXN"].shape) / (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"])
                    PD[eqtag]["CE_POLFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_POLFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = None
                    PD[eqtag]["CE_POLFLUX"] = None

                # Calculating absolute toroidal flux coordinate and related coordinates
                if "TFX" in PD[eqtag] and "PFN2TFX" in PD[eqtag] and PD[eqtag]["TFX"] is not None and PD[eqtag]["PFN2TFX"] is not None:
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Noise_Kernel(1.0e-3))
                    (PD[eqtag]["CS_TORFLUX"],nkk) = \
                          grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"],kernel=kk,xdata=PD[eqtag]["PFN2TFX"],ydata=PD[eqtag]["TFX"],epsilon=1.0e-1,method='adadelta',spars=[1.0e-2,0.9]))
                    PD[eqtag]["CS_TORFLUX"][0] = 0.0
                    npfn = np.linspace(0.0,1.0,11)
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(npfn,kernel=nkk,xdata=PD[eqtag]["PFN2TFX"],ydata=PD[eqtag]["TFX"],epsilon='None',do_drv=True))
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,3.0e-1,1.0e0),ptools.gp.Noise_Kernel(1.0e-3))
                    PD[eqtag]["CJ_POLFLUXN_TORFLUX"] = \
                          grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"],kernel=kk,xdata=npfn,ydata=1.0/gpdy,epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
                    PD[eqtag]["CE_TORFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    tbnd = PD["TBND"] if "TBND" in PD and PD["TBND"] is not None and np.abs(PD["TBND"]) > 0.0 else PD["CS_TORFLUX"][-1]
                    PD[eqtag]["CS_TORFLUXN"] = PD[eqtag]["CS_TORFLUX"] / tbnd
                    PD[eqtag]["CJ_"+cjb+"_TORFLUXN"] = PD[eqtag]["CJ_"+cjb+"_TORFLUX"] * tbnd
                    PD[eqtag]["CE_TORFLUXN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CS_RHOTORN"] = np.sqrt(PD[eqtag]["CS_TORFLUXN"])
                    PD[eqtag]["CJ_"+cjb+"_RHOTORN"] = 2.0 * PD[eqtag]["CS_RHOTORN"] * PD[eqtag]["CJ_"+cjb+"_TORFLUXN"]
                    PD[eqtag]["CE_RHOTORN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_TORFLUX"] = None
                    PD[eqtag]["CS_TORFLUXN"] = None
                    PD[eqtag]["CS_RHOTORN"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUXN"] = None
                    PD[eqtag]["CJ_"+cjb+"_RHOTORN"] = None
                    PD[eqtag]["CE_TORFLUX"] = None
                    PD[eqtag]["CE_TORFLUXN"] = None
                    PD[eqtag]["CE_RHOTORN"] = None

                # Calculating absolute major and minor radius coordinates and related coordinates
                if "PFNO" in PD[eqtag] and "PFNI" in PD[eqtag] and PD[eqtag]["PFNO"] is not None and PD[eqtag]["PFNI"] is not None:
                    psin = np.hstack((PD[eqtag]["PFNI"][:0:-1],PD[eqtag]["PFNO"]))
                    rmaj = np.hstack((PD[eqtag]["RHF2PFN"][:0:-1],PD[eqtag]["RLF2PFN"]))
                    nrmj = np.linspace(np.nanmin(rmaj),np.nanmax(rmaj),301)
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Noise_Kernel(1.0e-2))
                    (gpy,nkk) = grabGPR(CSO._GaussianProcessRegression1D__basic_fit(nrmj,kernel=kk,xdata=rmaj,ydata=psin,epsilon=1.0e-1,method='adadelta',spars=[1.0e-2,0.9]))
                    idx = np.where(np.diff(gpy[:-1]) * np.diff(gpy[1:]) <= 0.0)[0][0] + 1
                    if "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                        if np.abs(nrmj[idx] - PD[eqtag]["RMAG"]) > 1.0e-2:
                            PD[eqtag]["RMAG"] = float(nrmj[idx])
                    else:
                        PD[eqtag]["RMAG"] = float(nrmj[idx])
                    rmjifunc = interp1d(gpy[:idx+1],nrmj[:idx+1],bounds_error=False,fill_value='extrapolate')
                    rmjofunc = interp1d(gpy[idx:],nrmj[idx:],bounds_error=False,fill_value='extrapolate')
                    PD[eqtag]["CS_RMAJORO"] = rmjofunc(PD[eqtag]["CS_POLFLUXN"])
                    PD[eqtag]["CS_RMAJORO"][0] = PD[eqtag]["RMAG"]
                    PD[eqtag]["CS_RMAJORI"] = rmjifunc(PD[eqtag]["CS_POLFLUXN"])
                    PD[eqtag]["CS_RMAJORI"][0] = PD[eqtag]["RMAG"]
                    nrmj = np.hstack((PD[eqtag]["CS_RMAJORI"][:0:-1],PD[eqtag]["CS_RMAJORO"]))
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(nrmj,kernel=nkk,xdata=rmaj,ydata=psin,epsilon='None',do_drv=True))
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = gpdy[PD[eqtag]["CS_POLFLUXN"].size-1:]
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = gpdy[PD[eqtag]["CS_POLFLUXN"].size-1::-1]
                    PD[eqtag]["CE_RMAJORO"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CE_RMAJORI"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CS_RMINORO"] = np.abs(PD[eqtag]["CS_RMAJORO"] - PD[eqtag]["CS_RMAJORO"][0])
                    PD[eqtag]["CS_RMINORI"] = np.abs(PD[eqtag]["CS_RMAJORI"] - PD[eqtag]["CS_RMAJORI"][0])
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = PD[eqtag]["CJ_POLFLUXN_RMAJORO"]
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = -PD[eqtag]["CJ_POLFLUXN_RMAJORI"]
                    PD[eqtag]["CE_RMINORO"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CE_RMINORI"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_RMAJORO"] = None
                    PD[eqtag]["CS_RMAJORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = None
                    PD[eqtag]["CE_RMAJORO"] = None
                    PD[eqtag]["CE_RMAJORI"] = None
                    PD[eqtag]["CS_RMINORO"] = None
                    PD[eqtag]["CS_RMINORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = None
                    PD[eqtag]["CE_RMINORO"] = None
                    PD[eqtag]["CE_RMINORI"] = None

                # Calculating flux surface volume coordinate
                if "VOL" in PD[eqtag] and PD[eqtag]["VOL"] is not None:
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Linear_Kernel(1.0e1),ptools.gp.Noise_Kernel(1.0e-3))
                    (PD[eqtag]["CS_FSVOL"],nkk) = \
                          grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"],kernel=kk,xdata=PD[eqtag]["PFN2VOL"],ydata=PD[eqtag]["VOL"],epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"],kernel=nkk,xdata=PD[eqtag]["PFN2VOL"],ydata=PD[eqtag]["VOL"],epsilon='None',do_drv=True))
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = 1.0 / gpdy
                    PD[eqtag]["CE_FSVOL"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_FSVOL"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = None
                    PD[eqtag]["CE_FSVOL"] = None

                # Calculating flux surface surface area coordinate
                if "AREA" in PD[eqtag] and PD[eqtag]["AREA"] is not None:
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0,1.0e-1,1.0e0),ptools.gp.Linear_Kernel(1.0e1),ptools.gp.Noise_Kernel(1.0e-3))
                    (PD[eqtag]["CS_FSAREA"],nkk) = \
                          grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"],kernel=kk,xdata=PD[eqtag]["PFN2AREA"],ydata=PD[eqtag]["AREA"],epsilon=1.0e-2,method='adadelta',spars=[1.0e-2,0.9]))
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"],kernel=nkk,xdata=PD[eqtag]["PFN2AREA"],ydata=PD[eqtag]["AREA"],epsilon='None',do_drv=True))
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = 1.0 / gpdy
                    PD[eqtag]["CE_FSAREA"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_FSAREA"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = None
                    PD[eqtag]["CE_FSAREA"] = None

    return PD


def apply_coordinate_shift(procdata,z_shift=None):
    """
    AUG-SPECIFIC FUNCTION
    Applies a coordinate shift to the primary kinetic profiles (electron
    density, electron and ion temperatures, and angular frequency) according
    to the input z_shift argument. This is meant to approximate a vertical
    shift of the equilibrium such that the separatrix on the low-field-side
    is consistent with SOL model predictions. This is a first order
    correction to known magnetic equilibrium issues in later AUG discharges,
    and the applicability of procedure across all discharges is not tested
    and most probably also not correct.

    Note that this shift does NOT adjust the other magnetic equilibrium data
    to obtain a self-consistent description of the equilibrium.
    """
    PD = None
    if isinstance(procdata,dict):
        PD = procdata
    shift = float(edge_shift) if isinstance(edge_shift,(int,float)) else None

    if PD is not None and shift is not None:

        PD["ZSHIFT"] = shift
        PD["ZSHIFTEB"] = 0.0

        diag = "HRTS"
        quantities = ["NE","TE"]
        if diag in PD and PD[diag] is not None:
            for dtag in quantities:
                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

        diag = "LIDR"
        quantities = ["NE","TE"]
        if diag in PD and PD[diag] is not None:
            for dtag in quantities:
                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

        diag = "ECM"
        quantities = ["TE"]
        if diag in PD and PD[diag] is not None:
            for dtag in quantities:
                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]
                elif "PFN"+dtag in PD[diag] and PD[diag]["PFN"+dtag] is not None and "CS_POLFLUXN" in PD and PD["CS_POLFLUXN"] is not None:
                    rmajvec = np.full(PD[diag]["PFN"+dtag].shape,np.NaN)
                    fhfs = (PD[diag]["PFN"+dtag] < 0.0)
                    if np.any(fhfs) and "CS_RMAJORI" in PD and PD["CS_RMAJORI"] is not None:
                        ifunc = interp1d(PD["CS_POLFLUXN"],PD["CS_RMAJORI"],kind='linear',bounds_error=False,fill_value='extrapolate')
                        rmajvec[fhfs] = ifunc(np.abs(PD[diag]["PFN"+dtag][fhfs]))
                    if not np.all(fhfs) and "CS_RMAJORO" in PD and PD["CS_RMAJORO"] is not None:
                        ofunc = interp1d(PD["CS_POLFLUXN"],PD["CS_RMAJORO"],kind='linear',bounds_error=False,fill_value='extrapolate')
                        rmajvec[np.invert(fhfs)] = ofunc(np.abs(PD[diag]["PFN"+dtag][np.invert(fhfs)]))
                    PD[diag]["RM"+dtag] = rmajvec + PD["RMAJSHIFT"]
                    del PD[diag]["PFN"+dtag]

        diag = "CX"
        quantities = ["TI","AF","NIMP"]
        if diag in PD and PD[diag] is not None:
            for key in PD[diag]:
                for dtag in quantities:
                    if "RM"+dtag in PD[diag][key] and PD[diag][key]["RM"+dtag] is not None:
                        PD[diag][key]["RM"+dtag] = PD[diag][key]["RM"+dtag] + PD["RMAJSHIFT"]

        # Undecided whether to apply coordinate shift to the following profiles, code provided here in case
#        diag = "MAGN"
#        quantities = ["Q"]
#        if diag in PD and PD[diag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
#                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "NBP2"
#        quantities = ["QE","QI","S","J","TAU","NFIA","WFIA"]
#        if qtag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "PION"
#        quantities = ["QE","QI","NFIA","WFIA"]
#        if ctag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "OHM"
#        quantities = ["QE","QI","J"]
#        if ctag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

    return PD


def calculate_coordinate_systems(procdata,cdebug=False,fdebug=False):
    """
    """
    PD = None
    if isinstance(procdata,dict):
        PD = procdata

    if PD is not None:

        # Perform coordinate system unification and interpolation
        if "GPCOORDFLAG" in PD and PD["GPCOORDFLAG"]:
            try:
                PD = calc_coords_with_gp(PD,cdebug=cdebug)
            except (KeyError,ValueError,AttributeError):
                raise ValueError("Coordinate system interpolation failed.")

            # Perform coordinate shift according to tanh fit, setting new separatrix location at Te = 100 eV
            if "RMAJEDGE" in PD and PD["RMAJEDGE"] is not None and "RSHIFTFLAG" in PD and PD["RSHIFTFLAG"]:
                rmajedge_old = 3.96
                if "RMJO" in PD and PD["RMJO"] is not None:
                    rmajedge_old = PD["RMJO"][-1]
                shift = float(rmajedge_old - PD["RMAJEDGE"])
                PD = apply_coordinate_shift(PD,edge_shift=shift)

        # TODO: Need to test and tune B-spline coordinate interpolation system, also on derivatives

    if fdebug:
        print("process_aug.py: calculate_coordinate_systems() completed.")

    return PD
