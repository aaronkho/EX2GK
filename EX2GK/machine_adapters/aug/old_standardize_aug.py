# Script with functions to standardize extracted AUG data structure for final representation
# Developer: Aaron Ho - 10/07/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz

# Internal package imports
from EX2GK.tools.general import proctools as ptools


def process_averaged_profile(procdata,profdata,quantity,corrflag=False,rhoerrflag=True):
    """
    AUG-SPECIFIC FUNCTION
    Processes and formats raw profile data from pre-processing step into a
    standardized profile structure for the generalized fitting routine.
    
    Developer note - This function can be made into a general function, but
    that would require field names from processing step to be standardized
    as well. As such, it was best to leave this as implementation-specific.

    :arg procdata: dict. Implementation-specific object containing unified coordinate system and extraneous processed data.

    :arg profdata: dict. Implementation-specific object containing only the processed plasma profile data.

    :arg quantity: str. The specific quantity represented by the input plasma profile data, used to flag type of processing.

    :kwarg corrflag: bool. Flag to indicate if the corrected coordinate systems were applied to the profiles.

    :kwarg rhoerrflag: bool. Flag to toggle inclusion of radial errors coming from coordinate system conversion.

    :returns: dict. Formatted object containing the processed profile data (and fit parameters).
    """
    PD = None
    FD = None
    tag = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(profdata,dict):
        FD = copy.deepcopy(profdata)
    if isinstance(quantity,str):
        tag = quantity

    if PD is not None and FD is not None and tag is not None:

        # Define the radial position of the magnetic axis for processing of the diagnostic data
        ctag = 'C' if corrflag else ''
        ormag = PD["RMAG"] if "RMAG" in PD else None
        rmag = PD[ctag+"RMAG"] if ctag+"RMAG" in PD else None
        if ormag is None:
            psimin = np.amin(np.abs(PD["CS_POLFLUXN"]))
            aidx = np.where(np.abs(PD["CS_POLFLUXN"]) == psimin)[0][0]
            ormag = np.average([PD["CS_RMAJORO"][aidx],PD["CS_RMAJORI"][aidx]])
        if rmag is None:
            psimin = np.amin(np.abs(PD["CS_POLFLUXN"]))
            aidx = np.where(np.abs(PD["CS_POLFLUXN"]) == psimin)[0][0]
            rmag = np.average([PD["CS_"+ctag+"RMAJORO"][aidx],PD["CS_"+ctag+"RMAJORI"][aidx]])

        # For CX, MAGN at AUG
        if "PFN"+tag in FD and FD["PFN"+tag] is not None:
            FD["OPFN"] = FD["PFN"+tag].copy()
            ofhfs = (FD["R"+tag] < ormag) if "R"+tag in FD else (FD["OPFN"] < 0.0)
            (FD["PFN"],dummyj,FD["PFNEB"]) = ptools.convert_coords(PD,np.abs(FD["PFN"+tag]),"POLFLUXN",ctag+"POLFLUXN")
            if not rhoerrflag:
                FD["PFNEB"] = np.zeros(FD["PFN"].shape)
            (rmajo,dummyj,dummye) = ptools.convert_coords(PD,FD["PFN"][np.invert(ofhfs)],"POLFLUXN","RMAJORO",corrflag=corrflag)
            (rmaji,dummyj,dummye) = ptools.convert_coords(PD,FD["PFN"][ofhfs],"POLFLUXN","RMAJORI",corrflag=corrflag)
            rmaj = np.hstack((rmaji,rmajo))
            fhfs = (rmaj < rmag)
            FD["AXID"] = np.where(np.invert(fhfs))[0][0] if np.any(fhfs) else 0
            FD["RHOPN"] = np.sqrt(np.abs(FD["PFN"]))
            pfilt = np.invert(FD["RHOPN"] == 0.0)
            if "RHOPNEB" not in FD:
                FD["RHOPNEB"] = np.zeros(FD["RHOPN"].shape)
            FD["RHOPNEB"][pfilt] = np.sqrt(np.power(FD["RHOPNEB"][pfilt],2.0) + np.power(0.5 * FD["PFNEB"][pfilt] / FD["RHOPN"][pfilt],2.0))
            (FD["TFN"],dummyj,pteb) = ptools.convert_coords(PD,FD["PFN"],"POLFLUXN","TORFLUXN",corrflag=corrflag)
            FD["TFNEB"] = np.sqrt(np.power(FD["PFNEB"],2.0) + np.power(pteb,2.0)) if rhoerrflag else np.zeros(FD["PFNEB"].shape)
            FD["TFN"][FD["TFN"] < 1.0e-4] = 0.0   # Enforces true zero at magnetic axis
            FD["RHOTN"] = np.sqrt(FD["TFN"])
            tfilt = np.invert(FD["RHOTN"] == 0.0)
            FD["RHOTNEB"] = np.full(FD["RHOTN"].shape,0.002) if rhoerrflag else np.zeros(FD["RHOTN"].shape)
            FD["RHOTNEB"][tfilt] = 0.5 * FD["TFNEB"][tfilt] / FD["RHOTN"][tfilt]
            if len(fhfs) > 0:
                FD["PFN"][fhfs] = -FD["PFN"][fhfs]       # pol_flux from -1 to 1 (-1 is inner LCFS)
                FD["RHOPN"][fhfs] = -FD["RHOPN"][fhfs]   # rho_pol from -1 to 1 (-1 is inner LCFS)
                FD["TFN"][fhfs] = -FD["TFN"][fhfs]       # tor_flux from -1 to 1 (-1 is inner LCFS)
                FD["RHOTN"][fhfs] = -FD["RHOTN"][fhfs]   # rho_tor from -1 to 1 (-1 is inner LCFS)

    return FD


def standardize_diagnostic_data(procdata,newstruct=None):
    """
    JET-SPECIFIC FUNCTION
    Converts all profiles from the various diagnostic data into
    the same coordinate system and format for improved clarity
    during further processing and profile fitting.

    :arg procdata: dict. Implementation-specific object containing processed extracted data.

    :kwarg newstruct: dict. Optional object in which formatted data will be stored.

    :returns: dict. Formatted object containing all profile data for additional processing.
    """
    PD = dict()
    DD = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(newstruct,dict):
        DD = newstruct
    else:
        DD = dict()

    cf = PD["CC_FLAG"] if "CC_FLAG" in PD else False
    rf = PD["RHOEBFLAG"] if "RHOEBFLAG" in PD else False
    DD["POLFLAG"] = True if "POLFLAG" in PD and PD["POLFLAG"] else False
    DD["MIXDIAGFLAG"] = True if "MIXDIAGFLAG" in PD and PD["MIXDIAGFLAG"] else False
    DD["RAWDIAGFLAG"] = True if "RAWDIAGFLAG" in PD and PD["RAWDIAGFLAG"] else False

    # Remember that you modified process function to deal with each quantity at a time

    diag = "HRTS"
    quantities = ["NE","TE"]
    if diag in PD and PD[diag] is not None:
        for dtag in quantities:
            if dtag in PD[diag] and PD[diag][dtag] is not None:
                DD[diag] = process_averaged_profile(PD,PD[diag],quantity=dtag,corrflag=cf,rhoerrflag=rf)

    diag = "LIDR"
    quantities = ["NE","TE"]
    if diag in PD and PD[diag] is not None:
        for dtag in quantities:
            if dtag in PD[diag] and PD[diag][dtag] is not None:
                DD[diag] = process_averaged_profile(PD,PD[diag],quantity=dtag,corrflag=cf,rhoerrflag=rf)

    diag = "ECM"
    quantities = ["TE"]
    if diag in PD and PD[diag] is not None:
        for dtag in quantities:
            if dtag in PD[diag] and PD[diag][dtag] is not None:
                DD[diag] = process_averaged_profile(PD,PD[diag],quantity=dtag,corrflag=cf,rhoerrflag=rf)

        # Heuristic modifications to ECM data, not general to TE profiles
        if diag in DD and "TE" in DD[diag]:
            # Outer edge ECM data not trusted due to optically thin plasma
            rfilt = (DD[diag]["RHOTN"] <= 0.5)
            DD[diag]["TEEB"] = DD[diag]["TEEB"][rfilt]
            DD[diag]["TE"] = DD[diag]["TE"][rfilt]
            if "RTE" in DD[diag]:
                DD[diag]["RTE"] = DD[diag]["RTE"][rfilt]
            if "PFN" in DD[diag]:
                DD[diag]["PFN"] = DD[diag]["PFN"][rfilt]
            DD[diag]["RHOTN"] = DD[diag]["RHOTN"][rfilt]
            DD[diag]["RHOTNEB"] = DD[diag]["RHOTNEB"][rfilt]
            DD[diag]["RHOPN"] = DD[diag]["RHOPN"][rfilt]
            DD[diag]["RHOPNEB"] = DD[diag]["RHOPNEB"][rfilt]

            # Suspected underestimation in ECM error as plasma becomes more optically thin at larger rho
            DD[diag]["TEEB"] = DD[diag]["TEEB"] + 0.2 * DD[diag]["RHOTN"] * DD[diag]["TE"]

    # The charge exchange diagnostic is different from others at AUG, large amount of separate fields
    diag = "CX"
    quantities = ["TI","AF"]
    if diag in PD and PD[diag] is not None:
        DD[diag] = dict()
        for key in PD[diag]:
            DD[diag][key] = dict()
            for dtag in quantities:
                if dtag in PD[diag][key] and PD[diag][key][dtag] is not None:
                    DD[diag][key][dtag] = process_averaged_profile(PD,PD[diag][key],quantity=dtag,corrflag=cf,rhoerrflag=rf)

    diag = "MAGN"
    quantities = ["Q"]
    if diag in PD and PD[diag] is not None:
        DD[diag] = dict()
        for dtag in quantities:
            if dtag in PD[diag] and PD[diag][dtag] is not None:
                DD[diag][dtag] = process_averaged_profile(PD,PD[diag],quantity=dtag,corrflag=cf,rhoerrflag=rf)

    return DD


def get_standardized_profile_data(diagdata,label,hfsflag=False):
    """
    JET-SPECIFIC FUNCTION
    Extracts the raw diagnostic data from the input object with the
    desired radial coordinate vector. This function is nearly
    general but requires the diagnostic data fields to be specified
    in a certain way.

    :arg diagdata: dict. Formatted object containing raw diagnostic profile data.

    :arg label: str. Name of the quantity to be extracted.

    :kwarg hfsflag: bool. Flag which toggle usage of high-field side data.

    :returns: (array, array, array, array).
        Vector of radial points, vector of errors in radial points corresponding to the
        radial points, vector of specified quantity from diagnostic data corresponding
        to the radial points, vector of errors in specified quantity from diagnostic
        data corresponding to the radial points.
    """
    DD = None
    dtag = None
    if isinstance(diagdata,dict):
        DD = diagdata
        if isinstance(label,str) and label in DD and DD[label] is not None:
            dtag = label

    xval = None
    xerr = None
    yval = None
    yerr = None
    if DD is not None and dtag is not None and "AXID" in DD and DD["AXID"] is not None:
        idx = DD["AXID"]
        if "POLFLAG" in DD and DD["POLFLAG"] and "RHOPN" in RD:
            xval = -DD["RHOPN"][idx::-1] if hfsflag else DD["RHOPN"][idx:]
            xerr = np.zeros(xval.shape)
            if "RHOPNEB" in DD and DD["RHOPNEB"] is not None:
                xerr = DD["RHOPNEB"][idx::-1] if hfsflag else DD["RHOPNEB"][idx:]
        elif "RHOTN" in DD:
            xval = -DD["RHOTN"][idx::-1] if hfsflag else DD["RHOTN"][idx:]
            xerr = np.zeros(xval.shape)
            if "RHOTNEB" in DD and DD["RHOTNEB"] is not None:
                xerr = DD["RHOTNEB"][idx::-1] if hfsflag else DD["RHOTNEB"][idx:]
        yval = DD[dtag][idx::-1] if hfsflag else DD[dtag][idx:]
        yerr = np.zeros(yval.shape)
        if dtag+"EB" in DD and DD[dtag+"EB"] is not None:
            yerr = DD[dtag+"EB"][idx::-1] if hfsflag else DD[dtag+"EB"][idx:]

    return (xval,xerr,yval,yerr)


def get_standardized_impurity_data(diagdata):
    """
    JET-SPECIFIC FUNCTION
    Extracts the impurity ion data from the input diagnostic data
    object. This function is nearly general but requires the
    diagnostic data fields to be specified in a certain way.

    :arg diagdata: dict. Formatted object containing raw diagnostic impurity data.

    :returns: (float, float).
        Mass number of impurity ion observed by diagnostic, charge number of impurity
        ion observed by diagnostic.
    """
    DD = None
    if isinstance(diagdata,dict):
        DD = diagdata

    aimp = None
    zimp = None
    if DD is not None:
        aimp = float(DD["AIMP"]) if "AIMP" in DD and DD["AIMP"] is not None else -1.0
        zimp = float(DD["ZIMP"]) if "ZIMP" in DD and DD["ZIMP"] is not None else 0.0
        if aimp > 0.0 and zimp > (aimp / 2.0 + 0.6):
            zimp = aimp / 2.0

    return (aimp,zimp)


def add_filtered_profile_data(tag,newstruct=None,xval=None,xerr=None,yval=None,yerr=None):
    """
    STANDARDIZED FUNCTION
    Adds input data into standardized fields for the specified
    processed profiles, creating these fields if they do not
    already exist.

    If an object is passed in via the newstruct argument, this
    function first checks whether or not the fields already
    exist in that object. If so, the new data is appended to
    the existing data.

    :arg tag: str. The specific quantity represented by the input plasma profile data, used to flag type of processing.

    :kwarg newstruct: dict. Optional object in which new fields and formatted data will be stored.

    :kwarg xval: array. Vector of radial points.

    :kwarg xerr: array. Vector of errors in radial points corresponding to the radial points.

    :kwarg yval: array. Vector of specified quantity from diagnostic data corresponding to the radial points.

    :kwarg yerr: array. Vector of errors in specified quantity from diagnostic data corresponding to the radial points.

    :returns: dict. Object with identical structure as input object except with input profile appended to standard fields.
    """
    otag = None
    SD = None
    if isinstance(tag,str) and tag:
        otag = tag.upper()
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()

    xx = np.array([])
    xe = np.array([])
    yy = np.array([])
    ye = np.array([])
    if isinstance(xval,(list,tuple,np.ndarray)):
        xx = np.array(xval).flatten()
    elif isinstance(xval,(int,float)):
        xx = np.array([float(xval)])
    if isinstance(xerr,(list,tuple,np.ndarray)):
        xe = np.array(xerr).flatten()
    elif isinstance(xerr,(int,float)):
        xe = np.array([float(xerr)])
    if isinstance(yval,(list,tuple,np.ndarray)):
        yy = np.array(yval).flatten()
    elif isinstance(yval,(int,float)):
        yy = np.array([float(yval)])
    if isinstance(yerr,(list,tuple,np.ndarray)):
        ye = np.array(yerr).flatten()
    elif isinstance(yerr,(int,float)):
        ye = np.array([float(yerr)])

    idx = None
    if otag is not None:
        idx = 0
        if otag+"RAW" not in SD:
            SD[otag+"RAW"] = np.array([])
        if otag+"RAWEB" not in SD:
            SD[otag+"RAWEB"] = np.array([])
        if otag+"RAWX" not in SD:
            SD[otag+"RAWX"] = np.array([])
        if otag+"RAWXEB" not in SD:
            SD[otag+"RAWXEB"] = np.array([])
        if otag+"DMAP" not in SD:
            SD[otag+"DMAP"] = np.array([])
        elif SD[otag+"DMAP"].size > 0:
            idx = np.rint(np.nanmax(SD[otag+"DMAP"])) + 1

        if xx.size > 0 and xx.size == xe.size and xx.size == yy.size and xe.size == ye.size:
            SD[otag+"RAW"] = np.hstack((SD[otag+"RAW"],yy))
            SD[otag+"RAWEB"] = np.hstack((SD[otag+"RAWEB"],ye))
            SD[otag+"RAWX"] = np.hstack((SD[otag+"RAWX"],xx))
            SD[otag+"RAWXEB"] = np.hstack((SD[otag+"RAWXEB"],xe))
            SD[otag+"DMAP"] = np.hstack((SD[otag+"DMAP"],np.full(xx.shape,idx)))

    return SD


def add_filtered_impurity_data(newstruct=None,adata=None,zdata=None,xdata=None,edgeflag=False):
    """
    STANDARDIZED FUNCTION
    Adds input data into standardized fields for the specified
    processed impurity data, creating these fields if they do
    not already exist.

    If an object is passed in via the newstruct argument, this
    function first checks whether or not the fields already
    exist in that object. If so, the new data is appended to
    the existing data.

    :kwarg newstruct: dict. Optional object in which new fields and formatted data will be stored.

    :kwarg adata: array. Mass number of impurity ion observed by diagnostic.

    :kwarg zdata: array. Charge number of impurity ion observed by diagnostic.

    :kwarg xdata: array. Vector of radial points.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :returns: dict. Object with identical structure as input object except with input profile appended to standard fields.
    """
    SD = None
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()

    aa = np.array([])
    zz = np.array([])
    if isinstance(adata,(list,tuple,np.ndarray)):
        aa = np.array(adata).flatten()
    elif isinstance(adata,(int,float)):
        nume = 1
        if isinstance(xdata,(list,tuple)):
            nume = len(xdata)
        elif isinstance(xdata,np.ndarray):
            nume = xdata.size
        aa = np.array([float(adata)] * nume).flatten()
    if isinstance(zdata,(list,tuple,np.ndarray)):
        zz = np.array(zdata).flatten()
    elif isinstance(zdata,(int,float)):
        nume = 1
        if isinstance(xdata,(list,tuple)):
            nume = len(xdata)
        elif isinstance(xdata,np.ndarray):
            nume = xdata.size
        zz = np.array([float(zdata)] * nume).flatten()

    etag = 'E' if edgeflag else ''
    if etag+"AIMPRAW" not in SD:
        SD[etag+"AIMPRAW"] = np.array([])
    if etag+"ZIMPRAW" not in SD:
        SD[etag+"ZIMPRAW"] = np.array([])

    if aa.size > 0 and aa.size == zz.size:
        SD[etag+"AIMPRAW"] = np.hstack((SD[etag+"AIMPRAW"],aa))
        SD[etag+"ZIMPRAW"] = np.hstack((SD[etag+"ZIMPRAW"],zz))

    return SD


def transfer_general_data(procdata,newstruct=None):
    """
    JET-SPECIFIC FUNCTION
    Transfers data fields which require very basic processing directly
    from the implementation-specific object to the standardized object,
    ensuring that the standardized naming and format conventions are
    retained.

    :arg procdata: dict. Implementation-specific object containing processed extracted data.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :returns: dict. Standardized object with general metadata and source profiles transferred.
    """
    PD = None
    SD = None
    if isinstance(procdata,dict):
        PD = procdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()

    if PD is not None:

        # Transfer generic metadata
        SD["DEVICE"] = PD["DEVICE"] if "DEVICE" in PD else "JET"
        SD["INTERFACE"] = PD["INTERFACE"] if "INTERFACE" in PD else "UNKNOWN"
        SD["SHOT"] = int(PD["SHOT"]) if "SHOT" in PD else None
        SD["T1"] = float(PD["T1"]) if "T1" in PD else None
        SD["T2"] = float(PD["T2"]) if "T2" in PD else None
        SD["SHOTPHASE"] = int(PD["SHOTPHASE"]) if "SHOTPHASE" in PD else -1
        SD["EQLIST"] = copy.deepcopy(PD["EQLIST"]) if "EQLIST" in PD else []

        # Transfer 0D data
        SD["IP"] = float(PD["IP"]) if "IP" in PD else None
        SD["P_NBI"] = PD["NBI"] if "NBI" in PD else 0.0
        SD["P_ICRH"] = PD["ICRH"] if "ICRH" in PD else 0.0
        SD["P_ECRH"] = PD["ECRH"] if "ECRH" in PD else 0.0
        SD["P_LH"] = PD["LH"] if "LH" in PD else 0.0
        SD["P_OHMIC"] = PD["POHM"] if "POHM" in PD else 0.0
        SD["TWP"] = PD["TWP"] if "TWP" in PD else None
        SD["TWD"] = PD["TWD"] if "TWD" in PD else None
        SD["BVAC"] = PD["BVAC"] if "BVAC" in PD else None
        SD["RGEO"] = PD["RGEO"] if "RGEO" in PD else 1.7
        SD["RMAG"] = PD["RMAG"] if "RMAG" in PD else None
        SD["ZMAG"] = PD["ZMAG"] if "ZMAG" in PD else None
        SD["CRMAG"] = PD["CRMAG"] if "CRMAG" in PD else None

        # Transfer unified coordinate system data
        for key in PD:
            if re.search('^C[SCJE]_',key,flags=re.IGNORECASE):
                SD[key.upper()] = copy.deepcopy(PD[key])

        # Transfer neutral beam heating source data
#        csname = "CC_CPOLFLUXNO" if "CC_FLAG" in PD and PD["CC_FLAG"] else "CS_POLFLUXN"
#        qenbfunc = None
#        if "NBQE" in PD and PD["NBQE"] is not None and "PFNNBQE" in PD and PD["PFNNBQE"] is not None and PD["NBQE"].size == PD["PFNNBQE"].size:
#            qenbfunc = interp1d(PD["PFNNBQE"],PD["NBQE"],bounds_error=False,fill_value='extrapolate')
#        SD["QE_NBI"] = qenbfunc(PD[csname]) if qenbfunc is not None else np.zeros(PD[csname].shape)
#        qinbfunc = None
#        if "NBQI" in PD and PD["NBQI"] is not None and "PFNNBQI" in PD and PD["PFNNBQI"] is not None and PD["NBQI"].size == PD["PFNNBQI"].size:
#            qinbfunc = interp1d(PD["PFNNBQI"],PD["NBQI"],bounds_error=False,fill_value='extrapolate')
#        SD["QI1_NBI"] = qinbfunc(PD[csname]) if qinbfunc is not None else np.zeros(PD[csname].shape)
#        sinbfunc = None
#        if "NBSFI" in PD and PD["NBSFI"] is not None and "PFNNBSFI" in PD and PD["PFNNBSFI"] is not None and PD["NBSFI"].size == PD["PFNNBSFI"].size:
#            sinbfunc = interp1d(PD["PFNNBSFI"],PD["NBSFI"],bounds_error=False,fill_value='extrapolate')
#        SD["SI1_NBI"] = sinbfunc(PD[csname]) if sinbfunc is not None else np.zeros(PD[csname].shape)
##        SD["ANBI"]
##        SD["ZNBI"]
#        finbfunc = None
#        if "NFI" in PD and PD["NFI"] is not None and "PFNNFI" in PD and PD["PFNNFI"] is not None and PD["NFI"].size == PD["PFNNFI"].size:
#            finbfunc = interp1d(PD["PFNNFI"],PD["NFI"],bounds_error=False,fill_value='extrapolate')
#        SD["FI1_NBI"] = finbfunc(PD[csname]) if finbfunc is not None else np.zeros(PD[csname].shape)

        # Transfer ion cyclotron direct heating source data
#        qeicfunc = None
#        if "PDE" in PD and PD["PDE"] is not None and "PPDE" in PD and PD["PPDE"] is not None and PD["PDE"].size == PD["PPDE"].size:
#            qeicfunc = interp1d(PD["PPDE"],PD["PDE"],bounds_error=False,fill_value='extrapolate')
#        SD["QE_ICRH"] = qeicfunc(PD[csname]) if qeicfunc is not None else np.zeros(PD[csname].shape)
#        qiicfunc1 = None
#        if "PDI1" in PD and PD["PDI1"] is not None and "PPDI1" in PD and PD["PPDI1"] is not None and PD["PDI1"].size == PD["PPDI1"].size:
#            qiicfunc1 = interp1d(PD["PPDI1"],PD["PDI1"],bounds_error=False,fill_value='extrapolate')
#        SD["QI1_ICRH"] = qiicfunc1(PD[csname]) if qiicfunc1 is not None else np.zeros(PD[csname].shape)
#        qiicfunc2 = None
#        if "PDI2" in PD and PD["PDI2"] is not None and "PPDI2" in PD and PD["PPDI2"] is not None and PD["PDI2"].size == PD["PPDI2"].size:
#            qiicfunc2 = interp1d(PD["PPDI2"],PD["PDI2"],bounds_error=False,fill_value='extrapolate')
#        SD["QI2_ICRH"] = qiicfunc2(PD[csname]) if qiicfunc2 is not None else np.zeros(PD[csname].shape)
#        qiicfuncimp = None
#        if "PDIMP" in PD and PD["PDIMP"] is not None and "PPDIMP" in PD and PD["PPDIMP"] is not None and PD["PDIMP"].size == PD["PPDIMP"].size:
#            qiicfuncimp = interp1d(PD["PPDIMP"],PD["PDIMP"],bounds_error=False,fill_value='extrapolate')
#        SD["QIMP_ICRH"] = qiicfuncimp(PD[csname]) if qiicfuncimp is not None else np.zeros(PD[csname].shape)
##        SD["AICRH"]
##        SD["ZICRH"]

        # Transfer ion cyclotron collisional heating source data
#        qecofunc = None
#        if "PDCE" in PD and PD["PDCE"] is not None and "PPDCE" in PD and PD["PPDCE"] is not None and PD["PDCE"].size == PD["PPDCE"].size:
#            qecofunc = interp1d(PD["PPDCE"],PD["PDCE"],bounds_error=False,fill_value='extrapolate')
#        SD["QE_COL"] = qecofunc(PD[csname]) if qecofunc is not None else np.zeros(PD[csname].shape)
#        qicofunc = None
#        if "PDCI" in PD and PD["PDCI"] is not None and "PPDCI" in PD and PD["PPDCI"] is not None and PD["PDCI"].size == PD["PPDCI"].size:
#            qicofunc = interp1d(PD["PPDCI"],PD["PDCI"],bounds_error=False,fill_value='extrapolate')
#        SD["QI1_COL"] = qicofunc(PD[csname]) if qicofunc is not None else np.zeros(PD[csname].shape)

        # Calculate ohmic heating source profile
#        jsurffunc = None
#        if "JSURF" in PD and PD["JSURF"] is not None and "PJSURF" in PD and PD["PJSURF"] is not None and PD["JSURF"].size == PD["PJSURF"].size:
#            jsurffunc = interp1d(PD["PJSURF"],PD["JSURF"],bounds_error=False,fill_value='extrapolate')
#        jsurfvec = jsurffunc(PD[csname]) if jsurffunc is not None else np.zeros(PD[csname].shape)
#        vloopfunc = None
#        if "VLOOP" in PD and PD["VLOOP"] is not None and "PVLOOP" in PD and PD["PVLOOP"] is not None and PD["VLOOP"].size == PD["PVLOOP"].size:
#            vloopfunc = interp1d(PD["PVLOOP"],PD["VLOOP"],bounds_error=False,fill_value='extrapolate')
#        vloopvec = vloopfunc(PD[csname]) if vloopfunc is not None else np.zeros(PD[csname].shape)
#        SD["Q_OHMIC"] = jsurfvec * vloopvec

        # Calculate magnetic field vector, takes advantage of expected 1/R relation in interpolation
#        btfunc = None
#        if "BTAX" in PD and PD["BTAX"] is not None and "RBT" in PD and PD["RBT"] is not None:
#            invbt = 1.0 / PD["BTAX"]
#            btfunc = interp1d(PD["RBT"],invbt,bounds_error=False,fill_value='extrapolate')
#        SD["BT"] = 1.0 / btfunc(PD["CS_RMAJORO"]) if btfunc is not None else None

        # Select lowest of two Z-effective measurements as standard flat Z-effective estimate
        SD["FZEFF"] = 10.0
        if "ZEFV" in PD and PD["ZEFV"] is not None and PD["ZEFV"] < SD["FZEFF"]:
            SD["FZEFF"] = PD["ZEFV"]
            SD["FZEFFEB"] = PD["ZEFVEB"] if "ZEFVEB" in PD and PD["ZEFVEB"] is not None else 0.1 * SD["FZEFF"]
        if "ZEFH" in PD and PD["ZEFH"] is not None and PD["ZEFH"] < SD["FZEFF"]:
            SD["FZEFF"] = PD["ZEFH"]
            SD["FZEFFEB"] = PD["ZEFHEB"] if "ZEFHEB" in PD and PD["ZEFHEB"] is not None else 0.1 * SD["FZEFF"]
        if SD["FZEFF"] < 1.0:
            SD["FZEFF"] = 1.0
        SD["FZEFFFLAG"] = True

        # Transfer useful miscellaneous quantities
        SD["EQSOURCE"] = PD["EQLIST"][0] if "EQLIST" in PD and PD["EQLIST"] else "UNKNOWN"
        SD["AI1"] = 2.0   # Assumed deuterium plasma
        SD["ZI1"] = 1.0   # Assumed deuterium plasma
#        SD["AIMP"] = PD["AIMP"] if "AIMP" in PD else -1.0
#        SD["ZIMP"] = PD["ZIMP"] if "ZIMP" in PD else 0.0
        SD["WALLMAT"] = PD["WALLMAT"] if "WALLMAT" in PD else "UNKNOWN"
#        SD["RHOEBFLAG"] = True if "RHOEBFLAG" in PD and PD["RHOEBFLAG"] else False
        SD["POLFLAG"] = True if "POLFLAG" in PD and PD["POLFLAG"] else False
        SD["MIXDIAGFLAG"] = True if "MIXDIAGFLAG" in PD and PD["MIXDIAGFLAG"] else False
        SD["RAWDIAGFLAG"] = True if "RAWDIAGFLAG" in PD and PD["RAWDIAGFLAG"] else False
        SD["RAWEDGEFLAG"] = True if "RAWEDGEFLAG" in PD and PD["RAWEDGEFLAG"] else False

    return SD


def filter_ne(diagdata,newstruct=None,edgeflag=False,hfsflag=False,ftflag=True):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed electron density profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed electron density profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ftflag: bool. Flag to specify that input profile belongs to a time window within the flat-top phase.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "NE"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Region-specific filters
        if edgeflag:
            status = 0       # Dummy operation just to conserve structure
        else:
            # NE data in core can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                cfilt = (np.abs(darr[0,:]) <= 0.7)
                vallim = 0.7 * np.mean(darr[2,:][cfilt]) if np.any(cfilt) else 0.0
                rfilt = (np.abs(darr[0,:]) <= 0.7)
                lfilt = (darr[2,:] < vallim)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_te(diagdata,newstruct=None,edgeflag=False,hfsflag=False,ftflag=True):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed electron temperature profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed electron temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ftflag: bool. Flag to specify that input profile belongs to a time window within the flat-top phase.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "TE"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Region-specific filters
        if edgeflag:
            # TE data at edge can be corrupted with large values in automated pre-processing routines
            #     Technically does nothing since there are not edge-specific TE diagnostics at JET
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) >= 0.9)
                ufilt = (darr[2,:] > 300)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,rfilt],operation='and',finvert=True)

        else:
            # TE data in core can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                cfilt = (np.abs(darr[0,:]) <= 0.7)
                vallim = 0.5 * np.mean(darr[2,:][cfilt]) if np.any(cfilt) else 0.0
                rfilt = (np.abs(darr[0,:]) <= 0.7)
                lfilt = (darr[2,:] < vallim)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

            # TE data at edge can be corrupted with large values in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) >= 0.9)
                ufilt = (darr[2,:] > 300)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,rfilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_ti(diagdata,newstruct=None,edgeflag=False,hfsflag=False):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed ion temperature profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed ion temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "TI"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # TI data can be corrupted with extremely high values
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.0e7,upper_equality=False,indices=[2])

        if edgeflag:
            # Edge TI error suspected to be underestimated, especially away from the edge
            if darr.size > 0:
                rfilt = (darr[3,:] < 0.2 * darr[2,:])
                if np.any(rfilt):
                    darr[3,:][rfilt] = darr[3,:][rfilt] + np.abs(0.975 - darr[0,:][rfilt]) * 0.5 * darr[2,:][rfilt]
        else:
            # Core TI error known to be underestimated, especially closer to the edge
            if darr.size > 0:
                rfilt = (darr[3,:] < 0.1 * darr[2,:])
                if np.any(rfilt):
                    darr[3,:][rfilt] = darr[3,:][rfilt] + np.abs(darr[0,:][rfilt] - 0.175) * 0.25 * darr[2,:][rfilt]

            # Core TI data can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 1.05)
                lfilt = (darr[2,:] < 50)
                darr = ptools.custom_filter_2d(darr,filters=[lfilt,rfilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_vt(diagdata,newstruct=None,edgeflag=False,hfsflag=False):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed toroidal flow velocity profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed toroidal flow velocity profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "VT"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        if edgeflag:
            # Edge VT data can be very noisy, filter limits values to reasonable range
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) >= 0.75)
                ufilt = (np.abs(darr[2,:]) > 1.0e4)
                lfilt = (np.abs(darr[2,:]) < 1.0e5)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,lfilt,rfilt],operation='and')
        else:
            # Core VT data can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 0.8)
                lfilt = (np.abs(darr[2,:]) < 1.0e3)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,lfilt,rfilt],operation='and',finvert=True)

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_af(diagdata,newstruct=None,edgeflag=False,hfsflag=False):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed rotational angular frequency profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed rotational angular frequency profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "AF"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Artificially augment errors that seem unreasonably small
        if darr.size > 0:
            rfilt = (darr[3,:] < 0.05 * np.abs(darr[2,:]))
            if np.any(rfilt):
                darr[3,:][rfilt] = darr[3,:][rfilt] + 0.05 * np.abs(darr[2,:][rfilt])

        if edgeflag:
            # Edge AF data away from the edge tend to be untrustworthy
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,lower_bound=0.75,lower_equality=True,indices=[0])
        else:
            status = 0       # Dummy operation just to conserve structure

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_nimp(diagdata,newstruct=None,edgeflag=False,hfsflag=False):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed impurity ion density profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed impurity ion density profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "NIMP"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)
    (aimp,zimp) = get_standardized_impurity_data(DD)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        if edgeflag:
            #
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,lower_bound=0.8,upper_bound=1.0,lower_equality=False,upper_equality=True,indices=[0])

            #
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr,lower_bound=5.0e16,lower_equality=True,indices=[2])
        else:
            #
            if darr.size > 0:
                rfilt = (np.abs(darr[0,:]) <= 0.8)
                ufilt = (darr[2,:] < 2.0e17)
                darr = ptools.custom_filter_2d(darr,filters=[ufilt,rfilt],operation='and',finvert=True)

        # Error adjustment - too small error bars yields unreasonable profiles or causes numerical instability in GP fit
        if darr.size > 0:
            lfilt = (darr[3,:] < 1.0e16)
            if np.any(lfilt):
                darr[3,:][lfilt] = darr[3,:][lfilt] + 1.0e16

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.03,upper_equality=False,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)
            SD = add_filtered_impurity_data(newstruct=SD,adata=aimp,zdata=zimp,xdata=rho,edgeflag=edgeflag)

        status = 2 if edgeflag else 1

    return (SD,status)


def filter_q(diagdata,newstruct=None,conflag=False,hfsflag=False):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed safety factor profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg conflag: bool. Flag to specify that input profile comes from a constrained equilibrium calculation.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    dtag = "Q"
    status = 0

    (rho,rhoeb,val,valeb) = get_standardized_profile_data(DD,dtag,hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho,rhoeb,val,valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr,indices=[0,2])

        # Addition of heuristic Q error if it came from a constrained equilibrium or is unreasonably small
        if darr.size > 0:
            if conflag or np.average(darr[3,:]) <= 0.05:
                darr[3,:] = darr[3,:] + 0.05

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr,upper_bound=1.00,upper_equality=True,indices=[0])

        if darr.size > 0:
            rho = darr[0,:].flatten()
            rhoeb = darr[1,:].flatten()
            val = darr[2,:].flatten()
            valeb = darr[3,:].flatten()
            SD = add_filtered_profile_data(dtag,newstruct=SD,xval=rho,xerr=rhoeb,yval=val,yerr=valeb)

        status = 1

    return (SD,status)


def select_standardized_filter(diagdata,quantity,newstruct=None):
    """
    AUG-SPECIFIC FUNCTION
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    


def filter_standardized_diagnostic_data(diagdata,newstruct=None):
    """
    JET-SPECIFIC FUNCTION
    Applies pre-defined filters to the raw diagnostic data, optimized
    such to improve the quality of the fitting routine. Deletes the
    raw diagnostic data after filtering and standardizing, in order to
    reduce clutter in the final data object, but can be kept by
    providing the RAWDIAGFLAG field in the input object.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    DD = None
    SD = None
    if isinstance(diagdata,dict):
        DD = diagdata
    if isinstance(newstruct,dict):
        SD = newstruct
    else:
        SD = dict()
    fe = 0

    SD["NEDIAG"] = None
    SD["TEDIAG"] = None
    SD["TIDIAG"] = None
    SD["AFDIAG"] = None
#    SD["VTDIAG"] = None
    SD["NIMPDIAG"] = None
    SD["ETIDIAG"] = None
    SD["EAFDIAG"] = None
#    SD["EVTDIAG"] = None
    SD["ENIMPDIAG"] = None
    SD["QDIAG"] = None

    if DD is not None:
        SD["CS_DIAG"] = "RHOPOLN" if DD["POLFLAG"] else "RHOTORN"
        if "MIXDIAGFLAG" not in SD:
            SD["MIXDIAGFLAG"] = True if "MIXDIAGFLAG" in DD and DD["MIXDIAGFLAG"] else False
        ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

        diag = "HRTS"
        if diag in DD and DD[diag] is not None and (SD["MIXDIAGFLAG"] or fe == 0):
            (SD,nstatus) = filter_ne(DD[diag],newstruct=SD,ftflag=ftflag)
            if nstatus > 0 and SD["NERAW"].size > 0:
                if SD["NEDIAG"] is None:
                    SD["NEDIAG"] = []
                SD["NEDIAG"].append(diag)
            (SD,tstatus) = filter_te(DD[diag],newstruct=SD,ftflag=ftflag)
            if tstatus > 0 and SD["TERAW"].size > 0 and np.mean(SD["TERAW"]) > 100:
                if SD["TEDIAG"] is None:
                    SD["TEDIAG"] = []
                SD["TEDIAG"].append(diag)
            if nstatus > 0 and tstatus > 0 and SD["NERAW"].size > 0 and SD["TERAW"].size > 0 and np.mean(SD["TERAW"]) > 100:
                fe = 1

        diag = "LIDR"
        if diag in DD and DD[diag] is not None and (SD["MIXDIAGFLAG"] or fe == 0):
            (SD,nstatus) = filter_ne(DD[diag],newstruct=SD,hfsflag=True,ftflag=ftflag)
            if nstatus != 0:
                if SD["NEDIAG"] is None:
                    SD["NEDIAG"] = []
                SD["NEDIAG"].append(diag)
            (SD,tstatus) = filter_te(DD[diag],newstruct=SD,hfsflag=True,ftflag=ftflag)
            if tstatus != 0:
                if SD["TEDIAG"] is None:
                    SD["TEDIAG"] = []
                SD["TEDIAG"].append(diag)
            if nstatus != 0 and tstatus != 0:
                fe = 1

        diag = "ECM"
        if diag in DD and DD[diag] is not None and (SD["MIXDIAGFLAG"] or fe == 0):
            (SD,status) = filter_te(DD[diag],newstruct=SD,ftflag=ftflag)
            if status != 0:
                if SD["TEDIAG"] is None:
                    SD["TEDIAG"] = []
                SD["TEDIAG"].append(diag)
                fe = 1

        diag = "CX"
        if diag in DD and DD[diag] is not None:
            for key in DD[diag]:
                if re.match('^C[MP]Z$',key,flags=re.IGNORECASE):
                    if "TI" in DD[diag][key]:
                        (SD,status) = filter_ti(DD[diag][key]["TI"],newstruct=SD,edgeflag=True)
                        if status != 0:
                            if SD["ETIDIAG"] is None:
                                SD["ETIDIAG"] = []
                            SD["ETIDIAG"].append(key.upper())
                    if "AF" in DD[diag][key]:
                        (SD,status) = filter_af(DD[diag][key]["AF"],newstruct=SD,edgeflag=True)
                        if status != 0:
                            if SD["EAFDIAG"] is None:
                                SD["EAFDIAG"] = []
                            SD["EAFDIAG"].append(key.upper())
                    if "NIMP" in DD[diag][key]:
                        (SD,status) = filter_nimp(DD[diag][key]["NIMP"],newstruct=SD,edgeflag=True)
                        if status != 0:
                            if SD["ENIMPDIAG"] is None:
                                SD["ENIMPDIAG"] = []
                            SD["ENIMPDIAG"].append(key.upper())
                else:
                    if "TI" in DD[diag][key]:
                        if SD["TIDIAG"] is None:
                            SD["TIDIAG"] = []
                        (SD,status) = filter_ti(DD[diag][key]["TI"],newstruct=SD)
                        if status != 0:
                            SD["TIDIAG"].append(key.upper())
                    if "AF" in DD[diag][key]:
                        if SD["AFDIAG"] is None:
                            SD["AFDIAG"] = []
                        (SD,status) = filter_af(DD[diag][key]["AF"],newstruct=SD)
                        if status != 0:
                            SD["AFDIAG"].append(key.upper())
                    if "NIMP" in DD[diag][key]:
                        if SD["NIMPDIAG"] is None:
                            SD["NIMPDIAG"] = []
                        (SD,status) = filter_nimp(DD[diag][key]["NIMP"],newstruct=SD)
                        if status != 0:
                            SD["NIMPDIAG"].append(key.upper())

        diag = "MAGN"
        if diag in DD and DD[diag] is not None:
#            cflag = True if not re.match(r'^EFIT$',DD[diag]["DDA"],flags=re.IGNORECASE) else False
            cflag = False
            if "Q" in DD[diag]:
                (SD,status) = filter_q(DD[diag],newstruct=SD,conflag=cflag)
                if status != 0:
                    if SD["QDIAG"] is None:
                        SD["QDIAG"] = []
                    SD["QDIAG"].append(DD[diag]["QSF"].upper())

        if not ("RAWDIAGFLAG" in DD and DD["RAWDIAGFLAG"]):
            dlist = ["HRTS","LIDR","ECM","CX","MAGN"]
            SD = ptools.delete_fields_from_dict(SD,fields=dlist)

    return SD


def combine_edge_diagnostic_data(stddata):
    """
    JET-SPECIFIC FUNCTION
    Combines any edge and core diagnostic profile data of the same quantity,
    applying any additional checks and cross-diagnostic filters. Deletes
    edge diagnostic profiles after merging in order to reduce clutter in
    the final data object, but can be kept by providing the RAWEDGEFLAG
    field in the input object.

    :arg stddata: dict. Standardized object containing separated core and edge profiles.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    SD = None
    if isinstance(stddata,dict):
        SD = stddata

    if SD is not None:

        if "ETIRAW" in SD and SD["ETIRAW"].size > 0:
            if "TIRAW" not in SD:
                SD["TIRAW"] = np.array([])
                SD["TIRAWEB"] = np.array([])
                SD["TIRAWX"] = np.array([])
                SD["TIRAWXEB"] = np.array([])
                SD["TIDMAP"] = np.array([])
                SD["TIDIAG"] = []
            SD["TIRAW"] = np.hstack((SD["TIRAW"],SD["ETIRAW"]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"],SD["ETIRAWEB"]))
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"],SD["ETIRAWX"]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"],SD["ETIRAWXEB"]))
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"],SD["ETIDMAP"] + len(SD["TIDIAG"])))
            SD["TIDIAG"].extend(SD["ETIDIAG"])

        if "EVTRAW" in SD and SD["EVTRAW"].size > 0 and "VTRAW" in SD:
            # Only include edge toroidal velocity measurements if they are not excessively large
            if "VTRAW" in SD and SD["VTRAW"].size > 0 and np.abs(np.mean(SD["VTRAW"])) > np.abs(np.mean(SD["EVTRAW"])):
                SD["VTRAW"] = np.hstack((SD["VTRAW"],SD["EVTRAW"]))
                SD["VTRAWEB"] = np.hstack((SD["VTRAWEB"],SD["EVTRAWEB"]))
                SD["VTRAWX"] = np.hstack((SD["VTRAWX"],SD["EVTRAWX"]))
                SD["VTRAWXEB"] = np.hstack((SD["VTRAWXEB"],SD["EVTRAWXEB"]))
                SD["VTDMAP"] = np.hstack((SD["VTDMAP"],SD["EVTDMAP"] + len(SD["VTDIAG"])))
                SD["VTDIAG"].extend(SD["EVTDIAG"])

        if "EAFRAW" in SD and SD["EAFRAW"].size > 0 and "AFRAW" in SD:
            # Only include edge angular frequency measurements if they are not excessively large
            if SD["AFRAW"].size > 0 and np.sqrt(np.mean(np.power(SD["AFRAW"],2.0))) > np.sqrt(np.mean(np.power(SD["EAFRAW"],2.0))):
                SD["AFRAW"] = np.hstack((SD["AFRAW"],SD["EAFRAW"]))
                SD["AFRAWEB"] = np.hstack((SD["AFRAWEB"],SD["EAFRAWEB"]))
                SD["AFRAWX"] = np.hstack((SD["AFRAWX"],SD["EAFRAWX"]))
                SD["AFRAWXEB"] = np.hstack((SD["AFRAWXEB"],SD["EAFRAWXEB"]))
                SD["AFDMAP"] = np.hstack((SD["AFDMAP"],SD["EAFDMAP"] + len(SD["AFDIAG"])))
                SD["AFDIAG"].extend(SD["EAFDIAG"])

        if "ENIMPRAW" in SD and SD["ENIMPRAW"].size > 0:
            # Core charge exchange measurements usually polluted with bad impurity measurements, edge is cleaner
            if "NIMPRAW" in SD and SD["NIMPRAW"].size > 0:
                cfilt = (SD["NIMPRAW"] >= 0.5 * np.mean(SD["ENIMPRAW"]))
                SD["NIMPRAW"] = SD["NIMPRAW"][cfilt] if np.any(cfilt) else np.array([])
                SD["NIMPRAWEB"] = SD["NIMPRAWEB"][cfilt] if np.any(cfilt) else np.array([])
                SD["NIMPRAWX"] = SD["NIMPRAWX"][cfilt] if np.any(cfilt) else np.array([])
                SD["NIMPRAWXEB"] = SD["NIMPRAWXEB"][cfilt] if np.any(cfilt) else np.array([])
                SD["NIMPDMAP"] = SD["NIMPDMAP"][cfilt] if np.any(cfilt) else np.array([])
            else:
                SD["NIMPRAW"] = np.array([])
                SD["NIMPRAWEB"] = np.array([])
                SD["NIMPRAWX"] = np.array([])
                SD["NIMPRAWXEB"] = np.array([])
                SD["NIMPDMAP"] = np.array([])
                SD["AIMPRAW"] = np.array([])
                SD["ZIMPRAW"] = np.array([])

            SD["NIMPRAW"] = np.hstack((SD["NIMPRAW"],SD["ENIMPRAW"]))
            SD["NIMPRAWEB"] = np.hstack((SD["NIMPRAWEB"],SD["ENIMPRAWEB"]))
            SD["NIMPRAWX"] = np.hstack((SD["NIMPRAWX"],SD["ENIMPRAWX"]))
            SD["NIMPRAWXEB"] = np.hstack((SD["NIMPRAWXEB"],SD["ENIMPRAWXEB"]))
            SD["NIMPDMAP"] = np.hstack((SD["NIMPDMAP"],SD["ENIMPDMAP"] + len(SD["NIMPDIAG"])))
            SD["NIMPDIAG"].extend(SD["ENIMPDIAG"])
            SD["AIMPRAW"] = np.hstack((SD["AIMPRAW"],SD["EAIMPRAW"]))
            SD["ZIMPRAW"] = np.hstack((SD["ZIMPRAW"],SD["EZIMPRAW"]))

        if not ("RAWEDGEFLAG" in SD and SD["RAWEDGEFLAG"]):
            dlist = ["ETIRAW","ETIRAWEB","ETIRAWX","ETIRAWXEB","ETIDMAP", \
                     "EVTRAW","EVTRAWEB","EVTRAWX","EVTRAWXEB","EVTDMAP", \
                     "EAFRAW","EAFRAWEB","EAFRAWX","EAFRAWXEB","EAFDMAP", \
                     "ENIMPRAW","ENIMPRAWEB","ENIMPRAWX","ENIMPRAWXEB","ENIMPDMAP", \
                     "EAIMPRAW","EZIMPRAW"]
            SD = ptools.delete_fields_from_dict(SD,fields=dlist)

    return SD


def add_profile_boundary_data(stddata):
    """
    JET-SPECIFIC FUNCTION
    Inserts appropriate boundary constraints on the various profiles
    at the separatrix in order improve the robustness of the GP fit
    routine and to achieve more meaningful fits.

    :arg stddata: dict. Standardized object containing the processed profiles.

    :returns: dict. Object with identical structure as input object except with profile boundary constraints added to standard fields.
    """
    SD = None
    if isinstance(stddata,dict):
        SD = stddata

    ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

    ###  The following section adds profile boundary constraints for more robust GP fits
    if SD is not None:
#        if "NERAW" in SD and SD["NERAW"].size > 0 and np.nanmin(np.abs(SD["NERAWX"] - 1.0)) > 0.05:
#            idx = np.where((1.0 - SD["NERAWX"]) > 0.0)[0][-1]
#            eneval = 0.01 / (1.0 - SD["NERAWX"][idx]) * SD["NERAW"][idx]
#            SD["NERAWX"] = np.hstack((SD["NERAWX"],0.99))
#            SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"],0.0))
#            SD["NERAW"] = np.hstack((SD["NERAW"],eneval))
#            SD["NERAWEB"] = np.hstack((SD["NERAWEB"],0.3 * eneval))

        # 3-point NE boundary constraint based on data near rho=1.0 yields most robust results for both L- and H-mode time windows
        if "NERAW" in SD and SD["NERAW"].size > 0:
            efilt = np.all([SD["NERAWX"] >= 0.9,SD["NERAWX"] <= 1.0],axis=0)
            idx = np.where(SD["NERAWX"][efilt] >= np.nanmax(SD["NERAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgene = 0.8 * SD["NERAW"][efilt][idx] if idx is not None and SD["NERAW"][efilt][idx] < 2.0e-19 else 3.0e18
            edgeneeb = 2.0e18
#            xfilt = (SD["NERAWX"] < 1.03)
            SD["NERAWX"] = np.hstack((SD["NERAWX"],[0.98,1.00,1.02]))
            SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"],[0.0,0.0,0.0]))
            SD["NERAW"] = np.hstack((SD["NERAW"],[edgene,edgene,edgene]))
            SD["NERAWEB"] = np.hstack((SD["NERAWEB"],[5.0*edgeneeb,3.0*edgeneeb,1.0*edgeneeb]))
            SD["NEDMAP"] = np.hstack((SD["NEDMAP"],[len(SD["NEDIAG"])] * 3))
            SD["NEDIAG"].append("NEBC")

        # Pasting of TE edge data as TI profile constraint yields better resolution of TI pedestal
        #     Shown to improve boundary condition of integrated modelling
        if "TERAW" in SD and SD["TERAW"].size > 0 and "TIRAW" in SD and SD["TIRAW"].size > 0:
            efilt = np.all([SD["TERAWX"] >= 0.9,SD["TERAWX"] <= 1.0],axis=0)
            nume = np.count_nonzero(efilt)
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"],SD["TERAWX"][efilt]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"],SD["TERAWXEB"][efilt]))
            SD["TIRAW"] = np.hstack((SD["TIRAW"],SD["TERAW"][efilt]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"],SD["TERAWEB"][efilt]))
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"],[len(SD["TIDIAG"])] * nume))
            SD["TIDIAG"].append("TIBC")

        # 3-point TE boundary constraint based on data near rho=1.0 yields most robust results for L- and H-mode time windows
        if "TERAW" in SD and SD["TERAW"].size > 0:
            efilt = np.all([SD["TERAWX"] >= 0.9,SD["TERAWX"] <= 1.0],axis=0)
            idx = np.where(SD["TERAWX"][efilt] >= np.nanmax(SD["TERAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgete = 0.8 * SD["TERAW"][efilt][idx] if idx is not None else 100
            edgeteeb = 70
#            xfilt = (SD["RHOTERAW"] < 1.03)
            SD["TERAWX"] = np.hstack((SD["TERAWX"],[0.98,1.00,1.02]))
            SD["TERAWXEB"] = np.hstack((SD["TERAWXEB"],[0.0,0.0,0.0]))
            SD["TERAW"] = np.hstack((SD["TERAW"],[edgete,edgete,edgete]))
            SD["TERAWEB"] = np.hstack((SD["TERAWEB"],[2.0*edgeteeb,1.5*edgeteeb,1.0*edgeteeb]))
            SD["TEDMAP"] = np.hstack((SD["TEDMAP"],[len(SD["TEDIAG"])] * 3))
            SD["TEDIAG"].append("TEBC")

        # 3-point TI boundary constraint based on data near rho=1.0 yields most robust results for L- and H-mode time windows
        #     Is this even necessary if the TE boundary constraints have already been pasted in?
        if "TIRAW" in SD and SD["TIRAW"].size > 0:
            efilt = np.all([SD["TIRAWX"] >= 0.9,SD["TIRAWX"] <= 1.0],axis=0)
            idx = np.where(SD["TIRAWX"][efilt] >= np.nanmax(SD["TIRAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgeti = 0.8 * SD["TIRAW"][efilt][idx] if idx is not None else 100
            edgetieb = 70
#            xfilt = (SD["RHOTIRAW"] < 1.03)
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"],[0.98,1.00,1.02]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"],[0.0,0.0,0.0]))
            SD["TIRAW"] = np.hstack((SD["TIRAW"],[edgeti,edgeti,edgeti]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"],[1.5*edgetieb,1.25*edgetieb,1.0*edgetieb]))
            bcidx = len(SD["TIDIAG"])
            for ii in np.arange(0,len(SD["TIDIAG"])):
                if re.match('^TIBC$',SD["TIDIAG"][ii]):
                    bcidx = ii
            if bcidx >= len(SD["TIDIAG"]):
                SD["TIDIAG"].append("TIBC")
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"],[bcidx,bcidx,bcidx]))

        # Single point VT boundary constraint provides sufficient robustness (not really tested as we use AF)
        if "VTRAW" in SD and SD["VTRAW"].size > 0:
#            xfilt = (SD["RHOVTRAW"] < 1.03)
            SD["VTRAWX"] = np.hstack((SD["VTRAWX"],1.02))
            SD["VTRAWXEB"] = np.hstack((SD["VTRAWXEB"],0.0))
            SD["VTRAW"] = np.hstack((SD["VTRAW"],0))
            SD["VTRAWEB"] = np.hstack((SD["VTRAWEB"],2000))
            SD["VTDMAP"] = np.hstack((SD["VTDMAP"],[len(SD["VTDIAG"])]))
            SD["VTDIAG"].append("VTBC")

        # Single point AF boundary constraint based on edge data yields most robust results over many plasma regimes
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
            zfilt = np.all([SD["AFRAW"] == 0.0,SD["AFRAWEB"] <= 0.0],axis=0)
            SD["AFRAWEB"][zfilt] = 500
            edgeaf = 0
            edgeafeb = 2000
            efilt = (SD["AFRAWX"] >= 0.9)
            cfilt = np.all([SD["AFRAWX"] > 0.0,SD["AFRAWX"] <= 0.5],axis=0)
            if np.any(efilt) and np.any(cfilt):
                afe = np.mean(SD["AFRAW"][efilt])
                afc = np.mean(SD["AFRAW"][cfilt])
                if (afe * afc) < 0.0:
                    rhoe = np.mean(SD["AFRAWX"][efilt])
                    rhoc = np.mean(SD["AFRAWX"][cfilt])
                    edgeaf = (1.02 - rhoe) * (afc - afe) / (rhoc - rhoe) + afe
                    edgeafeb = 5000
#            xfilt = (SD["AFRAWX"] < 1.03)
            SD["AFRAWX"] = np.hstack((SD["AFRAWX"],1.02))
            SD["AFRAWXEB"] = np.hstack((SD["AFRAWXEB"],0.0))
            SD["AFRAW"] = np.hstack((SD["AFRAW"],edgeaf))
            SD["AFRAWEB"] = np.hstack((SD["AFRAWEB"],edgeafeb))
            SD["AFDMAP"] = np.hstack((SD["AFDMAP"],[len(SD["AFDIAG"])]))
            SD["AFDIAG"].append("AFBC")

        # Single point NIMP boundary constraint provides sufficient robustness
        if "NIMPRAW" in SD and SD["NIMPRAW"].size > 0:
#            xfilt = (SD["RHONIMPRAW"] < 1.03)
            SD["NIMPRAWX"] = np.hstack((SD["NIMPRAWX"],1.02))
            SD["NIMPRAWXEB"] = np.hstack((SD["NIMPRAWXEB"],0.0))
            SD["NIMPRAW"] = np.hstack((SD["NIMPRAW"],1.0e16))
            SD["NIMPRAWEB"] = np.hstack((2.0*SD["NIMPRAWEB"],4.0e15))
            SD["NIMPDMAP"] = np.hstack((SD["NIMPDMAP"],[len(SD["NIMPDIAG"])]))
            SD["NIMPDIAG"].append("NXBC")

        # Single point Q boundary constraint based on edge data provides sufficient robustness, max. value of 4
        if "QRAW" in SD and SD["QRAW"].size > 0:
            bfilt = np.all([SD["QRAWX"] >= 0.01,SD["QRAWX"] <= 0.99],axis=0)
            edgeq = np.nanmax((4.0,1.2 * np.nanmax(SD["QRAW"][bfilt])))
            edgeqeb = 0.5
#            xfilt = (SD["RHOQRAW"] <= 1.0)
            SD["QRAWX"] = np.hstack((SD["QRAWX"],1.02))
            SD["QRAWXEB"] = np.hstack((SD["QRAWXEB"],0.0))
            SD["QRAW"] = np.hstack((SD["QRAW"],edgeq))
            SD["QRAWEB"] = np.hstack((SD["QRAWEB"],edgeqeb))
            SD["QDMAP"] = np.hstack((SD["QDMAP"],np.nanmin(SD["QDMAP"])))

    return SD


def standardize_experimental_data(procdata,extra_info=None,pickle_dir=None):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    Main call function to convert processed data structure output from
    process_jet.py into a more generalized format for profile fitting.

    The output container should be in the same generalized format and
    structure as other implementations, minus the kernel selection and
    fit settings for the GPR1D profile fitting tool. The concept is
    that any user-defined scripts defined strictly on the data
    structure here should be portable to any other implementation.

    :arg procdata: dict. Implementation-specific object containing processed data.

    :kwarg extra_info: dict. Generic container for implementation-specific parameters and options.

    :kwarg pickle_dir: str. Directory in which to save the extracted and computed data into a Python pickle file, specifying this toggles saving.

    :returns: dict. Object identical in structure to input object, except containing standardized profile data for fitting.
    """
    PD = None
    SD = None
    infodict = dict()
    pdir = ''
    if isinstance(procdata,dict):
        PD = procdata
    elif isinstance(procdata,str) and os.path.isfile(procdata):
        PD = ptools.unpickle_this(procdata)
    else:
        raise TypeError('Invalid format for input processed data structure.')
    if isinstance(extra_info,dict):
        infodict = copy.deepcopy(extra_info)
    if pickle_dir and isinstance(pickle_dir,str):
        pdir = pickle_dir
        if not pdir.endswith('/'):
            pdir = pdir + '/'

    if PD is not None:
        PD["RHOEBFLAG"] = infodict["RHOEBFLAG"] if "RHOEBFLAG" in infodict else False
        PD["POLFLAG"] = infodict["POLFLAG"] if "POLFLAG" in infodict else False
        PD["MIXDIAGFLAG"] = infodict["MIXDIAGFLAG"] if "MIXDIAGFLAG" in infodict else False
        PD["RAWDIAGFLAG"] = infodict["RAWDIAGFLAG"] if "RAWDIAGFLAG" in infodict else False
        PD["RAWEDGEFLAG"] = infodict["RAWEDGEFLAG"] if "RAWEDGEFLAG" in infodict else False

        # Separates and formats the diagnostic data for filtering
        DD = standardize_diagnostic_data(PD,newstruct=None)

        # Transfer useful data to new structure, apply filters to remove nonsense data and create boundary constraints
        SD = transfer_general_data(PD,newstruct=None)
        SD = filter_standardized_diagnostic_data(DD,newstruct=SD)
        SD = combine_edge_diagnostic_data(SD)
        SD = add_profile_boundary_data(SD)

        SD["LAST_UPDATE"] = datetime.date

        # Save extracted shot data into a "pickle" file
        if pdir:
            if "TWNUM" in infodict and isinstance(infodict["TWNUM"],(int,float)) and int(infodict["TWNUM"]) > 0:
                twstr = "_T%02d" % (int(infodict["TWNUM"]))
            snstr = "%06d" % (int(SD["SHOT"]))
            stf = ptools.pickle_this(SD,'S'+snstr+twstr+'.p',pdir)
            if stf != 0:
                print("Error encountered in pickling process... recommend to do manually.")

    return SD
