# Script with functions to standardize extracted JET PPF data structure for final representation
# Developer: Aaron Ho - 10/07/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools
from EX2GK.tools.jet import proctools as jptools


def transfer_generic_data(procdata, newstruct=None, usepol=False, usexeb=False, useexp=True, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Transfers any crucial data fields which require little to no
    additional processing from the raw extracted data structure into
    a new processed data structure. This concept prevents the
    accumulation of unnecessary data in the final data structure.

    :arg procdata: dict. Implementation-specific object containing unified coordinate system and extraneous processed data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg usepol: bool. Flag to specify usage of rho poloidal as the raw data coordinate instead of rho toroidal.

    :kwarg usexeb: bool. Flag to specify inclusion of explicit x-errors from coordinate conversions in the raw data.

    :kwarg useexp: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with general metadata transferred.
    """
    PD = None
    SD = None
    if isinstance(procdata, dict):
        PD = procdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()

    if PD is not None:

        # Transfers generic metadata
        SD["DEVICE"] = PD["DEVICE"] if "DEVICE" in PD else "JET"
        SD["INTERFACE"] = PD["INTERFACE"] if "INTERFACE" in PD else "UNKNOWN"
        SD["SHOT"] = int(PD["SHOT"]) if "SHOT" in PD else None
        SD["T1"] = float(PD["T1"]) if "T1" in PD else None
        SD["T2"] = float(PD["T2"]) if "T2" in PD else None
        SD["SHOTPHASE"] = int(PD["SHOTPHASE"]) if "SHOTPHASE" in PD else -1
        SD["WINDOW"] = PD["WINDOW"] if "WINDOW" in PD else -1
        SD["ILWSTART"] = PD["ILWSTART"] if "ILWSTART" in PD and isinstance(PD["ILWSTART"], int) else 79854

        SD["HMODEFLAG"] = True if "HMODEFLAG" in PD and PD["HMODEFLAG"] else False
        SD["PELINJFLAG"] = True if "PELINJFLAG" in PD and PD["PELINJFLAG"] else False
        SD["USERZFLAG"] = True if "USERZFLAG" in PD and PD["USERZFLAG"] else False
        SD["ELMFILTFLAG"] = True if "ELMFILTFLAG" in PD and PD["ELMFILTFLAG"] else False
        SD["GPCOORDFLAG"] = True if "GPCOORDFLAG" in PD and PD["GPCOORDFLAG"] else False
        SD["RSHIFTFLAG"] = True if "RSHIFTFLAG" in PD and PD["RSHIFTFLAG"] else False
#        SD["PSICORRFLAG"] = True if "PSICORRFLAG" in PD and PD["PSICORRFLAG"] else False
        SD["USENSCALEFLAG"] = True if "USENSCALEFLAG" in PD and PD["USENSCALEFLAG"] else False
        SD["USELMODEFLAG"] = True if "USELMODEFLAG" in PD and PD["USELMODEFLAG"] else False
        SD["USENESMOOTHFLAG"] = True if "USENESMOOTHFLAG" in PD and PD["USENESMOOTHFLAG"] else False
        SD["USEECEFLAG"] = True if "USEECEFLAG" in PD and PD["USEECEFLAG"] else False
        SD["ABSECEFLAG"] = True if "ABSECEFLAG" in PD and PD["ABSECEFLAG"] else False
        SD["FORCEREFLFLAG"] = True if "FORCEREFLFLAG" in PD and PD["FORCEREFLFLAG"] else False
        SD["USEQEDGEFLAG"] = True if "USEQEDGEFLAG" in PD and PD["USEQEDGEFLAG"] else False
        SD["PURERAWFLAG"] = True if "PURERAWFLAG" in PD and PD["PURERAWFLAG"] else False
        SD["STDMEANFLAG"] = True if "STDMEANFLAG" in PD and PD["STDMEANFLAG"] else False
        SD["EXPFILTFLAG"] = True if useexp else False
        SD["WALLMAT"] = PD["WALLMAT"] if "WALLMAT" in PD and isinstance(PD["WALLMAT"], str) else "UNKNOWN"
        SD["CONTMAT"] = PD["CONTMAT"] if "CONTMAT" in PD and isinstance(PD["CONTMAT"], str) else "UNKNOWN"

        SD["POLFLAG"] = True if usepol else False
        SD["RHOEBFLAG"] = True if usexeb else False

        SD["EQLIST"] = PD["EQLIST"] if "EQLIST" in PD and isinstance(PD["EQLIST"], list) and len(PD["EQLIST"]) > 0 else ['efit']
        eqdda = SD["EQLIST"][0].upper() if len(SD["EQLIST"]) > 0 else None
        for key in PD:
            if re.match(r'^C?C[SJE]_.+$', key):
                SD[key] = copy.deepcopy(PD[key])

        # Required values with prescribed defaults
        SD["RMAG"] = PD["RMAG"] if "RMAG" in PD and PD["RMAG"] is not None else np.array([3.0])
        SD["RMAGEB"] = PD["RMAGEB"] if "RMAGEB" in PD else None
        SD["RMAGDS"] = PD["RMAGDS"] if "RMAGDS" in PD else "EX2GK: Internal assumption"
        SD["ZMAG"] = PD["ZMAG"] if "ZMAG" in PD and PD["ZMAG"] is not None else np.array([0.0])
        SD["ZMAGEB"] = PD["ZMAGEB"] if "ZMAGEB" in PD else None
        SD["ZMAGDS"] = PD["ZMAGDS"] if "ZMAGDS" in PD else "EX2GK: Internal assumption"
        SD["RGEO"] = PD["RGEO"] if "RGEO" in PD and PD["RGEO"] is not None else np.array([2.96])
        SD["RGEOEB"] = PD["RGEOEB"] if "RGEOEB" in PD else None
        SD["RGEODS"] = PD["RGEODS"] if "RGEODS" in PD else "EX2GK: Internal assumption"
        SD["ZGEO"] = PD["ZGEO"] if "ZGEO" in PD and PD["ZGEO"] is not None else SD["ZMAG"]
        SD["ZGEOEB"] = PD["ZGEOEB"] if "ZGEOEB" in PD and PD["ZGEOEB"] is not None else SD["ZMAGEB"]
        SD["ZGEODS"] = PD["ZGEODS"] if "ZGEODS" in PD else "EX2GK: Internal assumption"
        SD["IP"] = PD["IP"] if "IP" in PD and PD["IP"] is not None else np.array([3.0e6])
        SD["IPEB"] = PD["IPEB"] if "IPEB" in PD and PD["IPEB"] is not None else np.array([4.0e4])
        SD["IPDS"] = PD["IPDS"] if "IPDS" in PD else "EX2GK: Internal assumption"

        # Other magnetic quantities
        SD["CRMAG"] = PD["RMAG"] if "RMAG" in PD else None
        SD["CRMAGEB"] = PD["RMAGEB"] if "RMAGEB" in PD else None
        SD["CRGEO"] = PD["RGEO"] if "RGEO" in PD else None
        SD["CRGEOEB"] = PD["RGEOEB"] if "RGEOEB" in PD else None
        SD["RVAC"] = PD["RVAC"] if "RVAC" in PD and PD["RVAC"] is not None else np.array([2.96])
        SD["RVACEB"] = PD["RVACEB"] if "RVACEB" in PD else None
        SD["RVACDS"] = PD["RVACDS"] if "RVACDS" in PD else "EX2GK: Internal assumption"
        SD["BVAC"] = PD["BVAC"] if "BVAC" in PD else None
        SD["BVACEB"] = PD["BVACEB"] if "BVACEB" in PD else None
        SD["BVACDS"] = PD["BVACDS"] if "BVACDS" in PD else "Unknown"
        SD["FBND"] = PD["FBND"] if "FBND" in PD else None
        SD["FBNDEB"] = PD["FBNDEB"] if "FBNDEB" in PD else None
        SD["FBNDDS"] = PD["FBNDDS"] if "FBNDDS" in PD else "Unknown"
        SD["FAXS"] = PD["FAXS"] if "FAXS" in PD else None
        SD["FAXSEB"] = PD["FAXSEB"] if "FAXSEB" in PD else None
        SD["FAXSDS"] = PD["FAXSDS"] if "FAXSDS" in PD else "Unknown"
        SD["VLOOP"] = np.array([PD["VLOOP"][-1]]) if "VLOOP" in PD and PD["VLOOP"] is not None else None
        SD["VLOOPEB"] = np.array([PD["VLOOPEB"][-1]]) if "VLOOPEB" in PD and PD["VLOOPEB"] is not None else None
        SD["VLOOPDS"] = PD["VLOOPDS"] if "VLOOPDS" in PD else "Unknown"

        # Adjustment parameters
        SD["RMAJSHIFT"] = PD["RMAJSHIFT"] if "RMAJSHIFT" in PD else None
        SD["RMAJSHIFTEB"] = PD["RMAJSHIFTEB"] if "RMAJSHIFTEB" in PD else None
        SD["TESEP"] = PD["TESEP"] if "TESEP" in PD else None
        SD["NESEP"] = PD["NESEP"] if "NESEP" in PD else None
        SD["TESEPSHIFT"] = PD["TESEPSHIFT"] if "TESEPSHIFT" in PD else None
        SD["NESEPSHIFT"] = PD["NESEPSHIFT"] if "NESEPSHIFT" in PD else None
        SD["NESCALE"] = PD["HRTSC"] if "HRTSC" in PD and SD["USENSCALEFLAG"] else None
        SD["NESCALEEB"] = PD["HRTSCEB"] if "HRTSCEB" in PD and SD["USENSCALEFLAG"] else None
        SD["QEDGE"] = PD["QEDGE"+PD["QDDA"]] if SD["USEQEDGEFLAG"] and "QDDA" in PD and PD["QDDA"] is not None and "QEDGE"+PD["QDDA"] in PD else None

        rhopedtopte = None
        rhopedtopteeb = np.array([0.0])
        if "TERMAJTOP" in PD and PD["TERMAJTOP"] is not None:
#            ctag = "C" if SD["PSICORRFLAG"] else ""
            ctag = ""
            (rhopedtopte, jac, rhopedtopteeb) = jptools.convert_coords(PD, PD["TERMAJTOP"], "RMAJORO", "RHOTORN", prein=ctag, preout=ctag)
        SD["RHOPEDTOPTE"] = rhopedtopte
        SD["RHOPEDTOPTEEB"] = np.sqrt(np.power(rhopedtopteeb, 2.0) + np.power(0.02, 2.0))  # Added heuristic error range
        rhopedtopne = None
        rhopedtopneeb = np.array([0.0])
        if "NERMAJTOP" in PD and PD["NERMAJTOP"] is not None:
#            ctag = "C" if SD["PSICORRFLAG"] else ""
            ctag = ""
            (rhopedtopne, jac, rhopedtopneeb) = jptools.convert_coords(PD, PD["NERMAJTOP"], "RMAJORO", "RHOTORN", prein=ctag, preout=ctag)
        SD["RHOPEDTOPNE"] = rhopedtopne
        SD["RHOPEDTOPNEEB"] = np.sqrt(np.power(rhopedtopneeb, 2.0) + np.power(0.02, 2.0))  # Added heuristic error range

        # Calculate magnetic field vector, takes advantage of expected 1/R relation in interpolation
        btfunc = None
        if "BTAX" in PD and PD["BTAX"] is not None and "RBT" in PD and PD["RBT"] is not None:
            invbt = 1.0 / PD["BTAX"]
            btfunc = interp1d(PD["RBT"], invbt, bounds_error=False, fill_value='extrapolate')
        SD["BMAG"] = np.array([-1.0 / btfunc(SD["RMAG"])]).flatten() if btfunc is not None else np.array([3.0])
        SD["BMAGEB"] = None
        SD["BMAGDS"] = PD["BTAXDS"] if btfunc is not None else "EX2GK: Internal assumption"
        SD["CBMAG"] = np.array([-1.0 / btfunc(SD["CRMAG"])]).flatten() if btfunc is not None else np.array([3.0])
        SD["CBMAGEB"] = None
        SD["VAVG"] = np.array([np.mean(PD["VLOOP"])]).flatten() if "VLOOP" in PD and PD["VLOOP"] is not None else None
        SD["VAVGEB"] = None
        SD["VAVGDS"] = PD["VLOOPDS"] if "VLOOPDS" in PD else "Unknown"

        # These are not strictly necessary, most quantities used flux surface averaged magnetic fields, which are approximated by B_mag
#        btovec = 1.0 / btfunc(SD["CS_RMAJORO"]) if btfunc is not None else None
#        btivec = 1.0 / btfunc(SD["CS_RMAJORI"]) if btfunc is not None else None
#        if "CCS_BASE" in SD and SD["CCS_BASE"] is not None:
#            cbtovec = 1.0 / btfunc(SD["CCS_RMAJORO"]) if btfunc is not None and "CCS_RMAJORO" in SD and SD["CCS_RMAJORO"] is not None else None
#            cbtivec = 1.0 / btfunc(SD["CCS_RMAJORI"]) if btfunc is not None and "CCS_RMAJORI" in SD and SD["CCS_RMAJORI"] is not None else None

        # Transfer advanced geometrical data
        mu0 = 4.0 * np.pi * 1.0e-7
        SD["FS_RMAP"] = copy.deepcopy(PD["RMAP"]) if "RMAP" in PD else None
        SD["FS_ZMAP"] = copy.deepcopy(PD["ZMAP"]) if "ZMAP" in PD else None
        SD["FS_PFXMAP"] = copy.deepcopy(PD["PFXMAP"]) if "PFXMAP" in PD else None
        SD["FS_PFXMAPDS"] = PD["PFXMAPDS"] if "PFXMAPDS" in PD else "Unknown"
        SD["FS_RBND"] = copy.deepcopy(PD["RBND"]) if "RBND" in PD else None
        SD["FS_RBNDEB"] = copy.deepcopy(PD["RBNDEB"]) if "RBNDEB" in PD else None
        SD["FS_ZBND"] = copy.deepcopy(PD["ZBND"]) if "ZBND" in PD else None
        SD["FS_ZBNDEB"] = copy.deepcopy(PD["ZBNDEB"]) if "ZBNDEB" in PD else None
        SD["FS_BNDDS"] = "Unknown"
        if "RBNDDS" in PD and PD["RBNDDS"] is not None and PD["RBNDDS"] != "Unknown":
            SD["FS_BNDDS"] = SD["FS_BNDDS"] + "; " + PD["RBNDDS"] if SD["FS_BNDDS"] != "Unknown" else PD["RBNDDS"]
        if "ZBNDDS" in PD and PD["ZBNDDS"] is not None and PD["ZBNDDS"] != "Unknown":
            SD["FS_BNDDS"] = SD["FS_BNDDS"] + "; " + PD["ZBNDDS"] if SD["FS_BNDDS"] != "Unknown" else PD["ZBNDDS"]
        SD["FS_PFN"] = copy.deepcopy(PD["FPSIPFN"]) if "FPSIPFN" in PD else None
        SD["FS_PFNDS"] = PD["FPSIDS"] if "FPSIDS" in PD else "Unknown"
        SD["FS_PFX"] = PD["FPSIPFN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"] if SD["FS_PFN"] is not None and "FBND" in PD and PD["FBND"] is not None and "FAXS" in PD and PD["FAXS"] is not None else None
        SD["FS_PFXDS"] = "EX2GK: Internal calculation"
        SD["FS_F"] = copy.deepcopy(PD["FPSI"]) if "FPSI" in PD else None
        SD["FS_FDS"] = PD["FPSIDS"] if "FPSIDS" in PD else "Unknown"
        SD["FS_FP"] = PD["DFPSI"] * mu0 if "DFPSI" in PD and PD["DFPSI"] is not None else None
        SD["FS_FPDS"] = PD["DFPSIDS"] if "DFPSIDS" in PD else "Unknown"
        SD["FS_P"] = copy.deepcopy(PD["PPSI"]) if "PPSI" in PD else None
        SD["FS_PDS"] = PD["PPSIDS"] if "PPSIDS" in PD else "Unknown"
        SD["FS_PP"] = copy.deepcopy(PD["DPPSI"]) if "DPPSI" in PD else None
        SD["FS_PPDS"] = PD["DPPSIDS"] if "DPPSIDS" in PD else "Unknown"
        SD["FS_Q"] = copy.deepcopy(PD["Q"+eqdda.upper()]) if "Q"+eqdda.upper() in PD else None
        SD["FS_QDS"] = PD["Q"+eqdda.upper()+"DS"] if "Q"+eqdda.upper()+"DS" in PD else "Unknown"
        SD["FS_A"] = copy.deepcopy(PD["AREA"]) if "AREA" in PD and PD["AREA"] is not None else None
        SD["FS_AEB"] = copy.deepcopy(PD["AREAEB"]) if "AREAEB" in PD and PD["AREAEB"] is not None else None
        SD["FS_ADS"] = PD["AREADS"] if "AREADS" in PD else "Unknown"
        SD["FS_V"] = copy.deepcopy(PD["VOL"]) if "VOL" in PD and PD["VOL"] is not None else None
        SD["FS_VEB"] = copy.deepcopy(PD["VOLEB"]) if "VOLEB" in PD and PD["VOLEB"] is not None else None
        SD["FS_VDS"] = PD["VOLDS"] if "VOLDS" in PD else "Unknown"

#        SD["FS_PFN"] = copy.deepcopy(PD["FS_PFN"]) if "FS_PFN" in PD else None
#        SD["FS_RHOPN"] = copy.deepcopy(PD["FS_RHO"]) if "FS_RHO" in PD else None
#        SD["FS_BPOL"] = copy.deepcopy(PD["FS_BPOL"]) if "FS_BPOL" in PD else None
#        SD["FS_BPOL2"] = copy.deepcopy(PD["FS_BPOL2"]) if "FS_BPOL2" in PD else None
#        SD["FS_GPSI"] = SD["FS_RHOPN"] * PD["FS_GRHO"] if SD["FS_RHOPN"] is not None and "FS_GRHO" in PD and PD["FS_GRHO"] is not None else None
#        SD["FS_GPSI2"] = SD["FS_RHOPN"] * SD["FS_RHOPN"] * PD["FS_GRHO2"] if SD["FS_RHOPN"] is not None and "FS_GRHO2" in PD and PD["FS_GRHO2"] is not None else None
#        SD["FS_INVR"] = copy.deepcopy(PD["FS_IR"]) if "FS_IR" in PD else None
#        SD["FS_INVR2"] = copy.deepcopy(PD["FS_IR2"]) if "FS_IR2" in PD else None
#        SD["FS_JPHIBYR"] = copy.deepcopy(PD["FS_JPHI"]) if "FS_JPHI" in PD else None

        SD["PNBI"] = PD["PNBI"] if "PNBI" in PD else None
        SD["PNBIEB"] = PD["PNBIEB"] if "PNBIEB" in PD else None
        SD["PNBIDS"] = PD["PNBIDS"] if "PNBIDS" in PD else "Unknown"
        SD["LNBI"] = PD["LNBI"] if "LNBI" in PD else None
        SD["LNBIEB"] = PD["LNBIEB"] if "LNBIEB" in PD else None
        SD["LNBIDS"] = PD["LNBIDS"] if "LNBIDS" in PD else "Unknown"
        SD["PICRH"] = PD["PICRH"] if "PICRH" in PD else None
        SD["PICRHEB"] = PD["PICRHEB"] if "PICRHEB" in PD else None
        SD["PICRHDS"] = PD["PICRHDS"] if "PICRHDS" in PD else "Unknown"
        SD["LICRH"] = PD["LICRH"] if "LICRH" in PD else None
        SD["LICRHEB"] = PD["LICRHEB"] if "LICRHEB" in PD else None
        SD["LICRHDS"] = PD["LICRHDS"] if "LICRHDS" in PD else "Unknown"
        SD["PECRH"] = PD["PECRH"] if "PECRH" in PD else None
        SD["PECRHEB"] = PD["PECRHEB"] if "PECRHEB" in PD else None
        SD["PECRHDS"] = PD["PECRHDS"] if "PECRHDS" in PD else "Unknown"
        SD["LECRH"] = PD["LECRH"] if "LECRH" in PD else None
        SD["LECRHEB"] = PD["LECRHEB"] if "LECRHEB" in PD else None
        SD["LECRHDS"] = PD["LECRHDS"] if "LECRHDS" in PD else "Unknown"
        SD["PLH"] = PD["PLH"] if "PLH" in PD else None
        SD["PLHEB"] = PD["PLHEB"] if "PLHEB" in PD else None
        SD["PLHDS"] = PD["PLHDS"] if "PLHDS" in PD else "Unknown"
        SD["LLH"] = PD["LLH"] if "LLH" in PD else None
        SD["LLHEB"] = PD["LLHEB"] if "LLHEB" in PD else None
        SD["LLHDS"] = PD["LLHDS"] if "LLHDS" in PD else "Unknown"
        SD["POHM"] = PD["POHM"] if "POHM" in PD else None
        SD["POHMEB"] = PD["POHMEB"] if "POHMEB" in PD else None
        SD["POHMDS"] = PD["POHMDS"] if "POHMDS" in PD else "Unknown"
        SD["LOHM"] = PD["LOHM"] if "LOHM" in PD else None
        SD["LOHMEB"] = PD["LOHMEB"] if "LOHMEB" in PD else None
        SD["LOHMDS"] = PD["LOHMDS"] if "LOHMDS" in PD else "Unknown"
        SD["PRAD"] = PD["PRAD"] if "PRAD" in PD else None
        SD["PRADEB"] = PD["PRADEB"] if "PRADEB" in PD else None
        SD["PRADDS"] = PD["PRADDS"] if "PRADDS" in PD else "Unknown"
        SD["PRADBULK"] = PD["PRADBULK"] if "PRADBULK" in PD else None
        SD["PRADBULKEB"] = PD["PRADBULKEB"] if "PRADBULKEB" in PD else None
        SD["PRADBULKDS"] = PD["PRADBULKDS"] if "PRADBULKDS" in PD else "Unknown"
# TODO: Add net power for comparison against power threshold calculation
#        SD["PTOT"] = 

        SD["GAS0A"] = PD["GAS0A"] if "GAS0A" in PD else None
        SD["GAS0Z"] = PD["GAS0Z"] if "GAS0Z" in PD else None
        gaprov = PD["GAS0ADS"] if "GAS0ADS" in PD and PD["GAS0ADS"] is not None else "Unknown"
        gzprov = PD["GAS0ZDS"] if "GAS0ZDS" in PD and PD["GAS0ZDS"] is not None else "Unknown"
        SD["GAS0R"] = PD["GAS0R"] if "GAS0R" in PD else None
        SD["GAS0REB"] = PD["GAS0REB"] if "GAS0REB" in PD else None
        SD["GAS0RDS"] = PD["GAS0RDS"] if "GAS0RDS" in PD else "Unknown"
        if SD["GAS0RDS"] is not None and SD["GAS0RDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS0RDS"] = SD["GAS0RDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS0RDS"] = SD["GAS0RDS"] + "; " + gzprov
        SD["GAS0C"] = PD["GAS0C"] if "GAS0C" in PD else None
        SD["GAS0CEB"] = PD["GAS0CEB"] if "GAS0CEB" in PD else None
        SD["GAS0CDS"] = PD["GAS0CDS"] if "GAS0CDS" in PD else "Unknown"
        if SD["GAS0CDS"] is not None and SD["GAS0CDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS0CDS"] = SD["GAS0CDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS0CDS"] = SD["GAS0CDS"] + "; " + gzprov
        SD["GAS1A"] = PD["GAS1A"] if "GAS1A" in PD else None
        SD["GAS1Z"] = PD["GAS1Z"] if "GAS1Z" in PD else None
        gaprov = PD["GAS1ADS"] if "GAS1ADS" in PD and PD["GAS1ADS"] is not None else "Unknown"
        gzprov = PD["GAS1ZDS"] if "GAS1ZDS" in PD and PD["GAS1ZDS"] is not None else "Unknown"
        SD["GAS1R"] = PD["GAS1R"] if "GAS1R" in PD else None
        SD["GAS1REB"] = PD["GAS1REB"] if "GAS1REB" in PD else None
        SD["GAS1RDS"] = PD["GAS1RDS"] if "GAS1RDS" in PD else "Unknown"
        if SD["GAS1RDS"] is not None and SD["GAS1RDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS1RDS"] = SD["GAS1RDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS1RDS"] = SD["GAS1RDS"] + "; " + gzprov
        SD["GAS1C"] = PD["GAS1C"] if "GAS1C" in PD else None
        SD["GAS1CEB"] = PD["GAS1CEB"] if "GAS1CEB" in PD else None
        SD["GAS1CDS"] = PD["GAS1CDS"] if "GAS1CDS" in PD else "Unknown"
        if SD["GAS1CDS"] is not None and SD["GAS1CDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS1CDS"] = SD["GAS1CDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS1CDS"] = SD["GAS1CDS"] + "; " + gzprov
        SD["GAS2A"] = PD["GAS2A"] if "GAS2A" in PD else None
        SD["GAS2Z"] = PD["GAS2Z"] if "GAS2Z" in PD else None
        gaprov = PD["GAS2ADS"] if "GAS2ADS" in PD and PD["GAS2ADS"] is not None else "Unknown"
        gzprov = PD["GAS2ZDS"] if "GAS2ZDS" in PD and PD["GAS2ZDS"] is not None else "Unknown"
        SD["GAS2R"] = PD["GAS2R"] if "GAS2R" in PD else None
        SD["GAS2REB"] = PD["GAS2REB"] if "GAS2REB" in PD else None
        SD["GAS2RDS"] = PD["GAS2RDS"] if "GAS2RDS" in PD else "Unknown"
        if SD["GAS2RDS"] is not None and SD["GAS2RDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS2RDS"] = SD["GAS2RDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS2RDS"] = SD["GAS2RDS"] + "; " + gzprov
        SD["GAS2C"] = PD["GAS2C"] if "GAS2C" in PD else None
        SD["GAS2CEB"] = PD["GAS2CEB"] if "GAS2CEB" in PD else None
        SD["GAS2CDS"] = PD["GAS2CDS"] if "GAS2CDS" in PD else "Unknown"
        if SD["GAS2CDS"] is not None and SD["GAS2CDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS2CDS"] = SD["GAS2CDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS2CDS"] = SD["GAS2CDS"] + "; " + gzprov
        SD["GAS3A"] = PD["GAS3A"] if "GAS3A" in PD else None
        SD["GAS3Z"] = PD["GAS3Z"] if "GAS3Z" in PD else None
        gaprov = PD["GAS3ADS"] if "GAS3ADS" in PD and PD["GAS3ADS"] is not None else "Unknown"
        gzprov = PD["GAS3ZDS"] if "GAS3ZDS" in PD and PD["GAS3ZDS"] is not None else "Unknown"
        SD["GAS3R"] = PD["GAS3R"] if "GAS3R" in PD else None
        SD["GAS3REB"] = PD["GAS3REB"] if "GAS3REB" in PD else None
        SD["GAS3RDS"] = PD["GAS3RDS"] if "GAS3RDS" in PD else "Unknown"
        if SD["GAS3RDS"] is not None and SD["GAS3RDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS3RDS"] = SD["GAS3RDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS3RDS"] = SD["GAS3RDS"] + "; " + gzprov
        SD["GAS3C"] = PD["GAS3C"] if "GAS3C" in PD else None
        SD["GAS3CEB"] = PD["GAS3CEB"] if "GAS3CEB" in PD else None
        SD["GAS3CDS"] = PD["GAS3CDS"] if "GAS3CDS" in PD else "Unknown"
        if SD["GAS3CDS"] is not None and SD["GAS3CDS"] != "Unknown":
            if gaprov != "Unknown":
                SD["GAS3CDS"] = SD["GAS3CDS"] + "; " + gaprov
            if gzprov != "Unknown":
                SD["GAS3CDS"] = SD["GAS3CDS"] + "; " + gzprov

        SD["ZEFV"] = PD["ZEFV"] if "ZEFV" in PD else None                     # This one is the recommended signal
        SD["ZEFVEB"] = PD["ZEFVEB"] if "ZEFVEB" in PD else None
        SD["ZEFVDS"] = PD["ZEFVDS"] if "ZEFVDS" in PD else "Unknown"
        SD["ZEFH"] = PD["ZEFH"] if "ZEFH" in PD else None
        SD["ZEFHEB"] = PD["ZEFHEB"] if "ZEFHEB" in PD else None
        SD["ZEFHDS"] = PD["ZEFHDS"] if "ZEFHDS" in PD else "Unknown"

        SD["TWD"] = PD["TWD"] if "TWD" in PD else None
        SD["TWDEB"] = PD["TWDEB"] if "TWDEB" in PD else None
        SD["TWDDS"] = PD["TWDDS"] if "TWDDS" in PD else "Unknown"
        SD["TWE"] = PD["TWE"] if "TWE" in PD else None
        SD["TWEEB"] = PD["TWEEB"] if "TWEEB" in PD else None
        SD["TWEDS"] = PD["TWEDS"] if "TWEDS" in PD else "Unknown"
        SD["LI3D"] = PD["LI3D"] if "LI3D" in PD else None
        SD["LI3DEB"] = PD["LI3DEB"] if "LI3DEB" in PD else None
        SD["LI3DDS"] = PD["LI3DDS"] if "LI3DDS" in PD else "Unknown"
        SD["LI3E"] = PD["LI3E"] if "LI3E" in PD else None
        SD["LI3EEB"] = PD["LI3EEB"] if "LI3EEB" in PD else None
        SD["LI3EDS"] = PD["LI3EDS"] if "LI3EDS" in PD else "Unknown"
        SD["BETAND"] = PD["BETAND"] if "BETAND" in PD else None
        SD["BETANDEB"] = PD["BETANDEB"] if "BETANDEB" in PD else None
        SD["BETANDDS"] = PD["BETANDDS"] if "BETANDDS" in PD else "Unknown"
        SD["BETAPD"] = PD["BETAPD"] if "BETAPD" in PD else None
        SD["BETAPDEB"] = PD["BETAPDEB"] if "BETAPDEB" in PD else None
        SD["BETAPDDS"] = PD["BETAPDDS"] if "BETAPDDS" in PD else "Unknown"
        SD["BETATD"] = PD["BETATD"] if "BETATD" in PD else None
        SD["BETATDEB"] = PD["BETATDEB"] if "BETATDEB" in PD else None
        SD["BETATDDS"] = PD["BETATDDS"] if "BETATDDS" in PD else "Unknown"
        SD["BETANE"] = PD["BETANE"] if "BETANE" in PD else None
        SD["BETANEEB"] = PD["BETANEEB"] if "BETANEEB" in PD else None
        SD["BETANEDS"] = PD["BETANEDS"] if "BETANEDS" in PD else "Unknown"
        SD["BETAPE"] = PD["BETAPE"] if "BETAPE" in PD else None
        SD["BETAPEEB"] = PD["BETAPEEB"] if "BETAPEEB" in PD else None
        SD["BETAPEDS"] = PD["BETAPEDS"] if "BETAPEDS" in PD else "Unknown"
        SD["BETATE"] = PD["BETATE"] if "BETATE" in PD else None
        SD["BETATEEB"] = PD["BETATEEB"] if "BETATEEB" in PD else None
        SD["BETATEDS"] = PD["BETATEDS"] if "BETATEDS" in PD else "Unknown"
        SD["XSAREAP"] = np.array([PD["AREA"][-1]]) if "AREA" in PD and PD["AREA"] is not None else None
        SD["XSAREAPEB"] = np.array([PD["AREAEB"][-1]]) if "AREAEB" in PD and PD["AREAEB"] is not None else None
        SD["XSAREAPDS"] = PD["AREADS"] if "AREADS" in PD else "Unknown"
        SD["VOLP"] = np.array([PD["VOL"][-1]]) if "VOL" in PD and PD["VOL"] is not None else None
        SD["VOLPEB"] = np.array([PD["VOLEB"][-1]]) if "VOLEB" in PD and PD["VOLEB"] is not None else None
        SD["VOLPDS"] = PD["VOLDS"] if "VOLDS" in PD else "Unknown"
        SD["ELONG"] = PD["ELONG"] if "ELONG" in PD else None
        SD["ELONGEB"] = PD["ELONGEB"] if "ELONGEB" in PD else None
        SD["ELONGDS"] = PD["ELONGDS"] if "ELONGDS" in PD else "Unknown"
        SD["TRIANGL"] = PD["TRIANGL"] if "TRIANGL" in PD else None
        SD["TRIANGLEB"] = PD["TRIANGLEB"] if "TRIANGLEB" in PD else None
        SD["TRIANGLDS"] = PD["TRIANGLDS"] if "TRIANGLDS" in PD else "Unknown"
        SD["TRIANGU"] = PD["TRIANGU"] if "TRIANGU" in PD else None
        SD["TRIANGUEB"] = PD["TRIANGUEB"] if "TRIANGUEB" in PD else None
        SD["TRIANGUDS"] = PD["TRIANGUDS"] if "TRIANGUDS" in PD else "Unknown"
        SD["NEUT"] = PD["NEUT"] if "NEUT" in PD else None
        SD["NEUTEB"] = PD["NEUTEB"] if "NEUTEB" in PD else None
        SD["NEUTDS"] = PD["NEUTDS"] if "NEUTDS" in PD else "Unknown"

        hfrac = PD["HFRAC"] if "HFRAC" in PD and PD["HFRAC"] is not None and PD["HFRAC"] > 0.0 else np.array([0.0])
        dfrac = PD["DFRAC"] if "DFRAC" in PD and PD["DFRAC"] is not None and PD["DFRAC"] > 0.0 else np.array([0.001])
        tfrac = PD["TFRAC"] if "TFRAC" in PD and PD["TFRAC"] is not None and PD["TFRAC"] > 0.0 else np.array([0.0])
        hfraceb = PD["HFRACEB"] if "HFRACEB" in PD else np.array([0.0])
        dfraceb = PD["DFRACEB"] if "DFRACEB" in PD else np.array([0.0])
        tfraceb = PD["TFRACEB"] if "TFRACEB" in PD else np.array([0.0])
        fraclist = np.hstack((dfrac, hfrac, tfrac)).flatten()
        total = np.sum(fraclist)
        if hfrac > 0.0 and hfrac < total:
            SD["FH"] = hfrac / total
            SD["FHEB"] = hfraceb / total
            SD["FHDS"] = PD["HFRACDS"] if "HFRACDS" in PD else "EX2GK: Internal assumption"
        if dfrac > 0.0 and dfrac < total:
            SD["FD"] = dfrac / total
            SD["FDEB"] = dfraceb / total
            SD["FDDS"] = PD["DFRACDS"] if "DFRACDS" in PD else "EX2GK: Internal assumption"
        if tfrac > 0.0 and tfrac < total:
            SD["FT"] = tfrac / total
            SD["FTEB"] = tfraceb / total
            SD["FTDS"] = PD["TFRACDS"] if "TFRACDS" in PD else "EX2GK: Internal assumption"

        plasmats = ['D', 'H', 'T']
        fraceblist = np.hstack((dfraceb, hfraceb, tfraceb)).flatten()
        idxmax = np.where(fraclist >= np.nanmax(fraclist))[0][0]
        SD["PLASMAT"] = plasmats[idxmax]

        SD["RXPL"] = PD["RXPL"] if "RXPL" in PD else None
        SD["RXPLDS"] = PD["RXPLDS"] if "RXPLDS" in PD else "Unknown"
        SD["ZXPL"] = PD["ZXPL"] if "ZXPL" in PD else None
        SD["ZXPLDS"] = PD["ZXPLDS"] if "ZXPLDS" in PD else "Unknown"
        SD["RXPU"] = PD["RXPU"] if "RXPU" in PD else None
        SD["RXPUDS"] = PD["RXPUDS"] if "RXPUDS" in PD else "Unknown"
        SD["ZXPU"] = PD["ZXPU"] if "ZXPU" in PD else None
        SD["ZXPUDS"] = PD["ZXPUDS"] if "ZXPUDS" in PD else "Unknown"
        SD["RSPIL"] = PD["RSPIL"] if "RSPIL" in PD else None
        SD["RSPILDS"] = PD["RSPILDS"] if "RSPILDS" in PD else "Unknown"
        SD["ZSPIL"] = PD["ZSPIL"] if "ZSPIL" in PD else None
        SD["ZSPILDS"] = PD["ZSPILDS"] if "ZSPILDS" in PD else "Unknown"
        SD["RSPOL"] = PD["RSPOL"] if "RSPOL" in PD else None
        SD["RSPOLDS"] = PD["RSPOLDS"] if "RSPOLDS" in PD else "Unknown"
        SD["ZSPOL"] = PD["ZSPOL"] if "ZSPOL" in PD else None
        SD["ZSPOLDS"] = PD["ZSPOLDS"] if "ZSPOLDS" in PD else "Unknown"
        SD["RSPIU"] = PD["RSPIU"] if "RSPIU" in PD else None
        SD["RSPIUDS"] = PD["RSPIUDS"] if "RSPIUDS" in PD else "Unknown"
        SD["ZSPIU"] = PD["ZSPIU"] if "ZSPIU" in PD else None
        SD["ZSPIUDS"] = PD["ZSPIUDS"] if "ZSPIUDS" in PD else "Unknown"
        SD["RSPOU"] = PD["RSPOU"] if "RSPOU" in PD else None
        SD["RSPOUDS"] = PD["RSPOUDS"] if "RSPOUDS" in PD else "Unknown"
        SD["ZSPOU"] = PD["ZSPOU"] if "ZSPOU" in PD else None
        SD["ZSPOUDS"] = PD["ZSPOUDS"] if "ZSPOUDS" in PD else "Unknown"

        SD["NEBAR"] = PD["NEBAR"] if "NEBAR" in PD and PD["NEBAR"] is not None else None
        SD["NEBARDS"] = PD["NEBARDS"] if "NEBARDS" in PD else "EX2GK: Internal assumption"
        if SD["NEBAR"] is None:
            SD["NEBAR"] = np.array([0.9 * np.mean(PD["LIDR"]["NE"])]) if "LIDR" in PD and PD["LIDR"] is not None else np.array([5.0e19])
            SD["NEBARDS"] = PD["LIDR"]["NEDS"] if "LIDR" in PD and PD["LIDR"] is not None else "EX2GK: Internal assumption"
            if "HRTS" in PD and PD["HRTS"] is not None:
                SD["NEBAR"] = np.array([0.9 * np.mean(PD["HRTS"]["NE"])])
                SD["NEBARDS"] = PD["HRTS"]["NEDS"]
        if SD["NEBAR"] is None:
            SD["NEBAR"] = np.array([5.0e19])
            SD["NEBARDS"] = "EX2GK: Internal assumption"
        SD["RMIN"] = np.array([(PD["CS_RMAJORO"][-1] - PD["CS_RMAJORI"][-1]) / 2.0])
        SD["RMINDS"] = "EX2GK: Internal calculation"
        SD["PHTHR"] = PD["HMODEPTHR"] if "HMODEPTHR" in PD and PD["HMODEPTHR"] is not None else None
        SD["PHTHRDS"] = PD["HMODEPTHRDS"] if "HMODERPTHRDS" in PD else "EX2GK: Internal calculation"
        # Following L-H power threshold calculation taken from Martin et al., Journal of Physics, 123 (2008)
        if SD["PHTHR"] is None and "NEBAR" in SD and SD["NEBAR"] is not None:
            prov = None
            nebar = SD["NEBAR"] * 1.0e-20
            if "NEBARDS" in SD and SD["NEBARDS"] is not None:
                prov = prov + "; " + SD["NEBARDS"] if prov is not None else SD["NEBARDS"]
            bvac = np.abs(SD["BVAC"]) if "BVAC" in SD and SD["BVAC"] is not None else np.array([3.0])
            if "BVACDS" in SD and SD["BVACDS"] is not None:
                prov = prov + "; " + SD["BVACDS"] if prov is not None else SD["BVACDS"]
            rmin = SD["RMIN"] if "RMIN" in SD and SD["RMIN"] is not None else np.array([1.0])
            if "RMINDS" in SD and SD["RMINDS"] is not None:
                prov = prov + "; " + SD["RMINDS"] if prov is not None else SD["RMINDS"]
            rgeo = SD["RGEO"] if "RGEO" in SD and SD["RGEO"] is not None else np.array([3.0])
            if "RGEODS" in SD and SD["RGEODS"] is not None:
                prov = prov + "; " + SD["RGEODS"] if prov is not None else SD["RGEODS"]
            SD["PHTHR"] = 2.15e6 * np.exp(1.0) * np.power(nebar, 0.782) * np.power(bvac, 0.772) * np.power(rmin, 0.975) * np.power(rgeo, 0.999)  # in W
            SD["PHTHRDS"] = prov + "; EX2GK: Martin 2008 scaling" if prov is not None else "EX2GK: Martin 2008 scaling"

        SD["BOXNBI"] = PD["BOXNBI"] if "BOXNBI" in PD else []
        SD["PNBI4"] = PD["PNBI4"] if "PNBI4" in PD else None
        SD["PNBI4EB"] = PD["PNBI4EB"] if "PNBI4EB" in PD else None
        SD["PNBI4DS"] = PD["PNBI4DS"] if "PNBI4DS" in PD else "Unknown"
        SD["ENBI4"] = PD["ENBI4"] if "ENBI4" in PD else None
        SD["ENBI4DS"] = PD["ENBI4DS"] if "ENBI4DS" in PD else "Unknown"
        SD["E1NBI4"] = PD["E1NBI4"] if "E1NBI4" in PD else None
        SD["E1NBI4DS"] = PD["E1NBI4DS"] if "E1NBI4DS" in PD else "Unknown"
        SD["E2NBI4"] = PD["E2NBI4"] if "E2NBI4" in PD else None
        SD["E2NBI4DS"] = PD["E2NBI4DS"] if "E2NBI4DS" in PD else "Unknown"
        SD["E3NBI4"] = PD["E3NBI4"] if "E3NBI4" in PD else None
        SD["E3NBI4DS"] = PD["E3NBI4DS"] if "E3NBI4DS" in PD else "Unknown"
        SD["ANBI4"] = PD["ANBI4"] if "ANBI4" in PD else None
        SD["ANBI4DS"] = PD["ANBI4DS"] if "ANBI4DS" in PD else "Unknown"
        SD["ZNBI4"] = PD["ZNBI4"] if "ZNBI4" in PD else None
        SD["ZNBI4DS"] = PD["ZNBI4DS"] if "ZNBI4DS" in PD else "Unknown"
        SD["PININBI4"] = PD["PININBI4"] if "PININBI4" in PD else []
        SD["PININBI4DS"] = PD["PININBI4DS"] if "PININBI4DS" in PD else "Unknown"
        SD["PNBI8"] = PD["PNBI8"] if "PNBI8" in PD else None
        SD["PNBI8EB"] = PD["PNBI8EB"] if "PNBI8EB" in PD else None
        SD["PNBI8DS"] = PD["PNBI8DS"] if "PNBI8DS" in PD else "Unknown"
        SD["ENBI8"] = PD["ENBI8"] if "ENBI8" in PD else None
        SD["ENBI8DS"] = PD["ENBI8DS"] if "ENBI8DS" in PD else "Unknown"
        SD["E1NBI8"] = PD["E1NBI8"] if "E1NBI8" in PD else None
        SD["E1NBI8DS"] = PD["E1NBI8DS"] if "E1NBI8DS" in PD else "Unknown"
        SD["E2NBI8"] = PD["E2NBI8"] if "E2NBI8" in PD else None
        SD["E2NBI8DS"] = PD["E2NBI8DS"] if "E2NBI8DS" in PD else "Unknown"
        SD["E3NBI8"] = PD["E3NBI8"] if "E3NBI8" in PD else None
        SD["E3NBI8DS"] = PD["E3NBI8DS"] if "E3NBI8DS" in PD else "Unknown"
        SD["ANBI8"] = PD["ANBI8"] if "ANBI8" in PD else None
        SD["ANBI8DS"] = PD["ANBI8DS"] if "ANBI8DS" in PD else "Unknown"
        SD["ZNBI8"] = PD["ZNBI8"] if "ZNBI8" in PD else None
        SD["ZNBI8DS"] = PD["ZNBI8DS"] if "ZNBI8DS" in PD else "Unknown"
        SD["PININBI8"] = PD["PININBI8"] if "PININBI8" in PD else []
        SD["PININBI8DS"] = PD["PININBI8DS"] if "PININBI8DS" in PD else "Unknown"

        SD["ANTICRH"] = PD["ANTICRH"] if "ANTICRH" in PD else []
        SD["PICRHA"] = PD["PRFA"] if "PRFA" in PD else None
        SD["PICRHAEB"] = PD["PRFAEB"] if "PRFAEB" in PD else None
        SD["PICRHADS"] = PD["PRFADS"] if "PRFADS" in PD else "Unknown"
        SD["PHASICRHA"] = PD["PHASRFA"] if "PHASRFA" in PD else None
        SD["PHASICRHADS"] = PD["PHASRFADS"] if "PHASRFADS" in PD else "Unknown"
        SD["FREQICRHA"] = PD["FREQRFA"] if "FREQRFA" in PD else None
        SD["FREQICRHADS"] = PD["FREQRFADS"] if "FREQRFADS" in PD else "Unknown"
        SD["PICRHB"] = PD["PRFB"] if "PRFB" in PD else None
        SD["PICRHBEB"] = PD["PRFBEB"] if "PRFBEB" in PD else None
        SD["PICRHBDS"] = PD["PRFBDS"] if "PRFBDS" in PD else "Unknown"
        SD["PHASICRHB"] = PD["PHASRFB"] if "PHASRFB" in PD else None
        SD["PHASICRHBDS"] = PD["PHASRFBDS"] if "PHASRFBDS" in PD else "Unknown"
        SD["FREQICRHB"] = PD["FREQRFB"] if "FREQRFB" in PD else None
        SD["FREQICRHBDS"] = PD["FREQRFBDS"] if "FREQRFBDS" in PD else "Unknown"
        SD["PICRHC"] = PD["PRFC"] if "PRFC" in PD else None
        SD["PICRHCEB"] = PD["PRFCEB"] if "PRFCEB" in PD else None
        SD["PICRHCDS"] = PD["PRFCDS"] if "PRFCDS" in PD else "Unknown"
        SD["PHASICRHC"] = PD["PHASRFC"] if "PHASRFC" in PD else None
        SD["PHASICRHCDS"] = PD["PHASRFCDS"] if "PHASRFCDS" in PD else "Unknown"
        SD["FREQICRHC"] = PD["FREQRFC"] if "FREQRFC" in PD else None
        SD["FREQICRHCDS"] = PD["FREQRFCDS"] if "FREQRFCDS" in PD else "Unknown"
        SD["PICRHD"] = PD["PRFD"] if "PRFD" in PD else None
        SD["PICRHDEB"] = PD["PRFDEB"] if "PRFDEB" in PD else None
        SD["PICRHDDS"] = PD["PRFDDS"] if "PRFDDS" in PD else "Unknown"
        SD["PHASICRHD"] = PD["PHASRFD"] if "PHASRFD" in PD else None
        SD["PHASICRHDDS"] = PD["PHASRFDDS"] if "PHASRFDDS" in PD else "Unknown"
        SD["FREQICRHD"] = PD["FREQRFD"] if "FREQRFD" in PD else None
        SD["FREQICRHDDS"] = PD["FREQRFDDS"] if "FREQRFDDS" in PD else "Unknown"
        SD["PICRHE"] = PD["PRFE"] if "PRFE" in PD else None
        SD["PICRHEEB"] = PD["PRFEEB"] if "PRFEEB" in PD else None
        SD["PICRHEDS"] = PD["PRFEDS"] if "PRFEDS" in PD else "Unknown"
        SD["PHASICRHE"] = PD["PHASRFE"] if "PHASRFE" in PD else None
        SD["PHASICRHEDS"] = PD["PHASRFEDS"] if "PHASRFEDS" in PD else "Unknown"
        SD["FREQICRHE"] = PD["FREQRFE"] if "FREQRFE" in PD else None
        SD["FREQICRHEDS"] = PD["FREQRFEDS"] if "FREQRFEDS" in PD else "Unknown"

    if fdebug:
        print("standardize_jet.py: transfer_generic_data() completed.")

    return SD


def process_averaged_profile(procdata, profdata, quantity, newstruct=None, corrflag=False, rhoerrflag=True, somflag=False):
    """
    JET-SPECIFIC FUNCTION
    Processes and formats raw profile data from pre-processing step into a
    standardized profile structure for the generalized fitting routine.

    :arg procdata: dict. Implementation-specific object containing unified coordinate system and extraneous processed data.

    :arg profdata: dict. Implementation-specific object containing only the processed plasma profile data.

    :arg quantity: str. The specific quantity represented by the input plasma profile data, used to flag type of processing.

    :kwarg newstruct: dict. Optional object in which averaged quantity data from a given diagnostic will be stored.

    :kwarg corrflag: bool. Flag to indicate if the corrected coordinate systems were applied to the profiles.

    :kwarg rhoerrflag: bool. Flag to toggle inclusion of radial errors coming from coordinate system conversion.

    :kwarg somflag: bool. Flag to specify error bars as only the standard-deviation-of-the-mean component.

    :returns: dict. Formatted object containing the processed profile data (and fit parameters).
    """
    PD = None
    FD = None
    tag = None
    opt = 0
    DD = None
    if isinstance(procdata, dict):
        PD = procdata
    if isinstance(profdata, dict):
        FD = profdata
    if isinstance(quantity, str):
        tag = quantity
    if isinstance(newstruct, dict):
        DD = newstruct

    if PD is not None and FD is not None and tag is not None and tag in FD and FD[tag] is not None:

        # Copying essential data over to the new structure
        if not isinstance(DD, dict):
            DD = dict()
        DD["VAL"] = FD[tag].copy()
        DD["VALEB"] = FD[tag+"EB"].copy() if tag+"EB" in FD and FD[tag+"EB"] is not None else None
        if somflag and tag+"EM" in FD and FD[tag+"EM"] is not None:
            DD["VALEB"] = FD[tag+"EM"].copy()
        DD["N"] = FD["N"+tag].copy() if "N"+tag in FD and FD["N"+tag] is not None else None
        if DD["VAL"].size < 1:
            DD = None
        DD["PROV"] = FD[tag+"DS"] if tag+"DS" in FD else "Unknown"

    if DD is not None:

        # Define the radial position of the magnetic axis for processing of the diagnostic data
        cdebug = False
#        ctag = "C" if corrflag else ""
        ctag = ""
        ormag = PD["RMAG"] if "RMAG" in PD else None
        rmag = PD[ctag+"RMAG"] if ctag+"RMAG" in PD else None
        if ormag is None:
            psimin = np.amin(np.abs(PD["CS_POLFLUXN"]))
            aidx = np.where(np.abs(PD["CS_POLFLUXN"]) == psimin)[0][0]
            ormag = np.average([PD["CS_RMAJORO"][aidx], PD["CS_RMAJORI"][aidx]])
#        if rmag is None and corrflag:
#            psimin = np.amin(np.abs(PD[ctag+"CS_POLFLUXN"]))
#            aidx = np.where(np.abs(PD[ctag+"CS_POLFLUXN"]) == psimin)[0][0]
#            rmag = np.average([PD[ctag+"CS_RMAJORO"][aidx], PD[ctag+"CS_RMAJORI"][aidx]])
        else:
            rmag = ormag

        # For LIDR, HRTS, CXS, ECM1, TRA0 at JET
        if "RM"+tag in FD and FD["RM"+tag] is not None:
            DD["ORMAJ"] = FD["RM"+tag].copy()
            ofhfs = (DD["ORMAJ"] < ormag)
            pfn = np.ones(DD["ORMAJ"].shape)
            pfneb = np.zeros(DD["ORMAJ"].shape)
            if not np.all(ofhfs):
                (pfno, dummyj, pfnoeb) = jptools.convert_coords(PD, DD["ORMAJ"][np.invert(ofhfs)], "RMAJORO", "POLFLUXN", prein="", preout=ctag, cdebug=cdebug)
                pfn[np.invert(ofhfs)] = pfno
                pfneb[np.invert(ofhfs)] = pfnoeb
            if np.any(ofhfs):
                (pfni, dummyj, pfnieb) = jptools.convert_coords(PD, DD["ORMAJ"][ofhfs], "RMAJORI", "POLFLUXN", prein="", preout=ctag, cdebug=cdebug)
                pfn[ofhfs] = pfni
                pfneb[ofhfs] = pfnieb
            DD["PFN"] = pfn.copy()
            DD["PFNEB"] = pfneb.copy()
            if not rhoerrflag:
                DD["PFNEB"] = np.zeros(DD["PFN"].shape)
            rmaj = np.full(DD["ORMAJ"].shape, np.NaN)
            if not np.all(ofhfs):
                (rmajo, dummyj, dummye) = jptools.convert_coords(PD, np.abs(DD["PFN"][np.invert(ofhfs)]), "POLFLUXN", "RMAJORO", prein=ctag, preout=ctag, cdebug=cdebug)
                rmaj[np.invert(ofhfs)] = rmajo
            if np.any(ofhfs):
                (rmaji, dummyj, dummye) = jptools.convert_coords(PD, np.abs(DD["PFN"][ofhfs]), "POLFLUXN", "RMAJORI", prein=ctag, preout=ctag, cdebug=cdebug)
                rmaj[ofhfs] = rmaji
            DD["RMAJ"] = rmaj.copy()
            fhfs = (DD["RMAJ"] < rmag)
            axvec = np.where(np.invert(fhfs))[0] if np.any(fhfs) else []
            DD["AXID"] = axvec[0] if len(axvec) > 0 else 0
            DD["RHOPN"] = np.sqrt(np.abs(DD["PFN"]))
            pfilt = np.invert(DD["RHOPN"] == 0.0)
            DD["RHOPNEB"] = np.zeros(DD["RHOPN"].shape)
            DD["RHOPNEB"][pfilt] = np.sqrt(np.power(DD["RHOPNEB"][pfilt], 2.0) + np.power(0.5 * DD["PFNEB"][pfilt] / DD["RHOPN"][pfilt], 2.0))
            (DD["TFN"], dummyj, pteb) = jptools.convert_coords(PD, np.abs(DD["PFN"]), "POLFLUXN", "TORFLUXN", prein=ctag, preout=ctag, cdebug=cdebug)
            DD["TFNEB"] = np.sqrt(np.power(DD["PFNEB"], 2.0) + np.power(pteb, 2.0)) if rhoerrflag else np.zeros(DD["PFNEB"].shape)
            DD["RHOTN"] = np.sqrt(np.abs(DD["TFN"]))
            tfilt = np.invert(DD["RHOTN"] == 0.0)
            DD["RHOTNEB"] = np.full(DD["RHOTN"].shape, 0.002) if rhoerrflag else np.zeros(DD["RHOTN"].shape)
            DD["RHOTNEB"][tfilt] = 0.5 * DD["TFNEB"][tfilt] / DD["RHOTN"][tfilt]
            zfilt = (DD["TFN"] < 1.0e-4)   # Avoids true zero at magnetic axis - conflict with derivative constraint
            if np.any(zfilt):
                DD["PFN"][zfilt] = np.NaN
                DD["RHOPN"][zfilt] = np.NaN
                DD["TFN"][zfilt] = np.NaN
                DD["RHOTN"][zfilt] = np.NaN
            if np.any(fhfs):
                DD["PFN"][fhfs] = -DD["PFN"][fhfs]       # pol_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOPN"][fhfs] = -DD["RHOPN"][fhfs]   # rho_pol from -1 to 1 (-1 is inner LCFS)
                DD["TFN"][fhfs] = -DD["TFN"][fhfs]       # tor_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOTN"][fhfs] = -DD["RHOTN"][fhfs]   # rho_tor from -1 to 1 (-1 is inner LCFS)

        # For ECM2, NBP2, PION, MAGN, OHM, WSXP at JET
        elif "PFN"+tag in FD and FD["PFN"+tag] is not None:
            DD["OPFN"] = FD["PFN"+tag].copy()
            (DD["PFN"], dummyj, DD["PFNEB"]) = jptools.convert_coords(PD, DD["OPFN"], "POLFLUXN", "POLFLUXN", prein="", preout=ctag, cdebug=cdebug)
            if not rhoerrflag:
                DD["PFNEB"] = np.zeros(DD["PFN"].shape)
            pswap = np.where((DD["PFN"][:-1] * DD["PFN"][1:]) < 0.0)[0]
            if len(pswap) == 0:
                pdiff = np.diff(DD["PFN"])
                pswap = np.where((pdiff[:-1] * pdiff[1:]) < 0.0)[0]
            pidx = pswap[0] + 1 if len(pswap) > 0 else 0
            testvec = np.arange(0, DD["PFN"].size)
            ofhfs = (testvec < pidx)
            rmaj = np.full(DD["OPFN"].shape, np.NaN)
            if not np.all(ofhfs):
                (rmajo, dummyj, dummye) = jptools.convert_coords(PD, np.abs(DD["PFN"][np.invert(ofhfs)]), "POLFLUXN", "RMAJORO", prein=ctag, preout=ctag, cdebug=cdebug)
                rmaj[np.invert(ofhfs)] = rmajo
            if np.any(ofhfs):
                (rmaji, dummyj, dummye) = jptools.convert_coords(PD, np.abs(DD["PFN"][ofhfs]), "POLFLUXN", "RMAJORI", prein=ctag, preout=ctag, cdebug=cdebug)
                rmaj[ofhfs] = rmaji
            DD["RMAJ"] = rmaj.copy()
            fhfs = (DD["RMAJ"] < rmag)
            axvec = np.where(np.invert(fhfs))[0] if np.any(fhfs) else []
            DD["AXID"] = axvec[0] if len(axvec) > 0 else 0
            DD["RHOPN"] = np.sqrt(np.abs(DD["PFN"]))
            pfilt = np.invert(DD["RHOPN"] == 0.0)
            DD["RHOPNEB"] = np.zeros(DD["RHOPN"].shape)
            if "RHOPNEB" in FD and FD["RHOPNEB"] is not None:
                if FD["RHOPNEB"].size == 1:
                    DD["RHOPNEB"] = np.full(DD["RHOPN"].shape, FD["RHOPNEB"][0])
                elif FD["RHOPNEB"].size == DD["RHOPN"].size:
                    DD["RHOPNEB"] = FD["RHOPNEB"].squeeze()
            DD["RHOPNEB"][pfilt] = np.sqrt(np.power(DD["RHOPNEB"][pfilt], 2.0) + np.power(0.5 * DD["PFNEB"][pfilt] / DD["RHOPN"][pfilt], 2.0))
            (DD["TFN"], dummyj, pteb) = jptools.convert_coords(PD, np.abs(DD["PFN"]), "POLFLUXN", "TORFLUXN", prein=ctag, preout=ctag, cdebug=cdebug)
            DD["TFNEB"] = np.sqrt(np.power(DD["PFNEB"], 2.0) + np.power(pteb, 2.0)) if rhoerrflag else np.zeros(DD["PFNEB"].shape)
            DD["RHOTN"] = np.sqrt(np.abs(DD["TFN"]))
            tfilt = np.invert(DD["RHOTN"] == 0.0)
            DD["RHOTNEB"] = np.full(DD["RHOTN"].shape, 0.002) if rhoerrflag else np.zeros(DD["RHOTN"].shape)
            DD["RHOTNEB"][tfilt] = 0.5 * DD["TFNEB"][tfilt] / DD["RHOTN"][tfilt]
            zfilt = (DD["TFN"] < 1.0e-4)   # Avoids true zero at magnetic axis - conflict with derivative constraint
            if np.any(zfilt):
                DD["PFN"][zfilt] = np.NaN
                DD["RHOPN"][zfilt] = np.NaN
                DD["TFN"][zfilt] = np.NaN
                DD["RHOTN"][zfilt] = np.NaN
            if np.any(fhfs):
                DD["PFN"][fhfs] = -DD["PFN"][fhfs]       # pol_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOPN"][fhfs] = -DD["RHOPN"][fhfs]   # rho_pol from -1 to 1 (-1 is inner LCFS)
                DD["TFN"][fhfs] = -DD["TFN"][fhfs]       # tor_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOTN"][fhfs] = -DD["RHOTN"][fhfs]   # rho_tor from -1 to 1 (-1 is inner LCFS)

        # For TRAU at JET
        elif "TFN"+tag in FD and FD["TFN"+tag] is not None:
            DD["OTFN"] = FD["TFN"+tag].copy()
            (DD["TFN"], dummyj, DD["TFNEB"]) = jptools.convert_coords(PD, DD["OTFN"], "TORFLUXN", "TORFLUXN", prein="", preout=ctag, cdebug=cdebug)
            if not rhoerrflag:
                DD["TFNEB"] = np.zeros(DD["TFN"].shape)
            pswap = np.where((DD["TFN"][:-1] * DD["TFN"][1:]) < 0.0)[0]
            if len(pswap) == 0:
                pdiff = np.diff(DD["TFN"])
                pswap = np.where((pdiff[:-1] * pdiff[1:]) < 0.0)[0]
            pidx = pswap[0] + 1 if len(pswap) > 0 else 0
            testvec = np.arange(0, DD["TFN"].size)
            ofhfs = (testvec < pidx)
            rmaj = np.full(DD["OTFN"].shape, np.NaN)
            if not np.all(ofhfs):
                (rmajo, dummyj, dummye) = jptools.convert_coords(PD, np.abs(DD["TFN"][np.invert(ofhfs)]), "TORFLUXN", "RMAJORO", prein=ctag, preout=ctag, cdebug=cdebug)
                rmaj[np.invert(ofhfs)] = rmajo
            if np.any(ofhfs):
                (rmaji, dummyj, dummye) = jptools.convert_coords(PD, np.abs(DD["TFN"][ofhfs]), "TORFLUXN", "RMAJORI", prein=ctag, preout=ctag, cdebug=cdebug)
                rmaj[ofhfs] = rmaji
            DD["RMAJ"] = rmaj.copy()
            fhfs = (DD["RMAJ"] < rmag)
            axvec = np.where(np.invert(fhfs))[0] if np.any(fhfs) else []
            DD["AXID"] = axvec[0] if len(axvec) > 0 else 0
            DD["RHOTN"] = np.sqrt(np.abs(DD["TFN"]))
            pfilt = np.invert(DD["RHOTN"] == 0.0)
            DD["RHOTNEB"] = np.zeros(DD["RHOTN"].shape)
            if "RHOTNEB" in FD and FD["RHOTNEB"] is not None:
                if FD["RHOTNEB"].size == 1:
                    DD["RHOTNEB"] = np.full(DD["RHOTN"].shape, FD["RHOTNEB"][0])
                elif FD["RHOTNEB"].size == DD["RHOTN"].size:
                    DD["RHOTNEB"] = FD["RHOTNEB"].squeeze()
            DD["RHOTNEB"][pfilt] = np.sqrt(np.power(DD["RHOTNEB"][pfilt], 2.0) + np.power(0.5 * DD["TFNEB"][pfilt] / DD["RHOTN"][pfilt], 2.0))
            (DD["PFN"], dummyj, pteb) = jptools.convert_coords(PD, np.abs(DD["TFN"]), "TORFLUXN", "POLFLUXN", prein=ctag, preout=ctag, cdebug=cdebug)
            DD["PFNEB"] = np.sqrt(np.power(DD["TFNEB"], 2.0) + np.power(pteb, 2.0)) if rhoerrflag else np.zeros(DD["TFNEB"].shape)
            DD["RHOPN"] = np.sqrt(np.abs(DD["PFN"]))
            tfilt = np.invert(DD["RHOPN"] == 0.0)
            DD["RHOPNEB"] = np.full(DD["RHOPN"].shape, 0.002) if rhoerrflag else np.zeros(DD["RHOPN"].shape)
            DD["RHOPNEB"][tfilt] = 0.5 * DD["PFNEB"][tfilt] / DD["RHOPN"][tfilt]
            zfilt = (DD["PFN"] < 1.0e-4)   # Avoids true zero at magnetic axis - conflict with derivative constraint
            if np.any(zfilt):
                DD["PFN"][zfilt] = np.NaN
                DD["RHOPN"][zfilt] = np.NaN
                DD["TFN"][zfilt] = np.NaN
                DD["RHOTN"][zfilt] = np.NaN
            if np.any(fhfs):
                DD["PFN"][fhfs] = -DD["PFN"][fhfs]       # pol_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOPN"][fhfs] = -DD["RHOPN"][fhfs]   # rho_pol from -1 to 1 (-1 is inner LCFS)
                DD["TFN"][fhfs] = -DD["TFN"][fhfs]       # tor_flux from -1 to 1 (-1 is inner LCFS)
                DD["RHOTN"][fhfs] = -DD["RHOTN"][fhfs]   # rho_tor from -1 to 1 (-1 is inner LCFS)

    return DD


def standardize_diagnostic_data(procdata, newstruct=None, useexp=True, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Converts all profiles from the various diagnostic data into
    the same coordinate system and format for improved clarity
    during further processing and profile fitting.

    :arg procdata: dict. Implementation-specific object containing processed extracted data.

    :kwarg newstruct: dict. Optional object in which formatted data will be stored.

    :kwarg useexp: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Formatted object containing all profile data for additional processing.
    """
    PD = None
    DD = None
    if isinstance(procdata, dict):
        PD = procdata
    if isinstance(newstruct, dict):
        DD = newstruct
    else:
        DD = dict()

    if PD is not None:
#        cf = PD["PSICORRFLAG"] if "PSICORRFLAG" in PD else False
        cf = False
        rf = PD["RHOEBFLAG"] if "RHOEBFLAG" in PD else False
        sf = PD["STDMEANFLAG"] if "STDMEANFLAG" in PD else False
        DD["POLFLAG"] = True if "POLFLAG" in PD and PD["POLFLAG"] else False

        # Effective charge required for impurity density filtering, guestimates are given as defaults
        DD["ZEFF"] = PD["ZEFV"] if "ZEFV" in PD and PD["ZEFV"] is not None else np.array([1.5])             # Recommended signal
        DD["NEMAX"] = PD["NEBAR"] if "NEBAR" in PD and PD["NEBAR"] is not None else np.array([5.0e19])

        diag = "HRTS"
        quantities = ["NE", "TE"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = tempd

        if fdebug and "HRTS" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): HRTS Thomson scattering data formatted.")

        diag = "LIDR"
        quantities = ["NE", "TE"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = tempd

        if fdebug and "LIDR" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): LIDR Thomson scattering data formatted.")

        diag = "KG10"
        quantities = ["NE"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = tempd

        if fdebug and "KG10" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): KG10 reflectometry data formatted.")

        diag = "REFL"
        quantities = []

        if fdebug and "REFL" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): REFL reflectometry data formatted.")

        # ECE measurements are removed for carbon wall discharges, observed to be generally misaligned with LIDR
        eceflag = False if useexp and ("SHOT" in PD and PD["SHOT"] <= PD["ILWSTART"]) else True

        diag = "ECR"
        quantities = ["TE"]
        if diag in PD and PD[diag] is not None and eceflag:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = tempd

        if fdebug and "ECR" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): KK3 radiometer electron cyclotron emission data formatted.")

        diag = "ECM"
        quantities = ["TE"]
        if diag in PD and PD[diag] is not None and eceflag:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = tempd

        if fdebug and "ECM" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): ECM Michelson interferometer electron cyclotron emission data formatted.")

        # The charge exchange diagnostic is different from others at JET, large amount of separate fields
        diag = "CX"
        quantities = ["TI", "AF", "NIMP"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for key in PD[diag]:
                DD[diag][key] = dict()
                for dtag in quantities:
                    if dtag in PD[diag][key] and PD[diag][key][dtag] is not None:
                        tempd = process_averaged_profile(PD, PD[diag][key], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                        if tempd is not None:
                            DD[diag][key][dtag] = copy.deepcopy(tempd)

                            # Specify measured impurity, assume Ne if not given
                            if "Z" not in DD[diag][key]:
                                DD[diag][key]["A"] = PD[diag][key]["A"] if "A" in PD[diag][key] and PD[diag][key]["A"] is not None else np.array([20.0])
                                DD[diag][key]["Z"] = PD[diag][key]["Z"] if "Z" in PD[diag][key] and PD[diag][key]["Z"] is not None else np.array([10.0])
                                if "ADS" in PD[diag][key] and "ZDS" in PD[diag][key]:
                                    DD[diag][key]["PROV"] = PD[diag][key]["ADS"] + "; " + PD[diag][key]["ZDS"]
                            if "MAT" not in DD[diag][key] and "MAT" in PD[diag][key] and PD[diag][key]["MAT"] is not None:
                                DD[diag][key]["MAT"] = PD[diag][key]["MAT"]

        if fdebug and "CX" in DD:
            cxstr = ""
            for key in DD["CX"]:
                cxstr = cxstr + ", " + key.upper() if cxstr else cxstr + key.upper()
            if cxstr:
                print("standardize_jet.py: standardize_diagnostic_data(): %s charge exchange data formatted." % (cxstr))

        diag = "MAGN"
#        quantities = ["Q", "CQ"]
        quantities = ["Q"]
        eqdda = None
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = tempd
                        DD[diag][dtag]["EQTYPE"] = PD[diag]["EQTYPE"] if "EQTYPE" in PD[diag] else "UNKNOWN"
                        eqdda = PD[diag]["EQTYPE"] if "EQTYPE" in PD[diag] and eqdda is None else None

        if fdebug and "MAGN" in DD:
            if eqdda is None:
                eqdda = "UNKNOWN"
            print("standardize_jet.py: standardize_diagnostic_data(): %s safety factor data formatted." % (eqdda.upper()))

        diag = "WSXP"
        quantities = ["ZEFF", "NIMPL", "NIMPM", "NIMPH"]
        if diag in PD and PD[diag] is not None:
            DD[diag] = dict()
            for dtag in quantities:
                if dtag in PD[diag] and PD[diag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[diag], quantity=dtag, corrflag=cf, rhoerrflag=rf, somflag=sf)
                    if tempd is not None:
                        DD[diag][dtag] = copy.deepcopy(tempd)
                        if dtag.startswith("NIMP"):
                            key = dtag[1:]
                            if "A" not in DD[diag][dtag]:
                                DD[diag][dtag]["A"] = PD[diag]["A"+key] if "A"+key in PD[diag] and PD[diag]["A"+key] is not None else np.array([2.0])
                                if "A"+key+"DS" in PD[diag]:
                                    DD[diag][dtag]["PROV"] = DD[diag][dtag]["PROV"] + "; " + PD[diag]["A"+key+"DS"] if "PROV" in DD[diag][dtag]["PROV"] else PD[diag]["A"+key+"DS"]
                            if "Z" not in DD[diag][dtag]:
                                DD[diag][dtag]["Z"] = PD[diag]["Z"+key] if "Z"+key in PD[diag] and PD[diag]["Z"+key] is not None else np.array([1.0])
                                if "Z"+key+"DS" in PD[diag]:
                                    DD[diag][dtag]["PROV"] = DD[diag][dtag]["PROV"] + "; " + PD[diag]["Z"+key+"DS"] if "PROV" in DD[diag][dtag]["PROV"] else PD[diag]["Z"+key+"DS"]
                                    DD[diag][dtag]["ZPROV"] = PD[diag]["Z"+key+"DS"]
                            if "MAT" not in DD[diag][dtag] and "MAT"+key in PD[diag] and PD[diag]["MAT"+key] is not None:
                                DD[diag][dtag]["MAT"] = PD[diag]["MAT"+key]
                        else:
                            DD[diag][dtag] = tempd

        if fdebug and "WSXP" in DD:
            print("standardize_jet.py: standardize_diagnostic_data(): WSXP soft X-ray tomographic impurity data formatted.")

    if fdebug:
        print("standardize_jet.py: standardize_diagnostic_data() completed.")

    return DD


def get_standardized_profile_data(diagdata, hfsflag=False, polflag=False):
    """
    JET-SPECIFIC FUNCTION
    Extracts the raw diagnostic data from the input object with the
    desired radial coordinate vector. This function is nearly
    general but requires the diagnostic data fields to be specified
    in a certain way.

    :arg diagdata: dict. Formatted object containing raw diagnostic profile data.

    :kwarg hfsflag: bool. Flag which toggles usage of high-field side data.

    :kwarg polflag: bool. Flag which toggles usage of poloidal flux coordinate.

    :returns: (array, array, array, array).
        Vector of radial points, vector of errors in radial points corresponding to the
        radial points, vector of specified quantity from diagnostic data corresponding
        to the radial points, vector of errors in specified quantity from diagnostic
        data corresponding to the radial points.
    """
    DD = None
    if isinstance(diagdata, dict):
        DD = diagdata

    xval = None
    xerr = None
    yval = None
    yerr = None
    if DD is not None and "VAL" in DD and DD["VAL"] is not None and "AXID" in DD and DD["AXID"] is not None:
        idx = DD["AXID"]
        if polflag and "RHOPN" in DD:
            xval = -DD["RHOPN"][idx::-1] if hfsflag else DD["RHOPN"][idx:]
            xerr = np.zeros(xval.shape)
            if "RHOPNEB" in DD and DD["RHOPNEB"] is not None:
                xerr = DD["RHOPNEB"][idx::-1] if hfsflag else DD["RHOPNEB"][idx:]
        elif "RHOTN" in DD:
            xval = -DD["RHOTN"][idx::-1] if hfsflag else DD["RHOTN"][idx:]
            xerr = np.zeros(xval.shape)
            if "RHOTNEB" in DD and DD["RHOTNEB"] is not None:
                xerr = DD["RHOTNEB"][idx::-1] if hfsflag else DD["RHOTNEB"][idx:]
        yval = DD["VAL"][idx::-1] if hfsflag else DD["VAL"][idx:]
        yerr = np.zeros(yval.shape)
        if "VALEB" in DD and DD["VALEB"] is not None:
            yerr = DD["VALEB"][idx::-1] if hfsflag else DD["VALEB"][idx:]

    return (xval, xerr, yval, yerr)


def add_filtered_profile_data(tag, newstruct=None, xval=None, xerr=None, yval=None, yerr=None):
    """
    STANDARDIZED FUNCTION
    Adds input data into standardized fields for the specified
    processed profiles, creating these fields if they do not
    already exist.

    If an object is passed in via the newstruct argument, this
    function first checks whether or not the fields already
    exist in that object. If so, the new data is appended to
    the existing data.

    :arg tag: str. The specific quantity represented by the input plasma profile data, used to flag type of processing.

    :kwarg newstruct: dict. Optional object in which new fields and formatted data will be stored.

    :kwarg xval: array. Vector of radial points.

    :kwarg xerr: array. Vector of errors in radial points corresponding to the radial points.

    :kwarg yval: array. Vector of specified quantity from diagnostic data corresponding to the radial points.

    :kwarg yerr: array. Vector of errors in specified quantity from diagnostic data corresponding to the radial points.

    :returns: dict. Object with identical structure as input object except with input profile appended to standard fields.
    """
    otag = None
    SD = None
    if isinstance(tag, str) and tag:
        otag = tag.upper()
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()

    xx = np.array([])
    xe = np.array([])
    yy = np.array([])
    ye = np.array([])
    if isinstance(xval, (list, tuple, np.ndarray)):
        xx = np.array(xval).flatten()
    elif isinstance(xval, (int, float)):
        xx = np.array([float(xval)])
    if isinstance(xerr, (list, tuple, np.ndarray)):
        xe = np.array(xerr).flatten()
    elif isinstance(xerr, (int, float)):
        xe = np.array([float(xerr)])
    if isinstance(yval, (list, tuple, np.ndarray)):
        yy = np.array(yval).flatten()
    elif isinstance(yval, (int, float)):
        yy = np.array([float(yval)])
    if isinstance(yerr, (list, tuple, np.ndarray)):
        ye = np.array(yerr).flatten()
    elif isinstance(yerr, (int, float)):
        ye = np.array([float(yerr)])

    idx = None
    if otag is not None:
        idx = 0
        if otag+"RAW" not in SD:
            SD[otag+"RAW"] = np.array([])
        if otag+"RAWEB" not in SD:
            SD[otag+"RAWEB"] = np.array([])
        if otag+"RAWX" not in SD:
            SD[otag+"RAWX"] = np.array([])
        if otag+"RAWXEB" not in SD:
            SD[otag+"RAWXEB"] = np.array([])
        if otag+"DMAP" not in SD:
            SD[otag+"DMAP"] = np.array([], dtype=np.int64)
        elif SD[otag+"DMAP"].size > 0:
            idx = np.rint(np.nanmax(SD[otag+"DMAP"])) + 1

        if xx.size > 0 and xx.size == xe.size and xx.size == yy.size and xe.size == ye.size:
            SD[otag+"RAW"] = np.hstack((SD[otag+"RAW"], yy))
            SD[otag+"RAWEB"] = np.hstack((SD[otag+"RAWEB"], ye))
            SD[otag+"RAWX"] = np.hstack((SD[otag+"RAWX"], xx))
            SD[otag+"RAWXEB"] = np.hstack((SD[otag+"RAWXEB"], xe))
            SD[otag+"DMAP"] = np.hstack((SD[otag+"DMAP"], np.full(xx.shape, idx)))

    return SD


def error_multiplier(error, scale, lower_bound=None, upper_bound=None, value=None):
    """
    JET-SPECIFIC FUNCTION
    Applies multiplier to error vector, interprets upper and lower bounds as
    absolute values. If the actual value vector to provided, the bounds are
    interpreted as relative values.

    :arg error: array. Error vector to which multiplier is applied.

    :arg scale: array. Multiplication vector to be applied, singular entry treated as global multiplier.

    :kwarg lower_bound: array. Lower allowable bound of scaled error, singular entry treated as global lower bound.

    :kwarg upper_bound: array. Lower allowable bound of scaled error, singular entry treated as global lower bound.

    :kwarg value: array. Value vector associated with error vector, providing it switches bounds to relative.

    :returns: array. Scaled error vector.
    """
    valeb = None
    sc = None
    lb = None
    ub = None
    val = None
    if isinstance(error, (list, tuple, np.ndarray)):
        valeb = np.array(error).flatten()
    if isinstance(scale, (list, tuple, np.ndarray)):
        sc = np.array(scale).flatten()
    elif isinstance(scale, (int, float)) and valeb is not None:
        sc = np.full(valeb.shape, scale)
    if isinstance(lower_bound, (list, tuple, np.ndarray)):
        lb = np.array(lower_bound).flatten()
    elif isinstance(lower_bound, (int, float)) and valeb is not None:
        lb = np.full(valeb.shape, lower_bound)
    if lb is None and valeb is not None:
        lb = np.full(valeb.shape, -np.Inf)
    if isinstance(upper_bound, (list, tuple, np.ndarray)):
        ub = np.array(upper_bound).flatten()
    elif isinstance(upper_bound, (int, float)) and valeb is not None:
        ub = np.full(valeb.shape, upper_bound)
    if ub is None and valeb is not None:
        ub = np.full(valeb.shape, np.Inf)
    if isinstance(value, (list, tuple, np.ndarray)):
        val = np.array(value).flatten()

    if valeb is not None and sc is not None and sc.size == valeb.size:
        sc = np.abs(sc)
        if val is not None and val.size == valeb.size:
            if lb.size == val.size:
                lb = lb * val
            if ub.size == val.size:
                ub = ub * val

        mfilt = (sc < 1.0)
        if np.any(mfilt):
            if lb.size == valeb.size:
                sfilt = np.all([mfilt, valeb * sc > lb], axis=0)
                bfilt = np.all([np.invert(sfilt), valeb > lb], axis=0)
                if np.any(sfilt):
                    valeb[sfilt] = valeb[sfilt] * sc[sfilt]
                if np.any(bfilt):
                    valeb[bfilt] = lb[bfilt]
            else:
                valeb[mfilt] = valeb[mfilt] * sc[mfilt]
        if np.any(np.invert(mfilt)):
            if ub.size == valeb.size:
                sfilt = np.all([np.invert(mfilt), valeb * sc < ub], axis=0)
                bfilt = np.all([np.invert(sfilt), valeb < ub], axis=0)
                if np.any(sfilt):
                    valeb[sfilt] = valeb[sfilt] * sc[sfilt]
                if np.any(bfilt):
                    valeb[bfilt] = ub[bfilt]
            else:
                valeb[np.invert(mfilt)] = valeb[np.invert(mfilt)] * sc[np.invert(mfilt)]

    return valeb


def filter_ne(diagdata, newstruct=None, expflag=True, edgeflag=False, hfsflag=False, ftflag=True, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed electron density profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed electron density profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ftflag: bool. Flag to specify that input profile belongs to a time window within the flat-top phase.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "NE"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Region-specific filters
        # Technically does nothing since Li-beam diagnostic at JET is not currently coupled to this tool
        if edgeflag:
            status = 0       # Dummy operation just to conserve structure
        else:
            # Core NE data in core can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                cfilt = (np.abs(darr[0, :]) <= 0.7)
                vallim = 0.7 * np.mean(darr[2, :][cfilt]) if np.any(cfilt) else 0.0
                rfilt = (np.abs(darr[0, :]) <= 0.7)
                lfilt = (darr[2, :] < vallim)
                darr = ptools.custom_filter_2d(darr, filters=[lfilt, rfilt], operation='and', finvert=True)

            # Core NE data can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0:
                llim = 1.0e19 if ftflag else 5.0e18
                vfilt = (darr[2, :] > llim)
                efilt = (darr[3, :] > 1.1 * darr[2, :])
                darr = ptools.custom_filter_2d(darr, filters=[vfilt, efilt], operation='and', finvert=True)

            # Filters designed based on experience using JET diagnostic data
            if expflag:
                # Core NE data near edge can be corrupted with large errors, remove points to ensure error function does not overfit
                if darr.size > 0:
                    rfilt = (np.abs(darr[0, :]) >= 0.9)
                    vfilt = np.all([darr[2, :] < 1.0e19, darr[3, :] > 0.9 * darr[2, :]], axis=0)
                    darr = ptools.custom_filter_2d(darr, filters=[rfilt, vfilt], operation='and', finvert=True)

                # Core NE data near pedestal can be corrupted with large errors due to presence of ELMs, not always removed due to CHAIN2 pre
                if darr.size > 0:
                    efilt = np.all([np.abs(darr[0, :]) >= 0.8, np.abs(darr[0, :]) <= 0.975, darr[3, :] > 5.0e18], axis=0)
                    if np.any(efilt):
                        darr[3, :][efilt] = 0.7 * darr[3, :][efilt]

        # Boundary filters - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])
        if expflag and darr.size > 0:
            rfilt = (np.abs(darr[0, :]) >= 1.0)
            ufilt = (darr[2, :] > 1.0e19)
            darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD, status)


def filter_te(diagdata, newstruct=None, expflag=True, edgeflag=False, hfsflag=False, ftflag=True, trustflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed electron temperature profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed electron temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ftflag: bool. Flag to specify that input profile belongs to a time window within the flat-top phase.

    :kwarg trustflag: bool. Flag to specify that input profile is high-quality data, employs reduced filtering.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "TE"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Region-specific filters
        # Although no edge-specific TE diagnostics at JET, this setting is also used for ECE measurements due to better quality data
        if edgeflag or trustflag:
            # Edge TE data at edge can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0:
                llim = 1000 if ftflag else 100
                rfilt = (np.abs(darr[0, :]) >= 0.95)
                ufilt = (darr[2, :] > llim)
                darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

        else:
            # Core TE data in core can be corrupted with zeroes in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) <= 0.7)
                vallim = 0.5 * np.mean(darr[2, :][rfilt]) if np.any(rfilt) else 0.0
                lfilt = (darr[2, :] < vallim)
                darr = ptools.custom_filter_2d(darr, filters=[lfilt, rfilt], operation='and', finvert=True)

            # Core TE data at edge can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0:
                llim = 1000 if ftflag else 100
                rfilt = (np.abs(darr[0, :]) >= 0.95)
                ufilt = (darr[2, :] > llim)
                darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

            # Core TE data at edge can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0 and ftflag:
                rfilt = (np.abs(darr[0, :]) >= 0.99)
                ufilt = (darr[2, :] > 200)
                darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

            # Core TE data can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0:
                vfilt = (darr[2, :] > 2.0e2)
                efilt = (darr[3, :] > 0.9 * darr[2, :])
                darr = ptools.custom_filter_2d(darr, filters=[vfilt, efilt], operation='and', finvert=True)

            if expflag:
                # Core TE error at pedestal top greatly influenced by ELMs, ELM filtering not useful due to limited time resolution
                if darr.size > 0:
                    rfilt = np.all([np.abs(darr[0, :]) >= 0.7, np.abs(darr[0, :]) <= 0.95], axis=0)
                    efilt = (darr[3, :] >= 0.3 * darr[2, :])
                    pfilt = np.all([rfilt, efilt], axis=0)
                    if np.any(pfilt):
                        darr[3, :][pfilt] = 0.4 * darr[3, :][pfilt]

                # Core TE data near edge can be corrupted with large errors, remove points to ensure error function does not overfit
                if darr.size > 0:
                    rfilt = (np.abs(darr[0, :]) >= 0.9)
                    vfilt = np.all([darr[2, :] < 5.0e2, darr[3, :] > 3.0e2], axis=0)
                    darr = ptools.custom_filter_2d(darr, filters=[rfilt, vfilt], operation='and', finvert=True)

        # Boundary filters - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])
        if expflag and darr.size > 0:
            rfilt = (np.abs(darr[0, :]) >= 1.0)
            ufilt = (darr[2, :] > 5.0e2)
            darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD, status)


def filter_ti(diagdata, newstruct=None, expflag=True, edgeflag=False, hfsflag=False, ampflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed ion temperature profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed ion temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ampflag: bool. Flag to specify artifical amplification factor of errors, usually based on wall material.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "TI"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # TI data can be corrupted with extremely high values
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.0e6, upper_equality=False, indices=[2])

        # Region-specific filters
        if edgeflag:
            # Edge TI sometimes corrupted with large values due to stray radiation,  filtered using reported error
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr, upper_bound=1000, upper_equality=False, indices=[3])

            # Edge TI data inside pedestal can be corrupted with small values
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) <= 0.85)
                vallim = 0.5 * np.mean(darr[2, :][rfilt]) if np.any(rfilt) else 0.0
                lfilt = (darr[2, :] < vallim)
                darr = ptools.custom_filter_2d(darr, filters=[lfilt, rfilt], operation='and', finvert=True)

            # Edge TI data at edge can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) >= 0.99)
                ufilt = (darr[2, :] > 200)
                darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

            if expflag:
                # Edge TI error at pedestal top greatly influenced by ELMs
#                if darr.size > 0:
#                    rfilt = np.all([np.abs(darr[0, :]) >= 0.8, np.abs(darr[0, :]) <= 0.9], axis=0)
#                    efilt = (darr[3, :] >= 0.25 * darr[2, :])
#                    pfilt = np.all([rfilt, efilt], axis=0)
#                    if np.any(pfilt):
#                        darr[3, :][pfilt] = 0.4 * darr[3, :][pfilt]

                # Edge TI error suspected to be underestimated, especially away from the edge
                if darr.size > 0:
                    rfilt = (darr[3, :] < 0.2 * darr[2, :])
                    if np.any(rfilt):
                        efac = 0.3 # 0.5 if ampflag else 0.3
                        darr[3, :][rfilt] = darr[3, :][rfilt] + np.abs(0.975 - darr[0, :][rfilt]) * efac * darr[2, :][rfilt]
        else:
            # Core TI data can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) <= 1.05)
                lfilt = (darr[2, :] < 50)
                darr = ptools.custom_filter_2d(darr, filters=[lfilt, rfilt], operation='and', finvert=True)

            if expflag:
                # Core TI error known to be underestimated where it overlaps with edge measurements
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] >= 0.7, darr[3, :] < 0.2 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.175 if ampflag else 0.2
                        darr[3, :][rfilt] = darr[3, :][rfilt] + efac * darr[2, :][rfilt]

                # Core TI error can be overestimated in the central core
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] < 0.6, darr[3, :] > 0.1 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.7
                        darr[3, :][rfilt] = (darr[0, :][rfilt] + 0.6) * efac * darr[3, :][rfilt]

                # Core TI error can be underestimated in the central core
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] < 0.7, darr[3, :] < 0.1 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.2
                        darr[3, :][rfilt] = darr[3, :][rfilt] + efac * darr[0, :][rfilt] * darr[2, :][rfilt]

        # Boundary filter - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD, status)


def filter_timp(diagdata, newstruct=None, expflag=True, edgeflag=False, hfsflag=False, ampflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed impurity ion temperature profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed impurity ion temperature profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg ampscale: bool. Flag to specify artifical amplification factor of errors, usually based on wall material.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "TIMP"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # TIMP data can be corrupted with extremely high values
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.0e6, upper_equality=False, indices=[2])

        # Region-specific filters
        if edgeflag:
            # Edge TIMP sometimes corrupted with large values due to stray radiation, filtered using reported error
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr, upper_bound=1000, upper_equality=False, indices=[3])

            # Edge TIMP data inside pedestal can be corrupted with small values
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) <= 0.85)
                vallim = 0.5 * np.mean(darr[2, :][rfilt]) if np.any(rfilt) else 0.0
                lfilt = (darr[2, :] < vallim)
                darr = ptools.custom_filter_2d(darr, filters=[lfilt, rfilt], operation='and', finvert=True)

            # Edge TIMP data at edge can be corrupted with large errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) >= 0.99)
                ufilt = (darr[2, :] > 200)
                darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

            if expflag:
                # Edge TIMP error at pedestal top greatly influenced by ELMs
#                if darr.size > 0:
#                    rfilt = np.all([np.abs(darr[0, :]) >= 0.8, np.abs(darr[0, :]) <= 0.95], axis=0)
#                    efilt = (darr[3, :] >= 0.25 * darr[2, :])
#                    pfilt = np.all([rfilt, efilt], axis=0)
#                    if np.any(pfilt):
#                        darr[3, :][pfilt] = 0.4 * darr[3, :][pfilt]

                # Edge TIMP error suspected to be underestimated, especially away from the edge
                if darr.size > 0:
                    rfilt = (darr[3, :] < 0.15 * darr[2, :])
                    if np.any(rfilt):
                        efac = 0.4
                        darr[3, :][rfilt] = darr[3, :][rfilt] + np.abs(0.975 - darr[0, :][rfilt]) * efac * darr[2, :][rfilt]
        else:
            # Core TIMP measurements near separatrix are typically of very poor quality
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr, upper_bound=0.9, upper_equality=True, indices=[0])

            # Core TIMP data can be corrupted with zero errors in automated pre-processing routines
            if darr.size > 0:
                rfilt = (np.abs(darr[0, :]) <= 1.05)
                lfilt = (darr[2, :] < 50)
                darr = ptools.custom_filter_2d(darr, filters=[lfilt, rfilt], operation='and', finvert=True)

            if expflag:
                # Core TIMP error known to be underestimated where it overlaps with edge measurements
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] >= 0.7, darr[3, :] < 0.2 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.175 if ampflag else 0.2
                        darr[3, :][rfilt] = darr[3, :][rfilt] + efac * darr[2, :][rfilt]

                # Core TIMP error can be overestimated in the central core
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] < 0.6, darr[3, :] > 0.125 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.7
                        darr[3, :][rfilt] = (darr[0, :][rfilt] + 0.6) * efac * darr[3, :][rfilt]

                # Core TIMP error can be underestimated in the central core
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] < 0.7, darr[3, :] < 0.1 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.2
                        darr[3, :][rfilt] = darr[3, :][rfilt] + efac * np.power(darr[0, :][rfilt], 2.0) * darr[2, :][rfilt]

        # Boundary filter - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD, status)


def filter_af(diagdata, newstruct=None, expflag=True, edgeflag=False, hfsflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed rotational angular frequency profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed rotational angular frequency profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "AF"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Artificially augment errors that seem unreasonably small
        if darr.size > 0:
            rfilt = (darr[3, :] < 0.05 * np.abs(darr[2, :]))
            if np.any(rfilt):
                darr[3, :][rfilt] = darr[3, :][rfilt] + 0.05 * np.abs(darr[2, :][rfilt])

        # Region-specific filters
        if edgeflag:
            # Edge AF data away from the edge tend to be untrustworthy
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr, lower_bound=0.8, lower_equality=True, indices=[0])
        else:
            status = 0       # Dummy operation just to conserve structure

        # Boundary filter - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            otag = "E" + dtag if edgeflag else dtag
            SD = add_filtered_profile_data(otag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 2 if edgeflag else 1

    return (SD, status)


def filter_nimp(diagdata, newstruct=None, nbound=None, zmod=None, expflag=True, edgeflag=False, hfsflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed impurity ion density profiles,
    accounting for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed impurity ion density profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg nbound: float. Upper density limit to be used in filter, recommended to set to maximum electron density.

    :kwarg zmod: float. Modified average charge of impurity ion in plasma, used for adjusting lower bound in filtering, calculate as (Zeff - 1) / (Z**2 - Z)

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg edgeflag: bool. Flag to specify that input profile belongs to an edge-specific diagnostic.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    nm = 1.0e20
    zz = 10.0
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(nbound, (float, int)) and float(nbound) > 0.0:
        nm = float(nbound)
    if isinstance(zmod, (float, int)) and float(zmod) >= 1.0:
        zz = float(zmod)
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "NIMP"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)
    avec = DD["A"] if "A" in DD and len(DD["A"]) == len(rho) else None
    zvec = DD["Z"] if "Z" in DD and len(DD["Z"]) == len(rho) else None

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if avec is not None:
            darr = np.vstack((darr, avec))
        if zvec is not None:
            darr = np.vstack((darr, zvec))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Region-specific filters
        if edgeflag:
            # Edge NIMP data inside core region is typically untrustworthy due to high noise levels
            if darr.size > 0:
                darr = ptools.bounded_filter_2d(darr, lower_bound=0.8, upper_bound=1.0, lower_equality=False, upper_equality=True, indices=[0])

            # Edge NIMP data can be corrupted by zeros, filtered using flexible limit based on impurity charge
            if darr.size > 0:
                lb = 1.0e17 * zz
                #lb = 1.0e17 / zz
                darr = ptools.bounded_filter_2d(darr, lower_bound=lb, lower_equality=True, indices=[2])
        else:
            # Core NIMP data can contain unreasonably large values, filtered using flexible limit based on impurity charge
            if darr.size > 0:
                ub = nm * zz
                #ub = 1.0e18 / zz
                rfilt = (np.abs(darr[0, :]) > 0.8)
                ufilt = (darr[2, :] >= ub)
                darr = ptools.custom_filter_2d(darr, filters=[ufilt, rfilt], operation='and', finvert=True)

            if expflag:
                # Central core NIMP data can contain large values from neoclassical transport, hard to account for in fitting
                if darr.size > 0:
                    darr = ptools.bounded_filter_2d(darr, lower_bound=0.05, lower_equality=True, indices=[0])

                # Core NIMP error can be underestimated in the central core
                if darr.size > 0:
                    rfilt = np.all([darr[0, :] < 0.8, darr[3, :] < 0.1 * darr[2, :]], axis=0)
                    if np.any(rfilt):
                        efac = 0.05
                        darr[3, :][rfilt] = darr[3, :][rfilt] + efac * darr[2, :][rfilt]

        # Error adjustment - too small error bars yields unreasonable profiles or causes numerical instability in GP fit
        if darr.size > 0:
            elb = 3.0e17 * zz
            #elb = 3.0e17 / zz
            lfilt = (darr[3, :] < elb)
            if np.any(lfilt):
                darr[3, :][lfilt] = darr[3, :][lfilt] + elb

        # Error adjustment - too large error bars are generated by previous step for low density, low-Z impurities
        if darr.size > 0:
            lfilt = (darr[3, :] > 0.3 * darr[2, :])
            if np.any(lfilt):
                darr[3, :][lfilt] = 0.3 * darr[2, :][lfilt]

        # Boundary filter - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            SD = add_filtered_profile_data(dtag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)
            rownum = 4
            if avec is not None:
                SD["AIMP"] = darr[rownum, :].flatten()
                rownum += 1
            if zvec is not None:
                SD["ZIMP"] = darr[rownum, :].flatten()
                rownum += 1

        status = 2 if edgeflag else 1

    return (SD, status)


def filter_q(diagdata, newstruct=None, expflag=True, conflag=False, hfsflag=False, corrflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed safety factor profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg conflag: bool. Flag to specify that input profile comes from a constrained equilibrium calculation.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg corrflag: bool. Flag to specify that input profile is corrected data, with the bias correction applied.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "Q" #if not corrflag else "CQ"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Addition of heuristic Q error if it came from a constrained equilibrium or is unreasonably small
        if darr.size > 0:
            if conflag or np.average(darr[3, :]) <= 0.05:
                darr[3, :] = darr[3, :] + 0.05

        # Boundary filter - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.00, upper_equality=True, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            SD = add_filtered_profile_data(dtag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 1

    return (SD, status)


def filter_zeff(diagdata, newstruct=None, expflag=True, hfsflag=False, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed effective charge profiles, accounting
    for the remaining implementation-specific variables.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg hfsflag: bool. Flag to specify that input profile was measured from the inner half of the magnetic axis.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    elif isinstance(userscale, np.ndarray) and len(userscale) > 0 and float(userscale[0]) > 0.0:
        sc = float(userscale[0])
    dtag = "ZEFF"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=hfsflag)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Boundary filter - removes unnecessary data which slows down fit routine
        if expflag and darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.01, upper_equality=False, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            SD = add_filtered_profile_data(dtag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 1

    return (SD, status)


def filter_standardized_diagnostic_data(diagdata, newstruct=None, userscales=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Applies pre-defined filters to the raw diagnostic data, optimized
    such to improve the quality of the fitting routine.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg userscales: dict. Optional object containing user-defined error multipliers for the various quantities.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    DD = None
    SD = None
    MD = dict()
    if isinstance(diagdata, dict):
        DD = diagdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscales, dict):
        MD = userscales
    fe = 0

    SD["NEDIAG"] = None
    SD["NIDIAG"] = None
    SD["TEDIAG"] = None
    SD["TIDIAG"] = None
    SD["AFDIAG"] = None
    SD["TIMPDIAG"] = None
    SD["QDIAG"] = None
    SD["ZEFFDIAG"] = None
    SD["NIMPLIST"] = []

    SD["ETIDIAG"] = None
    SD["EAFDIAG"] = None
    SD["ETIMPDIAG"] = None
    SD["ENIMPLIST"] = []

    SD["NEEBMULT"] = float(MD["NE"]) if "NE" in MD and isinstance(MD["NE"], (float, int)) else None
    SD["TEEBMULT"] = float(MD["TE"]) if "TE" in MD and isinstance(MD["TE"], (float, int)) else None
    SD["NIEBMULT"] = float(MD["NI"]) if "NI" in MD and isinstance(MD["NI"], (float, int)) else None
    SD["TIEBMULT"] = float(MD["TI"]) if "TI" in MD and isinstance(MD["TI"], (float, int)) else None
    SD["NIMPEBMULT"] = float(MD["NIMP"]) if "NIMP" in MD and isinstance(MD["NIMP"], (float, int)) else None
    SD["TIMPEBMULT"] = float(MD["TIMP"]) if "TIMP" in MD and isinstance(MD["TIMP"], (float, int)) else None
    SD["AFEBMULT"] = float(MD["AF"]) if "AF" in MD and isinstance(MD["AF"], (float, int)) else None
    SD["QEBMULT"] = float(MD["Q"]) if "Q" in MD and isinstance(MD["Q"], (float, int)) else None
    SD["ZEFFEBMULT"] = float(MD["ZEFF"]) if "ZEFF" in MD and isinstance(MD["ZEFF"], (float, int)) else None

    #TODO: These should have the RAW tag as well
    SD["NEDS"] = None
    SD["NIDS"] = None
    SD["TEDS"] = None
    SD["TIDS"] = None
    SD["AFDS"] = None
    SD["TIMPDS"] = None
    SD["QDS"] = None
    SD["ZEFFDS"] = None
    SD["NIMPDSLIST"] = []
    SD["ZIMPDSLIST"] = []
    SD["ETIDS"] = None
    SD["EAFDS"] = None
    SD["ETIMPDS"] = None
    SD["ENIMPDSLIST"] = []
#    SD["EZIMPDSLIST"] = []

    if DD is not None:
        SD["CS_DIAG"] = "RHOPOLN" if DD["POLFLAG"] else "RHOTORN"
        ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False
        ef = True if "EXPFILTFLAG" in SD and SD["EXPFILTFLAG"] and not ("PURERAWFLAG" in SD and SD["PURERAWFLAG"]) else False

        nmax = DD["NEMAX"] if "NEMAX" in DD else 5.0e19
        zeff = DD["ZEFF"] if "ZEFF" in DD else 1.5

        diag = "HRTS"
        if diag in DD and DD[diag] is not None and fe == 0:
            nstatus = 0
            tstatus = 0
            if "NE" in DD[diag] and DD[diag]["NE"] is not None:
                (SD, nstatus) = filter_ne(DD[diag]["NE"], newstruct=SD, expflag=ef, ftflag=ftflag, userscale=SD["NEEBMULT"])
                if nstatus > 0 and "NERAW" in SD and SD["NERAW"].size > 0:
                    if SD["NEDIAG"] is None:
                        SD["NEDIAG"] = []
                    SD["NEDIAG"].append(diag)
                    SD["NEDS"] = SD["NEDS"] + "; " + DD[diag]["NE"]["PROV"] if SD["NEDS"] is not None else DD[diag]["NE"]["PROV"]
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD, tstatus) = filter_te(DD[diag]["TE"], newstruct=SD, expflag=ef, ftflag=ftflag, userscale=SD["TEEBMULT"])
                if tstatus > 0 and "TERAW" in SD and SD["TERAW"].size > 0 and np.mean(SD["TERAW"]) > 100:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)
                    SD["TEDS"] = SD["TEDS"] + "; " + DD[diag]["TE"]["PROV"] if SD["TEDS"] is not None else DD[diag]["TE"]["PROV"]

        diag = "LIDR"
        if diag in DD and DD[diag] is not None and fe == 0:
            nstatus = 0
            tstatus = 0
            if "NE" in DD[diag] and DD[diag]["NE"] is not None:
                (SD, nstatus) = filter_ne(DD[diag]["NE"], newstruct=SD, expflag=ef, ftflag=ftflag, userscale=SD["NEEBMULT"])
                if nstatus != 0:
                    if SD["NEDIAG"] is None:
                        SD["NEDIAG"] = []
                    SD["NEDIAG"].append(diag)
                    SD["NEDS"] = SD["NEDS"] + "; " + DD[diag]["NE"]["PROV"] if SD["NEDS"] is not None else DD[diag]["NE"]["PROV"]
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD, tstatus) = filter_te(DD[diag]["TE"], newstruct=SD, expflag=ef, ftflag=ftflag, userscale=SD["TEEBMULT"])
                if tstatus != 0:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)
                    SD["TEDS"] = SD["TEDS"] + "; " + DD[diag]["TE"]["PROV"] if SD["TEDS"] is not None else DD[diag]["TE"]["PROV"]

        diag = "KG10"
        if diag in DD and DD[diag] is not None and fe == 0:
            nstatus = 0
            if "NE" in DD[diag] and DD[diag]["NE"] is not None:
                (SD, nstatus) = filter_ne(DD[diag]["NE"], newstruct=SD, expflag=ef, ftflag=ftflag, userscale=SD["NEEBMULT"])
                if nstatus != 0:
                    if SD["NEDIAG"] is None:
                        SD["NEDIAG"] = []
                    SD["NEDIAG"].append(diag)
                    SD["NEDS"] = SD["NEDS"] + "; " + DD[diag]["NE"]["PROV"] if SD["NEDS"] is not None else DD[diag]["NE"]["PROV"]

        diag = "ECR"
        if diag in DD and DD[diag] is not None and fe == 0:
            tstatus = 0
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD, tstatus) = filter_te(DD[diag]["TE"], newstruct=SD, expflag=ef, ftflag=ftflag, trustflag=True, userscale=SD["TEEBMULT"])
                if tstatus != 0:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)
                    SD["TEDS"] = SD["TEDS"] + "; " + DD[diag]["TE"]["PROV"] if SD["TEDS"] is not None else DD[diag]["TE"]["PROV"]

        diag = "ECM"
        if diag in DD and DD[diag] is not None and fe == 0:
            tstatus = 0
            if "TE" in DD[diag] and DD[diag]["TE"] is not None:
                (SD, tstatus) = filter_te(DD[diag]["TE"], newstruct=SD, expflag=ef, ftflag=ftflag, trustflag=True, userscale=SD["TEEBMULT"])
                if tstatus != 0:
                    if SD["TEDIAG"] is None:
                        SD["TEDIAG"] = []
                    SD["TEDIAG"].append(diag)
                    SD["TEDS"] = SD["TEDS"] + "; " + DD[diag]["TE"]["PROV"] if SD["TEDS"] is not None else DD[diag]["TE"]["PROV"]

        diag = "WSXP"
        sximp = False
        if diag in DD and DD[diag] is not None:
            tstatus = 0
            if "ZEFF" in DD[diag] and DD[diag]["ZEFF"] is not None:
                (SD, zstatus) = filter_zeff(DD[diag]["ZEFF"], newstruct=SD, expflag=ef, userscale=SD["ZEFFEBMULT"])
                if zstatus != 0:
                    if SD["ZEFFDIAG"] is None:
                        SD["ZEFFDIAG"] = []
                    SD["ZEFFDIAG"].append(diag)
                    SD["ZEFFDS"] = SD["ZEFFDS"] + "; " + DD[diag]["ZEFF"]["PROV"] if SD["ZEFFDS"] is not None else DD[diag]["ZEFF"]["PROV"]
            # Put priority on WSXP impurity profiles
            if "NIMPL" in DD[diag] and DD[diag]["NIMPL"] is not None:
                zimp = np.mean(DD[diag]["NIMPL"]["Z"])
                zmod = (zeff - 1.0) / (zimp**2.0 - zimp)
                (ND, nstatus) = filter_nimp(DD[diag]["NIMPL"], newstruct=None, nbound=nmax, zmod=zmod, expflag=ef, userscale=SD["NIMPEBMULT"])
                if nstatus != 0:
                    zprov = DD[diag]["NIMPL"]["ZPROV"] if "ZIMP" in ND and "ZPROV" in DD[diag]["NIMPL"] else None
                    if "AIMP" not in ND:
                        ND["AIMP"] = DD[diag]["NIMPL"]["A"].copy()
                    if "ZIMP" not in ND:
                        ND["ZIMP"] = DD[diag]["NIMPL"]["Z"].copy()
                    if "MATIMP" not in ND:
                        ND["MATIMP"] = DD[diag]["NIMPL"]["MAT"]
                    ND["NIMPDIAG"] = []
                    ND["NIMPDIAG"].append(diag)
                    SD["NIMPLIST"].append(ND)
                    SD["NIMPDSLIST"].append(DD[diag]["NIMPL"]["PROV"])
                    SD["ZIMPDSLIST"].append(zprov)
                    sximp = True
            if "NIMPM" in DD[diag] and DD[diag]["NIMPM"] is not None:
                zimp = np.mean(DD[diag]["NIMPM"]["Z"])
                zmod = (zeff - 1.0) / (zimp**2.0 - zimp)
                (ND, nstatus) = filter_nimp(DD[diag]["NIMPM"], newstruct=None, nbound=nmax, zmod=zmod, expflag=ef, userscale=SD["NIMPEBMULT"])
                if nstatus != 0:
                    zprov = DD[diag]["NIMPM"]["ZPROV"] if "ZIMP" in ND and "ZPROV" in DD[diag]["NIMPM"] else None
                    if "AIMP" not in ND:
                        ND["AIMP"] = DD[diag]["NIMPM"]["A"].copy()
                    if "ZIMP" not in ND:
                        ND["ZIMP"] = DD[diag]["NIMPM"]["Z"].copy()
                    if "MATIMP" not in ND:
                        ND["MATIMP"] = DD[diag]["NIMPM"]["MAT"]
                    ND["NIMPDIAG"] = []
                    ND["NIMPDIAG"].append(diag)
                    SD["NIMPLIST"].append(ND)
                    SD["NIMPDSLIST"].append(DD[diag]["NIMPM"]["PROV"])
                    SD["ZIMPDSLIST"].append(zprov)
                    sximp = True
            if "NIMPH" in DD[diag] and DD[diag]["NIMPH"] is not None:
                zimp = np.mean(DD[diag]["NIMPH"]["Z"])
                zmod = (zeff - 1.0) / (zimp**2.0 - zimp)
                (ND, nstatus) = filter_nimp(DD[diag]["NIMPH"], newstruct=None, nbound=nmax, zmod=zmod, expflag=ef, userscale=SD["NIMPEBMULT"])
                if nstatus != 0:
                    zprov = DD[diag]["NIMPH"]["ZPROV"] if "ZIMP" in ND and "ZPROV" in DD[diag]["NIMPH"] else None
                    if "AIMP" not in ND:
                        ND["AIMP"] = DD[diag]["NIMPH"]["A"].copy()
                    if "ZIMP" not in ND:
                        ND["ZIMP"] = DD[diag]["NIMPH"]["Z"].copy()
                    if "MATIMP" not in ND:
                        ND["MATIMP"] = DD[diag]["NIMPH"]["MAT"]
                    ND["NIMPDIAG"] = []
                    ND["NIMPDIAG"].append(diag)
                    SD["NIMPLIST"].append(ND)
                    SD["NIMPDSLIST"].append(DD[diag]["NIMPH"]["PROV"])
                    SD["ZIMPDSLIST"].append(zprov)
                    sximp = True

        diag = "CX"
        if diag in DD and DD[diag] is not None:
            edgetihere = False
            for key in DD[diag]:
                if re.match(r'^((CX7[ABCD])|(CXSE))$', key, flags=re.IGNORECASE):
                    if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None and np.abs(DD[diag][key]["Z"] - 1.0) < 0.1:
                        if "TI" in DD[diag][key] and DD[diag][key]["TI"] is not None:
                            (SD, tstatus) = filter_ti(DD[diag][key]["TI"], newstruct=SD, expflag=ef, edgeflag=True, userscale=SD["TIEBMULT"])
                            if tstatus != 0:
                                if SD["ETIDIAG"] is None:
                                    SD["ETIDIAG"] = []
                                SD["ETIDIAG"].append(key.upper())
                                SD["ETIDS"] = SD["ETIDS"] + "; " + DD[diag][key]["TI"]["PROV"] if SD["ETIDS"] is not None else DD[diag][key]["TI"]["PROV"]
                            edgetihere = True
                        if "AF" in DD[diag][key] and DD[diag][key]["AF"] is not None:
                            (SD, fstatus) = filter_af(DD[diag][key]["AF"], newstruct=SD, expflag=ef, edgeflag=True, userscale=SD["AFEBMULT"])
                            if fstatus != 0:
                                if SD["EAFDIAG"] is None:
                                    SD["EAFDIAG"] = []
                                SD["EAFDIAG"].append(key.upper())
                                SD["EAFDS"] = SD["EAFDS"] + "; " + DD[diag][key]["AF"]["PROV"] if SD["EAFDS"] is not None else DD[diag][key]["AF"]["PROV"]
                    else:
                        if "TI" in DD[diag][key] and DD[diag][key]["TI"] is not None:
                            fcw = False if "WALLMAT" in SD and re.match(r'^C$', SD["WALLMAT"], flags=re.IGNORECASE) else True
                            (SD, tstatus) = filter_timp(DD[diag][key]["TI"], newstruct=SD, expflag=ef, edgeflag=True, ampflag=fcw, userscale=SD["TIMPEBMULT"])
                            if tstatus != 0:
                                if SD["ETIMPDIAG"] is None:
                                    SD["ETIMPDIAG"] = []
                                SD["ETIMPDIAG"].append(key.upper())
                                SD["ETIMPDS"] = SD["ETIMPDS"] + "; " + DD[diag][key]["TI"]["PROV"] if SD["ETIMPDS"] is not None else DD[diag][key]["TI"]["PROV"]
                        if "AF" in DD[diag][key] and DD[diag][key]["AF"] is not None:
                            (SD, fstatus) = filter_af(DD[diag][key]["AF"], newstruct=SD, expflag=ef, edgeflag=True, userscale=SD["AFEBMULT"])
                            if fstatus != 0:
                                if SD["EAFDIAG"] is None:
                                    SD["EAFDIAG"] = []
                                SD["EAFDIAG"].append(key.upper())
                                SD["EAFDS"] = SD["EAFDS"] + "; " + DD[diag][key]["AF"]["PROV"] if SD["EAFDS"] is not None else DD[diag][key]["AF"]["PROV"]
                        if "NIMP" in DD[diag][key] and DD[diag][key]["NIMP"] is not None and not sximp:
                            idx = len(SD["ENIMPLIST"])
                            if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None:
                                for jj in range(len(SD["ENIMPLIST"])):
                                    if np.abs(SD["ENIMPLIST"][jj]["ZIMP"] - DD[diag][key]["Z"]) < 0.01:
                                        idx = jj
                            temp = None if idx >= len(SD["ENIMPLIST"]) else copy.deepcopy(SD["ENIMPLIST"][idx])
                            zimp = DD[diag][key]["Z"]
                            zmod = (zeff - 1.0) / (zimp**2.0 - zimp)
                            (ND, nstatus) = filter_nimp(DD[diag][key]["NIMP"], newstruct=temp, nbound=nmax, zmod=zmod, expflag=ef, edgeflag=True, userscale=SD["NIMPEBMULT"])
                            if nstatus != 0:
                                if "AIMP" not in ND:
                                    ND["AIMP"] = DD[diag][key]["A"].copy()
                                if "ZIMP" not in ND:
                                    ND["ZIMP"] = DD[diag][key]["Z"].copy()
                                if idx >= len(SD["ENIMPLIST"]):
                                    ND["NIMPDIAG"] = []
                                    ND["NIMPDIAG"].append(key.upper())
                                    SD["ENIMPLIST"].append(ND)
                                    SD["ENIMPDSLIST"].append(DD[diag][key]["NIMP"]["PROV"])
                                else:
                                    ND["NIMPDIAG"] = SD["ENIMPLIST"][idx]["NIMPDIAG"]
                                    ND["NIMPDIAG"].append(key.upper())
                                    SD["ENIMPLIST"][idx] = copy.deepcopy(ND)
                                    SD["ENIMPDSLIST"][idx] = SD["ENIMPDSLIST"][idx] + "; " + DD[diag][key]["NIMP"]["PROV"]
            for key in DD[diag]:
                if not re.match(r'^((CX7[ABCD])|(CXSE))$', key, flags=re.IGNORECASE):
                    if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None and np.abs(DD[diag][key]["Z"] - 1.0) < 0.1:
                        if "TI" in DD[diag][key] and DD[diag][key]["TI"] is not None:
                            (SD, tstatus) = filter_ti(DD[diag][key]["TI"], newstruct=SD, expflag=ef, userscale=SD["TIEBMULT"])
                            if tstatus != 0:
                                if SD["TIDIAG"] is None:
                                    SD["TIDIAG"] = []
                                SD["TIDIAG"].append(key.upper())
                                SD["TIDS"] = SD["TIDS"] + "; " + DD[diag][key]["TI"]["PROV"] if SD["TIDS"] is not None else DD[diag][key]["TI"]["PROV"]
                        if "AF" in DD[diag][key] and DD[diag][key]["AF"] is not None:
                            (SD, fstatus) = filter_af(DD[diag][key]["AF"], newstruct=SD, expflag=ef, userscale=SD["AFEBMULT"])
                            if fstatus != 0:
                                if SD["AFDIAG"] is None:
                                    SD["AFDIAG"] = []
                                SD["AFDIAG"].append(key.upper())
                                SD["AFDS"] = SD["AFDS"] + "; " + DD[diag][key]["AF"]["PROV"] if SD["AFDS"] is not None else DD[diag][key]["AF"]["PROV"]
                    else:
                        if "TI" in DD[diag][key] and DD[diag][key]["TI"] is not None:
                            fcw = False if "WALLMAT" in SD and re.match(r'^C$', SD["WALLMAT"], flags=re.IGNORECASE) else True
                            (SD, tstatus) = filter_timp(DD[diag][key]["TI"], newstruct=SD, expflag=ef, ampflag=fcw, userscale=SD["TIMPEBMULT"])
                            if tstatus != 0:
                                if SD["TIMPDIAG"] is None:
                                    SD["TIMPDIAG"] = []
                                SD["TIMPDIAG"].append(key.upper())
                                SD["TIMPDS"] = SD["TIMPDS"] + "; " + DD[diag][key]["TI"]["PROV"] if SD["TIMPDS"] is not None else DD[diag][key]["TI"]["PROV"]
                        if "AF" in DD[diag][key] and DD[diag][key]["AF"] is not None:
                            (SD, fstatus) = filter_af(DD[diag][key]["AF"], newstruct=SD, expflag=ef, userscale=SD["AFEBMULT"])
                            if fstatus != 0:
                                if SD["AFDIAG"] is None:
                                    SD["AFDIAG"] = []
                                SD["AFDIAG"].append(key.upper())
                                SD["AFDS"] = SD["AFDS"] + "; " + DD[diag][key]["AF"]["PROV"] if SD["AFDS"] is not None else DD[diag][key]["AF"]["PROV"]
                        if "NIMP" in DD[diag][key] and DD[diag][key]["NIMP"] is not None and not sximp:
                            idx = len(SD["NIMPLIST"])
                            if "Z" in DD[diag][key] and DD[diag][key]["Z"] is not None:
                                for jj in range(len(SD["NIMPLIST"])):
                                    if np.abs(SD["NIMPLIST"][jj]["ZIMP"] - DD[diag][key]["Z"]) < 0.01:
                                        idx = jj
                            temp = None if idx >= len(SD["NIMPLIST"]) else copy.deepcopy(SD["NIMPLIST"][idx])
                            zimp = DD[diag][key]["Z"]
                            zmod = (zeff - 1.0) / (zimp**2.0 - zimp)
                            (ND, nstatus) = filter_nimp(DD[diag][key]["NIMP"], newstruct=temp, nbound=nmax, zmod=zmod, expflag=ef, userscale=SD["NIMPEBMULT"])
                            if nstatus != 0:
                                if "AIMP" not in ND:
                                    ND["AIMP"] = DD[diag][key]["A"].copy()
                                if "ZIMP" not in ND:
                                    ND["ZIMP"] = DD[diag][key]["Z"].copy()
                                if idx >= len(SD["NIMPLIST"]):
                                    ND["NIMPDIAG"] = []
                                    ND["NIMPDIAG"].append(key.upper())
                                    SD["NIMPLIST"].append(ND)
                                    SD["NIMPDSLIST"].append(DD[diag][key]["NIMP"]["PROV"])
                                else:
                                    ND["NIMPDIAG"] = SD["NIMPLIST"][idx]["NIMPDIAG"]
                                    ND["NIMPDIAG"].append(key.upper())
                                    SD["NIMPLIST"][idx] = copy.deepcopy(ND)
                                    SD["NIMPDSLIST"][idx] = SD["NIMPDSLIST"][idx] + "; " + DD[diag][key]["NIMP"]["PROV"]

        diag = "MAGN"
        if diag in DD and DD[diag] is not None:
            if "Q" in DD[diag] and DD[diag]["Q"] is not None:
                cflag = True if not re.match(r'^EFIT$', DD[diag]["Q"]["EQTYPE"], flags=re.IGNORECASE) else False
                (SD, qstatus) = filter_q(DD[diag]["Q"], newstruct=SD, expflag=ef, conflag=cflag, userscale=SD["QEBMULT"])
                if qstatus != 0:
                    if SD["QDIAG"] is None:
                        SD["QDIAG"] = []
                    SD["QDIAG"].append(DD[diag]["Q"]["EQTYPE"].upper())
                    SD["QDS"] = SD["QDS"] + "; " + DD[diag]["Q"]["PROV"] if SD["QDS"] is not None else DD[diag]["Q"]["PROV"]
#            if "CQ" in DD[diag] and DD[diag]["CQ"] is not None:
#                print(DD[diag]["CQ"], DD[diag]["CQ"]["EQTYPE"])
#                cflag = False
#                (SD, qstatus) = filter_q(DD[diag]["CQ"], newstruct=SD, expflag=ef, conflag=cflag, corrflag=True, userscale=SD["QEBMULT"])
#                if qstatus != 0:
#                    if "CQDIAG" not in SD or SD["CQDIAG"] is None:
#                        SD["CQDIAG"] = []
#                    SD["CQDIAG"].append(DD[diag]["CQ"]["EQTYPE"].upper())

            if "QDIAG" in SD and SD["QDIAG"] is not None and len(SD["QDIAG"]) > 0:
                SD["IQRAWX"] = copy.deepcopy(SD["QRAWX"])
                SD["IQRAWXEB"] = copy.deepcopy(SD["QRAWXEB"])
                SD["IQRAW"] = 1.0 / SD["QRAW"]
                SD["IQRAWEB"] = SD["QRAWEB"] / np.power(SD["QRAW"], 2.0)
                SD["IQDIAG"] = copy.deepcopy(SD["QDIAG"])
                SD["IQDMAP"] = copy.deepcopy(SD["QDMAP"])
                SD["IQDS"] = SD["IQDS"] + "; " + DD[diag]["Q"]["PROV"] if "IQDS" in SD and SD["IQDS"] is not None else DD[diag]["Q"]["PROV"]
#            if "CQDIAG" in SD and SD["CQDIAG"] is not None and len(SD["CQDIAG"]) > 0:
#                SD["ICQRAWX"] = copy.deepcopy(SD["CQRAWX"])
#                SD["ICQRAWXEB"] = copy.deepcopy(SD["CQRAWXEB"])
#                SD["ICQRAW"] = 1.0 / SD["CQRAW"]
#                SD["ICQRAWEB"] = SD["CQRAWEB"] / np.power(SD["CQRAW"], 2.0)
#                SD["ICQDIAG"] = copy.deepcopy(SD["CQDIAG"])
#                SD["ICQDMAP"] = copy.deepcopy(SD["CQDMAP"])

        if fdebug:
            fields = ["NEDIAG", "NIDIAG", "TEDIAG", "TIDIAG", "AFDIAG", "TIMPDIAG", "NIMPDIAG", "QDIAG", "ETIDIAG", "EAFDIAG", "ETIMPDIAG", "ENIMPDIAG"]
            for key in fields:
                if key in SD and SD[key] is not None:
                    cstr = ""
                    for diag in SD[key]:
                        cstr = cstr + ", " + diag.upper() if cstr else cstr + diag.upper()
                    if cstr:
                        qlabel = key[:-4] if not key.startswith("E") else key[1:-4]
                        region = "core" if not key.startswith("E") else "edge"
                        print("standardize_jet.py: filter_standardized_diagnostic_data(): %s data sorted into %s %s field." % (cstr, region, qlabel))
            for field in ["NIMPLIST", "ENIMPLIST"]:
                if field in SD and SD[field] is not None:
                    cstr = ""
                    for item in SD[field]:
                        for key in fields:
                            if key in item:
                                cstr = ""
                                for diag in item[key]:
                                    cstr = cstr + ", " + diag.upper() if cstr else cstr + diag.upper()
                    if cstr:
                        qlabel = field[:-4] if not field.startswith("E") else field[1:-4]
                        region = "core" if not field.startswith("E") else "edge"
                        print("standardize_jet.py: filter_standardized_diagnostic_data(): %s data sorted into %s %s field." % (cstr, region, qlabel))

        # RAWDIAGFLAG option not documented as is unsupported - keeps filtered diagnostic data in output dict if True
        if not ("RAWDIAGFLAG" in DD and DD["RAWDIAGFLAG"]):
            dlist = ["HRTS", "LIDR", "ECM", "CX", "MAGN"]
            SD = ptools.delete_fields_from_dict(SD, fields=dlist)

    if fdebug:
        print("standardize_jet.py: filter_standardized_diagnostic_data() completed.")

    return SD


def combine_edge_diagnostic_data(stddata, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Combines any edge and core diagnostic profile data of the same quantity,
    applying any additional checks and cross-diagnostic filters.

    :arg stddata: dict. Standardized object containing separated core and edge profiles.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    SD = None
    if isinstance(stddata, dict):
        SD = stddata

    if SD is not None:

        if "ETIRAW" in SD and SD["ETIRAW"].size > 0:
            if "TIRAW" not in SD:
                # Pasting of TE core data segment yields better estimation of TI profile if only edge data exists
                emptyte = True
                if "TERAW" in SD and SD["TERAW"].size > 0:
                    cfilt = np.all([SD["TERAWX"] >= 0.1, SD["TERAWX"] <= 0.6], axis=0)
                    if np.any(cfilt):
                        emptyte = False
                        SD["TIRAWX"] = SD["TERAWX"][cfilt][::2]
                        SD["TIRAWXEB"] = SD["TERAWXEB"][cfilt][::2]
                        SD["TIRAW"] = SD["TERAW"][cfilt][::2]
                        SD["TIRAWEB"] = (1.6 - SD["TERAWX"][cfilt][::2]) * SD["TERAWEB"][cfilt][::2]
                        SD["TIDIAG"] = []
                        SD["TIDMAP"] = np.full(SD["TIRAWX"].shape, len(SD["TIDIAG"])).flatten()
                        SD["TIDIAG"].append("TICC")
                        SD["TIDS"] = SD["TIDS"] + "; " + SD["TEDS"] if SD["TIDS"] is not None else SD["TEDS"]
                if emptyte:
                    SD["TIRAW"] = np.array([])
                    SD["TIRAWEB"] = np.array([])
                    SD["TIRAWX"] = np.array([])
                    SD["TIRAWXEB"] = np.array([])
                    SD["TIDMAP"] = np.array([])
                    SD["TIDIAG"] = []
                    SD["TIDS"] = None
            SD["TIRAW"] = np.hstack((SD["TIRAW"], SD["ETIRAW"]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"], SD["ETIRAWEB"]))
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"], SD["ETIRAWX"]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"], SD["ETIRAWXEB"]))
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"], SD["ETIDMAP"] + len(SD["TIDIAG"])))
            SD["TIDIAG"].extend(SD["ETIDIAG"])
            SD["TIDS"] = SD["TIDS"] + "; " + SD["ETIDS"] if SD["TIDS"] is not None else SD["ETIDS"]

        if "ETIMPRAW" in SD and SD["ETIMPRAW"].size > 0:
            if "TIMPRAW" not in SD:
                # Pasting of TE core data segment yields better estimation of TI profile if only edge data exists
                emptyte = True
                if "TERAW" in SD and SD["TERAW"].size > 0:
                    cfilt = np.all([SD["TERAWX"] >= 0.1, SD["TERAWX"] <= 0.6], axis=0)
                    if np.any(cfilt):
                        emptyte = False
                        SD["TIMPRAWX"] = SD["TERAWX"][cfilt][::2]
                        SD["TIMPRAWXEB"] = SD["TERAWXEB"][cfilt][::2]
                        SD["TIMPRAW"] = SD["TERAW"][cfilt][::2]
                        SD["TIMPRAWEB"] = (1.6 - SD["TERAWX"][cfilt][::2]) * SD["TERAWEB"][cfilt][::2]
                        SD["TIMPDIAG"] = []
                        SD["TIMPDMAP"] = np.full(SD["TIMPRAWX"].shape, len(SD["TIMPDIAG"])).flatten()
                        SD["TIMPDIAG"].append("TZCC")
                        SD["TIMPDS"] = SD["TIMPDS"] + "; " + SD["TEDS"] if SD["TIMPDS"] is not None else SD["TEDS"]
                if emptyte:
                    SD["TIMPRAW"] = np.array([])
                    SD["TIMPRAWEB"] = np.array([])
                    SD["TIMPRAWX"] = np.array([])
                    SD["TIMPRAWXEB"] = np.array([])
                    SD["TIMPDMAP"] = np.array([])
                    SD["TIMPDIAG"] = []
                    SD["TIMPDS"] = None
            SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"], SD["ETIMPRAW"]))
            SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"], SD["ETIMPRAWEB"]))
            SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"], SD["ETIMPRAWX"]))
            SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"], SD["ETIMPRAWXEB"]))
            SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"], SD["ETIMPDMAP"] + len(SD["TIMPDIAG"])))
            SD["TIMPDIAG"].extend(SD["ETIMPDIAG"])
            SD["TIMPDS"] = SD["TIMPDS"] + "; " + SD["ETIMPDS"] if SD["TIMPDS"] is not None else SD["ETIMPDS"]

        if "EAFRAW" in SD and SD["EAFRAW"].size > 0 and "AFRAW" in SD:
            # Only include edge angular frequency measurements if they are not excessively large
            if SD["AFRAW"].size > 0 and np.sqrt(np.mean(np.power(SD["AFRAW"], 2.0))) > np.sqrt(np.mean(np.power(SD["EAFRAW"], 2.0))):
                if "AFDIAG" not in SD or SD["AFDIAG"] is None:
                    SD["AFDIAG"] = []
                SD["AFRAW"] = np.hstack((SD["AFRAW"], SD["EAFRAW"]))
                SD["AFRAWEB"] = np.hstack((SD["AFRAWEB"], SD["EAFRAWEB"]))
                SD["AFRAWX"] = np.hstack((SD["AFRAWX"], SD["EAFRAWX"]))
                SD["AFRAWXEB"] = np.hstack((SD["AFRAWXEB"], SD["EAFRAWXEB"]))
                SD["AFDMAP"] = np.hstack((SD["AFDMAP"], SD["EAFDMAP"] + len(SD["AFDIAG"])))
                SD["AFDIAG"].extend(SD["EAFDIAG"])
                SD["AFDS"] = SD["AFDS"] + "; " + SD["EAFDS"] if SD["AFDS"] is not None else SD["EAFDS"]

        for ii in range(len(SD["ENIMPLIST"])):
            if "NIMPRAW" in SD["ENIMPLIST"][ii] and SD["ENIMPLIST"][ii]["NIMPRAW"] is not None:
                for jj in range(len(SD["NIMPLIST"])):
                    if "NIMPRAW" in SD["NIMPLIST"][jj] and SD["NIMPLIST"][jj]["NIMPRAW"] is not None:
                        # Core charge exchange measurements at edge usually polluted with bad impurity measurements, edge is cleaner
                        zcore = SD["NIMPLIST"][jj]["ZIMP"][-1]
                        zedge = SD["ENIMPLIST"][ii]["ZIMP"][0]
                        if SD["NIMPLIST"][jj]["NIMPRAW"].size > 0 and np.abs(zedge - zcore) < 0.01:
                            cfilt = (SD["NIMPLIST"][jj]["NIMPRAW"] >= 0.5 * np.mean(SD["ENIMPLIST"][ii]["NIMPRAW"]))
                            SD["NIMPLIST"][jj]["NIMPRAW"] = SD["NIMPLIST"][jj]["NIMPRAW"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPRAWEB"] = SD["NIMPLIST"][jj]["NIMPRAWEB"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPRAWX"] = SD["NIMPLIST"][jj]["NIMPRAWX"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPRAWXEB"] = SD["NIMPLIST"][jj]["NIMPRAWXEB"][cfilt] if np.any(cfilt) else np.array([])
                            SD["NIMPLIST"][jj]["NIMPDMAP"] = SD["NIMPLIST"][jj]["NIMPDMAP"][cfilt] if np.any(cfilt) else np.array([])
                            if len(SD["NIMPLIST"][jj]["ZIMP"]) > 1:
                                SD["NIMPLIST"][jj]["ZIMP"] = SD["NIMPLIST"][jj]["ZIMP"][cfilt] if np.any(cfilt) else np.array([])
                            if len(SD["NIMPLIST"][jj]["AIMP"]) > 1:
                                SD["NIMPLIST"][jj]["AIMP"] = SD["NIMPLIST"][jj]["AIMP"][cfilt] if np.any(cfilt) else np.array([])

                for jj in range(len(SD["NIMPLIST"])):
                    # Edge information is only be used if corresponding core information exists, lack of core data yields no useful fits
                    if "NIMPRAW" in SD["NIMPLIST"][jj] and SD["NIMPLIST"][jj]["NIMPRAW"] is not None:
                        zcore = SD["NIMPLIST"][jj]["ZIMP"][-1]
                        zedge = SD["ENIMPLIST"][ii]["ZIMP"][0]
                        fdata = (len(SD["NIMPLIST"][jj]["NIMPRAW"]) > 0)
                        if np.abs(zedge - zcore) < 0.01:
                            SD["NIMPLIST"][jj]["NIMPRAW"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAW"], SD["ENIMPLIST"][ii]["NIMPRAW"]))
                            SD["NIMPLIST"][jj]["NIMPRAWEB"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAWEB"], SD["ENIMPLIST"][ii]["NIMPRAWEB"]))
                            SD["NIMPLIST"][jj]["NIMPRAWX"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAWX"], SD["ENIMPLIST"][ii]["NIMPRAWX"]))
                            SD["NIMPLIST"][jj]["NIMPRAWXEB"] = np.hstack((SD["NIMPLIST"][jj]["NIMPRAWXEB"], SD["ENIMPLIST"][ii]["NIMPRAWXEB"]))
                            SD["NIMPLIST"][jj]["NIMPDMAP"] = np.hstack((SD["NIMPLIST"][jj]["NIMPDMAP"], SD["ENIMPLIST"][ii]["NIMPDMAP"] + len(SD["NIMPLIST"][jj]["NIMPDIAG"])))
                            if (len(SD["NIMPLIST"][jj]["ZIMP"]) > 1 or len(SD["ENIMPLIST"][ii]["ZIMP"]) > 1) and fdata:
                                SD["NIMPLIST"][jj]["ZIMP"] = np.hstack((SD["NIMPLIST"][jj]["ZIMP"], SD["ENIMPLIST"][ii]["ZIMP"]))
                            if (len(SD["NIMPLIST"][jj]["AIMP"]) > 1 or len(SD["ENIMPLIST"][ii]["AIMP"]) > 1) and fdata:
                                SD["NIMPLIST"][jj]["AIMP"] = np.hstack((SD["NIMPLIST"][jj]["AIMP"], SD["ENIMPLIST"][ii]["AIMP"]))
                            SD["NIMPLIST"][jj]["NIMPDIAG"].extend(SD["ENIMPLIST"][ii]["NIMPDIAG"])
                            SD["NIMPDSLIST"][jj] = SD["NIMPDSLIST"][jj] + "; " + SD["ENIMPDSLIST"][ii]

        # RAWEDGEFLAG option not documented as is unsupported - keeps filtered edge diagnostic data in output dict if True
        if not ("RAWEDGEFLAG" in SD and SD["RAWEDGEFLAG"]):
            dlist = ["ETIRAW", "ETIRAWEB", "ETIRAWX", "ETIRAWXEB", "ETIDMAP", \
                     "EAFRAW", "EAFRAWEB", "EAFRAWX", "EAFRAWXEB", "EAFDMAP", \
                     "ETIMPRAW", "ETIMPRAWEB", "ETIMPRAWX", "ETIMPRAWXEB", "ETIMPDMAP", \
                     "ETIDIAG", "EAFDIAG", "ETIMPDIAG", "ENIMPLIST", \
                     "ETIDS", "EAFDS", "ETIMPDS", "ENIMPDSLIST"]
            SD = ptools.delete_fields_from_dict(SD, fields=dlist)

    if fdebug:
        print("standardize_jet.py: combine_edge_diagnostic_data() completed.")

    return SD


def add_profile_boundary_data(stddata, pastete=True, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Inserts appropriate boundary constraints on the various profiles
    at the separatrix in order improve the robustness of the GP fit
    routine and to achieve more meaningful fits.

    :arg stddata: dict. Standardized object containing the processed profiles.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Object with identical structure as input object except with profile boundary constraints added to standard fields.
    """
    SD = None
    if isinstance(stddata, dict):
        SD = stddata

    ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False
    SD["EDGETEFLAG"] = True if pastete else False

    ###  The following section adds profile boundary constraints for more robust GP fits
    if SD is not None:
#        if "NERAW" in SD and SD["NERAW"].size > 0 and np.nanmin(np.abs(SD["NERAWX"] - 1.0)) > 0.05:
#            idx = np.where((1.0 - SD["NERAWX"]) > 0.0)[0][-1]
#            eneval = 0.01 / (1.0 - SD["NERAWX"][idx]) * SD["NERAW"][idx]
#            SD["NERAWX"] = np.hstack((SD["NERAWX"], 0.99))
#            SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"], 0.0))
#            SD["NERAW"] = np.hstack((SD["NERAW"], eneval))
#            SD["NERAWEB"] = np.hstack((SD["NERAWEB"], 0.3 * eneval))

        # Define NE boundary constraint based on results of automated processing routines
        edgene = float(SD["NESEP"][0]) if "NESEP" in SD and SD["NESEP"] is not None else 0
        if edgene < 5.0e18:
            edgene = 5.0e18     # Low default value needed for ramp-up / ramp-down windows, but high enough to catch errors in automated separatrix determination routine
        if "NERAW" in SD and SD["NERAW"].size > 0:
            # 2-point NE boundary constraint based on estimated value at rho=1.0 using prior knowledge if available
            if "RMAJSHIFT" in SD and SD["RMAJSHIFT"] is not None:
                edgeneeb = 0.2 * edgene
                SD["NERAWX"] = np.hstack((SD["NERAWX"], [1.00, 1.02]))
                SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"], [0.0, 0.0]))
                SD["NERAW"] = np.hstack((SD["NERAW"], [edgene, 0.8*edgene]))
                SD["NERAWEB"] = np.hstack((SD["NERAWEB"], [edgeneeb, edgeneeb]))
                SD["NEDMAP"] = np.hstack((SD["NEDMAP"], [len(SD["NEDIAG"])] * 2))
                SD["NEDIAG"].append("NEBC")
            # 3-point NE boundary constraint based on data near rho=1.0 yields most robust results for time windows where data is sparse
            else:
                efilt = np.all([SD["NERAWX"] >= 0.9, SD["NERAWX"] <= 1.0], axis=0)
                edgeneeb = 0.5 * edgene
                if np.any(efilt):
                    nextemp = SD["NERAWX"][efilt]
                    netemp = SD["NERAW"][efilt]
                    idx = np.where(nextemp >= np.nanmax(nextemp))[0][0]
                    if netemp[idx] < 1.0e19:
                        edgene = 0.8 * netemp[idx]
                sfilt = np.all([SD["NERAWX"] >= 0.95, SD["NERAWX"] <= 1.0], axis=0)
                if np.any(sfilt):
                    neavg = np.nanmean(SD["NERAW"][sfilt])
                    neebavg = np.nanmean(SD["NERAWEB"][sfilt])
                    if edgene < 0.4 * neavg:
                        edgene = 0.4 * neavg
                        edgeneeb = np.nanmin([edgene, neebavg])
                SD["NERAWX"] = np.hstack((SD["NERAWX"], [0.98, 1.00, 1.02]))
                SD["NERAWXEB"] = np.hstack((SD["NERAWXEB"], [0.0, 0.0, 0.0]))
                SD["NERAW"] = np.hstack((SD["NERAW"], [edgene, edgene, edgene]))
                SD["NERAWEB"] = np.hstack((SD["NERAWEB"], [2.0*edgeneeb, 1.5*edgeneeb, 1.0*edgeneeb]))
                SD["NEDMAP"] = np.hstack((SD["NEDMAP"], [len(SD["NEDIAG"])] * 3))
                SD["NEDIAG"].append("NEBC")

        # Pasting of TE edge data as TI profile constraint yields better resolution of TI pedestal
        #     Shown to improve boundary condition of integrated modelling
        apply_eibc = SD["EDGETEFLAG"]
        if "TERAW" in SD and SD["TERAW"].size > 0 and "TIRAW" in SD and SD["TIRAW"].size > 3:
            edgecxlist = ["CXSE"]
            for diag in edgecxlist:
                if diag in SD["TIDIAG"]:
                    apply_eibc = False
            if apply_eibc:
                efilt = np.all([SD["TERAWX"] >= 0.85, SD["TERAWX"] <= 1.0], axis=0)
                nume = np.count_nonzero(efilt)
                SD["TIRAWX"] = np.hstack((SD["TIRAWX"], SD["TERAWX"][efilt]))
                SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"], SD["TERAWXEB"][efilt]))
                SD["TIRAW"] = np.hstack((SD["TIRAW"], SD["TERAW"][efilt]))
                SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"], SD["TERAWEB"][efilt]))
                SD["TIDMAP"] = np.hstack((SD["TIDMAP"], [len(SD["TIDIAG"])] * nume))
                SD["TIDIAG"].append("TIBC")
                if "TICC" not in SD["TIDIAG"]:
                    SD["TIDS"] = SD["TIDS"] + "; " + SD["TEDS"]

        # Pasting of TE edge data as TIMP profile constraint yields better resolution of TIMP pedestal
        #     Shown to improve boundary condition of integrated modelling
        apply_ezbc = SD["EDGETEFLAG"]
        if "TERAW" in SD and SD["TERAW"].size > 0 and "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            edgecxlist = ["CX7A", "CX7B", "CX7C", "CX7D", "CXSE"]
            for diag in edgecxlist:
                if diag in SD["TIMPDIAG"]:
                    apply_ezbc = False
            if apply_ezbc:
                efilt = np.all([SD["TERAWX"] >= 0.85, SD["TERAWX"] <= 1.0], axis=0)
                nume = np.count_nonzero(efilt)
                SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"], SD["TERAWX"][efilt]))
                SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"], SD["TERAWXEB"][efilt]))
                SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"], SD["TERAW"][efilt]))
                SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"], SD["TERAWEB"][efilt]))
                SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"], [len(SD["TIMPDIAG"])] * nume))
                SD["TIMPDIAG"].append("TZBC")
                if "TZCC" not in SD["TIMPDIAG"]:
                    SD["TIMPDS"] = SD["TIMPDS"] + "; " + SD["TEDS"]

        # Determination of TE and TI and TIMP boundary constraint based on data near rho=1.0
        edgete = float(SD["TESEP"][0]) if "TESEP" in SD and SD["TESEP"] is not None else 0
        edgeti = np.NaN
        edgeteeb = 0.3 * edgete
        edgetieb = np.NaN
        if "TERAW" in SD and SD["TERAW"].size > 0 and edgete == 0:
            efilt = np.all([SD["TERAWX"] >= 0.9, SD["TERAWX"] <= 1.0], axis=0)
            idx = np.where(SD["TERAWX"][efilt] >= np.nanmax(SD["TERAWX"][efilt]))[0][0] if np.any(efilt) else None
            edgete = 0.7 * SD["TERAW"][efilt][idx] if idx is not None and not SD["RSHIFTFLAG"] else 100
            edgeteeb = np.abs(2.0 * SD["TERAWEB"][efilt][idx]) if idx is not None else 50
        if "TIRAW" in SD and SD["TIRAW"].size > 0:
            efilt = np.all([SD["TIRAWX"] >= 0.9, SD["TIRAWX"] <= 1.0], axis=0)
            if np.count_nonzero(efilt) > 3:
                idx = np.where(SD["TIRAWX"][efilt] >= np.nanmax(SD["TIRAWX"][efilt]))[0][0] if np.any(efilt) else None
                edgeti = 0.7 * SD["TIRAW"][efilt][idx] if idx is not None else 100
                edgetieb = np.abs(3.0 * SD["TIRAWEB"][efilt][idx]) if idx is not None else 50
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            efilt = np.all([SD["TIMPRAWX"] >= 0.9, SD["TIMPRAWX"] <= 1.0], axis=0)
            if np.count_nonzero(efilt) > 3:
                idx = np.where(SD["TIMPRAWX"][efilt] >= np.nanmax(SD["TIMPRAWX"][efilt]))[0][0] if np.any(efilt) else None
                edgeti = 0.7 * SD["TIMPRAW"][efilt][idx] if idx is not None else 100
                edgetieb = np.abs(3.0 * SD["TIMPRAWEB"][efilt][idx]) if idx is not None else 50
        if np.isfinite(edgeti) and np.isfinite(edgetieb) and edgetieb > 1.0 * edgeti:
            edgetieb = 0.5 * edgeti
        edget = np.nanmax([edgete, edgeti])
        # Is this complicated criterion even necessary any more?
        if edget > 500:
            edget = 300
        elif "RMAJSHIFT" in SD and SD["RMAJSHIFT"] is not None and edget < 100:
            edget = 100
        edgeteb = np.nanmin([0.5 * edget, np.nanmax([edgeteeb, edgetieb])])

        if "TERAW" in SD and SD["TERAW"].size > 0:
            # 2-point combined TE and TI boundary constraint based on estimated value at rho=1.0 using prior knowledge if available
            if "RMAJSHIFT" in SD and SD["RMAJSHIFT"] is not None:
                SD["TERAWX"] = np.hstack((SD["TERAWX"], [1.00, 1.02]))
                SD["TERAWXEB"] = np.hstack((SD["TERAWXEB"], [0.0, 0.0]))
                SD["TERAW"] = np.hstack((SD["TERAW"], [edget, edget])) if SD["EDGETEFLAG"] else np.hstack((SD["TERAW"], [edgete, edgete]))
                SD["TERAWEB"] = np.hstack((SD["TERAWEB"], [1.25*edgeteb, 1.0*edgeteb])) if SD["EDGETEFLAG"] else np.hstack((SD["TERAWEB"], [1.25*edgeteeb, 1.0*edgeteeb]))
                SD["TEDMAP"] = np.hstack((SD["TEDMAP"], [len(SD["TEDIAG"])] * 2))
                SD["TEDIAG"].append("TEBC")
            # 3-point combined TE and TI boundary constraint yields most robust results for time windows where data is sparse
            else:
                SD["TERAWX"] = np.hstack((SD["TERAWX"], [0.98, 1.00, 1.02]))
                SD["TERAWXEB"] = np.hstack((SD["TERAWXEB"], [0.0, 0.0, 0.0]))
                SD["TERAW"] = np.hstack((SD["TERAW"], [edget, edget, edget])) if SD["EDGETEFLAG"] else np.hstack((SD["TERAW"], [edgete, edgete, edgete]))
                SD["TERAWEB"] = np.hstack((SD["TERAWEB"], [2.0*edgeteb, 1.5*edgeteb, 1.0*edgeteb])) if SD["EDGETEFLAG"] else np.hstack((SD["TERAWEB"], [2.0*edgeteeb, 1.5*edgeteeb, 1.0*edgeteeb]))
                SD["TEDMAP"] = np.hstack((SD["TEDMAP"], [len(SD["TEDIAG"])] * 3))
                SD["TEDIAG"].append("TEBC")
        if "TIRAW" in SD and SD["TIRAW"].size > 3:
            # 3-point combined TE and TI boundary constraint yields most robust results for all time windows
            SD["TIRAWX"] = np.hstack((SD["TIRAWX"], [0.98, 1.00, 1.02]))
            SD["TIRAWXEB"] = np.hstack((SD["TIRAWXEB"], [0.0, 0.0, 0.0]))
            SD["TIRAW"] = np.hstack((SD["TIRAW"], [edget, edget, edget])) if SD["EDGETEFLAG"] else np.hstack((SD["TIRAW"], [edgeti, edgeti, edgeti]))
            SD["TIRAWEB"] = np.hstack((SD["TIRAWEB"], [1.5*edgeteb, 1.25*edgeteb, 1.0*edgeteb])) if SD["EDGETEFLAG"] else np.hstack((SD["TIRAWEB"], [1.5*edgetieb, 1.25*edgetieb, 1.0*edgetieb]))
            bcidx = len(SD["TIDIAG"])
            for ii in range(len(SD["TIDIAG"])):
                if re.match('^TIBC$', SD["TIDIAG"][ii]):
                    bcidx = ii
            if bcidx >= len(SD["TIDIAG"]):
                SD["TIDIAG"].append("TIBC")
            SD["TIDMAP"] = np.hstack((SD["TIDMAP"], [bcidx, bcidx, bcidx]))
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            # 2-point combined TE and TI boundary constraint based on estimated value at rho=1.0 using prior knowledge if available
            if "RMAJSHIFT" in SD and SD["RMAJSHIFT"] is not None:
                SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"], [1.00, 1.02]))
                SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"], [0.0, 0.0]))
                SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"], [edget, edget])) if SD["EDGETEFLAG"] else np.hstack((SD["TIMPRAW"], [edgeti, edgeti]))
                SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"], [1.25*edgeteb, 1.0*edgeteb])) if SD["EDGETEFLAG"] else np.hstack((SD["TIMPRAWEB"], [1.25*edgetieb, 1.0*edgetieb]))
                bcidx = len(SD["TIMPDIAG"])
                for ii in range(len(SD["TIMPDIAG"])):
                    if re.match('^TZBC$', SD["TIMPDIAG"][ii]):
                        bcidx = ii
                if bcidx >= len(SD["TIMPDIAG"]):
                    SD["TIMPDIAG"].append("TZBC")
                SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"], [bcidx, bcidx]))
            # 3-point combined TE and TI boundary constraint yields most robust results for all time windows
            else:
                SD["TIMPRAWX"] = np.hstack((SD["TIMPRAWX"], [0.98, 1.00, 1.02]))
                SD["TIMPRAWXEB"] = np.hstack((SD["TIMPRAWXEB"], [0.0, 0.0, 0.0]))
                SD["TIMPRAW"] = np.hstack((SD["TIMPRAW"], [edget, edget, edget])) if SD["EDGETEFLAG"] else np.hstack((SD["TIMPRAW"], [edgeti, edgeti, edgeti]))
                SD["TIMPRAWEB"] = np.hstack((SD["TIMPRAWEB"], [1.5*edgeteb, 1.25*edgeteb, 1.0*edgeteb])) if SD["EDGETEFLAG"] else np.hstack((SD["TIMPRAWEB"], [1.5*edgetieb, 1.25*edgetieb, 1.0*edgetieb]))
                bcidx = len(SD["TIMPDIAG"])
                for ii in range(len(SD["TIMPDIAG"])):
                    if re.match('^TZBC$', SD["TIMPDIAG"][ii]):
                        bcidx = ii
                if bcidx >= len(SD["TIMPDIAG"]):
                    SD["TIMPDIAG"].append("TZBC")
                SD["TIMPDMAP"] = np.hstack((SD["TIMPDMAP"], [bcidx, bcidx, bcidx]))

        # 1-point AF boundary constraint based on aggregate slope yields most robust results over many plasma regimes
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
#            zfilt = np.all([SD["AFRAW"] == 0.0, SD["AFRAWEB"] <= 0.0], axis=0)
#            SD["AFRAWEB"][zfilt] = 500
            afmax = np.nanmax(np.abs(SD["AFRAW"]))
            edgeaf = 0
            edgeafeb = 0.075 * afmax
            efilt = (SD["AFRAWX"] >= 0.9)
            cfilt = np.all([SD["AFRAWX"] > 0.0, SD["AFRAWX"] <= 0.5], axis=0)
            if np.any(efilt) and np.any(cfilt):
                afe = np.mean(SD["AFRAW"][efilt])
                afc = np.mean(SD["AFRAW"][cfilt])
                if (afe * afc) < 0.0:
                    rhoe = np.mean(SD["AFRAWX"][efilt])
                    rhoc = np.mean(SD["AFRAWX"][cfilt])
                    edgeaf = 0.8 * ((1.02 - rhoe) * (afc - afe) / (rhoc - rhoe) + afe)
                    edgeafeb = np.abs(0.25 * edgeaf)
            SD["AFRAWX"] = np.hstack((SD["AFRAWX"], 1.02))
            SD["AFRAWXEB"] = np.hstack((SD["AFRAWXEB"], 0.0))
            SD["AFRAW"] = np.hstack((SD["AFRAW"], edgeaf))
            SD["AFRAWEB"] = np.hstack((SD["AFRAWEB"], edgeafeb))
            SD["AFDMAP"] = np.hstack((SD["AFDMAP"], [len(SD["AFDIAG"])]))
            SD["AFDIAG"].append("AFBC")

        # Some assumptions for the impurity density filtering - needs a more robust selection
        fzeff = 0.0
        if "ZEFV" in SD and SD["ZEFV"] is not None and (fzeff < 1.0 or SD["ZEFV"] < fzeff):
            fzeff = SD["ZEFV"]
        if "ZEFH" in SD and SD["ZEFH"] is not None and (fzeff < 1.0 or SD["ZEFH"] < fzeff):
            fzeff = SD["ZEFH"]
        if fzeff < 1.0:
            fzeff = np.array([1.25]) if re.match(r'^Be$', SD["WALLMAT"], flags=re.IGNORECASE) else np.array([2.0])
        zi = 1.0

        # 1-point NIMP boundary constraint, dependent on impurity charge, provides sufficient robustness
        idx = 0
        for ii in range(len(SD["NIMPLIST"])):
            if "NIMPRAW" in SD["NIMPLIST"][ii] and SD["NIMPLIST"][ii]["NIMPRAW"] is not None and SD["NIMPLIST"][ii]["NIMPRAW"].size > 0:
                idx = idx + 1
                itag = ("%d" % (idx))
                zavg = np.mean(SD["NIMPLIST"][ii]["ZIMP"])
                edgenimp = 0.01 * edgene * (fzeff - zi) / (zavg - fzeff)
                edgenimpeb = 0.4 * (edgenimp + 2.0e16 / zavg)
                #if "ZEFFRAW" in SD and SD["ZEFFRAW"] is not None:
                #    edgenimp = 0.9 * SD["NIMPLIST"][ii]["NIMPRAW"][-1]
                #    edgenimpeb = 1.1 * SD["NIMPLIST"][ii]["NIMPRAWEB"][-1]
                SD["NIMP"+itag+"RAWX"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAWX"], 1.02))
                SD["NIMP"+itag+"RAWXEB"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAWXEB"], 0.0))
                SD["NIMP"+itag+"RAW"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAW"], edgenimp))
                SD["NIMP"+itag+"RAWEB"] = np.hstack((SD["NIMPLIST"][ii]["NIMPRAWEB"], edgenimpeb))
                SD["NIMP"+itag+"DMAP"] = np.hstack((SD["NIMPLIST"][ii]["NIMPDMAP"], [len(SD["NIMPLIST"][ii]["NIMPDIAG"])]))
                SD["AIMP"+itag+"RAW"] = SD["NIMPLIST"][ii]["AIMP"].copy()
                SD["AIMP"+itag+"RAWEB"] = SD["NIMPLIST"][ii]["AIMPEB"] if "AIMPEB" in SD["NIMPLIST"][ii] else SD["AIMP"+itag+"RAW"] * 0.0
                if len(SD["AIMP"+itag+"RAW"]) > 1:
                    SD["AIMP"+itag+"RAW"] = np.hstack((SD["AIMP"+itag+"RAW"], SD["AIMP"+itag+"RAW"][-1]))
                    SD["AIMP"+itag+"RAWEB"] = np.hstack((SD["AIMP"+itag+"RAWEB"], 0.0))
                SD["ZIMP"+itag+"RAW"] = SD["NIMPLIST"][ii]["ZIMP"].copy()
                SD["ZIMP"+itag+"RAWEB"] = SD["NIMPLIST"][ii]["ZIMPEB"] if "ZIMPEB" in SD["NIMPLIST"][ii] else SD["ZIMP"+itag+"RAW"] * 0.05
                if len(SD["ZIMP"+itag+"RAW"]) > 1:
                    SD["ZIMP"+itag+"RAW"] = np.hstack((SD["ZIMP"+itag+"RAW"], SD["ZIMP"+itag+"RAW"][-1]))
                    SD["ZIMP"+itag+"RAWEB"] = np.hstack((SD["ZIMP"+itag+"RAWEB"], 1.0))
                SD["MATIMP"+itag] = SD["NIMPLIST"][ii]["MATIMP"] if "MATIMP" in SD["NIMPLIST"][ii] else None
                SD["NIMP"+itag+"DIAG"] = SD["NIMPLIST"][ii]["NIMPDIAG"]
                SD["NIMP"+itag+"DIAG"].append("NXBC")
                SD["NIMP"+itag+"DS"] = SD["NIMPDSLIST"][ii] if ii < len(SD["NIMPDSLIST"]) else "Unknown"
                if len(SD["ZIMP"+itag+"RAW"]) > 1:
                    SD["ZIMP"+itag+"DS"] = SD["ZIMPDSLIST"][ii] if ii < len(SD["ZIMPDSLIST"]) else "Unknown"
        SD["NUMIMP"] = len(SD["NIMPLIST"])

        # 1-point Q boundary constraint based on edge data provides sufficient robustness, max. value of 4
        if "QRAW" in SD and SD["QRAW"].size > 0:
            bfilt = np.all([SD["QRAWX"] >= 0.01, SD["QRAWX"] <= 0.99], axis=0)
            edgeq = np.nanmax((4.0, 1.2 * np.nanmax(SD["QRAW"][bfilt])))
            edgeqeb = 0.5
            SD["QRAWX"] = np.hstack((SD["QRAWX"], 1.02))
            SD["QRAWXEB"] = np.hstack((SD["QRAWXEB"], 0.0))
            SD["QRAW"] = np.hstack((SD["QRAW"], edgeq))
            SD["QRAWEB"] = np.hstack((SD["QRAWEB"], edgeqeb))
            SD["QDMAP"] = np.hstack((SD["QDMAP"], len(SD["QDIAG"])))
            SD["QDIAG"].append("QBC")
#        if "CQRAW" in SD and SD["CQRAW"].size > 0:
#            bfilt = np.all([SD["CQRAWX"] >= 0.01, SD["CQRAWX"] <= 0.99], axis=0)
#            edgeq = np.nanmax((4.0, 1.2 * np.nanmax(SD["CQRAW"][bfilt])))
#            edgeqeb = 0.5
#            SD["CQRAWX"] = np.hstack((SD["CQRAWX"], 1.02))
#            SD["CQRAWXEB"] = np.hstack((SD["CQRAWXEB"], 0.0))
#            SD["CQRAW"] = np.hstack((SD["CQRAW"], edgeq))
#            SD["CQRAWEB"] = np.hstack((SD["CQRAWEB"], edgeqeb))
#            SD["CQDMAP"] = np.hstack((SD["CQDMAP"], len(SD["CQDIAG"])))
#            SD["CQDIAG"].append("QBC")
        if "IQRAW" in SD and SD["IQRAW"].size > 0:
            bfilt = np.all([SD["IQRAWX"] >= 0.01, SD["IQRAWX"] <= 0.99], axis=0)
            edgeq = np.nanmin((0.25, np.nanmin(SD["IQRAW"][bfilt] / 1.2)))
            edgeqeb = 0.02
            SD["IQRAWX"] = np.hstack((SD["IQRAWX"], 1.02))
            SD["IQRAWXEB"] = np.hstack((SD["IQRAWXEB"], 0.0))
            SD["IQRAW"] = np.hstack((SD["IQRAW"], edgeq))
            SD["IQRAWEB"] = np.hstack((SD["IQRAWEB"], edgeqeb))
            SD["IQDMAP"] = np.hstack((SD["IQDMAP"], len(SD["IQDIAG"])))
            SD["IQDIAG"].append("IQBC")
#        if "ICQRAW" in SD and SD["ICQRAW"].size > 0:
#            bfilt = np.all([SD["ICQRAWX"] >= 0.01, SD["ICQRAWX"] <= 0.99], axis=0)
#            edgeq = np.nanmax((0.25, np.nanmax(SD["ICQRAW"][bfilt] / 1.2)))
#            edgeqeb = 0.02
#            SD["ICQRAWX"] = np.hstack((SD["ICQRAWX"], 1.02))
#            SD["ICQRAWXEB"] = np.hstack((SD["ICQRAWXEB"], 0.0))
#            SD["ICQRAW"] = np.hstack((SD["ICQRAW"], edgeq))
#            SD["ICQRAWEB"] = np.hstack((SD["ICQRAWEB"], edgeqeb))
#            SD["ICQDMAP"] = np.hstack((SD["ICQDMAP"], len(SD["ICQDIAG"])))
#            SD["ICQDIAG"].append("IQBC")

        dlist = ["NIMPLIST", "ENIMPLIST", "NIMPDSLIST", "ENIMPDSLIST", "ZIMPDSLIST"]
        SD = ptools.delete_fields_from_dict(SD, fields=dlist)

    if fdebug:
        print("standardize_jet.py: add_profile_boundary_data() completed.")

    return SD


def standardize_source_data(procdata, newstruct=None, useexp=True, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Transfers source profile data fields from the implementation-
    specific object to the standardized object, with interpolation,
    ensuring that the standardized naming and format conventions are
    retained.

    :arg procdata: dict. Implementation-specific object containing processed source profile data.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg useexp: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Object with identical structure as input object except with source profiles interpolated and transferred.
    """
    PD = dict()
    QD = None
    if isinstance(procdata, dict):
        PD = procdata
    if isinstance(newstruct, dict):
        QD = newstruct
    else:
        QD = dict()

    if PD is not None:

#        cf = PD["PSICORRFLAG"] if "PSICORRFLAG" in PD else False
        cf = False
#        ctag = "C" if cf else ""
        ctag = ""
        rf = PD["RHOEBFLAG"] if "RHOEBFLAG" in PD else False
        QD["RAWSRCFLAG"] = True if "RAWSRCFLAG" in PD and PD["RAWSRCFLAG"] else False
        ftransp = False

        # Transfer TRANSP source data - should contain both NBI and ICRH if used in discharge
        qtag = "TRAU"
        quantities = ["QE", "QI", "J", "TAU"]  # "SFI", "NFI", "WFI" added later
#        code = None
#        sstr = ""
        if not ftransp:
            for src in ["NBI", "ICRH", "OHM"]:
                if qtag in PD and PD[qtag] is not None:
                    QD[src] = dict()
                    qquantities = copy.deepcopy(quantities)
                    if "AFIA"+src in PD[qtag] and PD[qtag]["AFIA"+src] is not None:
                        qquantities.extend(["SFIA", "NFIA", "WFIA"])
                    elif "AFI1"+src in PD[qtag] and PD[qtag]["AFI1"+src] is not None:
                        iion = 1
                        itag = "%d" % (iion)
                        while "AFI"+itag+src in PD[qtag] and "ZFI"+itag+src in PD[qtag]:
                            qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
                            iion += 1
                            itag = "%d" % (iion)
                    ifastion = 0
                    for ttag in qquantities:
                        dtag = ttag + src
                        ntag = ttag[:-1]+"1" if ttag.endswith("A") else ttag
                        if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                            tempd = process_averaged_profile(PD, PD[qtag], quantity=dtag, corrflag=cf, rhoerrflag=rf)
                            if tempd is not None:
                                QD[src][ntag] = tempd
                                if "A"+ntag[1:] not in QD[src] and (ttag.startswith("SFI") or ttag.startswith("NFI") or ttag.startswith("WFI")):
                                    afield = "A" + ttag[1:] + src
                                    zfield = "Z" + ttag[1:] + src
                                    if afield in PD[qtag] and zfield in PD[qtag]:
                                        ifastion += 1
                                        QD[src]["A"+ntag[1:]] = PD[qtag][afield] if PD[qtag][afield] is not None and PD[qtag][afield] >= 1.0 else np.array([2.0])
                                        QD[src]["Z"+ntag[1:]] = PD[qtag][zfield] if PD[qtag][zfield] is not None and PD[qtag][zfield] >= 1.0 else np.array([1.0])
                    if QD[src]:
                        if ifastion > 0:
                            QD[src]["NUMFI"] = ifastion
                        QD[src]["CODE"] = PD[qtag]["CODE"]
#                        code = PD[qtag]["CODE"]
                        ftransp = True
#                        if sstr:
#                            sstr += ", "
#                        sstr += src
                    else:
                        del QD[src]

#        if fdebug and "TRAU" in QD:
#            if code is None:
#                code = "UNKNOWN"
#            print("standardize_jet.py: standardize_source_data(): %s %s source data formatted." % (code, sstr))

        # Transfer TRANSP source data - should contain both NBI and ICRH if used in discharge
        qtag = "TRA0"
        quantities = ["QE", "QI", "J", "TAU"]  # "SFI", "NFI", "WFI" added later
#        code = None
#        sstr = ""
        if not ftransp:
            for src in ["NBI", "ICRH", "OHM"]:
                if qtag in PD and PD[qtag] is not None:
                    QD[src] = dict()
                    qquantities = copy.deepcopy(quantities)
                    if "AFIA"+src in PD[qtag] and PD[qtag]["AFIA"+src] is not None:
                        qquantities.extend(["SFIA", "NFIA", "WFIA"])
                    elif "AFI1"+src in PD[qtag] and PD[qtag]["AFI1"+src] is not None:
                        iion = 1
                        itag = "%d" % (iion)
                        while "AFI"+itag+src in PD[qtag] and "ZFI"+itag+src in PD[qtag]:
                            qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
                            iion += 1
                            itag = "%d" % (iion)
                    ifastion = 0
                    for ttag in qquantities:
                        dtag = ttag + src
                        ntag = ttag[:-1]+"1" if ttag.endswith("A") else ttag
                        if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                            tempd = process_averaged_profile(PD, PD[qtag], quantity=dtag, corrflag=cf, rhoerrflag=rf)
                            if tempd is not None:
                                QD[src][ntag] = tempd
                                if "A"+ntag[1:] not in QD[src] and (ttag.startswith("SFI") or ttag.startswith("NFI") or ttag.startswith("WFI")):
                                    afield = "A" + ttag[1:] + src
                                    zfield = "Z" + ttag[1:] + src
                                    if afield in PD[qtag] and zfield in PD[qtag]:
                                        ifastion += 1
                                        QD[src]["A"+ntag[1:]] = PD[qtag][afield] if PD[qtag][afield] is not None and PD[qtag][afield] >= 1.0 else np.array([2.0])
                                        QD[src]["Z"+ntag[1:]] = PD[qtag][zfield] if PD[qtag][zfield] is not None and PD[qtag][zfield] >= 1.0 else np.array([1.0])
                    if QD[src]:
                        if ifastion > 0:
                            QD[src]["NUMFI"] = ifastion
                        QD[src]["CODE"] = PD[qtag]["CODE"]
#                        code = PD[qtag]["CODE"]
                        ftransp = True
#                        if sstr:
#                            sstr += ", "
#                        sstr += src
                    else:
                        del QD[src]

#        if fdebug and "TRA0" in QD:
#            if code is None:
#                code = "UNKNOWN"
#            print("standardize_jet.py: standardize_source_data(): %s %s source data formatted." % (code, sstr))

        # Transfer neutral beam heating source data
        src = "NBI"
        qtag = "NBP2"
        quantities = ["QE", "QI", "J", "TAU"]  # "SFI", "NFI", "WFI" added later
        if qtag in PD and PD[qtag] is not None and not ftransp:
            QD[src] = dict()
            qquantities = copy.deepcopy(quantities)
            if "AFIA" in PD[qtag] and PD[qtag]["AFIA"] is not None:
                qquantities.extend(["SFIA", "NFIA", "WFIA"])
            elif "AFI1" in PD[qtag] and PD[qtag]["AFI1"] is not None:
                iion = 1
                itag = "%d" % (iion)
                while "AFI"+itag in PD[qtag] and "ZFI"+itag in PD[qtag]:
                    qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
                    iion += 1
                    itag = "%d" % (iion)
            ifastion = 0
            for ttag in qquantities:
                dtag = ttag
                ntag = ttag[:-1]+"1" if ttag.endswith("A") else ttag
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[qtag], quantity=dtag, corrflag=cf, rhoerrflag=rf)
                    if tempd is not None:
                        QD[src][ntag] = tempd
                        if "A"+ntag[1:] not in QD[src] and (ttag.startswith("SFI") or ttag.startswith("NFI") or ttag.startswith("WFI")):
                            afield = "A" + ttag[1:]
                            zfield = "Z" + ttag[1:]
                            if afield in PD[qtag] and zfield in PD[qtag]:
                                ifastion += 1
                                QD[src]["A"+ntag[1:]] = PD[qtag][afield] if PD[qtag][afield] is not None and PD[qtag][afield] >= 1.0 else np.array([2.0])
                                QD[src]["Z"+ntag[1:]] = PD[qtag][zfield] if PD[qtag][zfield] is not None and PD[qtag][zfield] >= 1.0 else np.array([1.0])
            if QD[src]:
                if ifastion > 0:
                    QD[src]["NUMFI"] = ifastion
                QD[src]["CODE"] = PD[qtag]["CODE"]
#                code = PD[qtag]["CODE"]
            else:
                del QD[src]

        if fdebug and "NBI" in QD:
            code = QD["NBI"]["CODE"] if "CODE" in QD["NBI"] else None
            if code is None:
                code = "UNKNOWN"
            print("standardize_jet.py: standardize_source_data(): %s NBI source data formatted." % (code))

        # Transfer ion cyclotron heating source data
        src = "ICRH"
        qtag = "PION"
        quantities = ["QE", "QI"]  # "NFI", "WFI" added later
#        code = None
        if qtag in PD and PD[qtag] is not None and not ftransp:
            QD[src] = dict()
            qquantities = copy.deepcopy(quantities)
            if "AFIA" in PD[qtag] and PD[qtag]["AFIA"] is not None:
                qquantities.extend(["NFIA", "WFIA"])
            elif "AFI1" in PD[qtag] and PD[qtag]["AFI1"] is not None:
                iion = 1
                itag = "%d" % (iion)
                while "AFI"+itag in PD[qtag] and "ZFI"+itag in PD[qtag]:
                    qquantities.extend(["NFI"+itag, "WFI"+itag])
                    iion += 1
                    itag = "%d" % (iion)
            ifastion = 0
            for ttag in qquantities:
                dtag = ttag
                ntag = ttag[:-1]+"1" if ttag.endswith("A") else ttag
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[qtag], quantity=dtag, corrflag=cf, rhoerrflag=rf)
                    if tempd is not None:
                        QD[src][ntag] = tempd
                        if "A"+ntag[1:] not in QD[src] and (ttag.startswith("SFI") or ttag.startswith("NFI") or ttag.startswith("WFI")):
                            afield = "A" + ttag[1:]
                            zfield = "Z" + ttag[1:]
                            if afield in PD[qtag] and zfield in PD[qtag]:   # Could also default to helium?
                                ifastion += 1
                                QD[src]["A"+ntag[1:]] = PD[qtag][afield] if PD[qtag][afield] is not None and PD[qtag][afield] >= 1.0 else np.array([1.0])
                                QD[src]["Z"+ntag[1:]] = PD[qtag][zfield] if PD[qtag][zfield] is not None and PD[qtag][zfield] >= 1.0 else np.array([1.0])
            if QD[src]:
                if ifastion > 0:
                    QD[src]["NUMFI"] = ifastion
                QD[src]["CODE"] = PD[qtag]["CODE"]
#                code = PD[qtag]["CODE"]
                # Subtracts NBI heat source profiles from PION, if NBI profiles are also extracted
                #    PION uses NBI profiles to calculate synergistic heating effects and includes base NBI contribution in output
                if "NBI" in QD and "QE" in QD["NBI"] and "QE" in QD[src]:
                    QD[src]["QE"]["VAL"] = QD[src]["QE"]["VAL"] - QD["NBI"]["QE"]["VAL"]
                    zfilt = (QD[src]["QE"]["VAL"] <= 0.0)
                    if np.any(zfilt):
                        QD[src]["QE"]["VAL"][zfilt] = 0.0
                if "NBI" in QD and "QI" in QD["NBI"] and "QI" in QD[src]:
                    QD[src]["QI"]["VAL"] = QD[src]["QI"]["VAL"] - QD["NBI"]["QI"]["VAL"]
                    zfilt = (QD[src]["QI"]["VAL"] <= 0.0)
                    if np.any(zfilt):
                        QD[src]["QI"]["VAL"][zfilt] = 0.0
            else:
                del QD[src]

        if fdebug and "ICRH" in QD:
            code = QD["ICRH"]["CODE"] if "CODE" in QD["ICRH"] else None
            if code is None:
                code = "UNKNOWN"
            print("standardize_jet.py: standardize_source_data(): %s ICRH source data formatted." % (code))

        # Transfer ohmic heating source data
        src = "OHM"
        qtag = "OHM"
        quantities = ["QE", "QI", "J"]
#        code = None
        if qtag in PD and PD[qtag] is not None and not ftransp:
            QD[src] = dict()
            ifastion = 0
            for dtag in quantities:
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[qtag], quantity=dtag, corrflag=cf, rhoerrflag=rf)
                    if tempd is not None:
                        QD[src][dtag] = tempd
            if QD[src]:
                if ifastion > 0:
                    QD[src]["NUMFI"] = ifastion
                QD[src]["CODE"] = PD[qtag]["CODE"]
#                code = PD[qtag]["CODE"] + " derived"
            else:
                del QD[src]

        if fdebug and "OHM" in QD:
            code = QD["OHM"]["CODE"] + " derived" if "CODE" in QD["OHM"] else None
            if code is None:
                code = "UNKNOWN"
            print("standardize_jet.py: standardize_source_data(): %s OHM source data formatted." % (code))

        # Transfer ohmic heating source data
        src = "RAD"
        qtag = "RAD"
        quantities = ["QRAD"]
#        code = None
        if qtag in PD and PD[qtag] is not None:
            QD[src] = dict()
            ifastion = 0
            for dtag in quantities:
                if dtag in PD[qtag] and PD[qtag][dtag] is not None:
                    tempd = process_averaged_profile(PD, PD[qtag], quantity=dtag, corrflag=cf, rhoerrflag=rf)
                    if tempd is not None:
                        QD[src][dtag] = tempd
            if QD[src]:
                if ifastion > 0:
                    QD[src]["NUMFI"] = ifastion
                QD[src]["CODE"] = PD[qtag]["CODE"]
#                code = PD[qtag]["CODE"]
            else:
                del QD[src]

        if fdebug and "RAD" in QD:
            code = QD["RAD"]["CODE"] if "CODE" in QD["RAD"] else None
            if code is None:
                code = "UNKNOWN"
            print("standardize_jet.py: standardize_source_data(): %s RAD source data formatted." % (code))

        # Remove rho=0 point in NBI source profiles calculated by PENCIL, spurious value due to zero volume in code, messes with fitting
        if "NBI" in QD and re.match(r'^NBP2$', QD["NBI"]["CODE"], flags=re.IGNORECASE):
            quantities = ["QE", "QI", "S", "J", "TAU", "NFIA", "WFIA"]
            for dtag in quantities:
                if dtag in QD["NBI"] and QD["NBI"][dtag]["VAL"] is not None:
                    QD["NBI"][dtag]["OPFN"] = QD["NBI"][dtag]["OPFN"][1:]
                    QD["NBI"][dtag]["VAL"] = QD["NBI"][dtag]["VAL"][1:]
                    QD["NBI"][dtag]["VALEB"] = QD["NBI"][dtag]["VALEB"][1:] if QD["NBI"][dtag]["VALEB"] is not None else None
                    QD["NBI"][dtag]["N"] = QD["NBI"][dtag]["N"][1:] if QD["NBI"][dtag]["N"] is not None else None
                    QD["NBI"][dtag]["PFN"] = QD["NBI"][dtag]["PFN"][1:]
                    QD["NBI"][dtag]["PFNEB"] = QD["NBI"][dtag]["PFNEB"][1:]
                    QD["NBI"][dtag]["RHOPN"] = QD["NBI"][dtag]["RHOPN"][1:]
                    QD["NBI"][dtag]["RHOPNEB"] = QD["NBI"][dtag]["RHOPNEB"][1:]
                    QD["NBI"][dtag]["TFN"] = QD["NBI"][dtag]["TFN"][1:]
                    QD["NBI"][dtag]["TFNEB"] = QD["NBI"][dtag]["TFNEB"][1:]
                    QD["NBI"][dtag]["RHOTN"] = QD["NBI"][dtag]["RHOTN"][1:]
                    QD["NBI"][dtag]["RHOTNEB"] = QD["NBI"][dtag]["RHOTNEB"][1:]
                    QD["NBI"][dtag]["AXID"] = QD["NBI"][dtag]["AXID"] - 1 if QD["NBI"][dtag]["AXID"] != 0 else 0

        # Remove rho=0 point in ICRH source profiles calculated by PION when remapping causes NaNs - commented out since new source filter removes NaNs
#        if "ICRH" in QD and re.match(r'^PION$', QD["ICRH"]["CODE"], flags=re.IGNORECASE):
#            quantities = ["QE", "QI", "S", "J", "TAU", "NFIA", "WFIA"]
#            for dtag in quantities:
#                if dtag in QD["ICRH"] and QD["ICRH"][dtag]["VAL"] is not None and not np.isfinite(QD["ICRH"][dtag]["RHOTN"][0]):
#                    QD["ICRH"][dtag]["OPFN"] = QD["ICRH"][dtag]["OPFN"][1:]
#                    QD["ICRH"][dtag]["VAL"] = QD["ICRH"][dtag]["VAL"][1:]
#                    QD["ICRH"][dtag]["VALEB"] = QD["ICRH"][dtag]["VALEB"][1:] if QD["ICRH"][dtag]["VALEB"] is not None else None
#                    QD["ICRH"][dtag]["N"] = QD["ICRH"][dtag]["N"][1:] if QD["ICRH"][dtag]["N"] is not None else None
#                    QD["ICRH"][dtag]["PFN"] = QD["ICRH"][dtag]["PFN"][1:]
#                    QD["ICRH"][dtag]["PFNEB"] = QD["ICRH"][dtag]["PFNEB"][1:]
#                    QD["ICRH"][dtag]["RHOPN"] = QD["ICRH"][dtag]["RHOPN"][1:]
#                    QD["ICRH"][dtag]["RHOPNEB"] = QD["ICRH"][dtag]["RHOPNEB"][1:]
#                    QD["ICRH"][dtag]["TFN"] = QD["ICRH"][dtag]["TFN"][1:]
#                    QD["ICRH"][dtag]["TFNEB"] = QD["ICRH"][dtag]["TFNEB"][1:]
#                    QD["ICRH"][dtag]["RHOTN"] = QD["ICRH"][dtag]["RHOTN"][1:]
#                    QD["ICRH"][dtag]["RHOTNEB"] = QD["ICRH"][dtag]["RHOTNEB"][1:]
#                    QD["ICRH"][dtag]["AXID"] = QD["ICRH"][dtag]["AXID"] - 1 if QD["ICRH"][dtag]["AXID"] != 0 else 0

        if not ("RAWSRCFLAG" in QD and QD["RAWSRCFLAG"]):
            dlist = ["TRAU", "TRA0", "NBP2", "PION", "OHM"]
            PD = ptools.delete_fields_from_dict(PD, fields=dlist)

    if fdebug:
        print("standardize_jet.py: standardize_source_data() completed.")

    return QD


def filter_source(srcdata, newstruct=None, tag=None, expflag=True, userscale=None):
    """
    JET-SPECIFIC FUNCTION
    Applies custom filters to processed plasma source profiles, accounting
    for the remaining implementation-specific variables.

    :arg srcdata: dict. Formatted object containing processed source profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg tag: str. Optional field name into which filtered data is placed, overwrites existing vector if field is already present.

    :kwarg expflag: bool. Flag to include filters based on speculative heuristics, as opposed to generally accepted metrics.

    :kwarg userscale: float. Value to specify user-defined error scaling, for increased customizability of data.

    :returns: (dict, int).
        Object with identical structure as input object except with input profile appended to standard fields,
        integer indicating the whether the data was processed and how it was processed.
    """
    DD = None
    SD = None
    sc = 1.0
    if isinstance(srcdata, dict):
        DD = srcdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    if isinstance(userscale, (int, float)) and float(userscale) > 0.0:
        sc = float(userscale)
    dtag = tag if isinstance(tag, str) else "SRC"
    status = 0

    (rho, rhoeb, val, valeb) = get_standardized_profile_data(DD, hfsflag=False, polflag=True)

    if rho is not None and val is not None:

        # Finite filter - removes nans and infs, along with associated indices on other vectors
        darr = np.vstack((rho, rhoeb, val, valeb))
        if darr.size > 0:
            darr = ptools.finite_filter_2d(darr, indices=[0, 2])

        # Boundary filter - removes unnecessary data which slows down fit routine
        if darr.size > 0:
            darr = ptools.bounded_filter_2d(darr, upper_bound=1.03, upper_equality=False, indices=[0])

        if darr.size > 0:
            rho = darr[0, :].flatten()
            rhoeb = darr[1, :].flatten()
            val = darr[2, :].flatten()
            valeb = darr[3, :].flatten()
            # Only allows user modification of error within 1 - 50% of base value, keeping values already beyond those bounds the same
            if expflag:
                valeb = error_multiplier(valeb, sc, lower_bound=0.01, upper_bound=0.5, value=val)
            SD = add_filtered_profile_data(dtag, newstruct=SD, xval=rho, xerr=rhoeb, yval=val, yerr=valeb)

        status = 1

    return (SD, status)


def transfer_standardized_source_data(srcdata, newstruct=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Applies pre-defined filters to the raw diagnostic data, optimized
    such to improve the quality of the fitting routine.

    :arg diagdata: dict. Formatted object containing processed safety factor profile information.

    :kwarg newstruct: dict. Optional object in which standardized data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Standardized object with profiles derived from filtered diagnostic data inserted.
    """
    QD = None
    SD = None
    if isinstance(srcdata, dict):
        QD = srcdata
    if isinstance(newstruct, dict):
        SD = newstruct
    else:
        SD = dict()
    fe = 0

    SD["QCODE"] = None
    SD["SCODE"] = None
    SD["JCODE"] = None
    SD["TAUCODE"] = None
    SD["NCODE"] = None
    SD["WCODE"] = None

    if QD is not None:
        SD["CS_SRC"] = "RHOPOLN"
        ef = True if "EXPFILTFLAG" in SD and SD["EXPFILTFLAG"] and not ("PURERAWFLAG" in SD and SD["PURERAWFLAG"]) else False
        src_cats = ["Q", "S", "J", "TAU", "N", "W"]

        src = "NBI"
        if src in QD and QD[src] is not None:
            gflag = False
            qlist = {"QE": "QE", "QI": "QI", "J": "J", "TAU": "TAU"}  # "SFI", "NFI", "WFI" added later
            filist = {}
            if "NUMFI" in QD[src]:
                for ifastion in range(QD[src]["NUMFI"]):
                    itag = "%d" % (ifastion+1)
                    filist["SFI"+itag] = "SFI" + itag
                    filist["NFI"+itag] = "NFI" + itag
                    filist["WFI"+itag] = "WFI" + itag
            qlist.update(filist)
            code = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
            for qtag in qlist:
                if qtag in QD[src] and QD[src][qtag] is not None:
                    (SD, sstatus) = filter_source(QD[src][qtag], newstruct=SD, tag=qlist[qtag]+src, expflag=ef)
                    if sstatus > 0 and qlist[qtag]+src+"RAW" in SD and SD[qlist[qtag]+src+"RAW"].size > 0:
                        gflag = True
                        for category in src_cats:
                            if qtag.startswith(category):
                                if SD[category+"CODE"] is None:
                                    SD[category+"CODE"] = []
                                if code.upper() not in SD[category+"CODE"]:
                                    SD[category+"CODE"].append(code.upper())
                        SD[qlist[qtag]+src+"DS"] = SD[qlist[qtag]+src+"DS"] + "; " + QD[src][qtag]["PROV"] if qlist[qtag]+src+"DS" in SD else QD[src][qtag]["PROV"]
                        if "A"+qtag[1:]+src not in SD and (qtag.startswith("S") or qtag.startswith("N") or qtag.startswith("W")):
                            afield = "A" + qtag[1:]
                            zfield = "Z" + qtag[1:]
                            if afield in QD[src] and zfield in QD[src]:
                                SD[afield+src] = QD[src][afield] if QD[src][afield] is not None else np.array([2.0])
                                SD[zfield+src] = QD[src][zfield] if QD[src][zfield] is not None else np.array([1.0])
            if gflag:
                if "NUMFI" in QD[src]:
                    SD["NUMFI"+src] = QD[src]["NUMFI"]
                SD[src+"CODE"] = code.upper()

        src = "ICRH"
        pionflag = False
        if src in QD and QD[src] is not None:
            gflag = False
            qlist = {"QE": "QE", "QI": "QI"}     # "NFI", "WFI" added later
            filist = {}
            if "NUMFI" in QD[src]:
                for ifastion in range(QD[src]["NUMFI"]):
                    itag = "%d" % (ifastion+1)
                    #filist["SFI"+itag] = "SFI" + itag
                    filist["NFI"+itag] = "NFI" + itag
                    filist["WFI"+itag] = "WFI" + itag
            qlist.update(filist)
            code = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
            for qtag in qlist:
                if qtag in QD[src] and QD[src][qtag] is not None:
                    (SD, sstatus) = filter_source(QD[src][qtag], newstruct=SD, tag=qlist[qtag]+src, expflag=ef)
                    if sstatus > 0 and qlist[qtag]+src+"RAW" in SD and SD[qlist[qtag]+src+"RAW"].size > 0:
                        gflag = True
                        for category in src_cats:
                            if qtag.startswith(category):
                                if SD[category+"CODE"] is None:
                                    SD[category+"CODE"] = []
                                if code.upper() not in SD[category+"CODE"]:
                                    SD[category+"CODE"].append(code.upper())
                        SD[qlist[qtag]+src+"DS"] = SD[qlist[qtag]+src+"DS"] + "; " + QD[src][qtag]["PROV"] if qlist[qtag]+src+"DS" in SD else QD[src][qtag]["PROV"]
                        if "A"+qtag[1:]+src not in SD and (qtag.startswith("S") or qtag.startswith("N") or qtag.startswith("W")):
                            afield = "A" + qtag[1:]
                            zfield = "Z" + qtag[1:]
                            if afield in QD[src] and zfield in QD[src]:
                                SD[afield+src] = QD[src][afield] if QD[src][afield] is not None else np.array([1.0])
                                SD[zfield+src] = QD[src][zfield] if QD[src][zfield] is not None else np.array([1.0])
            if gflag:
                if "NUMFI" in QD[src]:
                    SD["NUMFI"+src] = QD[src]["NUMFI"]
                SD[src+"CODE"] = code.upper()

        src = "LH"
        if src in QD and QD[src] is not None:
            gflag = False
            code = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
            if gflag:
                SD[src+"CODE"] = code.upper()
                for category in src_cats:
                    if qtag.startswith(category):
                        if SD[category+"CODE"] is None:
                            SD[category+"CODE"] = []
                        if code.upper() not in SD[category+"CODE"]:
                            SD[category+"CODE"].append(code.upper())
            print("You have found the EASTER EGG! Just kidding, this should not ever be printed.")

        src = "OHM"
        if src in QD and QD[src] is not None:
            gflag = False
            qlist = {"QE": "QE", "QI": "QI", "J": "J"}
            code = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
            for qtag in qlist:
                if qtag in QD[src] and QD[src][qtag] is not None:
                    (SD, sstatus) = filter_source(QD[src][qtag], newstruct=SD, tag=qlist[qtag]+src, expflag=ef)
                    if sstatus > 0 and qlist[qtag]+src+"RAW" in SD and SD[qlist[qtag]+src+"RAW"].size > 0:
                        gflag = True
                        for category in src_cats:
                            if qtag.startswith(category):
                                if SD[category+"CODE"] is None:
                                    SD[category+"CODE"] = []
                                if code.upper() not in SD[category+"CODE"]:
                                    SD[category+"CODE"].append(code.upper())
                        SD[qlist[qtag]+src+"DS"] = SD[qlist[qtag]+src+"DS"] + "; " + QD[src][qtag]["PROV"] if qlist[qtag]+src+"DS" in SD else QD[src][qtag]["PROV"]
#                        if "A"+qtag[1:]+src not in SD and (qtag.startswith("N") or qtag.startswith("W")):
#                            afield = "A" + qtag[1:]
#                            zfield = "Z" + qtag[1:]
#                            if afield in QD[src] and zfield in QD[src]:
#                                SD[afield+src] = QD[src][afield] if QD[src][afield] is not None else np.array([1.0])
#                                SD[zfield+src] = QD[src][zfield] if QD[src][zfield] is not None else np.array([1.0])
            if gflag:
                SD[src+"CODE"] = code.upper()

        src = "RAD"
        if src in QD and QD[src] is not None:
            gflag = False
            qlist = {"QRAD": "Q"}
            code = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
            for qtag in qlist:
                if qtag in QD[src] and QD[src][qtag] is not None:
                    (SD, sstatus) = filter_source(QD[src][qtag], newstruct=SD, tag=qlist[qtag]+src, expflag=ef)
                    if sstatus > 0 and qlist[qtag]+src+"RAW" in SD and SD[qlist[qtag]+src+"RAW"].size > 0:
                        gflag = True
                        for category in src_cats:
                            if qtag.startswith(category):
                                if SD[category+"CODE"] is None:
                                    SD[category+"CODE"] = []
                                if code.upper() not in SD[category+"CODE"]:
                                    SD[category+"CODE"].append(code.upper())
                        SD[qlist[qtag]+src+"DS"] = SD[qlist[qtag]+src+"DS"] + "; " + QD[src][qtag]["PROV"] if qlist[qtag]+src+"DS" in SD else QD[src][qtag]["PROV"]
            if gflag:
                SD[src+"CODE"] = code.upper()

        # Why was this duplicated from above?
#        src = "RAD"
#        if src in QD and QD[src] is not None:
#            gflag = False
#            code = QD[src]["CODE"] if "CODE" in QD[src] else "UNKNOWN"
#            if "QRAD" in QD[src] and QD[src]["QRAD"] is not None:
#                SD["Q"+src+"RAW"] = QD[src]["QRAD"]["VAL"].copy()
#                SD["Q"+src+"RAWEB"] = QD[src]["QRAD"]["VALEB"].copy()
#                SD["Q"+src+"RAWX"] = QD[src]["QRAD"]["RHOPN"].copy()
#                SD["Q"+src+"DS"] = SD["Q"+src+"DS"] + "; " + QD[src]["QRAD"]["PROV"] if SD["Q"+src+"DS"] is not None else QD[src]["QRAD"]["PROV"]
#                gflag = True
#                if SD["QCODE"] is None:
#                    SD["QCODE"] = []
#                if code.upper() not in SD["QCODE"]:
#                    SD["QCODE"].append(code.upper())
#            if gflag:
#                SD[src+"CODE"] = code.upper()

        if fdebug:
            fields = ["QCODE", "SCODE", "JCODE", "TAUCODE", "NCODE", "WCODE"]
            for key in fields:
                if SD[key] is not None:
                    cstr = ""
                    for diag in SD[key]:
                        cstr = cstr + ", " + diag.upper() if cstr else cstr + diag.upper()
                    if cstr:
                        qlabel = key[:-4]
                        print("standardize_jet.py: transfer_standardized_source_data(): %s data sorted into %s field." % (cstr, qlabel))

    if fdebug:
        print("standardize_jet.py: transfer_standardized_source_data() completed.")

    return SD


def add_derivative_constraint_data(stddata, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Inserts appropriate derivative constraints on the various profiles
    in order improve the robustness of the GP fit routine and to
    achieve more meaningful fits.

    :arg stddata: dict. Standardized object containing the processed profiles and GPR1D settings.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Standardized object with derivative constraints for profile to be fitted inserted.
    """
    SD = None
    if isinstance(stddata, dict):
        SD = stddata

    ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False

    ###  The following section adds profile derivative constraints for more robust GP fits
    if SD is not None and "CS_DIAG" in SD and re.match('^RHO((TOR)|(POL))N?$', SD["CS_DIAG"], flags=re.IGNORECASE):

        SD["SYMMETRIC"] = True

        # Zero NE derivative constraint at rho_tor = 0.0, axis of symmetry
        if "NERAW" in SD and SD["NERAW"].size > 0:
            SD["DNERAWX"] = np.array([0.0])
            SD["DNERAWXEB"] = np.array([0.0])
            SD["DNERAW"] = np.array([0.0])
            SD["DNERAWEB"] = np.array([0.0])

        # Zero TE derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TERAW" in SD and SD["TERAW"].size > 0:
            SD["DTERAWX"] = np.array([0.0])
            SD["DTERAWXEB"] = np.array([0.0])
            SD["DTERAW"] = np.array([0.0])
            SD["DTERAWEB"] = np.array([0.0])

        # Zero TI derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TIRAW" in SD and SD["TIRAW"].size > 3:
            # Additional derivative constraint if there is no inner core data, rho < 0.2
            cfilt = np.all([SD["TIRAWX"] >= 0.0, SD["TIRAWX"] <= 0.2], axis=0)
            uidx = np.where(SD["TIRAW"] == np.nanmax(SD["TIRAW"]))[0][0]
            lidx = np.where(SD["TIRAW"] == np.nanmin(SD["TIRAW"]))[0][0]
            corecon = float(0.3 * (SD["TIRAW"][uidx] - SD["TIRAW"][lidx]) / (SD["TIRAWX"][uidx] - SD["TIRAWX"][lidx])) if not np.any(cfilt) else None
            dcxvec = np.array([0.1]) if corecon is not None else np.array([])
            dcyvec = np.array([corecon]) if corecon is not None else np.array([])
            dcevec = np.array([0.3 * np.abs(corecon)]) if corecon is not None else np.array([])
            SD["DTIRAWX"] = np.hstack((0.0, dcxvec))
            SD["DTIRAWXEB"] = np.hstack((0.0, np.zeros(dcxvec.shape)))
            SD["DTIRAW"] = np.hstack((0.0, dcyvec))
            SD["DTIRAWEB"] = np.hstack((0.0, dcevec))

        # Zero AF derivative constraint at rho_tor = 0.0, axis of symmetry
        # Extra derivative constraint at inner-most point to avoid high core shear in GP fit due to lack of data
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
            # This filter works since the radial coordinate with JET data is always rhopol or rhotor
            iidx = np.where(SD["AFRAWX"] == np.nanmin(SD["AFRAWX"]))[0][0]
            edgecon = float(-SD["AFRAW"][iidx]) / 2.0 if SD["AFRAWX"][iidx] > 0.2 else None
            dcxvec = np.array([0.2]) if edgecon is not None else np.array([])
            dcyvec = np.array([edgecon]) if edgecon is not None else np.array([])
            dcevec = np.array([0.25 * np.abs(edgecon)]) if edgecon is not None else np.array([])
            SD["DAFRAWX"] = np.hstack((0.0, dcxvec))
            SD["DAFRAWXEB"] = np.hstack((0.0, np.zeros(dcxvec.shape)))
            SD["DAFRAW"] = np.hstack((0.0, dcyvec))
            SD["DAFRAWEB"] = np.hstack((0.0, dcevec))

        # Zero NIMP derivative constraint at rho_tor = 0.0, axis of symmetry
        for ii in range(SD["NUMIMP"]):
            itag = "%d" % (ii+1)
            if "NIMP"+itag+"RAW" in SD and SD["NIMP"+itag+"RAW"].size > 0:
                SD["DNIMP"+itag+"RAWX"] = np.array([0.0])
                SD["DNIMP"+itag+"RAWXEB"] = np.array([0.0])
                SD["DNIMP"+itag+"RAW"] = np.array([0.0])
                SD["DNIMP"+itag+"RAWEB"] = np.array([0.0])
            # Added ZIMP derivative constraint at rho_tor = 0.0, axis of symmetry (only added if Z profile is given)
            if "ZIMP"+itag+"RAW" in SD and SD["ZIMP"+itag+"RAW"].size > 1:
                SD["DZIMP"+itag+"RAWX"] = np.array([0.0])
                SD["DZIMP"+itag+"RAWXEB"] = np.array([0.0])
                SD["DZIMP"+itag+"RAW"] = np.array([0.0])
                SD["DZIMP"+itag+"RAWEB"] = np.array([0.0])

        # Zero TIMP derivative constraint at rho_tor = 0.0, axis of symmetry
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            # Additional derivative constraint if there is no inner core data, rho <= 0.2
            cfilt = np.all([SD["TIMPRAWX"] >= 0.0, SD["TIMPRAWX"] <= 0.2], axis=0)
            uidx = np.where(SD["TIMPRAW"] == np.nanmax(SD["TIMPRAW"]))[0][0]
            lidx = np.where(SD["TIMPRAW"] == np.nanmin(SD["TIMPRAW"]))[0][0]
            corecon = float(0.3 * (SD["TIMPRAW"][uidx] - SD["TIMPRAW"][lidx]) / (SD["TIMPRAWX"][uidx] - SD["TIMPRAWX"][lidx])) if not np.any(cfilt) else None
            # Additional derivative constraint if there is no core data, rho <= 0.5
            cfilt = np.all([SD["TIMPRAWX"] >= 0.0, SD["TIMPRAWX"] <= 0.5], axis=0)
            uidx = np.where(SD["TIMPRAW"] == np.nanmax(SD["TIMPRAW"]))[0][0]
            lidx = np.where(SD["TIMPRAW"] == np.nanmin(SD["TIMPRAW"]))[0][0]
            corecon2 = float(0.9 * (SD["TIMPRAW"][uidx] - SD["TIMPRAW"][lidx]) / (SD["TIMPRAWX"][uidx] - SD["TIMPRAWX"][lidx])) if not np.any(cfilt) else None
            dcxvec = np.array([0.1]) if corecon is not None else np.array([])
            dcyvec = np.array([corecon]) if corecon is not None else np.array([])
            dcevec = np.array([0.3 * np.abs(corecon)]) if corecon is not None else np.array([])
            if corecon2 is not None:
                dcxvec = np.hstack((dcxvec, 0.4))
                dcyvec = np.hstack((dcyvec, corecon2))
                dcevec = np.hstack((2.0 * dcevec, 0.2 * corecon2))
            SD["DTIMPRAWX"] = np.hstack((0.0, dcxvec))
            SD["DTIMPRAWXEB"] = np.hstack((0.0, np.zeros(dcxvec.shape)))
            SD["DTIMPRAW"] = np.hstack((0.0, dcyvec))
            SD["DTIMPRAWEB"] = np.hstack((0.0, dcevec))

        # Zero Q derivative constraint at rho_tor = 0.0, axis of symmetry
        if "QRAW" in SD and SD["QRAW"].size > 0:
            SD["DQRAWX"] = np.array([0.0])
            SD["DQRAWXEB"] = np.array([0.0])
            SD["DQRAW"] = np.array([0.0])
            SD["DQRAWEB"] = np.array([0.0])
        if "IQRAW" in SD and SD["IQRAW"].size > 0:
            SD["DIQRAWX"] = np.array([0.0])
            SD["DIQRAWXEB"] = np.array([0.0])
            SD["DIQRAW"] = np.array([0.0])
            SD["DIQRAWEB"] = np.array([0.0])

        # Zero CQ derivative constraint at rho_tor = 0.0, axis of symmetry
#        if "CQRAW" in SD and SD["CQRAW"].size > 0:
#            SD["DCQRAWX"] = np.array([0.0])
#            SD["DCQRAWXEB"] = np.array([0.0])
#            SD["DCQRAW"] = np.array([0.0])
#            SD["DCQRAWEB"] = np.array([0.0])
#        if "ICQRAW" in SD and SD["ICQRAW"].size > 0:
#            SD["DICQRAWX"] = np.array([0.0])
#            SD["DICQRAWXEB"] = np.array([0.0])
#            SD["DICQRAW"] = np.array([0.0])
#            SD["DICQRAWEB"] = np.array([0.0])

        # Zero ZEFF derivative constraint at rho_tor = 0.0, axis of symmetry
        if "ZEFFRAW" in SD and SD["ZEFFRAW"].size > 0:
            SD["DZEFFRAWX"] = np.array([0.0])
            SD["DZEFFRAWXEB"] = np.array([0.0])
            SD["DZEFFRAW"] = np.array([0.0])
            SD["DZEFFRAWEB"] = np.array([0.0])

        # Zero derivative constraint for all source profiles at rho_tor = 0.0, axis of symmetry
        sources = ["NBI", "ICRH", "ECRH", "LH", "OHM"]
        for src in sources:
            if "QE"+src+"RAW" in SD and SD["QE"+src+"RAW"].size > 0:
                SD["DQE"+src+"RAWX"] = np.array([0.0])
                SD["DQE"+src+"RAWXEB"] = np.array([0.0])
                SD["DQE"+src+"RAW"] = np.array([0.0])
                SD["DQE"+src+"RAWEB"] = np.array([0.0])
            if "QI"+src+"RAW" in SD and SD["QI"+src+"RAW"].size > 0:
                SD["DQI"+src+"RAWX"] = np.array([0.0])
                SD["DQI"+src+"RAWXEB"] = np.array([0.0])
                SD["DQI"+src+"RAW"] = np.array([0.0])
                SD["DQI"+src+"RAWEB"] = np.array([0.0])
            if "J"+src+"RAW" in SD and SD["J"+src+"RAW"].size > 0:
                SD["DJ"+src+"RAWX"] = np.array([0.0])
                SD["DJ"+src+"RAWXEB"] = np.array([0.0])
                SD["DJ"+src+"RAW"] = np.array([0.0])
                SD["DJ"+src+"RAWEB"] = np.array([0.0])
            if "TAU"+src+"RAW" in SD and SD["TAU"+src+"RAW"].size > 0:
                SD["DTAU"+src+"RAWX"] = np.array([0.0])
                SD["DTAU"+src+"RAWXEB"] = np.array([0.0])
                SD["DTAU"+src+"RAW"] = np.array([0.0])
                SD["DTAU"+src+"RAWEB"] = np.array([0.0])
            if "NUMFI"+src in SD and SD["NUMFI"+src] > 0:
                for ifastion in range(SD["NUMFI"+src]):
                    itag = "%d" % (ifastion+1)
                    if "SFI"+itag+src+"RAW" in SD and SD["SFI"+itag+src+"RAW"].size > 0:
                        SD["DSFI"+itag+src+"RAWX"] = np.array([0.0])
                        SD["DSFI"+itag+src+"RAWXEB"] = np.array([0.0])
                        SD["DSFI"+itag+src+"RAW"] = np.array([0.0])
                        SD["DSFI"+itag+src+"RAWEB"] = np.array([0.0])
                    if "NFI"+itag+src+"RAW" in SD and SD["NFI"+itag+src+"RAW"].size > 0:
                        SD["DNFI"+itag+src+"RAWX"] = np.array([0.0])
                        SD["DNFI"+itag+src+"RAWXEB"] = np.array([0.0])
                        SD["DNFI"+itag+src+"RAW"] = np.array([0.0])
                        SD["DNFI"+itag+src+"RAWEB"] = np.array([0.0])
                    if "WFI"+itag+src+"RAW" in SD and SD["WFI"+itag+src+"RAW"].size > 0:
                        SD["DWFI"+itag+src+"RAWX"] = np.array([0.0])
                        SD["DWFI"+itag+src+"RAWXEB"] = np.array([0.0])
                        SD["DWFI"+itag+src+"RAW"] = np.array([0.0])
                        SD["DWFI"+itag+src+"RAWEB"] = np.array([0.0])
        src = "RAD"
        if "Q"+src+"RAW" in SD and SD["Q"+src+"RAW"].size > 0:
            SD["DQ"+src+"RAWX"] = np.array([0.0])
            SD["DQ"+src+"RAWXEB"] = np.array([0.0])
            SD["DQ"+src+"RAW"] = np.array([0.0])
            SD["DQ"+src+"RAWEB"] = np.array([0.0])

    if fdebug:
        print("standardize_jet.py: add_derivative_constraint_data() completed.")

    return SD
