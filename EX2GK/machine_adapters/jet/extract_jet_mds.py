# Script with functions to grab PPF data from JET server via MDSplus
# Developer: Aaron Ho - 15/01/2017
#    Additional help for MDSplus can be found at http://www.mdsplus.org/
#    Additional help for TCI expression (underlying language of MDSplus databases) can be found at https://www.mdsplus.org/index.php/Documentation:Reference:TDI_INDEX

# Required imports
import os
import sys
import inspect
import numpy as np
import re
import copy
import datetime
import pickle
from scipy.interpolate import interp1d

# Required non-standard Python packages
from MDSplus import connection, mdsscalar

# List of signals categories which belong to JPF system
jpf_fields = ['ah', 'da', 'db', 'dc', 'dd', 'de', 'df', 'dg', 'dh', 'di', 'dj', \
              'gs', 'pf', 'pl', 'rf', 'sa', 'sc', 'ss', 'tf', 'vc', 'yc']


def mdsconnect(server):
    """
    Connect to MDS server via MDSIP.

    :arg server: str. Name of MDS server to open a connection with.

    :returns: obj. Python MDS Connection object.
    """
    if isinstance(server, str):
        conn = connection.Connection(server)
    else:
        raise TypeError("MDS server name must be a string.")
    return conn


def mdsdisconnect(conn):
    """
    Disconnect from MDS server, technically done automatically when script terminates.
    Recommended to apply this manually before script termination, just to be safe.

    :arg conn: obj. Python MDS Connection object to close.

    :returns: none.
    """
    if isinstance(conn, connection.Connection):
        conn.closeAllTrees()
    del conn


def getsig(shot, ti, tf, name, favg=True, nsplit=None, gdebug=False, **kwargs):
    """
    JET-SPECIFIC FUNCTION
    Obtain desired signal / data via MDSPlus interface, with optional averaging,
    from a single data field over the specified time window. A typical application
    would need to call this function multiple times to grab all the required
    data. This function does not check for valid inputs, instead outputting a
    blank numpy array if the inputs are invalid.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg name: str. DDA and DTYPE of data to be extracted, in format '<DDA>/<DTYPE>'.

    :kwarg favg: bool. Flag to toggle averaging of the extracted data over the specified time window.

    :kwarg nsplit: int. Optional number of equal time windows to split data into, default is 1.

    :kwarg gdebug: bool.  Toggles printing of select debugging statements, useful for determining issues in workflow.

    :kwarg conn: obj. Python MDS Connection object to grab data through. MANDATORY!

    :kwarg seq: int. Optional sequence number of data to be extracted within the JET PPF data system, default is 0.

    :kwarg uid: str. Optional user ID tag for accessing private entries within the JET PPF data system, default is jetppf.

    :kwarg repo: str. Optional selection of signal database, choice between PPF and JPF only.

    :returns: (array, array, array, float, float, int).
        Extracted data, vector of spatial or other coordinate points corresponding to extracted data, vector of
        time coordinate points corresponding to extracted data, time of first data point found, time of last
        data point found, number of data points found in terms of the time coordinate.
    """
    conn = kwargs.get("conn", None)
    seq = kwargs.get("seq", None)
    uid = kwargs.get("uid", None)
    repo = kwargs.get("repo", "ppf")
    if not isinstance(conn, connection.Connection):
        raise IOError("MDSplus connection object not provided to JET MDSplus adapter.")

    siglist = None
    sseq = '%d' % (int(seq)) if isinstance(seq, (float, int)) else '0'
    numt = int(nsplit) if isinstance(nsplit, (float, int)) and int(nsplit) > 0 else 1
    if gdebug:
        print(shot, ti, tf, name)

    # Extract data from chosen adapter
    dsig = None
    dstr = 'ppf/' + name + '/' + sseq
    if isinstance(repo, str):
        if re.match(r'^ppf$', repo.strip(), flags=re.IGNORECASE):
            dstr = 'ppf/' + name + '/' + sseq
        if re.match(r'^jpf$', repo.strip(), flags=re.IGNORECASE):
            dstr = 'jpf/' + name
    try:
        if isinstance(uid, str):
            conn.get('_suid=ppfuid($)', uid)
        dsig = conn.get('_sig=jet($,$)', dstr, shot)
    except KeyError:
        dsig = None
    if isinstance(dsig, mdsscalar.String):
        dsig = None

    # Place data into standardized variables
    data = np.array([])
    xvec = None
    tvec = None
    desc = None   # Unfortunately, this information is NOT provided via MDSplus at JET
    uidf = None
    seqf = None
    if dsig is not None:
        data = np.array(dsig)
    if data.size == 0:
        print('   Requested signal %-10s from shot #%8d is empty.' % (name, shot))
    elif data.ndim == 1:
        tvec = np.array(conn.get('dim_of(_sig,0)')).flatten()
    elif data.shape[0] == 1:
        tvec = np.array(conn.get('dim_of(_sig,0)')).flatten()
        if tvec.size != 1:
            data = np.atleast_1d(np.squeeze(data))
    elif data.ndim >= 2:
        xvec = np.array(conn.get('dim_of(_sig,0)')).flatten()
        tvec = np.array(conn.get('dim_of(_sig,1)')).flatten()
    if dsig is not None:
        uidf = conn.get('ppfgid()')
        seqf = conn.get('ppfinf(_sig)')

    # Post-process and place into standardized structure
    if tvec is not None:
        siglist = []
        timei = tvec[0] if ti is None else ti
        timef = tvec[-1] if tf is None else tf
        deltat = float(timef - timei) / float(numt)

        for ii in range(numt):
            siginfo = dict()
            itimei = timei + float(ii) * deltat if deltat > 1.0e-6 else timei
            idx = np.all([(tvec >= itimei), (tvec <= (itimei + deltat))], axis=0)    # boolean array flagging desired data points
            siginfo["T1"] = float(itimei)
            siginfo["T2"] = float(itimei + deltat)
            prov = "JET PPF: MDS+, %s, %s, seq.%d" % (name.upper(), uidf.decode("utf-8").lower(), seqf) if uidf is not None and seqf is not None else None

            # Automatically apply average over specified time window
            if np.any(idx):
                if xvec is None:
                    if favg:
                        siginfo["DATA"] = np.nanmean(data[idx]).flatten()
                    else:
                        siginfo["DATA"] = data[idx].copy()
                else:
                    if favg:
                        siginfo["DATA"] = np.nanmean(data[idx, :], axis=0) if data.shape[0] == tvec.size else np.nanmean(data[:, idx], axis=1)
                        siginfo["DATA"] = siginfo["DATA"].flatten()
                    else:
                        siginfo["DATA"] = data[idx, :].copy() if data.shape[0] == tvec.size else np.transpose(data[:, idx])
                siginfo["DATA"] = np.array(siginfo["DATA"], dtype=np.float64)
                siginfo["XVEC"] = np.array(xvec, dtype=np.float64) if xvec is not None else None
                siginfo["TVEC"] = np.array(tvec[idx], dtype=np.float64)
                siginfo["NAVG"] = siginfo["TVEC"].size
                siginfo["DESC"] = ""
                #siginfo["DESC"] = desc
                siginfo["PROV"] = prov
                if favg:
                    siginfo["TVEC"] = np.nanmean(siginfo["TVEC"])
            else:
                siginfo["DATA"] = None
                siginfo["XVEC"] = None
                siginfo["TVEC"] = None
                siginfo["NAVG"] = None
                siginfo["DESC"] = desc
                siginfo["PROV"] = prov
                if desc is None:
                    print('   Requested time slice [%8.5f, %8.5f] not in %-10s from shot #%8d.' % (siginfo["T1"], siginfo["T2"], name, shot))

            siglist.append(siginfo)

    return siglist


def get_data_with_file(shot, ti, tf, datafile, fdebug=False, **kwargs):
    """
    JET-SPECIFIC FUNCTION
    Obtains the base shot data for given shot / times, where base shot data
    must be specified in a text file.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg datafile: str. Name of file containing the DDA and DTYPE information to be extracted, along with averaging flags.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :kwarg conn: obj. Python MDS Connection object to grab data through. MANDATORY!

    :kwarg ttw: int. Integer flag indicating the plasma phase which this time window classifies as, for post-processing.

    :returns: dict. Implementation-specific object containing all data extracted and computed by this function.
    """
    conn = kwargs.get("conn", None)
    ttw = kwargs.get("ttw", None)
    if not isinstance(conn, connection.Connection):
        raise IOError("MDSplus connection object not provided to JET MDSplus adapter.")

    snum = None
    timei = None
    timef = None
    twtype = -1
    ntw = 1
    if isinstance(shot, (int, float)) and int(shot) > 0:
        snum = int(shot)
    if isinstance(ti, (int, float)):
        timei = float(ti)
    if isinstance(tf, (int, float)):
        timef = float(tf)
    if isinstance(ttw, (float, int)) and int(ttw) >= 0:
        twtype = int(ttw)

    RL = None
    if isinstance(conn, connection.Connection) and isinstance(snum, int) and datafile is not None and os.path.isfile(datafile):
        fields = dict()
        flags = dict()
        seqs = dict()
        uids = dict()
        repos = dict()
        with open(datafile, 'r') as ff:
            for line in ff:
                ufields = line.strip().split()
                if len(ufields) > 0 and not re.search('^#', ufields[0]):
                    udda = ufields[0].lower()
                    umap = "_".join(udda.split('/')).upper()
                    cline = False
                    if len(ufields) > 1 and not re.search('^#', ufields[1]) and not re.match('^-+$', ufields[1]):
                        umap = ufields[1].upper()
                    elif len(ufields) > 1 and re.search('^#', ufields[1]):
                        cline = True
                    uavg = True
                    usingle = False
                    udesc = False
                    if len(ufields) > 2 and not re.search('^#', ufields[2]) and not cline:
                        if int(ufields[2]) == 1:
                            uavg = False
                        elif int(ufields[2]) == 2:
                            uavg = False
                            usingle = True
                        elif int(value[2]) == 3:
                            uavg = False
                            usingle = True
                            udesc = True
                    elif len(ufields) > 2 and re.search('^#', ufields[2]):
                        cline = True
                    useq = None
                    if len(ufields) > 3 and not re.search('^#', ufields[3]) and not cline:
                        if re.search('[0-9]+', ufields[3]) and int(ufields[3]) >= 0:
                            useq = int(ufields[3])
                    elif len(ufields) > 3 and re.search('^#', ufields[3]):
                        cline = True
                    uuid = None
                    if len(ufields) > 4 and not re.search('^#', ufields[4]) and not cline:
                        uuid = ufields[4].lower()
                    elif len(ufields) > 4 and re.search('^#', ufields[4]):
                        cline = True
                    utop = udda.split('/')[0]

                    fields[udda] = umap
                    flags[udda] = (uavg, usingle, udesc)
                    seqs[udda] = useq
                    uids[udda] = uuid
                    repos[udda] = 'jpf' if utop in jpf_fields else 'ppf'

        if RL is None:
            RL = []
            RD = dict()
            RD["DEVICE"] = "JET"
            RD["INTERFACE"] = "MDSPLUS"
            RD["SHOT"] = snum
            RD["SHOTPHASE"] = twtype
            RD["WINDOW"] = 0
            RD["MAP"] = fields
            RD["FLAGS"] = flags
            RD["LAST_UPDATE"] = datetime.date
            deltat = float(timef - timei) / float(ntw)
            for jj in range(ntw):
                RD["T1"] = timei + float(jj) * deltat
                RD["T2"] = RD["T1"] + deltat
                RL.append(copy.deepcopy(RD))

        # Loop to grab data from PPF data system under the specified DDA/DTYPEs
        for key, tag in fields.items():

            if fdebug:
                print("extract_jet_mds.py: get_data_with_file(): Extracting %s..." % (key))

            avgflag = flags[key][0]
            fullflag = flags[key][1]
            descflag = flags[key][2]
            extras = {"conn": conn, "seq": seqs[key], "uid": uids[key], "repo": repos[key]}
            if descflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DESC"] if siglist is not None else None
                    RL[jj]["X_"+tag] = None
                    RL[jj]["T_"+tag] = None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            elif fullflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[0]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[0]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            else:
                siglist = getsig(snum, timei, timef, key, favg=avgflag, nsplit=ntw, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[jj]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[jj]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[jj]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[jj]["PROV"] if siglist is not None else None

        if fdebug:
            print("extract_jet_mds.py: get_data_with_file() completed.")

    return RL


def get_data_with_dict(shot, ti, tf, datadict, nwindows=None, fdebug=False, **kwargs):
    """
    JET-SPECIFIC FUNCTION
    Obtains the base shot data for given shot / times, where base shot data
    must be specified in a text file.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg datadict: dict. Python dictionary containing the DDA and DTYPE information to be extracted as keys, along with a list of settings as values.

    :kwarg nwindows: int. Optional number of equal time windows to split data into, default is 1.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :kwarg conn: obj. Python MDS Connection object to grab data through. MANDATORY!

    :kwarg ttw: int. Integer flag indicating the plasma phase which this time window classifies as, for post-processing.

    :returns: dict. Implementation-specific object containing all data extracted and computed by this function.
    """
    conn = kwargs.get("conn", None)
    ttw = kwargs.get("ttw", None)
    if not isinstance(conn, connection.Connection):
        raise IOError("MDSplus connection object not provided to JET MDSplus adapter.")

    snum = None
    timei = None
    timef = None
    indict = None
    twtype = -1
    ntw = 1
    if isinstance(shot, (int, float)) and int(shot) > 0:
        snum = int(shot)
    if isinstance(ti, (int, float)):
        timei = float(ti)
    if isinstance(tf, (int, float)):
        timef = float(tf)
    if isinstance(datadict, dict):
        indict = datadict
    if isinstance(ttw, (float, int)) and int(ttw) >= 0:
        twtype = int(ttw)
    if isinstance(nwindows, (int, float)) and int(nwindows) > 0:
        ntw = int(nwindows)

    RL = None
    if isinstance(conn, connection.Connection) and isinstance(snum, int) and isinstance(indict, dict):
        fields = dict()
        flags = dict()
        seqs = dict()
        uids = dict()
        repos = dict()
        for key, value in indict.items():
            if isinstance(key, str) and isinstance(value, (list, tuple)):
                udda = key.lower()
                umap = "_".join(udda.split('/')).upper()
                if len(value) > 0 and not re.match('^-+$', value[0]):
                    umap = value[0].upper()
                uavg = True
                usingle = False
                udesc = False
                if len(value) > 1 and isinstance(value[1], str) and re.match(r'^[0-9]+$', value[1]):
                    if int(value[1]) == 1:
                        uavg = False
                    elif int(value[1]) == 2:
                        uavg = False
                        usingle = True
                    elif int(value[2]) == 3:
                        uavg = False
                        usingle = True
                        udesc = True
                useq = None
                if len(value) > 2 and isinstance(value[2], str):
                    if re.match(r'^[0-9]+$', value[2]) and int(value[2]) >= 0:
                        useq = int(value[2])
                uuid = None
                if len(value) > 3 and isinstance(value[3], str):
                    uuid = value[3].lower()
                utop = udda.split('/')[0]

                fields[udda] = umap
                flags[udda] = (uavg, usingle, udesc)
                seqs[udda] = useq
                uids[udda] = uuid
                repos[udda] = 'jpf' if utop in jpf_fields else 'ppf'

        if RL is None:
            RL = []
            RD = dict()
            RD["DEVICE"] = "JET"
            RD["INTERFACE"] = "MDSPLUS"
            RD["SHOT"] = snum
            RD["SHOTPHASE"] = twtype
            RD["WINDOW"] = 0
            RD["MAP"] = fields
            RD["FLAGS"] = flags
            RD["LAST_UPDATE"] = datetime.date
            deltat = float(timef - timei) / float(ntw)
            for jj in range(ntw):
                RD["T1"] = timei + float(jj) * deltat
                RD["T2"] = RD["T1"] + deltat
                RL.append(copy.deepcopy(RD))

        # Loop to grab data from PPF data system under the specified DDA/DTYPEs
        for key, tag in fields.items():

            if fdebug:
                print("extract_jet_mds.py: get_data_with_dict(): Extracting %s..." % (key))

            avgflag = flags[key][0]
            fullflag = flags[key][1]
            descflag = flags[key][2]
            extras = {"conn": conn, "seq": seqs[key], "uid": uids[key], "repo": repos[key]}
            if descflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DESC"] if siglist is not None else None
                    RL[jj]["X_"+tag] = None
                    RL[jj]["T_"+tag] = None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            elif fullflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[0]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[0]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            else:
                siglist = getsig(snum, timei, timef, key, favg=avgflag, nsplit=ntw, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[jj]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[jj]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[jj]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[jj]["PROV"] if siglist is not None else None

        if fdebug:
            print("extract_jet_mds.py: get_data_with_dict() completed.")

    return RL
