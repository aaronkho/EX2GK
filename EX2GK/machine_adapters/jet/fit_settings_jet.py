# Script with functions to define optimized kernel settings in standardized format
# Developer: Aaron Ho - 14/11/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools


def generate_ne_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting electron density profiles. Also sets 7th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for electron density profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for electron density profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For NE pedestal modelling, HRTS data
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 2.0e-1, 2.0e-1, 1.5e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-1], [1.0e0, 4.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
#        FS["FIT_KPARS"] = np.array([3.0e2, 4.0e-1, 2.5e-1, 1.5e-1, loc, 8.0e-1])
#        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e2, 3.0e-1, 2.0e-1, 1.0e-1], [5.0e2, 6.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([3.0e-1, 3.0e-1, 4.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 3.0e1], [5.0e-1, 5.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 4.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For NE pedestal modelling, LIDR data
    if opt == 2:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 4.0e-1, 2.5e-1, 1.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 2.0e-1, 1.0e-1], [1.0e0, 6.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
#        FS["FIT_KPARS"] = np.array([3.0e2, 4.0e-1, 2.5e-1, 1.5e-1, loc, 8.0e-1])
#        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e2, 3.0e-1, 2.0e-1, 1.0e-1], [5.0e2, 6.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 4.0e-1, 4.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 2.0e-1, 3.0e1], [1.0e0, 5.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For NE pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.5e-1, 2.0e-1, 1.25e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[3.0e-1, 3.0e-1, 1.0e-1, 1.0e-1], [7.0e-1, 4.0e-1, 3.0e-1, 1.5e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([1.0e-1, 1.5e-1, 3.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 3.0e1], [2.0e-1, 2.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 4.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general NE profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([4.5e-1, 5.0e-1, 1.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 4.0e-1, 5.0e0], [5.0e-1, 6.0e-1, 1.5e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([1.0e-1, 3.0e-1, 9.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[8.0e-2, 2.0e-1, 7.0e1], [1.2e-1, 4.0e-1, 1.0e2]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 5.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e0], [1.0e0, 5.0e-1, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 3.0e-1, 5.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[5.0e-1, 1.0e-1, 3.0e1], [1.0e0, 5.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_te_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting electron temperature profiles. Also sets 5th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for electron temperature profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for electron temperature profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For TE pedestal modelling, pedestal is present in data but small, HRTS data
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 2.0e-1, 2.0e-1, 1.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-1], [1.0e0, 4.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
#        FS["FIT_KPARS"] = np.array([3.0e2, 4.0e-1, 2.0e-1, 1.0e-1, loc, 8.0e-1])
#        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e2, 3.0e-1, 1.0e-1, 1.0e-1], [5.0e2, 6.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 6.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 2.0e1], [1.0e0, 1.0e0, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 2.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For TE pedestal modelling, pedestal is present in data but small, LIDR data
    if opt == 2:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([7.0e-1, 3.0e-1, 2.0e-1, 1.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 2.0e-1, 1.0e-1, 1.0e-1], [1.0e0, 5.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
#        FS["FIT_KPARS"] = np.array([3.0e2, 4.0e-1, 2.0e-1, 1.0e-1, loc, 8.0e-1])
#        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e2, 3.0e-1, 1.0e-1, 1.0e-1], [5.0e2, 6.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 6.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 2.0e1], [1.0e0, 1.0e0, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For TE pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.5e-1, 3.5e-1, 1.5e-1, 1.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 3.0e-1, 1.0e-1, 1.0e-1], [7.0e-1, 4.0e-1, 2.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 2.0e1], [4.0e-1, 4.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 2.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general TE profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([4.5e-1, 5.0e-1, 1.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 4.0e-1, 5.0e0], [5.0e-1, 6.0e-1, 2.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([1.0e-1, 2.0e-1, 6.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[8.0e-2, 1.0e-1, 5.0e1], [1.2e-1, 3.0e-1, 7.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 5.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 5.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 2.0e1], [1.0e0, 1.0e0, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 5
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_ni_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting main ion density profiles. Also sets 7th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for main ion density profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for main ion density profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For NI pedestal modelling
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.5e-1, 1.5e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 2.0e-1, 2.0e-1, 1.0e-1], [1.0e0, 5.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([9.0e-1, 3.0e-1, 4.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[7.0e-1, 1.0e-1, 3.0e1], [1.0e0, 5.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For NI pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([4.5e-1, 3.0e-1, 2.0e-1, 2.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 2.0e-1, 2.0e-1, 1.5e-1], [5.0e-1, 4.0e-1, 3.0e-1, 2.5e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e1], [3.0e-1, 3.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general NI profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([2.5e-1, 3.0e-1, 5.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[2.0e-1, 2.0e-1, 4.0e0], [3.0e-1, 4.0e-1, 6.0e0]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 3.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([6.0e-2, 2.5e-1, 6.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[5.0e-2, 2.0e-1, 6.0e1], [1.0e-1, 3.0e-1, 7.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([9.0e-1, 3.0e-1, 4.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[7.0e-1, 1.0e-1, 3.0e1], [1.0e0, 5.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_ti_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting main ion temperature profiles. Also sets 5th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for main ion temperature profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for main ion temperature profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For TI pedestal modelling, pedestal is nearly imperceptible in the data but theoretically expected
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([8.0e-1, 3.0e-1, 2.0e-1, 6.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 2.0e-1, 1.0e-1, 5.0e-2], [1.0e0, 4.0e-1, 3.0e-1, 8.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 5.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 2.0e1], [1.0e0, 1.0e0, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For TI pedestal modelling without edge charge exchange data, more strict pedestal enforcement is often required
    elif opt == 2:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([6.0e-1, 3.0e-1, 2.0e-1, 4.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 2.0e-1, 1.0e-1, 3.0e-2], [1.0e0, 4.0e-1, 3.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-3 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 5.0e-2, 2.0e1], [1.0e0, 1.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For TI pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.5e-1, 3.5e-1, 1.5e-1, 5.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 3.0e-1, 1.0e-1, 5.0e-2], [6.0e-1, 4.0e-1, 2.0e-1, 8.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.5e-1, 2.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[2.0e-1, 1.0e-1, 2.0e1], [3.0e-1, 3.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general TI profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([3.5e-1, 4.0e-1, 2.5e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[3.0e-1, 3.0e-1, 2.0e1], [4.0e-1, 5.0e-1, 3.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 4.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.5e-1, 1.0e-1, 4.0e1], [2.5e-1, 3.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 5.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 2.0e1], [1.0e0, 1.0e0, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 5
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_nimp_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting impurity ion density profiles. Also sets 7th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for impurity ion density profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for impurity ion density profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For NIMP pedestal modelling, more for consistency rather than necessity
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.5e-1, 2.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 2.0e-1, 2.0e-1, 1.0e-1], [1.0e0, 4.0e-1, 3.0e-1, 3.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 2.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[3.0e-1, 1.0e-1, 1.0e1], [7.0e-1, 3.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For NIMP pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([4.5e-1, 3.0e-1, 2.0e-1, 2.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 2.0e-1, 2.0e-1, 1.5e-1], [5.0e-1, 4.0e-1, 3.0e-1, 2.5e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e1], [3.0e-1, 3.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general NIMP profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([2.5e-1, 3.0e-1, 5.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[2.0e-1, 2.0e-1, 4.0e0], [3.0e-1, 4.0e-1, 6.0e0]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 3.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([6.0e-2, 2.5e-1, 6.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[5.0e-2, 2.0e-1, 6.0e1], [1.0e-1, 3.0e-1, 7.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([3.0e-1, 7.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 7.0e-1, 2.0e1], [5.0e-1, 1.0e0, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 4.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_zimp_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting impurity charge profiles. Also sets 5th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for impurity charge profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for impurity ion density profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For ZIMP pedestal modelling, more for consistency rather than necessity
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.5e-1, 2.0e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 2.0e-1, 2.0e-1, 1.0e-1], [1.0e0, 4.0e-1, 3.0e-1, 3.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 2.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[3.0e-1, 1.0e-1, 1.0e1], [7.0e-1, 3.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For ZIMP pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.5e-1, 3.5e-1, 1.5e-1, 5.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 3.0e-1, 1.0e-1, 5.0e-2], [6.0e-1, 4.0e-1, 2.0e-1, 8.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.5e-1, 2.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[2.0e-1, 1.0e-1, 2.0e1], [3.0e-1, 3.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general TIMP profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([3.5e-1, 4.0e-1, 2.5e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[3.0e-1, 3.0e-1, 2.0e1], [4.0e-1, 5.0e-1, 3.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 4.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.5e-1, 1.0e-1, 4.0e1], [2.5e-1, 3.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 3.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([3.0e-1, 7.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 7.0e-1, 2.0e1], [5.0e-1, 1.0e0, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 5
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_timp_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting impurity ion temperature profiles. Also sets 5th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for impurity ion temperature profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for impurity ion temperature profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For TIMP pedestal modelling, pedestal is nearly imperceptible in the data but theoretically expected
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([8.0e-1, 3.0e-1, 2.0e-1, 6.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 2.0e-1, 1.0e-1, 5.0e-2], [1.0e0, 4.0e-1, 3.0e-1, 8.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 3.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 2.0e1], [1.0e0, 5.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For TIMP pedestal modelling without edge charge exchange data, more strict pedestal enforcement is often required
    elif opt == 2:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([6.0e-1, 3.0e-1, 2.0e-1, 4.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 2.0e-1, 1.0e-1, 3.0e-2], [1.0e0, 4.0e-1, 3.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-3 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 5.0e-2, 2.0e1], [1.0e0, 1.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For TIMP pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.5e-1, 3.5e-1, 1.5e-1, 5.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 3.0e-1, 1.0e-1, 5.0e-2], [6.0e-1, 4.0e-1, 2.0e-1, 8.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.5e-1, 2.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[2.0e-1, 1.0e-1, 2.0e1], [3.0e-1, 3.0e-1, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general TIMP profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([3.5e-1, 4.0e-1, 2.5e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[3.0e-1, 3.0e-1, 2.0e1], [4.0e-1, 5.0e-1, 3.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 4.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.5e-1, 1.0e-1, 4.0e1], [2.5e-1, 3.0e-1, 5.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 5.0e-1, 3.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 2.0e1], [1.0e0, 1.0e0, 4.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 5.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 5
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_vt_fit_settings(option=None, location=None, optflag=True, symflag=False):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting toroidal flow velocity profiles. Also sets 3rd-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for toroidal flow velocity profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is False.

    :returns: object. Standardized fit settings class instance with appropriate options for toroidal flow velocity profile fitting.
    """
    # Note that this function is not really used, as AF is the poloidally symmetric quantity related to this
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For VT pedestal modelling, more for consistency rather than necessity
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 4.0e-1, 2.0e-1, 1.0e-1, loc, 7.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 1.0e-1, 1.0e-1], [1.0e0, 5.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 4.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e1], [1.0e0, 5.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 2.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For VT pedestal modelling, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([2.5e-1, 4.0e-1, 2.0e-1, 1.4e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[2.0e-1, 3.0e-1, 1.0e-1, 1.0e-1], [3.0e-1, 5.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([3.0e-1, 1.5e-1, 1.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[2.0e-1, 1.0e-1, 1.0e1], [4.0e-1, 2.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 2.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general VT profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([3.0e-1, 3.5e-1, 2.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[2.0e-1, 3.0e-1, 1.5e1], [4.0e-1, 4.0e-1, 2.5e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 4.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 1.5e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.5e1], [3.0e-1, 2.0e-1, 2.5e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 4.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e1], [1.0e0, 5.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 3
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_af_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting angular frequency profiles. Also sets 3rd-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for angular frequency profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for angular frequency profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # For AF pedestal modelling, more for consistency rather than necessity
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 4.0e-1, 2.0e-1, 1.0e-1, loc, 7.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 1.0e-1, 1.0e-1], [1.0e0, 5.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 4.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e1], [1.0e0, 5.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 2.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For AF pedestal modelling, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([2.5e-1, 4.0e-1, 2.0e-1, 1.4e-1, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[2.0e-1, 3.0e-1, 1.0e-1, 1.0e-1], [3.0e-1, 5.0e-1, 3.0e-1, 2.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([3.0e-1, 1.5e-1, 1.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[2.0e-1, 1.0e-1, 1.0e1], [4.0e-1, 2.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 2.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general AF profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([3.0e-1, 3.5e-1, 2.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[2.0e-1, 3.0e-1, 1.5e1], [4.0e-1, 4.0e-1, 2.5e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 4.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 1.5e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.5e1], [3.0e-1, 2.0e-1, 2.5e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0], [1.0e0, 1.0e0, 1.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 4.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e1], [1.0e0, 5.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 3.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 3
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_q_fit_settings(option=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting safety factor profiles. Also sets 5th-order
    polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for safety factor profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for safety factor profile fitting.
    """
    FD = []
    opt = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)

    # For unconstrained EFIT Q profile
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 5.0e-1, 2.0e-1, 1.0e-1, 1.01e0, 6.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 1.0e0, 1.0e0, 1.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 1.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 5.0e0], [5.0e-1, 5.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For constrained EFIT Q profile
    elif opt == 2:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([1.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 1.01e0, 6.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 1.0e0, 1.0e0, 1.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 1.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 5.0e0], [5.0e-1, 5.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For typical Q profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([1.0e0, 5.0e-1, 1.5e-1, 3.0e-2, 1.01e0, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[8.0e-1, 4.0e-1, 1.0e-1, 1.0e-2], [1.2e1, 6.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 3.5e-1, 2.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 2.0e1], [3.0e-1, 4.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For generic Q profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([1.0e0, 6.0e-1, 4.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[8.0e-1, 5.0e-1, 3.0e1], [1.2e0, 7.0e-1, 5.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([1.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 2.0e-1, 1.5e1], [2.0e-1, 4.0e-1, 2.5e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([7.0e-1, 7.0e-1, 4.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 5.0e-1, 3.0e1], [1.0e0, 1.0e0, 5.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 1.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 5.0e0], [5.0e-1, 5.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 5
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_iq_fit_settings(option=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting inverse safety factor profiles. Also sets 5th-order
    polynomial fit settings as last resort. This is prefered over
    fitting directly on the safety factor due to extremely sharp
    feature at the plasma edge.

    :kwarg option: int. Choice of kernel settings to add for inverse safety factor profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for inverse safety factor profile fitting.
    """
    FD = []
    opt = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)

    # For unconstrained EFIT IQ profile
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([5.0e-1, 5.0e-1, 2.0e-1, 1.0e-1, 1.01e0, 6.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 1.0e0, 1.0e0, 1.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 1.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 5.0e0], [5.0e-1, 5.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For constrained EFIT IQ profile
    elif opt == 2:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([1.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 1.01e0, 6.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 1.0e0, 1.0e0, 1.0e-1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 1.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 5.0e0], [5.0e-1, 5.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For typical IQ profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "GwIG"
        FS["FIT_KPARS"] = np.array([1.0e0, 5.0e-1, 1.5e-1, 3.0e-2, 1.01e0, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[8.0e-1, 4.0e-1, 1.0e-1, 1.0e-2], [1.2e1, 6.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 3.5e-1, 2.5e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 3.0e-1, 2.0e1], [3.0e-1, 4.0e-1, 3.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # For general IQ profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([1.0e0, 6.0e-1, 4.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[8.0e-1, 5.0e-1, 3.0e1], [1.2e0, 7.0e-1, 5.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([1.0e-1, 3.0e-1, 2.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 2.0e-1, 1.5e1], [2.0e-1, 4.0e-1, 2.5e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "RQ"
        FS["FIT_KPARS"] = np.array([7.0e-1, 7.0e-1, 1.0e1])
        FS["FIT_KBNDS"] = np.atleast_2d([[5.0e-1, 5.0e-1, 5.0e0], [1.0e0, 1.0e0, 2.0e1]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = "RQ"
        FS["ERR_KPARS"] = np.array([2.0e-1, 2.0e-1, 1.0e1])
        FS["ERR_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 5.0e0], [5.0e-1, 5.0e-1, 2.0e1]]) if optflag else None
        FS["ERR_NRES"] = 5 if optflag else None
        FS["ERR_REGPAR"] = 10.0
        FS["ERR_ONAME"] = "adam"
        FS["ERR_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["ERR_EPSPAR"] = 1.0e-1 if optflag else -1
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 5
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_nbi_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting source profiles from neutral beam injection
    heating. Also sets 7th-order polynomial fit settings as last
    resort.

    :kwarg option: int. Choice of kernel settings to add for NBI source profile fitting.

    :kwarg location: float. Optional pedestal location in x coordinate to use in pedestal fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for NBI source profile fitting.
    """
    FD = []
    opt = None
    loc = 1.0
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # Accounts for density pedestal effects on deposition
    if opt == 1:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 3.0e-1, 2.0e-1, 1.0e-1, 3.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e0, 5.0e-1, 3.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For NBI source profiles with density pedestal, using statistical centroid from JET data as initial guess
    elif opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 2.0e-1, 1.0e-1, 1.0e-1, 2.0e-2, loc, 8.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 1.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [6.0e-1, 3.0e-1, 2.0e-1, 2.0e-1, 3.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For general NBI source profiles, using statistical centroid from JET data as initial guess
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([3.5e-1, 2.0e-1, 1.0e1, 2.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[3.0e-1, 1.0e-1, 8.0e0, 1.0e-3], [4.0e-1, 3.0e-1, 1.2e1, 5.0e-3]]) if optflag else None
        FS["FIT_NRES"] = 5 if otpflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-2])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-3], [1.0e0, 1.0e0, 1.0e1, 1.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_icrh_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting source profiles from ion cyclotron resonance
    heating. Also sets 7th-order polynomial fit settings as last
    resort.

    :kwarg option: int. Choice of kernel settings to add for ICRH source profile fitting.

    :kwarg location: float. Estimated deposition location for ICRH source profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for ICRH source profile fitting.
    """
    FD = []
    opt = None
    loc = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # Accounts for localized deposition
    if opt == 1 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([5.0e0, 5.0e-1, 2.0e-1, 1.0e-1, 3.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 7.0e-1, 2.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For localized ICRH source profiles, using statistical centroid from JET data as initial guess
    elif opt == 3 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([2.0e0, 2.5e-1, 7.0e-2, 9.0e-2, 2.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.5e0, 2.0e-1, 5.0e-2, 7.0e-2, 1.0e-2], [2.5e0, 3.0e-1, 1.0e-1, 1.0e-1, 3.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For general ICRH source profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-4], [1.0e0, 1.0e0, 1.0e1, 1.0e-3]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-4], [1.0e0, 1.0e0, 1.0e1, 1.0e-3]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_ecrh_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting source profiles from electron cyclotron resonance
    heating. Also sets 7th-order polynomial fit settings as last
    resort.

    :kwarg option: int. Choice of kernel settings to add for ECRH source profile fitting.

    :kwarg location: float. Estimated deposition location for ECRH source profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for ECRH source profile fitting.
    """
    FD = []
    opt = None
    loc = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # Accounts for localized deposition
    if opt == 1 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([5.0e0, 5.0e-1, 2.0e-1, 1.0e-1, 3.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 7.0e-1, 2.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For localized ECRH source profiles, using statistical centroid from JET data as initial guess (not tested)
    elif opt == 3 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([2.0e0, 2.5e-1, 7.0e-2, 9.0e-2, 3.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.5e0, 2.0e-1, 5.0e-2, 7.0e-1, 1.0e-2], [2.5e0, 3.0e-1, 1.0e-1, 1.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For general ECRH source profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-4], [1.0e0, 1.0e0, 1.0e1, 1.0e-3]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-4], [1.0e0, 1.0e0, 1.0e1, 1.0e-3]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_lh_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting source profiles from lower hybrid heating. Also 
    sets 7th-order polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for LH source profile fitting.

    :kwarg location: float. Estimated deposition location for LH source profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for LH source profile fitting.
    """
    FD = []
    opt = None
    loc = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # Accounts for localized deposition
    if opt == 1 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([5.0e0, 5.0e-1, 2.0e-1, 1.0e-1, 3.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 7.0e-1, 2.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For localized LH source profiles, using statistical centroid from JET data as initial guess (not tested)
    elif opt == 3 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([2.0e0, 2.5e-1, 7.0e-2, 9.0e-2, 3.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e0, 2.0e-1, 5.0e-2, 7.0e-2, 1.0e-2], [3.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For general LH source profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-2])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-3], [1.0e0, 1.0e0, 1.0e1, 1.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Default back-up kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-2])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-3], [1.0e0, 1.0e0, 1.0e1, 1.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_ohm_fit_settings(option=None, location=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting source profiles from ohmic heating. Also sets
    7th-order polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for ohmic source profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for ohmic source profile fitting.
    """
    FD = []
    opt = None
    loc = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)
    if isinstance(location, (float, int)):
        loc = float(location)

    # Accounts for localized deposition
    if opt == 1 and loc is not None:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(GwIG-n)"
        FS["FIT_KPARS"] = np.array([5.0e0, 5.0e-1, 2.0e-1, 1.0e-1, 3.0e-2, loc, 3.0e-1])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e0, 3.0e-1, 1.0e-1, 1.0e-1, 1.0e-2], [1.0e1, 7.0e-1, 2.0e-1, 2.0e-1, 5.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 1.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-2, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-1 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # For general ohmic source profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 5.5e-1, 3.0e0, 4.0e-4])
        FS["FIT_KBNDS"] = np.atleast_2d([[4.0e-1, 5.0e-1, 1.0e0, 3.0e-4], [7.0e-1, 6.0e-1, 1.0e1, 5.0e-4]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Default kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-3], [1.0e0, 1.0e0, 1.0e1, 1.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_rad_fit_settings(option=None, optflag=True, symflag=True):
    """
    JET-SPECIFIC FUNCTION
    Sets GPR1D-specific kernel selections and settings optimized
    for fitting source profiles from radiation profiles. Also sets
    7th-order polynomial fit settings as last resort.

    :kwarg option: int. Choice of kernel settings to add for radiation source profile fitting.

    :kwarg optflag: bool. Optional specification to skip hyperparameter optimization step of fitting routine.

    :kwarg symflag: bool. Optional specification of symmetry about zero for polynomial fit setting, default is True.

    :returns: object. Standardized fit settings class instance with appropriate options for ohmic source profile fitting.
    """
    FD = []
    opt = None
    if isinstance(option, (float, int)) and int(option) >= 0:
        opt = int(option)

    # For general radiation sink profiles, using statistical centroid from JET data as initial guess (not tested)
    if opt == 3:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([4.0e-1, 1.0e-1, 3.0e1, 5.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[3.0e-1, 1.0e-1, 2.5e1, 4.0e-3], [5.0e-1, 2.0e-1, 3.5e1, 6.0e-3]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Default kernel, most generally robust selection but lacks specific features
    else:
        FS = dict()
        FS["FTYPE"] = "GPR"
        FS["FIT_KNAME"] = "Sum(RQ-n)"
        FS["FIT_KPARS"] = np.array([5.0e-1, 1.0e-1, 3.0e0, 1.0e-3])
        FS["FIT_KBNDS"] = np.atleast_2d([[1.0e-1, 1.0e-2, 1.0e0, 1.0e-3], [1.0e0, 1.0e0, 1.0e1, 1.0e-2]]) if optflag else None
        FS["FIT_NRES"] = 5 if optflag else None
        FS["FIT_REGPAR"] = 2.0
        FS["FIT_ONAME"] = "adam"
        FS["FIT_OPARS"] = np.array([1.0e-1, 0.4, 0.8])
        FS["FIT_EPSPAR"] = 1.0e-2 if optflag else -1
        FS["ERR_KNAME"] = None
        FS["ERR_KPARS"] = None
        FS["ERR_KBNDS"] = None
        FS["ERR_NRES"] = None
        FS["ERR_REGPAR"] = 1.0
        FS["ERR_ONAME"] = None
        FS["ERR_OPARS"] = None
        FS["ERR_EPSPAR"] = None
        FD.append(FS)

    # Absolute last resort, always works but currently provides no error estimate
    FS = dict()
    FS["FTYPE"] = "POLY"
    FS["DEGREE"] = 7
    FS["XANCHORS"] = None
    FS["YANCHORS"] = None
    FS["OMITCST"] = False
    FS["OMITLIN"] = True if symflag else False
    FD.append(FS)

    return FD


def generate_fit_kernel_settings(stddata, fopt=True, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Selects the GPR1D fit settings options to be added to the input
    object depending on the profile data that is available and the
    characteristics of this data.

    :arg stddata: dict. Standardized object containing the processed profiles.

    :kwarg fopt: bool. Toggles use of JET optimized centroid kernel settings without additional optimization.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Standardized object with fit settings for each available profile inserted.
    """
    SD = None
    do_opt = True if fopt else False
    if isinstance(stddata, dict):
        SD = stddata

    if SD is not None and "CS_DIAG" in SD and re.match('^RHO((TOR)|(POL))N?$', SD["CS_DIAG"], flags=re.IGNORECASE):
        ftflag = True if "SHOTPHASE" in SD and SD["SHOTPHASE"] <= 0 else False
        symflag = True if "SYMMETRIC" in SD and SD["SYMMETRIC"] else False
        ne_ped_kopt = 0 if do_opt else 3
        te_ped_kopt = 0 if do_opt else 3

        # Pedestal detection in electron density, sharp gradient feature typically exists regardless of H-mode due to edge recycling
        ne_pedloc = np.NaN
        te_pedloc = np.NaN
        gflag = False
        if not ("USENESMOOTHFLAG" in SD and SD["USENESMOOTHFLAG"]) and "NERAW" in SD and SD["NERAW"].size > 0:
            xx = copy.deepcopy(SD["NERAWX"])
            xe = copy.deepcopy(SD["NERAWXEB"])
            yy = copy.deepcopy(SD["NERAW"])
            ye = copy.deepcopy(SD["NERAWEB"])
            bidx = SD["NEDIAG"].index("NEBC") if "NEBC" in SD["NEDIAG"] else -1
            bfilt = np.invert(SD["NEDMAP"] == bidx)
            efilt = (xx >= 0.8)
            bdiff = 0.0
            bminx = None
            xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
            ymax = yy[xmin[0]] if xmin.size > 0 else 7.0e19
            yedge = 0.0
            cfilt = np.all([efilt, bfilt], axis=0)
            if np.any(cfilt) and yy[cfilt].size > 1:
                peddata = np.vstack((xx[cfilt].flatten(), yy[cfilt].flatten()))
                peddata = peddata[:, np.argsort(peddata[0, :])]
                bdvec = np.diff(peddata[1, :].flatten()) / np.diff(peddata[0, :].flatten())
                bdiff = np.nanmin(bdvec)
                bdidx = np.where(bdvec == bdiff)[0][0]
                bminx = float(peddata[0, bdidx+1])
            if np.any(np.invert(efilt)):
                xtemp = xx[np.invert(efilt)]
                ytemp = yy[np.invert(efilt)]
                idxo = np.where(xtemp >= np.nanmax(xtemp))[0]
                yedge = ytemp[idxo[0]] if idxo.size > 0 else 0.0
            if bdiff <= -ymax or (ftflag and yedge > 1.5e19):
                ne_ped_kopt = 2 if np.nanmean(np.diff(np.sort(xx))) > 0.03 else 1
                ne_pedloc = bminx + 0.025 if bminx is not None and (bminx + 0.025) < 1.0 else 1.02
                gflag = True
                if not do_opt:
                    ne_ped_kopt = 3

        # Only attempt pedestal detection in electron temperature when time window is identified to be in the current flat-top phase
        if not ("USELMODEFLAG" in SD and SD["USELMODEFLAG"]) and "TERAW" in SD and SD["TERAW"].size > 0 and ftflag:
            xx = copy.deepcopy(SD["TERAWX"])
            xe = copy.deepcopy(SD["TERAWXEB"])
            yy = copy.deepcopy(SD["TERAW"])
            ye = copy.deepcopy(SD["TERAWEB"])
            bidx = SD["TEDIAG"].index("TEBC") if "TEBC" in SD["TEDIAG"] else -1
            bfilt = np.invert(SD["TEDMAP"] == bidx)
            efilt = (xx >= 0.8)
            bdiff = 0.0
            bminx = None
            xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
            ymax = yy[xmin[0]] if xmin.size > 0 else 5.0e3
            yedge = 0.0
            tflag = False
            cfilt = np.all([efilt, bfilt], axis=0)
            if np.any(efilt) and yy[efilt].size > 1:
                tflag = gflag
                peddata = np.vstack((xx[efilt].flatten(), yy[efilt].flatten()))
                peddata = peddata[:, np.argsort(peddata[0, :])]
                bdvec = np.diff(peddata[1, :].flatten()) / np.diff(peddata[0, :].flatten())
                bdiff = np.nanmin(bdvec)
                bdidx = np.where(bdvec == bdiff)[0][0]
                bminx = float(peddata[0, bdidx+1])
            if np.any(np.invert(efilt)):
                xtemp = xx[np.invert(efilt)]
                ytemp = yy[np.invert(efilt)]
                idxo = np.where(xtemp >= np.nanmax(xtemp))[0]
                yedge = ytemp[idxo[0]] if idxo.size > 0 else 0.0
            if bdiff <= -ymax or (tflag and yedge > 5.0e2):
                te_ped_kopt = 2 if np.nanmean(np.diff(np.sort(xx))) > 0.03 else 1
                te_pedloc = bminx + 0.025 if bminx is not None and (bminx + 0.025) < 1.0 else 1.02
                gflag = True
                if not do_opt:
                    te_ped_kopt = 3

        pedloc = float(np.nanmax([ne_pedloc, te_pedloc])) if np.isfinite(ne_pedloc) or np.isfinite(te_pedloc) else None
        if pedloc is None:
            ne_ped_kopt = 0 if do_opt else 3
            te_ped_kopt = 0 if do_opt else 3
        elif pedloc > 0.98:
            pedloc = 0.98

        if "NERAW" in SD and SD["NERAW"].size > 0:
            SD["NEGPSET"] = generate_ne_fit_settings(option=ne_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)
        if "TERAW" in SD and SD["TERAW"].size > 0:
            SD["TEGPSET"] = generate_te_fit_settings(option=te_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)
        if "TIRAW" in SD and SD["TIRAW"].size > 5:  # Not tested if this is fully valid
            ti_ped_kopt = te_ped_kopt if te_ped_kopt in [0, 3] else 2
            ti_pedloc = pedloc
            if ti_ped_kopt != 0:
                ti_pedloc = 0.8
                for ii in range(len(SD["TIDIAG"])):
                    if re.match(r'^((cx7[a-d])|(cxse))$', SD["TIDIAG"][ii], flags=re.IGNORECASE):
                        ti_ped_kopt = te_ped_kopt
                    elif not re.match(r'^tibc$', SD["TIDIAG"][ii], flags=re.IGNORECASE):
                        dfilt = (SD["TIDMAP"] == ii)
                        if np.any(dfilt):
                            ti_pedloc = np.nanmax([np.nanmax(SD["TIRAWX"][dfilt]), pedloc, 0.8])
                ti_pedloc = ti_pedloc + 0.025 if ti_ped_kopt != te_ped_kopt else pedloc
            SD["TIGPSET"] = generate_ti_fit_settings(option=ti_ped_kopt, location=ti_pedloc, optflag=do_opt, symflag=symflag)
        fnimp = False
        fzimp = False
        for ii in range(SD["NUMIMP"]):
            itag = ("%d" % (ii+1))
            if "NIMP"+itag+"RAW" in SD and SD["NIMP"+itag+"RAW"].size > 0:
                fnimp = True
            if "ZIMP"+itag+"RAW" in SD and SD["ZIMP"+itag+"RAW"].size > 0:
                fzimp = True
        if fnimp:
            nimp_ped_kopt = ne_ped_kopt if ne_ped_kopt in [0, 3] else 1
            SD["NIMPGPSET"] = generate_nimp_fit_settings(option=nimp_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)
        if fzimp:
            zimp_ped_kopt = ne_ped_kopt if ne_ped_kopt in [0, 3] else 1
            SD["ZIMPGPSET"] = generate_zimp_fit_settings(option=zimp_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)
        if "TIMPRAW" in SD and SD["TIMPRAW"].size > 0:
            timp_ped_kopt = te_ped_kopt if te_ped_kopt in [0, 3] else 2
            timp_pedloc = pedloc
            if timp_ped_kopt != 0:
                timp_pedloc = 0.8
                for ii in range(len(SD["TIMPDIAG"])):
                    if re.match(r'^((cx7[a-d])|(cxse))$', SD["TIMPDIAG"][ii], flags=re.IGNORECASE):
                        timp_ped_kopt = te_ped_kopt
                    elif not re.match(r'^tzbc$', SD["TIMPDIAG"][ii], flags=re.IGNORECASE):
                        dfilt = (SD["TIMPDMAP"] == ii)
                        if np.any(dfilt):
                            timp_pedloc = np.nanmax([np.nanmax(SD["TIMPRAWX"][dfilt]), pedloc, 0.8])
                timp_pedloc = timp_pedloc + 0.025 if timp_ped_kopt != te_ped_kopt else pedloc
            SD["TIMPGPSET"] = generate_timp_fit_settings(option=timp_ped_kopt, location=timp_pedloc, optflag=do_opt, symflag=symflag)
        if "AFRAW" in SD and SD["AFRAW"].size > 0:
            SD["AFGPSET"] = generate_af_fit_settings(option=te_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)
        if ("QRAW" in SD and SD["QRAW"].size > 0) or ("CQRAW" in SD and SD["CQRAW"].size > 0):
            SD["QGPSET"] = generate_q_fit_settings(option=te_ped_kopt, optflag=do_opt, symflag=symflag)
        if ("IQRAW" in SD and SD["IQRAW"].size > 0) or ("ICQRAW" in SD and SD["ICQRAW"].size > 0):
            SD["IQGPSET"] = generate_q_fit_settings(option=te_ped_kopt, optflag=do_opt, symflag=symflag)
        if "ZEFFRAW" in SD and SD["ZEFFRAW"].size > 0:
            SD["ZEFFGPSET"] = generate_zimp_fit_settings(option=ne_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)

        quantities = ["QE", "QI", "J", "TAU"]  # "SFI", "NFI", "WFI" added later and per source
        qquantities = copy.deepcopy(quantities)
        if "NUMFINBI" in SD:
            for ifastion in range(SD["NUMFINBI"]):
                itag = "%d" % (ifastion+1)
                qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
        for qtag in qquantities:
            if qtag+"NBIRAW" in SD and SD[qtag+"NBIRAW"].size > 0:
                nbi_ped_kopt = ne_ped_kopt if ne_ped_kopt in [0, 5] else 1
                SD[qtag+"NBIGPSET"] = generate_nbi_fit_settings(option=nbi_ped_kopt, location=pedloc, optflag=do_opt, symflag=symflag)
        qquantities = copy.deepcopy(quantities)
        if "NUMFIICRH" in SD:
            for ifastion in range(SD["NUMFIICRH"]):
                itag = "%d" % (ifastion+1)
                qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
        for qtag in qquantities:
            if qtag+"ICRHRAW" in SD and SD[qtag+"ICRHRAW"].size > 0:
                icrh_kopt = 1 if do_opt else 3
                depidx = np.where(SD[qtag+"ICRHRAW"] == np.nanmax(SD[qtag+"ICRHRAW"]))[0][0]
                deploc = float(SD[qtag+"ICRHRAWX"][depidx] - 0.01)
                SD[qtag+"ICRHGPSET"] = generate_icrh_fit_settings(option=icrh_kopt, location=deploc, optflag=do_opt, symflag=symflag)
        qquantities = copy.deepcopy(quantities)
        if "NUMFIECRH" in SD:
            for ifastion in range(SD["NUMFIECRH"]):
                itag = "%d" % (ifastion+1)
                qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
        for qtag in qquantities:
            if qtag+"ECRHRAW" in SD and SD[qtag+"ECRHRAW"].size > 0:
                ecrh_kopt = 1 if do_opt else 3
                depidx = np.where(SD[qtag+"ECRHRAW"] == np.nanmax(SD[qtag+"ECRHRAW"]))[0][0]
                deploc = float(SD[qtag+"ECRHRAWX"][depidx] - 0.01)
                SD[qtag+"ECRHGPSET"] = generate_ecrh_fit_settings(option=ecrh_kopt, location=deploc, optflag=do_opt, symflag=symflag)
        qquantities = copy.deepcopy(quantities)
        if "NUMFILH" in SD:
            for ifastion in range(SD["NUMFILH"]):
                itag = "%d" % (ifastion+1)
                qquantities.extend(["SFI"+itag, "NFI"+itag, "WFI"+itag])
        for qtag in qquantities:
            if qtag+"LHRAW" in SD and SD[qtag+"LHRAW"].size > 0:
                lh_kopt = 1 if do_opt else 3
                depidx = np.where(SD[qtag+"LHRAW"] == np.nanmax(SD[qtag+"LHRAW"]))[0][0]
                deploc = float(SD[qtag+"LHRAWX"][depidx] - 0.01)
                SD[qtag+"LHGPSET"] = generate_lh_fit_settings(option=lh_kopt, location=deploc, optflag=do_opt, symflag=symflag)
        for qtag in quantities:
            if qtag+"OHMRAW" in SD and SD[qtag+"OHMRAW"].size > 0:
                ohm_kopt = 1 if do_opt else 3
                ohm_pedloc = pedloc
                SD[qtag+"OHMGPSET"] = generate_ohm_fit_settings(option=ohm_kopt, location=ohm_pedloc, optflag=do_opt, symflag=symflag)
        if "QRADRAW" in SD and SD["QRADRAW"].size > 0:
            rad_kopt = 1 if do_opt else 3
            SD["QRADGPSET"] = generate_rad_fit_settings(option=rad_kopt, optflag=do_opt, symflag=symflag)

        SD["NOOPTFLAG"] = False if do_opt else True

    if fdebug:
        print("fit_settings_jet.py: generate_fit_kernel_settings() completed.")

    return SD
