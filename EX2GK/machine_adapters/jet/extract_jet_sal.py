# Script with functions to grab PPF data from JET server via Python 3.5 Single Access Layer
# Developer: Aaron Ho - 16/10/2017
#    Additional help for SAL at JET can be found at http://sal.jet.uk/ (not up yet)

# Required imports
import os
import sys
import numpy as np
import re
import copy
import datetime
import pickle
import socket
from scipy.interpolate import interp1d

current_host = socket.getfqdn()

# Required non-standard Python packages
sal = None
SALException = Exception
dataclass = None
fjet = True
if current_host.endswith(".jet.uk") or current_host.endswith(".jetdata.eu"):
    from jet.data import sal, dataclass
    SALException = sal.SALException
else:
    from sal import dataclass, core, client
    sal = client.SALClient('https://sal.jet.uk')
    SALException = core.exception.SALException
    fjet = False

# List of signals categories which belong to JPF system
jpf_fields = ['ah', 'da', 'db', 'dc', 'dd', 'de', 'df', 'dg', 'dh', 'di', 'dj', \
              'gs', 'pf', 'pl', 'rf', 'sa', 'sc', 'ss', 'tf', 'vc', 'yc']


def authenticate(username, password):
    sal.authenticate(user=username, password=password)

def getsig(shot, ti, tf, name, favg=True, nsplit=None, gdebug=False, **kwargs):
    """
    JET-SPECIFIC FUNCTION
    Obtain desired signal / data via Python 3.5 SAL interface, with optional
    averaging, from a single data field over the specified time window. A 
    typical application would need to call this function multiple times to
    grab all the required data.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg name: str. DDA and DTYPE of data to be extracted, in format '<DDA>/<DTYPE>'.

    :kwarg favg: bool. Flag to toggle averaging of the extracted data over the specified time window.

    :kwarg nsplit: int. Optional number of equal time windows to split data into, default is 1.

    :kwarg gdebug: bool.  Toggles printing of select debugging statements, useful for determining issues in workflow.

    :kwarg seq: int. Optional sequence number of data to be extracted within the JET PPF data system, default is 0.

    :kwarg uid: str. Optional user ID tag for accessing private entries within the JET PPF data system, default is jetppf.

    :kwarg repo: str. Optional selection of signal database, choice between PPF and JPF only, default is PPF.

    :returns: (array, array, array, float, float, int).
        Extracted data, vector of spatial or other coordinate points corresponding to extracted data, vector of
        time coordinate points corresponding to extracted data, time of first data point found, time of last
        data point found, number of data points found in terms of the time coordinate. Returns None if inputs are
        invalid, and an empty list if requested data is not found.
    """
    seq = kwargs.get("seq", None)
    uid = kwargs.get("uid", None)
    repo = kwargs.get("repo", "ppf")

    siglist = None
    sshot = '%d' % (int(shot))
    sseq = '%d' % (int(seq)) if isinstance(seq, (float, int)) else '0'
    numt = int(nsplit) if isinstance(nsplit, (float, int)) and int(nsplit) > 0 else 1
    if gdebug:
        print(shot, ti, tf, name)

    # Extract data from chosen adapter
    tsig = None
    isig = None
    uuid = 'jetppf'
    if isinstance(uid, str):
        uuid = uid
    rstr = 'ppf/signal/' + uuid +  '/' + name
    if isinstance(repo, str):
        if re.match(r'^ppf$', repo.strip(), flags=re.IGNORECASE):
            rstr = 'ppf/signal/' + uuid +  '/' + name
        if re.match(r'^jpf$', repo.strip(), flags=re.IGNORECASE):
            rstr = 'jpf/' + name + '/data'
    dstr = '/pulse/' + sshot + '/' + rstr
    if seq is not None:
        dstr = dstr + ':' + sseq
    try:
        isig = sal.list(dstr)
        tsig = sal.get(dstr)
    except SALException:
        isig = None
        tsig = None

    # Place data into standardized variables
    data = np.array([])
    tvec = None
    xvec = None
    desc = None
    uidf = None
    seqf = None
    if isinstance(tsig, dataclass.Signal):
        fscalar = False
        try:
            data = copy.deepcopy(tsig.data) if isinstance(tsig.data, np.ndarray) else np.array(copy.deepcopy(tsig.data))
        except AttributeError:
            fscalar = True
            data = copy.deepcopy(tsig.value) if isinstance(tsig.value, np.ndarray) else np.array([tsig.value])
        if fscalar:
            tvec = np.array([0.0])
        elif data.ndim == 1:
            tvec = np.array(tsig.dimensions[0].data).flatten()
        elif data.shape[0] == 1:
            tvec = np.array(tsig.dimensions[0].data).flatten()
            if tvec.size != 1:
                data = np.atleast_1d(np.squeeze(data))
        elif data.ndim >= 2:
            tvec = np.array(tsig.dimensions[0].data).flatten()
            xvec = np.array(tsig.dimensions[1].data).flatten()
        desc = tsig.description
        if isig is not None:
            uidf = uuid
            seqf = isig.revision_current
    if data.size == 0:
        print('   Requested signal %-10s from shot #%8d is empty.' % (name, shot))

    # Post-process and place into standardized structure
    if tvec is not None:
        siglist = []
        timei = tvec[0] if ti is None else ti
        timef = tvec[-1] if tf is None else tf
        deltat = float(timef - timei) / float(numt)

        for ii in range(numt):
            siginfo = dict()
            itimei = timei + float(ii) * deltat if deltat > 1.0e-6 else timei
            idx = np.all([(tvec >= itimei), (tvec <= (itimei + deltat))], axis=0)    # boolean array flagging desired data points
            siginfo["T1"] = float(itimei)
            siginfo["T2"] = float(itimei + deltat)
            prov = "JET PPF: SAL, %s, %s, seq.%d" % (name.upper(), uidf.lower(), seqf) if uidf is not None and seqf is not None else None

            # Automatically apply average over specified time window
            if np.any(idx):
                if xvec is None:
                    if favg:
                        siginfo["DATA"] = np.nanmean(data[idx]).flatten()
                    else:
                        siginfo["DATA"] = data[idx].copy()
                else:
                    if favg:
                        siginfo["DATA"] = np.nanmean(data[idx, :], axis=0) if data.shape[0] == tvec.size else np.nanmean(data[:, idx], axis=1)
                        siginfo["DATA"] = siginfo["DATA"].flatten()
                    else:
                        siginfo["DATA"] = data[idx, :].copy() if data.shape[0] == tvec.size else np.transpose(data[:, idx])
                siginfo["DATA"] = np.array(siginfo["DATA"], dtype=np.float64)
                siginfo["XVEC"] = np.array(xvec, dtype=np.float64) if xvec is not None else None
                siginfo["TVEC"] = np.array(tvec[idx], dtype=np.float64)
                siginfo["NAVG"] = siginfo["TVEC"].size
                siginfo["DESC"] = desc
                siginfo["PROV"] = prov
                if favg:
                    siginfo["TVEC"] = np.nanmean(siginfo["TVEC"])
            else:
                siginfo["DATA"] = None
                siginfo["XVEC"] = None
                siginfo["TVEC"] = None
                siginfo["NAVG"] = None
                siginfo["DESC"] = desc
                siginfo["PROV"] = prov
                if desc is None:
                    print('   Requested time slice [%8.5f, %8.5f] not in %-10s from shot #%8d.' % (siginfo["T1"], siginfo["T2"], name, shot))

            siglist.append(siginfo)

    return siglist


def get_data_with_file(shot, ti, tf, datafile, fdebug=False, **kwargs):
    """
    JET-SPECIFIC FUNCTION
    Obtains the base shot data for given shot / times, where base shot data
    must be specified in a text file.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg datafile: str. Name of file containing the DDA and DTYPE information to be extracted, along with averaging flags.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :kwarg ttw: int. Integer flag indicating the plasma phase which this time window classifies as, for post-processing.

    :returns: dict. Implementation-specific object containing all data extracted and computed by this function.
    """
    ttw = kwargs.get("ttw", None)

    snum = None
    timei = None
    timef = None
    twtype = -1
    ntw = 1
    if isinstance(shot, (int, float)) and int(shot) > 0:
        snum = int(shot)
    if isinstance(ti, (int, float)):
        timei = float(ti)
    if isinstance(tf, (int, float)):
        timef = float(tf)
    if isinstance(ttw, (float, int)) and int(ttw) >= 0:
        twtype = int(ttw)

    RL = None
    if isinstance(snum, int) and datafile is not None and os.path.isfile(datafile):
        if not fjet:
            username = kwargs.get("uid", None)
            password = kwargs.get("sid", None)
            if username is None or password is None:
                raise Exception("JET PPF access requires a valid username and password")
            authenticate(username, password)

        fields = dict()
        flags = dict()
        seqs = dict()
        uids = dict()
        repos = dict()
        trees = dict()
        with open(datafile, 'r') as ff:
            for line in ff:
                ufields = line.strip().split()
                if len(ufields) > 0 and not re.search('^#', ufields[0]):
                    udda = ufields[0].lower()
                    umap = "_".join(udda.split('/')).upper()
                    cline = False
                    if len(ufields) > 1 and not re.search('^#', ufields[1]) and not re.match('^-+$', ufields[1]):
                        umap = ufields[1].upper()
                    elif len(ufields) > 1 and re.search('^#', ufields[1]):
                        cline = True
                    uavg = True
                    usingle = False
                    udesc = False
                    if len(ufields) > 2 and not re.search('^#', ufields[2]) and not cline:
                        if int(ufields[2]) == 1:
                            uavg = False
                        elif int(ufields[2]) == 2:
                            uavg = False
                            usingle = True
                        elif int(ufields[2]) == 3:
                            uavg = False
                            usingle = True
                            udesc = True
                    elif len(ufields) > 2 and re.search('^#', ufields[2]):
                        cline = True
                    useq = None
                    if len(ufields) > 3 and not re.search('^#', ufields[3]) and not cline:
                        if re.search('[0-9]+', ufields[3]) and int(ufields[3]) >= 0:
                            useq = int(ufields[3])
                    elif len(ufields) > 3 and re.search('^#', ufields[3]):
                        cline = True
                    uuid = None
                    if len(ufields) > 4 and not re.search('^#', ufields[4]) and not cline:
                        uuid = ufields[4].lower()
                    elif len(ufields) > 4 and re.search('^#', ufields[4]):
                        cline = True
                    utop = udda.split('/')[0]

                    fields[udda] = umap
                    flags[udda] = (uavg, usingle, udesc)
                    seqs[udda] = useq
                    uids[udda] = uuid
                    repos[udda] = 'jpf' if utop in jpf_fields else 'ppf'
                    utree = uuid + '/' + utop if uuid is not None else 'jetppf/' + utop
                    if repos[udda] == 'jpf':
                        utree = utop
                    if utree not in trees:
                        trees[utree] = []
                    trees[utree].append(udda)

        if RL is None:
            RL = []
            RD = dict()
            RD["DEVICE"] = "JET"
            RD["INTERFACE"] = "SAL"
            RD["SHOT"] = snum
            RD["SHOTPHASE"] = twtype
            RD["WINDOW"] = 0
            RD["MAP"] = fields
            RD["FLAGS"] = flags
            RD["LAST_UPDATE"] = datetime.date
            deltat = float(timef - timei) / float(ntw)
            for jj in range(ntw):
                RD["T1"] = timei + float(jj) * deltat
                RD["T2"] = RD["T1"] + deltat
                RL.append(copy.deepcopy(RD))

        # Loop to check data from PPF using SAL functions before full query
        sshot = '%d' % (int(snum))
        dstr = '/pulse/' + sshot
        for key in trees:
            # SAL stores JPF signals as branches, which exists whether or not data is present - equivalent to let getsig() handle missing data
            if key not in jpf_fields:
                lobj = []
                try:
                    lstr = dstr + '/ppf/signal/' + key
                    lobj = sal.list(lstr).leaves
                except SALException:
                    for item in trees[key]:
                        if item in fields:
                            print('   Requested signal %-10s from shot #%8d is empty.' % (item, snum))
                            tag = fields[item]
                            fullflag = flags[item][1]
                            for jj in range(ntw):
                                RL[jj][tag] = None
                                RL[jj]["X_"+tag] = None
                                RL[jj]["T_"+tag] = None
                                RL[jj]["I_"+tag] = None
                            del fields[item]
                            del flags[item]
                            del seqs[item]
                            del uids[item]
                            del repos[item]
                else:
                    hlist = []
                    for idx in range(len(lobj)):
                        hlist.append(lobj[idx][0])
                    for item in trees[key]:
                        itemend = item.split('/')[-1]
                        if item in fields and itemend not in hlist:
                            print('   Requested signal %-10s from shot #%8d is empty.' % (item, snum))
                            tag = fields[item]
                            fullflag = flags[item][1]
                            for jj in range(ntw):
                                RL[jj][tag] = None
                                RL[jj]["X_"+tag] = None
                                RL[jj]["T_"+tag] = None
                                RL[jj]["I_"+tag] = None
                            del fields[item]
                            del flags[item]
                            del seqs[item]
                            del uids[item]
                            del repos[item]
            else:
                lobj = []
                try:
                    lstr = dstr + '/jpf/' + key
                    lobj = sal.list(lstr).branches
                except SALException:
                    for item in trees[key]:
                        if item in fields:
                            print('   Requested signal %-10s from shot #%8d is empty.' % (item, snum))
                            tag = fields[item]
                            fullflag = flags[item][1]
                            for jj in range(ntw):
                                RL[jj][tag] = None
                                RL[jj]["X_"+tag] = None
                                RL[jj]["T_"+tag] = None
                                RL[jj]["I_"+tag] = None
                            del fields[item]
                            del flags[item]
                            del seqs[item]
                            del uids[item]
                            del repos[item]

        # Loop to grab data from PPF data system under the specified DDA/DTYPEs
        for key, tag in fields.items():

            if fdebug:
                print("extract_jet_sal.py: get_data_with_file(): Extracting %s..." % (key))

            avgflag = flags[key][0]
            fullflag = flags[key][1]
            descflag = flags[key][2]
            extras = {"seq": seqs[key], "uid": uids[key], "repo": repos[key]}
            if descflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DESC"] if siglist is not None else None
                    RL[jj]["X_"+tag] = None
                    RL[jj]["T_"+tag] = None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            elif fullflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[0]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[0]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            else:
                siglist = getsig(snum, timei, timef, key, favg=avgflag, nsplit=ntw, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[jj]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[jj]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[jj]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[jj]["PROV"] if siglist is not None else None

        if fdebug:
            print("extract_jet_sal.py: get_data_with_file() completed.")

    return RL


def get_data_with_dict(shot, ti, tf, datadict, nwindows=None, fdebug=False, **kwargs):
    """
    JET-SPECIFIC FUNCTION
    Obtains the base shot data for given shot / times, where base shot data
    must be specified in a text file.

    :arg shot: int. Shot number within the JET PPF data system from which data will be extracted.

    :arg ti: float. Start of data extraction time window, referenced to time vector on JET PPF data system.

    :arg tf: float. End of data extraction time window, reference to time vector on JET PPF data system.

    :arg datadict: dict. Python dictionary containing the DDA and DTYPE information to be extracted as keys, along with a list of settings as values.

    :kwarg nwindows: int. Optional number of equal time windows to split data into, default is 1.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :kwarg ttw: int. Integer flag indicating the plasma phase which this time window classifies as, for post-processing.

    :returns: dict. Implementation-specific object containing all data extracted and computed by this function.
    """
    ttw = kwargs.get("ttw", None)

    snum = None
    timei = None
    timef = None
    indict = None
    twtype = -1
    ntw = 1
    if isinstance(shot, (int, float)) and int(shot) > 0:
        snum = int(shot)
    if isinstance(ti, (int, float)):
        timei = float(ti)
    if isinstance(tf, (int, float)):
        timef = float(tf)
    if isinstance(datadict, dict):
        indict = datadict
    if isinstance(ttw, (float, int)) and int(ttw) >= 0:
        twtype = int(ttw)
    if isinstance(nwindows, (int, float)) and int(nwindows) > 0:
        ntw = int(nwindows)

    RL = None
    if isinstance(snum, int) and isinstance(indict, dict):

        if not fjet:
            username = kwargs.get("uid", None)
            password = kwargs.get("sid", None)
            if username is None or password is None:
                raise Exception("JET PPF access requires a valid username and password")
            authenticate(username, password)

        fields = dict()
        flags = dict()
        seqs = dict()
        uids = dict()
        repos = dict()
        trees = dict()
        for key, value in indict.items():
            if isinstance(key, str) and isinstance(value, (list, tuple)):
                udda = key.lower()
                umap = "_".join(udda.split('/')).upper()
                if len(value) > 0 and not re.match('^-+$', value[0]):
                    umap = value[0].upper()
                uavg = True
                usingle = False
                udesc = False
                if len(value) > 1 and isinstance(value[1], str) and re.match(r'^[0-9]+$', value[1]):
                    if int(value[1]) == 1:
                        uavg = False
                    elif int(value[1]) == 2:
                        uavg = False
                        usingle = True
                    elif int(value[2]) == 3:
                        uavg = False
                        usingle = True
                        udesc = True
                useq = None
                if len(value) > 2 and isinstance(value[2], str):
                    if re.match(r'^[0-9]+$', value[2]) and int(value[2]) >= 0:
                        useq = int(value[2])
                uuid = None
                if len(value) > 3 and isinstance(value[3], str):
                    uuid = value[3].lower()
                utop = udda.split('/')[0]

                fields[udda] = umap
                flags[udda] = (uavg, usingle, udesc)
                seqs[udda] = useq
                uids[udda] = uuid
                repos[udda] = 'jpf' if utop in jpf_fields else 'ppf'
                utree = uuid + '/' + utop if uuid is not None else 'jetppf/' + utop
                if repos[udda] == 'jpf':
                    utree = utop
                if utree not in trees:
                    trees[utree] = []
                trees[utree].append(udda)

        if RL is None:
            RL = []
            RD = dict()
            RD["DEVICE"] = "JET"
            RD["INTERFACE"] = "SAL"
            RD["SHOT"] = snum
            RD["SHOTPHASE"] = twtype
            RD["WINDOW"] = 0
            RD["MAP"] = fields
            RD["FLAGS"] = flags
            RD["LAST_UPDATE"] = datetime.date
            deltat = float(timef - timei) / float(ntw)
            for jj in range(ntw):
                RD["T1"] = timei + float(jj) * deltat
                RD["T2"] = RD["T1"] + deltat
                RL.append(copy.deepcopy(RD))

        # Loop to check data from PPF using SAL functions before full query
        sshot = '%d' % (int(snum))
        dstr = '/pulse/' + sshot
        for key in trees:
            # SAL stores JPF signals as branches, which exists whether or not data is present - equivalent to let getsig() handle missing data
            if key not in jpf_fields:
                lobj = []
                try:
                    lstr = dstr + '/ppf/signal/' + key
                    lobj = sal.list(lstr).leaves
                except SALException:
                    for item in trees[key]:
                        if item in fields:
                            print('   Requested signal %-10s from shot #%8d is empty.' % (item, snum))
                            tag = fields[item]
                            fullflag = flags[item][1]
                            for jj in range(ntw):
                                RL[jj][tag] = None
                                RL[jj]["X_"+tag] = None
                                RL[jj]["T_"+tag] = None
                                RL[jj]["I_"+tag] = None
                            del fields[item]
                            del flags[item]
                            del seqs[item]
                            del uids[item]
                            del repos[item]
                else:
                    hlist = []
                    for idx in range(len(lobj)):
                        hlist.append(lobj[idx][0])
                    for item in trees[key]:
                        itemend = item.split('/')[-1]
                        if item in fields and itemend not in hlist:
                            print('   Requested signal %-10s from shot #%8d is empty.' % (item, snum))
                            tag = fields[item]
                            fullflag = flags[item][1]
                            for jj in range(ntw):
                                RL[jj][tag] = None
                                RL[jj]["X_"+tag] = None
                                RL[jj]["T_"+tag] = None
                                RL[jj]["I_"+tag] = None
                            del fields[item]
                            del flags[item]
                            del seqs[item]
                            del uids[item]
                            del repos[item]
            else:
                lobj = []
                try:
                    lstr = dstr + '/jpf/' + key
                    lobj = sal.list(lstr).branches
                except SALException:
                    for item in trees[key]:
                        if item in fields:
                            print('   Requested signal %-10s from shot #%8d is empty.' % (item, snum))
                            tag = fields[item]
                            fullflag = flags[item][1]
                            for jj in range(ntw):
                                RL[jj][tag] = None
                                RL[jj]["X_"+tag] = None
                                RL[jj]["T_"+tag] = None
                                RL[jj]["I_"+tag] = None
                            del fields[item]
                            del flags[item]
                            del seqs[item]
                            del uids[item]
                            del repos[item]

        # Loop to grab data from PPF data system under the specified DDA/DTYPEs
        for key, tag in fields.items():
            if fdebug:
                print("extract_jet_sal.py: get_data_with_dict(): Extracting %s..." % (key))
            avgflag = flags[key][0]
            fullflag = flags[key][1]
            descflag = flags[key][2]
            extras = {"seq": seqs[key], "uid": uids[key], "repo": repos[key]}
            if descflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DESC"] if siglist is not None else None
                    RL[jj]["X_"+tag] = None
                    RL[jj]["T_"+tag] = None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            elif fullflag:
                siglist = getsig(snum, None, None, key, favg=False, nsplit=None, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[0]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[0]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[0]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[0]["PROV"] if siglist is not None else None
            else:
                siglist = getsig(snum, timei, timef, key, favg=avgflag, nsplit=ntw, **extras)
                for jj in range(ntw):
                    RL[jj][tag] = siglist[jj]["DATA"] if siglist is not None else None
                    RL[jj]["X_"+tag] = siglist[jj]["XVEC"] if siglist is not None else None
                    RL[jj]["T_"+tag] = siglist[jj]["TVEC"] if siglist is not None else None
                    RL[jj]["I_"+tag] = siglist[jj]["PROV"] if siglist is not None else None

        if fdebug:
            print("extract_jet_sal.py: get_data_with_dict() completed.")

    return RL
