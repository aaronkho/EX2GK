# Adapter file for JET PPF data access - this should be the only imported file outside this directory
# Developer: Aaron Ho - 11/11/2017

# Required imports
import os
import sys
import socket
import importlib
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools
gmods = None
gmodm = None
gmodp = None
try:
    from EX2GK.machine_adapters.jet import extract_jet_sal as gmods
except (ImportError, OSError):
    gmods = None
try:
    from EX2GK.machine_adapters.jet import extract_jet_mds as gmodm
except ImportError:
    gmodm = None
try:
    from EX2GK.machine_adapters.jet import extract_jet_ppf as gmodp
except ImportError:
    gmodp = None
from EX2GK.machine_adapters.jet import process_jet as pmod
from EX2GK.machine_adapters.jet import standardize_jet as smod
from EX2GK.machine_adapters.jet import fit_settings_jet as kmod
from EX2GK.machine_adapters.jet import time_selector as tmod

current_host = socket.getfqdn()

# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def get_data(namelist):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides a single entry point into the adapter scripts
    developed to access and process JET PPF data. For ease of use, only
    this file should be imported into any user-defined script and only
    this function should be used to extract the desired data.

    The output container should be in the same generalized format and
    structure as other implementations, minus the kernel selection and
    fit settings for the GPR1D profile fitting tool. The concept is
    that any user-defined scripts defined strictly on the data
    structure here should be portable to any other implementation.

    An extra namelist can be provided via a Python dictionary object
    into the argument extras. The possible fields are listed below, and
    any other fields in the dictionary are ignored by these scripts.
        SHOTINFO      =  str. Contains shot number, time start, and time end, space-separated
        INTERFACE     =  str. Name of requested data access interface to be used
        USERNAME      =  str. Username within the JET data access system for remote access
        DATAFILE      =  str. Name of file containing DDA and DTYPE info, with averaging flags
        DATADICT      =  dict. Replaces DATAFILE argument to ingest a Python dictionary instead
        QUANTITIES    =  list. List of plasma quantities as strings for pre-defined extraction
        OUTPUTLEVEL   =  int. 1 = raw data, 2 = proc. raw data, 3 = std. data, 4 = with opt. GP settings
        SHOTPHASE     =  int. Phase of the discharge, used in processing decisions
        DEBUGFLAG     =  bool. Toggle printing of workflow progress statements, useful for debugging
        SELECTEQ      =  str. Equilibrium selection, used in processing decisions
        USERZFLAG     =  bool. Specify use of (r,z) diagnostic measurement coordinates for remapping with magnetic equilibrium
        ELMFILTFLAG   =  bool. Toggles application of ELM filtering based on edge spectrometer diagnostics
        GPCOORDFLAG   =  bool. Unify coordinate systems via GPR fitting, recommended to use!
        RSHIFTFLAG    =  bool. Applies ad-hoc radial shift in major radius coordinate at the height of the magnetic axis
        PURERAWFLAG   =  bool. Removes all data filters from workflow, uses raw data and time window average exactly as is
        STDMFLAG      =  bool. Use only the standard deviation of the mean for time window averaged errors, for testing only
        CDEBUG        =  bool. Print out raw coordinate data to files, for debugging
        EXPFILTFLAG   =  bool. Removes data filters designed purely from broad heuristic information about diagnostics
        EDGETEFLAG    =  bool. Enforces edge Ti = Te by pasting edge electron temperature measurements into ion temperature field
        NOOPTFLAG     =  bool. Specify use of preset GPR hyperparameters without additional optimization inside profile fitting step
        RHOEBFLAG     =  bool. Provide calculated rho errors in output, not meaningful at this moment
        USELMODEFLAG  =  bool. Forces GPR fit kernels to be initialized with L-mode settings
        USENESMOOTHFLAG = bool. Forces NE GPR fit kernel to be initialized without edge feature considerations
        USEECEFLAG    =  bool. Include ECE, ie. KK3 and ECM DDAs, measurements in electron temperature field
        POLFLAG       =  bool. Express standardized profiles with respect to rho_pol_norm

        NEEBMULT      =  float. Optional multiplier for electron density error bars
        TEEBMULT      =  float. Optional multiplier for electron temperature error bars
        NIEBMULT      =  float. Optional multiplier for main ion density error bars
        TIEBMULT      =  float. Optional multiplier for main ion temperature error bars
        NIMPEBMULT    =  float. Optional multiplier for impurity ion density error bars
        TIMPEBMULT    =  float. Optional multiplier for impurity ion temperature error bars
        AFEBMULT      =  float. Optional multiplier for rotational angular frequency error bars
        QEBMULT       =  float. Optional multiplier for safety factor error bars

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: list. List of Python dictionary or EX2GK custom class objects with data corresponding to the level of processing specified with outlevel argument.
    """
    argdict = dict()
    if isinstance(namelist, dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

    # Containers for output data
    outlist  = None

#    fdebug = True
    shot = None
    ti = None
    tf = None
    iname = 'sal'
    dfile = None
    ddict = None
    outlvl = 0
    ttw = 0
    if "SHOTINFO" in argdict and isinstance(argdict["SHOTINFO"], str):
        expstr = argdict["SHOTINFO"].strip()
        expinfo = expstr.split()
        if len(expinfo) > 0:
            shot = int(float(expinfo[0]))
        if len(expinfo) > 1:
            ti = float(expinfo[1])
        if len(expinfo) > 2:
            tf = float(expinfo[2])
    else:
        raise TypeError("Experimental information must be passed as a space-separable string.")
    if "INTERFACE" in argdict and isinstance(argdict["INTERFACE"], str):
        iname = argdict["INTERFACE"]
    if "DATAFILE" in argdict and isinstance(argdict["DATAFILE"], str) and os.path.isfile(argdict["DATAFILE"]):
        dfile = argdict["DATAFILE"]
    elif "DATADICT" in argdict and isinstance(argdict["DATADICT"], dict) and argdict["DATADICT"]:
        ddict = copy.deepcopy(argdict["DATADICT"])
    else:
        raise TypeError("Data extraction specification not provided or invalid for JET machine interface.")
    if "OUTPUTLEVEL" in argdict and isinstance(argdict["OUTPUTLEVEL"], (int, float)):
        outlvl = int(argdict["OUTPUTLEVEL"])
    if "SHOTPHASE" in argdict and isinstance(argdict["SHOTPHASE"], (int, float)) and int(argdict["SHOTPHASE"]) >= 0:
        ttw = int(argdict["SHOTPHASE"])
    fdebug = "DEBUGFLAG" in argdict and argdict["DEBUGFLAG"]

    # Note for scripting: Data extraction is designed to produce fatal error if 'shot' is not a non-zero positive integer.
    twlist = None
    if isinstance(dfile, str) and os.path.isfile(dfile):
        extras = {"ttw": ttw}
        if re.match(r'^sal$', iname, flags=re.IGNORECASE):
            if not current_host.endswith(".jet.uk") and not current_host.endswith(".jetdata.eu"):
                extras["uid"] = argdict["USERNAME"] if "USERNAME" in argdict and isinstance(argdict["USERNAME"], str) else None
                extras["sid"] = argdict["PASSWORD"] if "PASSWORD" in argdict and isinstance(argdict["PASSWORD"], str) else None
            twlist = gmods.get_data_with_file(shot, ti, tf, dfile, fdebug=fdebug, **extras)
        elif re.match(r'^mds(plus)?$', iname, flags=re.IGNORECASE):
            server = "mdsplus.jet.efda.org"
            if not current_host.endswith(".jet.uk"):
                if "USERNAME" in argdict and isinstance(argdict["USERNAME"], str):
                    server = "ssh://"+argdict["USERNAME"]+"@mdsplus.jetdata.eu"
                else:
                    raise EnvironmentError("Remote access to JET data requires a valid username argument.")
            extras["conn"] = gmodm.mdsconnect(server)
            twlist = gmodm.get_data_with_file(shot, ti, tf, dfile, fdebug=fdebug, **extras)
        elif re.match(r'^ppf$', iname, flags=re.IGNORECASE):
            twlist = gmodp.get_data_with_file(shot, ti, tf, dfile, fdebug=fdebug, **extras)
        else:
            raise ImportError("JET-specific adapter for %s data access interface not found." % (iname.upper()))
    elif isinstance(ddict, dict):
        nwindows = argdict["NWINDOWS"] if "NWINDOWS" in argdict and isinstance(argdict["NWINDOWS"], (int, float)) else 1
        extras = {"ttw": ttw}
        if re.match(r'^sal$', iname, flags=re.IGNORECASE):
            if not current_host.endswith(".jet.uk") and not current_host.endswith(".jetdata.eu"):
                extras["uid"] = argdict["USERNAME"] if "USERNAME" in argdict and isinstance(argdict["USERNAME"], str) else None
                extras["sid"] = argdict["PASSWORD"] if "PASSWORD" in argdict and isinstance(argdict["PASSWORD"], str) else None
            twlist = gmods.get_data_with_dict(shot, ti, tf, ddict, nwindows=nwindows, fdebug=fdebug, **extras)
        elif re.match(r'^mds(plus)?$', iname, flags=re.IGNORECASE):
            server = "mdsplus.jet.efda.org"
            if not current_host.endswith(".jet.uk"):
                if "USERNAME" in argdict and isinstance(argdict["USERNAME"], str):
                    server = "ssh://"+argdict["USERNAME"]+"@mdsplus.jetdata.eu"
                else:
                    raise EnvironmentError("Remote access to JET data requires a valid username argument.")
            extras["conn"] = gmodm.mdsconnect(server)
            twlist = gmodm.get_data_with_dict(shot, ti, tf, ddict, nwindows=nwindows, fdebug=fdebug, **extras)
        elif re.match(r'^ppf$', iname, flags=re.IGNORECASE):
            twlist = gmodp.get_data_with_dict(shot, ti, tf, ddict, nwindows=nwindows, fdebug=fdebug, **extras)
        else:
            raise ImportError("JET-specific adapter for %s data access interface not found." % (iname.upper()))

    if isinstance(twlist, list):

        outlist = []
        for jj in range(len(twlist)):

            datadict = twlist[jj]
            outobj = None
 
            if pmod is not None and isinstance(datadict, dict) and (outlvl == 0 or outlvl > 1):

                if "MAP" in datadict and datadict["MAP"] is not None:

                    eqselect = argdict["SELECTEQ"].lower() if "SELECTEQ" in argdict and isinstance(argdict["SELECTEQ"], str) else None
                    rzf = argdict["USERZFLAG"] if "USERZFLAG" in argdict else False
                    eff = argdict["ELMFILTFLAG"] if "ELMFILTFLAG" in argdict and ttw == 0 else False
                    gpc = argdict["GPCOORDFLAG"] if "GPCOORDFLAG" in argdict else False
                    sf = argdict["RSHIFTFLAG"] if "RSHIFTFLAG" in argdict else False
                    nsf = argdict["NSCALEFLAG"] if "NSCALEFLAG" in argdict else True
                    lmf = argdict["USELMODEFLAG"] if "USELMODEFLAG" in argdict else False
                    nef = argdict["USENESMOOTHFLAG"] if "USENESMOOTHFLAG" in argdict else False
                    rf = argdict["PURERAWFLAG"] if "PURERAWFLAG" in argdict else False
                    stdf = argdict["STDMFLAG"] if "STDMFLAG" in argdict else False
                    ecef = argdict["USEECEFLAG"] if "USEECEFLAG" in argdict else False
                    aef = argdict["ABSECEFLAG"] if "ABSECEFLAG" in argdict else False
                    qef = argdict["USEQEDGEFLAG"] if "USEQEDGEFLAG" in argdict else False
                    cdebug = True if "CDEBUG" in argdict and argdict["CDEBUG"] else False

                    # Unpack the necessary data for the next processing steps, discards all other raw data unless save flags specified
                    tempdict = pmod.transfer_generic_data(datadict, newstruct=None, \
                                                          userz=rzf, gpcoord=gpc, useshift=sf, usenscale=nsf, useece=ecef, absece=aef, useqedge=qef, \
                                                          uselmode=lmf, usenesmooth=nef, pureraw=rf, usesom=stdf,\
                                                          fdebug=fdebug)
                    tempdict = pmod.define_equilibrium(datadict, newstruct=tempdict, equilibrium=eqselect, fdebug=fdebug)
                    tempdict = pmod.define_time_filter(datadict, newstruct=tempdict, elmfilt=eff, fdebug=fdebug)
                    tempdict = pmod.unpack_general_data(datadict, newstruct=tempdict, fdebug=fdebug)
                    tempdict = pmod.unpack_config_data(datadict, newstruct=tempdict, fdebug=fdebug)
                    tempdict = pmod.unpack_coord_data(datadict, newstruct=tempdict, fdebug=fdebug)
#                    tempdict = pmod.unpack_advanced_geometry_data(datadict, newstruct=tempdict, fdebug=fdebug)
                    tempdict = pmod.calculate_coordinate_systems(tempdict, cdebug=cdebug, fdebug=fdebug)
                    tempdict = pmod.unpack_profile_data(datadict, newstruct=tempdict, fdebug=fdebug)
                    tempdict = pmod.unpack_source_data(datadict, newstruct=tempdict, fdebug=fdebug)
                    tempdict = pmod.apply_data_corrections(tempdict, fdebug=fdebug)

                    if isinstance(tempdict, dict):
                        datadict = copy.deepcopy(tempdict)

                else:
                    datadict = None

            if smod is not None and kmod is not None and datadict is not None and (outlvl == 0 or outlvl > 2):

                pf = argdict["POLFLAG"] if "POLFLAG" in argdict else False
                fxeb = argdict["RHOEBFLAG"] if "RHOEBFLAG" in argdict else False
                ef = argdict["EXPFILTFLAG"] if "EXPFILTFLAG" in argdict else True
                etef = argdict["EDGETEFLAG"] if "EDGETEFLAG" in argdict else True
                opf = False if "NOOPTFLAG" in argdict and argdict["NOOPTFLAG"] else True

                ebmults = dict()
                ebmults["NE"] = argdict["NEEBMULT"] if "NEEBMULT" in argdict else None
                ebmults["TE"] = argdict["TEEBMULT"] if "TEEBMULT" in argdict else None
                ebmults["NI"] = argdict["NIEBMULT"] if "NIEBMULT" in argdict else None
                ebmults["TI"] = argdict["TIEBMULT"] if "TIEBMULT" in argdict else None
                ebmults["NIMP"] = argdict["NIMPEBMULT"] if "NIMPEBMULT" in argdict else None
                ebmults["TIMP"] = argdict["TIMPEBMULT"] if "TIMPEBMULT" in argdict else None
                ebmults["AF"] = argdict["AFEBMULT"] if "AFEBMULT" in argdict else None
                ebmults["Q"] = argdict["QEBMULT"] if "QEBMULT" in argdict else None
                ebmults["ZEFF"] = argdict["ZEFFEBMULT"] if "ZEFFEBMULT" in argdict else None

                # Separates and formats the diagnostic data for filtering
                diagdict = smod.standardize_diagnostic_data(datadict, newstruct=None, useexp=ef, fdebug=fdebug)
                srcdict = smod.standardize_source_data(datadict, newstruct=None, useexp=ef, fdebug=fdebug)

                stddict = smod.transfer_generic_data(datadict, newstruct=None, usepol=pf, usexeb=fxeb, useexp=ef, fdebug=fdebug)
                stddict = smod.filter_standardized_diagnostic_data(diagdict, newstruct=stddict, userscales=ebmults, fdebug=fdebug)
                stddict = smod.combine_edge_diagnostic_data(stddict, fdebug=fdebug)
                stddict = smod.add_profile_boundary_data(stddict, pastete=etef, fdebug=fdebug)
                stddict = smod.transfer_standardized_source_data(srcdict, newstruct=stddict, fdebug=fdebug)
                stddict = smod.add_derivative_constraint_data(stddict, fdebug=fdebug)

                stddict = kmod.generate_fit_kernel_settings(stddict, fopt=opf, fdebug=fdebug)

                outobj = populate_classes(stddict, fdebug=fdebug)

                if fdebug:
                    print("jet - adapter.py: %s populated." % (type(outobj).__name__))
                    print("jet - adapter.py: get_data() completed!")

            else:
                outobj = copy.deepcopy(datadict)

            outlist.append(outobj)

    return outlist


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
# This function should probably be removed with the restructuring
def reset_fit_settings(inputdata, namelist):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides capability to reset the Gaussian process
    regression fit settings for re-fitting after data manipulation.

    :arg inputdata: dict. Python dictionary object with fully processed data according to the standardized adapter format.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: dict. Python dictionary object identical to input object except with GP fit settings added and / or reset to defaults.
    """
    outobj = None
    argdict = dict()
    if isinstance(inputdata, classes.EX2GKTimeWindow):
        outobj = copy.deepcopy(inputdata)
    elif isinstance(inputdata, dict):
        outobj = classes.EX2GKTimeWindow()
        outobj.importFromDict(copy.deepcopy(inputdata))
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))
    if isinstance(namelist, dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError('Invalid object passed into data access adapter for %s.' % (device.upper()))

    fdebug = "DEBUGFLAG" in argdict and argdict["DEBUGFLAG"]

    if kmod is not None and outobj is not None:
        opf = False if "NOOPTFLAG" in argdict and argdict["NOOPTFLAG"] else True

#        outdata = kmod.define_fit_settings(outdata, extra_info=argdict)
#        outdata = kmod.generate_fit_kernel_settings(outobj, fopt=opf, fdebug=fdebug)

    return outobj


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def get_time_traces(namelist):
    """
    JET-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides capability to extract time traces
    needed for time window selection.

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given implementation.

    :returns: dict. Python dictionary object with standardized time trace data.
    """
    outdata  = None
    if tmod is not None:
        outdata = tmod.get_time_data(namelist)
    return outdata


def populate_classes(stddict, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Populates the EX2GK classes with processed and standardized JET
    data, for use in the base program.

    :arg stddict: dict. Python dictionary object with fully processed data according to the standardized adapter format.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: obj. EX2GK custom time window class object filled with input data.
    """
    outobj = None

    if isinstance(stddict, dict):

        # Initialize required classes
        MC = classes.EX2GKMetaContainer()
        CC = classes.EX2GKCoordinateContainer()
        ZC = classes.EX2GKZeroDimensionalContainer()
        RC = classes.EX2GKRawDataContainer()
        EC = classes.EX2GKEquilibriumDataContainer()
        KC = classes.EX2GKFitSettingsContainer()
        FC = classes.EX2GKFlagContainer()

        # Transfer generic metadata
        device = stddict["DEVICE"] if "DEVICE" in stddict else "JET"
        interface = stddict["INTERFACE"] if "INTERFACE" in stddict and stddict["INTERFACE"] is not None else "UNKNOWN"
        shotnum = stddict["SHOT"] if "SHOT" in stddict else None
        t1 = stddict["T1"] if "T1" in stddict else None
        t2 = stddict["T2"] if "T2" in stddict else None
        window = stddict["WINDOW"] if "WINDOW" in stddict and stddict["WINDOW"] is not None else -1
        shotphase = stddict["SHOTPHASE"] if "SHOTPHASE" in stddict and stddict["SHOTPHASE"] is not None else -1
        eqsrc = stddict["EQLIST"][0].upper() if "EQLIST" in stddict and len(stddict["EQLIST"]) > 0 else "UNKNOWN"
        wallmat = stddict["WALLMAT"] if "WALLMAT" in stddict else "UNKNOWN"
        contmat = stddict["CONTMAT"] if "CONTMAT" in stddict else "UNKNOWN"
        pconfig = 1       # Assumed divertor configuration if not specified

        MC.setRequiredMetaData(device, interface, shotnum, t1, t2, window, shotphase, eqsrc, wallmat, contmat, pconfig)

        mspecs = []
        if "FH" in stddict or "FD" in stddict or "FT" in stddict:
            mtags = []
            fracs = np.array([])
            fracebs = np.array([])
            fprov = []
            for material in ["H", "D", "T"]:
                if "F"+material in stddict:
                    mtags.append(material)
                    fracs = np.hstack((fracs, stddict["F"+material]))
                    fracebs = np.hstack((fracebs, stddict["F"+material+"EB"]))
                    fprov.append(stddict["F"+material+"DS"])
            idxmax = np.where(fracs >= np.nanmax(fracs))[0][0]
            mspecs.append((mtags[idxmax], fracs[idxmax], fracebs[idxmax], fprov[idxmax]))
#            MC.addIonMetaData(name=mtags[idxmax], weight=fracs[idxmax])
            for matid in range(len(mtags)):
                if matid != idxmax and fracs[matid] >= 0.05:
                    mspecs.append((mtags[matid], fracs[matid], fracebs[matid], fprov[matid]))
#                    MC.addIonMetaData(name=mtags[matid], weight=fracs[matid])
#                ZC.addData("FRAC"+mtags[matid].upper(), fracs[matid], fracebs[matid], provenance=fprov[matid])
        elif "PLASMAT" in stddict and isinstance(stddict["PLASMAT"], str):
            mspecs.append((stddict["PLASMAT"].upper(), 1.0, 0.0, "EX2GK: Internal assumption"))
        else:
            # Assumed deuterium plasma if data not given
            mspecs.append(("D", 1.0, 0.0, "EX2GK: Internal assumption"))
        for imat in range(len(mspecs)):
            MC.addIonMetaData(name=mspecs[imat][0], weight=mspecs[imat][1])
            ZC.addData("FRAC"+mspecs[imat][0], mspecs[imat][1], mspecs[imat][2], provenance=mspecs[imat][3])

        ebmkeys = ["NE", "TE", "NI", "TI", "NIMP", "TIMP", "AF", "Q", "ZEFF"]
        for item in ebmkeys:
            if item+"EBMULT" in stddict and stddict[item+"EBMULT"] is not None:
                MC.addValue(item.upper()+"EBMULT", stddict[item+"EBMULT"])

        possible_boxes = ["4", "8"]
        if "BOXNBI" in stddict and len(stddict["BOXNBI"]) > 0:
            MC.addValue("CFGNBI", stddict["BOXNBI"])
            for tag in possible_boxes:
                pnbi = 0.0
                pnbieb = None
                pnbiprov = "EX2GK: Internal assumption"
                enbi = 0.0
                enbiprov = "EX2GK: Internal assumption"
                e1nbi = 0.0
                e1nbiprov = "EX2GK: Internal assumption"
                e2nbi = 0.0
                e2nbiprov = "EX2GK: Internal assumption"
                e3nbi = 0.0
                e3nbiprov = "EX2GK: Internal assumption"
                anbi = 2.0
                anbiprov = "EX2GK: Internal assumption"
                znbi = 1.0
                znbiprov = "EX2GK: Internal assumption"
                if tag in stddict["BOXNBI"]:
                    pnbi = stddict["PNBI"+tag] if "PNBI"+tag in stddict and stddict["PNBI"+tag] is not None else 0.0
                    pnbieb = stddict["PNBI"+tag+"EB"] if "PNBI"+tag+"EB" in stddict and stddict["PNBI"+tag+"EB"] is not None else None
                    pnbiprov = stddict["PNBI"+tag+"DS"] if "PNBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    enbi = stddict["ENBI"+tag] if "ENBI"+tag in stddict and stddict["ENBI"+tag] is not None else 0.0
                    enbiprov = stddict["ENBI"+tag+"DS"] if "ENBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    e1nbi = stddict["E1NBI"+tag] if "E1NBI"+tag in stddict and stddict["E1NBI"+tag] is not None else 0.0
                    e1nbiprov = stddict["E1NBI"+tag+"DS"] if "E1NBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    e2nbi = stddict["E2NBI"+tag] if "E2NBI"+tag in stddict and stddict["E2NBI"+tag] is not None else 0.0
                    e2nbiprov = stddict["E2NBI"+tag+"DS"] if "E2NBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    e3nbi = stddict["E3NBI"+tag] if "E3NBI"+tag in stddict and stddict["E3NBI"+tag] is not None else 0.0
                    e3nbiprov = stddict["E3NBI"+tag+"DS"] if "E3NBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    anbi = stddict["ANBI"+tag] if "ANBI"+tag in stddict and stddict["ANBI"+tag] is not None else 2.0   # Default to deuterium
                    anbiprov = stddict["ANBI"+tag+"DS"] if "ANBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    znbi = stddict["ZNBI"+tag] if "ZNBI"+tag in stddict and stddict["ZNBI"+tag] is not None else 1.0   # Default to deuterium
                    znbiprov = stddict["ZNBI"+tag+"DS"] if "ZNBI"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                ZC.addData("PNBI"+tag, pnbi, pnbieb, provenance=pnbiprov)
                ZC.addData("ENBI"+tag, enbi, None, provenance=enbiprov)
                ZC.addData("EFRAC1NBI"+tag, e1nbi, None, provenance=e1nbiprov)
                ZC.addData("EFRAC2NBI"+tag, e2nbi, None, provenance=e2nbiprov)
                ZC.addData("EFRAC3NBI"+tag, e3nbi, None, provenance=e3nbiprov)
                ZC.addData("ANBI"+tag, anbi, None, provenance=anbiprov)
                ZC.addData("ZNBI"+tag, znbi, None, provenance=znbiprov)

        possible_antenna = ["A", "B", "C", "D", "E"]
        if "ANTICRH" in stddict and len(stddict["ANTICRH"]) > 0:
            MC.addValue("CFGICRH", stddict["ANTICRH"])
            for tag in possible_antenna:
                picrh = 0.0
                picrheb = None
                picrhprov = "EX2GK: Internal assumption"
                phicrh = 0
                phicrhprov = "EX2GK: Internal assumption"
                fricrh = 0.0
                fricrhprov = "EX2GK: Internal assumption"
                if tag in stddict["ANTICRH"]:
                    picrh = stddict["PICRH"+tag] if "PICRH"+tag in stddict and stddict["PICRH"+tag] is not None else 0.0
                    picrheb = stddict["PICRH"+tag+"EB"] if "PICRH"+tag+"EB" in stddict and stddict["PICRH"+tag+"EB"] is not None else None
                    picrhprov = stddict["PICRH"+tag+"DS"] if "PICRH"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    phicrh = stddict["PHASICRH"+tag] if "PHASICRH"+tag in stddict and stddict["PHASICRH"+tag] is not None else 0
                    phicrhprov = stddict["PHASICRH"+tag+"DS"] if "PHASICRH"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                    fricrh = stddict["FREQICRH"+tag] if "FREQICRH"+tag in stddict and stddict["FREQICRH"+tag] is not None else 0.0
                    fricrhprov = stddict["FREQICRH"+tag+"DS"] if "FREQICRH"+tag+"DS" in stddict else "EX2GK: Internal assumption"
                ZC.addData("PICRH"+tag, picrh, picrheb, provenance=picrhprov)
                ZC.addData("PHASEICRH"+tag, phicrh, 0, provenance=phicrhprov, force_integer=True)
                ZC.addData("FREQICRH"+tag, fricrh, None, provenance=fricrhprov)

        if fdebug:
            print("jet - adapter.py: %s populated." % (type(MC).__name__))

        csdict = {"POLFLUXN": "PSIPOLN", "POLFLUX": "PSIPOL", "RHOPOLN": "RHOPOLN", "TORFLUXN": "PSITORN", "TORFLUX": "PSITOR", "RHOTORN": "RHOTORN", \
                  "RMAJORO": "RMAJORO", "RMAJORI": "RMAJORI", "RMINORO": "RMINORO", "RMINORI": "RMINORI", "FSVOL": "FSVOL", "FSAREA": "FSAREA"}
        #TODO: Add provenance for coordinate systems
        if "CS_BASE" in stddict and stddict["CS_BASE"] is not None and stddict["CS_BASE"] in csdict:
            if "CS_"+stddict["CS_BASE"] in stddict and stddict["CS_"+stddict["CS_BASE"]] is not None:
                jbase = stddict["CJ_BASE"] if "CJ_BASE" in stddict else stddict["CS_BASE"]
                rmag = stddict["RMAG"] if "RMAG" in stddict and stddict["RMAG"] is not None else 3.0
                CS = classes.EX2GKCoordinateSystem()
                CS.addCoordinate(csdict[stddict["CS_BASE"]], stddict["CS_"+stddict["CS_BASE"]], cs_prefix="")
                for key in csdict:
                    if not re.match(r'^'+stddict["CS_BASE"]+r'$', key, flags=re.IGNORECASE) and "CS_"+key in stddict and stddict["CS_"+key] is not None:
                        jvec = stddict["CJ_"+jbase+"_"+key].copy() if "CJ_"+jbase+"_"+key in stddict else None
                        evec = stddict["CE_"+key].copy() if "CE_"+key in stddict else None
                        CS.addCoordinate(csdict[key], stddict["CS_"+key], jvec, evec, cs_prefix="")
#                prefix = "C"
#                if prefix+"CS_BASE" in stddict and stddict[prefix+"CS_BASE"] is not None and stddict[prefix+"CS_BASE"] in csdict:
#                    jbase = stddict[prefix+"CJ_BASE"] if prefix+"CJ_BASE" in stddict else stddict[prefix+"CS_BASE"]
#                    if "CS_BASECORR" in stddict and stddict["CS_BASECORR"] is not None:
#                        jvec = stddict["CJ_"+jbase+"_BASECORR"].copy() if "CJ_"+jbase+"_BASECORR" in stddict else None
#                        evec = stddict["CE_BASECORR"].copy() if "CE_BASECORR" in stddict else None
#                        CS.addConversionCoordinate(prefix, stddict["CS_BASECORR"], jvec, evec, cs_suffix="")
#                    elif "CS_BASECORRO" in stddict and stddict["CS_BASECORRO"] is not None and "CS_BASECORRI" in stddict and stddict["CS_BASECORRI"] is not None:
#                        jvec = stddict["CJ_"+jbase+"_BASECORRO"].copy() if "CJ_"+jbase+"_BASECORRO" in stddict else None
#                        evec = stddict["CE_BASECORRO"].copy() if "CE_BASECORRO" in stddict else None
#                        CS.addConversionCoordinate(prefix, stddict["CS_BASECORRO"], jvec, evec, cs_suffix="O")
#                        jvec = stddict["CJ_"+jbase+"_BASECORRI"].copy() if "CJ_"+jbase+"_BASECORRI" in stddict else None
#                        evec = stddict["CE_BASECORRI"].copy() if "CE_BASECORRI" in stddict else None
#                        CS.addConversionCoordinate(prefix, stddict["CS_BASECORRI"], jvec, evec, cs_suffix="I")
#                    CS.computeMidplaneAveragedRadiusCoordinates(major_radius_magnetic_axis=rmag, overwrite=False)
#                    CC.addCoordinateSystemObject(CS)
#                    crmag = float(stddict["CRMAG"]) if "CRMAG" in stddict and stddict["CRMAG"] is not None else 3.0
#                    CCS = classes.EX2GKCoordinateSystem()
#                    CCS.addCoordinate(csdict[stddict[prefix+"CS_BASE"]], stddict[prefix+"CS_"+stddict[prefix+"CS_BASE"]], cs_prefix=prefix)
#                    for key in csdict:
#                        if not re.match(r'^'+stddict[prefix+"CS_BASE"]+r'$', key, flags=re.IGNORECASE) and prefix+"CS_"+key in stddict and stddict[prefix+"CS_"+key] is not None:
#                            jvec = stddict[prefix+"CJ_"+jbase+"_"+key].copy() if prefix+"CJ_"+jbase+"_"+key in stddict else None
#                            evec = stddict[prefix+"CE_"+key].copy() if prefix+"CE_"+key in stddict else None
#                            CCS.addCoordinate(csdict[key], stddict[prefix+"CS_"+key], jvec, evec, cs_prefix=prefix)
#                    CCS.computeMidplaneAveragedRadiusCoordinates(major_radius_magnetic_axis=crmag, overwrite=False)
#                    CC.addCoordinateSystemObject(CCS)
#                else:
                CS.computeMidplaneAveragedRadiusCoordinates(major_radius_magnetic_axis=rmag, overwrite=False)
                CC.addCoordinateSystemObject(CS)

        if fdebug:
            print("jet - adapter.py: %s populated." % (type(CC).__name__))

        # Transfer 0D data
        rmag = stddict["RMAG"] if "RMAG" in stddict and stddict["RMAG"] is not None else 3.0
        rmageb = stddict["RMAGEB"] if "RMAGEB" in stddict and stddict["RMAGEB"] is not None else None
        rmagprov = stddict["RMAGDS"] if "RMAGDS" in stddict else "EX2GK: Internal assumption"
        zmag = stddict["ZMAG"] if "ZMAG" in stddict and stddict["ZMAG"] is not None else 0.0
        zmageb = stddict["ZMAGEB"] if "ZMAGEB" in stddict and stddict["ZMAGEB"] is not None else None
        zmagprov = stddict["ZMAGDS"] if "ZMAGDS" in stddict else "EX2GK: Internal assumption"
        bmag = stddict["BMAG"] if "BMAG" in stddict and stddict["BMAG"] is not None else 3.0
        bmageb = stddict["BMAGEB"] + 0.04 if "BMAGEB" in stddict and stddict["BMAGEB"] is not None else 0.04
        bmagprov = stddict["BMAGDS"] if "BMAGDS" in stddict else "EX2GK: Internal assumption"
        ip = stddict["IP"] if "IP" in stddict and stddict["IP"] is not None else 3.0e6
        ipeb = stddict["IPEB"] if "IPEB" in stddict else None
        ipprov = stddict["IPDS"] if "IPDS" in stddict else "EX2GK: Internal assumption"
        ZC.addRequiredData(rmag, zmag, ip, bmag, rmageb, zmageb, ipeb, bmageb, rmagprov, zmagprov, ipprov, bmagprov)

        rgeo = stddict["RGEO"] if "RGEO" in stddict and stddict["RGEO"] is not None else 2.96
        rgeoeb = stddict["RGEOEB"] if "RGEOEB" in stddict and stddict["RGEOEB"] is not None else None
        rgeoprov = stddict["RGEODS"] if "RGEODS" in stddict else "EX2GK: Internal assumption"
        zgeo = stddict["ZGEO"] if "ZGEO" in stddict and stddict["ZGEO"] is not None else 0.0
        zgeoeb = stddict["ZGEOEB"] if "ZGEOEB" in stddict and stddict["ZGEOEB"] is not None else None
        zgeoprov = stddict["ZGEODS"] if "ZGEODS" in stddict else "EX2GK: Internal assumption"
        bvac = stddict["BVAC"] if "BVAC" in stddict and stddict["BVAC"] is not None else None
        bvaceb = stddict["BVACEB"] if "BVACEB" in stddict and stddict["BVACEB"] is not None else None
        bvacprov = None
        if bvac is not None:
            bvacprov = stddict["BVACDS"] if "BVACDS" in stddict else "Unknown"
        rvac = stddict["RVAC"] if "RVAC" in stddict and stddict["RVAC"] is not None else 2.96
        rvaceb = stddict["RVACEB"] if "RVACEB" in stddict and stddict["RVACEB"] is not None else None
        rvacprov = stddict["RVACDS"] if "RVACDS" in stddict else None
        fbtemp = CC[""]["PSIPOL"]["V"][-1] if "" in CC and "PSIPOL" in CC[""] else None   # But why?
        fatemp = CC[""]["PSIPOL"]["V"][0] if "" in CC and "PSIPOL" in CC[""] else None
        fbnd = stddict["FBND"] if "FBND" in stddict and stddict["FBND"] is not None else fbtemp
        fbndeb = stddict["FBNDEB"] if "FBNDEB" in stddict and stddict["FBNDEB"] is not None else None
        fbndprov = stddict["FBNDDS"] if "FBNDDS" in stddict else "EX2GK: Internal assumption"
        faxs = stddict["FAXS"] if "FAXS" in stddict and stddict["FAXS"] is not None else fatemp
        faxseb = stddict["FAXSEB"] if "FAXSEB" in stddict and stddict["FAXSEB"] is not None else None
        faxsprov = stddict["FAXSDS"] if "FAXSDS" in stddict else "EX2GK: Internal assumption"
        vloop = stddict["VLOOP"] if "VLOOP" in stddict and stddict["VLOOP"] is not None else fatemp
        vloopeb = stddict["VLOOPEB"] if "VLOOPEB" in stddict and stddict["VLOOPEB"] is not None else None
        vloopprov = stddict["VLOOPDS"] if "VLOOPDS" in stddict else "EX2GK: Internal assumption"
        elong = stddict["ELONG"] if "ELONG" in stddict and stddict["ELONG"] is not None else None
        elongeb = stddict["ELONGEB"] if "ELONGEB" in stddict and stddict["ELONGEB"] is not None and elong is not None else None
        elongprov = None
        if elong is not None:
            elongprov = stddict["ELONGDS"] if "ELONGDS" in stddict else "Unknown"
        triangl = stddict["TRIANGL"] if "TRIANGL" in stddict and stddict["TRIANGL"] is not None else None
        triangleb = stddict["TRIANGLEB"] if "TRIANGLEB" in stddict and stddict["TRIANGLEB"] is not None and triangl is not None else None
        trianglprov = None
        if triangl is not None:
            trianglprov = stddict["TRIANGLDS"] if "TRIANGLDS" in stddict else "Unknown"
        triangu = stddict["TRIANGU"] if "TRIANGU" in stddict and stddict["TRIANGU"] is not None else None
        triangueb = stddict["TRIANGUEB"] if "TRIANGUEB" in stddict and stddict["TRIANGUEB"] is not None and triangu is not None else None
        trianguprov = None
        if triangu is not None:
            trianguprov = stddict["TRIANGUDS"] if "TRIANGUDS" in stddict else "Unknown"
        ZC.addData("RGEO", rgeo, rgeoeb, provenance=rgeoprov)
        ZC.addData("ZGEO", zgeo, zgeoeb, provenance=zgeoprov)
        ZC.addData("BVAC", bvac, bvaceb, provenance=bvacprov)
        ZC.addData("RVAC", rvac, rvaceb, provenance=rvacprov)
        ZC.addData("PSIBND", fbnd, fbndeb, provenance=fbndprov)
        ZC.addData("PSIAXS", faxs, faxseb, provenance=faxsprov)
        ZC.addData("VLOOP", vloop, vloopeb, provenance=vloopprov)
        ZC.addData("ELONG", elong, elongeb, provenance=elongprov)
        ZC.addData("TRILO", triangl, triangleb, provenance=trianglprov)
        ZC.addData("TRIUP", triangu, triangueb, provenance=trianguprov)

        # The corrected coordinate system syntax is obsoleted
#        if "CCS_BASE" in stddict and stddict["CCS_BASE"] is not None:
#            crmag = float(stddict["CRMAG"]) if "CRMAG" in stddict and stddict["CRMAG"] is not None else 3.0
#            crmageb = float(stddict["CRMAGEB"]) if "CRMAGEB" in stddict and stddict["CRMAGEB"] is not None else None
#            crgeo = float(stddict["CRGEO"]) if "CRGEO" in stddict and stddict["CRGEO"] is not None else 2.96
#            crgeoeb = float(stddict["CRGEOEB"]) if "CRGEOEB" in stddict and stddict["CRGEOEB"] is not None else None
#            cbmag = float(stddict["CBMAG"]) if "CBMAG" in stddict and stddict["CBMAG"] is not None else 3.0
#            cbmageb = float(stddict["CBMAGEB"]) if "CBMAGEB" in stddict and stddict["CBMAGEB"] is not None else None
#            ZC.addData("CRMAG", crmag, crmageb)
#            ZC.addData("CZMAG", zmag, zmageb)       # Magnetic axis z coordinate not expected to deviate significantly
#            ZC.addData("CRGEO", crgeo, crgeoeb)
#            ZC.addData("CZGEO", zgeo, zgeoeb)       # Geometric center z coordinate not expected to deviate significantly
#            ZC.addData("CBMAG", cbmag, cbmageb)

        if "RMAJSHIFT" in stddict and stddict["RMAJSHIFT"] is not None:
            rshift = stddict["RMAJSHIFT"]
            rshifteb = stddict["RMAJSHIFTEB"] if "RMAJSHIFTEB" in stddict and stddict["RMAJSHIFTEB"] is not None else 0.0
            rshiftprov = stddict["RMAJSHIFTDS"] if "RMAJSHIFTDS" in stddict else "EX2GK: Internal calculation"
            ZC.addData("RSHIFT", rshift, rshifteb, provenance=rshiftprov)

        if "TESEPSHIFT" in stddict and stddict["TESEPSHIFT"] is not None:
            teseptarget = stddict["TESEPSHIFT"]
            teseptargeteb = 0.0
            teseptargetprov = stddict["TESEPSHIFTDS"] if "TESEPSHIFTDS" in stddict else "EX2GK: Internal calculation"
            ZC.addData("TESEPTARGET", teseptarget, teseptargeteb, provenance=teseptargetprov)

        if "NESEPSHIFT" in stddict and stddict["NESEPSHIFT"] is not None:
            neseptarget = stddict["NESEPSHIFT"]
            neseptargeteb = 0.0
            neseptargetprov = stddict["NESEPSHIFTDS"] if "NESEPSHIFTDS" in stddict else "EX2GK: Internal calculation"
            ZC.addData("NESEPTARGET", neseptarget, neseptargeteb, provenance=neseptargetprov)

        # Only count electorn temperature pedestal as real H-mode since density feature is always present
        if "RHOPEDTOPTE" in stddict and stddict["RHOPEDTOPTE"] is not None:
            pedtoploc = stddict["RHOPEDTOPTE"]
            pedtoploceb = stddict["RHOPEDTOPTEEB"] if "RHOPEDTOPTEEB" in stddict and stddict["RHOPEDTOPTEEB"] is not None else 0.0
            if "RHOPEDTOPNE" in stddict and stddict["RHOPEDTOPNE"] is not None:
                pedtoploc = (2.0 * stddict["RHOPEDTOPNE"] + pedtoploc) / 3.0
                pedtoploceb = np.sqrt(np.power(pedtoploceb, 2.0) + np.power(pedtoploc - stddict["RHOPEDTOPNE"], 2.0))
            pedtoplocprov = "EX2GK: Internal calculation"
            ZC.addData("RHOPEDTOP", pedtoploc, pedtoploceb, provenance=pedtoplocprov)

        if "NESCALE" in stddict and stddict["NESCALE"] is not None:
            nescale = stddict["NESCALE"]
            nescaleeb = stddict["NESCALEEB"] if "NESCALEEB" in stddict and stddict["NESCALEEB"] is not None else 0.0
            nescaleprov = "EX2GK: Internal calculation"
            ZC.addData("NESCALE", nescale, nescaleeb, provenance=nescaleprov)

        if "QEDGE" in stddict and stddict["QEDGE"] is not None:
            qedge = stddict["QEDGE"]
            qedgeeb = stddict["QEDGEEB"] if "QEDGEEB" in stddict and stddict["QEDGEEB"] is not None else 0.0
            qedgeprov = "EX2GK: Internal calculation"
            ZC.addData("QEDGE", qedge, qedgeeb, provenance=qedgeprov)

        for src in ["NBI", "ICRH", "ECRH", "LH", "OHM"]:
            if "P"+src in stddict and stddict["P"+src] is not None:
                pi = stddict["P"+src]
                pieb = stddict["P"+src+"EB"] if "P"+src+"EB" in stddict and stddict["P"+src+"EB"] is not None else 0.0
                piprov = stddict["P"+src+"DS"] if "P"+src+"DS" in stddict else "Unknown"
                ZC.addData("POWI"+src, pi, pieb, provenance=piprov)
                if "L"+src in stddict and stddict["L"+src] is not None:
                    pl = stddict["L"+src]
                    pleb = stddict["L"+src+"EB"] if "L"+src+"EB" in stddict and stddict["L"+src+"EB"] is not None else 0.0
                    plprov = stddict["L"+src+"DS"] if "L"+src+"DS" in stddict else "Unknown"
                    ZC.addData("POWL"+src, pl, pleb, provenance=plprov)
        src = "RAD"
        if "P"+src in stddict and stddict["P"+src] is not None:
            pl = stddict["P"+src]
            pleb = stddict["P"+src+"EB"] if "P"+src+"EB" in stddict and stddict["P"+src+"EB"] is not None else 0.0
            plprov = stddict["P"+src+"DS"] if "P"+src+"DS" in stddict else "Unknown"
            ZC.addData("POW"+src, pl, pleb, provenance=plprov)
        src = "RADBULK"
        if "P"+src in stddict and stddict["P"+src] is not None:
            pl = stddict["P"+src]
            pleb = stddict["P"+src+"EB"] if "P"+src+"EB" in stddict and stddict["P"+src+"EB"] is not None else 0.0
            plprov = stddict["P"+src+"DS"] if "P"+src+"DS" in stddict else "Unknown"
            ZC.addData("POW"+src, pl, pleb, provenance=plprov)

        # Insert gas fuelling data
        for ii in range(0,10):
            itag = "%d" % (ii)
            gtag = "GAS" + itag
            if gtag+"R" in stddict and stddict[gtag+"R"] is not None and gtag+"C" in stddict and stddict[gtag+"C"] is not None and gtag+"Z" in stddict and stddict[gtag+"Z"] is not None:
                gasz = stddict[gtag+"Z"][0]
                gasa = stddict[gtag+"A"][0] if gtag+"A" in stddict and stddict[gtag+"A"] is not None else None
                MC.addGasInjectionMetaData(gasz, gasa)
                grate = stddict[gtag+"R"]
                grateeb = stddict[gtag+"REB"] if gtag+"REB" in stddict and stddict[gtag+"REB"] is not None else 0.0
                grateprov = stddict[gtag+"RDS"] if gtag+"RDS" in stddict else "Unknown"
                gcum = stddict[gtag+"C"]
                gcumeb = stddict[gtag+"CEB"] if gtag+"CEB" in stddict and stddict[gtag+"CEB"] is not None else 0.0
                gcumprov = stddict[gtag+"CDS"] if gtag+"CDS" in stddict else "Unknown"
                ZC.addData("GASR"+itag, grate, grateeb, provenance=grateprov)
                ZC.addData("GASC"+itag, gcum, gcumeb, provenance=gcumprov)

        if "TWD" in stddict and stddict["TWD"] is not None:
            wp = stddict["TWD"]
            wpeb = stddict["TWDEB"] if "TWDEB" in stddict and stddict["TWDEB"] is not None else 0.05 * wp
            wpprov = stddict["TWDDS"] if "TWDDS" in stddict else "Unknown"
            ZC.addData("WEXP", wp, wpeb, provenance=wpprov)
        if "TWE" in stddict and stddict["TWE"] is not None:
            we = stddict["TWE"]
            weeb = stddict["TWEEB"] if "TWEEB" in stddict and stddict["TWEEB"] is not None else 0.05 * we
            weprov = stddict["TWEDS"] if "TWEDS" in stddict else "Unknown"
            ZC.addData("WMHD", we, weeb, provenance=weprov)
        if "LI3D" in stddict and stddict["LI3D"] is not None:
            lip = stddict["LI3D"]
            lipeb = stddict["LI3DEB"] if "LI3DEB" in stddict and stddict["LI3DEB"] is not None else 0.05 * wp
            lipprov = stddict["LI3DDS"] if "LI3DDS" in stddict else "Unknown"
            ZC.addData("LI3EXP", lip, lipeb, provenance=lipprov)
        if "LI3E" in stddict and stddict["LI3E"] is not None:
            lie = stddict["LI3E"]
            lieeb = stddict["LI3EEB"] if "LI3EEB" in stddict and stddict["LI3EEB"] is not None else 0.05 * we
            lieprov = stddict["LI3EDS"] if "LI3EDS" in stddict else "Unknown"
            ZC.addData("LI3MHD", lie, lieeb, provenance=lieprov)
        if "VAVG" in stddict and stddict["VAVG"] is not None:
            vavg = stddict["VAVG"]
            vavgeb = stddict["VAVGEB"] if "VAVGEB" in stddict and stddict["VAVGEB"] is not None else 0.0
            vavgprov = stddict["VAVGDS"] if "VAVGDS" in stddict else "Unknown"
            ZC.addData("VAVG", vavg, vavgeb, provenance=vavgprov)
        if "BETAND" in stddict and stddict["BETAND"] is not None:
            betan = stddict["BETAND"]
            betaneb = stddict["BETANDEB"] if "BETANDEB" in stddict and stddict["BETANDEB"] is not None else 0.0
            betanprov = stddict["BETANDDS"] if "BETANDDS" in stddict else "Unknown"
            ZC.addData("BETANEXP", betan, betaneb, provenance=betanprov)
        if "BETAPD" in stddict and stddict["BETAPD"] is not None:
            betap = stddict["BETAPD"]
            betapeb = stddict["BETAPDEB"] if "BETAPDEB" in stddict and stddict["BETAPDEB"] is not None else 0.0
            betapprov = stddict["BETAPDDS"] if "BETAPDDS" in stddict else "Unknown"
            ZC.addData("BETAPEXP", betap, betapeb, provenance=betapprov)
        if "BETATD" in stddict and stddict["BETATD"] is not None:
            betat = stddict["BETATD"] / 100.0
            betateb = stddict["BETATDEB"] / 100.0 if "BETATDEB" in stddict and stddict["BETATDEB"] is not None else 0.0
            betatprov = stddict["BETATDDS"] if "BETATDDS" in stddict else "Unknown"
            ZC.addData("BETATEXP", betat, betateb, provenance=betatprov)
        if "BETANE" in stddict and stddict["BETANE"] is not None:
            betan = stddict["BETANE"]
            betaneb = stddict["BETANEEB"] if "BETANEEB" in stddict and stddict["BETANEEB"] is not None else 0.0
            betanprov = stddict["BETANEDS"] if "BETANEDS" in stddict else "Unknown"
            ZC.addData("BETANMHD", betan, betaneb, provenance=betanprov)
        if "BETAPE" in stddict and stddict["BETAPE"] is not None:
            betap = stddict["BETAPE"]
            betapeb = stddict["BETAPEEB"] if "BETAPEEB" in stddict and stddict["BETAPEEB"] is not None else 0.0
            betapprov = stddict["BETAPEDS"] if "BETAPEDS" in stddict else "Unknown"
            ZC.addData("BETAPMHD", betap, betapeb, provenance=betapprov)
        if "BETATE" in stddict and stddict["BETATE"] is not None:
            betat = stddict["BETATE"] / 100.0    # Convert out of percentage
            betateb = stddict["BETATEEB"] / 100.0 if "BETATEEB" in stddict and stddict["BETATEEB"] is not None else 0.0
            betatprov = stddict["BETATEDS"] if "BETATEDS" in stddict else "Unknown"
            ZC.addData("BETATMHD", betat, betateb, provenance=betatprov)
        if "XSAREAP" in stddict and stddict["XSAREAP"] is not None:
            area = stddict["XSAREAP"]
            areaeb = stddict["XSAREAPEB"] if "XSAREAPEB" in stddict and stddict["XSAREAPEB"] is not None else 0.05 * area
            areaprov = stddict["XSAREADS"] if "XSAREADS" in stddict else "Unknown"
            ZC.addData("XSAREA", area, areaeb, provenance=areaprov)
        if "VOLP" in stddict and stddict["VOLP"] is not None:
            vol = stddict["VOLP"]
            voleb = stddict["VOLPEB"] if "VOLPEB" in stddict and stddict["VOLPEB"] is not None else 0.05 * vol
            volprov = stddict["VOLPDS"] if "VOLPDS" in stddict else "Unknown"
            ZC.addData("VOLUME", vol, voleb, provenance=volprov)
        if "NEUT" in stddict and stddict["NEUT"] is not None:
            rneut = stddict["NEUT"]
            rneuteb = stddict["NEUTEB"] if "NEUTEB" in stddict and stddict["NEUTEB"] is not None else 0.05 * rneut
            rneutprov = stddict["NEUTDS"] if "NEUTDS" in stddict else "Unknown"
            ZC.addData("NEUTRTOT", rneut, rneuteb, provenance=rneutprov)

        # Insert X-point and strike point location data, if available
        if "RXPL" in stddict and stddict["RXPL"] is not None and "ZXPL" in stddict and stddict["ZXPL"] is not None:
            rxpl = stddict["RXPL"]
            rxpleb = stddict["RXPLEB"] if "RXPLEB" in stddict and stddict["RXPLEB"] is not None else 0.0
            rxplprov = stddict["RXPLDS"] if "RXPLDS" in stddict else "Unknown"
            ZC.addData("RXPOINTL", rxpl, rxpleb, provenance=rxplprov)
            zxpl = stddict["ZXPL"]
            zxpleb = stddict["ZXPLEB"] if "ZXPLEB" in stddict and stddict["ZXPLEB"] is not None else 0.0
            zxplprov = stddict["ZXPLDS"] if "ZXPLDS" in stddict else "Unknown"
            ZC.addData("ZXPOINTL", zxpl, zxpleb, provenance=zxplprov)
        if "RXPU" in stddict and stddict["RXPU"] is not None and "ZXPU" in stddict and stddict["ZXPU"] is not None:
            rxpu = stddict["RXPU"]
            rxpueb = stddict["RXPUEB"] if "RXPUEB" in stddict and stddict["RXPUEB"] is not None else 0.0
            rxpuprov = stddict["RXPUDS"] if "RXPUDS" in stddict else "Unknown"
            ZC.addData("RXPOINTU", rxpu, rxpueb, provenance=rxpuprov)
            zxpu = stddict["ZXPU"]
            zxpueb = stddict["ZXPUEB"] if "ZXPUEB" in stddict and stddict["ZXPUEB"] is not None else 0.0
            zxpuprov = stddict["ZXPUDS"] if "ZXPUDS" in stddict else "Unknown"
            ZC.addData("ZXPOINTU", zxpu, zxpueb, provenance=zxpuprov)
        if "RSPIL" in stddict and stddict["RSPIL"] is not None and "ZSPIL" in stddict and stddict["ZSPIL"] is not None:
            rspil = stddict["RSPIL"]
            rspileb = stddict["RSPILEB"] if "RSPILEB" in stddict and stddict["RSPILEB"] is not None else 0.0
            rspilprov = stddict["RSPILDS"] if "RSPILDS" in stddict else "Unknown"
            ZC.addData("RSTRIKELIN", rspil, rspileb, provenance=rspilprov)
            zspil = stddict["ZSPIL"]
            zspileb = stddict["ZSPILEB"] if "ZSPILEB" in stddict and stddict["ZSPILEB"] is not None else 0.0
            zspilprov = stddict["ZSPILDS"] if "ZSPILDS" in stddict else "Unknown"
            ZC.addData("ZSTRIKELIN", zspil, zspileb, provenance=zspilprov)
        if "RSPOL" in stddict and stddict["RSPOL"] is not None and "ZSPOL" in stddict and stddict["ZSPOL"] is not None:
            rspol = stddict["RSPOL"]
            rspoleb = stddict["RSPOLEB"] if "RSPOLEB" in stddict and stddict["RSPOLEB"] is not None else 0.0
            rspolprov = stddict["RSPOLDS"] if "RSPOLDS" in stddict else "Unknown"
            ZC.addData("RSTRIKELOUT", rspol, rspoleb, provenance=rspolprov)
            zspol = stddict["ZSPOL"]
            zspoleb = stddict["ZSPOLEB"] if "ZSPOLEB" in stddict and stddict["ZSPOLEB"] is not None else 0.0
            zspolprov = stddict["ZSPOLDS"] if "ZSPOLDS" in stddict else "Unknown"
            ZC.addData("ZSTRIKELOUT", zspol, zspoleb, provenance=zspolprov)
        if "RSPIU" in stddict and stddict["RSPIU"] is not None and "ZSPIU" in stddict and stddict["ZSPIU"] is not None:
            rspiu = stddict["RSPIU"]
            rspiueb = stddict["RSPIUEB"] if "RSPIUEB" in stddict and stddict["RSPIUEB"] is not None else 0.0
            rspiuprov = stddict["RSPIUDS"] if "RSPIUDS" in stddict else "Unknown"
            ZC.addData("RSTRIKEUIN", rspiu, rspiueb, provenance=rspiuprov)
            zspiu = stddict["ZSPIU"]
            zspiueb = stddict["ZSPIUEB"] if "ZSPIUEB" in stddict and stddict["ZSPIUEB"] is not None else 0.0
            zspiuprov = stddict["ZSPIUDS"] if "ZSPIUDS" in stddict else "Unknown"
            ZC.addData("ZSTRIKEUIN", zspiu, zspiueb, provenance=zspiuprov)
        if "RSPOU" in stddict and stddict["RSPOU"] is not None and "ZSPOU" in stddict and stddict["ZSPOU"] is not None:
            rspou = stddict["RSPOU"]
            rspoueb = stddict["RSPOUEB"] if "RSPOUEB" in stddict and stddict["RSPOUEB"] is not None else 0.0
            rspouprov = stddict["RSPOUDS"] if "RSPOUDS" in stddict else "Unknown"
            ZC.addData("RSTRIKEUOUT", rspou, rspoueb, provenance=rspouprov)
            zspou = stddict["ZSPOU"]
            zspoueb = stddict["ZSPOUEB"] if "ZSPOUEB" in stddict and stddict["ZSPOUEB"] is not None else 0.0
            zspouprov = stddict["ZSPOUDS"] if "ZSPOUDS" in stddict else "Unknown"
            ZC.addData("ZSTRIKEUOUT", zspou, zspoueb, provenance=zspouprov)

        # Select lowest of two Z-effective measurements as standard flat Z-effective estimate
        ilwflag = True if re.match(r'^Be$', wallmat, flags=re.IGNORECASE) else False
        fzeff = 0.0
        fzeffeb = 0.0
        fzeffprov = "Unknown"
        if "ZEFV" in stddict and stddict["ZEFV"] is not None and (fzeff < 1.0 or stddict["ZEFV"] < fzeff):
            fzeff = stddict["ZEFV"]
            fzeffeb = stddict["ZEFVEB"] if "ZEFVEB" in stddict and stddict["ZEFVEB"] is not None else 0.1 * fzeff
            fzeffprov = stddict["ZEFVDS"] if "ZEFVDS" in stddict else "Unknown"
        if "ZEFH" in stddict and stddict["ZEFH"] is not None and (fzeff < 1.0 or stddict["ZEFH"] < fzeff):
            fzeff = stddict["ZEFH"]
            fzeffeb = stddict["ZEFHEB"] if "ZEFHEB" in stddict and stddict["ZEFHEB"] is not None else 0.1 * fzeff
            fzeffprov = stddict["ZEFHDS"] if "ZEFHDS" in stddict else "Unknown"
        if fzeff < 1.0:
            fzeff = 1.25 if ilwflag else 2.0
            fzeffeb = 0.05 if ilwflag else 0.1
            fzeffprov = "EX2GK: Internal assumption"
        ZC.addData("FZEFF", fzeff, fzeffeb, provenance=fzeffprov)
        FC.setFlag("FLATZEFF", True)

        if "PHTHR" in stddict and stddict["PHTHR"] is not None:
            powthresh = stddict["PHTHR"]
            powthresheb = stddict["PHTHREB"] if "PHTHREB" in stddict else 0.2 * powthresh
            powthreshprov = stddict["PHTHRDS"] if "PHTHRDS" in stddict else "Unknown"
            ZC.addData("POWTHRESHOLD", powthresh, powthresheb, provenance=powthreshprov)

        if fdebug:
            print("jet - adapter.py: %s populated." % (type(ZC).__name__))

        # Specify impurity-related quantities, defined using assumptions and known limitations
        zz1 = 6.0 if not ilwflag else 4.0
        zlim1 = 2.0 if not ilwflag else 1.2
        zz2 = None
        # Possible source of higher Z impurities in CW campaigns, although N, O, or Ne are also likely candidates, Ar chosen for lowest fuel dilution
        if fzeff > zlim1:
            if not ilwflag:
                zz2 = 18.0
            # Potential Ni contamination from ICRH antenna in ILW (C35 and beyond) yields higher measured ZEFF than expected
            else:
                zz2 = 28.0 if int(shotnum) > 87958 else 74.0

        prefix_list = CC.getPrefixes()
        prefix = prefix_list[0]

        varconv = {"NE": "NE", "TE": "TE", "NI": "NI", "TI": "TI", "NIMP": "NIMP", "TIMP": "TIMP", "AF": "AFTOR", "Q": "Q", "CQ": "Q"+prefix.upper(), \
                   "SFI": "SNI", "QE": "STE", "QI": "STI", "TAU": "SP", "J": "J", "NFI": "NFI", "WFI": "WFI", \
                   "ZEFF": "ZEFF", "ZIMP": "ZIMP", "IQ": "IOTA", "ICQ": "IOTA"+prefix.upper() }

        # Specify raw data for kinetic profiles, to be fitted in main routine
        cstemp = stddict["CS_DIAG"] if "CS_DIAG" in stddict and stddict["CS_DIAG"] is not None else "RHOTORN"
        csd = itemgetter(0)(ptools.define_coordinate_system(cstemp))
        wsxpflag = False
        if "NERAW" in stddict and stddict["NERAW"].size > 0:
            newname = varconv["NE"]
            prov = stddict["NEDS"] if "NEDS" in stddict and stddict["NEDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["NERAWX"], stddict["NERAW"], stddict["NEDIAG"], stddict["NEDMAP"], stddict["NERAWXEB"], stddict["NERAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DNERAW" in stddict and stddict["DNERAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DNERAWX"], stddict["DNERAW"], stddict["DNERAWXEB"], stddict["DNERAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        if "TERAW" in stddict and stddict["TERAW"].size > 0:
            newname = varconv["TE"]
            prov = stddict["TEDS"] if "TEDS" in stddict and stddict["TEDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["TERAWX"], stddict["TERAW"], stddict["TEDIAG"], stddict["TEDMAP"], stddict["TERAWXEB"], stddict["TERAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DTERAW" in stddict and stddict["DTERAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DTERAWX"], stddict["DTERAW"], stddict["DTERAWXEB"], stddict["DTERAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        if "TIRAW" in stddict and stddict["TIRAW"].size > 0:
            newname = varconv["TI"]
            prov = stddict["TIDS"] if "TIDS" in stddict and stddict["TIDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["TIRAWX"], stddict["TIRAW"], stddict["TIDIAG"], stddict["TIDMAP"], stddict["TIRAWXEB"], stddict["TIRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DTIRAW" in stddict and stddict["DTIRAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DTIRAWX"], stddict["DTIRAW"], stddict["DTIRAWXEB"], stddict["DTIRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        if "AFRAW" in stddict and stddict["AFRAW"].size > 0:
            newname = varconv["AF"]
            prov = stddict["AFDS"] if "AFDS" in stddict and stddict["AFDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["AFRAWX"], stddict["AFRAW"], stddict["AFDIAG"], stddict["AFDMAP"], stddict["AFRAWXEB"], stddict["AFRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DAFRAW" in stddict and stddict["DAFRAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DAFRAWX"], stddict["DAFRAW"], stddict["DAFRAWXEB"], stddict["DAFRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        for ii in range(stddict["NUMIMP"]):
            itag = "%d" % (ii+1)
            if "NIMP"+itag+"RAW" in stddict and stddict["NIMP"+itag+"RAW"].size > 0:
                if "WSXP" in stddict["NIMP"+itag+"DIAG"]:
                    wsxpflag = True
                newname = varconv["NIMP"]
                zimp = np.mean(stddict["ZIMP"+itag+"RAW"])[0]
                aimp = np.mean(stddict["AIMP"+itag+"RAW"])[0]
                elimp = stddict["MATIMP"+itag] if "MATIMP"+itag in stddict else None
                MC.addImpurityMetaData(charge=zimp, mass=aimp, name=elimp)
                nprov = stddict["NIMP"+itag+"DS"] if "NIMP"+itag+"DS" in stddict and stddict["NIMP"+itag+"DS"] is not None else "Unknown"
                ltag = "%d" % (MC["NUMIMP"])
                RC.addRawData(newname+ltag, stddict["NIMP"+itag+"RAWX"], stddict["NIMP"+itag+"RAW"], stddict["NIMP"+itag+"DIAG"], stddict["NIMP"+itag+"DMAP"], \
                              stddict["NIMP"+itag+"RAWXEB"], stddict["NIMP"+itag+"RAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=nprov)
                if "DNIMP"+itag+"RAW" in stddict and stddict["DNIMP"+itag+"RAW"].size > 0:
                    RC.addDerivativeConstraint(newname+ltag, stddict["DNIMP"+itag+"RAWX"], stddict["DNIMP"+itag+"RAW"], stddict["DNIMP"+itag+"RAWXEB"], stddict["DNIMP"+itag+"RAWEB"], \
                                               raw_x_coord=csd, raw_x_coord_prefix=prefix)
                if len(stddict["ZIMP"+itag+"RAW"]) == len(stddict["NIMP"+itag+"RAW"]):
                    newname = varconv["ZIMP"]
                    zprov = stddict["ZIMP"+itag+"DS"] if "ZIMP"+itag+"DS" in stddict and stddict["ZIMP"+itag+"DS"] is not None else "Unknown"
                    RC.addRawData(newname+ltag, stddict["NIMP"+itag+"RAWX"], stddict["ZIMP"+itag+"RAW"], stddict["NIMP"+itag+"DIAG"], stddict["NIMP"+itag+"DMAP"], \
                                  stddict["NIMP"+itag+"RAWXEB"], stddict["ZIMP"+itag+"RAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=zprov)
                    if "DZIMP"+itag+"RAW" in stddict and stddict["DZIMP"+itag+"RAW"].size > 0:
                        RC.addDerivativeConstraint(newname+ltag, stddict["DZIMP"+itag+"RAWX"], stddict["DZIMP"+itag+"RAW"], stddict["DZIMP"+itag+"RAWXEB"], stddict["DZIMP"+itag+"RAWEB"], \
                                                   raw_x_coord=csd, raw_x_coord_prefix=prefix)
                if zz2 is not None and np.abs(zimp - zz2) < 1.0:
                    zz2 = None
                if zz1 is not None and np.abs(zimp - zz1) < 0.5:
                    zz1 = zz2
                    zlim1 = None
                    zz2 = None
        if "TIMPRAW" in stddict and stddict["TIMPRAW"].size > 0:
            newname = varconv["TIMP"]
            prov = stddict["TIMPDS"] if "TIMPDS" in stddict and stddict["TIMPDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["TIMPRAWX"], stddict["TIMPRAW"], stddict["TIMPDIAG"], stddict["TIMPDMAP"], stddict["TIMPRAWXEB"], stddict["TIMPRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DTIMPRAW" in stddict and stddict["DTIMPRAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DTIMPRAWX"], stddict["DTIMPRAW"], stddict["DTIMPRAWXEB"], stddict["DTIMPRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        if "QRAW" in stddict and stddict["QRAW"].size > 0:
            newname = varconv["Q"]
            prov = stddict["QDS"] if "QDS" in stddict and stddict["QDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["QRAWX"], stddict["QRAW"], stddict["QDIAG"], stddict["QDMAP"], stddict["QRAWXEB"], stddict["QRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DQRAW" in stddict and stddict["DQRAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DQRAWX"], stddict["DQRAW"], stddict["DQRAWXEB"], stddict["DQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
#        if "CQRAW" in stddict and stddict["CQRAW"].size > 0:
#            newname = varconv["CQ"]
#            RC.addRawData(newname, stddict["CQRAWX"], stddict["CQRAW"], stddict["CQDIAG"], stddict["CQDMAP"], stddict["CQRAWXEB"], stddict["CQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
#            if "DCQRAW" in stddict and stddict["DCQRAW"].size > 0:
#                RC.addDerivativeConstraint(newname, stddict["DCQRAWX"], stddict["DCQRAW"], stddict["DCQRAWXEB"], stddict["DCQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        if "IQRAW" in stddict and stddict["IQRAW"].size > 0:
            newname = varconv["IQ"]
            prov = stddict["IQDS"] if "IQDS" in stddict and stddict["IQDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["IQRAWX"], stddict["IQRAW"], stddict["IQDIAG"], stddict["IQDMAP"], stddict["IQRAWXEB"], stddict["IQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DIQRAW" in stddict and stddict["DIQRAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DIQRAWX"], stddict["DIQRAW"], stddict["DIQRAWXEB"], stddict["DIQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
#        if "ICQRAW" in stddict and stddict["ICQRAW"].size > 0:
#            newname = varconv["ICQ"]
#            RC.addRawData(newname, stddict["ICQRAWX"], stddict["ICQRAW"], stddict["ICQDIAG"], stddict["ICQDMAP"], stddict["ICQRAWXEB"], stddict["ICQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
#            if "DICQRAW" in stddict and stddict["DICQRAW"].size > 0:
#                RC.addDerivativeConstraint(newname, stddict["DICQRAWX"], stddict["DICQRAW"], stddict["DICQRAWXEB"], stddict["DICQRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)
        if "ZEFFRAW" in stddict and stddict["ZEFFRAW"].size > 0:
            newname = varconv["ZEFF"]
            prov = stddict["ZEFFDS"] if "ZEFFDS" in stddict and stddict["ZEFFDS"] is not None else "Unknown"
            RC.addRawData(newname, stddict["ZEFFRAWX"], stddict["ZEFFRAW"], stddict["ZEFFDIAG"], stddict["ZEFFDMAP"], stddict["ZEFFRAWXEB"], stddict["ZEFFRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix, provenance=prov)
            if "DZEFFRAW" in stddict and stddict["DZEFFRAW"].size > 0:
                RC.addDerivativeConstraint(newname, stddict["DZEFFRAWX"], stddict["DZEFFRAW"], stddict["DZEFFRAWXEB"], stddict["DZEFFRAWEB"], raw_x_coord=csd, raw_x_coord_prefix=prefix)

        # Specify assumed impurities, defined using assumptions and known limitations
        if wsxpflag:
            MC.autoSetFillerImpurityData(leave_blank=True)
        else:
            if zz1 is not None:
                MC.addFillerImpurityMetaData(zz1, weight=1.0)
            else:
                MC.addFillerImpurityMetaData(28.0, weight=1.0)    # Use Ni if all suspected candidates are measured, chosen for low fuel dilution
            if zlim1 is not None and zz2 is not None:
                MC.addFillerImpurityLimitMetaData(zlim1, index=1)
                MC.addFillerImpurityMetaData(zz2, weight=1.0)

        if fdebug:
            print("jet - adapter.py: %s kinetic profiles populated." % (type(RC).__name__))

        # Specify raw data for source profiles, to be fitted in main routine
        mionlist = MC.getIonMetaData()
        cstemp = stddict["CS_SRC"] if "CS_SRC" in stddict and stddict["CS_SRC"] is not None else "RHOTORN"
        css = itemgetter(0)(ptools.define_coordinate_system(cstemp))
        for src in ["NBI", "ICRH", "ECRH", "LH", "OHM"]:
            if "QE"+src+"RAW" in stddict and stddict["QE"+src+"RAW"].size > 0:
                newname = varconv["QE"]
                xerr = stddict["QE"+src+"RAWXEB"] if "QE"+src+"RAWXEB" in stddict and stddict["QE"+src+"RAWXEB"] is not None else np.zeros(stddict["QE"+src+"RAWX"].shape)
                yerr = stddict["QE"+src+"RAWEB"] if "QE"+src+"RAWEB" in stddict and stddict["QE"+src+"RAWEB"] is not None else np.zeros(stddict["QE"+src+"RAW"].shape)
                diag = [stddict[src+"CODE"]]
                prov = stddict["QE"+src+"DS"] if "QE"+src+"DS" in stddict and stddict["QE"+src+"DS"] is not None else "Unknown"
                RC.addRawData(newname+src, stddict["QE"+src+"RAWX"], stddict["QE"+src+"RAW"], diag, stddict["QE"+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                if "DQE"+src+"RAW" in stddict and stddict["DQE"+src+"RAW"].size > 0:
                    RC.addDerivativeConstraint(newname+src, stddict["DQE"+src+"RAWX"], stddict["DQE"+src+"RAW"], stddict["DQE"+src+"RAWXEB"], stddict["DQE"+src+"RAWEB"], \
                                               raw_x_coord=css, raw_x_coord_prefix=prefix)
            if "QI"+src+"RAW" in stddict and stddict["QE"+src+"RAW"].size > 0:
                newname = varconv["QI"]
                xerr = stddict["QI"+src+"RAWXEB"] if "QI"+src+"RAWXEB" in stddict and stddict["QI"+src+"RAWXEB"] is not None else np.zeros(stddict["QI"+src+"RAWX"].shape)
                yerr = stddict["QI"+src+"RAWEB"] if "QI"+src+"RAWEB" in stddict and stddict["QI"+src+"RAWEB"] is not None else np.zeros(stddict["QI"+src+"RAW"].shape)
                diag = [stddict[src+"CODE"]]
                prov = stddict["QI"+src+"DS"] if "QI"+src+"DS" in stddict and stddict["QI"+src+"DS"] is not None else "Unknown"
                RC.addRawData(newname+src, stddict["QI"+src+"RAWX"], stddict["QI"+src+"RAW"], diag, stddict["QI"+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                if "DQI"+src+"RAW" in stddict and stddict["DQI"+src+"RAW"].size > 0:
                    RC.addDerivativeConstraint(newname+src, stddict["DQI"+src+"RAWX"], stddict["DQI"+src+"RAW"], stddict["DQI"+src+"RAWXEB"], stddict["DQI"+src+"RAWEB"], \
                                               raw_x_coord=css, raw_x_coord_prefix=prefix)
            if "TAU"+src+"RAW" in stddict and stddict["TAU"+src+"RAW"].size > 0:
                newname = varconv["TAU"]
                xerr = stddict["TAU"+src+"RAWXEB"] if "TAU"+src+"RAWXEB" in stddict and stddict["TAU"+src+"RAWXEB"] is not None else np.zeros(stddict["TAU"+src+"RAWX"].shape)
                yerr = stddict["TAU"+src+"RAWEB"] if "TAU"+src+"RAWEB" in stddict and stddict["TAU"+src+"RAWEB"] is not None else np.zeros(stddict["TAU"+src+"RAW"].shape)
                diag = [stddict[src+"CODE"]]
                prov = stddict["TAU"+src+"DS"] if "TAU"+src+"DS" in stddict and stddict["TAU"+src+"DS"] is not None else "Unknown"
                RC.addRawData(newname+src, stddict["TAU"+src+"RAWX"], stddict["TAU"+src+"RAW"], diag, stddict["TAU"+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                if "DTAU"+src+"RAW" in stddict and stddict["DTAU"+src+"RAW"].size > 0:
                    RC.addDerivativeConstraint(newname+src, stddict["DTAU"+src+"RAWX"], stddict["DTAU"+src+"RAW"], stddict["DTAU"+src+"RAWXEB"], stddict["DTAU"+src+"RAWEB"], \
                                               raw_x_coord=css, raw_x_coord_prefix=prefix)
            if "J"+src+"RAW" in stddict and stddict["J"+src+"RAW"].size > 0:
                newname = varconv["J"]
                xerr = stddict["J"+src+"RAWXEB"] if "J"+src+"RAWXEB" in stddict and stddict["J"+src+"RAWXEB"] is not None else np.zeros(stddict["J"+src+"RAWX"].shape)
                yerr = stddict["J"+src+"RAWEB"] if "J"+src+"RAWEB" in stddict and stddict["J"+src+"RAWEB"] is not None else np.zeros(stddict["J"+src+"RAW"].shape)
                diag = [stddict[src+"CODE"]]
                prov = stddict["J"+src+"DS"] if "J"+src+"DS" in stddict and stddict["J"+src+"DS"] is not None else "Unknown"
                RC.addRawData(newname+src, stddict["J"+src+"RAWX"], stddict["J"+src+"RAW"], diag, stddict["J"+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                if "DJ"+src+"RAW" in stddict and stddict["DJ"+src+"RAW"].size > 0:
                    RC.addDerivativeConstraint(newname+src, stddict["DJ"+src+"RAWX"], stddict["DJ"+src+"RAW"], stddict["DJ"+src+"RAWXEB"], stddict["DJ"+src+"RAWEB"], \
                                               raw_x_coord=css, raw_x_coord_prefix=prefix)

            if "NUMFI"+src in stddict and stddict["NUMFI"+src] > 0:
                for ifastion in range(stddict["NUMFI"+src]):
                    itag = "%d" % (ifastion+1)
                    afi = 1.0
                    zfi = 1.0
                    if "AFI"+itag+src in stddict and stddict["AFI"+itag+src] is not None and "ZFI"+itag+src in stddict and stddict["ZFI"+itag+src] is not None:
                        afi = stddict["AFI"+itag+src][0]
                        zfi = stddict["ZFI"+itag+src][0]
                        MC.addFastIonMetaData(zfi, afi, weight=0.0)
                        ntag = "%d" % MC["NUMFI"]
                        MC.addValue("SRCFI"+ntag, src)
                    if "SFI"+itag+src+"RAW" in stddict and stddict["SFI"+itag+src+"RAW"].size > 0:
                        iion = -1
                        for index, iname, ai, zi, iweight in mionlist:
                            if np.isclose(afi, ai) and np.isclose(zfi, zi):
                                iion = index
                        if iion < 0:
                            MC.addIonMetaData(zfi, afi, weight=0.0)
                            mionlist = MC.getIonMetaData()
                            iion = len(mionlist)
                        mtag = "%d" % (iion)
                        newname = varconv["SFI"] + mtag
                        xerr = stddict["SFI"+itag+src+"RAWXEB"] if "SFI"+itag+src+"RAWXEB" in stddict and stddict["SFI"+itag+src+"RAWXEB"] is not None else np.zeros(stddict["SFI"+itag+src+"RAWX"].shape)
                        yerr = stddict["SFI"+itag+src+"RAWEB"] if "SFI"+itag+src+"RAWEB" in stddict and stddict["SFI"+itag+src+"RAWEB"] is not None else np.zeros(stddict["SFI"+itag+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        prov = stddict["SFI"+itag+src+"DS"] if "SFI"+itag+src+"DS" in stddict and stddict["SFI"+itag+src+"DS"] is not None else "Unknown"
                        RC.addRawData(newname+src, stddict["SFI"+itag+src+"RAWX"], stddict["SFI"+itag+src+"RAW"], diag, stddict["SFI"+itag+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                        if "DSFI"+itag+src+"RAW" in stddict and stddict["DSFI"+itag+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src, stddict["DSFI"+itag+src+"RAWX"], stddict["DSFI"+itag+src+"RAW"], stddict["DSFI"+itag+src+"RAWXEB"], stddict["DSFI"+itag+src+"RAWEB"], \
                                                       raw_x_coord=css, raw_x_coord_prefix=prefix)
                    if "NFI"+itag+src+"RAW" in stddict and stddict["NFI"+itag+src+"RAW"].size > 0:
                        newname = varconv["NFI"] + itag
                        xerr = stddict["NFI"+itag+src+"RAWXEB"] if "NFI"+itag+src+"RAWXEB" in stddict and stddict["NFI"+itag+src+"RAWXEB"] is not None else np.zeros(stddict["NFI"+itag+src+"RAWX"].shape)
                        yerr = stddict["NFI"+itag+src+"RAWEB"] if "NFI"+itag+src+"RAWEB" in stddict and stddict["NFI"+itag+src+"RAWEB"] is not None else np.zeros(stddict["NFI"+itag+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
# You are here
                        prov = stddict["NFI"+itag+src+"DS"] if "NFI"+itag+src+"DS" in stddict and stddict["NFI"+itag+src+"DS"] is not None else "Unknown"
                        RC.addRawData(newname+src, stddict["NFI"+itag+src+"RAWX"], stddict["NFI"+itag+src+"RAW"], diag, stddict["NFI"+itag+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                        if "DNFI"+itag+src+"RAW" in stddict and stddict["DNFI"+itag+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src, stddict["DNFI"+itag+src+"RAWX"], stddict["DNFI"+itag+src+"RAW"], stddict["DNFI"+itag+src+"RAWXEB"], stddict["DNFI"+itag+src+"RAWEB"], \
                                                       raw_x_coord=css, raw_x_coord_prefix=prefix)
                    if "WFI"+itag+src+"RAW" in stddict and stddict["WFI"+itag+src+"RAW"].size > 0:
                        newname = varconv["WFI"] + itag
                        xerr = stddict["WFI"+itag+src+"RAWXEB"] if "WFI"+itag+src+"RAWXEB" in stddict and stddict["WFI"+itag+src+"RAWXEB"] is not None else np.zeros(stddict["WFI"+itag+src+"RAWX"].shape)
                        yerr = stddict["WFI"+itag+src+"RAWEB"] if "WFI"+itag+src+"RAWEB" in stddict and stddict["WFI"+itag+src+"RAWEB"] is not None else np.zeros(stddict["WFI"+itag+src+"RAW"].shape)
                        diag = [stddict[src+"CODE"]]
                        prov = stddict["WFI"+itag+src+"DS"] if "WFI"+itag+src+"DS" in stddict and stddict["WFI"+itag+src+"DS"] is not None else "Unknown"
                        RC.addRawData(newname+src, stddict["WFI"+itag+src+"RAWX"], stddict["WFI"+itag+src+"RAW"], diag, stddict["WFI"+itag+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
                        if "DWFI"+itag+src+"RAW" in stddict and stddict["DWFI"+itag+src+"RAW"].size > 0:
                            RC.addDerivativeConstraint(newname+src, stddict["DWFI"+itag+src+"RAWX"], stddict["DWFI"+itag+src+"RAW"], stddict["DWFI"+itag+src+"RAWXEB"], stddict["DWFI"+itag+src+"RAWEB"], \
                                                       raw_x_coord=css, raw_x_coord_prefix=prefix)

        src = "RAD"
        if "Q"+src+"RAW" in stddict and stddict["Q"+src+"RAW"].size > 0:
            newname = "ST"
            xerr = stddict["Q"+src+"RAWXEB"] if "Q"+src+"RAWXEB" in stddict and stddict["Q"+src+"RAWXEB"] is not None else np.zeros(stddict["Q"+src+"RAWX"].shape)
            yerr = stddict["Q"+src+"RAWEB"] if "Q"+src+"RAWEB" in stddict and stddict["Q"+src+"RAWEB"] is not None else np.zeros(stddict["Q"+src+"RAW"].shape)
            diag = [stddict[src+"CODE"]]
            prov = stddict["Q"+src+"DS"] if "Q"+src+"DS" in stddict and stddict["Q"+src+"DS"] is not None else "Unknown"
            RC.addRawData(newname+src, stddict["Q"+src+"RAWX"], stddict["Q"+src+"RAW"], diag, stddict["Q"+src+"DMAP"], xerr, yerr, raw_x_coord=css, raw_x_coord_prefix=prefix, provenance=prov)
            if "DQ"+src+"RAW" in stddict and stddict["DQ"+src+"RAW"].size > 0:
                RC.addDerivativeConstraint(newname+src, stddict["DQ"+src+"RAWX"], stddict["DQ"+src+"RAW"], stddict["DQ"+src+"RAWXEB"], stddict["DQ"+src+"RAWEB"], \
                                           raw_x_coord=css, raw_x_coord_prefix=prefix)

        if fdebug:
            print("jet - adapter.py: %s source profiles populated." % (type(RC).__name__))

        if "FS_RMAP" in stddict and "FS_ZMAP" in stddict and "FS_PFXMAP" in stddict:
            EC.addEquilibriumFluxMap(stddict["FS_PFXMAP"], stddict["FS_RMAP"], stddict["FS_ZMAP"], np.zeros(stddict["FS_PFXMAP"].shape), provenance=stddict["FS_PFXMAPDS"])
        if "FS_RBND" in stddict and "FS_ZBND" in stddict:
            reb = stddict["FS_RBNDEB"] if "FS_RBNDEB" in stddict else None
            zeb = stddict["FS_ZBNDEB"] if "FS_ZBNDEB" in stddict else None
            prov = stddict["FS_BNDDS"] if "FS_BNDDS" in stddict and stddict["FS_BNDDS"] is not None else "Unknown"
            EC.addEquilibriumBoundaryVector(stddict["FS_RBND"], stddict["FS_ZBND"], reb, zeb, provenance=prov)
        if "FS_PFN" in stddict and "FS_F" in stddict and "FS_FP" in stddict and "FS_P" in stddict and "FS_PP" in stddict and "FS_Q" in stddict:
            ff = stddict["FS_F"]
            pf = stddict["FS_P"]
            fp = stddict["FS_FP"]
            pp = stddict["FS_PP"]
            qv = stddict["FS_Q"]
            ffprov = stddict["FS_FDS"] if "FS_FDS" in stddict and stddict["FS_FDS"] is not None else "Unknown"
            pfprov = stddict["FS_PDS"] if "FS_PDS" in stddict and stddict["FS_PDS"] is not None else "Unknown"
            fpprov = stddict["FS_FPDS"] if "FS_FPDS" in stddict and stddict["FS_FPDS"] is not None else "Unknown"
            ppprov = stddict["FS_PPDS"] if "FS_PPDS" in stddict and stddict["FS_PPDS"] is not None else "Unknown"
            qvprov = stddict["FS_QDS"] if "FS_QDS" in stddict and stddict["FS_QDS"] is not None else "Unknown"
            EC.addEquilibriumFluxFunctions(stddict["FS_PFN"], "PSIPOLN", ff, pf, fp, pp, qv, \
                                           fvec_prov=ffprov, pvec_prov=pfprov, fprimevec_prov=fpprov, pprimevec_prov=ppprov, qvec_prov=qvprov)
        if "FS_PFN" in stddict and stddict["FS_PFN"] is not None and "FS_PFX" in stddict and stddict["FS_PFX"] is not None:
            xerr = stddict["FS_PFNEB"] if "FS_PFNEB" in stddict and stddict["FS_PFNEB"] is not None else np.zeros(stddict["FS_PFN"].shape)
            yerr = stddict["FS_PFXEB"] if "FS_PFXEB" in stddict and stddict["FS_PFXEB"] is not None else np.zeros(stddict["FS_PFX"].shape)
            prov = stddict["FS_PFXDS"] if "FS_PFXDS" in stddict and stddict["FS_PFXDS"] is not None else "Unknown"
            EC.addEquilibriumData("PSX", stddict["FS_PFX"], yerr, stddict["FS_PFN"], xerr, "PSIPOLN", provenance=prov)
        if "FS_A" in stddict and stddict["FS_A"] is not None and "FS_PFN" in stddict and stddict["FS_PFN"] is not None:
            areaeb = stddict["FS_AEB"] if "FS_AEB" in stddict and stddict["FS_AEB"] is not None else None
            areaprov = stddict["FS_ADS"] if "FS_ADS" in stddict and stddict["FS_ADS"] is not None else "Unknown"
            EC.addEquilibriumData("AREA", stddict["FS_A"], areaeb, stddict["FS_PFN"], None, "PSIPOLN", provenance=areaprov)
        if "FS_V" in stddict and stddict["FS_V"] is not None and "FS_PFN" in stddict and stddict["FS_PFN"] is not None:
            voleb = stddict["FS_VEB"] if "FS_VEB" in stddict and stddict["FS_VEB"] is not None else None
            volprov = stddict["FS_VDS"] if "FS_VDS" in stddict and stddict["FS_VDS"] is not None else "Unknown"
            EC.addEquilibriumData("VOL", stddict["FS_V"], voleb, stddict["FS_PFN"], None, "PSIPOLN", provenance=volprov)

#        eqvdict = {"PFN": "XPSIPOLN", "RHOPN": "XRHOPOLN", "INVR": "RINV", "INVR2": "RINV2", "BPOL": "BPOL", "BPOL2": "BPOL2", "GPSI": "GPSIPOLN", "GPSI2": "GPSIPOLN2", "JPHIBYR": "JPHIBYR"}
#        for key in eqvdict:
#            if "FS_"+key in stddict and stddict["FS_"+key] is not None:
#                EC.addEquilibriumData(eqvdict[key], stddict["FS_"+key], np.zeros(stddict["FS_"+key].shape))

        if fdebug:
            print("jet - adapter.py: %s equilibrium profiles populated." % (type(EC).__name__))

        gpkeys = [key for key in stddict if key.endswith("GPSET")]
        for key in gpkeys:
            nkey = key[:-5]
            newname = None
            newtag = None
            for src in ["NBI", "ICRH", "ECRH", "LH", "OHM", "RAD"]:
                mm = re.match(r'^([A-Z]+)([0-9]*)'+src+r'$', nkey)
                if mm and src == "RAD" and mm.group(1).upper() == "Q":
                    newname = "ST"
                    newtag = mm.group(2).upper()+src
                elif mm and mm.group(1).upper() in varconv:
                    newname = varconv[mm.group(1).upper()]
                    newtag = mm.group(2).upper()+src
            if newname is None:
                mm = re.match(r'^([A-Z]+)([0-9]*)$', nkey)
                if mm and mm.group(1).upper() in varconv:
                    newname = varconv[mm.group(1).upper()]
                    newtag = mm.group(2).upper()
            if isinstance(stddict[key], list) and newname is not None:
                FL = classes.EX2GKFitSettingsOrderedList()
                for ii in range(len(stddict[key])):
                    if "FTYPE" in stddict[key][ii] and isinstance(stddict[key][ii]["FTYPE"], str):
                        if stddict[key][ii]["FTYPE"].upper() == "GPR":
                            FS = classes.EX2GKGPFitSettings()
                            FS.defineFitKernelSettings(stddict[key][ii]["FIT_KNAME"], stddict[key][ii]["FIT_KPARS"], stddict[key][ii]["FIT_REGPAR"], \
                                                       stddict[key][ii]["FIT_ONAME"], stddict[key][ii]["FIT_OPARS"], stddict[key][ii]["FIT_EPSPAR"], \
                                                       stddict[key][ii]["FIT_NRES"], stddict[key][ii]["FIT_KBNDS"])
                            FS.defineErrorKernelSettings(stddict[key][ii]["ERR_KNAME"], stddict[key][ii]["ERR_KPARS"], stddict[key][ii]["ERR_REGPAR"], \
                                                         stddict[key][ii]["ERR_ONAME"], stddict[key][ii]["ERR_OPARS"], stddict[key][ii]["ERR_EPSPAR"], \
                                                         stddict[key][ii]["ERR_NRES"], stddict[key][ii]["ERR_KBNDS"])
                            FL.append(FS)
                        elif stddict[key][ii]["FTYPE"].upper() == "POLY":
                            FS = classes.EX2GKPolyFitSettings()
                            FS.definePolyFitSettings(stddict[key][ii]["DEGREE"], stddict[key][ii]["XANCHORS"], stddict[key][ii]["YANCHORS"], stddict[key][ii]["OMITCST"], stddict[key][ii]["OMITLIN"])
                            FL.append(FS)
                        elif stddict[key][ii]["FTYPE"].upper() == "LININT":
                            pass
                KC.addFitSettingsListObject(newname+newtag, FL)
                if MC["NUMI"] > 1 and newname == "SNI" and re.search(r'^[0-9]', newtag):
                    for ii in range(2, MC["NUMI"] + 1):
                        nntag = "%d" % (ii) + newtag[1:]
                        KC.addFitSettingsListObject(newname+nntag, FL)

        if fdebug:
            print("jet - adapter.py: %s populated." % (type(KC).__name__))

        # Transfer processing flags as metadata
        FC.setFlag("HMODE", "HMODEFLAG" in stddict and stddict["HMODEFLAG"])
        FC.setFlag("PELLET", "PELINJFLAG" in stddict and stddict["PELINJFLAG"])
        FC.setFlag("USERZ", "USERZFLAG" in stddict and stddict["USERZFLAG"])
        FC.setFlag("ELMFILT", "ELMFILTFLAG" in stddict and stddict["ELMFILTFLAG"])
        FC.setFlag("GPCOORD", "GPCOORDFLAG" in stddict and stddict["GPCOORDFLAG"])
        FC.setFlag("RSHIFT", "RSHIFTFLAG" in stddict and stddict["RSHIFTFLAG"])
        FC.setFlag("KEEPXEB", "RHOEBFLAG" in stddict and stddict["RHOEBFLAG"])
        FC.setFlag("NSCALE", "USENSCALEFLAG" in stddict and stddict["USENSCALEFLAG"])
        FC.setFlag("PURERAW", "PURERAWFLAG" in stddict and stddict["PURERAWFLAG"])
        FC.setFlag("STDMEAN", "STDMEANFLAG" in stddict and stddict["STDMEANFLAG"])
        FC.setFlag("EXPFILT", "EXPFILTFLAG" in stddict and stddict["EXPFILTFLAG"])
        FC.setFlag("EDGETE", "EDGETEFLAG" in stddict and stddict["EDGETEFLAG"])
        FC.setFlag("USELMODE", "USELMODEFLAG" in stddict and stddict["USELMODEFLAG"])
        FC.setFlag("USERQNE", "USENESMOOTHFLAG" in stddict and stddict["USENESMOOTHFLAG"])
        FC.setFlag("USEECE", "USEECEFLAG" in stddict and stddict["USEECEFLAG"])
        FC.setFlag("ABSECE", "ABSECEFLAG" in stddict and stddict["ABSECEFLAG"])
        FC.setFlag("USEREFL", "FORCEREFLFLAG" in stddict and stddict["FORCEREFLFLAG"])
        FC.setFlag("USEQEDGE", "USEQEDGEFLAG" in stddict and stddict["USEQEDGEFLAG"])
        FC.setFlag("NOOPT", "NOOPTFLAG" in stddict and stddict["NOOPTFLAG"])

        # Special flag which determines use of noise term within the GPR routine - not yet implemented
#        fgpn = "USEGPNOISE" in argdict and argdict["USEGPNOISE"] else False
#        FC.setFlag("GPNOISE", fgpn)

        if fdebug:
            print("jet - adapter.py: %s populated." % (type(FC).__name__))

        outobj = classes.EX2GKTimeWindow(meta=MC, cd=CC, zd=ZC, rd=RC, ed=EC, fsi=KC, flag=FC)

    return outobj
