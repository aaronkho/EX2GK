# Script with functions to process extracted JET PPF data into intermediate custom structure
# Developer: Aaron Ho - 26/02/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
from scipy.interpolate import interp1d, splrep, splev, splint, bisplrep, bisplev
from scipy.integrate import cumtrapz
from scipy.optimize import curve_fit

# Internal package imports
from EX2GK.tools.general import proctools as ptools, phystools as ftools
from EX2GK.tools.jet import proctools as jptools


def transfer_generic_data(rawdata, newstruct=None, userz=False, gpcoord=False, useshift=False, usenscale=True, useece=False, absece=False, useqedge=False, \
                          uselmode=False, usenesmooth=False, pureraw=False, usesom=False, userefl=False, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Transfers any crucial data fields which require little to no
    additional processing from the raw extracted data structure into
    a new processed data structure. This concept prevents the
    accumulation of unnecessary data in the final data structure.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg userz: bool. Toggles use of (r,z) coordinates for mapping diagnostic data into magnetic geometry, requires poloidal flux map.

    :kwarg gpcoord: bool. Toggles use of GPR for coordinate system interpolation, provides smoother derivatives for Jacobians.

    :kwarg useshift: bool. Toggles usage of equilibrium shift in midplane-averaged major radius based on electron temperature.

    :kwarg usenscale: bool. Toggles usage of density scaling based on measured and synthetic line-integrated values.

    :kwarg useece: bool. Toggles inclusion of electron temperature measurements from ECE diagnostics, KK3 and ECM.

    :kwarg absece: bool. Toggles inclusion of HFS ECE measurements in positive half of radial coordinate axis.

    :kwarg useqedge: bool. Toggles inclusion of q-profile edge modification to enforce consistency with total plasma current.

    :kwarg uselmode: bool. Toggles strict usage of L-mode options in data processing routine, useful for mislabeled L-mode plasmas.

    :kwarg usenesmooth: bool. Toggles strcit usage of RQ kernel for electron density fits, useful when pedestal top is near separatrix.

    :kwarg pureraw: bool. Toggles usage of experimental data exactly as extracted, without heuristic filters or error adjustments.

    :kwarg usesom: bool. Toggles usage of only the standard deviation of the mean as profile errors.

    :kwarg userefl: bool. Toggles usage of reflectometry diagnostics, currently only without any equilibrium remapping.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with general metadata transferred.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None:

        # Transfers generic metadata
        PD["DEVICE"] = RD["DEVICE"] if "DEVICE" in RD else "JET"
        PD["INTERFACE"] = RD["INTERFACE"] if "INTERFACE" in RD else "UNKNOWN"
        PD["SHOT"] = int(RD["SHOT"]) if "SHOT" in RD else None
        PD["T1"] = float(RD["T1"]) if "T1" in RD else None
        PD["T2"] = float(RD["T2"]) if "T2" in RD else None
        PD["SHOTPHASE"] = int(RD["SHOTPHASE"]) if "SHOTPHASE" in RD else -1
        PD["WINDOW"] = RD["WINDOW"] if "WINDOW" in RD else -1
        PD["ILWSTART"] = 79854

        PD["USERZFLAG"] = True if userz else False
        PD["RSHIFTFLAG"] = True if useshift else False
        PD["GPCOORDFLAG"] = True if gpcoord else False
        PD["USENSCALEFLAG"] = True if usenscale else False
        PD["PURERAWFLAG"] = True if pureraw else False
        PD["STDMEANFLAG"] = True if usesom else False
        PD["USELMODEFLAG"] = True if uselmode or PD["SHOTPHASE"] != 0 else False
        PD["USENESMOOTHFLAG"] = True if usenesmooth else False
        PD["USEECEFLAG"] = True if useece else False
        PD["ABSECEFLAG"] = True if absece else False
        PD["FORCEREFLFLAG"] = True if userefl else False
        PD["USEQEDGEFLAG"] = True if useqedge else False

        PD["WALLMAT"] = 'C' if int(PD["SHOT"]) <= PD["ILWSTART"] else 'Be'
        PD["CONTMAT"] = 'C' if int(PD["SHOT"]) <= PD["ILWSTART"] else 'W'

    if fdebug:
        print("process_jet.py: transfer_generic_data() completed.")

    return PD


def define_equilibrium(rawdata, newstruct=None, equilibrium=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Specifies the equilibrium to be used based on a pre-defined
    hierarchy, placing the equilibrium selected by the user at
    the highest preference if supplied.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg equilibrium: str. Optional string specifying which equilibrium is to be given priority, otherwise follows EFTM/EFTF/EFTP/EFIT.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with equilibrium selection defined.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()
    eqlist = ['eftm', 'eftf', 'eftp', 'efit']          # This list is in order of descending preference, do not tamper with it!

    if RD is not None and "MAP" in RD:
        m = RD["MAP"]
        eqgood = []
        reqvars = ['rmag', 'f', 'p', 'vjac', 'ajac', 'q']
        for ii in range(len(eqlist)):
            gflag = True
            for var in reqvars:
                if gflag:
                    if not (eqlist[ii]+'/'+var in m and RD[m[eqlist[ii]+'/'+var]] is not None):
                        gflag = False
            if gflag:
                if not (eqlist[ii]+'/rmji' in m and RD[m[eqlist[ii]+'/rmji']] is not None and eqlist[ii]+'/rmjo' in m and RD[m[eqlist[ii]+'/rmjo']] is not None):
                    if not (eqlist[ii]+'/psni' in m and RD[m[eqlist[ii]+'/psni']] is not None and eqlist[ii]+'/psno' in m and RD[m[eqlist[ii]+'/psno']] is not None):
                        if not (eqlist[ii]+'/psin' in m and RD[m[eqlist[ii]+'/psin']] is not None and eqlist[ii]+'/rpre' in m and RD[m[eqlist[ii]+'/rpre']] is not None):
                            gflag = False
            if gflag:
                if not (eqlist[ii]+'/fbnd' in m and RD[m[eqlist[ii]+'/fbnd']] is not None and eqlist[ii]+'/faxs' in m and RD[m[eqlist[ii]+'/faxs']] is not None):
                    if not (eqlist[ii]+'/ftor' in m and RD[m[eqlist[ii]+'/ftor']] is not None):
                        gflag = False
            if gflag:
                if not (eqlist[ii]+'/psi' in m and RD[m[eqlist[ii]+'/psi']] is not None):
                    if not (eqlist[ii]+'/sspr' in m and RD[m[eqlist[ii]+'/sspr']] is not None and eqlist[ii]+'/sspi' in m and RD[m[eqlist[ii]+'/sspi']] is not None):
                        gflag = False
            if gflag:
                eqgood.append(eqlist[ii])

        if isinstance(equilibrium, str) and re.match(r'^all$', equilibrium, flags=re.IGNORECASE):
            PD["EQLIST"] = eqlist
        elif isinstance(equilibrium, str) and equilibrium.lower() in eqlist and equilibrium.lower() in eqgood:
            PD["EQLIST"] = [equilibrium.lower()]
        elif len(eqgood) > 0:
            PD["EQLIST"] = [eqgood[0]]
        if "EQLIST" not in PD:
            raise ValueError("Required coordinate system data not found.")

    if fdebug:
        print("process_jet.py: define_equilibrium() completed.")

    return PD


def define_time_filter(rawdata, newstruct=None, elmfilt=True, threshold=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Defines time slice exclusion windows based on various time-dependent signals,
    primarily for filtering out time slices which would bias the time window averaged
    values.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg elmfilt: bool. Attempt ELM detection routine based on edge spectrometer diagnostics.

    :kwarg threshold: float. Optional threshold value for custom processing, default is calculated from signal.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with time slice exclusion windows defined.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None

        PD["ELMFILTFLAG"] = True if elmfilt else False

        # ELM detection routine based on edge spectrometer diagnostics, criteria designed to be trigger-happy but still very robust
        if PD["ELMFILTFLAG"]:
            nsig = None
            tsig = None
            nskip = 1
            sfac = 0.75
            if nsig is None and (('edg8/tbeo' in m and RD[m['edg8/tbeo']] is not None) or ('edg7/be2o' in m and RD[m['edg7/be2o']] is not None)):
                siga = RD[m['edg8/tbeo']] if 'edg8/tbeo' in m else None
                tima = RD["T_"+m['edg8/tbeo']] if siga is not None else None
                sigb = RD[m['edg7/be2o']] if 'edg7/be2o' in m else None
                timb = RD["T_"+m['edg7/be2o']] if sigb is not None else None
                if siga is not None and np.nanstd(siga) < 5.0 * np.nanmean(siga):
                    nsig = siga.flatten()
                    tsig = tima.flatten()
                    nskip = 7
                elif sigb is not None and np.nanstd(sigb) < 3.0 * np.nanmean(sigb):
                    nsig = sigb.flatten()
                    tsig = timb.flatten()
                    nskip = 7
            if nsig is None and (('edg7/dai' in m and RD[m['edg7/dai']] is not None) or ('edg7/dao' in m and RD[m['edg7/dao']] is not None)):
                siga = RD[m['edg7/dai']] if 'edg7/dai' in m else None
                tima = RD["T_"+m['edg7/dai']] if siga is not None else None
                sigb = RD[m['edg7/dao']] if 'edg7/dao' in m else None
                timb = RD["T_"+m['edg7/dao']] if sigb is not None else None
                if siga is not None and np.nanstd(siga) < 5.0 * np.nanmean(siga):
                    nsig = siga.flatten()
                    tsig = tima.flatten()
                elif sigb is not None and np.nanstd(sigb) < 3.0 * np.nanmean(sigb):
                    nsig = sigb.flatten()
                    tsig = timb.flatten()

            # Constructs list of time slice exclusion windows
            PD["ELMMASK"] = None
            if nsig is not None and tsig is not None:
                if nskip > 1:
                    tsig = tsig[::nskip]
                    nsig = nsig[::nskip]
                dthr = float(threshold) if isinstance(threshold, (int, float)) else sfac * np.nanstd(nsig)
                (belm, delm, dummy) = ftools.elm_detector(tsig, nsig, delta_threshold=dthr)
                belm = np.array(belm).flatten() if belm is not None else np.array([])
                delm = np.array(delm).flatten() if delm is not None else np.array([])
                emask = []
                if belm.size == delm.size:
                    for ii in range(belm.size):
                        temp = (belm[ii], belm[ii] + delm[ii])
                    if "T1" in PD and PD["T1"] is not None and "T2" in PD and PD["T2"] is not None:
                        if temp[0] < PD["T2"] and temp[1] > PD["T1"]:
                            emask.append(temp)
                    else:
                        emask.append(temp)
                if len(emask) > 0:
                    PD["ELMMASK"] = copy.deepcopy(emask)

        # TODO: Add an input power switch to handle L-mode windows
        # TODO: Add a warning if computed mask covers entire time window

    if fdebug:
        print("process_jet.py: define_time_filter() completed.")

    return PD


def unpack_general_data(rawdata, newstruct=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Unpacks the required general data from the raw extracted data container,
    pre-processes it with statistical averaging if necessary, and stores it
    in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with processed general data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None
        fullflag = True if "SELECTEQ" in RD and re.match(r'^all$', RD["SELECTEQ"], flags=re.IGNORECASE) else False

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['efit']
        if fullflag:
            for ii in range(len(eqlist)):
                if eqlist[ii].upper() not in PD:
                    PD[eqlist[ii].upper()] = dict()

        # Specification of time ranges to mask for ELM filtering
        tmlist = PD["ELMMASK"] if "ELMMASK" in PD else None

        if fdebug:
            print("process_jet.py: unpack_general_data(): Setup completed.")

        # Plasma current
        vals = RD[m['magn/ipla']] if 'magn/ipla' in m else None
        tvals = RD["T_"+m['magn/ipla']] if 'magn/ipla' in m else None
        prov = RD["I_"+m['magn/ipla']] if 'magn/ipla' in m else None
        if vals is None and 'efit/xip' in m:
            vals = RD[m['efit/xip']]
            tvals = RD["T_"+m['efit/xip']]
            prov = RD["I_"+m['efit/xip']]
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        (PD["IP"], PD["IPEB"], stdmean, navgmap) = ptools.filtered_average(vals)
        PD["IPEB"] = PD["IPEB"] + np.abs(0.01 * PD["IP"]) + 1.0e4 if PD["IP"] is not None and PD["IPEB"] is not None else None
        PD["IPDS"] = prov

        if fdebug and PD["IP"] is not None:
            print("process_jet.py: unpack_general_data(): Plasma current added.")

        # NBI power and error
        vals = RD[m['nbi/nblm']] if 'nbi/nblm' in m else None
        tvals = RD["T_"+m['nbi/nblm']] if 'nbi/nblm' in m else None
        prov = RD["I_"+m['nbi/nblm']] if 'nbi/nblm' in m else None
        if vals is None and 'nbi/ptot' in m:
            vals = RD[m['nbi/ptot']]
            tvals = RD["T_"+m['nbi/ptot']]
            prov = RD["I_"+m['nbi/ptot']]
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        if vals is not None:
            (PD["PNBI"], PD["PNBIEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["PNBIDS"] = prov

        if fdebug and "PNBI" in PD and PD["PNBI"] is not None:
            print("process_jet.py: unpack_general_data(): NBI power added.")

        # NBI power loss and error
        vals = RD[m['nbp2/shid']] if 'nbp2/shid' in m else None
        tvals = RD["T_"+m['nbp2/shid']] if 'nbp2/shid' in m else None
        prov = RD["I_"+m['nbp2/shid']] if 'nbp2/shid' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        if vals is not None:
            (PD["LNBI"], PD["LNBIEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["LNBIDS"] = prov

        if fdebug and "LNBI" in PD and PD["LNBI"] is not None:
            print("process_jet.py: unpack_general_data(): NBI power loss added.")

        # ICRH power and error
        vals = RD[m['icrh/ptot']] if 'icrh/ptot' in m else None
        tvals = RD["T_"+m['icrh/ptot']] if 'icrh/ptot' in m else None
        prov = RD["I_"+m['icrh/ptot']] if 'icrh/ptot' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        if vals is not None:
            (PD["PICRH"], PD["PICRHEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["PICRHDS"] = prov

        if fdebug and "PICRH" in PD and PD["PICRH"] is not None:
            print("process_jet.py: unpack_general_data(): ICRH power added.")

        # ICRH power loss and error
        vals = RD[m['pion/plss']] if 'pion/plss' in m else None
        tvals = RD["T_"+m['pion/plss']] if 'pion/plss' in m else None
        prov = RD["I_"+m['pion/plss']] if 'pion/plss' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        if vals is not None:
            (PD["LICRH"], PD["LICRHEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["LICRHDS"] = prov

        if fdebug and "LICRH" in PD and PD["LICRH"] is not None:
            print("process_jet.py: unpack_general_data(): ICRH power loss added.")

        # LH power and error
        vals = RD[m['lhcd/ptot']] if 'lhcd/ptot' in m else None
        tvals = RD["T_"+m['lhcd/ptot']] if 'lhcd/ptot' in m else None
        prov = RD["I_"+m['lhcd/ptot']] if 'lhcd/ptot' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        if vals is not None:
            (PD["PLH"], PD["PLHEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["PLHDS"] = prov

        if fdebug and "PLH" in PD and PD["PLH"] is not None:
            print("process_jet.py: unpack_general_data(): LH power added.")

        # Ohmic power
        vals = RD[m[eqlist[0].lower()+'/pohm']] if eqlist[0].lower()+'/pohm' in m else None
        tvals = RD["T_"+m[eqlist[0].lower()+'/pohm']] if eqlist[0].lower()+'/pohm' in m else None
        prov = RD["I_"+m[eqlist[0].lower()+'/pohm']] if eqlist[0].lower()+'/pohm' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        if vals is not None:
            (PD["POHM"], PD["POHMEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["POHMDS"] = prov

        if fdebug and "POHM" in PD and PD["POHM"] is not None:
            print("process_jet.py: unpack_general_data(): Ohmic power added.")

        # Total radiated power
        vals = RD[m['bara/topo']] if 'bara/topo' in m else None
        tvals = RD["T_"+m['bara/topo']] if 'bara/topo' in m else None
        prov = RD["I_"+m['bara/topo']] if 'bara/topo' in m else None
        if vals is None and 'bolt/topo' in m:
            vals = RD[m['bolt/topo']]
            tvals = RD["T_"+m['bolt/topo']]
            prov = RD["I_"+m['bolt/topo']]
        if vals is None and 'bolp/topo' in m:
            vals = RD[m['bolp/topo']]
            tvals = RD["T_"+m['bolp/topo']]
            prov = RD["I_"+m['bolp/topo']]
        if vals is None and 'bolo/topi' in m:
            vals = RD[m['bolo/topi']]
            tvals = RD["T_"+m['bolo/topi']]
            prov = RD["I_"+m['bolo/topi']]
        if vals is None and 'bolo/topo' in m:
            vals = RD[m['bolo/topo']]
            tvals = RD["T_"+m['bolo/topo']]
            prov = RD["I_"+m['bolo/topo']]
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        (PD["PRAD"], PD["PRADEB"], stdmean, navgmap) = ptools.filtered_average(vals)
        PD["PRADDS"] = prov

        if fdebug and PD["PRAD"] is not None:
            print("process_jet.py: unpack_general_data(): Total radiated power loss added.")

        # Bulk radiated power
        vals = RD[m['bolp/tobp']] if 'bolp/tobp' in m else None
        tvals = RD["T_"+m['bolp/tobp']] if 'bolp/tobp' in m else None
        prov = RD["I_"+m['bolp/tobp']] if 'bolp/tobp' in m else None
        if vals is None and 'bolt/tobu' in m:
            vals = RD[m['bolt/tobu']]
            tvals = RD["T_"+m['bolt/tobu']]
            prov = RD["I_"+m['bolt/tobu']]
        if vals is None and 'bolo/tobu' in m:
            vals = RD[m['bolo/tobu']]
            tvals = RD["T_"+m['bolo/tobu']]
            prov = RD["I_"+m['bolo/tobu']]
        if vals is None and 'bolo/tobh' in m:
            vals = RD[m['bolo/tobh']]
            tvals = RD["T_"+m['bolo/tobh']]
            prov = RD["I_"+m['bolo/tobh']]
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
        (PD["PRADBULK"], PD["PRADBULKEB"], stdmean, navgmap) = ptools.filtered_average(vals)
        PD["PRADBULKDS"] = prov

        if fdebug and PD["PRADBULK"] is not None:
            print("process_jet.py: unpack_general_data(): Bulk radiated power loss added.")

        # Net absorbed power
        PD["PNET"] = PD["POHM"].copy() if PD["POHM"] is not None else 0.0
        PD["PNETEB"] = PD["POHMEB"].copy() if PD["POHMEB"] is not None else 0.0
        if "PNBI" in PD and PD["PNBI"] is not None:
            PD["PNET"] = PD["PNET"] + PD["PNBI"]
            PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["PNBIEB"], 2.0))
            if "LNBI" in PD and PD["LNBI"] is not None:
                PD["PNET"] = PD["PNET"] - PD["LNBI"]
                PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["LNBIEB"], 2.0))
        if "PICRH" in PD and PD["PICRH"] is not None:
            PD["PNET"] = PD["PNET"] + PD["PICRH"]
            PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["PICRHEB"], 2.0))
            if "LICRH" in PD and PD["LICRH"] is not None:
                PD["PNET"] = PD["PNET"] - PD["LICRH"]
                PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["LICRHEB"], 2.0))
        if "PLH" in PD and PD["PLH"] is not None:
            PD["PNET"] = PD["PNET"] + PD["PLH"]
            PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["PLHEB"], 2.0))
        if "PRAD" in PD and PD["PRAD"] is not None:
            PD["PNET"] = PD["PNET"] - PD["PRAD"]
            PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["PRADEB"], 2.0))
        elif "PRADBULK" in PD and PD["PRADBULK"] is not None:
            PD["PNET"] = PD["PNET"] - PD["PRADBULK"]
            PD["PNETEB"] = np.sqrt(np.power(PD["PNETEB"], 2.0) + np.power(PD["PRADBULKEB"], 2.0))
        PD["PNETDS"] = "EX2GK: Internal calculation"

        # Gas fuelling - majority species
        tag = 'gasm' if int(PD["SHOT"]) >= 57559 else 'gasi'   # Reused for all gas fuelling data
        PD["GAS0A"] = RD[m[tag+'/maja']] if tag+'/maja' in m else None
        if PD["GAS0A"] is not None:
            PD["GAS0ADS"] = RD["I_"+m[tag+'/maja']] if tag+'maja' in m else None
        PD["GAS0Z"] = RD[m[tag+'/majz']] if tag+'/majz' in m else None
        if PD["GAS0Z"] is not None:
            PD["GAS0ZDS"] = RD["I_"+m[tag+'/majz']] if tag+'majz' in m else None
        PD["GAS0R"] = None
        PD["GAS0REB"] = None
        PD["GAS0C"] = None
        PD["GAS0CEB"] = None
        if PD["GAS0A"] is not None and PD["GAS0Z"] is not None:
            rvals = RD[m[tag+'/majr']] if tag+'/majr' in m else None
            (PD["GAS0R"], PD["GAS0REB"], stdmean, ngas) = ptools.filtered_average(rvals, percent_errors=0.1, use_n=True)
            PD["GAS0RDS"] = RD["I_"+m[tag+'/majr']] if tag+'/majr' in m else None
            if PD["GAS0R"] is not None and PD["GAS0R"] < 1.0e15:
                PD["GAS0R"] = np.array([0.0])
                PD["GAS0REB"] = np.array([0.0])
            cvals = RD[m[tag+'/majc']] if tag+'/majc' in m else None
            PD["GAS0C"] = np.array([cvals[0]]) if cvals is not None and cvals.size > 0 else None     # Use value at start of window
            PD["GAS0CDS"] = RD["I_"+m[tag+'/majc']] if tag+'/majc' in m else None
            #PD["GAS0CEB"] = 0.1 * PD["GAS0C"] if PD["GAS0C"] is not None else None

        # Gas fuelling - minority species 1
        PD["GAS1A"] = RD[m[tag+'/mn1a']] if tag+'/mn1a' in m else None
        if PD["GAS1A"] is not None:
            PD["GAS1ADS"] = RD["I_"+m[tag+'/mn1a']] if tag+'mn1a' in m else None
        PD["GAS1Z"] = RD[m[tag+'/mn1z']] if tag+'/mn1z' in m else None
        if PD["GAS1Z"] is not None:
            PD["GAS1ZDS"] = RD["I_"+m[tag+'/mn1z']] if tag+'mn1z' in m else None
        PD["GAS1R"] = None
        PD["GAS1REB"] = None
        PD["GAS1C"] = None
        PD["GAS1CEB"] = None
        if PD["GAS1A"] is not None and PD["GAS1Z"] is not None:
            rvals = RD[m[tag+'/mn1r']] if tag+'/mn1r' in m else None
            (PD["GAS1R"], PD["GAS1REB"], stdmean, ngas) = ptools.filtered_average(rvals, percent_errors=0.1, use_n=True)
            PD["GAS1RDS"] = RD["I_"+m[tag+'/mn1r']] if tag+'/mn1r' in m else None
            if PD["GAS1R"] is not None and PD["GAS1R"] < 1.0e15:
                PD["GAS1R"] = np.array([0.0])
                PD["GAS1REB"] = np.array([0.0])
            cvals = RD[m[tag+'/mn1c']] if tag+'/mn1c' in m else None
            PD["GAS1C"] = np.array([cvals[0]]) if cvals is not None and cvals.size > 0 else None     # Use value at start of window
            PD["GAS1CDS"] = RD["I_"+m[tag+'/mn1c']] if tag+'/mn1c' in m else None
            #PD["GAS1CEB"] = 0.1 * PD["GAS1C"] if PD["GAS1C"] is not None else None

        # Gas fuelling - minority species 2
        PD["GAS2A"] = RD[m[tag+'/mn2a']] if tag+'/mn2a' in m else None
        if PD["GAS2A"] is not None:
            PD["GAS2ADS"] = RD["I_"+m[tag+'/mn2a']] if tag+'mn2a' in m else None
        PD["GAS2Z"] = RD[m[tag+'/mn2z']] if tag+'/mn2z' in m else None
        if PD["GAS2Z"] is not None:
            PD["GAS2ZDS"] = RD["I_"+m[tag+'/mn2z']] if tag+'mn2z' in m else None
        PD["GAS2R"] = None
        PD["GAS2REB"] = None
        PD["GAS2C"] = None
        PD["GAS2CEB"] = None
        if PD["GAS2A"] is not None and PD["GAS2Z"] is not None:
            rvals = RD[m[tag+'/mn2r']] if tag+'/mn2r' in m else None
            (PD["GAS2R"], PD["GAS2REB"], stdmean, ngas) = ptools.filtered_average(rvals, percent_errors=0.1, use_n=True)
            PD["GAS2RDS"] = RD["I_"+m[tag+'/mn2r']] if tag+'/mn2r' in m else None
            if PD["GAS2R"] is not None and PD["GAS2R"] < 1.0e15:
                PD["GAS2R"] = np.array([0.0])
                PD["GAS2REB"] = np.array([0.0])
            cvals = RD[m[tag+'/mn2c']] if tag+'/mn2c' in m else None
            PD["GAS2C"] = np.array([cvals[0]]) if cvals is not None and cvals.size > 0 else None     # Use value at start of window
            PD["GAS2CDS"] = RD["I_"+m[tag+'/mn2c']] if tag+'/mn2c' in m else None
            #PD["GAS2CEB"] = 0.1 * PD["GAS2C"] if PD["GAS2C"] is not None else None

        # Gas fuelling - minority species 3 (unknown if it exists but left here for possible expansion)
        PD["GAS3A"] = RD[m[tag+'/mn3a']] if tag+'/mn3a' in m else None
        if PD["GAS3A"] is not None:
            PD["GAS3ADS"] = RD["I_"+m[tag+'/mn3a']] if tag+'mn3a' in m else None
        PD["GAS3Z"] = RD[m[tag+'/mn3z']] if tag+'/mn3z' in m else None
        if PD["GAS3Z"] is not None:
            PD["GAS3ZDS"] = RD["I_"+m[tag+'/mn3z']] if tag+'mn3z' in m else None
        PD["GAS3R"] = None
        PD["GAS3REB"] = None
        PD["GAS3C"] = None
        PD["GAS3CEB"] = None
        if PD["GAS3A"] is not None and PD["GAS3Z"] is not None:
            rvals = RD[m[tag+'/mn3r']] if tag+'/mn3r' in m else None
            (PD["GAS3R"], PD["GAS3REB"], stdmean, ngas) = ptools.filtered_average(rvals, percent_errors=0.1, use_n=True)
            PD["GAS3RDS"] = RD["I_"+m[tag+'/mn3r']] if tag+'/mn3r' in m else None
            if PD["GAS3R"] is not None and PD["GAS3R"] < 1.0e15:
                PD["GAS3R"] = 0.0
                PD["GAS3REB"] = 0.0
            cvals = RD[m[tag+'/mn3c']] if tag+'/mn3c' in m else None
            PD["GAS3C"] = np.array([cvals[0]]) if cvals is not None and cvals.size > 0 else None     # Use value at start of window
            PD["GAS3CDS"] = RD["I_"+m[tag+'/mn3c']] if tag+'/mn3c' in m else None
            #PD["GAS3CEB"] = 0.1 * PD["GAS3C"] if PD["GAS3C"] is not None else None

        if fdebug and PD["GAS0R"] is not None:
            print("process_jet.py: unpack_general_data(): Gas puffing rates added.")

        # Pellet fuelling - currently only detects possibility of on/off
        #   This should provide actual pellet data and maybe timing for pellet filter in the future
        PD["PELINJFLAG"] = True if 'pl/hf-pellet_out_vol' in m and RD[m['pl/hf-pellet_out_vol']] is not None else False
#        PD["PELINJFLAGDS"] = RD["I_"+m['pl/hf-pellet_out_vol']] if 'pl/hf-pellet_out_vol' in m and RD[m['pl/hf-pellet_out_vol']] is not None else None

        if fdebug and "PELINJFLAG" in PD:
            print("process_jet.py: unpack_general_data(): Pellet injection flag added.")

        # Line-averaged Z-effective
        vals = RD[m['ks3/zefv']] if 'ks3/zefv' in m else None
        errs = RD[m['ks3/dzfv']] if 'ks3/dzfv' in m and 'ks3/zefv' in m else None
        tvals = RD["T_"+m['ks3/zefv']] if 'ks3/zefv' in m else None
        prov = RD["I_"+m['ks3/zefv']] if 'ks3/zefv' in m else None
        dprov = RD["I_"+m['ks3/dzfv']] if 'ks3/dzfv' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (PD["ZEFV"], PD["ZEFVEB"], stdmean, nz) = ptools.filtered_average(vals, value_errors=errs, use_n=True)   # This one bypasses central core, polluted by divertor
        if PD["ZEFV"] is not None:
            PD["ZEFVDS"] = prov
            if errs is not None:
                PD["ZEFVDS"] = PD["ZEFVDS"] + "; " + dprov
        vals = RD[m['ks3/zefh']] if 'ks3/zefh' in m else None
        errs = RD[m['ks3/dzfh']] if 'ks3/dzfh' in m and 'ks3/zefh' in m else None
        tvals = RD["T_"+m['ks3/zefh']] if 'ks3/zefh' in m else None
        prov = RD["I_"+m['ks3/zefh']] if 'ks3/zefh' in m else None
        dprov = RD["I_"+m['ks3/dzfh']] if 'ks3/dzfh' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (PD["ZEFH"], PD["ZEFHEB"], stdmean, nz) = ptools.filtered_average(vals, value_errors=errs, use_n=True)   # This one avoids divertor pollution, sees central core
        if PD["ZEFH"] is not None:
            PD["ZEFHDS"] = prov
            if errs is not None:
                PD["ZEFHDS"] = PD["ZEFHDS"] + "; " + dprov

        if fdebug and (PD["ZEFV"] is not None or PD["ZEFH"] is not None):
            print("process_jet.py: unpack_general_data(): Line-integrated effective charge added.")

        # Total stored plasma energy, calculated using equilibrium code
        tag = 'wp'
        vtag = "TWE"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        # Total stored plasma energy, calculated using magnetic measurements
        tag = 'wdia'
        vtag = "TWD"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["TWD"] is not None:
            print("process_jet.py: unpack_general_data(): Stored plasma energy added.")

        # Internal inductance - ITER calculation, calculated using equilibrium code
        tag = 'xlim'
        vtag = "LI3E"
        PD[vtag] = None
        if PD["SHOT"] < PD["ILWSTART"]:
            for ii in range(len(eqlist)):
                if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                    vals = RD[m[eqlist[ii]+'/'+tag]]
                    tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    if vals is not None and tvals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(tvals, tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                    PD[vtag+"DS"] = prov
                if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                    vals = RD[m[eqlist[ii]+'/'+tag]]
                    tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    if vals is not None and tvals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(tvals, tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                    PD[eqlist[ii].upper()][vtag+"DS"] = prov
        tag = 'li3m'
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        # Internal inductance - ITER calculation, calculated using magnetic measurements
        tag = 'xlid'
        vtag = "LI3D"
        PD[vtag] = None
        if PD["SHOT"] < PD["ILWSTART"]:
            for ii in range(len(eqlist)):
                if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                    vals = RD[m[eqlist[ii]+'/'+tag]]
                    tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    if vals is not None and tvals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(tvals, tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                    PD[vtag+"DS"] = prov
                if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                    vals = RD[m[eqlist[ii]+'/'+tag]]
                    tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(tvals, tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                    PD[eqlist[ii].upper()][vtag+"DS"] = prov
        tag = 'li3d'
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["LI3D"] is not None:
            print("process_jet.py: unpack_general_data(): Internal inductance added.")

        # Total beta, calculated using equilibrium code - averaging expected to be done during extraction
        tag = 'btnm'
        vtag = "BETANE"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        # Total beta, calculated using magnetic measurements - averaging expected to be done during extraction
        tag = 'btnd'
        vtag = "BETAND"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["BETAND"] is not None:
            print("process_jet.py: unpack_general_data(): Global plasma beta added.")

        # Poloidal beta, calculated using equilibrium code - averaging expected to be done during extraction
        tag = 'btpm'
        vtag = "BETAPE"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        # Poloidal beta, calculated using magnetic measurements - averaging expected to be done during extraction
        tag = 'btpd'
        vtag = "BETAPD"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["BETAPD"] is not None:
            print("process_jet.py: unpack_general_data(): Beta poloidal added.")

        # Toroidal beta, calculated using equilibrium code - averaging expected to be done during extraction
        tag = 'bttm'
        vtag = "BETATE"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        # Toroidal beta, calculated using magnetic measurements - averaging expected to be done during extraction
        tag = 'bttd'
        vtag = "BETATD"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["BETATD"] is not None:
            print("process_jet.py: unpack_general_data(): Beta toroidal added.")

        # Toroidal magnetic field - averaging expected to be done during extraction
        tag = 'btax'
        vtag = "BTAX"         # B_tor(R) on ZMAG
        xtag = "RBT"
        PD[vtag] = None
        PD[xtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqlist[ii].upper()][vtag] is not None else None
                PD[eqlist[ii].upper()][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]] if PD[eqlist[ii].upper()][vtag] is not None else None

        # Toroidal magnetic field without plasma (at vacuum) at R=2.96m
        PD["RVAC"] = np.array([2.96])
        PD["RVACEB"] = np.array([0.0])
        PD["RVACDS"] = "EX2GK: Internal assumption"
        PD["BVAC"] = None
        PD["BVACEB"] = None
        if 'magn/bvac' in m and RD[m['magn/bvac']] is not None:
            vals = RD[m['magn/bvac']]
            tvals = RD["T_"+m['magn/bvac']] if 'magn/bvac' in m else None
            prov = RD["I_"+m['magn/bvac']] if 'magn/bvac' in m else None
            if vals is not None and tvals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(tvals, tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (PD["BVAC"], PD["BVACEB"], stdmean, navgmap) = ptools.filtered_average(vals)
            PD["BVACEB"] = PD["BVACEB"] + 0.02 if PD["BVACEB"] is not None else None
            PD["BVACDS"] = prov

        if fdebug and PD["BVAC"] is not None:
            print("process_jet.py: unpack_general_data(): Vacuum magnetic field added.")

        # Elongation at boundary, calculated using equilibrium code - averaging expected to be done during extraction
        tag = 'elon'
        vtag = "ELONG"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["ELONG"] is not None:
            print("process_jet.py: unpack_general_data(): Boundary elongation added.")

        # Lower triangularity at boundary, calculated using equilibrium code - averaging expected to be done during extraction
        tag = 'tril'
        vtag = "TRIANGL"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["TRIANGL"] is not None:
            print("process_jet.py: unpack_general_data(): Boundary lower triangularity added.")

        # Upper triangularity at boundary, calculated using equilibrium code - averaging expected to be done during extraction
        tag = 'triu'
        vtag = "TRIANGU"
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[vtag], PD[vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[vtag+"DS"] = prov
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                vals = RD[m[eqlist[ii]+'/'+tag]]
                tvals = RD["T_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                prov = RD["I_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (PD[eqlist[ii].upper()][vtag], PD[eqlist[ii].upper()][vtag+"EB"], stdmean, navgmap) = ptools.filtered_average(vals)
                PD[eqlist[ii].upper()][vtag+"DS"] = prov

        if fdebug and PD["TRIANGU"] is not None:
            print("process_jet.py: unpack_general_data(): Boundary upper triangularity added.")

        # Total neutron rate
        PD["NEUT"] = None
        PD["NEUTEB"] = None
        if 'tin/rnt' in m and RD[m['tin/rnt']] is not None:
            (PD["NEUT"], PD["NEUTEB"], stdmean, nnt) = ptools.filtered_average(RD[m['tin/rnt']], percent_errors=0.01, use_n=True)     # Assumed 1% error
            PD["NEUTDS"] = RD["I_"+m['tin/rnt']]

        if fdebug and PD["NEUT"] is not None:
            print("process_jet.py: unpack_general_data(): Neutron rate added.")

        # Primary plasma composition
        vals = RD[m['ks3b/hthd']] if 'ks3b/hthd' in m else None
        errs = RD[m['ks3b/dhth']] if 'ks3b/dhth' in m and 'ks3b/hthd' in m else None
        tvals = RD["T_"+m['ks3b/hthd']] if 'ks3b/hthd' in m else None
        hsprov = RD["I_"+m['ks3b/hthd']] if 'ks3b/hthd' in m else None
        hsdprov = RD["I_"+m['ks3b/dhth']] if 'ks3b/dhth' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (hfracs, hfracseb, stdmean, nhs) = ptools.filtered_average(vals, value_errors=errs, use_n=True)

        vals = RD[m['ks3b/dthd']] if 'ks3b/dthd' in m else None
        errs = RD[m['ks3b/ddth']] if 'ks3b/ddth' in m and 'ks3b/dthd' in m else None
        tvals = RD["T_"+m['ks3b/dthd']] if 'ks3b/dthd' in m else None
        dsprov = RD["I_"+m['ks3b/dthd']] if 'ks3b/dthd' in m else None
        dsdprov = RD["I_"+m['ks3b/ddth']] if 'ks3b/ddth' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (dfracs, dfracseb, stdmean, nds) = ptools.filtered_average(vals, value_errors=errs, use_n=True)

        vals = RD[m['ks3b/tttd']] if 'ks3b/tttd' in m else None
        errs = RD[m['ks3b/dttt']] if 'ks3b/dttt' in m and 'ks3b/tttd' in m else None
        tvals = RD["T_"+m['ks3b/tttd']] if 'ks3b/tttd' in m else None
        tsprov = RD["I_"+m['ks3b/tttd']] if 'ks3b/tttd' in m else None
        tsdprov = RD["I_"+m['ks3b/dttt']] if 'ks3b/dttt' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (tfracs, tfracseb, stdmean, nts) = ptools.filtered_average(vals, value_errors=errs, use_n=True)

        vals = RD[m['kt5p/hthd']] if 'kt5p/hthd' in m else None
        errs = RD[m['kt5p/dhth']] if 'kt5p/dhth' in m and 'kt5p/hthd' in m else None
        tvals = RD["T_"+m['kt5p/hthd']] if 'kt5p/hthd' in m else None
        hgprov = RD["I_"+m['kt5p/hthd']] if 'kt5p/hthd' in m else None
        hgdprov = RD["I_"+m['kt5p/dhth']] if 'kt5p/dhth' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (hfracg, hfracgeb, stdmean, nhs) = ptools.filtered_average(vals, value_errors=errs, use_n=True)

        vals = RD[m['kt5p/dthd']] if 'kt5p/dthd' in m else None
        errs = RD[m['kt5p/ddth']] if 'kt5p/ddth' in m and 'kt5p/dthd' in m else None
        tvals = RD["T_"+m['kt5p/dthd']] if 'kt5p/dthd' in m else None
        dgprov = RD["I_"+m['kt5p/dthd']] if 'kt5p/dthd' in m else None
        dgdprov = RD["I_"+m['kt5p/ddth']] if 'kt5p/ddth' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (dfracg, dfracgeb, stdmean, nds) = ptools.filtered_average(vals, value_errors=errs, use_n=True)

        vals = RD[m['kt5p/tttd']] if 'kt5p/tttd' in m else None
        errs = RD[m['kt5p/dttt']] if 'kt5p/dttt' in m and 'kt5p/tttd' in m else None
        tvals = RD["T_"+m['kt5p/tttd']] if 'kt5p/tttd' in m else None
        tgprov = RD["I_"+m['kt5p/tttd']] if 'kt5p/tttd' in m else None
        tgdprov = RD["I_"+m['kt5p/dttt']] if 'kt5p/dttt' in m else None
        if vals is not None and tvals is not None and tmlist is not None:
            tmask = ptools.create_range_mask(tvals, tmlist)
            if tmask is not None:
                vals = vals[tmask]
                if errs is not None:
                    errs = errs[tmask]
        (tfracg, tfracgeb, stdmean, nts) = ptools.filtered_average(vals, value_errors=errs, use_n=True)

        hfrac = hfracs if hfracs is not None else hfracg
        hfraceb = hfracseb if hfracseb is not None else hfracgeb
        hprov = hsprov if hsprov is not None else hgprov
        if hfracg is not None:
            hfrac = (hfrac + hfracg) / 2.0
            if hfracseb is not None:
                hfraceb = np.sqrt(np.power(hfraceb, 2.0) + np.power(hfracgeb, 2.0))
            if hprov is not None and hprov != hgprov:
                hprov = hprov + "; " + hgprov
        PD["HFRAC"] = hfrac
        PD["HFRACEB"] = hfraceb
        if PD["HFRAC"] is not None:
            PD["HFRACDS"] = hprov

        dfrac = dfracs if dfracs is not None else dfracg
        dfraceb = dfracseb if dfracseb is not None else dfracgeb
        dprov = dsprov if dsprov is not None else dgprov
        if dfracg is not None:
            dfrac = (dfrac + dfracg) / 2.0
            if dfracseb is not None:
                dfraceb = np.sqrt(np.power(dfraceb, 2.0) + np.power(dfracgeb, 2.0))
            if dprov is not None and dprov != dgprov:
                dprov = dprov + "; " + dgprov
        PD["DFRAC"] = dfrac
        PD["DFRACEB"] = dfraceb
        if PD["DFRAC"] is not None:
            PD["DFRACDS"] = dprov

        tfrac = tfracs if tfracs is not None else tfracg
        tfraceb = tfracseb if tfracseb is not None else tfracgeb
        tprov = tsprov if tsprov is not None else tgprov
        if tfracg is not None:
            tfrac = (tfrac + tfracg) / 2.0
            if tfracseb is not None:
                tfraceb = np.sqrt(np.power(tfraceb, 2.0) + np.power(tfracgeb, 2.0))
            if tprov is not None and tprov != tgprov:
                tprov = tprov + "; " + tgprov
        PD["TFRAC"] = tfrac
        PD["TFRACEB"] = tfraceb
        if PD["TFRAC"] is not None:
            PD["TFRACDS"] = tprov

        if fdebug and (PD["HFRAC"] is not None or PD["DFRAC"] is not None or PD["TFRAC"] is not None):
            print("process_jet.py: unpack_general_data(): Fuel isotopic composition added.")

    if fdebug:
        print("process_jet.py: unpack_general_data() completed.")

    return PD


def unpack_config_data(rawdata, newstruct=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Unpacks the machine-specific configuration fields from the raw extracted
    data container, and stores it in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Option object in which processed data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with processed configuration data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None

        PD["BOXNBI"] = []
        PD["PNBI4"] = None
        if 'nbi4/powt' in m and RD[m['nbi4/powt']] is not None:
            (PD["PNBI4"], PD["PNBI4EB"], pnbiem, navgmap) = ptools.filtered_average(RD[m['nbi4/powt']], percent_errors=0.1, lower_bounds=1.0e3, use_n=True)    # Taken from DDA Handbook
            if PD["PNBI4"] is not None:
                PD["PNBI4DS"] = RD["I_"+m['nbi4/powt']]
        if PD["PNBI4"] is not None and PD["PNBI4"] > 1.0e3:
            PD["BOXNBI"].append("4")
            PD["ENBI4"] = None
            PD["E1NBI4"] = None
            PD["E2NBI4"] = None
            PD["E3NBI4"] = None
            pinilist = []
            eprov = ""
            fprov = ""
            for ii in range(8):
                istr = "%d" % (ii+1)
                if 'nbi4/eng'+istr in m and RD[m['nbi4/eng'+istr]] is not None and 'nbi4/pfr'+istr in m and RD[m['nbi4/pfr'+istr]] is not None:
                    PD["ENBI4"] = RD[m['nbi4/eng'+istr]] if PD["ENBI4"] is None else PD["ENBI4"] + RD[m['nbi4/eng'+istr]]
                    pfrac = RD[m['nbi4/pfr'+istr]] / np.sum(RD[m['nbi4/pfr'+istr]])
                    PD["E1NBI4"] = pfrac[0] if PD["E1NBI4"] is None else PD["E1NBI4"] + pfrac[0]
                    PD["E2NBI4"] = pfrac[1] if PD["E2NBI4"] is None else PD["E2NBI4"] + pfrac[1]
                    PD["E3NBI4"] = pfrac[2] if PD["E3NBI4"] is None else PD["E3NBI4"] + pfrac[2]
                    pinilist.append(ii+1)
                    eprov = eprov + "; " + RD["I_"+m['nbi4/eng'+istr]] if eprov else RD["I_"+m['nbi4/eng'+istr]]
                    fprov = fprov + "; " + RD["I_"+m['nbi4/pfr'+istr]] if fprov else RD["I_"+m['nbi4/pfr'+istr]]
            if len(pinilist) > 0:
                npinis = float(len(pinilist))
                if PD["ENBI4"] is not None:
                    PD["ENBI4"] = np.array([PD["ENBI4"] / npinis])
                    PD["ENBI4DS"] = eprov
                if PD["E1NBI4"] is not None:
                    PD["E1NBI4"] = np.array([PD["E1NBI4"] / npinis])
                    PD["E1NBI4DS"] = fprov
                if PD["E2NBI4"] is not None:
                    PD["E2NBI4"] = np.array([PD["E2NBI4"] / npinis])
                    PD["E2NBI4DS"] = fprov
                if PD["E3NBI4"] is not None:
                    PD["E3NBI4"] = np.array([PD["E3NBI4"] / npinis])
                    PD["E3NBI4DS"] = fprov
            if 'nbi4/gasa' in m and RD[m['nbi4/gasa']] is not None and 'nbi4/gasz' in m and RD[m['nbi4/gasz']] is not None:
                PD["ANBI4"] = RD[m['nbi4/gasa']]
                PD["ANBI4DS"] = RD["I_"+m['nbi4/gasa']]
                PD["ZNBI4"] = RD[m['nbi4/gasz']]
                PD["ZNBI4DS"] = RD["I_"+m['nbi4/gasz']]
            PD["PININBI4"] = copy.deepcopy(pinilist)
            PD["PININBI4DS"] = eprov
        else:
            PD["PNBI4"] = None

        PD["PNBI8"] = None
        if 'nbi8/powt' in m and RD[m['nbi8/powt']] is not None:
            (PD["PNBI8"], PD["PNBI8EB"], pnbiem, navgmap) = ptools.filtered_average(RD[m['nbi8/powt']], percent_errors=0.1, lower_bounds=1.0e3, use_n=True)    # Taken from DDA Handbook
            if PD["PNBI8"] is not None:
                PD["PNBI8DS"] = RD["I_"+m['nbi8/powt']]
        if PD["PNBI8"] is not None and PD["PNBI8"] > 1.0e3:
            PD["BOXNBI"].append("8")
            PD["ENBI8"] = None
            PD["E1NBI8"] = None
            PD["E2NBI8"] = None
            PD["E3NBI8"] = None
            pinilist = []
            eprov = ""
            fprov = ""
            for ii in range(8):
                istr = "%d" % (ii+1)
                if 'nbi8/eng'+istr in m and RD[m['nbi8/eng'+istr]] is not None and 'nbi8/pfr'+istr in m and RD[m['nbi8/pfr'+istr]] is not None:
                    PD["ENBI8"] = RD[m['nbi8/eng'+istr]] if PD["ENBI8"] is None else PD["ENBI8"] + RD[m['nbi8/eng'+istr]]
                    pfrac = RD[m['nbi8/pfr'+istr]] / np.sum(RD[m['nbi8/pfr'+istr]])
                    PD["E1NBI8"] = pfrac[0] if PD["E1NBI8"] is None else PD["E1NBI8"] + pfrac[0]
                    PD["E2NBI8"] = pfrac[1] if PD["E2NBI8"] is None else PD["E2NBI8"] + pfrac[1]
                    PD["E3NBI8"] = pfrac[2] if PD["E3NBI8"] is None else PD["E3NBI8"] + pfrac[2]
                    pinilist.append(ii+1)
                    eprov = eprov + "; " + RD["I_"+m['nbi8/eng'+istr]] if eprov else RD["I_"+m['nbi8/eng'+istr]]
                    fprov = fprov + "; " + RD["I_"+m['nbi8/pfr'+istr]] if fprov else RD["I_"+m['nbi8/pfr'+istr]]
            if len(pinilist) > 0:
                npinis = float(len(pinilist))
                if PD["ENBI8"] is not None:
                    PD["ENBI8"] = np.array([PD["ENBI8"] / npinis])
                    PD["ENBI8DS"] = eprov
                if PD["E1NBI8"] is not None:
                    PD["E1NBI8"] = np.array([PD["E1NBI8"] / npinis])
                    PD["E1NBI8DS"] = fprov
                if PD["E2NBI8"] is not None:
                    PD["E2NBI8"] = np.array([PD["E2NBI8"] / npinis])
                    PD["E2NBI8DS"] = fprov
                if PD["E3NBI8"] is not None:
                    PD["E3NBI8"] = np.array([PD["E3NBI8"] / npinis])
                    PD["E3NBI8DS"] = fprov
            if 'nbi8/gasa' in m and RD[m['nbi8/gasa']] is not None and 'nbi8/gasz' in m and RD[m['nbi8/gasz']] is not None:
                PD["ANBI8"] = RD[m['nbi8/gasa']]
                PD["ANBI8DS"] = RD["I_"+m['nbi8/gasa']]
                PD["ZNBI8"] = RD[m['nbi8/gasz']]
                PD["ZNBI8DS"] = RD["I_"+m['nbi8/gasz']]
            PD["PININBI8"] = copy.deepcopy(pinilist)
            PD["PININBI8DS"] = eprov
        else:
            PD["PNBI8"] = None

        if fdebug and len(PD["BOXNBI"]) > 0:
            if "4" in PD["BOXNBI"]:
                print("process_jet.py: unpack_config_data(): NBI configuration (JET - Octant 4) added.")
            if "8" in PD["BOXNBI"]:
                print("process_jet.py: unpack_config_data(): NBI configuration (JET - Octant 8) added.")

        PD["ANTICRH"] = []
        if 'icrh/ptot' in m and RD[m['icrh/ptot']] is not None:
            PD["PRFA"] = np.array([0.0])
            if 'icrh/prfa' in m and RD[m['icrh/prfa']] is not None:
                vals = RD[m['icrh/prfa']]
                errs = RD[m['icrh/prae']] if 'icrh/prae' in m and RD[m['icrh/prae']] is not None else vals * 0.25
                (PD["PRFA"], PD["PRFAEB"], prfeb, navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
                if PD["PRFA"] is not None and PD["PRFA"] > 1.0e3:
                    PD["PRFADS"] = RD["I_"+m['icrh/prfa']]
                else:
                    PD["PRFA"] = np.array([0.0])
                PD["ANTICRH"].append("A")
            PD["PRFB"] = np.array([0.0])
            if 'icrh/prfb' in m and RD[m['icrh/prfb']] is not None:
                vals = RD[m['icrh/prfb']]
                errs = RD[m['icrh/prbe']] if 'icrh/prbe' in m and RD[m['icrh/prbe']] is not None else vals * 0.25
                (PD["PRFB"], PD["PRFBEB"], prfeb, navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
                if PD["PRFB"] is not None and PD["PRFB"] > 1.0e3:
                    PD["PRFBDS"] = RD["I_"+m['icrh/prfb']]
                else:
                    PD["PRFB"] = np.array([0.0])
                PD["ANTICRH"].append("B")
            PD["PRFC"] = np.array([0.0])
            if 'icrh/prfc' in m and RD[m['icrh/prfc']] is not None:
                vals = RD[m['icrh/prfc']]
                errs = RD[m['icrh/prce']] if 'icrh/prce' in m and RD[m['icrh/prce']] is not None else vals * 0.25
                (PD["PRFC"], PD["PRFCEB"], prfeb, navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
                if PD["PRFC"] is not None and PD["PRFC"] > 1.0e3:
                    PD["PRFCDS"] = RD["I_"+m['icrh/prfc']]
                else:
                    PD["PRFC"] = np.array([0.0])
                PD["ANTICRH"].append("C")
            PD["PRFD"] = np.array([0.0])
            if 'icrh/prfd' in m and RD[m['icrh/prfd']] is not None:
                vals = RD[m['icrh/prfd']]
                errs = RD[m['icrh/prde']] if 'icrh/prde' in m and RD[m['icrh/prde']] is not None else vals * 0.25
                (PD["PRFD"], PD["PRFDEB"], prfeb, navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
                if PD["PRFD"] is not None and PD["PRFD"] > 1.0e3:
                    PD["PRFDDS"] = RD["I_"+m['icrh/prfd']]
                else:
                    PD["PRFD"] = np.array([0.0])
                PD["ANTICRH"].append("D")
            PD["PRFE"] = np.array([0.0])
            if 'icrh/prfe' in m and RD[m['icrh/prfe']] is not None:
                vals = RD[m['icrh/prfe']]
                errs = vals * 0.25
                (PD["PRFE"], PD["PRFEEB"], prfeb, navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
                if PD["PRFE"] is not None and PD["PRFE"] > 1.0e3:
                    PD["PRFEDS"] = RD["I_"+m['icrh/prfe']]
                else:
                    PD["PRFE"] = np.array([0.0])
                PD["ANTICRH"].append("E")

            if len(PD["ANTICRH"]) > 0 and (PD["PRFA"] + PD["PRFB"] + PD["PRFC"] + PD["PRFD"] + PD["PRFE"]) > 1.0e3:
                rfflag = True
                PD["PHASRFA"] = np.array([180])
                if 'icrh/pha' in m and RD[m['icrh/pha']] is not None:
                    phdiffavg = np.mean(np.abs(np.diff(RD[m['icrh/pha']])))
                    PD["PHASRFA"] = np.array([90]) if phdiffavg < 170.0 and phdiffavg > 10.0 else np.array([180])
                    PD["PHASRFADS"] = RD["I_"+m['icrh/pha']]
                PD["FREQRFA"] = np.array([0.0])
                if 'icrh/fra' in m and RD[m['icrh/fra']] is not None:
                    PD["FREQRFA"] = RD[m['icrh/fra']]
                    PD["FREQRFADS"] = RD["I_"+m['icrh/fra']]
                PD["PHASRFB"] = np.array([180])
                if 'icrh/phb' in m and RD[m['icrh/phb']] is not None:
                    phdiffavg = np.mean(np.abs(np.diff(RD[m['icrh/phb']])))
                    PD["PHASRFB"] = np.array([90]) if phdiffavg < 170.0 and phdiffavg > 10.0 else np.array([180])
                    PD["PHASRFBDS"] = RD["I_"+m['icrh/phb']]
                PD["FREQRFB"] = np.array([0.0])
                if 'icrh/frb' in m and RD[m['icrh/frb']] is not None:
                    PD["FREQRFB"] = RD[m['icrh/frb']]
                    PD["FREQRFBDS"] = RD["I_"+m['icrh/frb']]
                PD["PHASRFC"] = np.array([180])
                if 'icrh/phc' in m and RD[m['icrh/phc']] is not None:
                    phdiffavg = np.mean(np.abs(np.diff(RD[m['icrh/phc']])))
                    PD["PHASRFC"] = np.array([90]) if phdiffavg < 170.0 and phdiffavg > 10.0 else np.array([180])
                    PD["PHASRFCDS"] = RD["I_"+m['icrh/phc']]
                PD["FREQRFC"] = np.array([0.0])
                if 'icrh/frc' in m and RD[m['icrh/frc']] is not None:
                    PD["FREQRFC"] = RD[m['icrh/frc']]
                    PD["FREQRFCDS"] = RD["I_"+m['icrh/frc']]
                PD["PHASRFD"] = np.array([180])
                if 'icrh/phd' in m and RD[m['icrh/phd']] is not None:
                    phdiffavg = np.mean(np.abs(np.diff(RD[m['icrh/phd']])))
                    PD["PHASRFD"] = np.array([90]) if phdiffavg < 170.0 and phdiffavg > 10.0 else np.array([180])
                    PD["PHASRFDDS"] = RD["I_"+m['icrh/phd']]
                PD["FREQRFD"] = np.array([0.0])
                if 'icrh/frd' in m and RD[m['icrh/frd']] is not None:
                    PD["FREQRFD"] = RD[m['icrh/frd']]
                    PD["FREQRFDDS"] = RD["I_"+m['icrh/frd']]
                PD["PHASRFE"] = np.array([180])
                if 'icrh/phe' in m and RD[m['icrh/phe']] is not None:
                    phdiffavg = np.mean(np.abs(np.diff(RD[m['icrh/phe']])))
                    PD["PHASRFE"] = np.array([90]) if phdiffavg < 170.0 and phdiffavg > 10.0 else np.array([180])
                    PD["PHASRFEDS"] = RD["I_"+m['icrh/phe']]
                PD["FREQRFE"] = np.array([0.0])
                if 'icrh/fre' in m and RD[m['icrh/fre']] is not None:
                    PD["FREQRFE"] = RD[m['icrh/fre']]
                    PD["FREQRFEDS"] = RD["I_"+m['icrh/fre']]

        if fdebug and len(PD["ANTICRH"]) > 0:
            if "A" in PD["ANTICRH"] or "B" in PD["ANTICRH"] or "C" in PD["ANTICRH"] or "D" in PD["ANTICRH"]:
                print("process_jet.py: unpack_config_data(): ICRH configuration (JET - A2) added.")
            if "E" in PD["ANTICRH"]:
                print("process_jet.py: unpack_config_data(): ICRH configuration (JET - ILA) added.")

    if fdebug:
        print("process_jet.py: unpack_config_data() completed.")

    return PD


def unpack_coord_data(rawdata, newstruct=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Unpacks the required coordinate data from the raw extracted data container,
    pre-processes it with statistical averaging if necessary, and stores it in
    another data container. Output should be in COCOS=1 definition.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with processed coordinate data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    logfitpar = 1.0e-5        # Factor to avoid computing logarithm of zero

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None
        fullflag = True if "SELECTEQ" in RD and re.match(r'^all$', RD["SELECTEQ"], flags=re.IGNORECASE) else False

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['efit']
        eqdda = eqlist[0].upper() if len(eqlist) > 0 else None
        if fullflag:
            for ii in range(len(eqlist)):
                if eqlist[ii].upper() not in PD:
                    PD[eqlist[ii].upper()] = dict()

        # Specification of time ranges to mask for ELM filtering
        tmlist = PD["ELMMASK"] if "ELMMASK" in PD else None

        if fdebug:
            print("process_jet.py: unpack_coord_data(): Setup completed.")

        # Geometric central radial coordinate of equilibrium - averaging expected to be done during extraction
        tag = 'rgeo'
        vtag = "RGEO"         # (Rmax + Rmin) / 2
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]

        # Geometric central vertical coordinate of equilibrium - averaging expected to be done during extraction
        tag = 'zgeo'
        vtag = "ZGEO"         # (Zmax - Zmin) / 2
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]

        if fdebug and (PD["RGEO"] is not None and PD["ZGEO"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of geometric centre added.")

        # Radial coordinate of magnetic axis - averaging expected to be done during extraction
        tag = 'rmag'
        vtag = "RMAG"         # R coord of magnetic axis
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]

        # Vertical coordinate of magnetic axis - averaging expected to be done during extraction
        tag = 'zmag'
        vtag = "ZMAG"         # Z coord of magnetic axis
        PD[vtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]

        if fdebug and (PD["RMAG"] is not None and PD["ZMAG"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of magnetic centre added.")

        # Polodial flux at magnetic axis and LCFS, used to undo normalization
        taga = 'fbnd'
        tagb = 'faxs'
        vtaga = "FBND"       # PSI at plasma boundary
        vtagb = "FAXS"       # PSI at magnetic axis
        PD[vtaga] = None
        PD[vtagb] = None
        for ii in range(len(eqlist)):
            if PD[vtaga] is None or PD[vtagb] is None:
                avals = RD[m[eqlist[ii]+'/'+taga]].flatten() if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None else None
                atvals = RD["T_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                aprov = RD["I_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                if avals is not None and atvals is not None and tmlist is not None:
                    atmask = ptools.create_range_mask(atvals, tmlist)
                    if atmask is not None:
                        avals = avals[atmask]
                aerrs = None
                (PD[vtaga], PD[vtaga+"EB"], stdmean, nf) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                PD[vtaga+"DS"] = aprov
                bvals = RD[m[eqlist[ii]+'/'+tagb]].flatten() if eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+taga]] is not None else None
                btvals = RD["T_"+m[eqlist[ii]+'/'+tagb]] if bvals is not None else None
                bprov = RD["I_"+m[eqlist[ii]+'/'+tagb]] if bvals is not None else None
                if bvals is not None and btvals is not None and tmlist is not None:
                    btmask = ptools.create_range_mask(btvals, tmlist)
                    if btmask is not None:
                        bvals = bvals[btmask]
                berrs = None
                (PD[vtagb], PD[vtagb+"EB"], stdmean, nf) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                PD[vtagb+"DS"] = bprov
                # Convert JET PPF into COCOS 1 convention
                if PD[vtaga] is not None:
                    PD[vtaga] = -PD[vtaga]
                if PD[vtagb] is not None:
                    PD[vtagb] = -PD[vtagb]
            if len(eqlist) > 1:
                avals = RD[m[eqlist[ii]+'/'+taga]].flatten() if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None else None
                atvals = RD["T_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                aprov = RD["I_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                if avals is not None and atvals is not None and tmlist is not None:
                    atmask = ptools.create_range_mask(atvals, tmlist)
                    if atmask is not None:
                        avals = avals[atmask]
                aerrs = None
                (PD[eqlist[ii].upper()][vtaga], PD[eqlist[ii].upper()][vtaga+"EB"], stdmean, nf) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                PD[eqlist[ii].upper()][vtaga+"DS"] = aprov
                bvals = RD[m[eqlist[ii]+'/'+tagb]].flatten() if eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+taga]] is not None else None
                btvals = RD["T_"+m[eqlist[ii]+'/'+tagb]] if bvals is not None else None
                bprov = RD["I_"+m[eqlist[ii]+'/'+tagb]] if bvals is not None else None
                if bvals is not None and btvals is not None and tmlist is not None:
                    btmask = ptools.create_range_mask(btvals, tmlist)
                    if btmask is not None:
                        bvals = bvals[btmask]
                berrs = None
                (PD[eqlist[ii].upper()][vtagb], PD[eqlist[ii].upper()][vtagb+"EB"], stdmean, nf) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                PD[eqlist[ii].upper()][vtagb+"DS"] = bprov
                # Convert JET PPF into COCOS 1 convention
                if PD[eqlist[ii].upper()][vtaga] is not None:
                    PD[eqlist[ii].upper()][vtaga] = -PD[eqlist[ii].upper()][vtaga]
                if PD[eqlist[ii].upper()][vtagb] is not None:
                    PD[eqlist[ii].upper()][vtagb] = -PD[eqlist[ii].upper()][vtagb]

        if fdebug and (PD["FBND"] is not None and PD["FAXS"] is not None):
            print("process_jet.py: unpack_coord_data(): Poloidal flux normalization values added.")

        # Safety factor profile
        PD["QDDA"] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            vals = RD[m[eqlist[ii]+'/q']] if eqlist[ii]+'/q' in m else None
            tvals = RD["T_"+m[eqlist[ii]+'/q']] if vals is not None else None
            prov = RD["I_"+m[eqlist[ii]+'/q']] if vals is not None else None
            if vals is not None and tvals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(tvals, tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            errs = None
            (PD["Q"+eqtag], PD["QEB"+eqtag], PD["QEM"+eqtag], navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
            PD["NQ"+eqtag] = np.count_nonzero(navgmap, axis=0) if PD["Q"+eqtag] is not None else None
            PD["QPFN"+eqtag] = RD["X_"+m[eqlist[ii]+'/q']] if vals is not None else None
            PD["Q"+eqtag+"DS"] = prov
            if vals is not None and PD["QDDA"] is None:
                PD["QDDA"] = eqtag
            if len(eqlist) > 1:
                PD[eqtag]["Q"] = PD["Q"+eqtag].copy() if PD["Q"+eqtag] is not None else None
                PD[eqtag]["QEB"] = PD["QEB"+eqtag].copy() if PD["Q"+eqtag] is not None else None
                PD[eqtag]["QPFN"] = PD["QPFN"+eqtag].copy() if PD["Q"+eqtag] is not None else None
                PD[eqtag]["QDS"] = PD["Q"+eqtag+"DS"] if PD["Q"+eqtag+"DS"] is not None else None

        if fdebug and PD["QDDA"] is not None and PD["Q"+PD["QDDA"]] is not None:
            print("process_jet.py: unpack_coord_data(): %s equilibrium chosen as main." % (PD["QDDA"]))

        # These are the JET defaults for EFIT (r,z) map
        eq_npoints = int(PD["Q"+PD["QDDA"]].size)
        rvec_default = np.linspace(1.65, 4.05, eq_npoints)
        zvec_default = np.linspace(-1.90, 2.15, eq_npoints)

        # Poloidal flux as a function of R coord and Z coord, normalized if data is available - averaging expected to be done during extraction (really should be handled here)
        tag = 'psi'
        taga = 'psir'
        tagb = 'psiz'
        tagc = 'sspr'
        tagd = 'sspi'
        vtag = "PFXMAP"
        rtag = "RMAP"
        ztag = "ZMAP"
        ntag = "PFNMAP"
        PD[vtag] = None
        PD[rtag] = None
        PD[ztag] = None
        PD[ntag] = None
        # This branch is used if full 2D psi map is found in EFIT DDA - reshapes flattened vector
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                # Convert JET PPF into COCOS 1/11 convention only if grid is not already normalized
                if np.nanmin(PD[vtag]) != 0.0 and np.nanmax(PD[vtag]) != 0.0:
                    PD[vtag] = -PD[vtag]
                PD[rtag] = None
                PD[ztag] = None
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
                if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None and eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+tagb]] is not None:
                    rvec = RD[m[eqlist[ii]+'/'+taga]]
                    zvec = RD[m[eqlist[ii]+'/'+tagb]]
                    prov = RD["I_"+m[eqlist[ii]+'/'+taga]] + "; " + RD["I_"+m[eqlist[ii]+'/'+tagb]]
                    if rvec is not None and zvec is not None:
                        (PD[rtag], PD[ztag]) = np.meshgrid(rvec.flatten(), zvec.flatten())
                        PD[vtag] = np.reshape(PD[vtag], PD[rtag].shape)
                        PD[vtag+"DS"] = PD[vtag+"DS"] + "; " + prov
                else:
                    (PD[rtag], PD[ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                    PD[vtag] = np.reshape(PD[vtag], PD[rtag].shape)
                if len(eqlist) > 1:
                    eqtag = eqlist[ii].upper()
                    if PD[vtag] is not None and eqtag in PD and "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                        PD[ntag] = (PD[eqtag]["FAXS"] - PD[vtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])
                        if PD[rtag] is not None and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                            hfilt = (PD[rtag] < PD[eqtag]["RMAG"])
                            PD[ntag][hfilt] = -PD[ntag][hfilt]
                        PD[ntag+"DS"] = "EX2GK: Internal calculation"
                else:
                    if PD[vtag] is not None and "FBND" in PD and "FAXS" in PD and PD["FBND"] is not None and PD["FAXS"] is not None:
                        PD[ntag] = (PD["FAXS"] - PD[vtag]) / (PD["FAXS"] - PD["FBND"])
                        if PD[rtag] is not None and "RMAG" in PD and PD["RMAG"] is not None:
                            hfilt = (PD[rtag] < PD["RMAG"])
                            PD[ntag][hfilt] = -PD[ntag][hfilt]
                        PD[ntag+"DS"] = "EX2GK: Internal calculation"
            if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                PD[eqtag][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                # Convert JET PPF into COCOS 1 convention only if grid is not already normalized
                if np.nanmin(PD[eqtag][vtag]) != 0.0 and np.nanmax(PD[eqtag][vtag]) != 0.0:
                    PD[eqtag][vtag] = -PD[eqtag][vtag]
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                PD[eqtag][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
                if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None and eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+tagb]] is not None:
                    rvec = RD[m[eqlist[ii]+'/'+taga]]
                    zvec = RD[m[eqlist[ii]+'/'+tagb]]
                    if rvec is not None and zvec is not None:
                        (PD[eqtag][rtag], PD[eqtag][ztag]) = np.meshgrid(rvec.flatten(), zvec.flatten())
                        PD[eqtag][vtag] = np.reshape(PD[eqtag][vtag], PD[eqtag][rtag].shape)
                        PD[vtag+"DS"] = PD[vtag+"DS"] + "; " + prov
                else:
                    (PD[eqtag][rtag], PD[eqtag][ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                    PD[eqtag][vtag] = np.reshape(PD[eqtag][vtag], PD[eqtag][rtag].shape)
                if PD[eqtag][vtag] is not None and "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag][ntag] = (PD[eqtag]["FAXS"] - PD[eqtag][vtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])
                    if PD[eqtag][rtag] is not None and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                        hfilt = (PD[eqtag][rtag] < PD[eqtag]["RMAG"])
                        PD[eqtag][ntag][hfilt] = -PD[eqtag][ntag][hfilt]
                    PD[eqtag][ntag+"DS"] = "EX2GK: Internal calculation"
        # Otherwise, this branch is used to reconstruct 2D psi map from 2D spline coefficients - the format used when disk space was precious
        if PD[vtag] is None:
            for ii in range(len(eqlist)):
                if eqlist[ii]+'/'+tagc in m and RD[m[eqlist[ii]+'/'+tagc]] is not None and RD[m[eqlist[ii]+'/'+tagc]].ndim > 1 and eqlist[ii]+'/'+tagd in m and RD[m[eqlist[ii]+'/'+tagd]] is not None:
                    vmaps = None
                    nmaps = None
                    prov = RD["I_"+m[eqlist[ii]+'/'+tagc]] + "; " + RD["I_"+m[eqlist[ii]+'/'+tagd]]
                    for jj in range(RD[m[eqlist[ii]+'/'+tagc]].shape[0]):
                        nrknots = int(RD[m[eqlist[ii]+'/'+tagd]][jj, 0])
                        nzknots = int(RD[m[eqlist[ii]+'/'+tagd]][jj, 1])
                        ncoeffs = int(RD[m[eqlist[ii]+'/'+tagd]][jj, 2])
                        rknots = RD[m[eqlist[ii]+'/'+tagc]][jj, :nrknots]
                        zknots = RD[m[eqlist[ii]+'/'+tagc]][jj, nrknots:nrknots+nzknots]
                        scoeffs = RD[m[eqlist[ii]+'/'+tagc]][jj, nrknots+nzknots:nrknots+nzknots+ncoeffs]
                        rdeg = 0
                        while int(rknots[rdeg+1]) == int(rknots[0]):
                            rdeg = rdeg + 1
                        zdeg = 0
                        while int(zknots[zdeg+1]) == int(zknots[0]):
                            zdeg = zdeg + 1
                        rvec = np.linspace(np.nanmin(rknots), np.nanmax(rknots), eq_npoints)
                        zvec = np.linspace(np.nanmin(zknots), np.nanmax(zknots), eq_npoints)
                        spvals = [rknots.tolist(), zknots.tolist(), scoeffs.tolist(), rdeg, zdeg]
                        temppsn = bisplev(rvec, zvec, spvals)
#                        if re.match(r'^eftm$', eqlist[ii], flags=re.IGNORECASE):
#                            nmaps = np.dstack((nmaps, temppsn)) if nmaps is not None else copy.deepcopy(temppsn)
                        nmaps = np.dstack((nmaps, temppsn.T)) if nmaps is not None else copy.deepcopy(temppsn.T)
                        if eqlist[ii]+'/faxs' in m and RD[m[eqlist[ii]+'/faxs']] is not None and eqlist[ii]+'/fbnd' in m and RD[m[eqlist[ii]+'/fbnd']] is not None:
                            fbnd = RD[m[eqlist[ii]+'/fbnd']][jj]
                            faxs = RD[m[eqlist[ii]+'/faxs']][jj]
                            # Convert JET PPF into COCOS 1 convention only if grid is not already normalized
                            if fbnd > faxs and float(faxs) != 0.0:
                                fbnd = -fbnd
                                faxs = -faxs
                            temppsi = temppsn * (fbnd - faxs) + faxs
                            vmaps = np.dstack((vmaps, temppsi.T)) if vmaps is not None else copy.deepcopy(temppsi.T)
                            prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/faxs']] + "; " + RD["I_"+m[eqlist[ii]+'/fbnd']]
                    if vmaps is not None:
                        PD[vtag] = np.mean(vmaps, axis=2) if vmaps.ndim > 2 else copy.deepcopy(vmaps)
                        (PD[rtag], PD[ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                        PD[vtag+"DS"] = prov
#                        if len(eqlist) > 1:
#                            if PD[vtag] is not None and eqtag in PD and "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
#                                PD[ntag] = (PD[eqtag]["FAXS"] - PD[vtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])
#                                if PD[rtag] is not None and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
#                                    hfilt = (PD[rtag] < PD[eqtag]["RMAG"])
#                                    PD[ntag][hfilt] = -PD[ntag][hfilt]
#                                PD[ntag+"DS"] = "EX2GK: Internal calculation"
#                        else:
                        if PD[vtag] is not None and "FBND" in PD and "FAXS" in PD and PD["FBND"] is not None and PD["FAXS"] is not None:
                            PD[ntag] = (PD["FAXS"] - PD[vtag]) / (PD["FAXS"] - PD["FBND"])   # Already converted to COCOS 1/11 earlier
                            if PD[rtag] is not None and "RMAG" in PD and PD["RMAG"] is not None:
                                hfilt = (PD[rtag] < PD["RMAG"])
                                PD[ntag][hfilt] = -PD[ntag][hfilt]
                            PD[ntag+"DS"] = "EX2GK: Internal calculation"
                    elif nmaps is not None:
                        PD[ntag] = np.mean(nmaps, axis=2) if nmaps.ndim > 2 else copy.deepcopy(nmaps)
                        (PD[rtag], PD[ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                        PD[ntag+"DS"] = prov
#                        if len(eqlist) > 1:
#                            if PD[rtag] is not None and eqtag in PD and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
#                                hfilt = (PD[rtag] < PD[eqtag]["RMAG"])
#                                PD[ntag][hfilt] = -PD[ntag][hfilt]
#                        else:
                        if PD[rtag] is not None and "RMAG" in PD and PD["RMAG"] is not None:
                            hfilt = (PD[rtag] < PD["RMAG"])
                            PD[ntag][hfilt] = -PD[ntag][hfilt]
                if eqlist[ii]+'/'+tagc in m and RD[m[eqlist[ii]+'/'+tagc]] is not None and RD[m[eqlist[ii]+'/'+tagc]].ndim > 1 and eqlist[ii]+'/'+tagd in m and RD[m[eqlist[ii]+'/'+tagd]] is not None and len(eqlist) > 1:
                    eqtag = eqlist[ii].upper()
                    vmaps = None
                    nmaps = None
                    prov = RD["I_"+m[eqlist[ii]+'/'+tagc]] + "; " + RD["I_"+m[eqlist[ii]+'/'+tagd]]
                    for jj in range(RD[m[eqlist[ii]+'/'+tagc]].shape[0]):
                        nrknots = int(RD[m[eqlist[ii]+'/'+tagd]][jj, 0])
                        nzknots = int(RD[m[eqlist[ii]+'/'+tagd]][jj, 1])
                        ncoeffs = int(RD[m[eqlist[ii]+'/'+tagd]][jj, 2])
                        rknots = RD[m[eqlist[ii]+'/'+tagc]][jj, :nrknots]
                        zknots = RD[m[eqlist[ii]+'/'+tagc]][jj, nrknots:nrknots+nzknots]
                        scoeffs = RD[m[eqlist[ii]+'/'+tagc]][jj, nrknots+nzknots:nrknots+nzknots+ncoeffs]
                        rdeg = 0
                        while int(rknots[rdeg+1]) == int(rknots[0]):
                            rdeg = rdeg + 1
                        zdeg = 0
                        while int(zknots[zdeg+1]) == int(zknots[0]):
                            zdeg = zdeg + 1
                        rvec = np.linspace(np.nanmin(rknots), np.nanmax(rknots), eq_npoints)
                        zvec = np.linspace(np.nanmin(zknots), np.nanmax(zknots), eq_npoints)
                        spvals = [rknots.tolist(), zknots.tolist(), scoeffs.tolist(), rdeg, zdeg]
                        temppsn = bisplev(rvec, zvec, spvals)
                        nmaps = np.dstack((nmaps, temppsn.T)) if nmaps is not None else copy.deepcopy(temppsn.T)
                        if eqlist[ii]+'/faxs' in m and RD[m[eqlist[ii]+'/faxs']] is not None and eqlist[ii]+'/fbnd' in m and RD[m[eqlist[ii]+'/fbnd']] is not None:
                            fbnd = RD[m[eqlist[ii]+'/fbnd']][jj, 0]
                            faxs = RD[m[eqlist[ii]+'/faxs']][jj, 0]
                            # Convert JET PPF into COCOS 1 convention only if grid is not already normalized
                            if fbnd > faxs and float(faxs) != 0.0:
                                fbnd = -fbnd
                                faxs = -faxs
                            temppsi = temppsn * (fbnd - faxs) + faxs
                            vmaps = np.dstack((vmaps, temppsi.T)) if vmaps is not None else copy.deepcopy(temppsi.T)
                            prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/faxs']] + "; " + RD["I_"+m[eqlist[ii]+'/fbnd']]
                    if vmaps is not None:
                        PD[eqtag][vtag] = np.mean(vmaps, axis=2) if vmaps.ndim > 2 else copy.deepcopy(vmaps)
                        (PD[eqtag][rtag], PD[eqtag][ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                        PD[eqtag][vtag+"DS"] = prov
                        if PD[eqtag][vtag] is not None and "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                            PD[eqtag][ntag] = (PD[eqtag]["FAXS"] - PD[eqtag][vtag]) / (PD[eqtag]["FAXS"] - PD[eqtag]["FBND"])   # Already converted to COCOS 1/11 earlier
                            if PD[eqtag][rtag] is not None and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                                hfilt = (PD[eqtag][rtag] < PD[eqtag]["RMAG"])
                                PD[eqtag][ntag][hfilt] = -PD[eqtag][ntag][hfilt]
                        PD[eqtag][ntag+"DS"] = "EX2GK: Internal calculation"
                    elif nmaps is not None:
                        PD[eqtag][ntag] = np.mean(nmaps, axis=2) if nmaps.ndim > 2 else copy.deepcopy(nmaps)
                        (PD[eqtag][rtag], PD[eqtag][ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                        PD[eqtag][ntag+"DS"] = prov
                        if PD[eqtag][rtag] is not None and "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                            hfilt = (PD[eqtag][rtag] < PD[eqtag]["RMAG"])
                            PD[eqtag][ntag][hfilt] = -PD[eqtag][ntag][hfilt]

        if fdebug and (PD["PFXMAP"] is not None or PD["PFNMAP"] is not None):
            print("process_jet.py: unpack_coord_data(): 2D poloidal flux map added.")

        # Standardized plasma boundary location as a function of poloidal angle
        taga = 'rbnd'
        tagb = 'zbnd'
        rtag = "RBND"
        ztag = "ZBND"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m:
                rvec = RD[m[eqlist[ii]+'/'+taga]]
                zvec = RD[m[eqlist[ii]+'/'+tagb]]
                if rvec is not None and zvec is not None:
                    trvec = RD["T_"+m[eqlist[ii]+'/'+taga]]
                    if trvec is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(trvec, tmlist)
                        if tmask is not None:
                            rvec = rvec[tmask]
                    (PD[rtag], PD[rtag+"EB"], stdmean, dummy) = ptools.filtered_average(rvec)
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    tzvec = RD["T_"+m[eqlist[ii]+'/'+tagb]]
                    if tzvec is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(tzvec, tmlist)
                        if tmask is not None:
                            zvec = zvec[tmask]
                    (PD[ztag], PD[ztag+"EB"], stdmean, dummy) = ptools.filtered_average(zvec)
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
#                PD[eqtag][rtag] = None
#                PD[eqtag][ztag] = None
                rvec = RD[m[eqlist[ii]+'/'+taga]]
                zvec = RD[m[eqlist[ii]+'/'+tagb]]
                if rvec is not None and zvec is not None:
                    trvec = RD["T_"+m[eqlist[ii]+'/'+taga]]
                    if trvec is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(trvec, tmlist)
                        if tmask is not None:
                            rvec = rvec[tmask]
                    (PD[eqtag][rtag], PD[eqtag][rtag+"EB"], stdmean, dummy) = ptools.filtered_average(rvec)
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    tzvec = RD["T_"+m[eqlist[ii]+'/'+tagb]]
                    if tzvec is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(tzvec, tmlist)
                        if tmask is not None:
                            zvec = zvec[tmask]
                    (PD[eqtag][ztag], PD[eqtag][ztag+"EB"], stdmean, dummy) = ptools.filtered_average(zvec)
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RBND"] is not None and PD["ZBND"] is not None):
            print("process_jet.py: unpack_coord_data(): LCFS contour added.")

        # Lower X-point location, calculated using equilibrium code - averaging expected to be done during extraction
        taga = 'rxpl'
        tagb = 'zxpl'
        rtag = "RXPL"
        ztag = "ZXPL"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and (PD[rtag] is None or PD[ztag] is None):
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[rtag] = rcoord
                    PD[ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[eqtag][rtag] = rcoord
                    PD[eqtag][ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RXPL"] is not None and PD["ZXPL"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of lower X-point added.")

        # Upper X-point location, calculated using equilibrium code - averaging expected to be done during extraction
        taga = 'rxpu'
        tagb = 'zxpu'
        rtag = "RXPU"
        ztag = "ZXPU"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and (PD[rtag] is None or PD[ztag] is None):
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[rtag] = rcoord
                    PD[ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[eqtag][rtag] = rcoord
                    PD[eqtag][ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RXPU"] is not None and PD["ZXPU"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of upper X-point added.")

        # Lower inner strike point location, calculated using equilibrium code - averaging expected to be done during extraction
        taga = 'rsil'
        tagb = 'zsil'
        rtag = "RSPIL"
        ztag = "ZSPIL"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and (PD[rtag] is None or PD[ztag] is None):
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[rtag] = rcoord
                    PD[ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[eqtag][rtag] = rcoord
                    PD[eqtag][ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RSPIL"] is not None and PD["ZSPIL"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of inner lower strike point added.")

        # Lower outer strike point location, calculated using equilibrium code - averaging expected to be done during extraction
        taga = 'rsol'
        tagb = 'zsol'
        rtag = "RSPOL"
        ztag = "ZSPOL"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and (PD[rtag] is None or PD[ztag] is None):
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[rtag] = rcoord
                    PD[ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[eqtag][rtag] = rcoord
                    PD[eqtag][ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RSPOL"] is not None and PD["ZSPOL"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of outer lower strike point added.")

        # Upper inner strike point location, calculated using equilibrium code - averaging expected to be done during extraction
        taga = 'rsiu'
        tagb = 'zsiu'
        rtag = "RSPIU"
        ztag = "ZSPIU"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and (PD[rtag] is None or PD[ztag] is None):
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[rtag] = rcoord
                    PD[ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[eqtag][rtag] = rcoord
                    PD[eqtag][ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RSPIU"] is not None and PD["ZSPIU"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of inner upper strike point added.")

        # Upper outer strike point location, calculated using equilibrium code - averaging expected to be done during extraction
        taga = 'rsou'
        tagb = 'zsou'
        rtag = "RSPOU"
        ztag = "ZSPOU"
        PD[rtag] = None
        PD[ztag] = None
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and (PD[rtag] is None or PD[ztag] is None):
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[rtag] = rcoord
                    PD[ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                PD[eqtag][rtag] = None
                PD[eqtag][ztag] = None
                rcoord = RD[m[eqlist[ii]+'/'+taga]]
                if float(rcoord) > 1.5 and float(rcoord) < 4.5:
                    PD[eqtag][rtag] = rcoord
                    PD[eqtag][ztag] = RD[m[eqlist[ii]+'/'+tagb]]
                    PD[eqtag][rtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    PD[eqtag][ztag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and (PD["RSPOU"] is not None and PD["ZSPOU"] is not None):
            print("process_jet.py: unpack_coord_data(): Position of outer upper strike point added.")

        # Flux functions, as functions of PSI_norm - averaging expected to be done during extraction (really should be handled here)
        taga = 'f'
        tagb = 'dfdp'
        vtag = "FPSI"
        vtagx = "FPSIPFN"
        dtag = "DFPSI"
        dtagx = "DFPSIPFN"
        PD[vtag] = None
        PD[dtag] = None
        PD[vtagx] = None
        PD[dtagx] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m:
                PD[vtag] = RD[m[eqlist[ii]+'/'+taga]]
                PD[dtag] = RD[m[eqlist[ii]+'/'+tagb]]   # Strangely, JET PPF seems to already store this field in COCOS 1 convention
                PD[vtagx] = RD["X_"+m[eqlist[ii]+'/'+taga]]
                PD[dtagx] = RD["X_"+m[eqlist[ii]+'/'+tagb]]
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                PD[dtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                PD[eqtag][vtag] = RD[m[eqlist[ii]+'/'+taga]]
                PD[eqtag][dtag] = RD[m[eqlist[ii]+'/'+tagb]]   # Strangely, JET PPF seems to already store this field in COCOS 1 convention
                PD[eqtag][vtagx] = RD["X_"+m[eqlist[ii]+'/'+taga]]
                PD[eqtag][dtagx] = RD["X_"+m[eqlist[ii]+'/'+tagb]]
                PD[eqtag][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                PD[eqtag][dtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and PD["FPSI"] is not None:
            print("process_jet.py: unpack_coord_data(): F-function profile added.")

        taga = 'p'
        tagb = 'dpdp'
        vtag = "PPSI"
        vtagx = "PPSIPFN"
        dtag = "DPPSI"
        dtagx = "DPPSIPFN"
        PD[vtag] = None
        PD[dtag] = None
        PD[vtagx] = None
        PD[dtagx] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m:
                PD[vtag] = RD[m[eqlist[ii]+'/'+taga]]
                PD[dtag] = RD[m[eqlist[ii]+'/'+tagb]]
                # Convert JET PPF to COCOS 1 convention
                if PD[dtag] is not None:
                    PD[dtag] = -PD[dtag]
                PD[vtagx] = RD["X_"+m[eqlist[ii]+'/'+taga]]
                PD[dtagx] = RD["X_"+m[eqlist[ii]+'/'+tagb]]
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                PD[dtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
            if eqlist[ii]+'/'+taga in m and eqlist[ii]+'/'+tagb in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                PD[eqtag][vtag] = RD[m[eqlist[ii]+'/'+taga]]
                PD[eqtag][dtag] = -RD[m[eqlist[ii]+'/'+tagb]]
                # Convert JET PPF to COCOS 1 convention
                if PD[eqtag][dtag] is not None:
                    PD[eqtag][dtag] = -PD[eqtag][dtag]
                PD[eqtag][vtagx] = RD["X_"+m[eqlist[ii]+'/'+taga]]
                PD[eqtag][dtagx] = RD["X_"+m[eqlist[ii]+'/'+tagb]]
                PD[eqtag][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                PD[eqtag][dtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]

        if fdebug and PD["PPSI"] is not None:
            print("process_jet.py: unpack_coord_data(): Pressure profile added.")

        # Major radius on ZMAG as a function of normalized polodial flux
        taga = 'rmji'
        tagb = 'rmjo'
        vtaga = "RMJI"        # R_HFS(PSI_norm) on ZMAG
        vtagb = "RMJO"        # R_LFS(PSI_norm) on ZMAG
        xtaga = "PFN2RMI"
        xtagb = "PFN2RMO"
        PD[vtaga] = None
        PD[vtagb] = None
        PD[xtaga] = None
        PD[xtagb] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None and eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+tagb]] is not None:
                if PD[xtaga] is None or PD[xtagb] is None:
                    avals = np.atleast_2d(RD[m[eqlist[ii]+'/'+taga]]) if eqlist[ii]+'/'+taga in m else None
                    if avals is not None and avals.size <= 1:
                        avals = None
                    atvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+taga]]) if avals is not None else None
                    if avals is not None and atvals is not None and tmlist is not None:
                        atmask = ptools.create_range_mask(atvals, tmlist)
                        if atmask is not None:
                            avals = avals[atmask]
                    aerrs = None
                    (PD[vtaga], PD[vtaga+"EB"], stdmean, nr) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                    PD[xtaga] = RD["X_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                    PD[vtaga+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    if PD[xtaga] is not None and PD[xtaga][0] > PD[xtaga][-1]:
                        PD[xtaga] = PD[xtaga][::-1]
                        PD[vtaga] = PD[vtaga][::-1]
                        PD[vtaga+"EB"] = PD[vtaga+"EB"][::-1]
                    if PD[vtaga] is not None and PD[vtaga][0] < PD[vtaga][-1]:
                        PD[vtaga] = PD[vtaga][::-1]
                        PD[vtaga+"EB"] = PD[vtaga+"EB"][::-1]
                    bvals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tagb]]) if eqlist[ii]+'/'+tagb in m else None
                    if bvals is not None and bvals.size <= 1:
                        bvals = None
                    btvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tagb]]) if bvals is not None else None
                    if bvals is not None and btvals is not None and tmlist is not None:
                        btmask = ptools.create_range_mask(btvals, tmlist)
                        if btmask is not None:
                            bvals = bvals[btmask]
                    berrs = None
                    (PD[vtagb], PD[vtagb+"EB"], stdmean, nr) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                    PD[xtagb] = RD["X_"+m[eqlist[ii]+'/'+tagb]] if bvals is not None else None
                    PD[vtagb+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
                    if PD[xtagb] is not None and PD[xtagb][0] > PD[xtagb][-1]:
                        PD[xtagb] = PD[xtagb][::-1]
                        PD[vtagb] = PD[vtagb][::-1]
                        PD[vtagb+"EB"] = PD[vtagb+"EB"][::-1]
                    if PD[vtagb] is not None and PD[vtagb][0] > PD[vtagb][-1]:
                        PD[vtagb] = PD[vtagb][::-1]
                        PD[vtagb+"EB"] = PD[vtagb+"EB"][::-1]
                if len(eqlist) > 1:
                    eqtag = eqlist[ii].upper()
                    if eqtag in PD and (PD[eqtag][xtaga] is None or PD[eqtag][xtagb] is None):
                        avals = np.atleast_2d(RD[m[eqlist[ii]+'/'+taga]])
                        if avals.size <= 1:
                            avals = None
                        atvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+taga]]) if avals is not None else None
                        if avals is not None and atvals is not None and tmlist is not None:
                            atmask = ptools.create_range_mask(atvals, tmlist)
                            if atmask is not None:
                                avals = avals[atmask]
                        aerrs = None
                        (PD[eqtag][vtaga], PD[eqtag][vtaga+"EB"], stdmean, nn) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                        PD[eqtag][xtaga] = RD["X_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                        PD[eqtag][vtaga+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                        if PD[eqtag][xtaga] is not None and PD[eqtag][xtaga][0] > PD[eqtag][xtaga][-1]:
                            PD[eqtag][xtaga] = PD[eqtag][xtaga][::-1]
                            PD[eqtag][vtaga] = PD[eqtag][vtaga][::-1]
                            PD[eqtag][vtaga+"EB"] = PD[eqtag][vtaga+"EB"][::-1]
                        if PD[eqtag][vtaga] is not None and PD[eqtag][vtaga][0] < PD[eqtag][vtaga][-1]:
                            PD[eqtag][vtaga] = PD[eqtag][vtaga][::-1]
                            PD[eqtag][vtaga+"EB"] = PD[eqtag][vtaga+"EB"][::-1]
                        bvals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tagb]])
                        if bvals.size <= 1:
                            bvals = None
                        btvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tagb]]) if bvals is not None else None
                        if bvals is not None and btvals is not None and tmlist is not None:
                            btmask = ptools.create_range_mask(btvals, tmlist)
                            if btmask is not None:
                                bvals = bvals[btmask]
                        berrs = np.zeros(bvals.shape) if bvals is not None else None
                        (PD[eqtag][vtagb], PD[eqtag][vtagb+"EB"], stdmean, nn) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                        PD[eqtag][xtagb] = RD["X_"+m[eqlist[ii]+'/'+tagb]] if bvals is not None else None
                        PD[eqtag][vtagb+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
                        if PD[eqtag][vtagb] is not None and PD[eqtag][vtagb][0] > PD[eqtag][vtagb][-1]:
                            PD[eqtag][xtagb] = PD[eqtag][xtagb][::-1]
                            PD[eqtag][vtagb] = PD[eqtag][vtagb][::-1]
                            PD[eqtag][vtagb+"EB"] = PD[eqtag][vtagb+"EB"][::-1]
                        if PD[eqtag][vtagb] is not None and PD[eqtag][vtagb][0] > PD[eqtag][vtagb][-1]:
                            PD[eqtag][vtagb] = PD[eqtag][vtagb][::-1]
                            PD[eqtag][vtagb+"EB"] = PD[eqtag][vtagb+"EB"][::-1]

        # Backup signals for major radius on ZMAG as a function of normalized polodial flux - uses same fields as previous block, only fills them if they are empty
        taga = 'psni'
        tagb = 'psno'
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None and eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+tagb]] is not None:
                if PD[xtaga] is None or PD[xtagb] is None:
                    avals = np.atleast_2d(RD[m[eqlist[ii]+'/'+taga]]) if eqlist[ii]+'/'+taga in m else None
                    if avals is not None and avals.size <= 1:
                        avals = None
                    atvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+taga]]) if avals is not None else None
                    if avals is not None and atvals is not None and tmlist is not None:
                        atmask = ptools.create_range_mask(atvals, tmlist)
                        if atmask is not None:
                            avals = avals[atmask]
                    aerrs = None
                    (xtemp, xebtemp, stdmean, nr) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                    vtemp = RD["X_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                    if xtemp is not None:
                        vfunc = interp1d(vtemp, xtemp, kind='linear', bounds_error=False, fill_value='extrapolate')
                        vutemp = np.abs(vfunc(xtemp + xebtemp) - vtemp)
                        vltemp = np.abs(vfunc(xtemp - xebtemp) - vtemp)
                        PD[xtaga] = copy.deepcopy(xtemp)
                        PD[vtaga] = copy.deepcopy(vtemp)
                        PD[vtaga+"EB"] = np.nanmax(np.vstack((vutemp, vltemp)), axis=0)
                        PD[vtaga+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                    if PD[xtaga] is not None and PD[xtaga][0] > PD[xtaga][-1]:
                        PD[xtaga] = PD[xtaga][::-1]
                        PD[vtaga] = PD[vtaga][::-1]
                        PD[vtaga+"EB"] = PD[vtaga+"EB"][::-1]
                    if PD[vtaga] is not None and PD[vtaga][0] < PD[vtaga][-1]:
                        PD[vtaga] = PD[vtaga][::-1]
                        PD[vtaga+"EB"] = PD[vtaga+"EB"][::-1]
                    bvals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tagb]]) if eqlist[ii]+'/'+tagb in m else None
                    if bvals is not None and bvals.size <= 1:
                        bvals = None
                    btvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tagb]]) if bvals is not None else None
                    if bvals is not None and btvals is not None and tmlist is not None:
                        btmask = ptools.create_range_mask(btvals, tmlist)
                        if btmask is not None:
                            bvals = bvals[btmask]
                    berrs = None
                    (xtemp, xebtemp, stdmean, nr) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                    vtemp = RD["X_"+m[eqlist[ii]+'/'+tagb]] if avals is not None else None
                    if xtemp is not None:
                        vfunc = interp1d(vtemp, xtemp, kind='linear', bounds_error=False, fill_value='extrapolate')
                        vutemp = np.abs(vfunc(xtemp + xebtemp) - vtemp)
                        vltemp = np.abs(vfunc(xtemp - xebtemp) - vtemp)
                        PD[xtagb] = copy.deepcopy(xtemp)
                        PD[vtagb] = copy.deepcopy(vtemp)
                        PD[vtagb+"EB"] = np.nanmax(np.vstack((vutemp, vltemp)), axis=0)
                        PD[vtagb+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
                    if PD[xtagb] is not None and PD[xtagb][0] > PD[xtagb][-1]:
                        PD[xtagb] = PD[xtagb][::-1]
                        PD[vtagb] = PD[vtagb][::-1]
                        PD[vtagb+"EB"] = PD[vtagb+"EB"][::-1]
                    if PD[vtagb] is not None and PD[vtagb][0] > PD[vtagb][-1]:
                        PD[vtagb] = PD[vtagb][::-1]
                        PD[vtagb+"EB"] = PD[vtagb+"EB"][::-1]
                if len(eqlist) > 1:
                    eqtag = eqlist[ii].upper()
                    if eqtag in PD and (PD[eqtag][xtaga] is None or PD[eqtag][xtagb] is None):
                        avals = np.atleast_2d(RD[m[eqlist[ii]+'/'+taga]])
                        if avals.size <= 1:
                            avals = None
                        atvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+taga]]) if avals is not None else None
                        if avals is not None and atvals is not None and tmlist is not None:
                            atmask = ptools.create_range_mask(atvals, tmlist)
                            if atmask is not None:
                                avals = avals[atmask]
                        aerrs = None
                        (xtemp, xebtemp, stdmean, nn) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                        vtemp = RD["X_"+m[eqlist[ii]+'/'+taga]] if avals is not None else None
                        if xtemp is not None:
                            vfunc = interp1d(vtemp, xtemp, kind='linear', bounds_error=False, fill_value='extrapolate')
                            vutemp = np.abs(vfunc(xtemp + xebtemp) - vtemp)
                            vltemp = np.abs(vfunc(xtemp - xebtemp) - vtemp)
                            PD[eqtag][xtaga] = copy.deepcopy(xtemp)
                            PD[eqtag][vtaga] = copy.deepcopy(vtemp)
                            PD[eqtag][vtaga+"EB"] = np.nanmax(np.vstack((vutemp, vltemp)), axis=0)
                            PD[eqtag][vtaga+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]]
                        if PD[eqtag][xtaga] is not None and PD[eqtag][xtaga][0] > PD[eqtag][xtaga][-1]:
                            PD[eqtag][xtaga] = PD[eqtag][xtaga][::-1]
                            PD[eqtag][vtaga] = PD[eqtag][vtaga][::-1]
                            PD[eqtag][vtaga+"EB"] = PD[eqtag][vtaga+"EB"][::-1]
                        if PD[eqtag][vtaga] is not None and PD[eqtag][vtaga][0] < PD[eqtag][vtaga][-1]:
                            PD[eqtag][vtaga] = PD[eqtag][vtaga][::-1]
                            PD[eqtag][vtaga+"EB"] = PD[eqtag][vtaga+"EB"][::-1]
                        bvals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tagb]])
                        if bvals.size <= 1:
                            bvals = None
                        btvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tagb]]) if bvals is not None else None
                        if bvals is not None and btvals is not None and tmlist is not None:
                            btmask = ptools.create_range_mask(btvals, tmlist)
                            if btmask is not None:
                                bvals = bvals[btmask]
                        berrs = np.zeros(bvals.shape) if bvals is not None else None
                        (xtemp, xebtemp, stdmean, nr) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                        vtemp = RD["X_"+m[eqlist[ii]+'/'+tagb]] if avals is not None else None
                        if xtemp is not None:
                            vfunc = interp1d(vtemp, xtemp, kind='linear', bounds_error=False, fill_value='extrapolate')
                            vutemp = np.abs(vfunc(xtemp + xebtemp) - vtemp)
                            vltemp = np.abs(vfunc(xtemp - xebtemp) - vtemp)
                            PD[eqtag][xtagb] = copy.deepcopy(xtemp)
                            PD[eqtag][vtagb] = copy.deepcopy(vtemp)
                            PD[eqtag][vtagb+"EB"] = np.nanmax(np.vstack((vutemp, vltemp)), axis=0)
                            PD[eqtag][vtagb+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tagb]]
                        if PD[eqtag][vtagb] is not None and PD[eqtag][vtagb][0] > PD[eqtag][vtagb][-1]:
                            PD[eqtag][xtagb] = PD[eqtag][xtagb][::-1]
                            PD[eqtag][vtagb] = PD[eqtag][vtagb][::-1]
                            PD[eqtag][vtagb+"EB"] = PD[eqtag][vtagb+"EB"][::-1]
                        if PD[eqtag][vtagb] is not None and PD[eqtag][vtagb][0] > PD[eqtag][vtagb][-1]:
                            PD[eqtag][vtagb] = PD[eqtag][vtagb][::-1]
                            PD[eqtag][vtagb+"EB"] = PD[eqtag][vtagb+"EB"][::-1]

        # Second backup signal for major radius on ZMAG as a function of normalized polodial flux - uses same fields as previous block, only fills them if they are empty
        #    This was the standard for discharges before JET#53260
        taga = 'psin'
        tagb = 'rpre'
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+taga in m and RD[m[eqlist[ii]+'/'+taga]] is not None and eqlist[ii]+'/'+tagb in m and RD[m[eqlist[ii]+'/'+tagb]] is not None:
                if PD[xtaga] is None or PD[xtagb] is None:
                    avals = np.atleast_2d(RD[m[eqlist[ii]+'/'+taga]])
                    if avals.size <= 1:
                        avals = None
                    atvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+taga]]) if avals is not None else None
                    if avals is not None and atvals is not None and tmlist is not None:
                        atmask = ptools.create_range_mask(atvals, tmlist)
                        if atmask is not None:
                            avals = avals[atmask]
                    aerrs = None
                    (xtemp, xebtemp, stdmean, nr) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                    #vtemp = RD["X_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                    bvals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tagb]])
                    if bvals.size <= 1:
                        bvals = None
                    btvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tagb]]) if bvals is not None else None
                    if bvals is not None and btvals is not None and tmlist is not None:
                        btmask = ptools.create_range_mask(btvals, tmlist)
                        if btmask is not None:
                            bvals = bvals[btmask]
                    berrs = None
                    (vtemp, vebtemp, stdmean, nr) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                    if xtemp is not None and vtemp is not None:
                        vfunc = interp1d(vtemp, xtemp, kind='linear', bounds_error=False, fill_value='extrapolate')
                        vutemp = np.abs(vfunc(xtemp + xebtemp) - vtemp)
                        vltemp = np.abs(vfunc(xtemp - xebtemp) - vtemp)
                        PD[xtagb] = copy.deepcopy(xtemp)
                        PD[vtagb] = copy.deepcopy(vtemp)
                        PD[vtagb+"EB"] = np.sqrt(np.power(vebtemp, 2.0) + np.power(np.nanmax(np.vstack((vutemp, vltemp)), axis=0), 2.0))
                        PD[vtagb+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]] + ';' + RD["I_"+m[eqlist[ii]+'/'+tagb]]
                    if PD[xtagb] is not None and PD[xtagb][0] > PD[xtagb][-1]:
                        PD[xtagb] = PD[xtagb][::-1]
                        PD[vtagb] = PD[vtagb][::-1]
                        PD[vtagb+"EB"] = PD[vtagb+"EB"][::-1]
                    if PD[vtagb] is not None and PD[vtagb][0] > PD[vtagb][-1]:
                        PD[vtagb] = PD[vtagb][::-1]
                        PD[vtagb+"EB"] = PD[vtagb+"EB"][::-1]
                    if PD[xtagb] is not None and PD[vtagb] is not None:
                        PD[xtaga] = copy.deepcopy(PD[xtagb])
                        PD[vtaga] = 2.0 * PD[vtagb][0] - PD[vtagb]
                        PD[vtaga+"EB"] = copy.deepcopy(PD[vtagb+"EB"])
                        PD[vtaga+"DS"] = copy.deepcopy(PD[vtagb+"DS"])
                if len(eqlist) > 1:
                    eqtag = eqlist[ii].upper()
                    if eqtag in PD and (PD[eqtag][xtaga] is None or PD[eqtag][xtagb] is None):
                        avals = np.atleast_2d(RD[m[eqlist[ii]+'/'+taga]])
                        if avals.size <= 1:
                            avals = None
                        atvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+taga]]) if avals is not None else None
                        if avals is not None and atvals is not None and tmlist is not None:
                            atmask = ptools.create_range_mask(atvals, tmlist)
                            if atmask is not None:
                                avals = avals[atmask]
                        aerrs = None
                        (xtemp, xebtemp, stdmean, nr) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
                        #vtemp = RD["X_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                        bvals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tagb]])
                        if bvals.size <= 1:
                            bvals = None
                        btvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tagb]]) if bvals is not None else None
                        if bvals is not None and btvals is not None and tmlist is not None:
                            btmask = ptools.create_range_mask(btvals, tmlist)
                            if btmask is not None:
                                bvals = bvals[btmask]
                        berrs = None
                        (vtemp, vebtemp, stdmean, nr) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
                        if xtemp is not None and vtemp is not None:
                            vfunc = interp1d(vtemp, xtemp, kind='linear', bounds_error=False, fill_value='extrapolate')
                            vutemp = np.abs(vfunc(xtemp + xebtemp) - vtemp)
                            vltemp = np.abs(vfunc(xtemp - xebtemp) - vtemp)
                            PD[eqtag][xtagb] = copy.deepcopy(xtemp)
                            PD[eqtag][vtagb] = copy.deepcopy(vtemp)
                            PD[eqtag][vtagb+"EB"] = np.sqrt(np.power(vebtemp, 2.0) + np.power(np.nanmax(np.vstack((vutemp, vltemp)), axis=0), 2.0))
                            PD[eqtag][vtagb+"DS"] = RD["I_"+m[eqlist[ii]+'/'+taga]] + ';' + RD["I_"+m[eqlist[ii]+'/'+tagb]]
                        if PD[eqtag][xtagb] is not None and PD[eqtag][xtagb][0] > PD[eqtag][xtagb][-1]:
                            PD[eqtag][xtagb] = PD[eqtag][xtagb][::-1]
                            PD[eqtag][vtagb] = PD[eqtag][vtagb][::-1]
                            PD[eqtag][vtagb+"EB"] = PD[eqtag][vtagb+"EB"][::-1]
                        if PD[eqtag][vtagb] is not None and PD[eqtag][vtagb][0] > PD[eqtag][vtagb][-1]:
                            PD[eqtag][vtagb] = PD[eqtag][vtagb][::-1]
                            PD[eqtag][vtagb+"EB"] = PD[eqtag][vtagb+"EB"][::-1]
                        if PD[eqtag][xtagb] is not None and PD[eqtag][vtagb] is not None:
                            PD[eqtag][xtaga] = copy.deepcopy(PD[eqtag][xtagb])
                            PD[eqtag][vtaga] = 2.0 * PD[eqtag][vtagb][0] - PD[eqtag][vtagb]
                            PD[eqtag][vtaga+"EB"] = copy.deepcopy(PD[eqtag][vtagb+"EB"])
                            PD[eqtag][vtaga+"DS"] = copy.deepcopy(PD[eqtag][vtagb+"DS"])

        # Calculate toroidal field as a function of psi from F function - not sure if this is still used anywhere
        if not ("BTAX" in PD and PD["BTAX"] is not None) and PD["FPSI"] is not None and PD["FPSIPFN"] is not None and PD["RMJO"] is not None and PD["PFN2RMO"] is not None:
            ifunc = interp1d(PD["PFN2RMO"], PD["RMJO"], bounds_error=False, fill_value='extrapolate')
            PD["RBT"] = ifunc(PD["FPSIPFN"])
            PD["BTAX"] = PD["FPSI"] / PD["RBT"]
            PD["BTAXDS"] = "EX2GK: Internal calculation"

        if fdebug and (PD["RMJO"] is not None and PD["RMJI"] is not None):
            print("process_jet.py: unpack_coord_data(): Midplane major radius coordinate added.")

        # Toroidal flux profile, used to define toroidal flux corrdinates
        # TODO: Toroidal flux now no longer multiplied by 2*pi to conform to COCOS=1, need to ensure usage further down the pipeline does not require 2*pi factor
        tag = 'ftor'
        vtag = "TFX"         # Toroidal magnetic flux(PSI_norm)
        xtag = "PFN2TFX"
        PD[vtag] = None
        PD[xtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                if vals is not None and vals.size <= 1:
                    vals = None
                tvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tag]]) if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                verrs = None
                (PD[vtag], PD[vtag+"EB"], stdmean, nf) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                # Convert JET PPF into COCOS 1 convention
                if PD[vtag] is not None:
                    PD[vtag] = PD[vtag] / (2.0 * np.pi)
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if vals is not None else None
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                vals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                if vals is not None and vals.size <= 1:
                    vals = None
                tvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tag]]) if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                verrs = None
                (PD[eqtag][vtag], PD[eqtag][vtag+"EB"], stdmean, nf) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                # Convert JET PPF into COCOS 1 convention
                if PD[eqtag][vtag] is not None:
                    PD[eqtag][vtag] = PD[eqtag][vtag] / (2.0 * np.pi)
                PD[eqtag][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]]
                PD[eqtag][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
        # If toroidal flux profile is unavailable, uses poloidal flux profile in combination with safety factor profile to calculate it
        if PD["TFX"] is None and eqdda is not None and PD["FBND"] is not None and PD["FAXS"] is not None:
            qval = PD["Q"+eqdda].copy()
            qerr = PD["QEB"+eqdda].copy()
            qpfn = PD["QPFN"+eqdda].copy()
            pfntemp = qpfn.copy()
            pfntemp[pfntemp < 0.0] = 0.0
            tfxtemp = np.zeros(pfntemp.shape)
            fbnd = float(PD["FBND"][0])
            faxs = float(PD["FAXS"][0])
            psitemp = qpfn * (fbnd - faxs) + faxs
            logq = np.log(qval + logfitpar)
            pqfunc = interp1d(psitemp, logq, kind='cubic', bounds_error=False, fill_value='extrapolate')
            testpsi = np.linspace(faxs, fbnd, 1001)
            qtemp = np.exp(pqfunc(testpsi)) - logfitpar
            for pp in range(1, pfntemp.size):
                idxs = np.where(testpsi >= psitemp[pp])[0] if fbnd >= faxs else np.where(testpsi <= psitemp[pp])[0]
                jj = idxs[0]+1 if idxs.size > 0 and idxs[0]+1 < testpsi.size else None
                tfxtemp[pp] = np.trapz(qtemp[:jj], testpsi[:jj]) if jj is not None else np.trapz(qtemp, testpsi)
            PD[vtag] = tfxtemp.copy()
            PD[xtag] = pfntemp.copy()
            PD[vtag+"DS"] = "EX2GK: Internal calculation"
        if len(eqlist) > 1:
            for ii in range(len(eqlist)):
                eqtag = eqlist[ii].upper()
                if PD[eqtag]["TFX"] is None and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    qval = PD[eqtag]["Q"].copy()
                    qerr = PD[eqtag]["QEB"].copy()
                    qpfn = PD[eqtag]["QPFN"].copy()
                    pfntemp = qpfn.copy()
                    pfntemp[pfntemp < 0.0] = 0.0
                    tfxtemp = np.zeros(pfntemp.shape)
                    fbnd = float(PD[eqtag]["FBND"][0])
                    faxs = float(PD[eqtag]["FAXS"][0])
                    psitemp = qpfn * (fbnd - faxs) + faxs
                    logq = np.log(qval + logfitpar)
                    pqfunc = interp1d(psitemp, logq, kind='cubic', bounds_error=False, fill_value='extrapolate')
                    testpsi = np.linspace(faxs, fbnd, 1001)
                    qtemp = np.exp(pqfunc(testpsi)) - logfitpar
                    for pp in range(1, pfntemp.size):
                        idxs = np.where(testpsi >= psitemp[pp])[0] if fbnd >= faxs else np.where(testpsi <= psitemp[pp])[0]
                        jj = idxs[0]+1 if idxs.size > 0 and idxs[0]+1 < testpsi.size else None
                        tfxtemp[pp] = np.trapz(qtemp[:jj], testpsi[:jj]) if jj is not None else np.trapz(qtemp, testpsi)
                    PD[eqtag][vtag] = tfxtemp.copy()
                    PD[eqtag][xtag] = pfntemp.copy()
                    PD[eqtag][vtag+"DS"] = "EX2GK: Internal calculation"

        if fdebug and PD["TFX"] is not None:
            print("process_jet.py: unpack_coord_data(): Toroidal flux coordinate added.")

        # Toroidal magnetic flux at LCFS, used to define the normalization of phi - averaging expected to be done during extraction (really should be handled here)
        btag = "TBND"        # Toroidal magnetic flux at plasma boundary
        tidx = np.where(np.abs(PD["PFN2TFX"] - 1.0) < 1.0e-6)[0][0] if PD["PFN2TFX"] is not None else 0
        PD[btag] = np.array([PD["TFX"][tidx]]) if PD["TFX"] is not None else None
        PD[btag+"DS"] = "EX2GK: Internal calculation"
        if len(eqlist) > 1:
            for ii in range(len(eqlist)):
                tidx = np.where(np.abs(PD[eqtag]["PFN2TFX"] - 1.0) < 1.0e-6)[0][0] if PD[eqtag]["PFN2TFX"] is not None else 0
                PD[eqtag][btag] = np.array([PD[eqtag]["TFX"][tidx]]) if PD[eqtag]["TFX"] is not None else None
                PD[eqtag][btag+"DS"] = "EX2GK: Internal calculation"

        # Flux surface volume as a function of normalized poloidal flux
        tag = 'vjac'
        vtag = "DVOL"        # Volume Jacobian(PSI_norm)
        xtag = "PDVOL"
        itag = "VOL"         # Flux surface volume(PSI_norm)
        PD[vtag] = None
        PD[xtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                if vals is not None and vals.size <= 1:
                    vals = None
                tvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tag]]) if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                verrs = None
                (PD[vtag], PD[vtag+"EB"], stdmean, nf) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                vals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                if vals is not None and vals.size <= 1:
                    vals = None
                tvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tag]]) if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                verrs = None
                (PD[eqtag][vtag], PD[eqtag][vtag+"EB"], stdmean, nf) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                PD[eqtag][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqtag][vtag] is not None else None
                PD[eqtag][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
        PD[itag] = cumtrapz(PD[vtag], PD[xtag], initial=0.0) if PD[vtag] is not None else None
        PD[itag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
        if PD[vtag] is not None and vtag+"EB" in PD and PD[vtag+"EB"] is not None:
            PD[itag+"EB"] = np.sqrt(cumtrapz(np.power(PD[vtag+"EB"], 2.0), PD[xtag], initial=0.0))
        if len(eqlist) > 1:
            for ii in range(len(eqlist)):
                eqtag = eqlist[ii].upper()
                PD[eqtag][itag] = cumtrapz(PD[eqtag][vtag], PD[eqtag][xtag], initial=0.0) if PD[eqtag][vtag] is not None else None
                PD[eqtag][itag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
                if PD[vtag] is not None and vtag+"EB" in PD and PD[vtag+"EB"] is not None:
                    PD[itag+"EB"] = np.sqrt(cumtrapz(np.power(PD[vtag+"EB"], 2.0), PD[xtag], initial=0.0))

        if fdebug and PD["VOL"] is not None:
            print("process_jet.py: unpack_coord_data(): Flux surface volume coordinate added.")
        if fdebug and PD["DVOL"] is not None:
            print("process_jet.py: unpack_coord_data(): Flux surface volume derivative added.")

        # Flux surface cross-sectional area as a function of normalized poloidal flux
        tag = 'ajac'
        vtag = "DAREA"       # Area Jacobian(PSI_norm)
        xtag = "PDAREA"
        itag = "AREA"        # Flux surface cross-sectional area(PSI_norm)
        PD[vtag] = None
        PD[xtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                vals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                if vals is not None and vals.size <= 1:
                    vals = None
                tvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tag]]) if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                verrs = None
                (PD[vtag], PD[vtag+"EB"], stdmean, nf) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                vals = np.atleast_2d(RD[m[eqlist[ii]+'/'+tag]])
                if vals is not None and vals.size <= 1:
                    vals = None
                tvals = np.atleast_2d(RD["T_"+m[eqlist[ii]+'/'+tag]]) if vals is not None else None
                if vals is not None and tvals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(tvals, tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                verrs = None
                (PD[eqtag][vtag], PD[eqtag][vtag+"EB"], stdmean, nf) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                PD[eqtag][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqtag][vtag] is not None else None
                PD[eqtag][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
        PD[itag] = cumtrapz(PD[vtag], PD[xtag], initial=0.0) if PD[vtag] is not None else None
        PD[itag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
        if PD[vtag] is not None and vtag+"EB" in PD and PD[vtag+"EB"] is not None:
            PD[itag+"EB"] = np.sqrt(cumtrapz(np.power(PD[vtag+"EB"], 2.0), PD[xtag], initial=0.0))
        if len(eqlist) > 1:
            for ii in range(len(eqlist)):
                eqtag = eqlist[ii].upper()
                PD[eqtag][itag] = cumtrapz(PD[eqtag][vtag], PD[eqtag][xtag], initial=0.0) if PD[eqtag][vtag] is not None else None
                PD[eqtag][itag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
                if PD[vtag] is not None and vtag+"EB" in PD and PD[vtag+"EB"] is not None:
                    PD[itag+"EB"] = np.sqrt(cumtrapz(np.power(PD[vtag+"EB"], 2.0), PD[xtag], initial=0.0))

        if fdebug and PD["AREA"] is not None:
            print("process_jet.py: unpack_coord_data(): Flux surface cross-sectional area coordinate added.")
        if fdebug and PD["DAREA"] is not None:
            print("process_jet.py: unpack_coord_data(): Flux surface cross-sectional area derivative added.")

        # The extractions below are repeated to get the inter-shot EFIT equilibrium (magnetics only)
        #    This is needed to apply equilibrium corrections to field-dependent diagnostics, only the necessary fields are kept
        mtag = 'efit'

        # Safety factor profile
        if eqlist[0] != mtag:
            eqtag = mtag.upper()
            vals = RD[m[mtag+'/q']] if mtag+'/q' in m else None
            tvals = RD["T_"+m[mtag+'/q']] if vals is not None else None
            if vals is not None and tvals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(tvals, tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            errs = None
            (PD["Q"+eqtag], PD["QEB"+eqtag], PD["QEM"+eqtag], navgmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
            PD["NQ"+eqtag] = np.count_nonzero(navgmap, axis=0) if PD["Q"+eqtag] is not None else None
            PD["QPFN"+eqtag] = RD["X_"+m[mtag+'/q']] if vals is not None else None
            if mtag+'/q' in m and PD["Q"+eqtag] is not None:
                PD["Q"+eqtag+"DS"] = RD["I_"+m[mtag+'/q']]
        eq_npoints = int(PD["QEFIT"].size) if "QEFIT" in PD and PD["QEFIT"] is not None else 33     # 33 is default inter-shot EFIT setting in JET
        rvec_default = np.linspace(1.65, 4.05, eq_npoints)
        zvec_default = np.linspace(-1.90, 2.15, eq_npoints)

        # Radial coordinate of magnetic axis - averaging expected to be done during extraction
        tag = 'rmag'
        vtag = "MRMAG"        # R coord of magnetic axis
        PD[vtag] = copy.deepcopy(PD["RMAG"]) if eqlist[0] == mtag else None
        if PD[vtag] is not None:
            PD[vtag+"DS"] = PD["RMAGDS"]
        if PD[vtag] is None and mtag+'/'+tag in m:
            PD[vtag] = RD[m[mtag+'/'+tag]]
            PD[vtag+"DS"] = RD["I_"+m[mtag+'/'+tag]]

        # Vertical coordinate of magnetic axis - averaging expected to be done during extraction
        tag = 'zmag'
        vtag = "MZMAG"        # Z coord of magnetic axis
        PD[vtag] = copy.deepcopy(PD["ZMAG"]) if eqlist[0] == mtag else None
        if PD[vtag] is not None:
            PD[vtag+"DS"] = PD["ZMAGDS"]
        if PD[vtag] is None and mtag+'/'+tag in m:
            PD[vtag] = RD[m[mtag+'/'+tag]]
            PD[vtag+"DS"] = RD["I_"+m[mtag+'/'+tag]]

        # Polodial flux at magnetic axis and LCFS, used to undo normalization
        taga = 'fbnd'
        tagb = 'faxs'
        vtaga = "MFBND"       # PSI at plasma boundary
        vtagb = "MFAXS"       # PSI at magnetic axis
        PD[vtaga] = copy.deepcopy(PD["FBND"]) if eqlist[0] == mtag else None
        PD[vtagb] = copy.deepcopy(PD["FAXS"]) if eqlist[0] == mtag else None
        if PD[vtaga] is None or PD[vtagb] is None:
            avals = RD[m[mtag+'/'+taga]].flatten() if mtag+'/'+taga in m and RD[m[mtag+'/'+taga]] is not None else None
            atvals = RD["T_"+m[mtag+'/'+taga]] if avals is not None else None
            if avals is not None and atvals is not None and tmlist is not None:
                atmask = ptools.create_range_mask(atvals, tmlist)
                if atmask is not None:
                    avals = avals[atmask]
            aerrs = None
            (PD[vtaga], avgeb, stdmean, nf) = ptools.filtered_average(avals, value_errors=aerrs, use_n=True)
            if mtag+'/'+taga in m and PD[vtaga] is not None:
                PD[vtaga+"DS"] = RD["I_"+m[mtag+'/'+taga]]
            bvals = RD[m[mtag+'/'+tagb]].flatten() if mtag+'/'+tagb in m and RD[m[mtag+'/'+taga]] is not None else None
            btvals = RD["T_"+m[mtag+'/'+tagb]] if bvals is not None else None
            if bvals is not None and btvals is not None and tmlist is not None:
                btmask = ptools.create_range_mask(btvals, tmlist)
                if btmask is not None:
                    bvals = bvals[btmask]
            berrs = None
            (PD[vtagb], avgeb, stdmean, nf) = ptools.filtered_average(bvals, value_errors=berrs, use_n=True)
            if mtag+'/'+tagb in m and PD[vtagb] is not None:
                PD[vtagb+"DS"] = RD["I_"+m[mtag+'/'+tagb]]
            # Convert JET PPF into COCOS 1 convention
            if PD[vtaga] is not None:
                PD[vtaga] = -PD[vtaga]
            if PD[vtagb] is not None:
                PD[vtagb] = -PD[vtagb]

        # Poloidal flux as a function of R coord and Z coord, normalized if data is available - averaging expected to be done during extraction (really should be handled here)
        tag = 'psi'
        taga = 'psir'
        tagb = 'psiz'
        tagc = 'sspr'
        tagd = 'sspi'
        vtag = "MPFXMAP"
        rtag = "MRMAP"
        ztag = "MZMAP"
        PD[vtag] = copy.deepcopy(PD["PFXMAP"]) if eqlist[0] == mtag else None
        PD[rtag] = copy.deepcopy(PD["RMAP"]) if eqlist[0] == mtag else None
        PD[ztag] = copy.deepcopy(PD["ZMAP"]) if eqlist[0] == mtag else None
        # This branch is used if full 2D psi map is found in EFIT DDA - reshapes flattened vector
        if PD[vtag] is None or PD[rtag] is None or PD[ztag] is None:
            if mtag+'/'+tag in m and RD[m[mtag+'/'+tag]] is not None:
                PD[vtag] = RD[m[mtag+'/'+tag]]
                prov = RD["I_"+m[mtag+'/'+tag]]
                if mtag+'/'+taga in m and RD[m[mtag+'/'+taga]] is not None and mtag+'/'+tagb in m and RD[m[mtag+'/'+tagb]] is not None:
                    rvec = RD[m[mtag+'/'+taga]]
                    zvec = RD[m[mtag+'/'+tagb]]
                    if rvec is not None and zvec is not None:
                        (PD[rtag], PD[ztag]) = np.meshgrid(rvec.flatten(), zvec.flatten())
                        PD[vtag] = np.reshape(PD[vtag], PD[rtag].shape)
                        prov = prov + "; " + RD["I_"+m[mtag+'/'+taga]] + "; " + RD["I_"+m[mtag+'/'+tagb]]
                else:
                    (PD[rtag], PD[ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                    PD[vtag] = np.reshape(PD[vtag], PD[rtag].shape)
                # Convert JET PPF into COCOS 1 convention only if grid is not already normalized
                if np.nanmin(PD[vtag]) != 0.0 and np.nanmax(PD[vtag]) != 0.0:
                    PD[vtag] = -PD[vtag]
                PD[vtag+"DS"] = prov
        # Otherwise, this branch is used to reconstruct 2D psi map from 2D spline coefficients - the format used when disk space was precious
        if PD[vtag] is None:
            if mtag+'/'+tagc in m and RD[m[mtag+'/'+tagc]] is not None and RD[m[mtag+'/'+tagc]].ndim > 1 and mtag+'/'+tagd in m and RD[m[mtag+'/'+tagd]] is not None:
                vmaps = None
                nmaps = None
                prov = RD["I_"+m[mtag+'/'+tagc]] + "; " + RD["I_"+m[mtag+'/'+tagd]]
                for jj in range(RD[m[mtag+'/'+tagc]].shape[0]):
                    nrknots = int(RD[m[mtag+'/'+tagd]][jj, 0])
                    nzknots = int(RD[m[mtag+'/'+tagd]][jj, 1])
                    ncoeffs = int(RD[m[mtag+'/'+tagd]][jj, 2])
                    rknots = RD[m[mtag+'/'+tagc]][jj, :nrknots]
                    zknots = RD[m[mtag+'/'+tagc]][jj, nrknots:nrknots+nzknots]
                    scoeffs = RD[m[mtag+'/'+tagc]][jj, nrknots+nzknots:nrknots+nzknots+ncoeffs]
                    rdeg = 0
                    while int(rknots[rdeg+1]) == int(rknots[0]):
                        rdeg = rdeg + 1
                    zdeg = 0
                    while int(zknots[zdeg+1]) == int(zknots[0]):
                        zdeg = zdeg + 1
                    rvec = np.linspace(np.nanmin(rknots), np.nanmax(rknots), eq_npoints)
                    zvec = np.linspace(np.nanmin(zknots), np.nanmax(zknots), eq_npoints)
                    spvals = [rknots.tolist(), zknots.tolist(), scoeffs.tolist(), rdeg, zdeg]
                    temppsn = bisplev(rvec, zvec, spvals)
#                    if re.match(r'^eftm$', eqlist[ii], flags=re.IGNORECASE):
#                        nmaps = np.dstack((nmaps, temppsn)) if nmaps is not None else copy.deepcopy(temppsn)
#                    else:
                    nmaps = np.dstack((nmaps, temppsn.T)) if nmaps is not None else copy.deepcopy(temppsn.T)
                    if mtag+'/faxs' in m and RD[m[mtag+'/faxs']] is not None and mtag+'/fbnd' in m and RD[m[mtag+'/fbnd']] is not None:
                        fbnd = RD[m[mtag+'/fbnd']][jj]
                        faxs = RD[m[mtag+'/faxs']][jj]
                        # Convert JET PPF into COCOS 1 convention only if grid is not already normalized
                        if fbnd > faxs and float(faxs) != 0.0:
                            fbnd = -fbnd
                            faxs = -faxs
                        temppsi = temppsn * (fbnd - faxs) + faxs
                        vmaps = np.dstack((vmaps, temppsi.T)) if vmaps is not None else copy.deepcopy(temppsi.T)
                if vmaps is not None:
                    PD[vtag] = np.mean(vmaps, axis=2) if vmaps.ndim > 2 else copy.deepcopy(vmaps)
                    (PD[rtag], PD[ztag]) = np.meshgrid(rvec_default.flatten(), zvec_default.flatten())
                    PD[vtag+"DS"] = prov

        # Flux function, F, as functions of PSI_norm - averaging expected to be done during extraction (really should be handled here)
        taga = 'f'
        vtag = "MFPSI"
        vtagx = "MFPSIPFN"
        PD[vtag] = copy.deepcopy(PD["FPSI"]) if eqlist[0] == mtag else None
        PD[vtagx] = copy.deepcopy(PD["FPSIPFN"]) if eqlist[0] == mtag else None
        if PD[vtag] is not None:
            PD[vtag+"DS"] = PD["FPSIDS"]
        if PD[vtag] is None and mtag+'/'+taga in m and mtag+'/'+tagb in m:
            PD[vtag] = RD[m[mtag+'/'+taga]]
            PD[vtagx] = RD["X_"+m[mtag+'/'+taga]]
            PD[vtag+"DS"] = RD["I_"+m[mtag+'/'+taga]]

        if fdebug and PD["MPFXMAP"] is not None:
            print("process_jet.py: unpack_coord_data(): Inter-shot equilibrium information added.")

    if fdebug:
        print("process_jet.py: unpack_coord_data() completed.")

    return PD


def unpack_advanced_geometry_data(rawdata, newstruct=None, fdebug=None):
    """
    JET-SPECIFIC FUNCTION
    Extracts advanced equilibrium data, if provided.

    .. note:
        This feature is not yet fully implemented! DO NOT USE!

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object containing advanced geometrical data on magnetic equilibrium.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None
        fullflag = True if "SELECTEQ" in RD and re.match(r'^all$', RD["SELECTEQ"], flags=re.IGNORECASE) else False

        # Advanced geometric fields are only provided for EFIT, not for other constrained equilibria?
        eqlist = ['efit']         # Implementation leaves architecture for additional equilibria for future development

        # Specification of time ranges to mask for ELM filtering
        tmlist = PD["ELMMASK"] if "ELMMASK" in PD else None

        # Flux function, F, used in Grad-Shafranov solution
        if 'equi/f' in m:
            PD["FS_F"] = RD[m['equi/f']]

        # Pressure profile, used in Grad-Shafranov solution
        if 'equi/p' in m:
            PD["FS_P"] = RD[m['equi/p']]

        # Flux-surface averaged B_pol
        if 'equi/bpol' in m:
            PD["FS_BPOL"] = RD[m['equi/bpol']]

        # Flux-surface averaged B_pol squared
        if 'equi/bpo2' in m:
            PD["FS_BPOL2"] = RD[m['equi/bpo2']]

        # Equilibrium magnetic label, sqrt(psi)
        if 'equi/rho' in m:
            PD["FS_RHOPN"] = RD[m['equi/grho']]
            PD["FS_PFN"] = np.power(PD["FS_RHOPN"], 2.0)

        # Flux-surface averaged gradient sqrt(psi)
        if 'equi/grho' in m:
            PD["FS_GRHOPN"] = RD[m['equi/grho']]        # Can be re-written as grad(psi) / sqrt(psi)

        # Flux-surface averaged gradient sqrt(psi) squared
        if 'equi/gro2' in m:
            PD["FS_GRHOPN2"] = RD[m['equi/grho']]       # Can be re-written as grad(psi)^2 / psi

        # Flux surface averaged 1/R
        if 'equi/riav' in m:
            PD["FS_IR"] = RD[m['equi/riav']]

        # Flux surface averaged 1/R^2
        if 'equi/r2iv' in m:
            PD["FS_IR2"] = RD[m['equi/r2iv']]

        # Flux surface averaged j_phi / R, result from Grad-Shafranov solution
        if "FS_PFN" in PD and PD["FS_PFN"] is not None:
            # Flux function, F * F' / mu0, used in Grad-Shafranov solution
            tag = 'dfdp'          # F * F' / mu0
            eqfp = None
            eqpfn = None
            for ii in range(len(eqlist)):
                if eqlist[ii]+'/'+tag in m and eqfp is None:
                    eqfp = RD[m[eqlist[ii]+'/'+tag]]
                    eqpfn = RD["X_"+m[eqlist[ii]+'/'+tag]]
#                if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
#                    PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]

            # Pressure gradient wrt. psi, used in Grad-Shafranov solution
            tag = 'dpdp'          # p'
            eqpp = None
            for ii in range(len(eqlist)):
                if eqlist[ii]+'/'+tag in m and eqpp is None:
                    eqpp = RD[m[eqlist[ii]+'/'+tag]]
#                if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
#                    PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]

            if eqpfn is not None and eqfp is not None and eqpp is not None:
                ppfunc = interp1d(eqpfn, eqpp, bounds_error=False, fill_value='extrapolate')
                fpfunc = interp1d(eqpfn, eqfp, bounds_error=False, fill_value='extrapolate')
                PD["FS_JPHI"] = ppfunc(PD["FS_PFN"]) + PD["FS_IR2"] * fpfunc(PD["FS_PFN"])

    if fdebug:
        print("process_jet.py: unpack_advanced_geometry_data() completed.")

    return PD


def find_midplane_radius(procdata, pfnvec, outer_first=False, cdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Converts normalized poloidal flux coordinate back to the major radius
    at the height of the magnetic axis. Requires unified coordinate system
    to already be present in data and assumes that the data is ordered
    starting from the high-field side to the low-field side.

    :arg procdata: dict. Implementation-specific object containing unified coordinate system data.

    :arg pfnvec: array. Vector of radial points in the normalized poloidal flux coordinate.

    :kwarg outer_first: bool. Flag to indicate that the input vector is ordered from low-field side to high-field side.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :returns: array. Associated vector of radial points in the midplane major radius coordinate.
    """
    PD = None
    dvec = None
    if isinstance(procdata, dict):
        PD = procdata
    if isinstance(pfnvec, (list, tuple)):
        dvec = np.array(pfnvec)
    elif isinstance(pfnvec, np.ndarray):
        dvec = pfnvec.copy()

    ovec = None
    if PD is not None and dvec is not None and "CS_POLFLUXN" in PD and PD["CS_POLFLUXN"] is not None and "CS_RMAJORO" in PD and PD["CS_RMAJORO"] is not None:
        pidx = dvec.size if outer_first else 0
        if dvec.size > 1:
            pswap = np.where(np.all([dvec[:-1] * dvec[1:] < 0.0, np.abs(dvec[:-1]) < 0.3, np.abs(dvec[1:]) < 0.3], axis=0))[0]
            if len(pswap) == 0:
                pdiff = np.diff(dvec)
                pswap = np.where(np.all([pdiff[:-1] * pdiff[1:] < 0.0, np.abs(dvec[1:-1]) < 0.3], axis=0))[0]
            pidx = pswap[0] + 1 if len(pswap) > 0 else pidx
        testvec = np.arange(0, dvec.size)
        ofhfs = (testvec < pidx)
        if outer_first:
            ofhfs = np.invert(ofhfs)
        ovec = np.full(dvec.shape, np.NaN)
        if not np.all(ofhfs):
            (rmajo, dummyj, dummye) = jptools.convert_coords(PD, np.abs(dvec[np.invert(ofhfs)]), "POLFLUXN", "RMAJORO", prein="", preout="", cdebug=cdebug)
            ovec[np.invert(ofhfs)] = rmajo
        if np.any(ofhfs):
            (rmaji, dummyj, dummye) = jptools.convert_coords(PD, np.abs(dvec[ofhfs]), "POLFLUXN", "RMAJORI", prein="", preout="", cdebug=cdebug)
            ovec[ofhfs] = rmaji

    return ovec


def unpack_profile_data(rawdata, newstruct=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Unpacks the required plasma profile data from the raw extracted data
    container, pre-processes it with statistical averaging if necessary,
    and stores it in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with processed plasma profile data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None
        ftflag = False if "SHOTPHASE" in RD and RD["SHOTPHASE"] > 0 else True
        pureflag = PD["PURERAWFLAG"] if "PURERAWFLAG" in PD else False
        lmflag = PD["USELMODEFLAG"] if "USELMODEFLAG" in PD else False

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['efit']
        eqdda = eqlist[0].upper() if len(eqlist) > 0 else None
        PD["ORMAJEDGE"] = np.array([PD["CS_RMAJORO"][-1]]).flatten() if "CS_RMAJORO" in PD else np.array([3.85])
        PD["RMAJEDGE"] = None

        # Specification of time ranges to mask for ELM filtering
        tmlist = PD["ELMMASK"] if "ELMMASK" in PD else None

        if fdebug:
            print("process_jet.py: unpack_profile_data(): Setup completed.")

        # Reflectometer system at JET - electron density (not produced if B0 < 1.9 T)
        use_kg10 = PD["FORCEREFLFLAG"] if "FORCEREFLFLAG" in PD else False
        KG10 = None
        if 'kg10/ne' in m and RD[m['kg10/ne']] is not None:
            if KG10 is None:
                KG10 = dict()
            vals = RD[m['kg10/ne']]                                    # 10% err. - assumed to be 2 sigma
            prov = RD["I_"+m['kg10/ne']] + "; " + RD["I_"+m['kg10/r']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['kg10/ne']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (KG10["NE"], KG10["NEEB"], KG10["NEEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=1.0e10, use_n=True)
            KG10["RMNE"] = RD[m['kg10/r']].copy()
            navgmap = np.atleast_2d(navgmap)
            KG10["NNE"] = np.count_nonzero(navgmap, axis=0).flatten()
            if 'kg10/z' in m and RD[m['kg10/z']] is not None:
                KG10["RNE"] = RD[m['kg10/r']].flatten()
                KG10["ZNE"] = RD[m['kg10/z']].flatten()
            if "USERZFLAG" in PD and PD["USERZFLAG"] and "RNE" in KG10 and "ZNE" in KG10 and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], KG10["RNE"], KG10["ZNE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                outerflag = True if KG10["RNE"][0] > KG10["RNE"][-1] else False
                KG10["RMNE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                prov = prov + "; " + RD["I_"+m['kg10/z']]
            KG10["NEDS"] = prov
        if use_kg10:
            PD["KG10"] = KG10

        if fdebug and "KG10" in PD and PD["KG10"] is not None:
            print("process_jet.py: unpack_profile_data(): KG10 reflectometry data added.")

        # Old reflectometer system at JET - electron density
        use_refl = False
        REFL = None
        if 'refl/nepr' in m and RD[m['refl/nepr']] is not None:
            if REFL is None:
                REFL = dict()
            vals = RD[m['refl/nepr']]                                    # 10% err. - assumed to be 2 sigma
            prov = RD["I_"+m['refl/nepr']] + "; " + RD["I_"+m['refl/xvec']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['refl/nepr']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (REFL["NE"], REFL["NEEB"], REFL["NEEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=1.0e10, use_n=True)
            REFL["RMNE"] = RD[m['refl/xvec']].copy()
            navgmap = np.atleast_2d(navgmap)
            REFL["NNE"] = np.count_nonzero(navgmap, axis=0).flatten()
#            if 'refl/xvec' in m and RD[m['refl/xvec']] is not None:
#                REFL["RNE"] = RD[m['refl/xvec']].flatten()
#                REFL["ZNE"] = RD[m['refl/xvec']].flatten()
#            if "USERZFLAG" in PD and PD["USERZFLAG"] and "RNE" in REFL and "ZNE" in REFL and "PFXMAP" in PD and PD["PFXMAP"] is not None:
#                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], REFL["RNE"], REFL["ZNE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
#                outerflag = True if REFL["RNE"][0] > REFL["RNE"][-1] else False
#                REFL["RMNE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
            REFL["NEDS"] = prov
        if use_refl:
            PD["REFL"] = REFL

        if fdebug and "REFL" in PD and PD["REFL"] is not None:
            print("process_jet.py: unpack_profile_data(): REFL reflectometry data added.")

        # Pre-initialize values for density and temperature pedestal functions
        tepedfunctype = 0
        tepedfunc = None
        tepedopt = None
        teyscale = None
        nepedfunctype = 0
        nepedfunc = None
        nepedopt = None
        neyscale = None
        nepedtop = 0.0
        necoreslp = None

        # Interferometry line-averaged density measurements
        nelineavg = None
        if 'kg1v/lid3' in m and RD[m['kg1v/lid3']] is not None:
            (nelineavg, neleb, nelem, navgmap) = ptools.filtered_average(RD[m['kg1v/lid3']])
            nelineavg = float(nelineavg[0])
            llos = 2.25     # Suspected active length of line-of-sight
            PD["NEBAR"] = np.array([nelineavg / llos])
            PD["NEBARDS"] = RD["I_"+m['kg1v/lid3']]

        # Calculate magnetic field vector, takes advantage of expected 1/R relation in interpolation
        btfunc = None
        if "BTAX" in PD and PD["BTAX"] is not None and "RBT" in PD and PD["RBT"] is not None:
            invbt = 1.0 / PD["BTAX"]
            btfunc = interp1d(PD["RBT"], invbt, bounds_error=False, fill_value='extrapolate')
        bmag = np.abs(1.0 / btfunc(PD["RMAG"])) if btfunc is not None and "RMAG" in PD and PD["RMAG"] is not None else None

        # Attempts to distinguish H-mode vs. L-mode using upper bound of power threshold scaling law (not used, doesn't work very well!!)
        #     Exponents are written as addition of two explicit terms to reflect upper bound values found in Martin et al., Journal of Physics, 123 (2008)
        pthreshold_exceeded = False
        usepthr = False
        if usepthr and ftflag and nelineavg is not None and bmag is not None and "CS_FSAREA" in PD and PD["CS_FSAREA"] is not None:
            baseexp = 1.0 + 0.057
            neavexp = 0.717 + 0.035
            btorexp = 0.717 + 0.035
            surfexp = 0.941 + 0.019
            PD["HMODEPTHR"] = 4.88e4 * np.exp(baseexp + neavexp * np.log(nelineavg * 1.0e-20) + btorexp * np.log(bmag) + surfexp * np.log(PD["CS_FSAREA"][-1]))   # in W
            if "PNET" in PD and PD["PNET"] is not None:
                pthreshold_exceeded = (float(PD["PNET"][0] - PD["PNETEB"][0]) > float(PD["HMODEPTHR"]))
            PD["HMODEPTHRDS"] = "EX2GK: Internal calculation"

        # High resolution Thomson scattering system at JET - electron density and temperature
        HRTS = None
        if 'hrts/te' in m and RD[m['hrts/te']] is not None:
            if HRTS is None:
                HRTS = dict()
            vals = RD[m['hrts/te']]                                    # Electron temp. (R)
            errs = RD[m['hrts/dte']] if 'hrts/dte' in m else None      # 1 sigma (see DDA handbook)
            prov = RD["I_"+m['hrts/te']] + "; " + RD["I_"+m['hrts/dte']] if errs is not None else RD["I_"+m['hrts/te']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['hrts/te']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
                    if errs is not None:
                        errs = errs[tmask]
            (HRTS["TE"], HRTS["TEEB"], HRTS["TEEM"], navgmap) = ptools.filtered_average(vals, value_errors=errs, lower_bounds=1.0e1, use_n=True)
            HRTS["RMTE"] = RD[m['hrts/rmid']].copy()
            navgmap = np.atleast_2d(navgmap)
            HRTS["NTE"] = np.count_nonzero(navgmap, axis=0).flatten()
            if 'hrts/z' in m and RD[m['hrts/z']] is not None:
                HRTS["RTE"] = RD["X_"+m['hrts/te']].copy()
                HRTS["ZTE"] = RD[m['hrts/z']].copy()
            if "USERZFLAG" in PD and PD["USERZFLAG"] and "RTE" in HRTS and "ZTE" in HRTS and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], HRTS["RTE"], HRTS["ZTE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                outerflag = True if HRTS["RTE"][0] > HRTS["RTE"][-1] else False
                HRTS["RMTE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                prov = prov + "; " + RD["I_"+m['hrts/z']]
            # Suspected overestimation in HRTS error, consequence of high-resolution?
#            if HRTS is not None and "TE" in HRTS and not pureflag:
#                HRTS["TEEB"] = 0.75 * HRTS["TEEB"]
            HRTS["TEDS"] = prov

            # Constructing rough edge temperature fit function (tanh for H-mode, quadratic of L-mode) used for estimating necessary shift in major radius
            if HRTS["TE"] is not None:
                # Define low-field-side midplane major radius using largest value in boundary vector from EFIT, use as reference point for pedestal region
                rmax = np.nanmax(PD["RBND"]) if "RBND" in PD and PD["RBND"] is not None else 3.85
                ffilt = np.isfinite(HRTS["TE"])
                xx = copy.deepcopy(HRTS["RMTE"][ffilt])
                yy = copy.deepcopy(HRTS["TE"][ffilt])
                ye = copy.deepcopy(HRTS["TEEB"][ffilt])
                efilt = (xx >= rmax - 0.15)   # Heuristic width sufficient to capture pedestal data at JET
                # Estimate pedestal top temperature as the temperature at point closest to but outside the heuristic pedestal region
                xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
                ymax = yy[xmin[0]] if xmin.size > 0 else 5.0e3
                idxo = np.where(xx >= np.nanmin(xx[efilt]))[0]
                yedge = yy[idxo[0]] if len(idxo) > 0 else 0.0
                if np.any(efilt) and yy[efilt].size > 1:
                    peddata = np.vstack((xx[efilt].flatten(), yy[efilt].flatten()))
                    peddata = peddata[:, np.argsort(peddata[0, :])]
                    bdvec = np.diff(peddata[1, :].flatten()) / np.diff(peddata[0, :].flatten())
                    teyscale = yedge
                    gfilt = np.all([efilt, yy < 1.1 * teyscale, yy > 0.0], axis=0)
                    if np.any(gfilt) and yy[gfilt].size > 3:
                        fxx = xx[gfilt]
                        fyy = yy[gfilt] / teyscale
                        fye = ye[gfilt] / teyscale

                        # Only applies major radius shift if minimum gradient (these are all negative values) and innermost point match criteria
                        #    This criteria allows for auto-rejection of L-mode cases, in which the separatrix temperature criteria is not always suitable
                        if not PD["USELMODEFLAG"] and np.nanmin(bdvec) <= -ymax and yedge > 5.0e2:
                            tepedfunctype = 1
                            tepedfunc = lambda x, scale, loc: 0.5 - 0.5 * np.tanh(scale * (x - loc))
                            if fdebug:
                                print("   Performing tanh fit on HRTS TE with H-mode settings...")
                            # Try block used to preserve backwards compatibility until further equivalence testing is done, but the implementation with bounds is the preferred version to keep though
                            try:
                                (tepedopt, pcov) = curve_fit(tepedfunc, fxx, fyy, p0=[25.0, 3.8], sigma=fye, maxfev=5000)
                            except RuntimeError:
                                (tepedopt, pcov) = curve_fit(tepedfunc, fxx, fyy, p0=[25.0, 3.8], sigma=fye, bounds=((0.5, 3.5), (100.0, 4.0)))
                        # Turns out there is a separatrix temperature criteria for L-mode as well
                        else:
                            tepedfunctype = 2
                            tepedfunc = lambda x, mult, scale, loc, c: mult * np.power(scale * (x - loc), 2.0) + c
                            if fdebug:
                                print("   Performing parabolic fit on HRTS TE with L-mode settings...")
                            # Try block used to preserve backwards compatibility until further equivalence testing is done, but the implementation with bounds is the preferred version to keep though
                            try:
                                (tepedopt, pcov) = curve_fit(tepedfunc, fxx, fyy, p0=[10.0, 1.0, 4.0, 0.2], sigma=fye, maxfev=5000, ftol=1.0e-6)
                            except RuntimeError:
                                (tepedopt, pcov) = curve_fit(tepedfunc, fxx, fyy, p0=[10.0, 1.0, 4.0, 0.2], sigma=fye, bounds=((-np.Inf, 0.1, 3.8, 0.0), (np.Inf, 5.0, 4.3, 0.2)), ftol=1.0e-6)

        if 'hrts/ne' in m and RD[m['hrts/ne']] is not None:
            if HRTS is None:
                HRTS = dict()
            vals = RD[m['hrts/ne']]                                    # Electron density (R)
            errs = RD[m['hrts/dne']] if 'hrts/dne' in m else None      # 1 sigma (see DDA handbook)
            prov = RD["I_"+m['hrts/ne']] + "; " + RD["I_"+m['hrts/dne']] if errs is not None else RD["I_"+m['hrts/ne']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['hrts/ne']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
                    if errs is not None:
                        errs = errs[tmask]
            (HRTS["NE"], HRTS["NEEB"], HRTS["NEEM"], navgmap) = ptools.filtered_average(vals, value_errors=errs, lower_bounds=1.0e10, use_n=True)
            HRTS["RMNE"] = RD[m['hrts/rmid']].copy()
            navgmap = np.atleast_2d(navgmap)
            HRTS["NNE"] = np.count_nonzero(navgmap, axis=0).flatten()
            if 'hrts/z' in m and RD[m['hrts/z']] is not None:
                HRTS["RNE"] = RD["X_"+m['hrts/ne']].copy()
                HRTS["ZNE"] = RD[m['hrts/z']].copy()
            if "USERZFLAG" in PD and PD["USERZFLAG"] and "RNE" in HRTS and "ZNE" in HRTS and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], HRTS["RNE"], HRTS["ZNE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                outerflag = True if HRTS["RNE"][0] > HRTS["RNE"][-1] else False
                HRTS["RMNE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                prov = prov + "; " + RD["I_"+m['hrts/z']]
            else:
                prov = prov + "; " + RD["I_"+m['hrts/rmid']]
            fnscale = PD["USENSCALEFLAG"] if "USENSCALEFLAG" in PD else True
            PD["CNEFLAG"] = True if 'hrtx/lid3' in m and RD[m['hrtx/lid3']] is not None and nelineavg is not None and fnscale else False
            (nelineavg_recon, neleb, nelem, navgmap) = ptools.filtered_average(RD[m['hrtx/lid3']])
            if nelineavg is None and nelineavg_recon is not None:
                nelineavg = nelineavg_recon
                llos = 2.25
                PD["NEBAR"] = np.array([nelineavg / llos])
                PD["NEBARDS"] = RD["I_"+m['hrtx/lid3']]
            PD["HRTSC"] = nelineavg / nelineavg_recon if PD["CNEFLAG"] else 1.0
            PD["HRTSCEB"] = np.array([0.0])
            HRTS["NE"] = PD["HRTSC"] * HRTS["NE"] if HRTS["NE"] is not None else None
            HRTS["NEEB"] = PD["HRTSC"] * HRTS["NEEB"] if HRTS["NEEB"] is not None else None
            HRTS["NEEM"] = PD["HRTSC"] * HRTS["NEEM"] if HRTS["NEEM"] is not None else None
            if PD["CNEFLAG"]:
                prov = prov + "; " + RD["I_"+m['hrtx/lid3']]
            # Suspected overestimation in HRTS error, consequence of high-resolution?
#            if HRTS is not None and "NE" in HRTS:
#                HRTS["NEEB"] = 0.9 * HRTS["NEEB"]
            HRTS["NEDS"] = prov

            # Constructing rough edge density fit function (tanh) used for estimating necessary shift in major radius (prioritizes reflectometry profiles)
            if "KG10" in PD and PD["KG10"] is not None:
                # Define low-field-side midplane major radius using largest value in boundary vector from EFIT, use as reference point for pedestal region
                rmax = np.nanmax(PD["RBND"]) if "RBND" in PD and PD["RBND"] is not None else 3.85
                ffilt = np.isfinite(HRTS["NE"])
                xx = copy.deepcopy(HRTS["RMNE"][ffilt])
                yy = copy.deepcopy(HRTS["NE"][ffilt])
                ye = copy.deepcopy(HRTS["NEEB"][ffilt])
                efilt = (xx >= rmax - 0.15)   # Heuristic width sufficient to capture pedestal data at JET
                # Define line-averaged electron density for separatrix density estimation
                raxs = PD["RMAG"] if "RMAG" in PD and PD["RMAG"] is not None else 3.00
                afilt = (xx >= raxs)
                #nelineavg = float(np.nanmean(yy[afilt]))
                # Define reference electron density for averaged core density gradient for separatrix density estimation
                rmid = 3.3
                needg = np.where(efilt)[0]
                nemid = np.where(xx >= rmid)[0]
                nepedtop = float(yy[needg[0]])
                necoreslp = float((yy[nemid[0]] - yy[needg[0]]) / (xx[nemid[0]] - xx[needg[0]]) / yy[nemid[0]])
                # Estimate pedestal top density as the density at point closest to but outside the heuristic pedestal region
                xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
                ymax = yy[xmin[0]] if xmin.size > 0 else 5.0e18
                idxo = np.where(xx >= np.nanmin(xx[efilt]))[0]
                yedge = yy[idxo[0]] if len(idxo) > 0 else 0.0
                if np.any(efilt) and yy[efilt].size > 1:
                    peddata = np.vstack((xx[efilt].flatten(), yy[efilt].flatten()))
                    peddata = peddata[:, np.argsort(peddata[0, :])]
                    bdvec = np.diff(peddata[1, :].flatten()) / np.diff(peddata[0, :].flatten())
                    neyscale = yedge
                    gfilt = np.all([efilt, yy < 1.1 * neyscale, yy > 0.0], axis=0)
                    if np.any(gfilt) and yy[gfilt].size > 3:
                        fxx = xx[gfilt]
                        fyy = yy[gfilt] / neyscale
                        fye = ye[gfilt] / neyscale

                        # Only applies major radius shift if minimum gradient (these are all negative values) and innermost point match criteria
                        if np.nanmin(bdvec) <= -ymax and yedge > 5.0e18:
                            nepedfunctype = 1
                            nepedfunc = lambda x, scale, loc: 0.5 - 0.5 * np.tanh(scale * (x - loc))
                            if fdebug:
                                print("   Performing tanh fit on KG10 NE with default settings...")
                            # Try block used to preserve backwards compatibility until further equivalence testing is done, but the implementation with bounds is the preferred version to keep though
                            try:
                                (nepedopt, pcov) = curve_fit(nepedfunc, fxx, fyy, p0=[25.0, 3.8], sigma=fye, maxfev=5000)
                            except RuntimeError:
                                (nepedopt, pcov) = curve_fit(nepedfunc, fxx, fyy, p0=[25.0, 3.8], sigma=fye, bounds=((0.5, 3.5), (100.0, 4.0)))

            elif HRTS["NE"] is not None:
                # Define low-field-side midplane major radius using largest value in boundary vector from EFIT, use as reference point for pedestal region
                rmax = np.nanmax(PD["RBND"]) if "RBND" in PD and PD["RBND"] is not None else 3.85
                ffilt = np.isfinite(HRTS["NE"])
                xx = copy.deepcopy(HRTS["RMNE"][ffilt])
                yy = copy.deepcopy(HRTS["NE"][ffilt])
                ye = copy.deepcopy(HRTS["NEEB"][ffilt])
                efilt = (xx >= rmax - 0.15)   # Heuristic width sufficient to capture pedestal data at JET
                # Define reference electron density for averaged core density gradient for separatrix density estimation
                rmid = 3.3
                needg = np.where(efilt)[0]
                nemid = np.where(xx >= rmid)[0]
                nepedtop = float(yy[needg[0]])
                necoreslp = float((yy[nemid[0]] - yy[needg[0]]) / (xx[nemid[0]] - xx[needg[0]]) / yy[nemid[0]])
                # Define line-averaged electron density for separatrix density estimation
                raxs = PD["RMAG"] if "RMAG" in PD and PD["RMAG"] is not None else 3.00
                afilt = (xx >= raxs)
                #nelineavg = float(np.nanmean(yy[afilt]))
                # Estimate pedestal top density as the density at point closest to but outside the heuristic pedestal region
                xmin = np.where(np.abs(xx) <= np.nanmin(np.abs(xx)))[0]
                ymax = yy[xmin[0]] if xmin.size > 0 else 5.0e18
                idxo = np.where(xx >= np.nanmin(xx[efilt]))[0]
                yedge = yy[idxo[0]] if len(idxo) > 0 else 0.0
                if np.any(efilt) and yy[efilt].size > 1:
                    peddata = np.vstack((xx[efilt].flatten(), yy[efilt].flatten()))
                    peddata = peddata[:, np.argsort(peddata[0, :])]
                    bdvec = np.diff(peddata[1, :].flatten()) / np.diff(peddata[0, :].flatten())
                    neyscale = yedge
                    gfilt = np.all([efilt, yy < 1.1 * neyscale, yy > 0.0], axis=0)
                    if np.any(gfilt) and yy[gfilt].size > 3:
                        fxx = xx[gfilt]
                        fyy = yy[gfilt] / neyscale
                        fye = ye[gfilt] / neyscale

                        # Only applies major radius shift if minimum gradient (these are all negative values) and innermost point match criteria
                        if np.nanmin(bdvec) <= -ymax and yedge > 5.0e18:
                            nepedfunctype = 1
                            nepedfunc = lambda x, scale, loc: 0.5 - 0.5 * np.tanh(scale * (x - loc))
                            if fdebug:
                                print("   Performing tanh fit on HRTS NE with default settings...")
                            # Try block used to preserve backwards compatibility until further equivalence testing is done, but the implementation with bounds is the preferred version to keep though
                            try:
                                (nepedopt, pcov) = curve_fit(nepedfunc, fxx, fyy, p0=[25.0, 3.8], sigma=fye, maxfev=5000)
                            except RuntimeError:
                                (nepedopt, pcov) = curve_fit(nepedfunc, fxx, fyy, p0=[25.0, 3.8], sigma=fye, bounds=((0.5, 3.5), (100.0, 4.0)))

        # Defines major radius shift required to adhere to separatrix density and temperature estimates
        tesep = None
        nesep = None
        termajedge = None
        nermajedge = None
        if tepedfunc is not None and tepedfunctype in [1, 2]:
            teupper = None
            if tepedfunctype == 1:
                tesep = 1.0e2               # Average criteria obtained from 2-point SOL model (+/- 20 eV)
                termajedge = (np.arctanh(1.0 - 2.0 * (tesep / teyscale)) / tepedopt[0]) + tepedopt[1]
                teupper = 1.5e2
            elif tepedfunctype == 2:
                tesep = 7.0e1               # Empirical criteria - unsure if also derived from results of 2-point SOL model
                if tepedopt[3] <= tesep / teyscale:
                    # There are two solutions to the quadratic equation
                    psol = tepedopt[2] + np.sqrt((tesep / teyscale - tepedopt[3]) / tepedopt[0]) / tepedopt[1]
                    nsol = tepedopt[2] - np.sqrt((tesep / teyscale - tepedopt[3]) / tepedopt[0]) / tepedopt[1]
                    termajedge = nsol if nsol >= (rmax - 0.15) and nsol <= rmax and psol >= rmax else psol
                teupper = 1.3e2
            if tepedfunctype == 1 and nepedfunc is not None:    # Only apply density criteria if time window is in H-mode (unknown density criteria for L-mode)
                nefac = 1.0 + 0.5 * necoreslp
                nesep = nefac * nelineavg   # Empirical criteria - should be 0.4 * volume averaged density but only have line average density right now
                if nesep < (nepedtop / 3.0):
                    nesep = nepedtop / 3.0
                elif nesep > (nepedtop / 2.0):
                    nesep = nepedtop / 2.0
                nermajedge = (np.arctanh(1.0 - 2.0 * (nesep / neyscale)) / nepedopt[0]) + nepedopt[1]
                if termajedge is not None and nermajedge < termajedge:
                    newte = tepedfunc(nermajedge, *tepedopt)
                    if newte * teyscale <= teupper:
                        tesep = float(newte * teyscale)
                        if nermajedge is not None:
                            PD["RMAJEDGE"] = np.array([nermajedge])
                    else:
                        if tepedfunctype == 1:
                            tesep = teupper
                            termajedge = (np.arctanh(1.0 - 2.0 * (tesep / teyscale)) / tepedopt[0]) + tepedopt[1]
                        elif tepedfunctype == 2:
                            if tepedopt[3] <= teupper / teyscale:
                                tesep = teupper
                                # There are two solutions to the quadratic equation
                                psol = tepedopt[2] + np.sqrt((tesep / teyscale - tepedopt[3]) / tepedopt[0]) / tepedopt[1]
                                nsol = tepedopt[2] - np.sqrt((tesep / teyscale - tepedopt[3]) / tepedopt[0]) / tepedopt[1]
                                termajedge = nsol if nsol >= (rmax - 0.15) and nsol <= rmax and psol >= rmax else psol
                        newne = nepedfunc(termajedge, *nepedopt)
                        nesep = float(newne * neyscale)
                        if termajedge is not None:
                            PD["RMAJEDGE"] = np.array([termajedge])
                elif termajedge is not None:
                    PD["RMAJEDGE"] = np.array([termajedge])
            else:
                if nelineavg is not None and necoreslp is not None:
                    nefac = 1.0 + 0.5 * necoreslp
                    nesep = nefac * nelineavg   # Empirical criteria - should be 0.4 * volume averaged density but only have line average density right now
                    if nesep < (nepedtop / 3.0):
                        nesep = nepedtop / 3.0
                    elif nesep > (nepedtop / 2.0):
                        nesep = nepedtop / 2.0
                if termajedge is not None:
                    PD["RMAJEDGE"] = np.array([termajedge])
        elif nepedfunc is not None:
            nefac = 1.0 + 0.5 * necoreslp
            nesep = nefac * nelineavg   # Empirical criteria - should be 0.4 * volume averaged density but only have line average density right now
            if nesep < (nepedtop / 3.0):
                nesep = nepedtop / 3.0
            elif nesep > (nepedtop / 2.0):
                nesep = nepedtop / 2.0
            nermajedge = (np.arctanh(1.0 - 2.0 * (nesep / neyscale)) / nepedopt[0]) + nepedopt[1]
            if nermajedge is not None:
                PD["RMAJEDGE"] = np.array([nermajedge])

        PD["TERMAJTOP"] = None
        PD["NERMAJTOP"] = None
        if nepedfunctype == 1 and tepedfunctype == 1:
            termajtop = float(tepedopt[1] - (2.0 / tepedopt[0]))
            if termajtop >= 3.7 and termajtop <= 4.2:
                PD["TERMAJTOP"] = np.array([termajtop])
            nermajtop = float(nepedopt[1] - (2.0 / nepedopt[0]))
            if nermajtop >= 3.7 and nermajtop <= 4.2:
                PD["NERMAJTOP"] = np.array([nermajtop])

        # These values are used later to define the boundary condition points added to raw data to constrain GPR fits
        PD["TESEP"] = np.array([tesep]) if tesep is not None else None
        PD["NESEP"] = np.array([nesep]) if nesep is not None else None
        PD["TESEPSHIFT"] = np.array([tesep]) if tesep is not None else None
        PD["NESEPSHIFT"] = np.array([nesep]) if nesep is not None else None
        PD["TERMAJEDGE"] = np.array([termajedge]) if termajedge is not None else None
        PD["NERMAJEDGE"] = np.array([nermajedge]) if nermajedge is not None else None
        PD["HMODEFLAG"] = (tepedfunc is not None and tepedfunctype == 1)

        if fdebug and (PD["TESEPSHIFT"] is not None and PD["NESEPSHIFT"] is not None):
            print("process_jet.py: unpack_profile_data(): Ad-hoc radial shift calculated.")

        # Removal of data near turning point of HRTS line-of-sight in flux, due to high noise
        if HRTS is not None and "TE" in HRTS and HRTS["TE"] is not None:
            rfilt = np.invert(np.all([HRTS["RMTE"] <= 3.25, HRTS["RMTE"] >= 2.75], axis=0))
            if np.any(rfilt):
                HRTS["TE"] = HRTS["TE"][rfilt]
                HRTS["TEEB"] = HRTS["TEEB"][rfilt]
                HRTS["TEEM"] = HRTS["TEEM"][rfilt]
                HRTS["RMTE"] = HRTS["RMTE"][rfilt]
                HRTS["NTE"] = HRTS["NTE"][rfilt]
                if "RTE" in HRTS and HRTS["RTE"] is not None:
                    HRTS["RTE"] = HRTS["RTE"][rfilt]
                if "ZTE" in HRTS and HRTS["ZTE"] is not None:
                    HRTS["ZTE"] = HRTS["ZTE"][rfilt]

                # Removal also applies to HRTS NE data
                if "NE" in HRTS and HRTS["NE"] is not None:
                    HRTS["NE"] = HRTS["NE"][rfilt]
                    HRTS["NEEB"] = HRTS["NEEB"][rfilt]
                    HRTS["NEEM"] = HRTS["NEEM"][rfilt]
                    HRTS["RMNE"] = HRTS["RMNE"][rfilt]
                    HRTS["NNE"] = HRTS["NNE"][rfilt]
                if "RNE" in HRTS and HRTS["RNE"] is not None:
                    HRTS["RNE"] = HRTS["RNE"][rfilt]
                if "ZNE" in HRTS and HRTS["ZNE"] is not None:
                    HRTS["ZNE"] = HRTS["ZNE"][rfilt]
        PD["HRTS"] = HRTS

        if fdebug and PD["HRTS"] is not None:
            print("process_jet.py: unpack_profile_data(): HRTS Thomson scattering data added.")

        # Original Thomson scattering system at JET - electron density and temperature
        LIDR = None
        if 'lidr/te' in m and RD[m['lidr/te']] is not None:
            if LIDR is None:
                LIDR = dict()
            vals = RD[m['lidr/te']]                                    # 10% err.
            lvals = RD[m['lidr/tel']] if 'lidr/tel' in m else None     # hilo is 1 sigma for the fit
            uvals = RD[m['lidr/teu']] if 'lidr/teu' in m else None
            prov = RD["I_"+m['lidr/te']]
            if lvals is not None:
                prov = prov + "; " + RD["I_"+m['lidr/tel']]
            if uvals is not None:
                prov = prov + "; " + RD["I_"+m['lidr/teu']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['lidr/te']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
                    if lvals is not None:
                        lvals = lvals[tmask]
                    if uvals is not None:
                        uvals = uvals[tmask]
            (LIDR["TE"], LIDR["TEEB"], LIDR["TEEM"], navgmap) = ptools.filtered_average(vals, lower_values=lvals, upper_values=uvals, lower_bounds=1.0e1, use_n=True)
            LIDR["RMTE"] = RD[m['lidr/rmid']].copy() if 'lidr/rmid' in m and RD[m['lidr/rmid']] is not None else RD["X_"+m['lidr/te']].copy()
            navgmap = np.atleast_2d(navgmap)
            LIDR["NTE"] = np.count_nonzero(navgmap, axis=0).flatten()
            if 'lidr/z' in m and RD[m['lidr/z']] is not None:
                LIDR["RTE"] = RD["T_"+m['lidr/z']].copy()
                LIDR["ZTE"] = RD[m['lidr/z']].copy()
            if "USERZFLAG" in PD and PD["USERZFLAG"] and "RTE" in LIDR and "ZTE" in LIDR and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], LIDR["RTE"], LIDR["ZTE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                outerflag = True if LIDR["RTE"][0] > LIDR["RTE"][-1] else False
                LIDR["RMTE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                prov = prov + "; " + RD["I_"+m['lidr/z']]
            else:
                prov = prov + "; " + RD["I_"+m['lidr/rmid']]
            LIDR["TEDS"] = prov
            nanfilt = np.invert(np.isnan(LIDR["TE"]))
            if not np.all(nanfilt):
                LIDR["TE"] = LIDR["TE"][nanfilt]
                LIDR["TEEB"] = LIDR["TEEB"][nanfilt]
                LIDR["TEEM"] = LIDR["TEEM"][nanfilt]
                LIDR["RMTE"] = LIDR["RMTE"][nanfilt]
                LIDR["NTE"] = LIDR["NTE"][nanfilt]
                if "RTE" in LIDR and LIDR["RTE"] is not None:
                    LIDR["RTE"] = LIDR["RTE"][nanfilt]
                if "ZTE" in LIDR and LIDR["ZTE"] is not None:
                    LIDR["ZTE"] = LIDR["ZTE"][nanfilt]
        if 'lidr/ne' in m and RD[m['lidr/ne']] is not None:
            if LIDR is None:
                LIDR = dict()
            vals = RD[m['lidr/ne']]                                    # 5% err.
            lvals = RD[m['lidr/nel']] if 'lidr/nel' in m else None     # hilo is 1 sigma for the fit
            uvals = RD[m['lidr/neu']] if 'lidr/neu' in m else None
            prov = RD["I_"+m['lidr/ne']]
            if lvals is not None:
                prov = prov + "; " + RD["I_"+m['lidr/nel']]
            if uvals is not None:
                prov = prov + "; " + RD["I_"+m['lidr/neu']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['lidr/ne']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
                    if lvals is not None:
                        lvals = lvals[tmask]
                    if uvals is not None:
                        uvals = uvals[tmask]
            (LIDR["NE"], LIDR["NEEB"], LIDR["NEEM"], navgmap) = ptools.filtered_average(vals, lower_values=lvals, upper_values=uvals, lower_bounds=1.0e10, use_n=True)
            LIDR["RMNE"] = RD[m['lidr/rmid']].copy() if 'lidr/rmid' in m and RD[m['lidr/rmid']] is not None else RD["X_"+m['lidr/ne']].copy()
            navgmap = np.atleast_2d(navgmap)
            LIDR["NNE"] = np.count_nonzero(navgmap, axis=0).flatten()
            if 'lidr/z' in m and RD[m['lidr/z']] is not None:
                LIDR["RNE"] = RD["T_"+m['lidr/z']].copy()
                LIDR["ZNE"] = RD[m['lidr/z']].copy()
            if "USERZFLAG" in PD and PD["USERZFLAG"] and "RNE" in LIDR and "ZNE" in LIDR and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], LIDR["RNE"], LIDR["ZNE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                outerflag = True if LIDR["RNE"][0] > LIDR["RNE"][-1] else False
                LIDR["RMNE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                prov = prov + "; " + RD["I_"+m['lidr/z']]
            else:
                prov = prov + "; " + RD["I_"+m['lidr/rmid']]
            LIDR["NEDS"] = prov
            # Strange that this filters on LIDR TE NaNs... is this intended?
            if not np.all(nanfilt):
                LIDR["NE"] = LIDR["NE"][nanfilt]
                LIDR["NEEB"] = LIDR["NEEB"][nanfilt]
                LIDR["NEEM"] = LIDR["NEEM"][nanfilt]
                LIDR["RMNE"] = LIDR["RMNE"][nanfilt]
                LIDR["NNE"] = LIDR["NNE"][nanfilt]
                if "RNE" in LIDR and LIDR["RNE"] is not None:
                    LIDR["RNE"] = LIDR["RNE"][nanfilt]
                if "ZNE" in LIDR and LIDR["ZNE"] is not None:
                    LIDR["ZNE"] = LIDR["ZNE"][nanfilt]

        # Removal of LIDR data near separatrix, large systematic errors due to optical properties of plasma
        if LIDR is not None:
            if "NE" in LIDR and LIDR["NE"] is not None:
                nelim = 2.0e19 if not (HRTS is not None and "NE" in HRTS) else -1.0e19
                rfilt = np.invert(np.all([LIDR["RMNE"] >= float(PD["ORMAJEDGE"][0] - 0.1), LIDR["NE"] >= nelim], axis=0))
                if np.any(rfilt):
                    LIDR["NE"] = LIDR["NE"][rfilt]
                    LIDR["NEEB"] = LIDR["NEEB"][rfilt]
                    LIDR["NEEM"] = LIDR["NEEM"][rfilt]
                    LIDR["RMNE"] = LIDR["RMNE"][rfilt]
                    LIDR["NNE"] = LIDR["NNE"][rfilt]
                    if "RNE" in LIDR and LIDR["RNE"] is not None:
                        LIDR["RNE"] = LIDR["RNE"][rfilt]
                    if "ZNE" in LIDR and LIDR["ZNE"] is not None:
                        LIDR["ZNE"] = LIDR["ZNE"][rfilt]
                # Suspected underestimation in LIDR error compared to HRTS
#                if LIDR is not None and "NE" in LIDR and not pureflag:
#                    LIDR["NEEB"] = 1.2 * LIDR["NEEB"]
            if "TE" in LIDR and LIDR["TE"] is not None:
                telim = 5.0e2 if not (HRTS is not None and "TE" in HRTS) else -1.0e2
                rfilt = np.invert(np.all([LIDR["RMTE"] >= float(PD["ORMAJEDGE"][0] - 0.1), LIDR["TE"] >= telim], axis=0))
                if np.any(rfilt):
                    LIDR["TE"] = LIDR["TE"][rfilt]
                    LIDR["TEEB"] = LIDR["TEEB"][rfilt]
                    LIDR["TEEM"] = LIDR["TEEM"][rfilt]
                    LIDR["RMTE"] = LIDR["RMTE"][rfilt]
                    LIDR["NTE"] = LIDR["NTE"][rfilt]
                    if "RTE" in LIDR and LIDR["RTE"] is not None:
                        LIDR["RTE"] = LIDR["RTE"][rfilt]
                    if "ZTE" in LIDR and LIDR["ZTE"] is not None:
                        LIDR["ZTE"] = LIDR["ZTE"][rfilt]
                # Suspected underestimation in LIDR error compared to HRTS
#                if LIDR is not None and "TE" in LIDR and not pureflag:
#                    LIDR["TEEB"] = 1.2 * LIDR["TEEB"]
        PD["LIDR"] = LIDR

        if fdebug and PD["LIDR"] is not None:
            print("process_jet.py: unpack_profile_data(): LIDR Thomson scattering data added.")

        # Electron cyclotron emission from radiometer system at JET - electron temperature
        ECR = None
        pvec = []
        cpvec = []
        apvec = []
        for nn in range(96):
            ktag = "%02d" % (nn + 1)
            if 'kk3/te'+ktag in m and RD[m['kk3/te'+ktag]] is not None and 'kk3/rc'+ktag in m and RD[m['kk3/rc'+ktag]] is not None and 'kk3/ra'+ktag in m and RD[m['kk3/ra'+ktag]] is not None:
                if ECR is None:
                    ECR = dict()
                rmax = np.nanmax(PD["RBND"]) if "RBND" in PD and PD["RBND"] is not None else 3.85
                rcfunc = interp1d(RD["T_"+m['kk3/rc'+ktag]], RD[m['kk3/rc'+ktag]], kind='linear', bounds_error=False, fill_value=(RD[m['kk3/rc'+ktag]][0], RD[m['kk3/rc'+ktag]][-1]))
                rafunc = interp1d(RD["T_"+m['kk3/ra'+ktag]], RD[m['kk3/ra'+ktag]], kind='linear', bounds_error=False, fill_value=(RD[m['kk3/ra'+ktag]][0], RD[m['kk3/ra'+ktag]][-1]))
                vals = RD[m['kk3/te'+ktag]]                            # 10% err. - assumed to be 2 sigma
                tvals = RD["T_"+m['kk3/te'+ktag]].copy()
                rcvals = rcfunc(tvals)
                if vals is not None and tmlist is not None and np.mean(rcvals) > (rmax - 0.15):
                    tmask = ptools.create_range_mask(RD["T_"+m['kk3/te'+ktag]], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                        tvals = tvals[tmask]
                (te, teeb, teem, navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=1.0e1, use_n=True)
                rcvals = rcfunc(tvals)
                (rc, rceb, rcem, rcmap) = ptools.filtered_average(rcvals)
                ravals = rafunc(tvals)
                (ra, raeb, raem, ramap) = ptools.filtered_average(ravals)
                if te is None:
                    te = np.NaN
                    teeb = np.NaN
                    teem = np.NaN
                if rc is None:
                    rc = np.NaN
                if ra is None:
                    ra = np.NaN
                pvec.append(RD["I_"+m['kk3/te'+ktag]])
                cpvec.append(RD["I_"+m['kk3/rc'+ktag]])
                apvec.append(RD["I_"+m['kk3/ra'+ktag]])
                ECR["TE"] = np.hstack((ECR["TE"], te)) if "TE" in ECR else np.array([te]).flatten()
                ECR["TEEB"] = np.hstack((ECR["TEEB"], teeb)) if "TEEB" in ECR else np.array([teeb]).flatten()
                ECR["TEEM"] = np.hstack((ECR["TEEM"], teem)) if "TEEM" in ECR else np.array([teem]).flatten()
                ECR["RMTE"] = np.hstack((ECR["RMTE"], rc)) if "RMTE" in ECR else np.array([rc]).flatten()
                ECR["NTE"] = np.hstack((ECR["NTE"], np.count_nonzero(navgmap))) if "NTE" in ECR else np.array([np.count_nonzero(navgmap)]).flatten()
                ECR["RTE"] = np.hstack((ECR["RTE"], ra)) if "RTE" in ECR else np.array([ra]).flatten()
                zte = 0.1335 if int(PD["SHOT"]) < 80318 else 0.2485
                ECR["ZTE"] = np.hstack((ECR["ZTE"], zte)) if "ZTE" in ECR else np.array([zte]).flatten()
        use_ra = False
        if ECR is not None:
            if "USERZFLAG" in PD and PD["USERZFLAG"] and eqdda is not None and eqdda != "EFIT" and "RTE" in ECR and "ZTE" in ECR \
               and "PFXMAP" in PD and PD["PFXMAP"] is not None and "MPFXMAP" in PD and PD["MPFXMAP"] is not None \
               and "FPSI" in PD and PD["FPSI"] is not None and "MFPSI" in PD and PD["MFPSI"] is not None:
                rmdiff = np.diff(ECR["RMTE"])
                didxv = np.where((rmdiff[:-1] * rmdiff[1:]) < 0.0)[0]
                if len(didxv) > 0:
                    didx = didxv[0] + 2
                    if didx < ECR["RMTE"].size:
                        ECR["RMTE"] = np.hstack((ECR["RMTE"][didx:], ECR["RMTE"][:didx]))
                        ECR["TE"] = np.hstack((ECR["TE"][didx:], ECR["TE"][:didx]))
                        ECR["TEEB"] = np.hstack((ECR["TEEB"][didx:], ECR["TEEB"][:didx]))
                        ECR["TEEM"] = np.hstack((ECR["TEEM"][didx:], ECR["TEEM"][:didx]))
                        ECR["NTE"] = np.hstack((ECR["NTE"][didx:], ECR["NTE"][:didx]))
                        ECR["RTE"] = np.hstack((ECR["RTE"][didx:], ECR["RTE"][:didx]))
                        ECR["ZTE"] = np.hstack((ECR["ZTE"][didx:], ECR["ZTE"][:didx]))
                rint = np.linspace(np.nanmin(ECR["RTE"])-0.2, np.nanmax(ECR["RTE"])+0.2, 201)
                zint = np.full(rint.shape, 0.1335) if int(PD["SHOT"]) < 80318 else np.full(rint.shape, 0.2485)
                # ECE requires original EFIT equilibrium to apply the equilibrium shift
                mfpsipfx = PD["MFPSIPFN"] * (PD["MFBND"] - PD["MFAXS"]) + PD["MFAXS"]
                fpsipfx = PD["FPSIPFN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
                btotold, bpold, btold = ftools.rz2b(PD["MPFXMAP"], PD["MRMAP"], PD["MZMAP"], ECR["RTE"], ECR["ZTE"], PD["MFPSI"], mfpsipfx, axsval=float(PD["MFAXS"][0]), bndval=float(PD["MFBND"][0]))
                btotnew, bpnew, btnew = ftools.rz2b(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], rint, zint, PD["FPSI"], fpsipfx, axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]))
                blosfunc = interp1d(btotnew, rint, kind='linear', bounds_error=False, fill_value='extrapolate')
                rlostemp = blosfunc(btotold)
                ECR["RTE"] = rlostemp
                pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], ECR["RTE"], ECR["ZTE"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                outerflag = True if ECR["RTE"][0] > ECR["RTE"][-1] else False
                ECR["RMTE"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                use_ra = True
            # Flip values to match other diagnostics - in order from HFS to LFS
            if "RMTE" in ECR and ECR["RMTE"][0] > ECR["RMTE"][-1]:
                ECR["RMTE"] = ECR["RMTE"][::-1]
                ECR["TE"] = ECR["TE"][::-1]
                ECR["TEEB"] = ECR["TEEB"][::-1]
                ECR["TEEM"] = ECR["TEEM"][::-1]
                ECR["NTE"] = ECR["NTE"][::-1]
                ECR["RTE"] = ECR["RTE"][::-1]
                ECR["ZTE"] = ECR["ZTE"][::-1]
                pvec = pvec[::-1]
                cpvec = cpvec[::-1]
                apvec = apvec[::-1]
        # Heuristic removal of ECR data, based on observed issues during fits
        if ECR is not None and "TE" in ECR and ECR["TE"] is not None:
            # Removal of core data which is lower than profile-averaged temperature and edge data which is higher than profile-averaged temperature
            if ECR is not None and "RMTE" in ECR and ECR["RMTE"] is not None and not pureflag:
                refte = ECR["TE"].copy()
                if "HRTS" in PD and PD["HRTS"] is not None and "TE" in PD["HRTS"]:
                    refte = PD["HRTS"]["TE"].copy()
                elif "LIDR" in PD and PD["LIDR"] is not None and "TE" in PD["LIDR"]:
                    refte = PD["LIDR"]["TE"].copy()
                teavg = np.mean(refte)
                corefilt = np.all([ECR["RMTE"] <= 3.3, ECR["RMTE"] >= 2.6], axis=0)
                edgefilt = np.any([ECR["RMTE"] <= 2.3, ECR["RMTE"] >= 3.6], axis=0)
                corefilt = np.all([corefilt, ECR["TE"] < 0.75 * teavg], axis=0)
                edgefilt = np.all([edgefilt, ECR["TE"] > 1.2 * teavg], axis=0)
                rfilt = np.invert(np.any([corefilt, edgefilt], axis=0))
                if np.any(rfilt):
                    ECR["TE"] = ECR["TE"][rfilt]
                    ECR["TEEB"] = ECR["TEEB"][rfilt]
                    ECR["TEEM"] = ECR["TEEM"][rfilt]
                    ECR["RMTE"] = ECR["RMTE"][rfilt]
                    ECR["NTE"] = ECR["NTE"][rfilt]
                    if "RTE" in ECR and ECR["RTE"] is not None:
                        ECR["RTE"] = ECR["RTE"][rfilt]
                    if "ZTE" in ECR and ECR["ZTE"] is not None:
                        ECR["ZTE"] = ECR["ZTE"][rfilt]
                    pvec = np.array(pvec)[rfilt].tolist()
                    cpvec = np.array(cpvec)[rfilt].tolist()
                    apvec = np.array(apvec)[rfilt].tolist()
                else:
                    ECR = None
            # Outer edge ECR data not trusted due to optically thin plasma
            if ECR is not None and "RMTE" in ECR and ECR["RMTE"] is not None and not pureflag:
                rfilt = np.all([ECR["RMTE"] <= float(PD["ORMAJEDGE"][0] - 0.05), ECR["RMTE"] >= 2.2], axis=0)
                if np.any(rfilt):
                    ECR["TE"] = ECR["TE"][rfilt]
                    ECR["TEEB"] = ECR["TEEB"][rfilt]
                    ECR["TEEM"] = ECR["TEEM"][rfilt]
                    ECR["RMTE"] = ECR["RMTE"][rfilt]
                    ECR["NTE"] = ECR["NTE"][rfilt]
                    if "RTE" in ECR and ECR["RTE"] is not None:
                        ECR["RTE"] = ECR["RTE"][rfilt]
                    if "ZTE" in ECR and ECR["ZTE"] is not None:
                        ECR["ZTE"] = ECR["ZTE"][rfilt]
                    fidx = np.arange(0, len(rfilt))[rfilt].tolist()
                    pvec = np.array(pvec)[rfilt].tolist()
                    cpvec = np.array(cpvec)[rfilt].tolist()
                    apvec = np.array(apvec)[rfilt].tolist()
                else:
                    ECR = None
                # Suspected underestimation in ECR error as plasma becomes more optically thin close to separatrix
#                if not pureflag:
#                    ECR["TEEB"] = ECR["TEEB"] + 0.325 * np.abs(ECR["RMTE"] - 3.0) * ECR["TE"]
        if ECR is not None and "RMAG" in PD and PD["RMAG"] is not None:
            if "ABSECEFLAG" in PD and PD["ABSECEFLAG"]:
                fhfs = ECR["RMTE"] < float(PD["RMAG"][0])
                (rmajo, dummyj, dummye) = jptools.convert_coords(PD, np.abs(ECR["RMTE"][fhfs]), "RMAJORI", "RMAJORO", prein="", preout="")
                ECR["RMTE"][fhfs] = rmajo
            else:
                rfilt = ECR["RMTE"] >= float(PD["RMAG"][0])
                if np.any(rfilt):
                    ECR["TE"] = ECR["TE"][rfilt]
                    ECR["TEEB"] = ECR["TEEB"][rfilt]
                    ECR["TEEM"] = ECR["TEEM"][rfilt]
                    ECR["RMTE"] = ECR["RMTE"][rfilt]
                    ECR["NTE"] = ECR["NTE"][rfilt]
                    if "RTE" in ECR and ECR["RTE"] is not None:
                        ECR["RTE"] = ECR["RTE"][rfilt]
                    if "ZTE" in ECR and ECR["ZTE"] is not None:
                        ECR["ZTE"] = ECR["ZTE"][rfilt]
                    fidx = np.arange(0, len(rfilt))[rfilt].tolist()
                    pvec = np.array(pvec)[rfilt].tolist()
                    cpvec = np.array(cpvec)[rfilt].tolist()
                    apvec = np.array(apvec)[rfilt].tolist()
        if ECR is not None and len(pvec) > 0:
            prov = '; '.join(pvec)
            rprov = '; '.join(apvec) if use_ra else '; '.join(cpvec)
            ECR["TEDS"] = prov + "; " + rprov
        PD["ECR"] = ECR if "USEECEFLAG" in PD and PD["USEECEFLAG"] else None

        if fdebug and PD["ECR"] is not None:
            print("process_jet.py: unpack_profile_data(): KK3 radiometer electron cyclotron emission data added.")

        # Modify LIDR in central core if ECE data exists (only for non-steady-state)
        if not pureflag and lmflag and PD["ECR"] is not None and PD["LIDR"] is not None:
            if "TE" in PD["LIDR"] and PD["LIDR"]["TE"] is not None and np.any(PD["ECR"]["RMTE"] <= 3.15):
                rfilt = (PD["LIDR"]["RMTE"] >= 3.2)
                if np.any(rfilt):
                    PD["LIDR"]["TE"] = PD["LIDR"]["TE"][rfilt]
                    PD["LIDR"]["TEEB"] = PD["LIDR"]["TEEB"][rfilt]
                    PD["LIDR"]["TEEM"] = PD["LIDR"]["TEEM"][rfilt]
                    PD["LIDR"]["RMTE"] = PD["LIDR"]["RMTE"][rfilt]
                    PD["LIDR"]["NTE"] = PD["LIDR"]["NTE"][rfilt]
                    if "RTE" in PD["LIDR"] and PD["LIDR"]["RTE"] is not None:
                        PD["LIDR"]["RTE"] = PD["LIDR"]["RTE"][rfilt]
                    if "ZTE" in PD["LIDR"] and PD["LIDR"]["ZTE"] is not None:
                        PD["LIDR"]["ZTE"] = PD["LIDR"]["ZTE"][rfilt]

        # Electron cyclotron emission from Michelson interferometer system at JET - electron temperature
        ECM = None
        # Priority given to data from chain2, if available
        if 'ecm2/tece' in m and RD[m['ecm2/tece']] is not None and 'ecm2/pece' in m and RD[m['ecm2/pece']] is not None:
            if ECM is None:
                ECM = dict()
            vals = RD[m['ecm2/tece']]                                   # No error estimate
            lvals = RD[m['ecm2/tecl']] if 'ecm2/tecl' in m else None    # hilo assumed to be 1 sigma
            uvals = RD[m['ecm2/tech']] if 'ecm2/tech' in m else None
            prov = RD["I_"+m['ecm2/tece']]
            if lvals is not None:
                prov = prov + "; " + RD["I_"+m['ecm2/tecl']]
            if uvals is not None:
                prov = prov + "; " + RD["I_"+m['ecm2/tech']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['ecm2/tece']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
                    if lvals is not None:
                        lvals = lvals[tmask]
                    if uvals is not None:
                        uvals = uvals[tmask]
            (ECM["TE"], ECM["TEEB"], ECM["TEEM"], navgmap) = ptools.filtered_average(vals, lower_values=lvals, upper_values=uvals, lower_bounds=5.0e1, use_n=True)
            navgmap = np.atleast_2d(navgmap)
            ECM["NTE"] = np.count_nonzero(navgmap, axis=0).flatten()
            ECM["RHOPN"] = np.mean(RD[m['ecm2/pece']], axis=0) if RD[m['ecm2/pece']].ndim >= 2 else copy.deepcopy(RD[m['ecm2/pece']])
            ECM["RHOPNEB"] = np.std(RD[m['ecm2/pece']], axis=0) if RD[m['ecm2/pece']].ndim >= 2 else np.zeros(RD[m['ecm2/pece']].shape)
            prov = prov + "; " + RD["I_"+m['ecm2/pece']] if prov is not None else RD["I_"+m['ecm2/pece']]
            ECM["PFNTE"] = np.power(ECM["RHOPN"], 2.0)
            pdiff = np.diff(ECM["PFNTE"])
            pswap = np.where((pdiff[:-1] * pdiff[1:]) < 0.0)[0]
            pidx = pswap[0] + 1 if len(pswap) > 0 else 0
            testvec = np.arange(0, ECM["PFNTE"].size)
            fhfs = (testvec < pidx)
            if np.any(fhfs):
                ECM["PFNTE"][fhfs] = -ECM["PFNTE"][fhfs]
            ECM["TEDS"] = prov
        # Otherwise use data from chain1, if available
        elif 'ecm1/prfl' in m and RD[m['ecm1/prfl']] is not None:
            if ECM is None:
                ECM = dict()
            vals = RD[m['ecm1/prfl']]                                   # 10% accuracy, 3% precision
            prov = RD["I_"+m['ecm1/prfl']]
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['ecm1/prfl']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (ECM["TE"], ECM["TEEB"], ECM["TEEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.03, lower_bounds=5.0e1, use_n=True)
            ECM["RMTE"] = RD["X_"+m['ecm1/prfl']].copy()
            navgmap = np.atleast_2d(navgmap)
            ECM["NTE"] = np.count_nonzero(navgmap, axis=0).flatten()
            ECM["TEDS"] = prov
            # Remove ECM data if it does not match with HRTS or LIDR data, lower trust in ECM since it has greater dependence on equilibrium
            if HRTS is not None:
                efilt = (np.all([ECM["RMTE"] <= 3.5, ECM["RMTE"] >= 3.1], axis=0))
                hfilt = (np.all([HRTS["RMTE"] <= 3.5, HRTS["RMTE"] >= 3.1], axis=0))
                if np.abs((np.mean(ECM["TE"][efilt]) - np.mean(HRTS["TE"][hfilt])) / np.mean(HRTS["TE"][hfilt])) > 0.2:
                    ECM = None
            elif LIDR is not None:
                efilt = (np.all([ECM["RMTE"] <= 3.5, ECM["RMTE"] >= 3.1], axis=0))
                lfilt = (np.all([LIDR["RMTE"] <= 3.5, LIDR["RMTE"] >= 3.1], axis=0))
                if np.abs((np.mean(ECM["TE"][efilt]) - np.mean(LIDR["TE"][lfilt])) / np.mean(LIDR["TE"][lfilt])) > 0.2:
                    ECM = None

        if ECM is not None and "TE" in ECM and ECM["TE"] is not None:
            # Outer edge ECM data not trusted due to optically thin plasma
            if "RMTE" in ECM and ECM["RMTE"] is not None:
                rfilt = (np.all([ECM["RMTE"] <= float(PD["ORMAJEDGE"][0] - 0.05), ECM["RMTE"] >= 2.2], axis=0))
                if np.any(rfilt) and not pureflag:
                    ECM["TE"] = ECM["TE"][rfilt]
                    ECM["TEEB"] = ECM["TEEB"][rfilt]
                    ECM["TEEM"] = ECM["TEEM"][rfilt]
                    ECM["RMTE"] = ECM["RMTE"][rfilt]
                    ECM["NTE"] = ECM["NTE"][rfilt]
                    if "RTE" in ECM and ECM["RTE"] is not None:
                        ECM["RTE"] = ECM["RTE"][rfilt]
                    if "ZTE" in ECM and ECM["ZTE"] is not None:
                        ECM["ZTE"] = ECM["ZTE"][rfilt]
                # Suspected underestimation in ECM error as plasma becomes more optically thin close to separatrix
                if not pureflag:
                    ECM["TEEB"] = ECM["TEEB"] + 0.325 * np.abs(ECM["RMTE"] - 3.0) * ECM["TE"]
            elif "PFNTE" in ECM and ECM["PFNTE"] is not None:
                rfilt = np.all([ECM["PFNTE"] <= 0.55, ECM["PFNTE"] >= 0.000625], axis=0)
                if np.any(rfilt) and not pureflag:
                    ECM["TE"] = ECM["TE"][rfilt]
                    ECM["TEEB"] = ECM["TEEB"][rfilt]
                    ECM["TEEM"] = ECM["TEEM"][rfilt]
                    ECM["PFNTE"] = ECM["PFNTE"][rfilt]
                    ECM["NTE"] = ECM["NTE"][rfilt]
                    if "RHOPN" in ECM and ECM["RHOPN"] is not None:
                        ECM["RHOPN"] = ECM["RHOPN"][rfilt]
                    if "RHOPNEB" in ECM and ECM["RHOPNEB"] is not None:
                        ECM["RHOPNEB"] = ECM["RHOPNEB"][rfilt]
                # Suspected underestimation in ECM error as plasma becomes more optically thin close to separatrix
                if not pureflag:
                    ECM["TEEB"] = ECM["TEEB"] + 0.25 * np.sqrt(np.abs(ECM["PFNTE"])) * ECM["TE"]
#       Removed for the moment, unsure if equilibrium correction procedure is identical to ECR
#        PD["ECM"] = ECM if "USEECEFLAG" in PD and PD["USEECEFLAG"] else None
        PD["ECM"] = None

        if fdebug and PD["ECM"] is not None:
            print("process_jet.py: unpack_profile_data(): ECM Michelson interferometer electron cyclotron emission data added.")

        # Charge exchange spectroscopy system at JET - ion temperature, angular frequency, toroidal velocity, and impurity density
        CX = None
        tags = ['cx', \
                'cxsm', 'cxdm', 'cxgm', 'cxfm', 'cxhm', 'cxkm', \
                'cxs4', 'cxd4', 'cxg4', 'cxf4', 'cxh4', \
                'cxs6', 'cxd6', 'cxg6', 'cxf6', 'cxh6', \
                'cxs8', 'cxd8', 'cxg8', 'cxf8', 'cxh8', \
                'cx7a', 'cx7b', 'cx7c', 'cx7d', 'cxse']
        for kk in range(len(tags)):
            if tags[kk]+'/ti' in m and RD[m[tags[kk]+'/ti']] is not None:
                if CX is None:
                    CX = dict()
                CXD = dict()

                # CXSE has different field names than other edge CX PPFs
                if re.match('cxse', tags[kk]):   # Errors given as 1 sigma in CXSE (see DDA handbook)
                    vals = RD[m[tags[kk]+'/ti']]
                    errs = RD[m[tags[kk]+'/tie']] if tags[kk]+'/tie' in m and RD[m[tags[kk]+'/tie']] is not None else None
                    tiprov = RD["I_"+m[tags[kk]+'/ti']] + "; " + RD["I_"+m[tags[kk]+'/tie']] if errs is not None else RD["I_"+m[tags[kk]+'/ti']]
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(RD["T_"+m[tags[kk]+'/ti']], tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                            if errs is not None:
                                errs = errs[tmask]
                    (CXD["TI"], CXD["TIEB"], CXD["TIEM"], navgmap) = ptools.filtered_average(vals, value_errors=errs, lower_bounds=1.0e1, upper_bounds=1.0e7, use_n=True)
                    CXD["RMTI"] = RD["X_"+m[tags[kk]+'/ti']] if CXD["TI"] is not None else None
                    navgmap = np.atleast_2d(navgmap)
                    CXD["NTI"] = np.count_nonzero(navgmap, axis=0).flatten()
                    if tags[kk]+'/rt' in m and RD[m[tags[kk]+'/rt']] is not None:
                        CXD["RTI"] = RD[m[tags[kk]+'/rt']].copy()
                        if np.mean(np.diff(CXD["RTI"])) < 1.0e-6:
                            CXD["RTI"] = RD["X_"+m[tags[kk]+'/rt']].copy()
                        CXD["ZTI"] = np.full(CXD["RTI"].shape, PD["ZMAG"]) if "ZMAG" in PD else np.zeros(CXD["RTI"].shape)
                        if CXD["RMTI"] is None:
                            CXD["RMTI"] = CXD["RTI"].copy()
                    if "USERZFLAG" in PD and PD["USERZFLAG"] and "RTI" in CXD and "ZTI" in CXD and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                        pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], CXD["RTI"], CXD["ZTI"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                        outerflag = True if CXD["RTI"][0] > CXD["RTI"][-1] else False
                        CXD["RMTI"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                        tiprov = tiprov + "; " + RD["I_"+m[tags[kk]+'/rt']]
                    if CXD["TIEB"] is not None:
                        zfilt = (CXD["TIEB"] < 1.0)
                        if np.any(zfilt):
                            CXD["TIEB"][zfilt] = 0.2 * CXD["TI"][zfilt]
                    CXD["TIDS"] = tiprov
                    # Process toroidal rotation data if present
                    if tags[kk]+'/angf' in m and RD[m[tags[kk]+'/angf']] is not None:
                        vals = RD[m[tags[kk]+'/angf']]
                        errs = RD[m[tags[kk]+'/ange']] if tags[kk]+'/ange' in m and RD[m[tags[kk]+'/ange']] is not None else None
                        afprov = RD["I_"+m[tags[kk]+'/angf']] + "; " + RD["I_"+m[tags[kk]+'/ange']] if errs is not None else RD["I_"+m[tags[kk]+'/angf']]
                        if vals is not None and tmlist is not None:
                            tmask = ptools.create_range_mask(RD["T_"+m[tags[kk]+'/angf']], tmlist)
                            if tmask is not None:
                                vals = vals[tmask]
                                if errs is not None:
                                    errs = errs[tmask]
                        (afc, afceb, afcem, navgcmap) = ptools.filtered_average(vals, value_errors=errs, use_n=True)
                        (afu, afueb, afuem, navgumap) = ptools.filtered_average(vals, value_errors=errs, lower_bounds=1.0e-5, use_n=True)
                        (afl, afleb, aflem, navglmap) = ptools.filtered_average(vals, value_errors=errs, upper_bounds=-1.0e-5, use_n=True)
                        nafu = np.zeros(afc.shape).flatten()
                        nafl = np.zeros(afc.shape).flatten()
                        ufilt = np.full(afc.shape, True)
                        lfilt = np.full(afc.shape, True)
                        if afu is not None:
                            if afu.size > 1:
                                navgumap = np.atleast_2d(navgumap)
                            nafu = np.count_nonzero(navgumap, axis=0).flatten()
                            ufilt = np.invert(np.isfinite(afu))
                            afu[ufilt] = 0.0
                            afueb[ufilt] = 0.0
                            afuem[ufilt] = 0.0
                        else:
                            afu = np.zeros(afc.shape)
                            afueb = np.zeros(afc.shape)
                            afuem = np.zeros(afc.shape)
                        if afl is not None:
                            if afl.size > 1:
                                navglmap = np.atleast_2d(navglmap)
                            nafl = np.count_nonzero(navglmap, axis=0).flatten()
                            lfilt = np.invert(np.isfinite(afl))
                            afl[lfilt] = 0.0
                            afleb[lfilt] = 0.0
                            aflem[lfilt] = 0.0
                        else:
                            afl = np.zeros(afc.shape)
                            afleb = np.zeros(afc.shape)
                            aflem = np.zeros(afc.shape)
                        ffilt = np.invert(np.all([ufilt, lfilt], axis=0))
                        nfilt = np.all([ffilt, (nafu + nafl) > 0], axis=0)
                        CXD["AF"] = np.full(nfilt.shape, np.NaN)
                        CXD["AFEB"] = np.full(nfilt.shape, np.NaN)
                        CXD["AFEM"] = np.full(nfilt.shape, np.NaN)
                        if np.any(nfilt):
                            CXD["AF"][nfilt] = (afu[nfilt] * nafu[nfilt] + afl[nfilt] * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt])
                            CXD["AFEB"][nfilt] = np.sqrt((np.power(afueb[nfilt], 2.0) * nafu[nfilt] + np.power(afleb[nfilt], 2.0) * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt]))
                            CXD["AFEM"][nfilt] = np.sqrt((np.power(afuem[nfilt], 2.0) * nafu[nfilt] + np.power(aflem[nfilt], 2.0) * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt]))
                        CXD["RMAF"] = RD["X_"+m[tags[kk]+'/angf']] if CXD["AF"] is not None else None
                        if CXD["RMAF"] is None and CXD["RMTI"] is not None:
                            CXD["RMAF"] = CXD["RMTI"].copy()
                        navgmap = None
                        if navgumap is not None:
                            navgmap = navgumap if navgmap is None else navgmap + navgumap
                        if navglmap is not None:
                            navgmap = navglmap if navgmap is None else navgmap + navglmap
                        CXD["NAF"] = np.count_nonzero(navgmap, axis=0).flatten() if navgmap is not None else np.zeros(afc.shape, dtype=np.int64)
                        if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" in CXD and CXD["ZTI"] is not None:
                            CXD["RAF"] = CXD["RTI"].copy()
                            CXD["ZAF"] = CXD["ZTI"].copy()
                            if "RMTI" in CXD and CXD["RMTI"] is not None:
                                CXD["RMAF"] = CXD["RMTI"].copy()
                                afprov = afprov + "; " + RD["I_"+m[tags[kk]+'/rt']]
                        # Angular frequency estimates in inner core typically off due to high measurement noise
                        rfilt = np.invert(np.all([CXD["RMAF"] >= 2.75, CXD["RMAF"] <= 3.25], axis=0))
                        if not np.all(rfilt) and not pureflag:
                            CXD["AF"] = CXD["AF"][rfilt]
                            CXD["AFEB"] = CXD["AFEB"][rfilt]
                            CXD["AFEM"] = CXD["AFEM"][rfilt]
                            CXD["RMAF"] = CXD["RMAF"][rfilt]
                            CXD["NAF"] = CXD["NAF"][rfilt]
                            if "RAF" in CXD and CXD["RAF"] is not None:
                                CXD["RAF"] = CXD["RAF"][rfilt]
                            if "ZAF" in CXD and CXD["ZAF"] is not None:
                                CXD["ZAF"] = CXD["ZAF"][rfilt]
                        CXD["AFDS"] = afprov
                    # Process impurity density data if present
                    if tags[kk]+'/nz' in m and RD[m[tags[kk]+'/nz']] is not None:
                        vals = RD[m[tags[kk]+'/nz']]
                        errs = RD[m[tags[kk]+'/nze']] if tags[kk]+'/nze' in m and RD[m[tags[kk]+'/nze']] is not None else None
                        niprov = RD["I_"+m[tags[kk]+'/nz']] + "; " + RD["I_"+m[tags[kk]+'/nze']] if errs is not None else RD["I_"+m[tags[kk]+'/nz']]
                        if vals is not None and tmlist is not None:
                            tmask = ptools.create_range_mask(RD["T_"+m[tags[kk]+'/nz']], tmlist)
                            if tmask is not None:
                                vals = vals[tmask]
                                if errs is not None:
                                    errs = errs[tmask]
                        (CXD["NIMP"], CXD["NIMPEB"], CXD["NIMPEM"], navgmap) = ptools.filtered_average(vals, value_errors=errs, lower_bounds=1.0e10, use_n=True)
                        CXD["RMNIMP"] = RD["X_"+m[tags[kk]+'/nz']] if CXD["NIMP"] is not None else None
                        if CXD["RMNIMP"] is None and CXD["RMTI"] is not None:
                            CXD["RMNIMP"] = CXD["RMTI"].copy()
                        navgmap = np.atleast_2d(navgmap)
                        CXD["NNIMP"] = np.count_nonzero(navgmap, axis=0).flatten()
                        if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" in CXD and CXD["ZTI"] is not None:
                            CXD["RNIMP"] = CXD["RTI"].copy()
                            CXD["ZNIMP"] = CXD["ZTI"].copy()
                            if "RMTI" in CXD and CXD["RMTI"] is not None:
                                CXD["RMNIMP"] = CXD["RMTI"].copy()
                                niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/rt']]
                        CXD["NIMPDS"] = niprov
                    CXD["A"] = None
                    CXD["Z"] = None
                    CXD["MAT"] = None
                    if tags[kk]+'/z' in m and RD[m[tags[kk]+'/z']] is not None:
                        CXD["Z"] = float(np.mean(RD[m[tags[kk]+'/z']]))
                        CXD["A"] = float(np.mean(RD[m[tags[kk]+'/mass']])) if tags[kk]+'/mass' in m and RD[m[tags[kk]+'/mass']] is not None else None
                        CXD["ZDS"] = RD["I_"+m[tags[kk]+'/z']]
                        CXD["ADS"] = RD["I_"+m[tags[kk]+'/mass']]
                    if CXD["A"] is None or CXD["Z"] is None or (CXD["Z"] is not None and CXD["Z"] < 0.9):
                        CXD["A"] = 12.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 8.0
                        CXD["Z"] = 6.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 4.0
                    if CXD["Z"] is not None:
                        (eimp, aimp, zimp) = ptools.define_ion_species(z=CXD["Z"], a=CXD["A"])
                        CXD["MAT"] = eimp
                    if not isinstance(CXD["A"], np.ndarray):
                        CXD["A"] = np.array([CXD["A"]]).flatten()
                    if not isinstance(CXD["Z"], np.ndarray):
                        CXD["Z"] = np.array([CXD["Z"]]).flatten()
                    if CX is not None and CXD:
                        CX[tags[kk].upper()] = CXD

                # Edge CX PPFs have different field names than the core CX PPFs
                elif re.match('cx7[abcd]', tags[kk]):      # Errors assumed to be given as 1 sigma in CX7A-D DDAs
                    vals = RD[m[tags[kk]+'/ti']]
                    lerrs = RD[m[tags[kk]+'/tilo']] if tags[kk]+'/tilo' in m and RD[m[tags[kk]+'/tilo']] is not None else None
                    uerrs = RD[m[tags[kk]+'/tihi']] if tags[kk]+'/tihi' in m and RD[m[tags[kk]+'/tihi']] is not None else None
                    tiprov = RD["I_"+m[tags[kk]+'/ti']]
                    if lerrs is not None:
                        tiprov = tiprov + "; " + RD["I_"+m[tags[kk]+'/tilo']]
                    if uerrs is not None:
                        tiprov = tiprov + "; " + RD["I_"+m[tags[kk]+'/tihi']]
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(RD["T_"+m[tags[kk]+'/ti']], tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                            if lerrs is not None:
                                lerrs = lerrs[tmask]
                            if uerrs is not None:
                                uerrs = uerrs[tmask]
                    (CXD["TI"], CXD["TIEB"], CXD["TIEM"], navgmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, lower_bounds=1.0e1, upper_bounds=1.0e7, use_n=True)
                    CXD["RMTI"] = RD["X_"+m[tags[kk]+'/ti']] if CXD["TI"] is not None else None
                    navgmap = np.atleast_2d(navgmap)
                    CXD["NTI"] = np.count_nonzero(navgmap, axis=0).flatten()
                    if tags[kk]+'/rave' in m and RD[m[tags[kk]+'/rave']] is not None:
                        CXD["RTI"] = RD[m[tags[kk]+'/rave']].copy()
                        CXD["ZTI"] = np.full(CXD["RTI"].shape, PD["ZMAG"]) if "ZMAG" in PD else np.zeros(CXD["RTI"].shape)
                        if CXD["RMTI"] is None:
                            CXD["RMTI"] = CXD["RTI"].copy()
                    if "USERZFLAG" in PD and PD["USERZFLAG"] and "RTI" in CXD and "ZTI" in CXD and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                        pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], CXD["RTI"], CXD["ZTI"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                        outerflag = True if CXD["RTI"][0] > CXD["RTI"][-1] else False
                        CXD["RMTI"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                        tiprov = tiprov + "; " + RD["I_"+m[tags[kk]+'/rave']]
                    if CXD["TIEB"] is not None:
                        zfilt = (CXD["TIEB"] < 1.0)
                        if np.any(zfilt):
                            CXD["TIEB"][zfilt] = 0.2 * CXD["TI"][zfilt]
                    CXD["TIDS"] = tiprov
                    # Process toroidal rotation data if present
                    if tags[kk]+'/angf' in m and RD[m[tags[kk]+'/angf']] is not None:
                        vals = RD[m[tags[kk]+'/angf']]
                        lerrs = RD[m[tags[kk]+'/aflo']] if tags[kk]+'/aflo' in m and RD[m[tags[kk]+'/aflo']] is not None else None
                        uerrs = RD[m[tags[kk]+'/afhi']] if tags[kk]+'/afhi' in m and RD[m[tags[kk]+'/afhi']] is not None else None
                        afprov = RD["I_"+m[tags[kk]+'/angf']]
                        if lerrs is not None:
                            afprov = afprov + "; " + RD["I_"+m[tags[kk]+'/aflo']]
                        if uerrs is not None:
                            afprov = afprov + "; " + RD["I_"+m[tags[kk]+'/afhi']]
                        if vals is not None and tmlist is not None:
                            tmask = ptools.create_range_mask(RD["T_"+m[tags[kk]+'/angf']], tmlist)
                            if tmask is not None:
                                vals = vals[tmask]
                                if lerrs is not None:
                                    lerrs = lerrs[tmask]
                                if uerrs is not None:
                                    uerrs = uerrs[tmask]
                        (afc, afceb, afcem, navgcmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, use_n=True)
                        (afu, afueb, afuem, navgumap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, lower_bounds=1.0e-5, use_n=True)
                        (afl, afleb, aflem, navglmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, upper_bounds=-1.0e-5, use_n=True)
                        nafu = np.zeros(afc.shape).flatten()
                        nafl = np.zeros(afc.shape).flatten()
                        ufilt = np.full(afc.shape, True)
                        lfilt = np.full(afc.shape, True)
                        if afu is not None:
                            if afu.size > 1:
                                navgumap = np.atleast_2d(navgumap)
                            nafu = np.count_nonzero(navgumap, axis=0).flatten()
                            ufilt = np.invert(np.isfinite(afu))
                            afu[ufilt] = 0.0
                            afueb[ufilt] = 0.0
                            afuem[ufilt] = 0.0
                        else:
                            afu = np.zeros(afc.shape)
                            afueb = np.zeros(afc.shape)
                            afuem = np.zeros(afc.shape)
                        if afl is not None:
                            if afl.size > 1:
                                navglmap = np.atleast_2d(navglmap)
                            nafl = np.count_nonzero(navglmap, axis=0).flatten()
                            lfilt = np.invert(np.isfinite(afl))
                            afl[lfilt] = 0.0
                            afleb[lfilt] = 0.0
                            aflem[lfilt] = 0.0
                        else:
                            afl = np.zeros(afc.shape)
                            afleb = np.zeros(afc.shape)
                            aflem = np.zeros(afc.shape)
                        ffilt = np.invert(np.all([ufilt, lfilt], axis=0))
                        nfilt = np.all([ffilt, (nafu + nafl) > 0], axis=0)
                        CXD["AF"] = np.full(nfilt.shape, np.NaN)
                        CXD["AFEB"] = np.full(nfilt.shape, np.NaN)
                        CXD["AFEM"] = np.full(nfilt.shape, np.NaN)
                        if np.any(nfilt):
                            CXD["AF"][nfilt] = (afu[nfilt] * nafu[nfilt] + afl[nfilt] * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt])
                            CXD["AFEB"][nfilt] = np.sqrt((np.power(afueb[nfilt], 2.0) * nafu[nfilt] + np.power(afleb[nfilt], 2.0) * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt]))
                            CXD["AFEM"][nfilt] = np.sqrt((np.power(afuem[nfilt], 2.0) * nafu[nfilt] + np.power(aflem[nfilt], 2.0) * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt]))
                        CXD["RMAF"] = RD["X_"+m[tags[kk]+'/angf']] if CXD["AF"] is not None else None
                        if CXD["RMAF"] is None and CXD["RMTI"] is not None:
                            CXD["RMAF"] = CXD["RMTI"].copy()
                        navgmap = None
                        if navgumap is not None:
                            navgmap = navgumap if navgmap is None else navgmap + navgumap
                        if navglmap is not None:
                            navgmap = navglmap if navgmap is None else navgmap + navglmap
                        CXD["NAF"] = np.count_nonzero(navgmap, axis=0).flatten() if navgmap is not None else np.zeros(afc.shape, dtype=np.int64)
                        if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" in CXD and CXD["ZTI"] is not None:
                            CXD["RAF"] = CXD["RTI"].copy()
                            CXD["ZAF"] = CXD["ZTI"].copy()
                            if "RMTI" in CXD and CXD["RMTI"] is not None:
                                CXD["RMAF"] = CXD["RMTI"].copy()
                                afprov = afprov + "; " + RD["I_"+m[tags[kk]+'/rave']]
                        rfilt = np.invert(np.all([CXD["RMAF"] >= 2.75, CXD["RMAF"] <= 3.25], axis=0))
                        # Angular frequency estimates in inner core typically off due to high measurement noise
                        if not np.all(rfilt) and not pureflag:
                            CXD["AF"] = CXD["AF"][rfilt]
                            CXD["AFEB"] = CXD["AFEB"][rfilt]
                            CXD["AFEM"] = CXD["AFEM"][rfilt]
                            CXD["RMAF"] = CXD["RMAF"][rfilt]
                            CXD["NAF"] = CXD["NAF"][rfilt]
                            if "RAF" in CXD and CXD["RAF"] is not None:
                                CXD["RAF"] = CXD["RAF"][rfilt]
                            if "ZAF" in CXD and CXD["ZAF"] is not None:
                                CXD["ZAF"] = CXD["ZAF"][rfilt]
                        CXD["AFDS"] = afprov
                    # Process impurity density data if present
                    if tags[kk]+'/dens' in m and RD[m[tags[kk]+'/dens']] is not None and not f[tags[kk]+'/dens'][0]:
                        vals = RD[m[tags[kk]+'/dens']]
                        relerrs = RD[m[tags[kk]+'/conc']] if tags[kk]+'/conc' in m and RD[m[tags[kk]+'/conc']] is not None else None
                        rellerrs = RD[m[tags[kk]+'/colo']] if tags[kk]+'/colo' in m and RD[m[tags[kk]+'/colo']] is not None else None
                        reluerrs = RD[m[tags[kk]+'/cohi']] if tags[kk]+'/cohi' in m and RD[m[tags[kk]+'/cohi']] is not None else None
                        if relerrs is not None:
                            relerrs[np.abs(relerrs) < 1.0e-6] = 1.0
                        lerrs = vals * rellerrs / relerrs if relerrs is not None and rellerrs is not None else None
                        uerrs = vals * reluerrs / relerrs if relerrs is not None and reluerrs is not None else None
                        niprov = RD["I_"+m[tags[kk]+'/dens']]
                        if relerrs is not None:
                            niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/conc']]
                        if rellerrs is not None:
                            niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/colo']]
                        if reluerrs is not None:
                            niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/cohi']]
                        if vals is not None and tmlist is not None:
                            tmask = ptools.create_range_mask(RD["T_"+m[tags[kk]+'/dens']], tmlist)
                            if tmask is not None:
                                vals = vals[tmask]
                                if lerrs is not None:
                                    lerrs = lerrs[tmask]
                                if uerrs is not None:
                                    uerrs = uerrs[tmask]
                        (CXD["NIMP"], CXD["NIMPEB"], CXD["NIMPEM"], navgmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, lower_bounds=1.0e10, use_n=True)
                        CXD["RMNIMP"] = RD["X_"+m[tags[kk]+'/dens']] if CXD["NIMP"] is not None else None
                        if CXD["RMNIMP"] is None and CXD["RMTI"] is not None:
                            CXD["RMNIMP"] = CXD["RMTI"].copy()
                        navgmap = np.atleast_2d(navgmap)
                        CXD["NNIMP"] = np.count_nonzero(navgmap, axis=0).flatten()
                        if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" in CXD and CXD["ZTI"] is not None:
                            CXD["RNIMP"] = CXD["RTI"].copy()
                            CXD["ZNIMP"] = CXD["ZTI"].copy()
                            if "RMTI" in CXD and CXD["RMTI"] is not None:
                                CXD["RMNIMP"] = CXD["RMTI"].copy()
                                niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/rave']]
                        CXD["NIMPDS"] = niprov
                    CXD["A"] = None
                    CXD["Z"] = None
                    CXD["MAT"] = None
                    if tags[kk]+'/z' in m and RD[m[tags[kk]+'/z']] is not None:
                        CXD["Z"] = float(np.mean(RD[m[tags[kk]+'/z']]))
                        CXD["A"] = float(np.mean(RD[m[tags[kk]+'/mass']])) if tags[kk]+'/mass' in m and RD[m[tags[kk]+'/mass']] is not None else None
                        CXD["ZDS"] = RD["I_"+m[tags[kk]+'/z']]
                        CXD["ADS"] = RD["I_"+m[tags[kk]+'/mass']]
                    if CXD["A"] is None or CXD["Z"] is None or (CXD["Z"] is not None and CXD["Z"] < 0.9):
                        CXD["A"] = 12.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 8.0
                        CXD["Z"] = 6.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 4.0
                    if CXD["Z"] is not None:
                        (eimp, aimp, zimp) = ptools.define_ion_species(z=CXD["Z"], a=CXD["A"])
                        CXD["MAT"] = eimp
                    if not isinstance(CXD["A"], np.ndarray):
                        CXD["A"] = np.array([CXD["A"]]).flatten()
                    if not isinstance(CXD["Z"], np.ndarray):
                        CXD["Z"] = np.array([CXD["Z"]]).flatten()
                    if CX is not None and CXD:
                        CX[tags[kk].upper()] = CXD

                # All other CX PPFs have the same field structure
                else:      # Errors assumed to be given as 1 sigma in all other CX DDAs
                    vals = RD[m[tags[kk]+'/ti']]
                    lerrs = RD[m[tags[kk]+'/tilo']] if tags[kk]+'/tilo' in m and RD[m[tags[kk]+'/tilo']] is not None else None
                    uerrs = RD[m[tags[kk]+'/tihi']] if tags[kk]+'/tihi' in m and RD[m[tags[kk]+'/tihi']] is not None else None
                    tiprov = RD["I_"+m[tags[kk]+'/ti']]
                    if lerrs is not None:
                        tiprov = tiprov + "; " + RD["I_"+m[tags[kk]+'/tilo']]
                    if uerrs is not None:
                        tiprov = tiprov + "; " + RD["I_"+m[tags[kk]+'/tihi']]
                    (CXD["TI"], CXD["TIEB"], CXD["TIEM"], navgmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, lower_bounds=1.0e1, upper_bounds=1.0e7, use_n=True)
                    CXD["RTI"] = RD["X_"+m[tags[kk]+'/ti']] if CXD["TI"] is not None else None
                    navgmap = np.atleast_2d(navgmap)
                    CXD["NTI"] = np.count_nonzero(navgmap, axis=0).flatten()
                    rprov = None
                    if tags[kk]+'/rcor' in m and RD[m[tags[kk]+'/rcor']] is not None:
                        CXD["RMTI"] = RD[m[tags[kk]+'/rcor']].copy()
                        CXD["RTI"] = CXD["RMTI"].copy()
                    if tags[kk]+'/rpos' in m and RD[m[tags[kk]+'/rpos']] is not None:
                        CXD["RTI"] = RD[m[tags[kk]+'/rpos']].copy()
                        rprov = rprov + "; " + RD["I_"+m[tags[kk]+'/rpos']] if rprov is not None else RD["I_"+m[tags[kk]+'/rpos']]
                    elif tags[kk]+'/rcor' in m and RD[m[tags[kk]+'/rcor']] is not None:
                        rprov = rprov + "; " + RD["I_"+m[tags[kk]+'/rcor']] if rprov is not None else RD["I_"+m[tags[kk]+'/rcor']]
                    if tags[kk]+'/pos' in m and RD[m[tags[kk]+'/pos']] is not None:
                        CXD["ZTI"] = RD[m[tags[kk]+'/pos']].copy()
                        rprov = rprov + "; " + RD["I_"+m[tags[kk]+'/pos']] if rprov is not None else RD["I_"+m[tags[kk]+'/pos']]
                    if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" not in CXD:
                        CXD["ZTI"] = np.full(CXD["RTI"].shape, PD["ZMAG"]) if "ZMAG" in PD else np.zeros(CXD["RTI"].shape)
                        if CXD["RMTI"] is None:
                            CXD["RMTI"] = CXD["RTI"].copy()
                    if "USERZFLAG" in PD and PD["USERZFLAG"] and "RTI" in CXD and "ZTI" in CXD and "PFXMAP" in PD and PD["PFXMAP"] is not None:
                        pfntemp = ftools.rz2pf(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], CXD["RTI"], CXD["ZTI"], axsval=float(PD["FAXS"][0]), bndval=float(PD["FBND"][0]), style=1)
                        outerflag = True if CXD["RTI"][0] > CXD["RTI"][-1] else False
                        CXD["RMTI"] = find_midplane_radius(PD, pfntemp, outer_first=outerflag)
                    if rprov is not None:
                        tiprov = tiprov + "; " + rprov
                    if CXD["TIEB"] is not None:
                        zfilt = (CXD["TIEB"] < 1.0)
                        if np.any(zfilt):
                            CXD["TIEB"][zfilt] = 0.2 * CXD["TI"][zfilt]
                    CXD["TIDS"] = tiprov
                    # Process toroidal rotation data if present
                    if tags[kk]+'/angf' in m and RD[m[tags[kk]+'/angf']] is not None:
                        vals = RD[m[tags[kk]+'/angf']]
                        lerrs = RD[m[tags[kk]+'/aflo']] if tags[kk]+'/aflo' in m and RD[m[tags[kk]+'/aflo']] is not None else None
                        uerrs = RD[m[tags[kk]+'/afhi']] if tags[kk]+'/afhi' in m and RD[m[tags[kk]+'/afhi']] is not None else None
                        afprov = RD["I_"+m[tags[kk]+'/angf']]
                        if lerrs is not None:
                            afprov = afprov + "; " + RD["I_"+m[tags[kk]+'/aflo']]
                        if uerrs is not None:
                            afprov = afprov + "; " + RD["I_"+m[tags[kk]+'/afhi']]
                        (afc, afceb, afcem, navgcmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, use_n=True)
                        (afu, afueb, afuem, navgumap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, lower_bounds=1.0e-5, use_n=True)
                        (afl, afleb, aflem, navglmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, upper_bounds=-1.0e-5, use_n=True)
                        nafu = np.zeros(afc.shape, dtype=np.int64).flatten()
                        nafl = np.zeros(afc.shape, dtype=np.int64).flatten()
                        ufilt = np.full(afc.shape, True)
                        lfilt = np.full(afc.shape, True)
                        if afu is not None:
                            if afu.size > 1:
                                navgumap = np.atleast_2d(navgumap)
                            nafu = np.count_nonzero(navgumap, axis=0).flatten()
                            ufilt = np.invert(np.isfinite(afu))
                            afu[ufilt] = 0.0
                            afueb[ufilt] = 0.0
                            afuem[ufilt] = 0.0
                        else:
                            afu = np.zeros(afc.shape)
                            afueb = np.zeros(afc.shape)
                            afuem = np.zeros(afc.shape)
                        if afl is not None:
                            if afl.size > 1:
                                navglmap = np.atleast_2d(navglmap)
                            nafl = np.count_nonzero(navglmap, axis=0).flatten()
                            lfilt = np.invert(np.isfinite(afl))
                            afl[lfilt] = 0.0
                            afleb[lfilt] = 0.0
                            aflem[lfilt] = 0.0
                        else:
                            afl = np.zeros(afc.shape)
                            afleb = np.zeros(afc.shape)
                            aflem = np.zeros(afc.shape)
                        ffilt = np.invert(np.all([ufilt, lfilt], axis=0))
                        nfilt = np.all([ffilt, (nafu + nafl) > 0], axis=0)
                        CXD["AF"] = np.full(nfilt.shape, np.NaN)
                        CXD["AFEB"] = np.full(nfilt.shape, np.NaN)
                        CXD["AFEM"] = np.full(nfilt.shape, np.NaN)
                        if np.any(nfilt):
                            CXD["AF"][nfilt] = (afu[nfilt] * nafu[nfilt] + afl[nfilt] * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt])
                            CXD["AFEB"][nfilt] = np.sqrt((np.power(afueb[nfilt], 2.0) * nafu[nfilt] + np.power(afleb[nfilt], 2.0) * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt]))
                            CXD["AFEM"][nfilt] = np.sqrt((np.power(afuem[nfilt], 2.0) * nafu[nfilt] + np.power(aflem[nfilt], 2.0) * nafl[nfilt]) / (nafu[nfilt] + nafl[nfilt]))
                        CXD["RMAF"] = RD["X_"+m[tags[kk]+'/angf']] if CXD["AF"] is not None else None
                        if CXD["RMAF"] is None and CXD["RMTI"] is not None:
                            CXD["RMAF"] = CXD["RMTI"].copy()
                        navgmap = None
                        if navgumap is not None:
                            navgmap = navgumap if navgmap is None else navgmap + navgumap
                        if navglmap is not None:
                            navgmap = navglmap if navgmap is None else navgmap + navglmap
                        CXD["NAF"] = np.count_nonzero(navgmap, axis=0).flatten() if navgmap is not None else np.zeros(afc.shape, dtype=np.int64)
                        if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" in CXD and CXD["ZTI"] is not None:
                            CXD["RAF"] = CXD["RTI"].copy()
                            CXD["ZAF"] = CXD["ZTI"].copy()
                            if "RMTI" in CXD and CXD["RMTI"] is not None:
                                CXD["RMAF"] = CXD["RMTI"].copy()
                        if rprov is not None:
                            afprov = afprov + "; " + rprov
                        # Angular frequency estimates in inner core typically off due to high measurement noise
                        rfilt = np.invert(np.all([CXD["RMAF"] >= 2.75, CXD["RMAF"] <= 3.25], axis=0))
                        if not np.all(rfilt) and not pureflag:
                            CXD["AF"] = CXD["AF"][rfilt]
                            CXD["AFEB"] = CXD["AFEB"][rfilt]
                            CXD["AFEM"] = CXD["AFEM"][rfilt]
                            CXD["RMAF"] = CXD["RMAF"][rfilt]
                            CXD["NAF"] = CXD["NAF"][rfilt]
                            if "RAF" in CXD and CXD["RAF"] is not None:
                                CXD["RAF"] = CXD["RAF"][rfilt]
                            if "ZAF" in CXD and CXD["ZAF"] is not None:
                                CXD["ZAF"] = CXD["ZAF"][rfilt]
                        CXD["AFDS"] = afprov
                    # Process impurity density data if present
                    if tags[kk]+'/dens' in m and RD[m[tags[kk]+'/dens']] is not None and not f[tags[kk]+'/dens'][0]:
                        vals = RD[m[tags[kk]+'/dens']]
                        relerrs = RD[m[tags[kk]+'/conc']] if tags[kk]+'/conc' in m and RD[m[tags[kk]+'/conc']] is not None else None
                        rellerrs = RD[m[tags[kk]+'/colo']] if tags[kk]+'/colo' in m and RD[m[tags[kk]+'/colo']] is not None else None
                        reluerrs = RD[m[tags[kk]+'/cohi']] if tags[kk]+'/cohi' in m and RD[m[tags[kk]+'/cohi']] is not None else None
                        if relerrs is not None:
                            relerrs[np.abs(relerrs) < 1.0e-6] = 1.0
                        lerrs = vals * rellerrs / relerrs if relerrs is not None and rellerrs is not None else None
                        uerrs = vals * reluerrs / relerrs if relerrs is not None and reluerrs is not None else None
                        niprov = RD["I_"+m[tags[kk]+'/dens']]
                        if relerrs is not None:
                            niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/conc']]
                        if rellerrs is not None:
                            niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/colo']]
                        if reluerrs is not None:
                            niprov = niprov + "; " + RD["I_"+m[tags[kk]+'/cohi']]
                        (CXD["NIMP"], CXD["NIMPEB"], CXD["NIMPEM"], navgmap) = ptools.filtered_average(vals, lower_values=lerrs, upper_values=uerrs, lower_bounds=1.0e10, use_n=True)
                        CXD["RMNIMP"] = RD["X_"+m[tags[kk]+'/dens']] if CXD["NIMP"] is not None else None
                        if CXD["RMNIMP"] is None and CXD["RMTI"] is not None:
                            CXD["RMNIMP"] = CXD["RMTI"].copy()
                        navgmap = np.atleast_2d(navgmap)
                        CXD["NNIMP"] = np.count_nonzero(navgmap, axis=0).flatten()
                        if "RTI" in CXD and CXD["RTI"] is not None and "ZTI" in CXD and CXD["ZTI"] is not None:
                            CXD["RNIMP"] = CXD["RTI"].copy()
                            CXD["ZNIMP"] = CXD["ZTI"].copy()
                            if "RMTI" in CXD and CXD["RMTI"] is not None:
                                CXD["RMNIMP"] = CXD["RMTI"].copy()
                        if rprov is not None:
                            niprov = niprov + "; " + rprov
                        CXD["NIMPDS"] = niprov
                    CXD["A"] = None
                    CXD["Z"] = None
                    CXD["MAT"] = None
                    if tags[kk]+'/z' in m and RD[m[tags[kk]+'/z']] is not None:
                        CXD["Z"] = float(np.mean(RD[m[tags[kk]+'/z']]))
                        CXD["A"] = float(np.mean(RD[m[tags[kk]+'/mass']])) if tags[kk]+'/mass' in m and RD[m[tags[kk]+'/mass']] is not None else None
                        CXD["ZDS"] = RD["I_"+m[tags[kk]+'/z']]
                        CXD["ADS"] = RD["I_"+m[tags[kk]+'/mass']]
                    if CXD["A"] is None or CXD["Z"] is None:
                        if re.match(r'.*m$', tags[kk], flags=re.IGNORECASE):
                            CXD["A"] = 12.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 8.0
                            CXD["Z"] = 6.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 4.0
                        elif re.match(r'.*4$', tags[kk], flags=re.IGNORECASE):
                            CXD["A"] = 8.0
                            CXD["Z"] = 4.0
                        elif re.match(r'.*6$', tags[kk], flags=re.IGNORECASE):
                            CXD["A"] = 20.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 8.0
                            CXD["Z"] = 10.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 4.0
                        elif re.match(r'.*8$', tags[kk], flags=re.IGNORECASE):
                            CXD["A"] = 40.0
                            CXD["Z"] = 18.0
                    if CXD["A"] is None or CXD["Z"] is None or (CXD["Z"] is not None and CXD["Z"] < 0.9):
                        CXD["A"] = 12.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 8.0
                        CXD["Z"] = 6.0 if "SHOT" in RD and int(RD["SHOT"]) <= PD["ILWSTART"] else 4.0
                    if CXD["Z"] is not None:
                        (eimp, aimp, zimp) = ptools.define_ion_species(z=CXD["Z"], a=CXD["A"])
                        CXD["MAT"] = eimp
                    if not isinstance(CXD["A"], np.ndarray):
                        CXD["A"] = np.array([CXD["A"]]).flatten()
                    if not isinstance(CXD["Z"], np.ndarray):
                        CXD["Z"] = np.array([CXD["Z"]]).flatten()
                    if CX is not None and CXD:
                        CX[tags[kk].upper()] = CXD

        PD["CX"] = CX

        if fdebug and PD["CX"] is not None:
            cxstr = ""
            for key in PD["CX"]:
                cxstr = cxstr + ", " + key.upper() if cxstr else key.upper()
            if cxstr:
                print("process_jet.py: unpack_profile_data(): %s charge exchange data added." % (cxstr))

        # Magnetic field reconstruction from array of magnetic sensors at JET - safety factor
        #   Maybe this field is unnecessary, there is a field called SQ in EFIT which estimates shear, but not in EFTM
        MAGN = None
        if eqdda is not None and "Q"+eqdda in PD and PD["Q"+eqdda] is not None:
            MAGN = dict()
            MAGN["Q"] = PD["Q"+eqdda].copy()
            MAGN["QEB"] = PD["QEB"+eqdda].copy()
            MAGN["QEM"] = PD["QEM"+eqdda].copy()
            MAGN["PFNQ"] = PD["QPFN"+eqdda].copy()
            MAGN["NQ"] = PD["NQ"+eqdda].copy()
            MAGN["EQTYPE"] = eqdda
            MAGN["QDS"] = PD["Q"+eqdda+"DS"]
        else:
            vals = None
            for ii in range(len(eqlist)):
                if vals is None:
                    MAGN = dict()
                    vals = RD[m[eqlist[ii]+'/q']] if eqlist[ii]+'/q' in m else None
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(RD["T_"+m[eqlist[ii]+'/q']], tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    verrs = None
                    (MAGN["Q"], MAGN["QEB"], MAGN["QEM"], navgmap) = ptools.filtered_average(vals, value_errors=verrs, use_n=True)
                    MAGN["PFNQ"] = RD["X_"+m[eqlist[ii]+'/q']] if vals is not None else None
                    navgmap = np.atleast_2d(navgmap)
                    MAGN["NQ"] = np.count_nonzero(navgmap, axis=0).flatten() if vals is not None else None
                    MAGN["EQTYPE"] = eqlist[ii].upper() if vals is not None else None
                    MAGN["QDS"] = RD["I_"+m[eqlist[ii]+'/q']]
        PD["MAGN"] = MAGN

        if fdebug and PD["MAGN"] is not None:
            print("process_jet.py: unpack_profile_data(): %s safety factor data added." % (eqdda.upper()))

        # Effective charge profile and impurity profile estimation using soft X-ray diagnostics
        WSXP = None
        if 'wsxp/zeff' in m and RD[m['wsxp/zeff']] is not None:
            WSXP = dict()
            vals = RD[m['wsxp/zeff']]                                  # Effective charge profile
            if vals is not None and tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['wsxp/zeff']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (WSXP["ZEFF"], WSXP["ZEFFEB"], WSXP["ZEFFEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=0.98, use_n=True)
            WSXP["PFNZEFF"] = RD["X_"+m['wsxp/zeff']].copy()
            navgmap = np.atleast_2d(navgmap)
            WSXP["NZEFF"] = np.count_nonzero(navgmap, axis=0).flatten()
            WSXP["ZEFFDS"] = RD["I_"+m['wsxp/zeff']]

            # Light impurity
            if 'wsxp/lzav' in m and RD[m['wsxp/lzav']] is not None:
                vals = RD[m['wsxp/lzav']]
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['wsxp/lzav']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (WSXP["NIMPL"], WSXP["NIMPLEB"], WSXP["NIMPLEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=1.0e15, use_n=True)
                WSXP["PFNNIMPL"] = RD["X_"+m['wsxp/lzav']].copy()
                navgmap = np.atleast_2d(navgmap)
                WSXP["NNIMPL"] = np.count_nonzero(navgmap, axis=0).flatten()
                WSXP["NIMPLDS"] = RD["I_"+m['wsxp/lzav']]
                # Typically large variations in light impurity profiles over time, perhaps due to Z-effective matching in routine?
                #if WSXP["NIMPL"] is not None and WSXP["NIMPLEB"] is not None:
                #    error_limit = 0.15     # This 15% is arbitrary
                #    efilt = (WSXP["NIMPLEB"] > error_limit * WSXP["NIMPL"])
                #    if np.any(efilt):
                #        WSXP["NIMPLEB"][efilt] = error_limit * WSXP["NIMPL"]
                element = "Be" if PD["SHOT"] >= PD["ILWSTART"] else "C"            # Default value for light impurity
                eprov = "EX2GK: Internal assumption"
                if 'wsxp/lzel' in m and RD[m['wsxp/lzel']] is not None:
                    desc = RD[m['wsxp/lzel']]
                    ddd = re.match(r'^.+:\s*([^\s]+)\s*$', desc, flags=re.IGNORECASE)
                    if ddd and ddd.group(1):
                        element = ddd.group(1).strip()
                        eprov = RD["I_"+m['wsxp/lzel']]
                if isinstance(element, str):
                    (WSXP["MATIMPL"], afull, zfull) = ptools.define_ion_species(short_name=element)
                    WSXP["AIMPL"] = np.array([afull])
                    WSXP["AIMPLEB"] = np.array([0.0])
                    WSXP["ZIMPL"] = np.array([zfull])
                    WSXP["ZIMPLEB"] = np.array([0.0])
                    WSXP["AIMPLDS"] = eprov
                    WSXP["ZIMPLDS"] = eprov
                if 'wsxp/melz' in m and RD[m['wsxp/melz']] is not None:
                    vals = RD[m['wsxp/melz']]
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(RD["T_"+m['wsxp/melz']], tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (WSXP["ZIMPL"], WSXP["ZIMPLEB"], dummy, navgmap) = ptools.filtered_average(vals, use_n=True)
                    WSXP["ZIMPLDS"] = RD["I_"+m['wsxp/melz']]

            # Mid-range impurity
            if 'wsxp/ozav' in m and RD[m['wsxp/ozav']] is not None:
                vals = RD[m['wsxp/ozav']]
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['wsxp/ozav']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (WSXP["NIMPM"], WSXP["NIMPMEB"], WSXP["NIMPMEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=1.0e13, use_n=True)
                WSXP["PFNNIMPM"] = RD["X_"+m['wsxp/ozav']].copy()
                navgmap = np.atleast_2d(navgmap)
                WSXP["NNIMPM"] = np.count_nonzero(navgmap, axis=0).flatten()
                WSXP["NIMPMDS"] = RD["I_"+m['wsxp/ozav']]
                element = "Ni" if PD["SHOT"] >= PD["ILWSTART"] else "Ne"           # Default value for medium impurity
                eprov = "EX2GK: Internal assumption"
                if 'wsxp/ozel' in m and RD[m['wsxp/ozel']] is not None:
                    desc = RD[m['wsxp/ozel']]
                    ddd = re.match(r'^.+:\s*([^\s]+)\s*$', desc, flags=re.IGNORECASE)
                    if ddd and ddd.group(1):
                        element = ddd.group(1).strip()
                        eprov = RD["I_"+m['wsxp/ozel']]
                if isinstance(element, str):
                    (WSXP["MATIMPM"], afull, zfull) = ptools.define_ion_species(short_name=element)
                    WSXP["AIMPM"] = np.array([afull])
                    WSXP["AIMPMEB"] = np.array([0.0])
                    WSXP["ZIMPM"] = np.array([zfull])
                    WSXP["ZIMPMEB"] = np.array([0.0])
                    WSXP["AIMPMDS"] = eprov
                    WSXP["ZIMPMDS"] = eprov
                if 'wsxp/meoz' in m and RD[m['wsxp/meoz']] is not None:
                    vals = RD[m['wsxp/meoz']]
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(RD["T_"+m['wsxp/meoz']], tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (WSXP["ZIMPM"], WSXP["ZIMPMEB"], dummy, navgmap) = ptools.filtered_average(vals, use_n=True)
                    WSXP["ZIMPMDS"] = RD["I_"+m['wsxp/meoz']]

            # Heavy impurity analysis
            if 'wsxp/hzav' in m and RD[m['wsxp/hzav']] is not None:
                vals = RD[m['wsxp/hzav']]
                if vals is not None and tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['wsxp/hzav']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (WSXP["NIMPH"], WSXP["NIMPHEB"], WSXP["NIMPHEM"], navgmap) = ptools.filtered_average(vals, percent_errors=0.05, lower_bounds=1.0e11, use_n=True)
                WSXP["PFNNIMPH"] = RD["X_"+m['wsxp/hzav']].copy()
                navgmap = np.atleast_2d(navgmap)
                WSXP["NNIMPH"] = np.count_nonzero(navgmap, axis=0).flatten()
                WSXP["NIMPHDS"] = RD["I_"+m['wsxp/hzav']]
                element = "W" if PD["SHOT"] >= PD["ILWSTART"] else "Cu"            # Default value for heavy impurity
                eprov = "EX2GK: Internal assumption"
                if 'wsxp/hzel' in m and RD[m['wsxp/hzel']] is not None:
                    desc = RD[m['wsxp/hzel']]
                    ddd = re.match(r'^.+:\s*([^\s]+)\s*$', desc, flags=re.IGNORECASE)
                    if ddd and ddd.group(1):
                        element = ddd.group(1).strip()
                        eprov = RD["I_"+m['wsxp/hzel']]
                if isinstance(element, str):
                    (WSXP["MATIMPH"], afull, zfull) = ptools.define_ion_species(short_name=element)
                    WSXP["AIMPH"] = np.array([afull])
                    WSXP["AIMPHEB"] = np.array([0.0])
                    WSXP["ZIMPH"] = np.array([zfull])
                    WSXP["ZIMPHEB"] = np.array([0.0])
                    WSXP["AIMPHDS"] = eprov
                    WSXP["ZIMPHDS"] = eprov
                if 'wsxp/mehz' in m and RD[m['wsxp/mehz']] is not None:
                    vals = RD[m['wsxp/mehz']]
                    if vals is not None and tmlist is not None:
                        tmask = ptools.create_range_mask(RD["T_"+m['wsxp/mehz']], tmlist)
                        if tmask is not None:
                            vals = vals[tmask]
                    (WSXP["ZIMPH"], WSXP["ZIMPHEB"], dummy, navgmap) = ptools.filtered_average(vals, use_n=True)
                    WSXP["ZIMPHDS"] = RD["I_"+m['wsxp/mehz']]

        PD["WSXP"] = WSXP

        if fdebug and PD["WSXP"] is not None:
            print("process_jet.py: unpack_profile_data(): %s soft X-ray tomographic impurity profile data added." % (eqdda.upper()))

    if fdebug:
        print("process_jet.py: unpack_profile_data() completed.")

    return PD


def unpack_source_data(rawdata, newstruct=None, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Unpacks the required power deposition profile data from the raw extracted 
    data container, pre-processes it with statistical averaging if necessary,
    and stores it in another data container.

    :arg rawdata: dict. Implementation-specific object containing raw extracted data.

    :kwarg newstruct: dict. Optional object in which processed data will be stored.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Implementation-specific object with processed power profile data inserted.
    """
    RD = None
    PD = None
    if isinstance(rawdata, dict):
        RD = rawdata
    if isinstance(newstruct, dict):
        PD = newstruct
    else:
        PD = dict()

    if RD is not None and "MAP" in RD and RD["MAP"] is not None:
        m = RD["MAP"]
        f = RD["FLAGS"] if "FLAGS" in RD else None

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqlist = PD["EQLIST"] if "EQLIST" in PD and PD["EQLIST"] else ['efit']
        eqdda = eqlist[0].upper() if len(eqlist) > 0 else None
        rmag = float(PD["RMAG"][0]) if "RMAG" in PD else 3.00

        # Specification of time ranges to mask for ELM filtering
        tmlist = PD["ELMMASK"] if "ELMMASK" in PD else None

        if fdebug:
            print("process_jet.py: unpack_source_data(): Setup completed.")

        # TRANSP sources (after transfer into PPF - upgraded) - these take precedence over all other calculations
        TRAU = None

        # Radial coordinate transformation for TRANSP - this is not steady in time so time-averaging may result in strangeness
#        trxvec = None
#        if 'trau/spnt' in m and RD[m['trau/spnt']] is not None:
#            if TRAU is None:
#                TRAU = dict()
#            trxvec = RD[m['trau/spnt']]

        # NBI heat source density profile to electrons - calculated by TRANSP
        if 'trau/pbe' in m and RD[m['trau/pbe']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/pbe']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/pbe']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["QENBI"], TRAU["QENBIEB"], TRAU["QENBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNQENBI"] = np.power(RD["X_"+m['trau/pbe']], 2.0)
            #TRAU["QENBIEB"] = TRAU["QENBIEB"] + 0.05 * TRAU["QENBI"]
            TRAU["QENBIDS"] = RD["I_"+m['trau/pbe']]

        # NBI heat source density profile to ions - calculated by TRANSP
        if 'trau/pbi' in m and RD[m['trau/pbi']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/pbi']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/pbi']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (qnb, qnbeb, qnbem, navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["QINBI"] = TRAU["QINBI"] + qnb if "QINBI" in TRAU and TRAU["QINBI"] is not None else qnb
            TRAU["QINBIEB"] = np.sqrt(np.power(TRAU["QINBIEB"], 2.0) + np.power(qnbeb, 2.0)) if "QINBIEB" in TRAU and TRAU["QINBIEB"] is not None else qnbeb
            TRAU["QINBIEM"] = np.sqrt(np.power(TRAU["QINBIEM"], 2.0) + np.power(qnbem, 2.0)) if "QINBIEM" in TRAU and TRAU["QINBIEM"] is not None else qnbem
            if "TFNQINBI" not in TRAU:
                TRAU["TFNQINBI"] = np.power(RD["X_"+m['trau/pbi']], 2.0)
            #TRAU["QINBIEB"] = TRAU["QINBIEB"] + 0.05 * TRAU["QINBI"]
            TRAU["QINBIDS"] = RD["I_"+m['trau/pbi']]
        if 'trau/pbth' in m and RD[m['trau/pbth']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/pbth']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/pbth']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (qnb, qnbeb, qnbem, navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["QINBI"] = TRAU["QINBI"] + qnb if "QINBI" in TRAU and TRAU["QINBI"] is not None else qnb
            TRAU["QINBIEB"] = np.sqrt(np.power(TRAU["QINBIEB"], 2.0) + np.power(qnbeb, 2.0)) if "QINBIEB" in TRAU and TRAU["QINBIEB"] is not None else qnbeb
            TRAU["QINBIEM"] = np.sqrt(np.power(TRAU["QINBIEM"], 2.0) + np.power(qnbem, 2.0)) if "QINBIEM" in TRAU and TRAU["QINBIEM"] is not None else qnbem
            if "TFNQINBI" not in TRAU:
                TRAU["TFNQINBI"] = np.power(RD["X_"+m['trau/pbth']], 2.0)
            #TRAU["QINBIEB"] = TRAU["QINBIEB"] + 0.05 * TRAU["QINBI"]
            TRAU["QINBIDS"] = RD["I_"+m['trau/pbth']]

        # NBI electron particle source density profile - calculated by TRANSP
#        if 'trau/sbe' in m and RD[m['trau/sbe']] is not None:
#            if TRAU is None:
#                TRAU = dict()
#            vals = RD[m['trau/sbe']]
#            if tmlist is not None:
#                tmask = ptools.create_range_mask(RD["T_"+m['trau/sbe']], tmlist)
#                if tmask is not None:
#                    vals = vals[tmask]
#            (TRAU["SNBI"], TRAU["SNBIEB"], TRAU["SNBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
#            TRAU["TFNSNBI"] = np.power(RD["X_"+m['trau/sbe']], 2.0)
#            #TRAU["SNBIEB"] = TRAU["SNBIEB"] + 0.05 * TRAU["SNBI"]
#            TRAU["SNBIDS"] = RD["I_"+m['trau/sbe']]

        # NBI particle source density profile - calculated by TRANSP
        hnbi = -1
        dnbi = -1
        tnbi = -1
        inbi = 0
        if 'trau/sbh' in m and RD[m['trau/sbh']] is not None:
            nn = "%d" % (inbi + 1)
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/sbh']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/sbh']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["SFI"+nn+"NBI"], TRAU["SFI"+nn+"NBIEB"], TRAU["SFI"+nn+"NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            #TRAU["SFI"+nn+"NBI"] = TRAU["SFI"+nn+"NBI"] + snb if "SFI"+nn+"NBI" in TRAU and TRAU["SFI"+nn+"NBI"] is not None else copy.deepcopy(snb)
            #TRAU["SFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["SFI"+nn+"NBIEB"], 2.0) + np.power(snbeb, 2.0)) if "SFI"+nn+"NBIEB" in TRAU and TRAU["SFI"+nn+"NBIEB"] is not None else copy.deepcopy(snbeb)
            #TRAU["SFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["SFI"+nn+"NBIEM"], 2.0) + np.power(snbem, 2.0)) if "SFI"+nn+"NBIEM" in TRAU and TRAU["SFI"+nn+"NBIEM"] is not None else copy.deepcopy(snbem)
            TRAU["TFNSFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/sbh']], 2.0)
            #TRAU["SFI"+nn+"NBIEB"] = TRAU["SFI"+nn+"NBIEB"] + 0.05 * TRAU["SFI"+nn+"NBI"]
            TRAU["SFI"+nn+"NBIDS"] = RD["I_"+m['trau/sbh']]
            #TRAU["SFI"+nn+"NBIDS"] = TRAU["SFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/sbh']] if "SFI"+nn+"NBIDS" in TRAU and TRAU["SFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/sbh']]
            inbi += 1
            hnbi = inbi
        if 'trau/sbd' in m and RD[m['trau/sbd']] is not None:
            nn = "%d" % (inbi + 1)
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/sbd']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/sbd']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["SFI"+nn+"NBI"], TRAU["SFI"+nn+"NBIEB"], TRAU["SFI"+nn+"NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            #TRAU["SFI"+nn+"NBI"] = TRAU["SFI"+nn+"NBI"] + snb if "SFI"+nn+"NBI" in TRAU and TRAU["SFI"+nn+"NBI"] is not None else snb
            #TRAU["SFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["SFI"+nn+"NBIEB"], 2.0) + np.power(snbeb, 2.0)) if "SFI"+nn+"NBIEB" in TRAU and TRAU["SFI"+nn+"NBIEB"] is not None else snbeb
            #TRAU["SFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["SFI"+nn+"NBIEM"], 2.0) + np.power(snbem, 2.0)) if "SFI"+nn+"NBIEM" in TRAU and TRAU["SFI"+nn+"NBIEM"] is not None else snbem
            TRAU["TFNSFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/sbd']], 2.0)
            #TRAU["SFI"+nn+"NBIEB"] = TRAU["SFI"+nn+"NBIEB"] + 0.05 * TRAU["SFI"+nn+"NBI"]
            TRAU["SFI"+nn+"NBIDS"] = RD["I_"+m['trau/sbd']]
            #TRAU["SFI"+nn+"NBIDS"] = TRAU["SFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/sbd']] if "SFI"+nn+"NBIDS" in TRAU and TRAU["SFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/sbd']]
            inbi += 1
            dnbi = inbi
        if 'trau/sbt' in m and RD[m['trau/sbt']] is not None:
            nn = "%d" % (inbi + 1)
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/sbt']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/sbt']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["SFI"+nn+"NBI"], TRAU["SFI"+nn+"NBIEB"], TRAU["SFI"+nn+"NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            #TRAU["SFI"+nn+"NBI"] = TRAU["SFI"+nn+"NBI"] + snb if "SFI"+nn+"NBI" in TRAU and TRAU["SFI"+nn+"NBI"] is not None else snb
            #TRAU["SFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["SFI"+nn+"NBIEB"], 2.0) + np.power(snbeb, 2.0)) if "SFI"+nn+"NBIEB" in TRAU and TRAU["SFI"+nn+"NBIEB"] is not None else snbeb
            #TRAU["SFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["SFI"+nn+"NBIEM"], 2.0) + np.power(snbem, 2.0)) if "SFI"+nn+"NBIEM" in TRAU and TRAU["SFI"+nn+"NBIEM"] is not None else snbem
            TRAU["TFNSFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/sbt']], 2.0)
            #TRAU["SFI"+nn+"NBIEB"] = TRAU["SFI"+nn+"NBIEB"] + 0.05 * TRAU["SFI"+nn+"NBI"]
            TRAU["SFI"+nn+"NBIDS"] = RD["I_"+m['trau/sbt']]
            #TRAU["SFI"+nn+"NBIDS"] = TRAU["SFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/sbt']] if "SFI"+nn+"NBIDS" in TRAU and TRAU["SFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/sbt']]
            inbi += 1
            tnbi = inbi

        # NBI current source density profile - calculated by TRANSP
        if 'trau/curb' in m and RD[m['trau/curb']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/curb']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/curb']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["JNBI"], TRAU["JNBIEB"], TRAU["JNBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNJNBI"] = np.power(RD["X_"+m['trau/curb']], 2.0)
            #TRAU["JNBIEB"] = TRAU["JNBIEB"] + 0.05 * TRAU["JNBI"]
            TRAU["JNBIDS"] = RD["I_"+m['trau/curb']]

        # NBI momentum source density profile - calculated by TRANSP
        if 'trau/tqin' in m and RD[m['trau/tqin']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/tqin']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/tqin']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["TAUNBI"], TRAU["TAUNBIEB"], TRAU["TAUNBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNTAUNBI"] = np.power(RD["X_"+m['trau/tqin']], 2.0)
            #TRAU["TAUNBIEB"] = TRAU["TAUNBIEB"] + 0.05 * TRAU["TAUNBI"]
            TRAU["TAUNBIDS"] = RD["I_"+m['trau/tqin']]

        # NBI fast ion density and fast ion energy density profiles - calculated by TRANSP
        if 'trau/bdnh' in m and RD[m['trau/bdnh']] is not None:
            nn = "%d" % (hnbi)
            if hnbi <= 0:
                nn = "%d" % (inbi)
                inbi += 1
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/bdnh']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/bdnh']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["NFI"+nn+"NBI"], TRAU["NFI"+nn+"NBIEB"], TRAU["NFI"+nn+"NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNNFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/bdnh']], 2.0)
            #TRAU["NFI"+nn+"NBIEB"] = TRAU["NFI"+nn+"NBIEB"] + 0.05 * TRAU["NFI"+nn+"NBI"]
            TRAU["NFI"+nn+"NBIDS"] = RD["I_"+m['trau/bdnh']]
            if 'trau/upah' in m and RD[m['trau/upah']] is not None:
                vals = RD[m['trau/upah']]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['trau/upah']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRAU["WFI"+nn+"NBI"] = TRAU["WFI"+nn+"NBI"] + wpa if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None else wpa
                TRAU["WFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEB"], 2.0) + np.power(wpaeb, 2.0)) if "WFI"+nn+"NBIEB" in TRAU and TRAU["WFI"+nn+"NBIEB"] is not None else wpaeb
                TRAU["WFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEM"], 2.0) + np.power(wpaem, 2.0)) if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None else wpaem
                if "TFNWFI"+nn+"NBI" not in TRAU:
                    TRAU["TFNWFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/upah']], 2.0)
                TRAU["WFI"+nn+"NBIDS"] = TRAU["WFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/upah']] if "WFI"+nn+"NBIDS" in TRAU and TRAU["WFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/upah']]
            if 'trau/upph' in m and RD[m['trau/upph']] is not None:
                vals = RD[m['trau/upph']]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['trau/upph']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpp, wppeb, wppem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRAU["WFI"+nn+"NBI"] = TRAU["WFI"+nn+"NBI"] + wpp if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None else wpp
                TRAU["WFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEB"], 2.0) + np.power(wppeb, 2.0)) if "WFI"+nn+"NBIEB" in TRAU and TRAU["WFI"+nn+"NBIEB"] is not None else wppeb
                TRAU["WFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEM"], 2.0) + np.power(wppem, 2.0)) if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None else wppem
                if "TFNWFI"+nn+"NBI" not in TRAU:
                    TRAU["TFNWFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/upph']], 2.0)
                TRAU["WFI"+nn+"NBIDS"] = TRAU["WFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/upph']] if "WFI"+nn+"NBIDS" in TRAU and TRAU["WFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/upph']]
            TRAU["AFI"+nn+"NBI"] = np.array([1.0]).flatten()
            TRAU["AFI"+nn+"NBIDS"] = "EX2GK: Internal assumption"
            TRAU["ZFI"+nn+"NBI"] = np.array([1.0]).flatten()
            TRAU["ZFI"+nn+"NBIDS"] = "EX2GK: Internal assumption"
        if 'trau/bdnd' in m and RD[m['trau/bdnd']] is not None and (('trau/upad' in m and RD[m['trau/upad']] is not None) or ('trau/uppd' in m and RD[m['trau/uppd']] is not None)):
            nn = "%d" % (dnbi)
            if dnbi <= 0:
                nn = "%d" % (inbi)
                inbi += 1
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/bdnd']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/bdnd']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["NFI"+nn+"NBI"], TRAU["NFI"+nn+"NBIEB"], TRAU["NFI"+nn+"NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNNFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/bdnd']], 2.0)
            #TRAU["NFI"+nn+"NBIEB"] = TRAU["NFI"+nn+"NBIEB"] + 0.05 * TRAU["NFI"+nn+"NBI"]
            TRAU["NFI"+nn+"NBIDS"] = RD["I_"+m['trau/bdnd']]
            if 'trau/upad' in m and RD[m['trau/upad']] is not None:
                vals = RD[m['trau/upad']]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['trau/upad']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRAU["WFI"+nn+"NBI"] = TRAU["WFI"+nn+"NBI"] + wpa if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None else wpa
                TRAU["WFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEB"], 2.0) + np.power(wpaeb, 2.0)) if "WFI"+nn+"NBIEB" in TRAU and TRAU["WFI"+nn+"NBIEB"] is not None else wpaeb
                TRAU["WFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEM"], 2.0) + np.power(wpaem, 2.0)) if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None else wpaem
                if "TFNWFI"+nn+"NBI" not in TRAU:
                    TRAU["TFNWFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/upad']], 2.0)
                TRAU["WFI"+nn+"NBIDS"] = TRAU["WFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/upad']] if "WFI"+nn+"NBIDS" in TRAU and TRAU["WFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/upad']]
            if 'trau/uppd' in m and RD[m['trau/uppd']] is not None:
                vals = RD[m['trau/uppd']]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['trau/uppd']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpp, wppeb, wppem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRAU["WFI"+nn+"NBI"] = TRAU["WFI"+nn+"NBI"] + wpp if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None else wpp
                TRAU["WFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEB"], 2.0) + np.power(wppeb, 2.0)) if "WFI"+nn+"NBIEB" in TRAU and TRAU["WFI"+nn+"NBIEB"] is not None else wppeb
                TRAU["WFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEM"], 2.0) + np.power(wppem, 2.0)) if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None else wppem
                if "TFNWFI"+nn+"NBI" not in TRAU:
                    TRAU["TFNWFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/uppd']], 2.0)
                TRAU["WFI"+nn+"NBIDS"] = TRAU["WFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/uppd']] if "WFI"+nn+"NBIDS" in TRAU and TRAU["WFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/uppd']]
            TRAU["AFI"+nn+"NBI"] = np.array([2.0]).flatten()
            TRAU["AFI"+nn+"NBIDS"] = "EX2GK: Internal assumption"
            TRAU["ZFI"+nn+"NBI"] = np.array([1.0]).flatten()
            TRAU["ZFI"+nn+"NBIDS"] = "EX2GK: Internal assumption"
        if 'trau/bdnt' in m and RD[m['trau/bdnt']] is not None and (('trau/upat' in m and RD[m['trau/upat']] is not None) or ('trau/uppt' in m and RD[m['trau/uppt']] is not None)):
            nn = "%d" % (tnbi)
            if tnbi <= 0:
                nn = "%d" % (inbi)
                inbi += 1
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/bdnt']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/bdnt']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["NFI"+nn+"NBI"], TRAU["NFI"+nn+"NBIEB"], TRAU["NFI"+nn+"NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNNFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/bdnt']], 2.0)
            #TRAU["NFI"+nn+"NBIEB"] = TRAU["NFI"+nn+"NBIEB"] + 0.05 * TRAU["NFI"+nn+"NBI"]
            TRAU["NFI"+nn+"NBIDS"] = RD["I_"+m['trau/bdnt']]
            if 'trau/upat' in m and RD[m['trau/upat']] is not None:
                vals = RD[m['trau/upat']]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['trau/upat']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRAU["WFI"+nn+"NBI"] = TRAU["WFI"+nn+"NBI"] + wpa if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None else wpa
                TRAU["WFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEB"], 2.0) + np.power(wpaeb, 2.0)) if "WFI"+nn+"NBIEB" in TRAU and TRAU["WFI"+nn+"NBIEB"] is not None else wpaeb
                TRAU["WFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEM"], 2.0) + np.power(wpaem, 2.0)) if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None else wpaem
                if "TFNWFI"+nn+"NBI" not in TRAU:
                    TRAU["TFNWFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/upat']], 2.0)
                TRAU["WFI"+nn+"NBIDS"] = TRAU["WFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/upat']] if "WFI"+nn+"NBIDS" in TRAU and TRAU["WFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/upat']]
            if 'trau/uppt' in m and RD[m['trau/uppt']] is not None:
                vals = RD[m['trau/uppt']]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['trau/uppt']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpp, wppeb, wppem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRAU["WFI"+nn+"NBI"] = TRAU["WFI"+nn+"NBI"] + wpp if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None else wpp
                TRAU["WFI"+nn+"NBIEB"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEB"], 2.0) + np.power(wppeb, 2.0)) if "WFI"+nn+"NBIEB" in TRAU and TRAU["WFI"+nn+"NBIEB"] is not None else wppeb
                TRAU["WFI"+nn+"NBIEM"] = np.sqrt(np.power(TRAU["WFI"+nn+"NBIEM"], 2.0) + np.power(wppem, 2.0)) if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None else wppem
                if "TFNWFI"+nn+"NBI" not in TRAU:
                    TRAU["TFNWFI"+nn+"NBI"] = np.power(RD["X_"+m['trau/uppt']], 2.0)
                TRAU["WFI"+nn+"NBIDS"] = TRAU["WFI"+nn+"NBIDS"] + "; " + RD["I_"+m['trau/uppt']] if "WFI"+nn+"NBIDS" in TRAU and TRAU["WFI"+nn+"NBIDS"] is not None else RD["I_"+m['trau/uppt']]
            TRAU["AFI"+nn+"NBI"] = np.array([3.0]).flatten()
            TRAU["AFI"+nn+"NBIDS"] = "EX2GK: Internal assumption"
            TRAU["ZFI"+nn+"NBI"] = np.array([1.0]).flatten()
            TRAU["ZFI"+nn+"NBIDS"] = "EX2GK: Internal assumption"
#        if TRAU is not None:
#            for ii in range(inbi):
#                nn = "%d" % (ii + 1)
#                if "NFI"+nn+"NBI" in TRAU and TRAU["NFI"+nn+"NBI"] is not None:
#                    TRAU["NFIANBI"] = TRAU["NFIANBI"] + TRAU["NFI"+nn+"NBI"] if "NFIANBI" in TRAU else TRAU["NFI"+nn+"NBI"].copy()
#                    if "TFNNFIANBI" not in TRAU:
#                        TRAU["TFNNFIANBI"] = TRAU["TFNNFI"+nn+"NBI"].copy()
#                    TRAU["NFIANBIEB"] = np.sqrt(np.power(TRAU["NFIANBIEB"], 2.0) + np.power(TRAU["NFI"+nn+"NBIEB"], 2.0)) if "NFIANBIEB" in TRAU else TRAU["NFI"+nn+"NBIEB"].copy()
#                    if "NFI"+nn+"NBIEM" in TRAU and TRAU["NFI"+nn+"NBIEM"] is not None:
#                        TRAU["NFIANBIEM"] = np.sqrt(np.power(TRAU["NFIANBIEM"], 2.0) + np.power(TRAU["NFI"+nn+"NBIEM"], 2.0)) if "NFIANBIEM" in TRAU else TRAU["NFI"+nn+"NBIEM"].copy()
#                    TRAU["NFIANBIDS"] = TRAU["NFIANBIDS"] + "; " + TRAU["NFI"+nn+"NBIDS"] if "NFIANBIDS" in TRAU else TRAU["NFI"+nn+"NBIDS"]
#                if "WFI"+nn+"NBI" in TRAU and TRAU["WFI"+nn+"NBI"] is not None:
#                    TRAU["WFIANBI"] = TRAU["WFIANBI"] + TRAU["WFI"+nn+"NBI"] if "WFIANBI" in TRAU else TRAU["WFI"+nn+"NBI"].copy()
#                    if "TFNWFIANBI" not in TRAU:
#                        TRAU["TFNWFIANBI"] = TRAU["TFNWFI"+nn+"NBI"].copy()
#                    TRAU["WFIANBIEB"] = TRAU["WFIANBIEB"] + TRAU["WFI"+nn+"NBIEB"] if "WFIANBIEB" in TRAU else TRAU["WFI"+nn+"NBIEB"].copy()
#                    if "WFI"+nn+"NBIEM" in TRAU and TRAU["WFI"+nn+"NBIEM"] is not None:
#                        TRAU["WFIANBIEM"] = TRAU["WFIANBIEM"] + TRAU["WFI"+nn+"NBIEM"] if "WFIANBIEM" in TRAU else TRAU["WFI"+nn+"NBIEM"].copy()
#                    TRAU["WFIANBIDS"] = TRAU["WFIANBIDS"] + "; " + TRAU["WFI"+nn+"NBIDS"] if "WFIANBIDS" in TRAU else TRAU["WFI"+nn+"NBIDS"]
#            if "NFIANBI" not in TRAU:
#                TRAU["NFIANBI"] = None
#                if "TFNNFIANBI" not in TRAU:
#                    TRAU["TFNNFIANBI"] = None
#                TRAU["NFIANBIEB"] = None
#                TRAU["NFIANBIEM"] = None
#            if "WFIANBI" not in TRAU:
#                TRAU["WFIANBI"] = None
#                if "TFNWFIANBI" not in TRAU:
#                    TRAU["TFNWFIANBI"] = None
#                TRAU["WFIANBIEB"] = None
#                TRAU["WFIANBIEM"] = None

        # RF heat source density profile to electrons - calculated by TRANSP
        if 'trau/qrfe' in m and RD[m['trau/qrfe']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/qrfe']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/qrfe']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["QEICRH"], TRAU["QEICRHEB"], TRAU["QEICRHEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNQEICRH"] = np.power(RD["X_"+m['trau/qrfe']], 2.0)
            #TRAU["QEICRHEB"] = TRAU["QEICRHEB"] + 0.05 * TRAU["QEICRH"]
            TRAU["QEICRHDS"] = RD["I_"+m['tra0/qrfe']]

        # RF heat source density profile to ions - calculated by TRANSP
        if 'trau/qrfi' in m and RD[m['trau/qrfi']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/qrfi']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['tra0/qrfi']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["QIICRH"], TRAU["QIICRHEB"], TRAU["QIICRHEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNQIICRH"] = np.power(RD["X_"+m['trau/qrfi']], 2.0)
            #TRAU["QIICRHEB"] = TRAU["QIICRHEB"] + 0.05 * TRAU["QIICRH"]
            TRAU["QIICRHDS"] = RD["I_"+m['trau/qrfi']]

        # RF fast ion density profile - calculated by TRANSP
        if 'trau/nmin' in m and RD[m['trau/nmin']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/nmin']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/nmin']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (TRAU["NFI1ICRH"], TRAU["NFI1ICRHEB"], TRAU["NFI1ICRHEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["TFNNFI1ICRH"] = np.power(RD["X_"+m['trau/nmin']], 2.0)
            #TRAU["NFI1ICRHEB"] = TRAU["NFI1ICRHEB"] + 0.05 * TRAU["NFI1ICRH"]
            TRAU["NFI1ICRHDS"] = RD["I_"+m['tra0/nmin']]
        if TRAU is not None:
            if "NFI1ICRH" in TRAU and TRAU["NFI1ICRH"] is not None:
                TRAU["NFIAICRH"] = TRAU["NFIAICRH"] + TRAU["NFI1ICRH"] if "NFIAICRH" in TRAU else TRAU["NFI1ICRH"].copy()
                if "TFNNFIAICRH" not in TRAU:
                    TRAU["TFNNFIAICRH"] = TRAU["TFNNFI1ICRH"].copy()
                TRAU["NFIAICRHEB"] = np.sqrt(np.power(TRAU["NFIAICRHEB"], 2.0) + np.power(TRAU["NFI1ICRHEB"], 2.0)) if "NFIAICRHEB" in TRAU else TRAU["NFI1ICRHEB"].copy()
                if "NFI1ICRHEM" in TRAU and TRAU["NFI1ICRHEM"] is not None:
                    TRAU["NFIAICRHEM"] = np.sqrt(np.power(TRAU["NFIAICRHEM"], 2.0) + np.power(TRAU["NFI1ICRHEM"], 2.0)) if "NFIAICRHEM" in TRAU else TRAU["NFI1ICRHEM"].copy()
                TRAU["NFIAICRHDS"] = TRAU["NFIAICRHDS"] + "; " + TRAU["NFI1ICRHDS"] if "NFIAICRHDS" in TRAU and TRAU["NFIAICRHDS"] is not None else TRAU["NFI1ICRHDS"]

        # RF fast ion energy density profile - calculated by TRANSP
        if 'trau/umpa' in m and RD[m['trau/umpa']] is not None and 'trau/umpp' in m and RD[m['trau/umpp']] is not None:
            if TRAU is None:
                TRAU = dict()
            vals = RD[m['trau/umpa']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/umpa']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["WFI1ICRH"] = TRAU["WFI1ICRH"] + wpa if "WFI1ICRH" in TRAU and TRAU["WFI1ICRH"] is not None else wpa
            TRAU["WFI1ICRHEB"] = np.sqrt(np.power(TRAU["WFI1ICRHEB"], 2.0) + np.power(wpaeb, 2.0)) if "WFI1ICRHEB" in TRAU and TRAU["WFI1ICRHEB"] is not None else wpaeb
            TRAU["WFI1ICRHEM"] = np.sqrt(np.power(TRAU["WFI1ICRHEM"], 2.0) + np.power(wpaem, 2.0)) if "WFI1ICRHEM" in TRAU and TRAU["WFI1ICRHEM"] is not None else wpaem
            TRAU["WFI1ICRHDS"] = TRAU["WFI1ICRHDS"] + "; " + RD["I_"+m['trau/umpa']] if "WFI1ICRHDS" in TRAU and TRAU["WFI1ICRHDS"] is not None else RD["I_"+m['trau/umpa']]
            vals = RD[m['trau/umpp']]
            if tmlist is not None:
                tmask = ptools.create_range_mask(RD["T_"+m['trau/umpp']], tmlist)
                if tmask is not None:
                    vals = vals[tmask]
            (wpp, wppeb, wppem, navgmap) = ptools.filtered_average(vals, use_n=True)
            TRAU["WFI1ICRH"] = TRAU["WFI1ICRH"] + wpp if "WFI1ICRH" in TRAU and TRAU["WFI1ICRH"] is not None else wpp
            TRAU["WFI1ICRHEB"] = np.sqrt(np.power(TRAU["WFI1ICRHEB"], 2.0) + np.power(wppeb, 2.0)) if "WFI1ICRHEB" in TRAU and TRAU["WFI1ICRHEB"] is not None else wppeb
            TRAU["WFI1ICRHEM"] = np.sqrt(np.power(TRAU["WFI1ICRHEM"], 2.0) + np.power(wppem, 2.0)) if "WFI1ICRHEM" in TRAU and TRAU["WFI1ICRHEM"] is not None else wppem
            TRAU["WFI1ICRHDS"] = TRAU["WFI1ICRHDS"] + "; " + RD["I_"+m['trau/umpp']] if "WFI1ICRHDS" in TRAU and TRAU["WFI1ICRHDS"] is not None else RD["I_"+m['trau/umpp']]
            TRAU["TFNWFI1ICRH"] = np.power(RD["X_"+m['trau/umpp']], 2.0)
            #TRAU["WFI1ICRHEB"] = TRAU["WFI1ICRHEB"] + 0.05 * TRAU["WFI1ICRH"]
        if TRAU is not None:
            if "WFI1ICRH" in TRAU and TRAU["WFI1ICRH"] is not None:
                TRAU["WFIAICRH"] = TRAU["WFIAICRH"] + TRAU["WFIICRH"] if "WFIAICRH" in TRAU else TRAU["WFI1ICRH"].copy()
                if "TFNWFIAICRH" not in TRAU:
                    TRAU["TFNWFIAICRH"] = TRAU["TFNWFI1ICRH"].copy()
                TRAU["WFIAICRHEB"] = TRAU["WFIAICRHEB"] + TRAU["WFI1ICRHEB"] if "WFIAICRHEB" in TRAU else TRAU["WFI1ICRHEB"].copy()
                if "WFIICRHEM" in TRAU and TRAU["WFIICRHEM"] is not None:
                    TRAU["WFIAICRHEM"] = TRAU["WFIAICRHEM"] + TRAU["WFI1ICRHEM"] if "WFIAICRHEM" in TRAU else TRAU["WFI1ICRHEM"].copy()
                TRAU["WFIAICRHDS"] = TRAU["WFIAICRHDS"] + "; " + TRAU["WFI1ICRHDS"] if "WFIAICRHDS" in TRAU and TRAU["WFIAICRHDS"] is not None else TRAU["WFI1ICRHDS"]

        # RF fast ion species info - output by PION in chain2
        if TRAU is not None and "NFI1ICRH" in TRAU and "WFI1ICRH" in TRAU:
            TRAU["AFI1ICRH"] = np.array([np.mean(RD[m['pion/am1']])]).flatten() if 'pion/am1' in m and RD[m['pion/am1']] is not None else None
            TRAU["ZFI1ICRH"] = np.array([np.mean(RD[m['pion/ach1']])]).flatten() if 'pion/ach1' in m and RD[m['pion/ach1']] is not None else None
            if not ("AFI1ICRHDS" in TRAU and TRAU["AFI1ICRHDS"] is not None):
                TRAU["AFI1ICRHDS"] = RD["I_"+m['pion/am1']]
            if not ("ZFI1ICRHDS" in TRAU and TRAU["ZFI1ICRHDS"] is not None):
                TRAU["ZFI1ICRHDS"] = RD["I_"+m['pion/ach1']]
            if TRAU["AFI1ICRH"] is None or TRAU["ZFI1ICRH"] is None:
                TRAU["AFI1ICRH"] = np.array([np.mean(RD[m['pion/am2']])]).flatten() if 'pion/am2' in m and RD[m['pion/am2']] is not None else None
                TRAU["ZFI1ICRH"] = np.array([np.mean(RD[m['pion/ach2']])]).flatten() if 'pion/ach2' in m and RD[m['pion/ach2']] is not None else None
                if not ("AFI1ICRHDS" in TRAU and TRAU["AFI1ICRHDS"] is not None):
                    TRAU["AFI1ICRHDS"] = RD["I_"+m['pion/am2']]
                if not ("ZFI1ICRHDS" in TRAU and TRAU["ZFI1ICRHDS"] is not None):
                    TRAU["ZFI1ICRHDS"] = RD["I_"+m['pion/ach2']]
        if TRAU is not None:
            if "AFI1ICRH" in TRAU and TRAU["AFI1ICRH"] is not None and "ZFI1ICRH" in TRAU and TRAU["ZFI1ICRH"] is not None:
                TRAU["AFIAICRH"] = copy.deepcopy(TRAU["AFI1ICRH"])
                TRAU["ZFIAICRH"] = copy.deepcopy(TRAU["ZFI1ICRH"])
                if "AFI1ICRHDS" in TRAU:
                    TRAU["AFIAICRHDS"] = TRAU["AFI1ICRHDS"]
                if "ZFI1ICRHDS" in TRAU:
                    TRAU["ZFIAICRHDS"] = TRAU["ZFI1ICRHDS"]

        if TRAU is not None:
            TRAU["CODE"] = "TRANSP"

        PD["TRAU"] = TRAU

        # TRANSP sources (after transfer into PPF) - these take precedence over all other calculations
        TRA0 = None

        # Radial coordinate transformation for TRANSP - this is not steady in time so time-averaging may result in strangeness
#        trxvec = None
#        if 'tra0/trfl' in m and RD[m['tra0/trfl']] is not None:
#            if TRA0 is None:
#                TRA0 = dict()
#            trxvec = RD[m['tra0/trfl']]

        # Total current density profile - calculated by TRANSP, not strictly necessary
        if 'tra0/cur' in m and RD[m['tra0/cur']] is not None:
            rfilt = (RD["X_"+m['tra0/cur']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/cur']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/cur']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["JOHM"], TRA0["JOHMEB"], TRA0["JOHMEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMJOHM"] = RD["X_"+m['tra0/cur']][rfilt]
                #TRA0["JOHMEB"] = TRA0["JOHMEB"] + 0.05 * TRA0["JOHM"]
                TRA0["JOHMDS"] = RD["I_"+m['tra0/cur']]

        # Ohmic power density profile - calculated by TRANSP
        if 'tra0/qoh' in m and RD[m['tra0/qoh']] is not None:
            rfilt = (RD["X_"+m['tra0/qoh']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/qoh']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/qoh']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["QEOHM"], TRA0["QEOHMEB"], TRA0["QEOHMEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMJOHM"] = RD["X_"+m['tra0/qoh']][rfilt]
                #TRA0["QEOHMEB"] = TRA0["QEOHMEB"] + 0.05 * TRA0["QEOHM"]
                TRA0["QEOHMDS"] = RD["I_"+m['tra0/qoh']]

        # NBI heat source density profile to electrons - calculated by TRANSP
        if 'tra0/qbe' in m and RD[m['tra0/qbe']] is not None:
            rfilt = (RD["X_"+m['tra0/qbe']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/qbe']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/qbe']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["QENBI"], TRA0["QENBIEB"], TRA0["QENBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMQENBI"] = RD["X_"+m['tra0/qbe']][rfilt]
                #TRA0["QENBIEB"] = TRA0["QENBIEB"] + 0.05 * TRA0["QENBI"]
                TRA0["QENBIDS"] = RD["I_"+m['tra0/qbe']]

        # NBI heat source density profile to ions - calculated by TRANSP
        if 'tra0/qbi' in m and RD[m['tra0/qbi']] is not None:
            rfilt = (RD["X_"+m['tra0/qbi']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/qbi']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/qbi']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["QINBI"], TRA0["QINBIEB"], TRA0["QINBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMQINBI"] = RD["X_"+m['tra0/qbi']][rfilt]
                #TRA0["QINBIEB"] = TRA0["QINBIEB"] + 0.05 * TRA0["QINBI"]
                TRA0["QINBIDS"] = RD["I_"+m['tra0/qbi']]

        # NBI particle source density profile - calculated by TRANSP
        if 'tra0/sbe' in m and RD[m['tra0/sbe']] is not None:
            rfilt = (RD["X_"+m['tra0/sbe']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/sbe']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/sbe']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["SFIANBI"], TRA0["SFIANBIEB"], TRA0["SFIANBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMSFIANBI"] = RD["X_"+m['tra0/sbe']][rfilt]
                #TRA0["SFIANBIEB"] = TRA0["SFIANBIEB"] + 0.05 * TRA0["SFIANBI"]
                TRA0["SFIANBIDS"] = RD["I_"+m['tra0/sbe']]

        # NBI current source density profile - calculated by TRANSP
        if 'tra0/cb' in m and RD[m['tra0/cb']] is not None:
            rfilt = (RD["X_"+m['tra0/cb']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/cb']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/cb']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["JNBI"], TRA0["JNBIEB"], TRA0["JNBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMJNBI"] = RD["X_"+m['tra0/cb']][rfilt]
                #TRA0["JNBIEB"] = TRA0["JNBIEB"] + 0.05 * TRA0["JNBI"]
                TRA0["JNBIDS"] = RD["I_"+m['tra0/cb']]

        # NBI momentum source density profile - calculated by TRANSP
        if 'tra0/tqin' in m and RD[m['tra0/tqin']] is not None:
            rfilt = (RD["X_"+m['tra0/tqin']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/tqin']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/tqin']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["TAUNBI"], TRA0["TAUNBIEB"], TRA0["TAUNBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMTAUNBI"] = RD["X_"+m['tra0/tqin']][rfilt]
                #TRA0["TAUNBIEB"] = TRA0["TAUNBIEB"] + 0.05 * TRA0["TAUNBI"]
                TRA0["TAUNBIDS"] = RD["I_"+m['tra0/tqin']]

        # NBI fast ion density profile - calculated by TRANSP
        if 'tra0/nb' in m and RD[m['tra0/nb']] is not None:
            rfilt = (RD["X_"+m['tra0/nb']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/nb']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/nb']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["NFI1NBI"], TRA0["NFI1NBIEB"], TRA0["NFI1NBIEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMNFI1NBI"] = RD["X_"+m['tra0/nb']][rfilt]
                #TRA0["NFI1NBIEB"] = TRA0["NFI1NBIEB"] + 0.05 * TRA0["NFI1NBI"]
                TRA0["NFI1NBIDS"] = RD["I_"+m['tra0/nb']]
        if TRA0 is not None and "NFINBI" in TRA0:
            if TRA0["NFI1NBI"] is not None:
                TRA0["NFIANBI"] = TRA0["NFIANBI"] + TRA0["NFI1NBI"] if "NFI1ANBI" in TRA0 else TRA0["NFI1NBI"].copy()
                if "RMNFIANBI" not in TRA0:
                    TRA0["RMNFIANBI"] = TRA0["RMNFI1NBI"].copy()
                TRA0["NFIANBIEB"] = np.sqrt(np.power(TRA0["NFIANBIEB"], 2.0) + np.power(TRA0["NFI1NBIEB"], 2.0)) if "NFIANBIEB" in TRA0 else TRA0["NFI1NBIEB"].copy()
                if "NFI1NBIEM" in TRA0 and TRA0["NFI1NBIEM"] is not None:
                    TRA0["NFIANBIEM"] = np.sqrt(np.power(TRA0["NFIANBIEM"], 2.0) + np.power(TRA0["NFI1NBIEM"], 2.0)) if "NFIANBIEM" in TRA0 else TRA0["NFI1NBIEM"].copy()
                TRA0["NFIANBIDS"] = TRA0["NFIANBIDS"] + "; " + TRA0["NFI1NBIDS"] if "NFIANBIDS" in TRA0 else TRA0["NFI1NBIDS"]
            else:
                TRA0["NFIANBI"] = None
                if "RMNFIANBI" not in TRA0:
                    TRA0["RMNFIANBI"] = None
                TRA0["NFIANBIEB"] = None
                if "NFIANBIEM" in TRA0:
                    TRA0["NFIANBIEM"] = None

        # NBI fast ion energy density profile - calculated by TRANSP
        if 'tra0/wbpa' in m and RD[m['tra0/wbpa']] is not None and 'tra0/wbpp' in m and RD[m['tra0/wbpp']] is not None:
            rfilt = (RD["X_"+m['tra0/wbpa']] >= rmag)
            if np.any(rfilt):
                vals = RD[m['tra0/wbpa']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/wbpa']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["WFI1NBI"] = TRA0["WFI1NBI"] + wpa if "WFI1NBI" in TRA0 and TRA0["WFI1NBI"] is not None else wpa
                TRA0["WFI1NBIEB"] = np.sqrt(np.power(TRA0["WFI1NBIEB"], 2.0) + np.power(wpaeb, 2.0)) if "WFI1NBIEB" in TRA0 and TRA0["WFI1NBIEB"] is not None else wpaeb
                TRA0["WFI1NBIEM"] = np.sqrt(np.power(TRA0["WFI1NBIEM"], 2.0) + np.power(wpaem, 2.0)) if "WFI1NBIEM" in TRA0 and TRA0["WFI1NBIEM"] is not None else wpaem
                if "RMWFI1NBI" not in TRA0:
                    TRA0["RMWFI1NBI"] = RD["X_"+m['tra0/wbpa']][rfilt]
                #TRA0["WFI1NBIEB"] = TRA0["WFI1NBIEB"] + 0.05 * TRA0["WFINBI"]
                TRA0["WFI1NBIDS"] = TRA0["WFI1NBIDS"] + "; " + RD["I_"+m['tra0/wbpa']] if "WFI1NBIDS" in TRA0 and TRA0["WFI1NBIDS"] is not None else RD["I_"+m['tra0/wbpa']]
            rfilt = (RD["X_"+m['tra0/wbpp']] >= rmag)
            if np.any(rfilt):
                vals = RD[m['tra0/wbpp']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/wbpp']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["WFI1NBI"] = TRA0["WFI1NBI"] + wpp if "WFI1NBI" in TRA0 and TRA0["WFI1NBI"] is not None else wpp
                TRA0["WFI1NBIEB"] = np.sqrt(np.power(TRA0["WFI1NBIEB"], 2.0) + np.power(wppeb, 2.0)) if "WFI1NBIEB" in TRA0 and TRA0["WFI1NBIEB"] is not None else wppeb
                TRA0["WFI1NBIEM"] = np.sqrt(np.power(TRA0["WFI1NBIEM"], 2.0) + np.power(wppem, 2.0)) if "WFI1NBIEM" in TRA0 and TRA0["WFI1NBIEM"] is not None else wppem
                if "RMWFI1NBI" not in TRA0:
                    TRA0["RMWFI1NBI"] = RD["X_"+m['tra0/wbpp']][rfilt]
                #TRA0["WFI1NBIEB"] = TRA0["WFI1NBIEB"] + 0.05 * TRA0["WFINBI"]
                TRA0["WFI1NBIDS"] = TRA0["WFI1NBIDS"] + "; " + RD["I_"+m['tra0/wbpp']] if "WFI1NBIDS" in TRA0 and TRA0["WFI1NBIDS"] is not None else RD["I_"+m['tra0/wbpp']]
        if TRA0 is not None and "WFI1NBI" in TRA0:
            if TRA0["WFINBI"] is not None:
                TRA0["WFIANBI"] = TRA0["WFIANBI"] + TRA0["WFI1NBI"] if "WFIANBI" in TRA0 else TRA0["WFI1NBI"].copy()
                if "RMWFIANBI" not in TRA0:
                    TRA0["RMWFIANBI"] = TRA0["RMWFI1NBI"].copy()
                TRA0["WFIANBIEB"] = np.sqrt(np.power(TRA0["WFIANBIEB"], 2.0) + np.sqrt(TRA0["WFI1NBIEB"], 2.0)) if "WFIANBIEB" in TRA0 else TRA0["WFI1NBIEB"].copy()
                if "WFINBIEM" in TRA0 and TRA0["WFINBIEM"] is not None:
                    TRA0["WFIANBIEM"] = np.sqrt(np.power(TRA0["WFIANBIEM"], 2.0) + np.sqrt(TRA0["WFINBIEM"], 2.0)) if "WFIANBIEM" in TRA0 else TRA0["WFI1NBIEM"].copy()
                TRA0["WFIANBIDS"] = TRA0["WFIANBIDS"] + "; " + TRA0["WFI1NBIDS"] if "WFIANBIDS" in TRA0 else TRA0["WFI1NBIDS"]
            else:
                TRA0["WFIANBI"] = None
                if "RMWFIANBI" not in TRA0:
                    TRA0["RMWFIANBI"] = None
                TRA0["WFIANBIEB"] = None
                if "WFIANBIEM" in TRA0:
                    TRA0["WFIANBIEM"] = None

        # NBI fast ion species info - taken from NBI4 and NBI8
        if TRA0 is not None:
            afi4 = float(np.mean(RD[m['nbi4/gasa']])) if 'nbi4/gasa' in m and RD[m['nbi4/gasa']] is not None else None
            zfi4 = float(np.mean(RD[m['nbi4/gasz']])) if 'nbi4/gasz' in m and RD[m['nbi4/gasz']] is not None else None
            afi8 = float(np.mean(RD[m['nbi8/gasa']])) if 'nbi8/gasa' in m and RD[m['nbi8/gasa']] is not None else None
            zfi8 = float(np.mean(RD[m['nbi8/gasz']])) if 'nbi8/gasz' in m and RD[m['nbi8/gasz']] is not None else None
            TRA0["AFIANBI"] = None
            TRA0["ZFIANBI"] = None
            if afi4 is not None and afi4 >= 1.0 and zfi4 is not None and zfi4 >= 1.0:
                TRA0["AFIANBI"] = float((TRA0["AFIANBI"] + afi4) / 2.0) if TRA0["AFIANBI"] is not None else float(afi4)
                TRA0["ZFIANBI"] = float((TRA0["ZFIANBI"] + zfi4) / 2.0) if TRA0["ZFIANBI"] is not None else float(zfi4)
                TRA0["AFIANBIDS"] = TRA0["AFIANBIDS"] + "; " + RD["I_"+m['nbi4/gasa']] if "AFIANBIDS" in TRA0 and TRA0["AFIANBIDS"] is not None else RD["I_"+m['nbi4/gasa']]
                TRA0["ZFIANBIDS"] = TRA0["ZFIANBIDS"] + "; " + RD["I_"+m['nbi4/gasz']] if "ZFIANBIDS" in TRA0 and TRA0["ZFIANBIDS"] is not None else RD["I_"+m['nbi4/gasz']]
            if afi8 is not None and afi8 >= 1.0 and zfi8 is not None and zfi8 >= 1.0:
                TRA0["AFIANBI"] = float((TRA0["AFIANBI"] + afi8) / 2.0) if TRA0["AFIANBI"] is not None else float(afi8)
                TRA0["ZFIANBI"] = float((TRA0["ZFIANBI"] + zfi8) / 2.0) if TRA0["ZFIANBI"] is not None else float(zfi8)
                TRA0["AFIANBIDS"] = TRA0["AFIANBIDS"] + "; " + RD["I_"+m['nbi8/gasa']] if "AFIANBIDS" in TRA0 and TRA0["AFIANBIDS"] is not None else RD["I_"+m['nbi8/gasa']]
                TRA0["ZFIANBIDS"] = TRA0["ZFIANBIDS"] + "; " + RD["I_"+m['nbi8/gasz']] if "ZFIANBIDS" in TRA0 and TRA0["ZFIANBIDS"] is not None else RD["I_"+m['nbi8/gasz']]
            if TRA0["AFIANBI"] is not None and not isinstance(TRA0["AFIANBI"], np.ndarray):
                TRA0["AFIANBI"] = np.array([TRA0["AFIANBI"]]).flatten()
            if TRA0["ZFIANBI"] is not None and not isinstance(TRA0["ZFIANBI"], np.ndarray):
                TRA0["ZFIANBI"] = np.array([TRA0["ZFIANBI"]]).flatten()
            TRA0["CODE"] = "TRA0"

        # RF heat source density profile to electrons - calculated by TRANSP
        if 'tra0/qrfe' in m and RD[m['tra0/qrfe']] is not None:
            rfilt = (RD["X_"+m['tra0/qrfe']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/qrfe']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/qrfe']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["QEICRH"], TRA0["QEICRHEB"], TRA0["QEICRHEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMQEICRH"] = RD["X_"+m['tra0/qrfe']][rfilt]
                #TRA0["QEICRHEB"] = TRA0["QEICRHEB"] + 0.05 * TRA0["QEICRH"]
                TRA0["QEICRHDS"] = RD["I_"+m['tra0/qrfe']]

        # RF heat source density profile to ions - calculated by TRANSP
        if 'tra0/qrfi' in m and RD[m['tra0/qrfi']] is not None:
            rfilt = (RD["X_"+m['tra0/qrfi']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/qrfi']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/qrfi']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["QIICRH"], TRA0["QIICRHEB"], TRA0["QIICRHEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMQIICRH"] = RD["X_"+m['tra0/qrfi']][rfilt]
                #TRA0["QIICRHEB"] = TRA0["QIICRHEB"] + 0.05 * TRA0["QIICRH"]
                TRA0["QIICRHDS"] = RD["I_"+m['tra0/qrfi']]

        # RF fast ion density profile - calculated by TRANSP
        if 'tra0/nmin' in m and RD[m['tra0/nmin']] is not None:
            rfilt = (RD["X_"+m['tra0/nmin']] >= rmag)
            if np.any(rfilt):
                if TRA0 is None:
                    TRA0 = dict()
                vals = RD[m['tra0/nmin']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/nmin']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (TRA0["NFI1ICRH"], TRA0["NFI1ICRHEB"], TRA0["NFI1ICRHEM"], navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["RMNFI1ICRH"] = RD["X_"+m['tra0/nmin']][rfilt]
                #TRA0["NFI1ICRHEB"] = TRA0["NFI1ICRHEB"] + 0.05 * TRA0["NFI1ICRH"]
                TRA0["NFI1ICRHDS"] = RD["I_"+m['tra0/nmin']]
        if TRA0 is not None and "NFI1ICRH" in TRA0:
            if TRA0["NFI1ICRH"] is not None:
                TRA0["NFIAICRH"] = TRA0["NFIAICRH"] + TRA0["NFI1ICRH"] if "NFIAICRH" in TRA0 else TRA0["NFI1ICRH"].copy()
                if "RMNFIAICRH" not in TRA0:
                    TRA0["RMNFIAICRH"] = TRA0["RMNFI1ICRH"].copy()
                TRA0["NFIAICRHEB"] = TRA0["NFIAICRHEB"] + TRA0["NFI1ICRHEB"] if "NFIAICRHEB" in TRA0 else TRA0["NFI1ICRHEB"].copy()
                if "NFI1ICRHEM" in TRA0 and TRA0["NFI1ICRHEM"] is not None:
                    TRA0["NFIAICRHEM"] = TRA0["NFIAICRHEM"] + TRA0["NFI1ICRHEM"] if "NFIAICRHEM" in TRA0 else TRA0["NFI1ICRHEM"].copy()
                TRA0["NFIAICRHDS"] = TRA0["NFIAICRHDS"] + "; " + TRA0["NFI1ICRHDS"] if "NFIAICRHDS" in TRA0 and TRA0["NFIAICRHDS"] is not None else TRA0["NFI1ICRHDS"]
            else:
                TRA0["NFIAICRH"] = None
                if "RMNFIAICRH" not in TRA0:
                    TRA0["RMNFIAICRH"] = None
                TRA0["NFIAICRHEB"] = None
                if "NFIAICRHEM" in TRA0:
                    TRA0["NFIAICRHEM"] = None

        # RF fast ion energy density profile - calculated by TRANSP
        if 'tra0/wmpa' in m and RD[m['tra0/wmpa']] is not None:
            rfilt = (RD["X_"+m['tra0/wmpa']] >= rmag)
            if np.any(rfilt):
                vals = RD[m['tra0/wmpa']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/wmpa']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpa, wpaeb, wpaem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["WFI1ICRH"] = TRA0["WFI1ICRH"] + wpa if "WFI1ICRH" in TRA0 and TRA0["WFI1ICRH"] is not None else wpa
                TRA0["WFI1ICRHEB"] = np.sqrt(np.power(TRA0["WFI1ICRHEB"], 2.0) + np.power(wpaeb, 2.0)) if "WFI1ICRHEB" in TRA0 and TRA0["WFI1ICRHEB"] is not None else wpaeb
                TRA0["WFI1ICRHEM"] = np.sqrt(np.power(TRA0["WFI1ICRHEM"], 2.0) + np.power(wpaem, 2.0)) if "WFI1ICRHEM" in TRA0 and TRA0["WFI1ICRHEM"] is not None else wpaem
                if "RMWFI1ICRH" not in TRA0:
                    TRA0["RMWFI1ICRH"] = RD["X_"+m['tra0/wmpa']][rfilt]
                TRA0["WFI1ICRHDS"] = TRA0["WFI1ICRHDS"] + "; " + RD["I_"+m['tra0/wmpa']] if "WFI1ICRHDS" in TRA0 and TRA0["WFI1ICRHDS"] is not None else RD["I_"+m['tra0/wmpa']]
        if 'tra0/wmpp' in m and RD[m['tra0/wmpp']] is not None:
            rfilt = (RD["X_"+m['tra0/wmpp']] >= rmag)
            if np.any(rfilt):
                vals = RD[m['tra0/wmpp']][:, rfilt]
                if tmlist is not None:
                    tmask = ptools.create_range_mask(RD["T_"+m['tra0/wmpp']], tmlist)
                    if tmask is not None:
                        vals = vals[tmask]
                (wpp, wppeb, wppem, navgmap) = ptools.filtered_average(vals, use_n=True)
                TRA0["WFI1ICRH"] = TRA0["WFI1ICRH"] + wpp if "WFI1ICRH" in TRA0 and TRA0["WFI1ICRH"] is not None else wpp
                TRA0["WFI1ICRHEB"] = np.sqrt(np.power(TRA0["WFI1ICRHEB"], 2.0) + np.power(wppeb, 2.0)) if "WFI1ICRHEB" in TRA0 and TRA0["WFI1ICRHEB"] is not None else wppeb
                TRA0["WFI1ICRHEM"] = np.sqrt(np.power(TRA0["WFI1ICRHEM"], 2.0) + np.power(wppem, 2.0)) if "WFI1ICRHEM" in TRA0 and TRA0["WFI1ICRHEM"] is not None else wppem
                if "RMWFI1ICRH" not in TRA0:
                    TRA0["RMWFI1ICRH"] = RD["X_"+m['tra0/wmpp']][rfilt]
                TRA0["WFI1ICRHDS"] = TRA0["WFI1ICRHDS"] + "; " + RD["I_"+m['tra0/wmpp']] if "WFI1ICRHDS" in TRA0 and TRA0["WFI1ICRHDS"] is not None else RD["I_"+m['tra0/wmpp']]
        if TRA0 is not None and "WFI1ICRH" in TRA0:
            if TRA0["WFI1ICRH"] is not None:
                TRA0["WFIAICRH"] = TRA0["WFIAICRH"] + TRA0["WFI1ICRH"] if "WFIAICRH" in TRA0 else TRA0["WFI1ICRH"].copy()
                if "RMWFIAICRH" not in TRA0:
                    TRA0["RMWFIAICRH"] = TRA0["RMWFI1ICRH"].copy()
                TRA0["WFIAICRHEB"] = TRA0["WFIAICRHEB"] + TRA0["WFI1ICRHEB"] if "WFIAICRHEB" in TRA0 else TRA0["WFI1ICRHEB"].copy()
                if "WFIICRHEM" in TRA0 and TRA0["WFI1ICRHEM"] is not None:
                    TRA0["WFIAICRHEM"] = TRA0["WFIAICRHEM"] + TRA0["WFI1ICRHEM"] if "WFIAICRHEM" in TRA0 else TRA0["WFI1ICRHEM"].copy()
                TRA0["WFIAICRHDS"] = TRA0["WFIAICRHDS"] + "; " + TRA0["WFI1ICRHDS"] if "WFIAICRHDS" in TRA0 and TRA0["WFIAICRHDS"] is not None else TRA0["WFI1ICRHDS"]
            else:
                TRA0["WFIAICRH"] = None
                if "RMWFIAICRH" not in TRA0:
                    TRA0["RMWFIAICRH"] = None
                TRA0["WFIAICRHEB"] = None
                if "WFIAICRHEM" in TRA0:
                    TRA0["WFIAICRHEM"] = None

        # RF fast ion species info - output by PION in chain2
        if TRA0 is not None:
            TRA0["AFIAICRH"] = np.array([np.mean(RD[m['pion/am1']])]).flatten() if 'pion/am1' in m and RD[m['pion/am1']] is not None else None
            TRA0["ZFIAICRH"] = np.array([np.mean(RD[m['pion/ach1']])]).flatten() if 'pion/ach1' in m and RD[m['pion/ach1']] is not None else None
            if not ("AFIAICRHDS" in TRA0 and TRA0["AFIAICRHDS"] is not None):
                TRA0["AFIAICRHDS"] = RD["I_"+m['pion/am1']]
            if not ("ZFIAICRHDS" in TRA0 and TRA0["ZFIAICRHDS"] is not None):
                TRA0["ZFIAICRHDS"] = RD["I_"+m['pion/ach1']]
            if TRA0["AFIAICRH"] is None or TRA0["ZFIAICRH"] is None:
                TRA0["AFIAICRH"] = np.array([np.mean(RD[m['pion/am2']])]).flatten() if 'pion/am2' in m and RD[m['pion/am2']] is not None else None
                TRA0["ZFIAICRH"] = np.array([np.mean(RD[m['pion/ach2']])]).flatten() if 'pion/ach2' in m and RD[m['pion/ach2']] is not None else None
                if not ("AFIAICRHDS" in TRA0 and TRA0["AFIAICRHDS"] is not None):
                    TRA0["AFIAICRHDS"] = RD["I_"+m['pion/am2']]
                if not ("ZFIAICRHDS" in TRA0 and TRA0["ZFIAICRHDS"] is not None):
                    TRA0["ZFIAICRHDS"] = RD["I_"+m['pion/ach2']]
            TRA0["CODE"] = "TRANSP"

        PD["TRA0"] = TRA0 if not ("TRAU" in PD and PD["TRAU"] is not None) else None

        # NBI sources - remove entry at magnetic axis since it seems to be poorly constrained
        NBP2 = None

        # NBI heat source density profile to electrons - calculated by PENCIL in chain2
        if 'nbp2/ge' in m and RD[m['nbp2/ge']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["QE"] = RD[m['nbp2/ge']]
            NBP2["PFNQE"] = np.power(RD["X_"+m['nbp2/ge']], 2.0)
            NBP2["QEEB"] = 0.05 * NBP2["QE"]
            NBP2["QEDS"] = RD["I_"+m['nbp2/ge']]

        # NBI heat source density profile to ions - calculated by PENCIL in chain2
        if 'nbp2/gi' in m and RD[m['nbp2/gi']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["QI"] = RD[m['nbp2/gi']]
            NBP2["PFNQI"] = np.power(RD["X_"+m['nbp2/gi']], 2.0)
            NBP2["QIEB"] = 0.05 * NBP2["QI"]
            NBP2["QIDS"] = RD["I_"+m['nbp2/gi']]

        # NBI particle source density profile - calculated by PENCIL in chain2
        if 'nbp2/sv' in m and RD[m['nbp2/sv']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["SFI1"] = RD[m['nbp2/sv']]
            NBP2["PFNSFI1"] = np.power(RD["X_"+m['nbp2/sv']], 2.0)
            NBP2["SFI1EB"] = 0.05 * NBP2["SFI1"]
            NBP2["SFI1DS"] = RD["I_"+m['nbp2/sv']]
        if NBP2 is not None and "SFI1" in NBP2:
            NBP2["SFIA"] = NBP2["SFIA"] + NBP2["SFI1"] if "SFIA" in NBP2 else NBP2["SFI1"].copy()
            if "PFNSFIA" not in NBP2:
                NBP2["PFNSFIA"] = NBP2["PFNSFI1"].copy()
            NBP2["SFIAEB"] = 0.05 * NBP2["SFIA"]
            NBP2["SFIADS"] = NBP2["SFIADS"] + "; " + NBP2["SFI1DS"] if "SFIADS" in NBP2 and NBP2["SFIADS"] is not None else NBP2["SFI1DS"]

        # NBI current source density profile - calculated by PENCIL in chain2
        if 'nbp2/jbdc' in m and RD[m['nbp2/jbdc']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["J"] = RD[m['nbp2/jbdc']]
            NBP2["PFNJ"] = np.power(RD["X_"+m['nbp2/jbdc']], 2.0)
            NBP2["JEB"] = 0.05 * NBP2["J"]
            NBP2["JDS"] = RD["I_"+m['nbp2/jbdc']]

        # NBI momentum source density profile - calculated by PENCIL in chain2
        if 'nbp2/torp' in m and RD[m['nbp2/torp']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["TAU"] = RD[m['nbp2/torp']]
            NBP2["PFNTAU"] = np.power(RD["X_"+m['nbp2/torp']], 2.0)
            NBP2["TAUEB"] = 0.05 * NBP2["TAU"]
            NBP2["TAUDS"] = RD["I_"+m['nbp2/torp']]

        # NBI fast ion density profile - calculated by PENCIL in chain2
        if 'nbp2/nf' in m and RD[m['nbp2/nf']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["NFI1"] = RD[m['nbp2/nf']]
            NBP2["PFNNFI1"] = np.power(RD["X_"+m['nbp2/nf']], 2.0)
            NBP2["NFI1EB"] = 0.05 * NBP2["NFI1"]
            NBP2["NFI1DS"] = RD["I_"+m['nbp2/nf']]
        if NBP2 is not None and "NFI1" in NBP2:
            NBP2["NFIA"] = NBP2["NFIA"] + NBP2["NFI1"] if "NFIA" in NBP2 else NBP2["NFI1"].copy()
            if "PFNNFIA" not in NBP2:
                NBP2["PFNNFIA"] = NBP2["PFNNFI1"].copy()
            NBP2["NFIAEB"] = 0.05 * NBP2["NFIA"]
            NBP2["NFIADS"] = NBP2["NFIADS"] + "; " + NBP2["NFI1DS"] if "NFIADS" in NBP2 and NBP2["NFIADS"] is not None else NBP2["NFI1DS"]

        # NBI fast ion energy density profile - calculated by PENCIL in chain2
        if 'nbp2/wpar' in m and RD[m['nbp2/wpar']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["WFI1"] = RD[m['nbp2/wpar']] + NBP2["WFI1"] if "WFI1" in NBP2 else RD[m['nbp2/wpar']]
            if "PFNWFI1" not in NBP2:
                NBP2["PFNWFI1"] = np.power(RD["X_"+m['nbp2/wpar']], 2.0)
            NBP2["WFI1EB"] = 0.05 * NBP2["WFI1"]
            NBP2["WFI1DS"] = NBP2["WFI1DS"] + "; " + RD["I_"+m['nbp2/wpar']] if "WFI1DS" in NBP2 and NBP2["WFI1DS"] is not None else RD["I_"+m['nbp2/wpar']]
        if 'nbp2/wper' in m and RD[m['nbp2/wper']] is not None:
            if NBP2 is None:
                NBP2 = dict()
            NBP2["WFI1"] = RD[m['nbp2/wper']] + NBP2["WFI1"] if "WFI1" in NBP2 else RD[m['nbp2/wper']]
            if "PFNWFI1" not in NBP2:
                NBP2["PFNWFI1"] = np.power(RD["X_"+m['nbp2/wper']], 2.0)
            NBP2["WFI1EB"] = 0.05 * NBP2["WFI1"]
            NBP2["WFI1DS"] = NBP2["WFI1DS"] + "; " + RD["I_"+m['nbp2/wper']] if "WFI1DS" in NBP2 and NBP2["WFI1DS"] is not None else RD["I_"+m['nbp2/wper']]
        if NBP2 is not None and "WFI1" in NBP2:
            NBP2["WFIA"] = NBP2["WFIA"] + NBP2["WFI1"] if "WFIA" in NBP2 else NBP2["WFI1"].copy()
            if "PFNWFIA" not in NBP2:
                NBP2["PFNWFIA"] = NBP2["PFNWFI1"].copy()
            NBP2["WFIAEB"] = 0.05 * NBP2["WFIA"]
            NBP2["WFIADS"] = NBP2["WFIADS"] + "; " + NBP2["WFI1DS"] if "WFIADS" in NBP2 and NBP2["WFIADS"] is not None else NBP2["WFI1DS"]

        # NBI fast ion species info - taken from NBI4 and NBI8
        if NBP2 is not None:
            afi4 = float(np.mean(RD[m['nbi4/gasa']])) if 'nbi4/gasa' in m and RD[m['nbi4/gasa']] is not None else None
            zfi4 = float(np.mean(RD[m['nbi4/gasz']])) if 'nbi4/gasz' in m and RD[m['nbi4/gasz']] is not None else None
            afi8 = float(np.mean(RD[m['nbi8/gasa']])) if 'nbi8/gasa' in m and RD[m['nbi8/gasa']] is not None else None
            zfi8 = float(np.mean(RD[m['nbi8/gasz']])) if 'nbi8/gasz' in m and RD[m['nbi8/gasz']] is not None else None
            NBP2["AFI1"] = None
            NBP2["ZFI1"] = None
            if afi4 is not None and afi4 >= 1.0 and zfi4 is not None and zfi4 >= 1.0:
                NBP2["AFI1"] = float((NBP2["AFI1"] + afi4) / 2.0) if NBP2["AFI1"] is not None else float(afi4)
                NBP2["ZFI1"] = float((NBP2["ZFI1"] + zfi4) / 2.0) if NBP2["ZFI1"] is not None else float(zfi4)
                NBP2["AFI1DS"] = NBP2["AFI1DS"] + "; " + RD["I_"+m['nbi4/gasa']] if "AFI1DS" in NBP2 and NBP2["AFI1DS"] is not None else RD["I_"+m['nbi4/gasa']]
                NBP2["ZFI1DS"] = NBP2["ZFI1DS"] + "; " + RD["I_"+m['nbi4/gasz']] if "ZFI1DS" in NBP2 and NBP2["ZFI1DS"] is not None else RD["I_"+m['nbi4/gasz']]
            if afi8 is not None and afi8 >= 1.0 and zfi8 is not None and zfi8 >= 1.0:
                NBP2["AFI1"] = float((NBP2["AFI1"] + afi8) / 2.0) if NBP2["AFI1"] is not None else float(afi8)
                NBP2["ZFI1"] = float((NBP2["ZFI1"] + zfi8) / 2.0) if NBP2["ZFI1"] is not None else float(zfi8)
                NBP2["AFI1DS"] = NBP2["AFI1DS"] + "; " + RD["I_"+m['nbi8/gasa']] if "AFI1DS" in NBP2 and NBP2["AFI1DS"] is not None else RD["I_"+m['nbi8/gasa']]
                NBP2["ZFI1DS"] = NBP2["ZFI1DS"] + "; " + RD["I_"+m['nbi8/gasz']] if "ZFI1DS" in NBP2 and NBP2["ZFI1DS"] is not None else RD["I_"+m['nbi8/gasz']]
            if NBP2["AFI1"] is not None and not isinstance(NBP2["AFI1"], np.ndarray):
                NBP2["AFI1"] = np.array([NBP2["AFI1"]]).flatten()
            if NBP2["ZFI1"] is not None and not isinstance(NBP2["ZFI1"], np.ndarray):
                NBP2["ZFI1"] = np.array([NBP2["ZFI1"]]).flatten()
            NBP2["CODE"] = "PENCIL"

        PD["NBP2"] = NBP2 if PD["TRA0"] is None else None

        if fdebug and PD["NBP2"] is not None:
            print("process_jet.py: unpack_source_data(): %s NBI source data added." % (PD["NBP2"]["CODE"]))

        # ICRH sources (called RF since ECRH is not available at JET)
        PION = None

        # RF heat source density profile to electrons via direct absorption - calculated by PION in chain2
        if 'pion/pde' in m and RD[m['pion/pde']] is not None:
            if PION is None:
                PION = dict()
            PION["QE"] = RD[m['pion/pde']]
            PION["PFNQE"] = np.power(RD["X_"+m['pion/pde']], 2.0)
            PION["QEEB"] = 0.05 * PION["QE"] + 0.05 * np.nanmean(PION["QE"])
            PION["QEDS"] = PION["QEDS"] + "; " + RD["I_"+m['pion/pde']] if "QEDS" in PION and PION["QEDS"] is not None else RD["I_"+m['pion/pde']]

        # RF heat source density profile to electrons via collisions - calculated by PION in chain2
        if 'pion/pdce' in m and RD[m['pion/pdce']] is not None:
            if PION is None:
                PION = dict()
            PION["QE"] = RD[m['pion/pdce']] + PION["QE"] if "QE" in PION else RD[m['pion/pdce']]
            if "PFNQE" not in PION:
                PION["PFNQE"] = np.power(RD["X_"+m['pion/pdce']], 2.0)
            PION["QEEB"] = 0.05 * PION["QE"] + 0.05 * np.nanmean(PION["QE"])
            PION["QEDS"] = PION["QEDS"] + "; " + RD["I_"+m['pion/pdce']] if "QEDS" in PION and PION["QEDS"] is not None else RD["I_"+m['pion/pdce']]

        # RF heat source density profile to impurity species via direct absorption - calculated by PION in chain2
        if 'pion/pdim' in m and RD[m['pion/pdim']] is not None:
            if PION is None:
                PION = dict()
            PION["QI"] = RD[m['pion/pdim']]
            PION["PFNQI"] = np.power(RD["X_"+m['pion/pdim']], 2.0)
            PION["QIEB"] = 0.05 * PION["QI"] + 0.05 * np.nanmean(PION["QI"])
            PION["QIDS"] = PION["QIDS"] + "; " + RD["I_"+m['pion/pdim']] if "QIDS" in PION and PION["QIDS"] is not None else RD["I_"+m['pion/pdim']]

        # RF heat source density profile to ions via collisions - calculated by PION in chain2
        if 'pion/pdci' in m and RD[m['pion/pdci']] is not None:
            if PION is None:
                PION = dict()
            PION["QI"] = RD[m['pion/pdci']] + PION["QI"] if "QI" in PION else RD[m['pion/pdci']]
            if "PFNQI" not in PION:
                PION["PFNQI"] = np.power(RD["X_"+m['pion/pdci']], 2.0)
            PION["QIEB"] = 0.05 * PION["QI"] + 0.05 * np.nanmean(PION["QI"])
            PION["QIDS"] = PION["QIDS"] + "; " + RD["I_"+m['pion/pdci']] if "QIDS" in PION and PION["QIDS"] is not None else RD["I_"+m['pion/pdci']]

        # RF heat source density profile to fast ion species 1 - calculated by PION in chain2
        if 'pion/pd1' in m and RD[m['pion/pd1']] is not None:
            if PION is None:
                PION = dict()
            PION["QFI1"] = RD[m['pion/pd1']]
            PION["PFNQFI1"] = np.power(RD["X_"+m['pion/pd1']], 2.0)
            PION["QFI1EB"] = 0.05 * PION["QFI1"] + 0.05 * np.nanmean(PION["QFI1"])
            PION["QFI1DS"] = RD["I_"+m['pion/pd1']]

        # RF heat source density profile to fast ion species 2 - calculated by PION in chain2
        if 'pion/pd2' in m and RD[m['pion/pd2']] is not None:
            if PION is None:
                PION = dict()
            PION["QFI2"] = RD[m['pion/pd2']]
            PION["PFNQFI2"] = np.power(RD["X_"+m['pion/pd2']], 2.0)
            PION["QFI2EB"] = 0.05 * PION["QFI2"] + 0.05 * np.nanmean(PION["QFI2"])
            PION["QFI2DS"] = RD["I_"+m['pion/pd2']]

        # RF fast ion density profile for species 1 - calculated by PION in chain2
        if 'pion/nf1' in m and RD[m['pion/nf1']] is not None:
            if PION is None:
                PION = dict()
            PION["NFI1"] = RD[m['pion/nf1']]
            PION["PFNNFI1"] = np.power(RD["X_"+m['pion/nf1']], 2.0)
            PION["NFI1EB"] = 0.05 * PION["NFI1"] + 0.05 * np.nanmean(PION["NFI1"])
            PION["NFI1DS"] = RD["I_"+m['pion/nf1']]
        if PION is not None and "NFI1" in PION:
            PION["NFIA"] = PION["NFIA"] + PION["NFI1"] if "NFIA" in PION else PION["NFI1"].copy()
            if "PFNNFIA" not in PION:
                PION["PFNNFIA"] = PION["PFNNFI1"].copy()
            PION["NFIAEB"] = PION["NFIAEB"] + PION["NFI1EB"] if "NFIAEB" in PION else PION["NFI1EB"].copy()
            PION["NFIADS"] = PION["NFIADS"] + "; " + PION["NFI1DS"] if "NFIADS" in PION and PION["NFIADS"] is not None else PION["NFI1DS"]

        # RF fast ion density profile for species 2 - calculated by PION in chain2
        if 'pion/nf2' in m and RD[m['pion/nf2']] is not None:
            if PION is None:
                PION = dict()
            PION["NFI2"] = RD[m['pion/nf2']]
            PION["PFNNFI2"] = np.power(RD["X_"+m['pion/nf2']], 2.0)
            PION["NFI2EB"] = 0.05 * PION["NFI2"] + 0.05 * np.nanmean(PION["NFI2"])
            PION["NFI2DS"] = RD["I_"+m['pion/nf2']]
        if PION is not None and "NFI2" in PION:
            PION["NFIA"] = PION["NFIA"] + PION["NFI2"] if "NFIA" in PION else PION["NFI2"].copy()
            if "PFNNFIA" not in PION:
                PION["PFNNFIA"] = PION["PFNNFI2"].copy()
            PION["NFIAEB"] = PION["NFIAEB"] + PION["NFI2EB"] if "NFIAEB" in PION else PION["NFI2EB"].copy()
            PION["NFIADS"] = PION["NFIADS"] + "; " + PION["NFI2DS"] if "NFIADS" in PION and PION["NFIADS"] is not None else PION["NFI2DS"]

        # RF fast ion energy density profile - calculated by PION in chain2
        if 'pion/wpar' in m and RD[m['pion/wpar']] is not None:
            if PION is None:
                PION = dict()
            PION["WFIA"] = RD[m['pion/wpar']] + PION["WFIA"] if "WFIA" in PION else RD[m['pion/wpar']]
            if "PFNWFIA" not in PION:
                PION["PFNWFIA"] = np.power(RD["X_"+m['pion/wpar']], 2.0)
            PION["WFIAEB"] = 0.05 * PION["WFIA"] + 0.05 * np.nanmean(PION["WFIA"])
            PION["WFIADS"] = PION["WFIADS"] + "; " + RD["I_"+m['pion/wpar']] if "WFIADS" in PION and PION["WFIADS"] is not None else RD["I_"+m['pion/wpar']]
        if 'pion/wper' in m and RD[m['pion/wper']] is not None:
            if PION is None:
                PION = dict()
            PION["WFIA"] = RD[m['pion/wper']] + PION["WFIA"] if "WFIA" in PION else RD[m['pion/wper']]
            if "PFNWFIA" not in PION:
                PION["PFNWFIA"] = np.power(RD["X_"+m['pion/wper']], 2.0)
            PION["WFIAEB"] = 0.05 * PION["WFIA"] + 0.05 * np.nanmean(PION["WFIA"])
            PION["WFIADS"] = PION["WFIADS"] + "; " + RD["I_"+m['pion/wper']] if "WFIADS" in PION and PION["WFIADS"] is not None else RD["I_"+m['pion/wper']]

        # RF fast ion energy density profile for species 1 - calculated by PION in chain2
        if 'pion/wdf1' in m and RD[m['pion/wdf1']] is not None:
            if PION is None:
                PION = dict()
            PION["WFI1"] = RD[m['pion/wdf1']]
            PION["PFNWFI1"] = np.power(RD["X_"+m['pion/wdf1']], 2.0)
            PION["WFI1EB"] = 0.05 * PION["WFI1"] + 0.05 * np.nanmean(PION["WFI1"])
            PION["WFI1DS"] = RD["I_"+m['pion/wdf1']]

        # RF fast ion energy density profile for species 2 - calculated by PION in chain2
        if 'pion/wdf2' in m and RD[m['pion/wdf2']] is not None:
            if PION is None:
                PION = dict()
            PION["WFI2"] = RD[m['pion/wdf2']]
            PION["PFNWFI2"] = np.power(RD["X_"+m['pion/wdf2']], 2.0)
            PION["WFI2EB"] = 0.05 * PION["WFI2"] + 0.05 * np.nanmean(PION["WFI2"])
            PION["WFI2DS"] = RD["I_"+m['pion/wdf2']]

        # RF fast ion species info - output by PION in chain2
        if PION is not None:
            PION["AFI1"] = np.array([np.mean(RD[m['pion/am1']])]).flatten() if 'pion/am1' in m and RD[m['pion/am1']] is not None else None
            PION["ZFI1"] = np.array([np.mean(RD[m['pion/ach1']])]).flatten() if 'pion/ach1' in m and RD[m['pion/ach1']] is not None else None
            if PION["AFI1"] is not None:
                PION["AFI1DS"] = RD["I_"+m['pion/am1']]
            if PION["ZFI1"] is not None:
                PION["ZFI1DS"] = RD["I_"+m['pion/ach1']]
            PION["AFI2"] = np.array([np.mean(RD[m['pion/am2']])]).flatten() if 'pion/am2' in m and RD[m['pion/am2']] is not None else None
            PION["ZFI2"] = np.array([np.mean(RD[m['pion/ach2']])]).flatten() if 'pion/ach2' in m and RD[m['pion/ach2']] is not None else None
            if PION["AFI2"] is not None:
                PION["AFI2DS"] = RD["I_"+m['pion/am2']]
            if PION["ZFI2"] is not None:
                PION["ZFI2DS"] = RD["I_"+m['pion/ach2']]
            PION["CODE"] = "PION"

        PD["PION"] = PION if PD["TRA0"] is None else None

        if fdebug and PD["PION"] is not None:
            print("process_jet.py: unpack_source_data(): %s ICRH source data added." % (PD["PION"]["CODE"]))

        # Flux surface averaged current density as a function of normalized polodial flux - averaging expected to be done during extraction (really should be handled here)
        tag = 'jsur'
        vtag = "JSURF"        # Flux surface averaged current density(PSI_norm)
        xtag = "PJSURF"
        PD[vtag] = None
        PD[xtag] = None
        for ii in range(len(eqlist)):
            if eqlist[ii]+'/'+tag in m and PD[vtag] is None:
                PD[vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[vtag] is not None else None
                PD[vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]
            if eqlist[ii]+'/'+tag in m and len(eqlist) > 1:
                PD[eqlist[ii].upper()][vtag] = RD[m[eqlist[ii]+'/'+tag]]
                PD[eqlist[ii].upper()][xtag] = RD["X_"+m[eqlist[ii]+'/'+tag]] if PD[eqlist[ii].upper()][vtag] is not None else None
                PD[eqlist[ii].upper()][vtag+"DS"] = RD["I_"+m[eqlist[ii]+'/'+tag]]

        # Flux surface average loop voltage as a function of normalized poloidal flux - averaging expected to be done during extraction (really should be handled here)
        vtag = "VLOOP"        # Loop voltage(PSI_norm) - calculated from time derivative of PSI
        xtag = "PVLOOP"
        PD[vtag] = None
        PD[xtag] = None
        for ii in range(len(eqlist)):
            if PD[vtag] is None:
                fbndl = None
                tbndl = None
                faxsl = None
                taxsl = None
                prov = None
                tag = 'fbnd'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    fbndl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    tbndl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if fbndl is not None else None
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    fbndl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                tag = 'faxs'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    faxsl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    taxsl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if faxsl is not None else None
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    faxsl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                if fbndl is not None and fbndl.size > 1 and faxsl is not None and faxsl.size == fbndl.size and PD["PJSURF"] is not None:
                    fpsi = np.zeros((taxsl.size, PD["PJSURF"].size))
                    for pp in range(fpsi.shape[0]):
                        fpsi[pp, :] = PD["PJSURF"] * (fbndl[pp] - faxsl[pp]) + faxsl[pp]
                    vloop = np.diff(fpsi, axis=0)
                    for qq in range(vloop.shape[1]):
                        vloop[:, qq] = vloop[:, qq] / np.diff(taxsl)
                    verrs = np.zeros(vloop.shape)
                    (PD[vtag], PD[vtag+"EB"], stdmean, nv) = ptools.filtered_average(vloop, value_errors=verrs, use_n=True)
                    PD[vtag+"EB"] = PD[vtag+"EB"] / 7.0   # Heuristic compensation for noise added in time derivative procedure
                    PD[xtag] = PD["PJSURF"].copy() if PD[vtag] is not None else None
                    PD[vtag+"DS"] = prov + "; " + PD["JSURFDS"] if prov is not None else PD["JSURFDS"]
            if len(eqlist) > 1:
                eqtag = eqlist[ii].upper()
                fbndl = None
                tbndl = None
                faxsl = None
                taxsl = None
                prov = None
                tag = 'fbnd'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    fbndl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    tbndl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if fbndl is not None else None
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    fbndl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                tag = 'faxs'
                if eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None and RD[m[eqlist[ii]+'/'+tag]].size > 1:
                    faxsl = RD[m[eqlist[ii]+'/'+tag]].flatten()
                    taxsl = RD["T_"+m[eqlist[ii]+'/'+tag]].flatten() if faxsl is not None else None
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                elif eqlist[ii]+'/'+tag in m and RD[m[eqlist[ii]+'/'+tag]] is not None:
                    faxsl = np.array([RD[m[eqlist[ii]+'/'+tag]]]).flatten()
                    prov = prov + "; " + RD["I_"+m[eqlist[ii]+'/'+tag]] if prov is not None else RD["I_"+m[eqlist[ii]+'/'+tag]]
                if fbndl is not None and fbndl.size > 1 and faxsl is not None and faxsl.size == fbndl.size and PD[eqtag]["PJSURF"] is not None:
                    fpsi = np.zeros((taxsl.size, PD[eqtag]["PJSURF"].size))
                    for pp in range(fpsi.shape[0]):
                        fpsi[pp, :] = psi[pp, :] * (fbndl[pp] - faxsl[pp]) + faxsl[pp]
                    vloop = np.diff(fpsi, axis=0)
                    for qq in range(vloop.shape[1]):
                        vloop[:, qq] = vloop[:, qq] / np.diff(taxsl)
                    verrs = np.zeros(vloop.shape)
                    (PD[eqtag][vtag], PD[eqtag][vtag+"EB"], stdmean, nv) = ptools.filtered_average(vloop, value_errors=verrs, use_n=True)
                    PD[eqtag][vtag+"EB"] = PD[eqtag][vtag+"EB"] / 7.0   # Heuristic compensation for noise added in time derivative procedure
                    PD[eqtag][xtag] = PD[eqtag]["PJSURF"].copy() if PD[eqtag][vtag] is not None else None
                    PD[eqtag][vtag+"DS"] = prov + "; " + PD[eqtag]["JSURFDS"] if prov is not None else PD[eqtag]["JSURFDS"]

        # Ohmic sources
        OHM = None
        if "JSURF" in PD and PD["JSURF"] is not None and "VLOOP" in PD and PD["VLOOP"] is not None:
            if OHM is None:
                OHM = dict()
            circumference = np.abs(2.0 * np.pi * PD["RMAG"]) if "RMAG" in PD and PD["RMAG"] is not None else 6.0 * np.pi
            OHM["J"] = PD["JSURF"].copy()
            OHM["PFNJ"] = PD["PJSURF"].copy()
            OHM["JEB"] = 0.03 * OHM["J"]         # Estimated from data on JEC2020
            OHM["JDS"] = PD["JSURFDS"]
            OHM["QE"] = np.abs(PD["JSURF"] * PD["VLOOP"] / circumference)
            OHM["QEEB"] = np.sqrt(np.power(PD["JSURF"] * PD["VLOOPEB"], 2.0) + np.power(0.03 * PD["JSURF"] * PD["VLOOP"], 2.0)) / circumference
            OHM["PFNQE"] = PD["PJSURF"].copy()
            OHM["QEDS"] = PD["VLOOPDS"]
            OHM["QI"] = np.abs(PD["JSURF"] * PD["VLOOP"] / circumference)
            OHM["QIEB"] = np.sqrt(np.power(PD["JSURF"] * PD["VLOOPEB"], 2.0) + np.power(0.03 * PD["JSURF"] * PD["VLOOP"], 2.0)) / circumference
            OHM["PFNQI"] = PD["PJSURF"].copy()
            OHM["QIDS"] = PD["VLOOPDS"]
            OHM["CODE"] = eqdda.upper()
        PD["OHM"] = OHM #if PD["TRA0"] is None else None   # Should this be ignored if TRA0 exists?

        if fdebug and PD["OHM"] is not None:
            print("process_jet.py: unpack_source_data(): %s ohmic source data added." % (PD["OHM"]["CODE"]))

        # Radiation sources
        RAD = None
        if RAD is None and 'bara/avfl' in m and RD[m['bara/avfl']] is not None and 'bara/psi' in m and RD[m['bara/psi']] is not None:
            if RAD is None:
                RAD = dict()
            RAD["QRAD"] = RD[m['bara/avfl']].flatten()
            RAD["PFNQRAD"] = RD[m['bara/psi']].flatten()
            RAD["QRADEB"] = 0.05 * RAD["QRAD"]
            RAD["QRADDS"] = RD["I_"+m['bara/avfl']] + "; " + RD["I_"+m['bara/psi']]
            RAD["CODE"] = "BARA"
        if RAD is None and 'bolt/avfl' in m and RD[m['bolt/avfl']] is not None and 'bolt/psi' in m and RD[m['bolt/psi']] is not None:
            if RAD is None:
                RAD = dict()
            RAD["QRAD"] = RD[m['bolt/avfl']].flatten()
            RAD["PFNQRAD"] = RD[m['bolt/psi']].flatten()
            RAD["QRADEB"] = 0.05 * RAD["QRAD"]
            RAD["QRADDS"] = RD["I_"+m['bolt/avfl']] + "; " + RD["I_"+m['bolt/psi']]
            RAD["CODE"] = "BOLT"
        PD["RAD"] = RAD

        if fdebug and PD["RAD"] is not None:
            print("process_jet.py: unpack_source_data(): %s radiation source data added." % (PD["RAD"]["CODE"]))

    if fdebug:
        print("process_jet.py: unpack_source_data() completed.")

    return PD


def calc_coords_with_bspline(procdata, cdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Calculates the relations between the various radial coordinate systems to
    be used for further processing. Interpolates all systems to be on the same
    101 point base uniformly spread in normalized poloidal flux coordinate,
    such that conversion between systems can be done easily with linear
    interpolations hereafter. Additionally computes the necessary Jacobians
    in order to convert derivatives with respect to one coordinate system into
    derivative with respect to another coordinate system.

    This function uses smoothed cubic B-splines to perform the necessary
    interpolations, with special care taken to not introduce discontinuities
    in the second derivative of the individual splines. This ensures
    smoothness in the first derivatives to be used for the coordinate
    Jacobians.

    Normalized poloidal flux was chosen to be the base coordinate in this
    implementation simply because most profile information in the JET PPF
    system was already expressed with respect to this coordinate. This reduces
    the overall conversion errors attached to the data.

    Coordinates computed and associated names
        POLFLUXN = Normalized poloidal flux
        POLFLUX  = Absolute poloidal flux
        RHOPOLN  = Normalized poloidal rho (sqrt POLFLUXN)
        TORFLUXN = Normalized toroidal flux
        TORFLUX  = Absolute toroidal flux
        RHOTORN  = Normalized toroidal rho (sqrt TORFLUXN)
        RMAJORO  = Absolute outer major radius at height of magnetic axis
        RMAJORI  = Absolute inner major radius at height of magnetic axis
        RMINORO  = Absolute outer minor radius at height of magnetic axis
        RMINORI  = Absolute inner minor radius at height of magnetic axis
        FSVOL    = Flux surface volume
        FSAREA   = Flux surface cross-sectional area

    Prefix guide
        CS = Coordinate system
        CJ = Coordinate Jacobian
        CE = Coordinate error

    :arg procdata: dict. Implementation-specific object containing processed radial coordinate data.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :returns: dict. Identical to input object with unified coordinate system data inserted.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    if PD is not None:

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqdda = PD["EQLIST"][0].upper()
        eqlist = PD["EQLIST"] if len(PD["EQLIST"]) > 1 else []

        # Print coordinate system data from equilibrium, for debugging purposes
        if cdebug:
            debugdir = './coord_debug/'
            if not os.path.isdir(debugdir):
                os.makedirs(debugdir)
            if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
                with open(debugdir+'tfx.txt', 'w') as ff1:
                    for ii in range(PD["TFX"].size):
                        ff1.write("   %15.6e   %15.6e\n" % (PD["PFN2TFX"][ii], PD["TFX"][ii]))
            if "RMJO" in PD and "RMJI" in PD and PD["RMJO"] is not None and PD["RMJI"] is not None:
                with open(debugdir+'pfn.txt', 'w') as ff2:
                    psin = np.hstack((PD["PFN2RMI"][:0:-1], PD["PFN2RMO"]))
                    rmaj = np.hstack((PD["RMJI"][:0:-1], PD["RMJO"]))
                    for ii in range(psin.size):
                        ff2.write("   %15.6e   %15.6e\n" % (rmaj[ii], psin[ii]))
            if "VOL" in PD and PD["VOL"] is not None:
                with open(debugdir+'vol.txt', 'w') as ff3:
                    for ii in range(PD["VOL"].size):
                        ff3.write("   %15.6e   %15.6e\n" % (PD["PDVOL"][ii], PD["VOL"][ii]))
            if "AREA" in PD and PD["AREA"] is not None:
                with open(debugdir+'area.txt', 'w') as ff4:
                    for ii in range(PD["AREA"].size):
                        ff4.write("   %15.6e   %15.6e\n" % (PD["PDAREA"][ii], PD["AREA"][ii]))

        # Definition of the base coordinate system
        csb = "POLFLUXN"
        cjb = "POLFLUXN"
        PD["CS_PREFIXES"] = [""]
        PD["CS_BASE"] = csb
        PD["CJ_BASE"] = cjb
        PD["CS_"+csb] = np.linspace(0.0, 1.0, 101)
        PD["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD["CS_"+csb].shape)
        PD["CE_"+csb] = np.zeros(PD["CS_"+csb].shape)

        # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
        PD["CS_RHOPOLN"] = np.sqrt(PD["CS_POLFLUXN"])
        PD["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD["CS_RHOPOLN"]

        # Calculating absolute poloidal flux coordinate
        if "FBND" in PD and "FAXS" in PD and PD["FBND"] is not None and PD["FAXS"] is not None:
            PD["CS_POLFLUX"] = PD["CS_POLFLUXN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
            PD["CJ_"+cjb+"_POLFLUX"] = np.ones(PD["CS_"+csb].shape) / (PD["FBND"] - PD["FAXS"])
            PD["CE_POLFLUX"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_POLFLUX"] = None
            PD["CJ_"+cjb+"_POLFLUX"] = None
            PD["CE_POLFLUX"] = None

        # Calculating absolute toroidal flux coordinate and related coordinates
        if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
            xdata = PD["PFN2TFX"]
            ydata = PD["TFX"]
            spvals = splrep(xdata, ydata)
            PD["CS_TORFLUX"] = splev(PD["CS_"+csb], spvals, der=0)
            PD["CS_TORFLUX"][0] = 0.0
            PD["CJ_"+cjb+"_TORFLUX"] = 1.0 / splev(PD["CS_"+csb], spvals, der=1)
            PD["CE_TORFLUX"] = np.zeros(PD["CS_"+csb].shape)
            tbnd = PD["TBND"] if "TBND" in PD and PD["TBND"] is not None and np.abs(PD["TBND"]) > 0.0 else PD["CS_TORFLUX"][-1]
            PD["CS_TORFLUXN"] = PD["CS_TORFLUX"] / tbnd
            PD["CJ_"+cjb+"_TORFLUXN"] = PD["CJ_"+cjb+"_TORFLUX"] * tbnd
            PD["CE_TORFLUXN"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RHOTORN"] = np.sqrt(PD["CS_TORFLUXN"])
            PD["CJ_"+cjb+"_RHOTORN"] = 2.0 * PD["CS_RHOTORN"] * PD["CJ_"+cjb+"_TORFLUXN"]
            PD["CE_RHOTORN"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_TORFLUX"] = None
            PD["CS_TORFLUXN"] = None
            PD["CS_RHOTORN"] = None
            PD["CJ_"+cjb+"_TORFLUX"] = None
            PD["CJ_"+cjb+"_TORFLUXN"] = None
            PD["CJ_"+cjb+"_RHOTORN"] = None
            PD["CE_TORFLUX"] = None
            PD["CE_TORFLUXN"] = None
            PD["CE_RHOTORN"] = None

        # Calculating absolute major and minor radius coordinates and related coordinates
        if "RMJO" in PD and "RMJI" in PD and PD["RMJO"] is not None and PD["RMJI"] is not None:
            xdata = np.hstack((-PD["PFN2RMI"][:0:-1], PD["PFN2RMO"]))
            ydata = np.hstack((PD["RMJI"][:0:-1], PD["RMJO"]))
            spvals = splrep(xdata, ydata)
            PD["CS_RMAJORO"] = splev(PD["CS_"+csb], spvals, der=0)
            PD["CS_RMAJORO"][0] = PD["RMJO"][0]
            PD["CJ_"+cjb+"_RMAJORO"] = 1.0 / splev(PD["CS_"+csb], spvals, der=1)
            PD["CE_RMAJORO"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RMAJORI"] = splev(-PD["CS_"+csb], spvals, der=0)
            PD["CS_RMAJORI"][0] = PD["RMJO"][0]
            PD["CJ_"+cjb+"_RMAJORI"] = 1.0 / splev(-PD["CS_"+csb], spvals, der=1)
            PD["CE_RMAJORI"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RMINORO"] = np.abs(PD["CS_RMAJORO"] - PD["CS_RMAJORO"][0])
            PD["CS_RMINORI"] = np.abs(PD["CS_RMAJORI"] - PD["CS_RMAJORI"][0])
            PD["CJ_"+cjb+"_RMINORO"] = PD["CJ_"+cjb+"_RMAJORO"]
            PD["CJ_"+cjb+"_RMINORI"] = -PD["CJ_"+cjb+"_RMAJORI"]
            PD["CE_RMINORO"] = np.zeros(PD["CS_"+csb].shape)
            PD["CE_RMINORI"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_RMAJORO"] = None
            PD["CS_RMAJORI"] = None
            PD["CJ_"+cjb+"_RMAJORO"] = None
            PD["CJ_"+cjb+"_RMAJORI"] = None
            PD["CE_RMAJORO"] = None
            PD["CE_RMAJORI"] = None
            PD["CS_RMINORO"] = None
            PD["CS_RMINORI"] = None
            PD["CJ_"+cjb+"_RMINORO"] = None
            PD["CJ_"+cjb+"_RMINORI"] = None
            PD["CE_RMINORO"] = None
            PD["CE_RMINORI"] = None

        # Calculating flux surface volume coordinate
        if "VOL" in PD and PD["VOL"] is not None:
            xdata = PD["PDVOL"]
            ydata = PD["VOL"]
            spvals = splrep(xdata, ydata)
            PD["CS_FSVOL"] = splev(PD["CS_"+csb], spvals, der=0)
            PD["CJ_"+cjb+"_FSVOL"] = 1.0 / splev(PD["CS_"+csb], spvals, der=1)
            PD["CE_FSVOL"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_FSVOL"] = None
            PD["CJ_"+cjb+"_FSVOL"] = None
            PD["CE_FSVOL"] = None

        # Calculating flux surface cross-sectional area coordinate
        if "AREA" in PD and PD["AREA"] is not None:
            xdata = PD["PDAREA"]
            ydata = PD["AREA"]
            spvals = splrep(xdata, ydata)
            PD["CS_FSAREA"] = splev(PD["CS_"+csb], spvals, der=0)
            PD["CJ_"+cjb+"_FSAREA"] = 1.0 / splev(PD["CS_"+csb], spvals, der=1)
            PD["CE_FSAREA"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_FSAREA"] = None
            PD["CJ_"+cjb+"_FSAREA"] = None
            PD["CE_FSAREA"] = None

        # Performs identical coordinate system fitting for all magnetic equilibria, if specified, and stores it in a separate index
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqtag in PD and PD[eqtag] is not None:

                # Definition of the base coordinate system
                PD[eqtag]["CS_BASE"] = csb
                PD[eqtag]["CJ_BASE"] = cjb
                PD[eqtag]["CS_"+csb] = np.linspace(0.0, 1.0, 101)
                PD[eqtag]["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD[eqtag]["CS_"+csb].shape)
                PD[eqtag]["CE_"+csb] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
                PD[eqtag]["CS_RHOPOLN"] = np.sqrt(PD[eqtag]["CS_POLFLUXN"])
                PD[eqtag]["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD[eqtag]["CS_RHOPOLN"]
                PD[eqtag]["CE_RHOPOLN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating absolute poloidal flux coordinate
                if "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag]["CS_POLFLUX"] = PD[eqtag]["CS_POLFLUXN"] * (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"]) + PD[eqtag]["FAXS"]
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = np.ones(PD[eqtag]["CS_POLFLUXN"].shape) / (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"])
                    PD[eqtag]["CE_POLFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_POLFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = None
                    PD[eqtag]["CE_POLFLUX"] = None

                # Calculating absolute toroidal flux coordinate and related coordinates
                if "TFX" in PD[eqtag] and "PFN2TFX" in PD[eqtag] and PD[eqtag]["TFX"] is not None and PD[eqtag]["PFN2TFX"] is not None:
                    xdata = PD[eqtag]["PFN2TFX"]
                    ydata = PD[eqtag]["TFX"]
                    spvals = splrep(xdata, ydata)
                    PD[eqtag]["CS_POLFLUX"] = splev(PD[eqtag]["CS_"+csb], spvals, der=0)
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = 1.0 / splev(PD[eqtag]["CS_"+csb], spvals, der=1)
                    PD[eqtag]["CE_POLFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_TORFLUX"] = None
                    PD[eqtag]["CS_TORFLUXN"] = None
                    PD[eqtag]["CS_RHOTORN"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUXN"] = None
                    PD[eqtag]["CJ_"+cjb+"_RHOTORN"] = None
                    PD[eqtag]["CE_TORFLUX"] = None
                    PD[eqtag]["CE_TORFLUXN"] = None
                    PD[eqtag]["CE_RHOTORN"] = None

                # Calculating absolute major and minor radius coordinates and related coordinates
                if "RMJO" in PD[eqtag] and "RMJI" in PD[eqtag] and PD[eqtag]["RMJO"] is not None and PD[eqtag]["RMJI"] is not None:
                    xdata = np.hstack((-PD[eqtag]["PFN2RMI"][:0:-1], PD[eqtag]["PFN2RMO"]))
                    ydata = np.hstack((PD[eqtag]["RMJI"][:0:-1], PD[eqtag]["RMJO"]))
                    spvals = splrep(xdata, ydata)
                    PD[eqtag]["CS_RMAJORO"] = splev(PD[eqtag]["CS_"+csb], spvals, der=0)
                    PD[eqtag]["CS_RMAJORO"][0] = PD[eqtag]["RMJO"][0]
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = 1.0 / splev(PD[eqtag]["CS_"+csb], spvals, der=1)
                    PD[eqtag]["CE_RMAJORO"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CS_RMAJORI"] = splev(-PD[eqtag]["CS_"+csb], spvals, der=0)
                    PD[eqtag]["CS_RMAJORI"][0] = PD[eqtag]["RMJO"][0]
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = 1.0 / splev(-PD[eqtag]["CS_"+csb], spvals, der=1)
                    PD[eqtag]["CE_RMAJORI"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CS_RMINORO"] = np.abs(PD[eqtag]["CS_RMAJORO"] - PD[eqtag]["CS_RMAJORO"][0])
                    PD[eqtag]["CS_RMINORI"] = np.abs(PD[eqtag]["CS_RMAJORI"] - PD[eqtag]["CS_RMAJORI"][0])
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = PD[eqtag]["CJ_"+cjb+"_RMAJORO"]
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = -PD[eqtag]["CJ_"+cjb+"_RMAJORI"]
                    PD[eqtag]["CE_RMINORO"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CE_RMINORI"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_RMAJORO"] = None
                    PD[eqtag]["CS_RMAJORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = None
                    PD[eqtag]["CE_RMAJORO"] = None
                    PD[eqtag]["CE_RMAJORI"] = None
                    PD[eqtag]["CS_RMINORO"] = None
                    PD[eqtag]["CS_RMINORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = None
                    PD[eqtag]["CE_RMINORO"] = None
                    PD[eqtag]["CE_RMINORI"] = None

                # Calculating flux surface volume coordinate
                if "VOL" in PD[eqtag] and PD[eqtag]["VOL"] is not None:
                    xdata = PD[eqtag]["PDVOL"]
                    ydata = PD[eqtag]["VOL"]
                    spvals = splrep(xdata, ydata)
                    PD[eqtag]["CS_FSVOL"] = splev(PD[eqtag]["CS_"+csb], spvals, der=0)
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = 1.0 / splev(PD[eqtag]["CS_"+csb], spvals, der=1)
                    PD[eqtag]["CE_FSVOL"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_FSVOL"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = None
                    PD[eqtag]["CE_FSVOL"] = None

                # Calculating flux surface cross-sectional area coordinate
                if "AREA" in PD[eqtag] and PD[eqtag]["AREA"] is not None:
                    xdata = PD[eqtag]["PDAREA"]
                    ydata = PD[eqtag]["AREA"]
                    spvals = splrep(xdata, ydata)
                    PD[eqtag]["CS_FSAREA"] = splev(PD[eqtag]["CS_"+csb], spvals, der=0)
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = 1.0 / splev(PD[eqtag]["CS_"+csb], spvals, der=1)
                    PD[eqtag]["CE_FSAREA"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_FSAREA"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = None
                    PD[eqtag]["CE_FSAREA"] = None

    return PD


def calc_coords_with_gp(procdata, cdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Calculates the relations between the various radial coordinate systems to
    be used for further processing. Interpolates all systems to be on the same
    101 point base uniformly spread in normalized poloidal flux coordinate,
    such that conversion between systems can be done easily with linear
    interpolations hereafter. Additionally computes the necessary Jacobians
    in order to convert derivatives with respect to one coordinate system into
    derivative with respect to another coordinate system.

    This function uses the Gaussian process regression tool, GPR1D, to perform
    the necessary interpolations. This algorithm takes longer than traditional
    methods but has yielded more useable results without tweaking the free
    parameters of the fit, especially in the Jacobians.

    Normalized poloidal flux was chosen to be the base coordinate in this
    implementation simply because most profile information in the JET PPF
    system was already expressed with respect to this coordinate. This reduces
    the overall conversion errors attached to the data.

    Coordinates computed and associated names
        POLFLUXN = Normalized poloidal flux
        POLFLUX  = Absolute poloidal flux
        RHOPOLN  = Normalized poloidal rho (sqrt POLFLUXN)
        TORFLUXN = Normalized toroidal flux
        TORFLUX  = Absolute toroidal flux
        RHOTORN  = Normalized toroidal rho (sqrt TORFLUXN)
        RMAJORO  = Absolute outer major radius at height of magnetic axis
        RMAJORI  = Absolute inner major radius at height of magnetic axis
        RMINORO  = Absolute outer minor radius at height of magnetic axis
        RMINORI  = Absolute inner minor radius at height of magnetic axis
        FSVOL    = Flux surface volume
        FSAREA   = Flux surface cross-sectional area

    Prefix guide
        CS = Coordinate system
        CJ = Coordinate Jacobian
        CE = Coordinate error

    :arg procdata: dict. Implementation-specific object containing processed radial coordinate data.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :returns: dict. Identical to input object with unified coordinate system data inserted.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    grabfit = itemgetter(0)
    grabGPR = itemgetter(0, 4)

    if PD is not None:

        # Specification of which magnetic equilibrium entry to use, default is standard EFIT
        eqdda = PD["EQLIST"][0].upper()
        eqlist = PD["EQLIST"] if len(PD["EQLIST"]) > 1 else []

        # Print coordinate system data from equilibrium, for debugging purposes
        if cdebug:
            debugdir = './coord_debug/'
            if not os.path.isdir(debugdir):
                os.makedirs(debugdir)
            if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
                with open(debugdir+'tfx.txt', 'w') as ff1:
                    for ii in range(PD["TFX"].size):
                        ff1.write("   %15.6e   %15.6e\n" % (PD["PFN2TFX"][ii], PD["TFX"][ii]))
            if "RMJO" in PD and "RMJI" in PD and PD["RMJO"] is not None and PD["RMJI"] is not None:
                with open(debugdir+'pfn.txt', 'w') as ff2:
                    psin = np.hstack((PD["PFN2RMI"][:0:-1], PD["PFN2RMO"]))
                    rmaj = np.hstack((PD["RMJI"][:0:-1], PD["RMJO"]))
                    for ii in range(psin.size):
                        ff2.write("   %15.6e   %15.6e\n" % (rmaj[ii], psin[ii]))
            if "VOL" in PD and PD["VOL"] is not None:
                with open(debugdir+'vol.txt', 'w') as ff3:
                    for ii in range(PD["VOL"].size):
                        ff3.write("   %15.6e   %15.6e\n" % (PD["PDVOL"][ii], PD["VOL"][ii]))
            if "AREA" in PD and PD["AREA"] is not None:
                with open(debugdir+'area.txt', 'w') as ff4:
                    for ii in range(PD["AREA"].size):
                        ff4.write("   %15.6e   %15.6e\n" % (PD["PDAREA"][ii], PD["AREA"][ii]))

        CSO = ptools.gp.GaussianProcessRegression1D()       # Initializing custom object for coordinate system fitting

        # Definition of the base coordinate system
        csb = "POLFLUXN"
        cjb = "POLFLUXN"
        PD["CS_PREFIXES"] = [""]
        PD["CS_BASE"] = csb
        PD["CJ_BASE"] = cjb
        PD["CS_"+csb] = np.linspace(0.0, 1.0, 101)
        PD["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD["CS_"+csb].shape)
        PD["CE_"+csb] = np.zeros(PD["CS_"+csb].shape)

        # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
        PD["CS_RHOPOLN"] = np.sqrt(PD["CS_POLFLUXN"])
        PD["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD["CS_RHOPOLN"]

        # Calculating absolute poloidal flux coordinate
        if "FBND" in PD and "FAXS" in PD and PD["FBND"] is not None and PD["FAXS"] is not None:
            PD["CS_POLFLUX"] = PD["CS_POLFLUXN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
            PD["CJ_"+cjb+"_POLFLUX"] = np.ones(PD["CS_"+csb].shape) / (PD["FBND"] - PD["FAXS"])
            PD["CE_POLFLUX"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_POLFLUX"] = None
            PD["CJ_"+cjb+"_POLFLUX"] = None
            PD["CE_POLFLUX"] = None

        # Calculating absolute toroidal flux coordinate and related coordinates
        if "TFX" in PD and "PFN2TFX" in PD and PD["TFX"] is not None:
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Noise_Kernel(1.0e-3))
            (PD["CS_TORFLUX"], nkk) = \
                  grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"], kernel=kk, xdata=PD["PFN2TFX"], ydata=PD["TFX"], yerr='None', epsilon=1.0e-1, method='adadelta', spars=[1.0e-2, 0.9]))
            PD["CS_TORFLUX"][0] = 0.0
            npfn = np.linspace(0.0, 1.0, 11)
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(npfn, kernel=nkk, xdata=PD["PFN2TFX"], ydata=PD["TFX"], epsilon='None', do_drv=True))
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 3.0e-1, 1.0e0), ptools.gp.Noise_Kernel(1.0e-3))
            PD["CJ_"+cjb+"_TORFLUX"] = \
                  grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"], kernel=kk, xdata=npfn, ydata=1.0/gpdy, epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
            PD["CE_TORFLUX"] = np.zeros(PD["CS_"+csb].shape)
            tbnd = PD["TBND"] if "TBND" in PD and PD["TBND"] is not None and np.abs(PD["TBND"]) > 0.0 else PD["CS_TORFLUX"][-1]
            PD["CS_TORFLUXN"] = PD["CS_TORFLUX"] / tbnd
            PD["CJ_"+cjb+"_TORFLUXN"] = PD["CJ_"+cjb+"_TORFLUX"] * tbnd
            PD["CE_TORFLUXN"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RHOTORN"] = np.sqrt(PD["CS_TORFLUXN"])
            PD["CJ_"+cjb+"_RHOTORN"] = 2.0 * PD["CS_RHOTORN"] * PD["CJ_"+cjb+"_TORFLUXN"]
            PD["CE_RHOTORN"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_TORFLUX"] = None
            PD["CS_TORFLUXN"] = None
            PD["CS_RHOTORN"] = None
            PD["CJ_"+cjb+"_TORFLUX"] = None
            PD["CJ_"+cjb+"_TORFLUXN"] = None
            PD["CJ_"+cjb+"_RHOTORN"] = None
            PD["CE_TORFLUX"] = None
            PD["CE_TORFLUXN"] = None
            PD["CE_RHOTORN"] = None

        # Calculating absolute major and minor radius coordinates and related coordinates
        if "RMJO" in PD and "RMJI" in PD and PD["RMJO"] is not None and PD["RMJI"] is not None:
            psin = np.hstack((PD["PFN2RMI"][:0:-1], PD["PFN2RMO"]))
            rmaj = np.hstack((PD["RMJI"][:0:-1], PD["RMJO"]))
            nrmj = np.linspace(np.nanmin(rmaj), np.nanmax(rmaj), 301)
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 5.0e-2, 1.0e0), ptools.gp.Noise_Kernel(1.0e-2))
            (gpy, nkk) = grabGPR(CSO._GaussianProcessRegression1D__basic_fit(nrmj, kernel=kk, xdata=rmaj, ydata=psin, epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
            idx = np.where(np.diff(gpy[:-1]) * np.diff(gpy[1:]) <= 0.0)[0][0] + 1
            if "RMAG" in PD and PD["RMAG"] is not None:
                if np.abs(nrmj[idx] - PD["RMAG"]) > 1.0e-2:
                    PD["RMAG"] = float(nrmj[idx])
            else:
                PD["RMAG"] = float(nrmj[idx])
            rmjifunc = interp1d(gpy[:idx+1], nrmj[:idx+1], bounds_error=False, fill_value='extrapolate')
            rmjofunc = interp1d(gpy[idx:], nrmj[idx:], bounds_error=False, fill_value='extrapolate')
            PD["CS_RMAJORO"] = rmjofunc(PD["CS_POLFLUXN"])
            PD["CS_RMAJORO"][0] = PD["RMAG"]
            PD["CS_RMAJORI"] = rmjifunc(PD["CS_POLFLUXN"])
            PD["CS_RMAJORI"][0] = PD["RMAG"]
            rgeotemp = (PD["CS_RMAJORO"][-1] + PD["CS_RMAJORI"][-1]) / 2.0
            if "RGEO" in PD and PD["RGEO"] is not None:
                if np.abs(rgeotemp - PD["RGEO"]) > 1.0e-2:
                    PD["RGEO"] = float(rgeotemp)
            else:
                PD["RGEO"] = float(rgeotemp)
            nrmj = np.hstack((PD["CS_RMAJORI"][:0:-1], PD["CS_RMAJORO"]))
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(nrmj, kernel=nkk, xdata=rmaj, ydata=psin, epsilon='None', do_drv=True))
            PD["CJ_"+cjb+"_RMAJORO"] = gpdy[PD["CS_POLFLUXN"].size-1:]
            PD["CJ_"+cjb+"_RMAJORO"][0] = 0.0
            PD["CJ_"+cjb+"_RMAJORI"] = gpdy[PD["CS_POLFLUXN"].size-1::-1]
            PD["CJ_"+cjb+"_RMAJORI"][0] = 0.0
            PD["CE_RMAJORO"] = np.zeros(PD["CS_"+csb].shape)
            PD["CE_RMAJORI"] = np.zeros(PD["CS_"+csb].shape)
            PD["CS_RMINORO"] = np.abs(PD["CS_RMAJORO"] - PD["CS_RMAJORO"][0])
            PD["CS_RMINORI"] = np.abs(PD["CS_RMAJORI"] - PD["CS_RMAJORI"][0])
            PD["CJ_"+cjb+"_RMINORO"] = PD["CJ_"+cjb+"_RMAJORO"]
            PD["CJ_"+cjb+"_RMINORI"] = -PD["CJ_"+cjb+"_RMAJORI"]
            PD["CE_RMINORO"] = np.zeros(PD["CS_"+csb].shape)
            PD["CE_RMINORI"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_RMAJORO"] = None
            PD["CS_RMAJORI"] = None
            PD["CJ_"+cjb+"_RMAJORO"] = None
            PD["CJ_"+cjb+"_RMAJORI"] = None
            PD["CE_RMAJORO"] = None
            PD["CE_RMAJORI"] = None
            PD["CS_RMINORO"] = None
            PD["CS_RMINORI"] = None
            PD["CJ_"+cjb+"_RMINORO"] = None
            PD["CJ_"+cjb+"_RMINORI"] = None
            PD["CE_RMINORO"] = None
            PD["CE_RMINORI"] = None

        # Calculating flux surface volume coordinate
        if "VOL" in PD and PD["VOL"] is not None:
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Linear_Kernel(1.0e1), ptools.gp.Noise_Kernel(1.0e-3))
            (PD["CS_FSVOL"], nkk) = \
                  grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"], kernel=kk, xdata=PD["PDVOL"], ydata=PD["VOL"], epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"], kernel=nkk, xdata=PD["PDVOL"], ydata=PD["VOL"], epsilon='None', do_drv=True))
            PD["CJ_"+cjb+"_FSVOL"] = 1.0 / gpdy
            PD["CE_FSVOL"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_FSVOL"] = None
            PD["CJ_"+cjb+"_FSVOL"] = None
            PD["CE_FSVOL"] = None

        # Calculating flux surface cross-sectional area coordinate
        if "AREA" in PD and PD["AREA"] is not None:
            kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Linear_Kernel(1.0e1), ptools.gp.Noise_Kernel(1.0e-3))
            (PD["CS_FSAREA"], nkk) = \
                  grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"], kernel=kk, xdata=PD["PDAREA"], ydata=PD["AREA"], epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
            gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD["CS_POLFLUXN"], kernel=nkk, xdata=PD["PDAREA"], ydata=PD["AREA"], epsilon='None', do_drv=True))
            PD["CJ_"+cjb+"_FSAREA"] = 1.0 / gpdy
            PD["CE_FSAREA"] = np.zeros(PD["CS_"+csb].shape)
        else:
            PD["CS_FSAREA"] = None
            PD["CJ_"+cjb+"_FSAREA"] = None
            PD["CE_FSAREA"] = None

        # Performs identical coordinate system fitting for all magnetic equilibria, if specified, and stores it in a separate index
        for ii in range(len(eqlist)):
            eqtag = eqlist[ii].upper()
            if eqtag in PD and PD[eqtag] is not None:

                # Definition of the base coordinate system
                PD[eqtag]["CS_BASE"] = csb
                PD[eqtag]["CJ_BASE"] = cjb
                PD[eqtag]["CS_"+csb] = np.linspace(0.0, 1.0, 101)
                PD[eqtag]["CJ_"+cjb+"_POLFLUXN"] = np.ones(PD[eqtag]["CS_"+csb].shape)
                PD[eqtag]["CE_"+csb] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating poloidal flux label coordinate - square-root normalized poloidal flux
                PD[eqtag]["CS_RHOPOLN"] = np.sqrt(PD[eqtag]["CS_POLFLUXN"])
                PD[eqtag]["CJ_"+cjb+"_RHOPOLN"] = 2.0 * PD[eqtag]["CS_RHOPOLN"]
                PD[eqtag]["CE_RHOPOLN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)

                # Calculating absolute poloidal flux coordinate
                if "FBND" in PD[eqtag] and "FAXS" in PD[eqtag] and PD[eqtag]["FBND"] is not None and PD[eqtag]["FAXS"] is not None:
                    PD[eqtag]["CS_POLFLUX"] = PD[eqtag]["CS_POLFLUXN"] * (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"]) + PD[eqtag]["FAXS"]
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = np.ones(PD[eqtag]["CS_POLFLUXN"].shape) / (PD[eqtag]["FBND"] - PD[eqtag]["FAXS"])
                    PD[eqtag]["CE_POLFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_POLFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_POLFLUX"] = None
                    PD[eqtag]["CE_POLFLUX"] = None

                # Calculating absolute toroidal flux coordinate and related coordinates
                if "TFX" in PD[eqtag] and "PFN2TFX" in PD[eqtag] and PD[eqtag]["TFX"] is not None and PD[eqtag]["PFN2TFX"] is not None:
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Noise_Kernel(1.0e-3))
                    (PD[eqtag]["CS_TORFLUX"], nkk) = \
                          grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"], kernel=kk, xdata=PD[eqtag]["PFN2TFX"], ydata=PD[eqtag]["TFX"], epsilon=1.0e-1, method='adadelta', spars=[1.0e-2, 0.9]))
                    PD[eqtag]["CS_TORFLUX"][0] = 0.0
                    npfn = np.linspace(0.0, 1.0, 11)
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(npfn, kernel=nkk, xdata=PD[eqtag]["PFN2TFX"], ydata=PD[eqtag]["TFX"], epsilon='None', do_drv=True))
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 3.0e-1, 1.0e0), ptools.gp.Noise_Kernel(1.0e-3))
                    PD[eqtag]["CJ_"+cjb+"_TORFLUX"] = \
                          grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"], kernel=kk, xdata=npfn, ydata=1.0/gpdy, epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
                    PD[eqtag]["CE_TORFLUX"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    tbnd = PD["TBND"] if "TBND" in PD and PD["TBND"] is not None and np.abs(PD["TBND"]) > 0.0 else PD["CS_TORFLUX"][-1]
                    PD[eqtag]["CS_TORFLUXN"] = PD[eqtag]["CS_TORFLUX"] / tbnd
                    PD[eqtag]["CJ_"+cjb+"_TORFLUXN"] = PD[eqtag]["CJ_"+cjb+"_TORFLUX"] * tbnd
                    PD[eqtag]["CE_TORFLUXN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CS_RHOTORN"] = np.sqrt(PD[eqtag]["CS_TORFLUXN"])
                    PD[eqtag]["CJ_"+cjb+"_RHOTORN"] = 2.0 * PD[eqtag]["CS_RHOTORN"] * PD[eqtag]["CJ_"+cjb+"_TORFLUXN"]
                    PD[eqtag]["CE_RHOTORN"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_TORFLUX"] = None
                    PD[eqtag]["CS_TORFLUXN"] = None
                    PD[eqtag]["CS_RHOTORN"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUX"] = None
                    PD[eqtag]["CJ_"+cjb+"_TORFLUXN"] = None
                    PD[eqtag]["CJ_"+cjb+"_RHOTORN"] = None
                    PD[eqtag]["CE_TORFLUX"] = None
                    PD[eqtag]["CE_TORFLUXN"] = None
                    PD[eqtag]["CE_RHOTORN"] = None

                # Calculating absolute major and minor radius coordinates and related coordinates
                if "RMJO" in PD[eqtag] and "RMJI" in PD[eqtag] and PD[eqtag]["RMJO"] is not None and PD[eqtag]["RMJI"] is not None:
                    psin = np.hstack((PD[eqtag]["PFN2RMI"][:0:-1], PD[eqtag]["PFN2RMO"]))
                    rmaj = np.hstack((PD[eqtag]["RMJI"][:0:-1], PD[eqtag]["RMJO"]))
                    nrmj = np.linspace(np.nanmin(rmaj), np.nanmax(rmaj), 301)
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Noise_Kernel(1.0e-2))
                    (gpy, nkk) = grabGPR(CSO._GaussianProcessRegression1D__basic_fit(nrmj, kernel=kk, xdata=rmaj, ydata=psin, epsilon=1.0e-1, method='adadelta', spars=[1.0e-2, 0.9]))
                    idx = np.where(np.diff(gpy[:-1]) * np.diff(gpy[1:]) <= 0.0)[0][0] + 1
                    if "RMAG" in PD[eqtag] and PD[eqtag]["RMAG"] is not None:
                        if np.abs(nrmj[idx] - PD[eqtag]["RMAG"]) > 1.0e-2:
                            PD[eqtag]["RMAG"] = float(nrmj[idx])
                    else:
                        PD[eqtag]["RMAG"] = float(nrmj[idx])
                    rmjifunc = interp1d(gpy[:idx+1], nrmj[:idx+1], bounds_error=False, fill_value='extrapolate')
                    rmjofunc = interp1d(gpy[idx:], nrmj[idx:], bounds_error=False, fill_value='extrapolate')
                    PD[eqtag]["CS_RMAJORO"] = rmjofunc(PD[eqtag]["CS_POLFLUXN"])
                    PD[eqtag]["CS_RMAJORO"][0] = PD[eqtag]["RMAG"]
                    PD[eqtag]["CS_RMAJORI"] = rmjifunc(PD[eqtag]["CS_POLFLUXN"])
                    PD[eqtag]["CS_RMAJORI"][0] = PD[eqtag]["RMAG"]
                    nrmj = np.hstack((PD[eqtag]["CS_RMAJORI"][:0:-1], PD[eqtag]["CS_RMAJORO"]))
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(nrmj, kernel=nkk, xdata=rmaj, ydata=psin, epsilon='None', do_drv=True))
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = gpdy[PD[eqtag]["CS_POLFLUXN"].size-1:]
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"][0] = 0.0
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = gpdy[PD[eqtag]["CS_POLFLUXN"].size-1::-1]
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"][0] = 0.0
                    PD[eqtag]["CE_RMAJORO"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CE_RMAJORI"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CS_RMINORO"] = np.abs(PD[eqtag]["CS_RMAJORO"] - PD[eqtag]["CS_RMAJORO"][0])
                    PD[eqtag]["CS_RMINORI"] = np.abs(PD[eqtag]["CS_RMAJORI"] - PD[eqtag]["CS_RMAJORI"][0])
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = PD[eqtag]["CJ_"+cjb+"_RMAJORO"]
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = -PD[eqtag]["CJ_"+cjb+"_RMAJORI"]
                    PD[eqtag]["CE_RMINORO"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                    PD[eqtag]["CE_RMINORI"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_RMAJORO"] = None
                    PD[eqtag]["CS_RMAJORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMAJORI"] = None
                    PD[eqtag]["CE_RMAJORO"] = None
                    PD[eqtag]["CE_RMAJORI"] = None
                    PD[eqtag]["CS_RMINORO"] = None
                    PD[eqtag]["CS_RMINORI"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORO"] = None
                    PD[eqtag]["CJ_"+cjb+"_RMINORI"] = None
                    PD[eqtag]["CE_RMINORO"] = None
                    PD[eqtag]["CE_RMINORI"] = None

                # Calculating flux surface volume coordinate
                if "VOL" in PD[eqtag] and PD[eqtag]["VOL"] is not None:
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Linear_Kernel(1.0e1), ptools.gp.Noise_Kernel(1.0e-3))
                    (PD[eqtag]["CS_FSVOL"], nkk) = \
                          grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"], kernel=kk, xdata=PD[eqtag]["PDVOL"], ydata=PD[eqtag]["VOL"], epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"], kernel=nkk, xdata=PD[eqtag]["PDVOL"], ydata=PD[eqtag]["VOL"], epsilon='None', do_drv=True))
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = 1.0 / gpdy
                    PD[eqtag]["CE_FSVOL"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_FSVOL"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSVOL"] = None
                    PD[eqtag]["CE_FSVOL"] = None

                # Calculating flux surface cross-sectional area coordinate
                if "AREA" in PD[eqtag] and PD[eqtag]["AREA"] is not None:
                    kk = ptools.gp.Sum_Kernel(ptools.gp.RQ_Kernel(1.0e0, 1.0e-1, 1.0e0), ptools.gp.Linear_Kernel(1.0e1), ptools.gp.Noise_Kernel(1.0e-3))
                    (PD[eqtag]["CS_FSAREA"], nkk) = \
                          grabGPR(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"], kernel=kk, xdata=PD[eqtag]["PDAREA"], ydata=PD[eqtag]["AREA"], epsilon=1.0e-2, method='adadelta', spars=[1.0e-2, 0.9]))
                    gpdy = grabfit(CSO._GaussianProcessRegression1D__basic_fit(PD[eqtag]["CS_POLFLUXN"], kernel=nkk, xdata=PD[eqtag]["PDAREA"], ydata=PD[eqtag]["AREA"], epsilon='None', do_drv=True))
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = 1.0 / gpdy
                    PD[eqtag]["CE_FSAREA"] = np.zeros(PD[eqtag]["CS_"+csb].shape)
                else:
                    PD[eqtag]["CS_FSAREA"] = None
                    PD[eqtag]["CJ_"+cjb+"_FSAREA"] = None
                    PD[eqtag]["CE_FSAREA"] = None

    return PD


def calc_geom_with_bspline(procdata, cdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Calculates the relations between the various radial coordinate systems to
    be used for further processing. Interpolates all systems to be on the same
    101 point base uniformly spread in normalized poloidal flux coordinate,
    such that conversion between systems can be done easily with linear
    interpolations hereafter. Additionally computes the necessary Jacobians
    in order to convert derivatives with respect to one coordinate system into
    derivative with respect to another coordinate system.

    This function uses smoothed cubic B-splines to perform the necessary
    interpolations, with special care taken to not introduce discontinuities
    in the second derivative of the individual splines. This ensures
    smoothness in the first derivatives to be used for the coordinate
    Jacobians.

    Normalized poloidal flux was chosen to be the base coordinate in this
    implementation simply because most profile information in the JET PPF
    system was already expressed with respect to this coordinate. This reduces
    the overall conversion errors attached to the data.

    .. note:
        This function is not used in the current JET-specific implementation

    Flux surface averages stored
        INVR       = Inverse major radius
        INVR2      = Inverse major radius squared
        POLFLUXNG  = Gradient of normalised poloidal flux with respect to major radius
        POLFLUXNG2 = Squared gradient of normalised poloidal flux with respect to major radius
        BPOL       = Poloidal magnetic field
        BPOL2      = Polodial magnetic field squared
        F          = Torodial magnetic field times major radius (flux function)
        P          = Total plasma pressure
        JPHIBYR    = Toroidal current divided by major radius (flux function)

    Prefix guide
        CS = Coordinate system
        CJ = Coordinate Jacobian
        CE = Coordinate error

    :arg procdata: dict. Implementation-specific object containing processed radial coordinate data.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :returns: dict. Identical to input object with unified coordinate system data inserted.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    if PD is not None and "CS_POLFLUXN" in PD and PD["CS_POLFLUXN"] is not None and "FS_PFN" in PD and PD["FS_PFN"] is not None:

        # Print coordinate system data from equilibrium, for debugging purposes
        if cdebug:
            debugdir = './coord_debug/'
            if not os.path.isdir(debugdir):
                os.makedirs(debugdir)
            if "FS_PFN" in PD and "FS_BPOL" in PD and PD["FS_BPOL"] is not None and "FS_BPOL2" in PD and PD["FS_BPOL2"] is not None:
                with open(debugdir+'bpol.txt', 'w') as ff1:
                    for ii in range(PD["FS_BPOL"].size):
                        ff1.write("   %15.6e   %15.6e   %15.6e\n" % (PD["FS_PFN"][ii], PD["FS_BPOL"][ii], PD["FS_BPOL2"][ii]))

        if "FS_IR" in PD and PD["FS_IR"] is not None:
            xdata = PD["FS_PFN"]
            ydata = PD["FS_IR"]
            spvals = splrep(xdata, ydata)
            PD["CS_INVR"] = splev(PD["CS_POLFLUXN"], spvals, der=0)
            PD["CE_INVR"] = np.zeros(xdata.shape)
        else:
            PD["CS_INVR"] = None
            PD["CE_INVR"] = None

        if "FS_IR2" in PD and PD["FS_IR2"] is not None:
            xdata = PD["FS_PFN"]
            ydata = PD["FS_IR2"]
            spvals = splrep(xdata, ydata)
            PD["CS_INVR2"] = splev(PD["CS_POLFLUXN"], spvals, der=0)
            PD["CE_INVR2"] = np.zeros(xdata.shape)
        else:
            PD["CS_INVR2"] = None
            PD["CE_INVR2"] = None

        if "FS_RHO" in PD and PD["FS_RHO"] is not None and "FS_GRHO" in PD and PD["FS_GRHO"] is not None:
            xdata = PD["FS_PFN"]
            ydata = PD["FS_RHO"] * PD["FS_GRHO"]
            spvals = splrep(xdata, ydata)
            PD["CS_POLFLUXNG"] = splev(PD["CS_POLFLUXN"], spvals, der=0)
            PD["CE_POLFLUXNG"] = np.zeros(xdata.shape)
        else:
            PD["CS_POLFLUXNG"] = None
            PD["CE_POLFLUXNG"] = None

        if "FS_RHO" in PD and PD["FS_RHO"] is not None and "FS_GRHO2" in PD and PD["FS_GRHO2"] is not None:
            xdata = PD["FS_PFN"]
            ydata = PD["FS_RHO"] * PD["FS_RHO"] * PD["FS_GRHO2"]
            spvals = splrep(xdata, ydata)
            PD["CS_POLFLUXNG2"] = splev(PD["CS_POLFLUXN"], spvals, der=0)
            PD["CE_POLFLUXNG2"] = np.zeros(xdata.shape)
        else:
            PD["CS_POLFLUXNG2"] = None
            PD["CE_POLFLUXNG2"] = None

        if "FS_BPOL" in PD and PD["FS_BPOL"] is not None:
            xdata = PD["FS_PFN"]
            ydata = PD["FS_BPOL"]
            spvals = splrep(xdata, ydata)
            PD["CS_BPOL"] = splev(PD["CS_POLFLUXN"], spvals, der=0)
            PD["CE_BPOL"] = np.zeros(xdata.shape)
        else:
            PD["CS_BPOL"] = None
            PD["CE_BPOL"] = None

        if "FS_BPOL2" in PD and PD["FS_BPOL2"] is not None:
            xdata = PD["FS_PFN"]
            ydata = PD["FS_BPOL2"]
            spvals = splrep(xdata, ydata)
            PD["CS_BPOL2"] = splev(PD["CS_POLFLUXN"], spvals, der=0)
            PD["CE_BPOL2"] = np.zeros(xdata.shape)
        else:
            PD["CS_BPOL2"] = None
            PD["CE_BPOL2"] = None

    return PD


def delete_raw_coord_data(procdata):
    """
    JET-SPECIFIC FUNCTION
    Deletes raw coordinate data from structure to remove unnecessary clutter.

    :arg procdata: dict. Implementation-specific object containing raw coordinate data.

    :returns: dict. Identical to input object with all raw coordinate data removed.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    if PD is not None:

        dlist = ["RMJI", "RMJO", "PFN2RMI", "PFN2RMO", "PFN2RHF", "PFN2RLF", \
                 "PFNI", "PFNO", "RHF2PFN", "RLF2PFN", \
                 "TFX", "PFN2TFX", \
#                 "DVOL", "PDVOL", "VOL", "DAREA", "PDAREA", "AREA", \
                 "RMJIEB", "RMJOEB", "PFNIEB", "PFNOEB"
#                 "TFXEB", "DVOLEB", "VOLEB", "DAREAEB", "AREAEB"
                ]
        PD = ptools.delete_fields_from_dict(PD, fields=dlist)
        if "EQLIST" in PD and PD["EQLIST"] is not None and len(PD["EQLIST"]) > 1:
            for eq in PD["EQLIST"]:
                if eq.upper() in PD:
                    PD[eq.upper()] = ptools.delete_fields_from_dict(PD[eq.upper()], fields=dlist)
        elif "EQLIST" in PD and PD["EQLIST"] is not None and len(PD["EQLIST"]) > 0:
            for eq in PD["EQLIST"]:
                if eq.upper() in PD:
                    del PD[eq.upper()]

    return PD


def calculate_coordinate_systems(procdata, cdebug=False, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Attempts to construct a self-consistent description of the common
    radial coordinates used for plasma transport studies, using the 1D
    descriptions provided from the extracted 2D equilibrium.

    :arg procdata: dict. Implementation-specific object containing raw coordinate data.

    :kwarg cdebug: bool. Flag to enable printing of raw coordinate system data into ASCII files for debugging.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Identical to input object with unified coordinate system data inserted.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    if PD is not None:

        # Perform coordinate system unification and interpolation
        if "GPCOORDFLAG" in PD and PD["GPCOORDFLAG"]:
            try:
                PD = calc_coords_with_gp(PD, cdebug=cdebug)
            except (KeyError, ValueError, AttributeError):
                raise ValueError("Coordinate system interpolation failed.")

        # TODO: Should make a non-GP coordinate unification function or remove choice to not use GP fitting for coordinates
        else:
            PD = calc_coords_with_bspline(PD, cdebug=cdebug)

        # RAWCOORDFLAG option not documented as is unsupported - keeps raw coordinate data in output dict if True
        if not ("RAWCOORDFLAG" in PD and PD["RAWCOORDFLAG"]):
            tempdict = delete_raw_coord_data(PD)

    if fdebug:
        print("process_jet.py: calculate_coordinate_systems() completed.")

    return PD


def apply_coordinate_shift(procdata, edge_shift=None, allow_inward_shift=False):
    """
    JET-SPECIFIC FUNCTION
    Applies a coordinate shift to the primary kinetic profiles (electron
    density, electron and ion temperatures, and angular frequency) according
    to the input edge_shift argument. This is meant to approximate a lateral
    shift of the equilibrium such that the separatrix on the low-field-side
    is consistent with SOL model predictions. This is a first order
    correction to known magnetic equilibrium issues in the ILW-campaigns,
    and the applicability of procedure to the high-field-side is not tested
    and most probably also not correct.

    Note that this shift does NOT adjust the other magnetic equilibrium data
    to obtain a self-consistent description of the equilibrium.

    :arg procdata: dict. Implementation-specific object containing processed radial coordinate data.

    :kwarg edge_shift: float. Amount by which to shift the various diagnostic radial coordinates, in major radius.

    :kwarg allow_inward_shift: bool. Toggles application of inward shifts if requested shift is negative, default is ignored.

    :returns: dict. Identical to input object with radial coordinate shift applied to the diagnostic data.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata
    shift = float(edge_shift) if isinstance(edge_shift, (int, float)) else None

    if PD is not None and shift is not None:

        PD["RMAJSHIFT"] = np.array([shift]) if shift > 0.0 or allow_inward_shift else np.array([0.0])
        PD["RMAJSHIFTEB"] = np.array([0.0])

        diag = "HRTS"
        quantities = ["NE", "TE"]
        if diag in PD and PD[diag] is not None:
            for dtag in quantities:
                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

        diag = "LIDR"
        quantities = ["NE", "TE"]
        if diag in PD and PD[diag] is not None:
            for dtag in quantities:
                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

        # Unsure how to handle this for ECE diagnostics, do nothing currently
#        diag = "ECR"
#        quantities = ["TE"]
#        if diag in PD and PD[diag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
#                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

#        diag = "ECM"
#        quantities = ["TE"]
#        if diag in PD and PD[diag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
#                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]
#                elif "PFN"+dtag in PD[diag] and PD[diag]["PFN"+dtag] is not None and "CS_POLFLUXN" in PD and PD["CS_POLFLUXN"] is not None:
#                    rmajvec = np.full(PD[diag]["PFN"+dtag].shape, np.NaN)
#                    fhfs = (PD[diag]["PFN"+dtag] < 0.0)
#                    if np.any(fhfs) and "CS_RMAJORI" in PD and PD["CS_RMAJORI"] is not None:
#                        ifunc = interp1d(PD["CS_POLFLUXN"], PD["CS_RMAJORI"], kind='linear', bounds_error=False, fill_value='extrapolate')
#                        rmajvec[fhfs] = ifunc(np.abs(PD[diag]["PFN"+dtag][fhfs]))
#                    if not np.all(fhfs) and "CS_RMAJORO" in PD and PD["CS_RMAJORO"] is not None:
#                        ofunc = interp1d(PD["CS_POLFLUXN"], PD["CS_RMAJORO"], kind='linear', bounds_error=False, fill_value='extrapolate')
#                        rmajvec[np.invert(fhfs)] = ofunc(np.abs(PD[diag]["PFN"+dtag][np.invert(fhfs)]))
#                    PD[diag]["RM"+dtag] = rmajvec + PD["RMAJSHIFT"]
#                    del PD[diag]["PFN"+dtag]

        diag = "CX"
        quantities = ["TI", "AF", "NIMP"]
        if diag in PD and PD[diag] is not None:
            for key in PD[diag]:
                for dtag in quantities:
                    if "RM"+dtag in PD[diag][key] and PD[diag][key]["RM"+dtag] is not None:
                        PD[diag][key]["RM"+dtag] = PD[diag][key]["RM"+dtag] + PD["RMAJSHIFT"]

        # Undecided whether to apply coordinate shift to the following profiles, code provided here in case
#        diag = "MAGN"
#        quantities = ["Q"]
#        if diag in PD and PD[diag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[diag] and PD[diag]["RM"+dtag] is not None:
#                    PD[diag]["RM"+dtag] = PD[diag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "NBP2"
#        quantities = ["QE", "QI", "S", "J", "TAU", "NFIA", "WFIA"]
#        if qtag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "PION"
#        quantities = ["QE", "QI", "NFIA", "WFIA"]
#        if ctag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "OHM"
#        quantities = ["QE", "QI", "J"]
#        if ctag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

#        ctag = "RAD"
#        quantities = ["QRAD"]
#        if ctag in PD and PD[ctag] is not None:
#            for dtag in quantities:
#                if "RM"+dtag in PD[ctag] and PD[ctag]["RM"+dtag] is not None:
#                    PD[ctag]["RM"+dtag] = PD[ctag]["RM"+dtag] + PD["RMAJSHIFT"]

    return PD


def compute_q_edge_correction(procdata, tag=""):
    """
    JET-SPECIFIC FUNCTION
    This function computes the safety factor value at the separatrix
    which is consistent with the total plasma current, according to
    the JETTO manual [G. Cenacchi, 1988].

    :arg procdata: dict. Implementation-specific object containing unified coordinates and diagnostic data.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Identical to input object with user-defined corrections applied to data.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    if PD is not None and isinstance(tag, str):

        # Compute q value at separatrix which is consistent with total plasma current
        if "IP" in PD and PD["IP"] is not None and PD["PFXMAP"] is not None and PD["RBND"] is not None and PD["ZBND"] is not None and PD["FPSI"] is not None:
            rbnd = PD["RBND"].copy()
            zbnd = PD["ZBND"].copy()
            # Reduce boundary resolution down to avoid noise in derivative calculation around X-point
            if len(rbnd) >= 500:
                rbnd = rbnd[::5]
                zbnd = zbnd[::5]
            if len(rbnd) >= 400:
                rbnd = rbnd[::4]
                zbnd = zbnd[::4]
            if len(rbnd) >= 300:
                rbnd = rbnd[::3]
                zbnd = zbnd[::3]
            if len(rbnd) >= 200:
                rbnd = rbnd[::2]
                zbnd = zbnd[::2]
            # Ensure complete loop by appending starting point onto contour array
            rbnd = np.hstack((rbnd, rbnd[0]))
            zbnd = np.hstack((zbnd, zbnd[0]))
            pfxf = PD["FPSIPFN"] * (PD["FBND"] - PD["FAXS"]) + PD["FAXS"]
            order = 3
            dr = 0.01
            dz = 0.01
            btotbnd, bpolbnd, btorbnd = ftools.rz2b(PD["PFXMAP"], PD["RMAP"], PD["ZMAP"], rbnd, zbnd, PD["FPSI"], pfxf, deltar=dr, deltaz=dz, axsval=float(PD["FAXS"]), bndval=float(PD["FBND"]), order=order, prefilter=False)
            bpol = 0.5 * (bpolbnd[1:] + bpolbnd[:-1])
            dl = np.sqrt(np.power(np.diff(rbnd), 2.0) + np.power(np.diff(zbnd), 2.0))
            r2 = np.power(0.5 * (rbnd[1:] + rbnd[:-1]), 2.0)
            a_bnd = float(np.sum(dl / (r2 * bpol)) / np.sum(dl / bpol))
            k_bnd = float(np.sum(dl * bpol) * np.sum(dl / bpol))
            f_bnd = float(PD["FPSI"][-1])
            mu0 = 1.25663706e-6
            ip = float(PD["IP"])
            # Sign adherence is guaranteed by enforcing COCOS=1 in unpacking functions
            PD["QEDGE"+tag] = f_bnd * a_bnd * k_bnd / (2.0 * np.pi * mu0 * ip)

    return PD

def apply_data_corrections(procdata, fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    This function is also responsible for computing and applying any
    heuristic changes to the magnetic geometry or individual coordinate
    systems. Ideally, this is also done self-consistently but there is
    no guarantee.

    :arg procdata: dict. Implementation-specific object containing unified coordinates and diagnostic data.

    :kwarg fdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: dict. Identical to input object with user-defined corrections applied to data.
    """
    PD = None
    if isinstance(procdata, dict):
        PD = procdata

    if PD is not None:

        # Perform coordinate shift according to tanh fit, setting new separatrix location at Te = 100 eV
        if "RMAJEDGE" in PD and PD["RMAJEDGE"] is not None and "RSHIFTFLAG" in PD and PD["RSHIFTFLAG"]:
            rmajedge_old = float(PD["ORMAJEDGE"][0]) if "ORMAJEDGE" in PD and PD["ORMAJEDGE"] is not None else None
            if rmajedge_old is None and "CS_RMAJORO" in PD and PD["CS_RMAJORO"] is not None:
                rmajedge_old = float(PD["CS_RMAJORO"][-1])
            shift = rmajedge_old - float(PD["RMAJEDGE"][0])
            PD = apply_coordinate_shift(PD, edge_shift=shift, allow_inward_shift=False)

        if fdebug:
            if "RMAJSHIFT" in PD and PD["RMAJSHIFT"] is not None:
                print("process_jet.py: apply_data_corrections(): Radial shift of %.3f m applied." % (float(PD["RMAJSHIFT"][0])))
            else:
                print("process_jet.py: apply_data_corrections(): No radial shift was applied.")

        if "USEQEDGEFLAG" in PD and PD["USEQEDGEFLAG"] and "QDDA" in PD and PD["QDDA"] is not None:
            PD = compute_q_edge_correction(PD, tag=PD["QDDA"])

            if fdebug and "QEDGE"+PD["QDDA"] in PD and PD["QEDGE"+PD["QDDA"]] is not None:
                print("process_jet.py: apply_data_corrections(): Computed current consistent edge q as %.3f." % (float(PD["QEDGE"+PD["QDDA"]])))

    if fdebug:
        print("process_jet.py: apply_data_corrections() completed.")

    return PD
