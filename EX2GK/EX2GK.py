# Script with functions to perform profile fitting on standardized extracted data
# Developer: Aaron Ho - 14/02/2017

# Required imports
import os
import sys
import copy
import warnings
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
import importlib
import importlib.util
import collections
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools, phystools as qtools

np_types = (np.int8,np.int16,np.int32,np.int64,       # Signed integer types
            np.uint8,np.uint16,np.uint32,np.uint64,   # Unsigned integer types
            np.float16,np.float32,np.float64)         # Floating point decimal types


def extract_time_traces(machine,namelist=None):
    """
    Extracts experimental data from various machine databases,
    simultaneously converting it into a standardized format for
    use in automated time window selection module.

    Note that this extraction and standardization require
    machine-specific adapters to be developed!!!

    :arg machine: str. Defines the machine database from which data will be extracted.

    :kwarg namelist: dict. Namelist including implementation-specific fields, specifying extra options and settings to be used by the data extraction and processing scripts.

    :returns: dict. Python dictionary object with standardized time trace data.
    """
    argdict = dict()
    if not isinstance(machine,str):
        raise TypeError("Machine specification must be a string. Extraction aborted.")
        sys.exit(51)
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)

    # Containers for output data
    outdata  = None

    # Designed to fail and throw ImportError if the appropriate machine adapter is not found.
    adapter = importlib.import_module('.machine_adapters.'+machine.lower()+'.adapter',package='EX2GK')

    if adapter is not None:
        outdata = adapter.get_time_traces(namelist=argdict)

    return outdata


def extract_and_standardize_data(machine,namelist=None):
    """
    Extracts experimental data from various machine databases,
    simultaneously converting it into a standardized format for
    use in the GPR1D fitting module.

    Note that this extraction and standardization require
    machine-specific adapters to be developed!!!

    :arg machine: str. Defines the machine database from which data will be extracted.

    :kwarg namelist: dict. Namelist including implementation-specific fields, specifying extra options and settings to be used by the data extraction and processing scripts.

    :returns: dict. Python dictionary object with data corresponding to the level of processing specified with outlevel argument.
    """
    outlvl = 0
    argdict = dict()
    if not isinstance(machine,str):
        raise TypeError("Machine specification must be a string. Extraction aborted.")
        sys.exit(51)
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)

    # Containers for output data
    outlist  = None

    # Designed to fail and throw ImportError if the appropriate machine adapter is not found.
    adapter = importlib.import_module('.machine_adapters.'+machine.lower()+'.adapter',package='EX2GK')

    if adapter is not None:
        outlist = adapter.get_data(namelist=argdict)

    custompath = argdict["CUSTOMPATH"] if "CUSTOMPATH" in argdict and isinstance(argdict["CUSTOMPATH"],str) else None 
    if custompath is not None and not custompath.endswith('/'):
        custompath = custompath + '/'
    if isinstance(outlist,list):
        for jj in np.arange(0,len(outlist)):
            outdata = outlist[jj]
            custom = None
            if custompath is not None and os.path.exists(custompath) and os.path.isfile(custompath+'EX2GK_custom_filters.py'):
                spec = importlib.util.spec_from_file_location('EX2GK_custom_filters',custompath+'EX2GK_custom_filters.py')
                custom = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(custom)
            if custom is None:
                try:
                    custom = importlib.import_module('EX2GK_custom_filters')
                except (ImportError,AttributeError):
                    custom = None
            if custom is None:
                custom = importlib.import_module('.EX2GK_custom_filters',package='EX2GK')
            if custom is not None:
                outdata = custom.apply_custom_filters(outdata)

            outlist[jj] = copy.deepcopy(outdata)

    return outlist


def reset_gp_fit_settings(inputdata,machine,namelist=None):
    """
    Resets the GP fit settings for given standardized
    experimental data using defaults from various machine
    adapters.

    Note that this extraction and standardization require
    machine-specific adapters to be developed!!!

    :arg inputdata: dict. Python dictionary object with fully processed data according to the standardized adapter format.

    :arg machine: str. Defines the machine-specific adapter from which settings will be defined.

    :kwarg namelist: dict. Namelist including implementation-specific fields, specifying extra options and settings to be used by the data extraction and processing scripts.

    :returns: dict. Python dictionary object with data corresponding to the level of processing specified with outlevel argument.
    """
    outdata = None
    argdict = dict()
    if isinstance(inputdata,dict):
        outdata = copy.deepcopy(inputdata)
    if not isinstance(machine,str):
        raise TypeError("Machine specification must be a string. Extraction aborted.")
        sys.exit(51)
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)

    # Designed to fail and throw ImportError if the appropriate machine adapter is not found.
    adapter = importlib.import_module('.machine_adapters.'+machine.lower()+'.adapter',package='EX2GK')

    if outdata is not None and adapter is not None:
        outdata = adapter.reset_fit_settings(outdata,argdict)

    return outdata


def fit_standardized_data(inputdata,namelist=None):
    """
    Fits processed data and stores data in a standardized format for
    expressing the profiles and associated dimensionless gyrokinetic
    parameters. Note that the input data must also be standardized
    accordingly or else this program will fail.

    The additions made by this function to the output container should be
    completely standardized and usable in all further processing scripts
    provided in this package.

    :arg inputdata: dict. Standardized object containing processed and filtered profile data and associated GPR fit settings.

    :kwarg namelist: dict. Namelist including standardized fields defining global options and settings to be used by a given execution.

    :returns: dict. Object identical in structure to input object, except containing standardized fitted profiles and dimensionless gyrokinetic parameters.
    """
    twobj = None
    argdict = dict()
    if isinstance(inputdata,classes.EX2GKTimeWindow):
        twobj = inputdata
    elif isinstance(inputdata,dict):
        twobj = classes.EX2GKTimeWindow()
        twobj.importFromDict(copy.deepcopy(inputdata))
    elif isinstance(inputdata,str) and os.path.isfile(inputdata): # Should remove pickle
        temp = ptools.unpickle_this(inputdata)
        if not isinstance(temp,classes.EX2GKTimeWindow):
            raise TypeError('Invalid input to EX2GK fitting routine.')
    else:
        raise TypeError('Invalid input to EX2GK fitting routine.')
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)

    if isinstance(twobj,classes.EX2GKTimeWindow) and twobj.isReady():

        # There are retained for legacy - accepted with depreciation warning
        if "FITCOORDS" in argdict:
            coords = argdict["FITCOORDS"]
            warnings.warn("Namelist argument %s will be deprecated, please use %s instead." % ("FITCOORDS","CSO"),DeprecationWarning)
        if "FITMIN" in argdict:
            xmin = float(argdict["FITMIN"]) if isinstance(argdict["FITMIN"],(float,int)) else 0.0
            warnings.warn("Namelist argument %s will be deprecated, please use %s instead." % ("FITMIN","CSOMIN"),DeprecationWarning)
        if "FITMAX" in argdict:
            xmax = float(argdict["FITMAX"]) if isinstance(argdict["FITMAX"],(float,int)) else 1.0
            warnings.warn("Namelist argument %s will be deprecated, please use %s instead." % ("FITMAX","CSOMAX"),DeprecationWarning)
        if "FITPOINTS" in argdict:
            xnum = int(argdict["FITPOINTS"]) if isinstance(argdict["FITPOINTS"],(float,int)) else 101
            warnings.warn("Namelist argument %s will be deprecated, please use %s instead." % ("FITPOINTS","CSON"),DeprecationWarning)
        fdebug = "DEBUGFLAG" in argdict and argdict["DEBUGFLAG"]

        coords = argdict["CSO"] if "CSO" in argdict else 'rhotor'
        prefix = argdict["CSOP"].upper() if "CSOP" in argdict else ''
        xmin = float(argdict["CSOMIN"]) if "CSOMIN" in argdict and isinstance(argdict["CSOMIN"],(float,int)) else 0.0
        xmax = float(argdict["CSOMAX"]) if "CSOMAX" in argdict and isinstance(argdict["CSOMAX"],(float,int)) else 1.0
        xnum = int(argdict["CSON"]) if "CSON" in argdict and isinstance(argdict["CSON"],(float,int)) else 101
        newrho = np.linspace(xmin,xmax,xnum)

#        cslist = ['rhotor','rhotorn','rhopol','rhopoln','polflux','polfluxn','torflux','torfluxn', \
#                  'rmajoro','rmajori','rmidavg','rminoro','rminori','rminorn','fsvol','fsarea', \
#                  'psitor','psitorn','psipol','psipoln','volume','area']
        cs = itemgetter(0)(ptools.define_coordinate_system(coords))

        if "X" in twobj["RD"]:
            newrho = copy.deepcopy(twobj["RD"]["X"][""])
            if cs is not None:
                (newrho,dummyj,dummye) = twobj["CD"].convert(twobj["RD"]["X"]["X"],twobj["RD"]["X"].coordinate,cs,twobj["RD"]["X"].coord_prefix,prefix)
            if "RMAG" in twobj["ZD"] and twobj["ZD"]["RMAG"][""] is not None:
                twobj["CD"].computeMidplaneAveragedRadiusCoordinates(major_radius_magnetic_axis=twobj["ZD"]["RMAG"][""], overwrite=False)

        if cs is not None and newrho.size > 0:
            if "NIGPFLAG" in argdict:
                twobj["FLAG"].setFlag("NIGP",argdict["NIGPFLAG"])
            if "GPSETFLAG" in argdict:
                twobj["FLAG"].setFlag("GPSET",argdict["GPSETFLAG"])
            if "TISCALEFLAG" in argdict:
                twobj["FLAG"].setFlag("TISCALE",argdict["TISCALEFLAG"])
            if "LINFIFLAG" in argdict:
                twobj["FLAG"].setFlag("LINFI",argdict["LINFIFLAG"])
            if "LINSRCFLAG" in argdict:
                twobj["FLAG"].setFlag("LINSRC",argdict["LINSRCFLAG"])

            passcrit = None
            if "QCPASS" in argdict and isinstance(argdict["QCPASS"],(float,int,np_types)) and float(argdict["QCPASS"]) > 0.0:
                passcrit = float(argdict["QCPASS"])

            twobj.setDebugMode(fdebug)
            twobj.fitPrimaryProfiles(newrho,cs,prefix)
            twobj.fitSecondaryProfiles()
            twobj.fitTertiaryProfiles()
            twobj.fitSourceProfiles()
            twobj.postProcess(quality_check_sigma=passcrit)
            twobj.updateTimestamp()

    return twobj


def compute_code_specific_data(inputdata,namelist=None):
    """
    Processes fitted data into specific gyrokinetic quantities
    required as inputs for various codes. Requires a code-specific
    adapter to be implemented into the tool!

    A general adapter is provided for convenience, which calculates
    useful parameters using common formulae. NOT guaranteed to be
    compatible with any specific gyrokinetic code!!!

    :arg inputdata: dict. Standardized object containing processed and fitted profile data.

    :kwarg namelist: dict. Namelist including standardized fields defining global options and settings to be used by a given execution.

    :returns: dict. Object indentical in structure to input object, except containing post-processed quantities determined by selected adapter.
    """
    ED = None
    argdict = dict()
    if isinstance(inputdata,classes.EX2GKTimeWindow):
        ED = copy.deepcopy(inputdata)
    elif isinstance(inputdata,dict):
        ED = classes.EX2GKTimeWindow.importFromDict(copy.deepcopy(inputdata))
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    fdebug = "DEBUGFLAG" in argdict and argdict["DEBUGFLAG"]

    if "FINALCODE" in argdict and isinstance(argdict["FINALCODE"],str):
        adapter = None
        try:
            adapter = importlib.import_module('.code_adapters.'+argdict["FINALCODE"].lower()+'.adapter',package='EX2GK')
            if adapter is not None:
                fflag = True if "FORCEPOST" in argdict and argdict["FORCEPOST"] else False
                csproc = argdict["CSPROC"] if argdict and "CSPROC" in argdict else None
                ED = adapter.calc_code_inputs(ED,sysopt=csproc,forceflag=fflag,fdebug=fdebug)
        except ImportError:
            print("Adapter for %s not found. Skipping post-processing request..." % (argdict["FINALCODE"].lower()))
        except Exception as e:
            print("Processing error in adapter for %s. Skipping post-processing request..." % (argdict["FINALCODE"].lower()))
            print("  ",repr(e))

    return ED


# MAIN CALL FUNCTION to process the data in passed shot data structure or passed intermediate pickle file
def grab_and_fit(namelist):
    """
    Main call function of the EX2GK program, extracts experimental data,
    performs pre-processing and filtering, fits using GPR1D submodule,
    and returns the final data within a Python dictionary object. The
    outputs were designed to be used as inputs for gyrokinetic (GK)
    simulations, though they are not limited to that explicit purpose.

    Note that this extraction and standardization require machine-
    specific adapters to be developed!!!

    The namelist provided MUST be a Python dictionary object, otherwise
    the program will not operate. Only the MACHINE field is mandatory
    and any items not on the following list will be ignored, unless they
    have a designated meaning within the machine-specific adapter script
    called by this program.
        MACHINE       =  str. Name of the machine from which data will be extracted.
        OUTPUTLEVEL   =  int. 0 = complete run, 1 = raw data only, others are implementation-specific. Default 0
        CSO           =  str. Name of the coordinate system in which the fits will be saved, including Jacobians. Default rhotor
        CSOMIN        =  float. Minimum value in coordinate system for the fit prediction output vector. Default 0.0
        CSOMAX        =  float. Maximum value in coordinate system for the fit prediction output vector. Default 1.0
        CSON          =  int. Number of points to be used in the fit prediction output vector. Default 101
        FINALCODE     =  str. Optional name of the gyrokinetic code for which the values are to be used
        NIGPFLAG      =  bool. Toggles use of x-errors inside GPR fit routine (not recommended). Default False
        GPSETFLAG     =  bool. Keeps original GPR1D settings lists if True. Default False
        LINFIFLAG     =  bool. Toggles use of linear interpolation for fast ion density and energy density. Default False
        LINSRCFLAG    =  bool. Toggles use of linear interpolation for heat, particle, momentum, current sources. Default False
        TISCALEFLAG   =  bool. Toggles scaling of T_imp profile to create T_i profile if only sparse data is available. Default True
        QCPASS        =  float. Pass criterion for basic data quality checks, toggles calculation if specified

    :arg namelist: dict. Implementation-specific namelist of required fields, options and settings to be used by a given execution.

    :returns: dict. Python dictionary object with data corresponding to the level of processing specified with namelist option.
    """
    argdict = None
    if isinstance(namelist,dict):
        argdict = copy.deepcopy(namelist)
    else:
        raise TypeError("Namelist argument must be a dictionary. Program aborted.")
        sys.exit(1)

    outlist = None
    if isinstance(argdict,dict) and "MACHINE" in argdict:
        outlist = extract_and_standardize_data(argdict["MACHINE"],namelist=argdict)
        for jj in np.arange(0,len(outlist)):
            outdict = outlist[jj]
            if "OUTPUTLEVEL" in argdict and argdict["OUTPUTLEVEL"] == 0:
                outdict = fit_standardized_data(outdict,namelist=argdict)
                if "FINALCODE" in argdict and argdict["FINALCODE"]:
                    outdict = compute_code_specific_data(outdict,namelist=argdict)
            outlist[jj] = copy.deepcopy(outdict)

    return outlist
