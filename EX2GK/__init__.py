# Required file to use as Python package

from pkg_resources import get_distribution

__version__ = get_distribution("EX2GK").version
__author__ = "Aaron Ho"

import os
import sys
import importlib

__location__ = os.path.dirname(os.path.abspath(__file__))
#sys.path.append(os.path.join(parentDir,'GPR1D'))

#import GPR1D
#GPR1D = importlib.import_module('.GPR1D.GPR1D',package='EX2GK')
