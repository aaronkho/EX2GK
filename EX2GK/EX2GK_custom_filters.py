# Script with functions to perform apply custom filters to standardized data before fitting
# Developer: Aaron Ho - 13/02/2018   (updated: 05/06/2018)

# Default imports
import os

#### Place required imports below ####

import re
import numpy as np

#### Place required imports above ####

# Internal package imports - contains many useful functions
from EX2GK.tools.general import classes, proctools as ptools

def apply_custom_filters(time_window,fdebug=False):
    """
    USER-DEFINED FUNCTION (STANDARDIZED NAME)
    This function gives the user direct access to the raw data,
    including the standard GPR fit settings, before the GPR fit
    procedure is applied to the data. It is recommended that
    the user place any customized data manipulations here when
    running the tool, and that the naming / format conventions
    in the data dictionary be preserved as the fit routine does
    not understand any other syntax. A wrapper class to ensure
    this is foreseen in future development.

    Datatypes = NE, TE, TI, AF, NIMP[1-9], TIMP, Q, NFI, WFI

    RD Fields -  "CSX"       =  x-coordinate of indicated profile
                 "CSP"       =  x-coordinate prefix of indicated profile (indicates corrected coordinates)
                 ""          =  y-values of indicated profile
                 "X"         =  x-values of indicated profile
                 "EB"        =  y-errors of indicated profile
                 "XEB"       =  x-errors of indicated profile
                 "SRC"       =  list of diagnostics providing raw data
                 "MAP"       =  index vector linking data points to diagnostic
    "FSI"                    =  default GPR settings for indicated profile

    :arg datadict: dict. Python dictionary object with standardized data format from EX2GK.

    :returns: dict. Python dictionary object with customized filters applied to standardized data format from EX2GK.
    """
    SD = None
    if isinstance(time_window,classes.EX2GKTimeWindow):
        SD = time_window

    if SD is not None:

        # Python lists for easy data searching
        cslist = SD["CD"].keys()                     # Coordinate systems available in object
        rdlist = SD["RD"].getPresentFields('list')   # Raw profile data available in object

        #### This section is for known and accepted data faults - DO NOT MODIFY!!! ####

        if SD["META"]["DEVICE"] is not None and re.match(r'^JET$',SD["META"]["DEVICE"],flags=re.IGNORECASE):
            if SD["META"]["SHOT"] is not None and SD["META"]["SHOT"] == 92427:
                if "TIMP" in rdlist:
                    RD = SD["RD"]["TIMP"].exportToDict()
                    for ii in np.arange(0,len(RD["SRC"])):
                        if re.match(r'^CXGM$',RD["SRC"][ii],flags=re.IGNORECASE) and re.match(r'^RHO((POL)|(TOR))N?$',RD["XCS"],flags=re.IGNORECASE):
                            dfilt = np.invert(np.all([RD["MAP"] == ii,RD["X"] < 0.5],axis=0))
                            RD["X"] = RD["X"][dfilt]
                            RD["XEB"] = RD["XEB"][dfilt]
                            RD[""] = RD[""][dfilt]
                            RD["EB"] = RD["EB"][dfilt]
                            RD["MAP"] = RD["MAP"][dfilt]
                    robj = classes.EX2GKOneDimensionalData.importFromDict(RD)
                    robj.name = "TIMP"
                    SD["RD"].addRawDataObject(robj)

        #### Place custom filters below - do not modify above! ####




        #### Place custom filters above - do not modify below! ####

        # Here to ensure that this is never an empty if statement
        if fdebug:
            print("Applying custom filters from %s completed." % (os.path.abspath(__file__)))

    return SD
