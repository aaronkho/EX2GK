{"DESCRIPTION": 'EX2GK data representation guide - flat dictionary',

 "DETAILS": 'For more information, search for \"Class variables\" inside tools/general/classes.py',

 "META":  { "DESCRIPTION": 'Meta data',

    "DEVICE": 'Experimental device identifier',
    "INTERFACE": 'Data extraction interface identifier',
    "MATWALL": 'Main element of first wall or plasma facing components',
    "MATSTRIKE": 'Main element of plasma strike surfaces, limiter or divertor',
    "CONFIG": 'Plasma configuration / heat exhaust scheme',
    "SHOT": 'Discharge identification number',
    "T1": 'Time window start, as referenced in experimental data repository',
    "T2": 'Time window end, as referenced in experimental data repository',
    "WINTYPE": 'Type identification index for averaging window',
    "WINPHASE": 'Phase identification index of time window',
    "EQSRC": 'Equilibrium source identifier',
    "NUMI": 'Number of main ion species',
    "NUMIMP": 'Number of impurity ion species which have data in time window',
    "NUMZ": 'Number of filler impurity ion species, used to enforce quasineutrailty',
    "CSO": 'Name of the requested coordinate system on which output is expressed',
    "CSOP": 'Prefix of the requested coordinate system on which output is expressed',
    "CSOMIN": 'Minimum coordinate value for the fit prediction output vector',
    "CSOMAX": 'Maximum coordinate value for the fit prediction output vector',
    "QCPASS": 'Pass criteria of data quality checks, expressed in sigma',
    "CODE": 'Target code for post-processing, if performed',
    "CODETAG": 'Field prefix for post-processed quantities, if performed',
    "CODEBASE": 'Reference coordinate for post-processed quantities, if performed',

 },

 "ED":   { "DESCRIPTION": 'Equilibrium data',

    "PSI": 'Poloidal flux map',
    "F": 'Toroidal flux profile',
    "P": 'Pressure profile',
    "FFP": 'Toroidal flux derivative profile',
    "PP": 'Pressure derivative profile',
    "Q": 'Safety factor profile',
    "BND": 'Plasma boundary location vector, if present',
    "LIM": 'Plasma limiter location vector, if present',

 },

 "ZD":   { "DESCRIPTION": 'Zero-dimensional data',

    "<vtag>": 'Value of one-dimensional quantity',
    "<vtag>EB": '1 sigma error of value of one-dimensional quantity',

 },

 "RD":    { "DESCRIPTION": 'One-dimensional raw profile data',

    "<qtag>": 'Profile quantity <qtag> as a function of coordinate system',
    "<qtag>X": 'Corresponding coordinate system values for raw profile data',
    "<qtag>XCS": 'Label of coordinate system in which raw profile data are expressed',
    "<qtag>XCP": 'Label prefix of coordinate system in which raw profile data are expressed, for corrected systems if any',
    "<qtag>EB": '1 sigma error of profile quantity as a function of coordinate system',
    "<qtag>XEB": '1 sigma error on coordinate values for raw profile data',
    "<qtag>GRAD": 'Derivative of profile quantity as a function of coordinate system',
    "<qtag>XGRAD": 'Corresponding coordinate system values for raw profile derivative data',
    "<qtag>XEBGRAD": '1 sigma error on coordinate values for raw profile derivative data',
    "<qtag>GRADEB": '1 sigma error of derivative of profile quantity as a function of coordinate system',
    "<qtag>DMAP": 'Diagnostic list index for corresponding profile quantity raw data',
    "<qtag>DIAG": 'Diagnostic list for profile quantity'

 },

 "PD":    { "DESCRIPTION": 'One-dimensional fitted profile data on unified coordinate grid',

    "X": 'Requested fit coordinate system values for fitted profile data',
    "XEB": '1 sigma error of requested fit coordinate system for fitted profile data',
    "<qtag>": 'Profile quantity as a function of requested fit coordinate system',
    "<qtag>EB": '1 sigma error of profile quantity as a function of requested fit coordinate system',
    "<qtag>GRAD": 'Derivative of profile quantity as a function of requested fit coordinate system',
    "<qtag>GRADEB": '1 sigma error of derivative of profile quantity as a function of requested fit coordinate system',

 },

 "CD":    { "DESCRIPTION": 'Coordinate systems',

    "BASE": 'Label of unified coordinate system base',
    "CONVERT": 'Conversion vector into another coordinate system base, if present',
    "<ctag>": { "DESCRIPTION": 'Coordinate system on unified coordinate system base',
                "V": 'Vector of coordinate system evaluated on unified coordinate system base',
                "J": 'Jacobian of coordinate system with respect to the unified coordinate system base',
                "E": '1 sigma error of coordinate system evaluated on unified coordinate system base',
    },

    # Labels for <ctag> include:
    #    "PSIPOLN": 'Normalized poloidal flux : PSIPOL / PSIPOL[-1]',
    #    "PSIPOL": 'Absolute poloidal flux',
    #    "RHOPOLN": 'Normalized poloidal rho : sqrt(PSIPOLN)',
    #    "PSITORN": 'Normalized toroidal flux : PSITOR / PSITOR[-1]',
    #    "PSITOR": 'Absolute toroidal flux',
    #    "RHOTORN": 'Normalized toroidal rho : sqrt(PSITORN)',
    #    "RMAJORO": 'Absolute outer major radius at height of magnetic axis',
    #    "RMAJORI": 'Absolute inner major radius at height of magnetic axis',
    #    "RMINORO": 'Absolute outer minor radius at height of magnetic axis',
    #    "RMINORI": 'Absolute inner minor radius at height of magnetic axis',
    #    "RMAJORA": 'Averaged major radius at height of magnetic axis',
    #    "RMINORA": 'Averaged minor radius at height of magnetic axis',
    #    "FSVOL": 'Flux surface volume',
    #    "FSAREA": 'Flux surface cross-sectional area',

 },


 "FSI":   { "DESCRIPTION": 'Generalized fitting routine selection and associated settings for profile fitting routines',

    "<qtag>": #'Generalized fitting routine selection and associated settings for profile quantity <qtag>',
              [ 'List of fitting routine selections, in order from highest to lowest preference',

        {
            "FTYPE" : 'Identifier for fitting routine to be used',
        },

    ],

 },

 "FSO":   { "DESCRIPTION": 'Generalized optimized fit parameters from profile fitting routines',

    "<qtag>": #'Generalized fitting routine selection and associated settings for profile quantity <qtag>'
              [ 'Selected fitting routine and result of optimization',

        {
            "FTYPE" : 'Identifier for fitting routine to be used',
        },

    ],

 },

 "FLAG":  { "DESCRIPTION": 'Implementation-dependent option flags for data processing'
 },

}
