#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

# Required imports
import os
import sys
import datetime
import re
import pwd
import time
import copy
import pickle
import numpy as np

import matplotlib
matplotlib.use("Qt4Agg")

QtCore = None
QtGui = None
QtWidgets = None
try:
    from PyQt4 import QtCore, QtGui
    QtWidgets = QtGui
except ImportError:
    from PyQt5 import QtCore, QtGui, QtWidgets
pyqtversion = QtCore.PYQT_VERSION_STR if QtCore is not None else "0"

#import pyqtgraph as pg
#from pyqtgraph import setConfigOption

import matplotlib
old_mpl = pversion.parse(matplotlib.__version__) <= pversion.parse("2.0.0")
fqt5 = pversion.parse(pyqtversion) >= pversion.parse("5.0.0")
mplqt = None
if fqt5:
    matplotlib.use("Qt5Agg")
    from matplotlib.backends import backend_qt5agg as mplqt
else:
    try:
        matplotlib.use("Qt4Agg")
        from matplotlib.backends import backend_qt4agg as mplqt
    except ValueError:
        matplotlib.use("QtAgg")
        from matplotlib.backends import backend_qtagg as mplqt
from matplotlib import figure as mplfig

from EX2GK import EX2GK, __version__


class extractor_EX2GK(QtGui.QWidget):

    def __init__(self):
        super(extractor_EX2GK, self).__init__()
        self.initUI()
        self.device = None
        self.csidx = 0
        self.sigma = 1.0
        self.db = dict()

    def initUI(self):

#        pg.setConfigOption('background', 'w')
#        pg.setConfigOption('foreground', 'k')

        self.LoadDatabaseButton = QtGui.QPushButton("Load Pickle File")
        self.LoadDatabaseButton.clicked.connect(self.load_database)
#        self.OverwriteBox = QtGui.QCheckBox("Overwrite existing entries")
        self.ClearDataButton = QtGui.QPushButton("Clear Data")
        self.ClearDataButton.clicked.connect(self.clear_data)

        lbox = QtGui.QHBoxLayout()
        lbox.addWidget(self.LoadDatabaseButton)
#        lbox.addWidget(self.OverwriteBox)
        lbox.addWidget(self.ClearDataButton)

        self.ShotNumberList = QtGui.QComboBox()
        self.ShotNumberList.setEnabled(False)
        self.ShotNumberList.currentIndexChanged.connect(self.populate_time_slices)
        self.TimeSliceList = QtGui.QComboBox()
        self.TimeSliceList.setEnabled(False)
        self.TimeSliceList.currentIndexChanged.connect(self.populate_variables)
        self.AllTimesBox = QtGui.QCheckBox("Use all times slices")
        self.AllTimesBox.setEnabled(False)
        self.AllTimesBox.toggled.connect(self.toggle_all_times)

        sbox = QtGui.QVBoxLayout()
        sbox.addWidget(self.ShotNumberList)
        sbox.addWidget(self.TimeSliceList)
        sbox.addWidget(self.AllTimesBox)

        self.VariablesLabel = QtGui.QLabel("Variables")
        self.VariablesLabel.setEnabled(False)
        self.GrabNEBox = QtGui.QCheckBox("NE")
        self.GrabNEBox.setEnabled(False)
        self.GrabNEBox.toggled.connect(self.toggle_ne)
        self.GrabNIMPBox = QtGui.QCheckBox("NIMP")
        self.GrabNIMPBox.setEnabled(False)
        self.GrabNIMPBox.toggled.connect(self.toggle_nimp)
        self.GrabTEBox = QtGui.QCheckBox("TE")
        self.GrabTEBox.setEnabled(False)
        self.GrabTEBox.toggled.connect(self.toggle_te)
        self.GrabTIBox = QtGui.QCheckBox("TI")
        self.GrabTIBox.setEnabled(False)
        self.GrabTIBox.toggled.connect(self.toggle_ti)
        self.GrabAFBox = QtGui.QCheckBox("AF")
        self.GrabAFBox.setEnabled(False)
        self.GrabAFBox.toggled.connect(self.toggle_af)
        self.GrabNFBox = QtGui.QCheckBox("NF")
        self.GrabNFBox.setEnabled(False)
        self.GrabNFBox.toggled.connect(self.toggle_nf)

        vgbox = QtGui.QGridLayout()
        vgbox.addWidget(self.GrabNEBox,1,0)
        vgbox.addWidget(self.GrabNIMPBox,1,1)
        vgbox.addWidget(self.GrabTEBox,2,0)
        vgbox.addWidget(self.GrabTIBox,2,1)
        vgbox.addWidget(self.GrabAFBox,3,0)
        vgbox.addWidget(self.GrabNFBox,3,1)

        self.CoordinateList = QtGui.QComboBox()
        self.CoordinateList.setEnabled(False)
        self.PrintRawDataButton = QtGui.QPushButton("Print Raw Data to ASCII")
        self.PrintRawDataButton.setEnabled(False)
        self.PrintRawDataButton.clicked.connect(self.print_raw_data)
        self.PrintFitDataButton = QtGui.QPushButton("Print Fit Data to ASCII")
        self.PrintFitDataButton.setEnabled(False)
        self.PrintFitDataButton.clicked.connect(self.print_fit_data)
        self.PlotDataButton = QtGui.QPushButton("Plot Data")
        self.PlotDataButton.setEnabled(False)
        self.PlotDataButton.clicked.connect(self.plot_data)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.VariablesLabel)
        vbox.addLayout(vgbox)
        vbox.addWidget(self.CoordinateList)
        vbox.addWidget(self.PrintRawDataButton)
        vbox.addWidget(self.PrintFitDataButton)
        vbox.addWidget(self.PlotDataButton)

        dbox = QtGui.QHBoxLayout()
        dbox.addLayout(sbox)
        dbox.addLayout(vbox)

        self.figure = matplotlib.figure.Figure()
        self.PlotArea = mplqt.FigureCanvasQTAgg(self.figure)

        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(lbox)
        vbox.addItem(QtGui.QSpacerItem(0,10,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        vbox.addLayout(dbox)
        vbox.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        vbox.addWidget(self.PlotArea)

        self.setLayout(vbox)

        self.setGeometry(20, 20, 300, 500)
        self.setWindowTitle('EX2GK - Data Visualizer and Formatter')
        self.show()


    def reset_variables(self):
        self.VariablesLabel.setEnabled(False)
        self.GrabNEBox.setEnabled(False)
        self.GrabNIMPBox.setEnabled(False)
        self.GrabTEBox.setEnabled(False)
        self.GrabTIBox.setEnabled(False)
        self.GrabAFBox.setEnabled(False)
        self.GrabNFBox.setEnabled(False)


    def reset_time_slices(self):
        self.TimeSliceList.clear()
        self.TimeSliceList.setEnabled(False)
        self.times = np.array([])


    def reset_shot_numbers(self):
        self.ShotNumberList.clear()
        self.ShotNumberList.setEnabled(False)
        self.shotnums = np.array([])


    def populate_variables(self):
        if self.AllTimesBox.isChecked():
            self.VariablesLabel.setEnabled(True)
            self.GrabNEBox.setEnabled(True)
            self.GrabNIMPBox.setEnabled(True)
            self.GrabTEBox.setEnabled(True)
            self.GrabTIBox.setEnabled(True)
            self.GrabAFBox.setEnabled(True)
            self.GrabNFBox.setEnabled(True)
        else:
            if isinstance(self.db,dict):
                sidx = self.ShotNumberList.getCurrentIndex()
                tidx = self.TimeSliceList.getCurrentIndex()
                if self.shotnums.size > 0 and sidx < self.shotnums.size and self.times.shape[0] > 0 and tidx < self.times.shape[0]:
                    sdata = self.db[sidx][tidx]
                    self.VariablesLabel.setEnabled(True)
                    self.GrabNEBox.setEnabled("NE" in sdata and sdata["NE"] is not None)
                    self.GrabNIMPBox.setEnabled("NIMP" in sdata and sdata["NIMP"] is not None)
                    self.GrabTEBox.setEnabled("TE" in sdata and sdata["TE"] is not None)
                    self.GrabTIBox.setEnabled("TI" in sdata and sdata["TI"] is not None)
                    self.GrabAFBox.setEnabled("AF" in sdata and sdata["AF"] is not None)
                    self.GrabNFBox.setEnabled("NF" in sdata and sdata["NF"] is not None)


    def populate_time_slices(self):
        self.reset_time_slices()
        if isinstance(self.db,dict):
            sidx = self.ShotNumberList.getCurrentIndex()
            if self.shotnums.size > 0 and sidx < self.shotnums.size:
                for ii in np.arange(0,len(self.db[sidx])):
                    sdata = self.db[sidx][ii]
                    self.times = np.atleast_2d([sdata["T1"],sdata["T2"]]) if self.times.size == 0 else np.vstack((self.times,[sdata["T1"],sdata["T2"]]))
                    tsstr = "%.3f -- %.3f" % (self.times[ii,0],self.times[ii,1])
                    self.TimeSliceList.addItem(tsstr)
                self.TimeSliceList.setEnabled(True)
        if self.TimeSliceList.count > 0:
            self.TimeSliceList.setCurrentIndex(0)
            self.populate_variables()
            if self.AllTimesBox.isChecked():
                self.TimeSliceList.setEnabled(False)
        else:
            self.reset_variables()


    def populate_shot_numbers(self):
        self.reset_shot_numbers()
        if isinstance(self.db,dict):
            for key in self.db:
                self.shotnums = np.hstack((self.shotnums,int(key)))
            self.shotnums = np.sort(self.shotnums)
            for ii in np.arange(0,self.shotnums.size):
                snstr = "%d" % (self.shotnums[ii])
                self.ShotNumberList.addItem(snstr)
            self.ShotNumberList.setEnabled(True)
        if self.ShotNumberList.count > 0:
            self.ShotNumberList.setCurrentIndex(0)
            self.populate_time_slices()
        else:
            self.reset_time_slices()
            self.reset_variables()


    def toggle_all_times(self):
        self.TimeSliceList.setDisabled(self.AllTimesBox.isChecked())
        if self.TimeSliceList.count > 0:
            self.populate_variables()
        else:
            self.reset_variables()


    def load_database(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '', 'Pickle files (*.p);;All files (*)')
        if filename and os.path.isfile(filename):
            self.db = EX2GK.ptools.unpickle_this(filename)
            self.populate_shot_numbers()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Requested data not available in this time slice.")
            msg.exec_()


    def clear_data(self):
        self.reset_shot_numbers()
        self.reset_time_slices()
        self.reset_variables()


    def get_coordinate(self):
        ocs = "RHOTORN"
        xlabel = "Normalized Toroidal Rho"
        xunit = ""
        self.csidx = self.CoordinateList.currentIndex()
        if self.csidx == 1:
            ocs = "RHOPOLN"
            xlabel = "Normalized Poloidal Rho"
            xunit = ""
        elif self.csidx == 2:
            ocs = "TORFLUXN"
            xlabel = "Normalized Toroidal Flux"
            xunit = ""
        elif self.csidx == 3:
            ocs = "POLFLUXN"
            xlabel = "Normalized Poloidal Flux"
            xunit = ""
        elif self.csidx == 4:
            ocs = "TORFLUX"
            xlabel = "Toroidal Flux"
            xunit = "Wb"
        elif self.csidx == 5:
            ocs = "POLFLUX"
            xlabel = "Poloidal Flux"
            xunit = "Wb"
        elif self.csidx == 6:
            ocs = "RMAJORO"
            xlabel = "Major Radius"
            xunit = "m"
        elif self.csidx == 7:
            ocs = "RMAJORI"
            xlabel = "Major Radius"
            xunit = "m"
        elif self.csidx == 8:
            ocs = "RMIDAVG"
            xlabel = "Midplane-Averaged Minor Radius"
            xunit = "m"
        return (ocs,xlabel,xunit)


    def get_quantity(self):
        key = None
        ylabel = ""
        yunit = ""
        if self.GrabNEBox.isChecked():
            key = "NE"
            ylabel = "Electron Density"
            yunit = "m^-3"
        if self.GrabTEBox.isChecked():
            key = "TE"
            ylabel = "Electron Temperature"
            yunit = "eV"
        if self.GrabTIBox.isChecked():
            key = "TI"
            ylabel = "Ion Temperature"
            yunit = "eV"
        if self.GrabNIMPBox.isChecked():
            key = "NIMP"
            ylabel = "Impurity Ion Density"
            yunit = "m^-3"
        if self.GrabANGFBox.isChecked():
            key = "AF"
            ylabel = "Angular Frequency"
            yunit = "s^-1"
        if self.GrabNFBox.isChecked():
            key = "NF"
            ylabel = "Fast Ion Density"
            yunit = "m^-3"
        return (key,ylabel,yunit)


    def toggle_ne(self):
        print(self.GrabNEBox.isChecked())
#        self.GrabNEBox.setEnabled(False)
#        self.GrabNIMPBox.setEnabled(False)
#        self.GrabTEBox.setEnabled(False)
#        self.GrabTIBox.setEnabled(False)
#        self.GrabAFBox.setEnabled(False)
#        self.GrabNFBox.setEnabled(False)


    def toggle_nimp(self):
        self.GrabNEBox.setEnabled(True)
        self.GrabNIMPBox.setEnabled(True)
        self.GrabTEBox.setEnabled(True)
        self.GrabTIBox.setEnabled(True)
        self.GrabAFBox.setEnabled(True)
        self.GrabNFBox.setEnabled(True)


    def toggle_te(self):
        self.GrabNEBox.setEnabled(True)
        self.GrabNIMPBox.setEnabled(True)
        self.GrabTEBox.setEnabled(True)
        self.GrabTIBox.setEnabled(True)
        self.GrabAFBox.setEnabled(True)
        self.GrabNFBox.setEnabled(True)


    def toggle_ti(self):
        self.GrabNEBox.setEnabled(True)
        self.GrabNIMPBox.setEnabled(True)
        self.GrabTEBox.setEnabled(True)
        self.GrabTIBox.setEnabled(True)
        self.GrabAFBox.setEnabled(True)
        self.GrabNFBox.setEnabled(True)


    def toggle_af(self):
        self.GrabNEBox.setEnabled(True)
        self.GrabNIMPBox.setEnabled(True)
        self.GrabTEBox.setEnabled(True)
        self.GrabTIBox.setEnabled(True)
        self.GrabAFBox.setEnabled(True)
        self.GrabNFBox.setEnabled(True)


    def toggle_nf(self):
        self.GrabNEBox.setEnabled(True)
        self.GrabNIMPBox.setEnabled(True)
        self.GrabTEBox.setEnabled(True)
        self.GrabTIBox.setEnabled(True)
        self.GrabAFBox.setEnabled(True)
        self.GrabNFBox.setEnabled(True)


    def print_raw_data(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if filename and isinstance(self.db,dict):
            (ocs,xlabel,xunit) = get_coordinates()
            (key,ylabel,yunit) = get_quantity()
            sidx = self.ShotNumberList.getCurrentIndex()
            snum = self.shotnums[sidx]
            tidx = np.arange(0,self.times.shape[0]) if self.AllTimesBox.isChecked() else np.array([self.TimeSliceList.getCurrentIndex()])
            if snum in self.db:
                with open(filename,'w') as ff:
                    ff.write("### EX2GK - Raw Data Save File ###\n")
                    ff.write("\n")
                    for ii in tidx:
                        sdata = self.db[snum][ii]
                        if isinstance(sdata,dict):
                            ff.write("START OF HEADER\n")
                            ff.write("\n")
                            ff.write("        Shot Number: %20d\n" % (sdata["SHOT"]))
                            ff.write("  Radial Coordinate: %20s\n" % (ocs))
                            ff.write("  Time Window Start: %18.6f s\n" % (sdata["T1"]))
                            ff.write("    Time Window End: %18.6f s\n" % (sdata["T2"]))
                            ff.write("\n")
                            ff.write("END OF HEADER\n")
                            ff.write("\n")
                            if key+"RAW" in sdata:
                                ics = sdata["CS_OUT"]
                                corrflag = sdata["CC_FLAG"] if "CC_FLAG" in sdata else False
                                xraw = sdata[key+"RAWX"].flatten()
                                xeraw = sdata[key+"RAWXEB"].flatten()
                                (xraw,csj,xeraw) = EX2GK.ptools.convert_coords(sdata,xraw,ics,ocs,corrflag=corrflag)
                                yraw = sdata[key+"RAW"].flatten()
                                yeraw = sdata[key+"RAWEB"].flatten()
                                ff.write("%15s%15s%15s%15s\n" % (ocs,key,"Err. "+key,"Err. "+ocs))
                                for ii in np.arange(0,xraw.size):
                                    ff.write("%15.4f%15.6e%15.6e%15.4f\n" % (xraw[ii],yraw[ii],yeraw[ii],xeraw[ii]))
                                ff.write("\n")
            print("Raw data written into %s." % (filename))

    def print_fit_data(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if filename and isinstance(self.db,dict):
            (ocs,xlabel,xunit) = get_coordinates()
            (key,ylabel,yunit) = get_quantity()
            sidx = self.ShotNumberList.getCurrentIndex()
            snum = self.shotnums[sidx]
            tidx = np.arange(0,self.times.shape[0]) if self.AllTimesBox.isChecked() else np.array([self.TimeSliceList.getCurrentIndex()])
            if snum in self.db:
                with open(filename,'w') as ff:
                    ff.write("### EX2GK - Fit Data Save File ###\n")
                    ff.write("\n")
                    for ii in tidx:
                        sdata = self.db[snum][ii]
                        if isinstance(sdata,dict):
                            ff.write("START OF HEADER\n")
                            ff.write("\n")
                            ff.write("        Shot Number: %20d\n" % (sdata["SHOT"]))
                            ff.write("  Radial Coordinate: %20s\n" % (ocs))
                            ff.write("  Time Window Start: %18.6f s\n" % (sdata["T1"]))
                            ff.write("    Time Window End: %18.6f s\n" % (sdata["T2"]))
                            ff.write("\n")
                            ff.write("END OF HEADER\n")
                            ff.write("\n")
                            if key+"RAW" in sdata:
                                ics = sdata["CS_OUT"]
                                corrflag = sdata["CC_FLAG"] if "CC_FLAG" in sdata else False
                                xfit = sdata[ics].flatten()
                                (xfit,csj,xefitc) = EX2GK.ptools.convert_coords(sdata,xfit,fcs,ocs,corrflag=corrflag)
                                yfit = sdata[key].flatten()
                                yefit = sdata[key+"EB"].flatten()
                                ff.write("%15s%15s%15s\n" % (ocs,key+" Fit","Err. "+key+" Fit"))
                                for ii in np.arange(0,xfit.size):
                                    ff.write("%15.4f%15.6e%15.6e\n" % (xfit[ii],yfit[ii],yefit[ii]))
                                ff.write("\n")
            print("Fit data written into %s." % (filename))


    def plot_data(self):
        if isinstance(self.db,dict):
            (ocs,xlabel,xunit) = get_coordinates()
            (key,ylabel,yunit) = get_quantity()
            sidx = self.ShotNumberList.getCurrentIndex()
            snum = self.shotnums[sidx]
            if not self.AllTimesBox.isChecked():
                tidx = self.TimeSliceList.getCurrentIndex()
                sdata = self.db[snum][tidx]
                styles = ['+','x','o','s','d','^','v','<','>','*']
                fig = self.figure
                fig.clear()
                ax = fig.add_subplot(111)
                if key in sdata and key+"RAW" in sdata and sdata[key].size > 1:
                    corrflag = sdata["CC_FLAG"] if "CC_FLAG" in sdata else False
                    xraw = sdata[key+"RAWX"].flatten()
                    xeraw = sdata[key+"RAWXEB"].flatten()
                    if not re.match(ics,ocs,flags=re.IGNORECASE):
                        (xraw,csj,xeraw) = EX2GK.ptools.convert_coords(sdata,xraw,ics,ocs,corrflag=corrflag)
                    yraw = sdata[key+"RAW"].flatten()
                    yeraw = sdata[key+"RAWEB"].flatten()
                    xfit = sdata[sdata["CS_OUT"]].flatten()
                    yfit = sdata[key].flatten()
                    yefit = sdata[key+"EB"].flatten()
                    for ii in np.arange(0,len(sdata[key+"DIAG"])):
                        filt = (sdata[key+"DMAP"] == ii)
                        if np.any(filt):
                            ax.errorbar(xraw[filt],yraw[filt],yerr=yeraw[filt],color='k',ls='',marker=styles[ii])
                    ax.plot(xfit,yfit,color='r',ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit,yl,yu,facecolor='r',edgecolor='None',alpha=0.2)
                ax.set_ylabel(ylabel)
                ax.set_xlabel(xlabel)
                self.PlotArea.figure.draw()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Requested data not available in this time slice.")
            msg.exec_()


def main():

    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('EX2GK Extractor')
    ex = extractor_EX2GK()

    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
