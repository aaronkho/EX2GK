#!/usr/bin/env python

# Required imports
import os
import sys
import socket
import datetime
import re
import pwd
import time
import copy
import pickle
import collections
import importlib
import inspect
import packaging.version as pversion
import numpy as np

from IPython import embed
QtCore = None
QtGui = None
QtWidgets = None
try:
    from PyQt4 import QtCore, QtGui
    QtWidgets = QtGui
except ImportError:
    from PyQt5 import QtCore, QtGui, QtWidgets
pyqtversion = QtCore.PYQT_VERSION_STR if QtCore is not None else "0"

import matplotlib
old_mpl = pversion.parse(matplotlib.__version__) <= pversion.parse("2.0.0")
fqt5 = pversion.parse(pyqtversion) >= pversion.parse("5.0.0")
mplqt = None
if fqt5:
    matplotlib.use("Qt5Agg")
    from matplotlib.backends import backend_qt5agg as mplqt
else:
    try:
        matplotlib.use("Qt4Agg")
        from matplotlib.backends import backend_qt4agg as mplqt
    except ValueError:
        matplotlib.use("QtAgg")
        from matplotlib.backends import backend_qtagg as mplqt
from matplotlib import figure as mplfig

if QtCore is None or QtGui is None or QtWidgets is None or mplqt is None:
    raise ImportError("Python install does not have Qt support! GUI cannot be used!")

from EX2GK import EX2GK, __version__

currentdir = os.getcwd()
customfilters = None
fcustomincurrent = False
if os.path.isfile(currentdir+'/EX2GK_custom_filters.py'):
    spec = importlib.util.spec_from_file_location('EX2GK_custom_filters',currentdir+'/EX2GK_custom_filters.py')
    customfilters = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(customfilters)
    fcustomincurrent = True
if customfilters is None:
    try:
        customfilters = importlib.import_module('EX2GK_custom_filters')
    except (ImportError,AttributeError):
        try:
            customfilters = importlib.import_module('.EX2GK_custom_filters',package='EX2GK')
        except:
            pass


data_fields = {"*"   : {"IDZ": [("Zeff","timeZeff","rhop",1),("Zeff_unc","timeZeff","rhop",1)],
                        "TOT": [("Zeff","time","-",1),("P_OH","time","-",1),("SHINE_TH","time","-",1),
                                ("Wmhd","time","-",1),("Wth","time","-",1),("Wfi","time","-",1),
                                ("beta_N","time","-",1),("beta_Nth","time","-",1)],
                        "MAG": [("ULid12","T-MAG-1","-",1)],
                        "FPG": [("Rgeo","time","-",1),("ahor","time","-",1),("betpol","time","-",1)],
                        "ZES": [("Zeff","TimeBase","-",1),("axk-H1","TimeBase","-",1),("ZeffProf","TimeBase","rho",1),("ZeffErr","TimeBase","rho",1)],
                        "FPC": [("BTF","TIMEF","-",1),("IpiFP","TIMEF","-",1)],
                        "BMA": [("Wdia","time","-",1),("betdia","time","-",1)],
                        "UVS": [("H_tot","time","-",1),("D_tot","time","-",1),("He_tot","time","-",1),("N_tot","time","-",1),("Ne_tot","time","-",1)],
                        "CXF": [("H2HD","time","-",1)],
                        "NIS": [("PNI","T-B","-",1),("PNIQ","T-B","-",1)],
                        "ICP": [("PICRN","T-B","-",1),("PICRFl","T-B","-",1)],
                        "ECS": [("PECRH","T-B","-",1)],
                        "ENR": [("NRATE_II","Timebase","-",1),("NRATE","Timebase","-",1),("NRATEERR","Timebase","-",1)]
                       },
               "TT"  : {"TOT": [("Zeff","time","-",1),("P_OH","time","-",1),("SHINE_TH","time","-",1),
                                ("Wmhd","time","-",1),("Wth","time","-",1),("Wfi","time","-",1),
                                ("beta_N","time","-",1),("beta_Nth","time","-",1)],
                        "MAG": [("ULid12","T-MAG-1","-",1)],
                        "ZES": [("Zeff","TimeBase","-",1),("axk-H1","TimeBase","-",1)],
                        "FPC": [("BTF","TIMEF","-",1),("IpiFP","TIMEF","-",1)],
                        "BMA": [("Wdia","time","-",1),("betdia","time","-",1)],
                        "UVS": [("H_tot","time","-",1),("D_tot","time","-",1),("He_tot","time","-",1),("N_tot","time","-",1),("Ne_tot","time","-",1)],
                        "CXF": [("H2HD","time","-",1)],
                        "NIS": [("PNI","T-B","-",1),("PNIQ","T-B","-",1)],
                        "ICP": [("PICRN","T-B","-",1),("PICRFl","T-B","-",1)],
                        "ECS": [("PECRH","T-B","-",1)],
                        "ENR": [("NRATE_II","Timebase","-",1),("NRATE","Timebase","-",1),("NRATEERR","Timebase","-",1)]
                       },
               "EQ"  : {"IDE": [("PFM","time","Ri,Zj",0),("TFLx","time","PFL",0),("Qpsi","time","PFL",0),("Jpol","time","PFL",0),
                                ("Vol","time","PFL",0),("Area","time","PFL",0),("SSQ","time","-",0),("SSQnam","-","-",0)],
                        "EQH": [("PFM","time","Ri,Zj",0),("TFLx","time","PFL",0),("Qpsi","time","PFL",0),("Jpol","time","PFL",0),
                                ("Vol","time","PFL",0),("Area","time","PFL",0),("SSQ","time","-",0),("SSQnam","-","-",0)],
                        "EQI": [("PFM","time","Ri,Zj",0),("TFLx","time","PFL",0),("Qpsi","time","PFL",0),("Jpol","time","PFL",0),
                                ("Vol","time","PFL",0),("Area","time","PFL",0),("SSQ","time","-",0),("SSQnam","-","-",0)],
                        "FPP": [("PFM","time","Ri,Zj",0),("TFLx","time","PFL",0),("Qpsi","time","PFL",0),("Jpol","time","PFL",0),
                                ("Vol","time","PFL",0),("Area","time","PFL",0),("SSQ","time","-",0),("SSQnam","-","-",0)],
                       },
               "NE"  : {"IDA": [("ne","time","rhop",1),("ne_unc","time","rhop",1),("dne_dr","time","rhop",1)],
                        "VTA": [("Ne_c","TIM_CORE","R_core,Z_core",1),("SigNe_c","TIM_CORE","R_core,Z_core",1),
                                ("Ne_e","TIM_EDGE","R_edge,Z_edge",1),("SigNe_e","TIM_EDGE","R_edge,Z_edge",1)],
                        "VTN": [("Nd","TIME","Rd,Zd",1),("Ndlow","TIME","Rd,Zd",1),("Ndupp","TIME","Rd,Zd",1),
                                ("Ne","TIME","Re,Ze",1),("Nelow","TIME","Re,Ze",1),("Neupp","TIME","Re,Ze",1),
                                ("Nf","TIME","Rf,Zf",1),("Nflow","TIME","Rf,Zf",1),("Nfupp","TIME","Rf,Zf",1)],
                        "LIN": [("ne","time","R,Z",1),("ne_unc","time","R,Z",1)],
                        "DCR": [("profile","time_c","-",1)],
                        "DLP": [("ne","time","rp",1)],
                        "DPR": [("ne","time","rp",1)]
                       },
               "TE"  : {"IDA": [("Te","time","rhop",1),("Te_unc","time","rhop",1),("dTe_dr","time","rhop",1)],
                        "VTA": [("Te_c","TIM_CORE","R_core,Z_core",1),("SigTe_c","TIM_CORE","R_core,Z_core",1),
                                ("Te_e","TIM_EDGE","R_edge,Z_edge",1),("SigTe_e","TIM_EDGE","R_edge,Z_edge",1)
                               ],
                        "VTN": [("Td","TIME","Rd,Zd",1),("Tdlow","TIME","Rd,Zd",1),("Tdupp","TIME","Rd,Zd",1),
                                ("Te","TIME","Re,Ze",1),("Telow","TIME","Re,Ze",1),("Teupp","TIME","Re,Ze",1),
                                ("Tf","TIME","Rf,Zf",1),("Tflow","TIME","Rf,Zf",1),("Tfupp","TIME","Rf,Zf",1)],
                        "CEC": [("Trad-A","time-A","R-A,z-A",1)],
                        "RMD": [("Trad-A","time-A","R-A,z-A",1)]
                       },
               "TI":   {"CAZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
                        "CCZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
                        "CEZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2),
                                ("Ti_c","time","R_time,z_time",1),("err_Ti_c","time","R_time,z_time",1),
                                ("Ti","time","R_time,z_time",1),("err_Ti","time","R_time,z_time",1),
                                ("fit_stat","time","R_time,z_time",1),("inte","time","R_time,z_time",1)],
                        "CHZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
                        "CMZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
                        "CNZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
                        "COZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
#                        "CPZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
#                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],    # Used for poloidal rotation only
                        "CUZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)],
                        "CXZ": [("Ti_c","time","R,z",1),("err_Ti_c","time","R,z",1),("Ti","time","R,z",1),("err_Ti","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),("LineInfo","Number","-",2),("LineInfo","Z","-",2)]
                       },
               "NI"  : {},
               "NIMP": {"CAS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CCS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CES": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CHS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CMS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CNS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "COS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CPS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CUS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)],
                        "CXS": [("nimp","time","R,z",1),("nimp_plc","time","R,z",1),("err_nimp","time","R,z",1),("Info","imp_Z","-",2)]
                       },
               "AF"  : {"CAZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CCZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CEZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1),
                                ("vrot","time","R_time,z_time",1),("err_vrot","time","R_time,z_time",1),
                                ("fit_stat","time","R_time,z_time",1),("inte","time","R_time,z_time",1)],
                        "CHZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CMZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CNZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "COZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CPZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CUZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)],
                        "CXZ": [("vrot","time","R,z",1),("err_vrot","time","R,z",1),
                                ("fit_stat","time","R,z",1),("inte","time","R,z",1)]
                       },
               "FI"  : {},
               "QSTJ": {}
              }


class QTableWidgetTimeWindow(QtGui.QTableWidgetItem):

    def __init__(self,value,sdata,index):
        super(QTableWidgetTimeWindow,self).__init__('%s' % (value))
        self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self._sdata = copy.deepcopy(sdata) if isinstance(sdata,EX2GK.classes.EX2GKTimeWindow) else dict()
        self._tw = int(index) if int(index) >= 0 else -1

    def __eq__(self,other):
        if (isinstance(other, self.__class__)):
            eqsnum = False
            eqtbeg = False
            eqtend = False
            if self._sdata and other._sdata:
                eqsnum = ("SHOT" in self._sdata["META"] and "SHOT" in other._sdata["META"] and self._sdata["META"]["SHOT"] == other._sdata["META"]["SHOT"])
                eqtbeg = ("T1" in self._sdata["META"] and "T1" in other._sdata["META"] and np.abs(self._sdata["META"]["T1"] - other._sdata["META"]["T1"]) < 1.0e6)
                eqtend = ("T2" in self._sdata["META"] and "T2" in other._sdata["META"] and np.abs(self._sdata["META"]["T2"] - other._sdata["META"]["T2"]) < 1.0e6)
            return (eqsnum and eqtbeg and eqtend)
        else:
            return super(QTableWidgetTimeWindow,self).__eq__(other)

    def __ne__(self,other):
        return not self.__eq__(other)

    def get_data(self):
        return self._sdata

    def get_shot_number(self):
        val = self._sdata["META"]["SHOT"] if "SHOT" in self._sdata["META"] else None
        return val

    def get_time_window_number(self):
        return self._tw

    def set_data(self,sdata):
        if isinstance(sdata,dict):
            self._sdata = copy.deepcopy(sdata)

    def set_time_window_number(self,index):
        if int(index) >= 0:
            self._tw = int(index)


class time_trace_plotter(QtGui.QWidget):

    def __init__(self,data,ti,tf):
        super(time_trace_plotter, self).__init__()
        self.sdata = None
        self.ti = None
        self.tf = None
        if isinstance(data,dict):
            self.sdata = data
        if isinstance(ti,(int,float)):
            self.ti = float(ti)
        if isinstance(tf,(int,float)):
            self.tf = float(tf)
        if self.sdata is not None:
            self.initUI()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.exec_()

    def initUI(self):

        fig = matplotlib.figure.Figure()
        ti = -9999.9999 if self.ti is None else self.ti
        tf = 9999.9999 if self.tf is None else self.tf

        ax1 = fig.add_subplot(121)
        if "IP" in self.sdata and self.sdata["IP"] is not None and "TIP" in self.sdata and self.sdata["TIP"] is not None:
            tfilt = np.all([self.sdata["TIP"] >= ti,self.sdata["TIP"] <= tf],axis=0)
            ax1.plot(self.sdata["TIP"][tfilt],np.abs(self.sdata["IP"][tfilt])*1.0e-6,color='r',label=r'I_p [MA]')
            if "BMAG" in self.sdata and self.sdata["BMAG"] is not None and "TBMAG" in self.sdata and self.sdata["TBMAG"] is not None:
                tfilt = np.all([self.sdata["TBMAG"] >= ti,self.sdata["TBMAG"] <= tf],axis=0)
                ax1.plot(self.sdata["TBMAG"][tfilt],np.abs(self.sdata["BMAG"][tfilt]),color='b',label=r'B_{mag} [T]')
            if "WMHD" in self.sdata and self.sdata["WMHD"] is not None and "TWMHD" in self.sdata and self.sdata["TWMHD"] is not None:
                tfilt = np.all([self.sdata["TWMHD"] >= ti,self.sdata["TWMHD"] <= tf],axis=0)
                ax1.plot(self.sdata["TWMHD"][tfilt],self.sdata["WMHD"][tfilt]*1.0e-6,color='g',label=r'W_{mhd} [MJ]')
            if "WTH" in self.sdata and self.sdata["WTH"] is not None and "TWTH" in self.sdata and self.sdata["TWTH"] is not None:
                tfilt = np.all([self.sdata["TWTH"] >= ti,self.sdata["TWTH"] <= tf],axis=0)
                ax1.plot(self.sdata["TWTH"][tfilt],self.sdata["WTH"][tfilt]*1.0e-6,color='m',label=r'W_{th} [MJ]')
            if "ZEFS" in self.sdata and self.sdata["ZEFS"] is not None and "TZEFS" in self.sdata and self.sdata["TZEFS"] is not None:
                tfilt = np.all([self.sdata["TZEFS"] >= ti,self.sdata["TZEFS"] <= tf],axis=0)
                ax1.plot(self.sdata["TZEFS"][tfilt],self.sdata["ZEFS"][tfilt],color='c',label=r'Z_{eff}')
            elif "ZEFT" in self.sdata and self.sdata["ZEFT"] is not None and "TZEFT" in self.sdata and self.sdata["TZEFT"] is not None:
                tfilt = np.all([self.sdata["TZEFT"] >= ti,self.sdata["TZEFT"] <= tf],axis=0)
                ax1.plot(self.sdata["TZEFT"][tfilt],self.sdata["ZEFT"][tfilt],color='c',label=r'Z_{eff}')
            ax1.legend(loc='best')
        ax1.set_ylabel(r'Plasma Parameters')
        ax1.set_xlabel(r'Time [s]')

        ax2 = fig.add_subplot(122)
        if "OHM" in self.sdata and self.sdata["OHM"] is not None and "TOHM" in self.sdata and self.sdata["TOHM"] is not None:
            sc = 1.0e-3 if np.mean(np.abs(self.sdata["OHM"])) < 1.0e4 else 1.0e-6
            unit = 'kW' if np.mean(np.abs(self.sdata["OHM"])) < 1.0e4 else 'MW'
            tfilt = np.all([self.sdata["TOHM"] >= ti,self.sdata["TOHM"] <= tf],axis=0)
            ax2.plot(self.sdata["TOHM"][tfilt],self.sdata["OHM"][tfilt]*sc,color='k',label=r'P_{ohm} ['+unit+']')
            if "ICRH" in self.sdata and self.sdata["ICRH"] is not None and "TICRH" in self.sdata and self.sdata["TICRH"] is not None:
                tfilt = np.all([self.sdata["TICRH"] >= ti,self.sdata["TICRH"] <= tf],axis=0)
                ax2.plot(self.sdata["TICRH"][tfilt],self.sdata["ICRH"][tfilt]*1.0e-6,color='b',label=r'P_{IC} [MW]')
            if "NBI" in self.sdata and self.sdata["NBI"] is not None and "TNBI" in self.sdata and self.sdata["TNBI"] is not None:
                tfilt = np.all([self.sdata["TNBI"] >= ti,self.sdata["TNBI"] <= tf],axis=0)
                ax2.plot(self.sdata["TNBI"][tfilt],self.sdata["NBI"][tfilt]*1.0e-6,color='r',label=r'P_{NBI} [MW]')
            if "ECRH" in self.sdata and self.sdata["ECRH"] is not None and "TECRH" in self.sdata and self.sdata["TECRH"] is not None:
                tfilt = np.all([self.sdata["TECRH"] >= ti,self.sdata["TECRH"] <= tf],axis=0)
                ax2.plot(self.sdata["TECRH"][tfilt],self.sdata["ECRH"][tfilt]*1.0e-6,color='g',label=r'P_{EC} [MW]')
            ax2.legend(loc='best')
        ax2.set_ylabel(r'Plasma Heating')
        ax2.set_xlabel(r'Time [s]')

        self.TPlotWidget = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtGui.QVBoxLayout()
        gbox.addWidget(self.TPlotWidget)

        self.setLayout(gbox)

        self.setGeometry(10, 10, 1100, 500)
        self.setWindowTitle('Time Trace Plots')
        self.show()


class profile_plotter(QtGui.QWidget):

    def __init__(self,data,sigma=None,csidx=None,xerrflag=False):
        super(profile_plotter, self).__init__()
        self.sdata = None
        self.sigma = 1.0
        self.csidx = 0
        self.xerrflag = False
        if isinstance(data,EX2GK.classes.EX2GKTimeWindow) and data.isReady():
            self.sdata = data
        if isinstance(sigma,(int,float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if isinstance(csidx,(int, float)):
            self.csidx = int(csidx)
        if xerrflag:
            self.xerrflag = True
        if self.sdata is not None:
            self.initUI()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data for profile plotting tool.")
            msg.exec_()

    def initUI(self):

        ocp = self.sdata["META"]["CSOP"]
        cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
        ocs = cslist[0]
        xlims = (0.0,1.0)
        fxlim = True
        if self.csidx < len(cslist) and ocp+cslist[self.csidx] in self.sdata["CD"].coord_keys():
            ocs = cslist[self.csidx]
            xlims = (np.amin(self.sdata["CD"][ocp][ocs]["V"]),np.amax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX","POLFLUX","RMAJORO","RMAJORI"]:
                fxlim = False
        (xt,xlabel,xunit,fl,fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + " [" + xunit + "]" if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["NE","TE","TIMP","NIMP","NFINBI","WFINBI","AFTOR","NFIICRH","WFIICRH"]
        qqextra = {"NIMP": "N", "TIMP": "T"}
        qscales = {"NE" : 19, "TE" : 3, "NIMP": 17, "TIMP" : 3, "NFINBI": 18, "WFINBI" : 3, "AFTOR" : 3, "NFIICRH" : 18, "WFIICRH" : 3}
        styles = ['+','x','o','s','d','^','v','<','>','*']
        colors = ['r','b','g','m','c','y']
        fig = matplotlib.figure.Figure()
        ny = 3
        nx = 3

        for qq in np.arange(0,len(qlist)):
            (qtag,ylabel,yunit,fqlbl,fqunit) = EX2GK.ptools.define_quantity(qlist[qq],exponent=qscales[qlist[qq]])
            scale = np.power(10.0,-qscales[qlist[qq]])
            nplot = qq + 1
            ax = fig.add_subplot(ny,nx,nplot)
            si = 0
            ci = 0
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1:
                ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ocp
                xraw = self.sdata["RD"][qtag]["X"].flatten()
                xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                if ics != ocs:
                    (xraw,dj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,ocp)
                    xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                yraw = self.sdata["RD"][qtag][""].flatten()
                yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                for ii in np.arange(0,len(self.sdata["RD"][qtag]["SRC"])):
                    filt = (self.sdata["RD"][qtag]["MAP"] == ii)
                    if np.any(filt):
                        xerr = self.sigma * xeraw[filt]
                        yerr = self.sigma * yeraw[filt]
                        if self.xerrflag:
                            ax.errorbar(xraw[filt],yraw[filt]*scale,xerr=xerr,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                        else:
                            ax.errorbar(xraw[filt],yraw[filt]*scale,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                        si = si + 1
            if qlist[qq] in qqextra:
                nqtag = qqextra[qlist[qq]]
                for idx in np.arange(0,numimp):
                    itag = "%d" % (idx+1)
                    if nqtag+"IMP"+itag in self.sdata["RD"] and self.sdata["RD"][nqtag+"IMP"+itag].npoints > 0:
                        ics = self.sdata["RD"][nqtag+"IMP"+itag].coordinate if self.sdata["RD"][nqtag+"IMP"+itag].coordinate is not None else ocs
                        icp = self.sdata["RD"][nqtag+"IMP"+itag].coord_prefix if self.sdata["RD"][nqtag+"IMP"+itag].coordinate is not None else ocp
                        xraw = self.sdata["RD"][nqtag+"IMP"+itag]["X"].flatten()
                        xeraw = self.sdata["RD"][nqtag+"IMP"+itag]["XEB"].flatten()
                        if ics != ocs:
                            (xraw,dj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,ocp)
                            xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                        yraw = self.sdata["RD"][nqtag+"IMP"+itag][""].flatten()
                        yeraw = self.sdata["RD"][nqtag+"IMP"+itag]["EB"].flatten()
                        for ii in np.arange(0,len(self.sdata["RD"][nqtag+"IMP"+itag]["SRC"])):
                            filt = (self.sdata["RD"][nqtag+"IMP"+itag]["MAP"] == ii)
                            if np.any(filt):
                                xerr = self.sigma * xeraw[filt]
                                yerr = self.sigma * yeraw[filt]
                                if self.xerrflag:
                                    ax.errorbar(xraw[filt],yraw[filt]*scale,xerr=xerr,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                                else:
                                    ax.errorbar(xraw[filt],yraw[filt]*scale,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                                si = si + 1
                    if "PD" in self.sdata and nqtag+"IMP"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"IMP"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["PD"][nqtag+"IMP"+itag][""].flatten()
                        yefit = self.sdata["PD"][nqtag+"IMP"+itag]["EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
                    elif nqtag+"IMP"+itag+"_" in self.sdata["RD"] and self.sdata["RD"][nqtag+"IMP"+itag+"_"].npoints > 1:
                        ics = self.sdata["RD"][nqtag+"IMP"+itag+"_"].coordinate if self.sdata["RD"][nqtag+"IMP"+itag+"_"].coordinate is not None else ocs
                        icp = self.sdata["RD"][nqtag+"IMP"+itag+"_"].coord_prefix if self.sdata["RD"][nqtag+"IMP"+itag+"_"].coordinate is not None else ocp
                        xfit = self.sdata["RD"][nqtag+"IMP"+itag+"_"]["X"].flatten()
                        xefit = self.sdata["RD"][nqtag+"IMP"+itag+"_"]["XEB"].flatten()
                        if ics != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,ics,ocs,icp,ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["RD"][nqtag+"IMP"+itag+"_"][""].flatten()
                        yefit = self.sdata["RD"][nqtag+"IMP"+itag+"_"]["EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
                for idx in np.arange(0,numz):
                    itag = "%d" % (idx+1)
                    if "PD" in self.sdata and nqtag+"Z"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"Z"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["PD"][nqtag+"Z"+itag][""].flatten()
                        yefit = self.sdata["PD"][nqtag+"Z"+itag]["EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
                    elif nqtag+"Z"+itag+"_" in self.sdata["RD"] and self.sdata["RD"][nqtag+"Z"+itag+"_"].npoints > 1:
                        ics = self.sdata["RD"][nqtag+"Z"+itag+"_"].coordinate if self.sdata["RD"][nqtag+"Z"+itag+"_"].coordinate is not None else ocs
                        icp = self.sdata["RD"][nqtag+"Z"+itag+"_"].coord_prefix if self.sdata["RD"][nqtag+"Z"+itag+"_"].coordinate is not None else ocp
                        xfit = self.sdata["RD"][nqtag+"Z"+itag+"_"]["X"].flatten()
                        xefit = self.sdata["RD"][nqtag+"Z"+itag+"_"]["XEB"].flatten()
                        if ics != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,ics,ocs,icp,ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["RD"][nqtag+"Z"+itag+"_"][""].flatten()
                        yefit = self.sdata["RD"][nqtag+"Z"+itag+"_"]["EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
                if qlist[qq] == "TIMP":
                    minsize = 0 if self.sdata["FLAG"].checkFlag("TISCALE") else 3
                    if "TI" in self.sdata["RD"] and self.sdata["RD"]["TI"].npoints > minsize:
                        ics = self.sdata["RD"]["TI"].coordinate if self.sdata["RD"]["TI"].coordinate is not None else ocs
                        icp = self.sdata["RD"]["TI"].coord_prefix if self.sdata["RD"]["TI"].coordinate is not None else ocp
                        xraw = self.sdata["RD"]["TI"]["X"].flatten()
                        xeraw = self.sdata["RD"]["TI"]["XEB"].flatten()
                        if ics != ocs:
                            (xraw,dj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,ocp)
                            xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                        yraw = self.sdata["RD"]["TI"][""].flatten()
                        yeraw = self.sdata["RD"]["TI"]["EB"].flatten()
                        for ii in np.arange(0,len(self.sdata["RD"]["TI"]["SRC"])):
                            filt = (self.sdata["RD"]["TI"]["MAP"] == ii)
                            if np.any(filt):
                                xerr = self.sigma * xeraw[filt]
                                yerr = self.sigma * yeraw[filt]
                                if self.xerrflag:
                                    ax.errorbar(xraw[filt],yraw[filt]*scale,xerr=xerr,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                                else:
                                    ax.errorbar(xraw[filt],yraw[filt]*scale,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                                si = si + 1
                        if "PD" in self.sdata and "TI1" in self.sdata["PD"] and self.sdata["PD"]["TI1"] is not None:
                            xfit = self.sdata["PD"]["X"][""].flatten()
                            xefit = self.sdata["PD"]["X"]["EB"].flatten()
                            if self.sdata["META"]["CSO"] != ocs:
                                (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                                xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                            yfit = self.sdata["PD"]["TI1"][""].flatten()
                            yefit = self.sdata["PD"]["TI1"]["EB"].flatten()
                            ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                            yl = yfit - self.sigma * yefit
                            yu = yfit + self.sigma * yefit
                            ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                            ci = ci + 1
                        elif "TIMP"+itag+"_" in self.sdata["RD"] and self.sdata["RD"]["TIMP"+itag+"_"].npoints > 1:
                            ics = self.sdata["RD"]["TIMP"+itag+"_"].coordinate if self.sdata["RD"]["TIMP"+itag+"_"].coordinate is not None else ocs
                            icp = self.sdata["RD"]["TIMP"+itag+"_"].coord_prefix if self.sdata["RD"]["TIMP"+itag+"_"].coordinate is not None else ocp
                            xfit = self.sdata["RD"]["TIMP"+itag+"_"]["X"].flatten()
                            xefit = self.sdata["RD"]["TIMP"+itag+"_"]["XEB"].flatten()
                            if ics != ocs:
                                (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,ics,ocs,icp,ocp)
                                xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                            yfit = self.sdata["RD"]["TIMP"+itag+"_"][""].flatten()
                            yefit = self.sdata["RD"]["TIMP"+itag+"_"]["EB"].flatten()
                            ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                            yl = yfit - self.sigma * yefit
                            yu = yfit + self.sigma * yefit
                            ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                            ci = ci + 1
            else:
                if "PD" in self.sdata and qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                        xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                    ci = ci + 1
                elif qtag+"_" in self.sdata["RD"] and self.sdata["RD"][qtag+"_"].npoints > 1:
                    ics = self.sdata["RD"][qtag+"_"].coordinate if self.sdata["RD"][qtag+"_"].coordinate is not None else ocs
                    icp = self.sdata["RD"][qtag+"_"].coord_prefix if self.sdata["RD"][qtag+"_"].coordinate is not None else ocp
                    xfit = self.sdata["RD"][qtag+"_"]["X"].flatten()
                    xefit = self.sdata["RD"][qtag+"_"]["XEB"].flatten()
                    if ics != ocs:
                        (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,ics,ocs,icp,ocp)
                        xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                    yfit = self.sdata["RD"][qtag+"_"][""].flatten()
                    yefit = self.sdata["RD"][qtag+"_"]["EB"].flatten()
                    ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                    ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0],xlims[1])
            if qtag == "TIMP":
                qlabel = r'Ion Temperature'
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel)
            ax.set_xlabel(pxlabel)

        sid = "%s #%d: %.4f - %.4f" % (self.sdata["META"]["DEVICE"],self.sdata["META"]["SHOT"],self.sdata["META"]["T1"],self.sdata["META"]["T2"])
        ida_warning = "IDA profiles may be shifted as it recalculates a refined equilibrium."
        fig.suptitle(sid+"\n"+ida_warning,fontsize=18)

        self.PPlotWidget = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtGui.QVBoxLayout()
        gbox.addWidget(self.PPlotWidget)

        self.setLayout(gbox)

        self.setGeometry(10, 10, 1100, 1100)
        self.setWindowTitle('Profile Plots')
        self.show()


class gradient_plotter(QtGui.QWidget):

    def __init__(self,data,sigma=None,csidx=None,xerrflag=False,procflag=False):
        super(gradient_plotter, self).__init__()
        self.sdata = None
        self.sigma = 1.0
        self.csidx = 0
        self.xerrflag = False
        self.procflag = False
        if isinstance(data,dict):
            self.sdata = data
        if isinstance(sigma,(int,float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if isinstance(csidx,(int, float)):
            self.csidx = int(csidx)
        if xerrflag:
            self.xerrflag = True
        if procflag:
            self.procflag = True
        if self.sdata is not None:
            self.initUI()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.exec_()

    def initUI(self):

        ocp = self.sdata["META"]["CSOP"]
        cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
        ocs = cslist[0]
        xlims = (0.0,1.0)
        fxlim = True
        if self.csidx < len(cslist) and ocp+cslist[self.csidx] in self.sdata["CD"].coord_keys():
            ocs = cslist[self.csidx]
            xlims = (np.amin(self.sdata["CD"][ocp][ocs]["V"]),np.amax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX","POLFLUX","RMAJORO","RMAJORI"]:
                fxlim = False
        (xt,xlabel,xunit,fl,fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + " [" + xunit + "]" if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["NE","TE","TIMP","TI","NIMP","AFTOR"]
        qqextra = {"NIMP": "N", "TIMP": "T"}
        qscales = {"NE" : 19, "TE" : 3, "NIMP": 17, "TIMP" : 3, "TI": 3, "AFTOR" : 3}
        qlims = {"NE": (-3.0,10.0), "TE": (-3.0,10.0), "NIMP": (-3.0,10.0), "TIMP": (-3.0,10.0), "TI": (-3.0,10.0), "AFTOR": (-10.0,40.0)}
        styles = ['+','x','o','s','d','^','v','<','>','*']
        colors = ['r','b','g','m','c','y']
        fig = matplotlib.figure.Figure()
        ny = 3
        nx = 2

        dq_opts = {"CMN":    {"NE": "CMN_ILNE", "TE":   "CMN_ILTE", "TIMP": "CMN_ILTIMP", "TI":   "CMN_ILTI", "NIMP": "CMN_ILNIMP", "AFTOR":   "CMN_ILAF"}, \
                   "QLK":    {"NE":  "QLK_ANE", "TE":    "QLK_ATE", "TIMP":  "QLK_ATIMP", "TI":    "QLK_ATI", "NIMP":  "QLK_ANIMP", "AFTOR":  "QLK_AUTOR"}   }
        ctag = self.sdata["META"]["CODETAG"] if self.procflag and self.sdata["META"]["CODETAG"] in dq_opts else "None"
        yunitd = r'[1' if ctag in dq_opts else r''
        yunitd = yunitd + r' / ' + xunit + r']' if xunit else r''

        for qq in np.arange(0,len(qlist)):
            qexp = qscales[qlist[qq]] if ctag not in dq_opts else 0
            (qtag,ylabel,yunit,fqlbl,fqunit) = EX2GK.ptools.define_quantity(qlist[qq],exponent=qexp)
            if ctag in dq_opts:
                qtag = "OUT"+dq_opts[ctag][qlist[qq]]
            ylabel = r'Log. ' + ylabel + r' Grad.' if ctag in dq_opts else ylabel + r' Grad.'
            gtag = "" if ctag in dq_opts else "GRAD"
            scale = np.power(10.0,-qexp)
            nplot = qq + 1
            ax = fig.add_subplot(ny,nx,nplot)
            ci = 0
            if qlist[qq] in qqextra:
                nqtag = qqextra[qlist[qq]]
                if ctag in dq_opts:
                    mm = re.match(r'^(.*)'+qlist[qq]+r'$',dq_opts[ctag][qlist[qq]])
                    if mm:
                        nqtag = "OUT"+mm.group(1).upper()+qqextra[qlist[qq]]
                for idx in np.arange(0,numimp):
                    itag = "%d" % (idx+1)
                    if nqtag+"IMP"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"IMP"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["PD"][nqtag+"IMP"+itag][gtag+""].flatten()
                        yefit = self.sdata["PD"][nqtag+"IMP"+itag][gtag+"EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
                for idx in np.arange(0,numz):
                    itag = "%d" % (idx+1)
                    if nqtag+"Z"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"Z"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["PD"][nqtag+"Z"+itag][gtag+""].flatten()
                        yefit = self.sdata["PD"][nqtag+"Z"+itag][gtag+"EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
            elif qlist[qq] == "TI":
                for idx in np.arange(0,numion):
                    itag = "%d" % (idx+1)
                    if qtag+itag in self.sdata["PD"] and self.sdata["PD"][qtag+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["PD"][qtag+itag][gtag+""].flatten()
                        yefit = self.sdata["PD"][qtag+itag][gtag+"EB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
            else:
                if qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                        xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                    yfit = self.sdata["PD"][qtag][gtag+""].flatten()
                    yefit = self.sdata["PD"][qtag][gtag+"EB"].flatten()
                    ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                    ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0],xlims[1])
            if ctag in dq_opts:
                ax.set_ylim(qlims[qlist[qq]][0],qlims[qlist[qq]][1])
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            if ctag in dq_opts:
                pylabel = ylabel + r' ' + yunitd if yunitd else ylabel
            ax.set_ylabel(pylabel)
            ax.set_xlabel(pxlabel)

        self.DPlotWidget = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtGui.QVBoxLayout()
        gbox.addWidget(self.DPlotWidget)

        self.setLayout(gbox)

        self.setGeometry(10, 10, 1100, 1100)
        self.setWindowTitle('Profile Gradient Plots')
        self.show()


class source_plotter(QtGui.QWidget):

    def __init__(self,data,sigma=None,csidx=None,xerrflag=False):
        super(source_plotter, self).__init__()
        self.sdata = None
        self.sigma = 1.0
        self.csidx = 0
        self.xerrflag = False
        if isinstance(data,dict):
            self.sdata = data
        if isinstance(sigma,(int,float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if isinstance(csidx,(int, float)):
            self.csidx = int(csidx)
        if xerrflag:
            self.xerrflag = True
        if self.sdata is not None:
            self.initUI()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.exec_()

    def initUI(self):

        ocp = self.sdata["META"]["CSOP"]
        cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
        ocs = cslist[0]
        xlims = (0.0,1.0)
        fxlim = True
        if self.csidx < len(cslist) and ocp+cslist[self.csidx] in self.sdata["CD"].coord_keys():
            ocs = cslist[self.csidx]
            xlims = (np.amin(self.sdata["CD"][ocp][ocs]["V"]),np.amax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX","POLFLUX","RMAJORO","RMAJORI"]:
                fxlim = False
        (xt,xlabel,xunit,fl,fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + " [" + xunit + "]" if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["SNINBI","SPNBI","STENBI","STINBI","STEICRH","STIICRH"]
        qscales = {"SNINBI" : 19, "SPNBI" : 0, "STENBI": 6, "STINBI" : 6, "STEICRH": 6, "STIICRH" : 6}
        styles = ['+','x','o','s','d','^','v','<','>','*']
        colors = ['r','b','g','m','c','y']
        fig = matplotlib.figure.Figure()
        ny = 3
        nx = 2

        for qq in np.arange(0,len(qlist)):
            (qtag,ylabel,yunit,fqlbl,fqunit) = EX2GK.ptools.define_quantity(qlist[qq],exponent=qscales[qlist[qq]])
            scale = np.power(10.0,-qscales[qlist[qq]])
            nplot = qq + 1
            ax = fig.add_subplot(ny,nx,nplot)
            si = 0
            ci = 0
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1:
                ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ocp
                xraw = self.sdata["RD"][qtag]["X"].flatten()
                xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                if ics != ocs:
                    (xraw,dj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,ocp)
                    xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                yraw = self.sdata["RD"][qtag][""].flatten()
                yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                for ii in np.arange(0,len(self.sdata["RD"][qtag]["SRC"])):
                    filt = (self.sdata["RD"][qtag]["MAP"] == ii)
                    if np.any(filt):
                        xerr = self.sigma * xeraw[filt]
                        yerr = self.sigma * yeraw[filt]
                        if self.xerrflag:
                            ax.errorbar(xraw[filt],yraw[filt]*scale,xerr=xerr,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                        else:
                            ax.errorbar(xraw[filt],yraw[filt]*scale,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                        si = si + 1
                if qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                        xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                    ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0],xlims[1])
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel)
            ax.set_xlabel(pxlabel)

        self.SPlotWidget = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtGui.QVBoxLayout()
        gbox.addWidget(self.SPlotWidget)

        self.setLayout(gbox)

        self.setGeometry(10, 10, 1100, 1100)
        self.setWindowTitle('Source Plots')
        self.show()


class misc_plotter(QtGui.QWidget):

    def __init__(self,data,sigma=None,csidx=None,xerrflag=False):
        super(misc_plotter, self).__init__()
        self.sdata = None
        self.sigma = 1.0
        self.csidx = 0
        self.xerrflag = False
        if isinstance(data,dict):
            self.sdata = data
        if isinstance(sigma,(int,float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if isinstance(csidx,(int, float)):
            self.csidx = int(csidx)
        if xerrflag:
            self.xerrflag = True
        if self.sdata is not None:
            self.initUI()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.exec_()

    def initUI(self):

        ocp = self.sdata["META"]["CSOP"]
        prefix_list = self.sdata["CD"].getPrefixes()
        cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
        ocs = cslist[0]
        xlims = (0.0,1.0)
        fxlim = True
        if self.csidx < len(cslist) and ocp+cslist[self.csidx] in self.sdata["CD"].coord_keys():
            ocs = cslist[self.csidx]
            xlims = (np.amin(self.sdata["CD"][ocp][ocs]["V"]),np.amax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX","POLFLUX","RMAJORO","RMAJORI"]:
                fxlim = False
        (xt,xlabel,xunit,fl,fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + " [" + xunit + "]" if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["Q","ZEFF"]
        qscales = {"Q" : 0, "ZEFF" : 0}
        styles = ['+','x','o','s','d','^','v','<','>','*']
        colors = ['r','b','g','m','c','y']
        fig = matplotlib.figure.Figure()
        ny = 2
        nx = 2

        for qq in np.arange(0,len(qlist)):
            (qtag,ylabel,yunit,fqlbl,fqunit) = EX2GK.ptools.define_quantity(qlist[qq],exponent=qscales[qlist[qq]])
            scale = np.power(10.0,-qscales[qlist[qq]])
            nplot = 2 * qq + 1
            ax = fig.add_subplot(ny,nx,nplot)
            si = 0
            ci = 0
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1:
                ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ocp
                xraw = self.sdata["RD"][qtag]["X"].flatten()
                xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                if ics != ocs:
                    (xraw,dj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,ocp)
                    xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                yraw = self.sdata["RD"][qtag][""].flatten()
                yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                for ii in np.arange(0,len(self.sdata["RD"][qtag]["SRC"])):
                    filt = (self.sdata["RD"][qtag]["MAP"] == ii)
                    if np.any(filt):
                        xerr = self.sigma * xeraw[filt]
                        yerr = self.sigma * yeraw[filt]
                        if self.xerrflag:
                            ax.errorbar(xraw[filt],yraw[filt]*scale,xerr=xerr,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                        else:
                            ax.errorbar(xraw[filt],yraw[filt]*scale,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                        si = si + 1
                if qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                        xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                    ci = ci + 1
            if qtag == "Q" and len(prefix_list) > 1:
                for jj in np.arange(0,len(prefix_list)):
                    ctag = prefix_list[jj]
                    if ctag and ctag+qtag in self.sdata["RD"] and self.sdata["RD"][ctag+qtag].npoints > 1:
                        ics = self.sdata["RD"][ctag+qtag].coordinate if self.sdata["RD"][ctag+qtag].coordinate is not None else ocs
                        icp = self.sdata["RD"][ctag+qtag].coord_prefix if self.sdata["RD"][ctag+qtag].coordinate is not None else ocp
                        xraw = self.sdata["RD"][ctag+qtag]["X"].flatten()
                        xeraw = self.sdata["RD"][ctag+qtag]["XEB"].flatten()
                        if ics != ocs:
                            (xraw,dj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,ocp)
                            xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                        yraw = self.sdata["RD"][ctag+qtag][""].flatten()
                        yeraw = self.sdata["RD"][ctag+qtag]["EB"].flatten()
                        for ii in np.arange(0,len(self.sdata["RD"][ctag+qtag]["SRC"])):
                            filt = (self.sdata["RD"][ctag+qtag]["MAP"] == ii)
                            if np.any(filt):
                                xerr = self.sigma * xeraw[filt]
                                yerr = self.sigma * yeraw[filt]
                                if self.xerrflag:
                                    ax.errorbar(xraw[filt],yraw[filt]*scale,xerr=xerr,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                                else:
                                    ax.errorbar(xraw[filt],yraw[filt]*scale,yerr=yerr*scale,color='k',ls='',marker=styles[si])
                                si = si + 1
                        if qtag in self.sdata["PD"] and self.sdata["PD"][ctag+qtag].npoints > 0:
                            xfit = self.sdata["PD"]["X"][""].flatten()
                            xefit = self.sdata["PD"]["X"]["EB"].flatten()
                            if self.sdata["META"]["CSO"] != ocs:
                                (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                                xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                            yfit = self.sdata["PD"][ctag+qtag][""].flatten()
                            yefit = self.sdata["PD"][ctag+qtag]["EB"].flatten()
                            ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                            yl = yfit - self.sigma * yefit
                            yu = yfit + self.sigma * yefit
                            ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                            ci = ci + 1
            elif qtag == "ZEFF" and qtag+"P" in self.sdata["PD"] and self.sdata["PD"][qtag+"P"].npoints > 0:
                xfit = self.sdata["PD"]["X"][""].flatten()
                xefit = self.sdata["PD"]["X"]["EB"].flatten()
                if self.sdata["META"]["CSO"] != ocs:
                    (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                    xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                yfit = self.sdata["PD"][qtag+"P"][""].flatten()
                yefit = self.sdata["PD"][qtag+"P"]["EB"].flatten()
                ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                yl = yfit - self.sigma * yefit
                yu = yfit + self.sigma * yefit
                ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0],xlims[1])
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel)
            ax.set_xlabel(pxlabel)

            nplot = 2 * qq + 2
            ax = fig.add_subplot(ny,nx,nplot)
            si = 0
            ci = 0
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1 and qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0 and "GRAD" in self.sdata["PD"][qtag]:
                xfit = self.sdata["PD"]["X"][""].flatten()
                xefit = self.sdata["PD"]["X"]["EB"].flatten()
                if self.sdata["META"]["CSO"] != ocs:
                    (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                    xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                yfit = self.sdata["PD"][qtag]["GRAD"].flatten()
                yefit = self.sdata["PD"][qtag]["GRADEB"].flatten()
                ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                yl = yfit - self.sigma * yefit
                yu = yfit + self.sigma * yefit
                ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                ci = ci + 1
            if qtag == "Q" and len(prefix_list) > 1:
                for jj in np.arange(0,len(prefix_list)):
                    ctag = prefix_list[jj]
                    if ctag+qtag in self.sdata["RD"] and self.sdata["RD"][ctag+qtag].npoints > 1 and ctag+qtag in self.sdata["PD"] and self.sdata["PD"][ctag+qtag].npoints > 0 and "GRAD" in self.sdata["PD"][ctag+qtag]:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                            xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                        yfit = self.sdata["PD"][ctag+qtag]["GRAD"].flatten()
                        yefit = self.sdata["PD"][ctag+qtag]["GRADEB"].flatten()
                        ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                        ci = ci + 1
            elif qtag == "ZEFF" and qtag+"P" in self.sdata["PD"] and self.sdata["PD"][qtag+"P"].npoints > 0 and "GRAD" in self.sdata["PD"][qtag+"P"]:
                xfit = self.sdata["PD"]["X"][""].flatten()
                xefit = self.sdata["PD"]["X"]["EB"].flatten()
                if self.sdata["META"]["CSO"] != ocs:
                    (xfit,dj,txefit) = self.sdata["CD"].convert(xfit,self.sdata["META"]["CSO"],ocs,self.sdata["META"]["CSOP"],ocp)
                    xefit = np.sqrt(np.power(xefit,2.0) + np.power(txefit,2.0))
                yfit = self.sdata["PD"][qtag+"P"]["GRAD"].flatten()
                yefit = self.sdata["PD"][qtag+"P"]["GRADEB"].flatten()
                ax.plot(xfit,yfit*scale,color=colors[ci],ls='-')
                yl = yfit - self.sigma * yefit
                yu = yfit + self.sigma * yefit
                ax.fill_between(xfit,yl*scale,yu*scale,facecolor=colors[ci],edgecolor='None',alpha=0.2)
                ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0],xlims[1])
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel)
            ax.set_xlabel(pxlabel)

        self.MPlotWidget = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtGui.QVBoxLayout()
        gbox.addWidget(self.MPlotWidget)

        self.setLayout(gbox)

        self.setGeometry(10, 10, 1100, 1100)
        self.setWindowTitle('Miscellaneous Plots')
        self.show()


# Not yet available for use
class equilibrium_plotter(QtGui.QWidget):

    def __init__(self,data):
        super(equilibrium_plotter, self).__init__()
        self.sdata = None
        if isinstance(data,dict):
            self.sdata = data
        if self.sdata is not None:
            self.initUI()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.exec_()

    def initUI(self):

        fig = matplotlib.figure.Figure()
        if "ED" in self.sdata and "PSI" in self.sdata["ED"] and self.sdata["ED"]["PSI"] is not None:
            ax = fig.add_subplot(111)
            cs = ax.contour(self.sdata["ED"]["PSI"]["X"],self.sdata["ED"]["PSI"]["Y"],self.sdata["ED"]["PSI"][""],20)
            ax.clabel(cs,inline=True,fontsize=10)
            ax.set_xlabel('Major Radius [m]')
            ax.set_ylabel('Height [m]')
            if "BND" in self.sdata["ED"] and self.sdata["ED"]["BND"] is not None:
                ax.plot(self.sdata["ED"]["BND"][""],self.sdata["ED"]["BND"]["X"],color='k')
            if "LIM" in self.sdata["ED"] and self.sdata["ED"]["LIM"] is not None:
                ax.plot(self.sdata["ED"]["LIM"][""],self.sdata["ED"]["LIM"]["X"],color='r')

        self.EPlotWidget = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtGui.QVBoxLayout()
        gbox.addWidget(self.EPlotWidget)

        self.setLayout(gbox)

        self.setGeometry(10, 10, 500, 800)
        self.setWindowTitle('Equilibrium Plot')
        self.show()


class EX2GK_AUG(QtGui.QWidget):

    def __init__(self):
        super(EX2GK_AUG, self).__init__()
        location = inspect.getsourcefile(EX2GK.extract_and_standardize_data)
        self.exsrcdir = os.path.dirname(location) + '/'
        print("Using EX2GK definition from: %s" % (self.exsrcdir))
        location = inspect.getsourcefile(customfilters.apply_custom_filters) if customfilters is not None else None
        self.filtsrcdir = os.path.dirname(location) + '/' if location is not None else "N/A"
        print("Using EX2GK custom filter definition from: %s" % (self.filtsrcdir))
        location = inspect.getsourcefile(EX2GK.ptools.gp.GaussianProcessRegression1D)
        self.gpsrcdir = os.path.dirname(location) + '/'
        print("Using GPR1D definition from: %s" % (self.gpsrcdir))
        self.device = "AUG"
        self.sdata = None
        self.csidx = 0
#        self.dlist = dict()
        self.didx = None
        self.clist = []
        self._csconv = {"RHOTORN": 0, "RHOPOLN": 1, "PSIPOLN": 2, "PSITORN": 3, "PSIPOL": 4, "PSITOR": 5, "RMAJORO": 6, "RMAJORI": 7, "RMINORO": 8, "RMINORI": 9}
        self.tdata = None
        self._preset_fields = data_fields
        self.initUI()

    def initUI(self):

        self.LoadDataButton = QtGui.QPushButton("Load Data")
        self.LoadDataButton.clicked.connect(self._load_data)
        self.ViewTraceButton = QtGui.QPushButton("View Time Traces")
        self.ViewTraceButton.clicked.connect(self._plot_time_traces)
        self.ShotNumberEntry = QtGui.QLineEdit()
        self.ShotNumberEntry.setValidator(QtGui.QIntValidator(None))
        self.ShotNumberEntry.setMaxLength(6)
        self.ShotNumberEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeStartEntry = QtGui.QLineEdit()
        self.TimeStartEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeStartEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeEndEntry = QtGui.QLineEdit()
        self.TimeEndEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeEndEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.NTimeWindowsEntry = QtGui.QLineEdit("1")
        self.NTimeWindowsEntry.setValidator(QtGui.QIntValidator(None))
        self.NTimeWindowsEntry.setMaxLength(3)
        self.NTimeWindowsEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.InterfaceList = QtGui.QComboBox()
        self.InterfaceList.addItem("DD3")
#        self.InterfaceList.addItem("DD")
        self.EquilibriumList = QtGui.QComboBox()
        self.EquilibriumList.addItem("EQH")
        self.EquilibriumList.addItem("EQI")
        self.EquilibriumList.addItem("FPP")
        self.EquilibriumList.setCurrentIndex(2)
        self.EquilibriumList.currentIndexChanged.connect(self._recompile_data_list)

        twbox = QtGui.QFormLayout()
        twbox.addRow("Shot Number",self.ShotNumberEntry)
        twbox.addRow("Time Start (s)",self.TimeStartEntry)
        twbox.addRow("Time End (s)",self.TimeEndEntry)
        twbox.addRow("No. of Windows",self.NTimeWindowsEntry)
        twbox.addRow("Data Interface",self.InterfaceList)
        twbox.addRow("Equilibrium",self.EquilibriumList)

        self.ListInstructions = QtGui.QLabel("Double-click a window entry to load it into the right panel. Otherwise, the currently loaded entry is highlighted.")
        self.ListInstructions.setWordWrap(True)
        self.DatabaseList = QtGui.QTableWidget()
        self.DatabaseList.setEnabled(False)
        self.DatabaseList.setMinimumSize(10,100)
        self.DatabaseList.setColumnCount(1)
        self.DatabaseList.setHorizontalHeaderLabels(["Time Window"])
        self.DatabaseList.verticalHeader().setVisible(False)
        self.DatabaseList.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.DatabaseList.cellDoubleClicked.connect(self._select_data)
        self.SortWindowsButton = QtGui.QPushButton("Sort Windows")
        self.SortWindowsButton.setEnabled(False)
        self.SortWindowsButton.clicked.connect(self._generate_table)
        self.DeleteWindowButton = QtGui.QPushButton("Delete Window")
        self.DeleteWindowButton.setEnabled(False)
        self.DeleteWindowButton.clicked.connect(self._delete_current_row)

        wmbox = QtGui.QHBoxLayout()
        wmbox.addWidget(self.SortWindowsButton)
        wmbox.addWidget(self.DeleteWindowButton)

        dlbox = QtGui.QVBoxLayout()
        dlbox.addWidget(self.ListInstructions)
        dlbox.addWidget(self.DatabaseList)
        dlbox.addLayout(wmbox)

        svbox = QtGui.QVBoxLayout()
        svbox.addWidget(self.LoadDataButton)
        svbox.addWidget(self.ViewTraceButton)
        svbox.addLayout(twbox)
        svbox.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        svbox.addLayout(dlbox)

        self.TabPanel = QtGui.QTabWidget()

        self.BasicOptionsTab = QtGui.QWidget()
        self.BasicOptionsTab.setLayout(self.BasicUI())
        self.DataSpecificationTab = QtGui.QWidget()
        self.DataSpecificationTab.setLayout(self.DataSpecificationUI())
        self.ProfileModificationTab = QtGui.QWidget()
        self.ProfileModificationTab.setLayout(self.ModificationUI())
        self.AdvancedOptionsTab = QtGui.QWidget()
        self.AdvancedOptionsTab.setLayout(self.AdvancedUI())

        self.TabPanel.addTab(self.BasicOptionsTab,"Standard")
#        self.TabPanel.addTab(self.DataSpecificationTab,"Data List")
#        self.TabPanel.addTab(self.ProfileModificationTab,"Modification")
        self.TabPanel.addTab(self.AdvancedOptionsTab,"Unsupported")

        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(svbox,40)
        hbox.addItem(QtGui.QSpacerItem(10,0,QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding))
        hbox.addWidget(self.TabPanel,70)

        self.setLayout(hbox)

        self.setGeometry(20, 20, 1000, 600)
        self.setWindowTitle('EX2GK GUI (AUG)')
        self.show()

    def BasicUI(self):

        self.ExtractingLabel = QtGui.QLabel("Extracting")
        self.GrabAllBox = QtGui.QCheckBox("ALL")
        self.GrabAllBox.clicked.connect(self._enable_all_quantities)
        self.GrabNEBox = QtGui.QCheckBox("NE")
        self.GrabNEBox.clicked.connect(self._reset_all_box)
        self.GrabNEBox.toggled.connect(self._recompile_data_list)
        self.GrabTEBox = QtGui.QCheckBox("TE")
        self.GrabTEBox.clicked.connect(self._reset_all_box)
        self.GrabTEBox.toggled.connect(self._recompile_data_list)
        self.GrabTIBox = QtGui.QCheckBox("TI, TIMP")
        self.GrabTIBox.clicked.connect(self._reset_all_box)
        self.GrabTIBox.toggled.connect(self._recompile_data_list)
        self.GrabNIBox = QtGui.QCheckBox("NI, NIMP")
        self.GrabNIBox.clicked.connect(self._reset_all_box)
        self.GrabNIBox.toggled.connect(self._recompile_data_list)
        self.GrabAFBox = QtGui.QCheckBox("ANGF")
        self.GrabAFBox.clicked.connect(self._reset_all_box)
        self.GrabAFBox.toggled.connect(self._recompile_data_list)
        self.GrabFIBox = QtGui.QCheckBox("NFI, WFI")
        self.GrabFIBox.clicked.connect(self._reset_all_box)
        self.GrabFIBox.toggled.connect(self._recompile_data_list)
        self.GrabSourceBox = QtGui.QCheckBox("Sources")
        self.GrabSourceBox.clicked.connect(self._reset_all_box)
        self.GrabSourceBox.toggled.connect(self._recompile_data_list)

        qgbox = QtGui.QGridLayout()
        qgbox.addWidget(self.GrabAllBox,1,0)
        qgbox.addWidget(self.GrabNEBox,1,1)
        qgbox.addWidget(self.GrabTEBox,1,2)
        qgbox.addWidget(self.GrabTIBox,1,3)
        qgbox.addWidget(self.GrabNIBox,2,0)
        qgbox.addWidget(self.GrabAFBox,2,1)
        qgbox.addWidget(self.GrabFIBox,2,2)
        qgbox.addWidget(self.GrabSourceBox,2,3)

        self.ExtractDataButton = QtGui.QPushButton("Extract Data")
        self.ExtractDataButton.clicked.connect(self._extract_data)
        self.SaveRawDataButton = QtGui.QPushButton("Print Raw Data")
        self.SaveRawDataButton.setEnabled(False)
        self.SaveRawDataButton.clicked.connect(self._save_raw_data)

        edbox = QtGui.QHBoxLayout()
        edbox.addWidget(self.ExtractDataButton)
        edbox.addWidget(self.SaveRawDataButton)

        qqbox = QtGui.QVBoxLayout()
        qqbox.addWidget(self.ExtractingLabel)
        qqbox.addLayout(qgbox)
        qqbox.addLayout(edbox)

        self.FittingLabel = QtGui.QLabel("Fitting")
        self.FittingLabel.setEnabled(False)
        self.CoordinateList = QtGui.QComboBox()
        self.CoordinateList.setEnabled(False)

        csbox = QtGui.QFormLayout()
        csbox.addRow("Output Coordinate",self.CoordinateList)

        self.UseLinearFastIonFitBox = QtGui.QCheckBox("Linear interp. on fast ions")
        self.UseLinearFastIonFitBox.setEnabled(False)
        self.UseLinearSourceFitBox = QtGui.QCheckBox("Linear interp. on sources")
        self.UseLinearSourceFitBox.setEnabled(False)
        self.UseLinearSourceFitBox.setChecked(True)

        libox = QtGui.QHBoxLayout()
        libox.addWidget(self.UseLinearFastIonFitBox)
        libox.addWidget(self.UseLinearSourceFitBox)

        self.FitDataButton = QtGui.QPushButton("Fit Window")
        self.FitDataButton.setEnabled(False)
        self.FitDataButton.clicked.connect(self._fit_data)
        self.FitAllDataButton = QtGui.QPushButton("Fit All Windows")
        self.FitAllDataButton.setEnabled(False)
        self.FitAllDataButton.clicked.connect(self._fit_all_data)
        self.SaveFitDataButton = QtGui.QPushButton("Print Fit Data")
        self.SaveFitDataButton.setEnabled(False)
        self.SaveFitDataButton.clicked.connect(self._save_fit_data)

        fdbox = QtGui.QHBoxLayout()
        fdbox.addWidget(self.FitDataButton)
        fdbox.addWidget(self.FitAllDataButton)
        fdbox.addWidget(self.SaveFitDataButton)

        dobox = QtGui.QVBoxLayout()
        dobox.addWidget(self.FittingLabel)
        dobox.addLayout(libox)
        dobox.addLayout(csbox)
        dobox.addLayout(fdbox)

        self.PlottingLabel = QtGui.QLabel("Plotting")
        self.PlottingLabel.setEnabled(False)
        self.PlotProfileButton = QtGui.QPushButton("Profiles")
        self.PlotProfileButton.setEnabled(False)
        self.PlotProfileButton.clicked.connect(self._plot_profile_data)
        self.PlotGradientButton = QtGui.QPushButton("Gradients")
        self.PlotGradientButton.setEnabled(False)
        self.PlotGradientButton.clicked.connect(self._plot_gradient_data)
        self.PlotSourceButton = QtGui.QPushButton("Sources")
        self.PlotSourceButton.setEnabled(False)
        self.PlotSourceButton.clicked.connect(self._plot_source_data)
        self.PlotMiscButton = QtGui.QPushButton("Misc.")
        self.PlotMiscButton.setEnabled(False)
        self.PlotMiscButton.clicked.connect(self._plot_misc_data)

        pbbox = QtGui.QGridLayout()
        pbbox.addWidget(self.PlottingLabel,0,0)
        pbbox.addWidget(self.PlotProfileButton,1,0)
        pbbox.addWidget(self.PlotGradientButton,1,1)
        pbbox.addWidget(self.PlotSourceButton,1,2)
        pbbox.addWidget(self.PlotMiscButton,1,3)

        self.CheckingLabel = QtGui.QLabel("Checking")
        self.CheckingLabel.setEnabled(False)
        self.PassSigmaEntry = QtGui.QLineEdit("1.96")
        self.PassSigmaEntry.setEnabled(False)
        self.PassSigmaEntry.setValidator(QtGui.QDoubleValidator(None))
        self.PassSigmaEntry.setAlignment(QtCore.Qt.AlignLeft)

        pwbox = QtGui.QFormLayout()
        pwbox.addRow("Pass Width Criterion",self.PassSigmaEntry)

        self.QualityCheckButton = QtGui.QPushButton("Perform Basic Checks")
        self.QualityCheckButton.setEnabled(False)
        self.QualityCheckButton.clicked.connect(self._run_quality_checks)
        self.ExplorationModeButton = QtGui.QPushButton("Data Exploration Mode")
        self.ExplorationModeButton.setEnabled(False)
        self.ExplorationModeButton.clicked.connect(self._enter_exploration_mode)

        cdbox = QtGui.QHBoxLayout()
        cdbox.addWidget(self.QualityCheckButton)
        cdbox.addWidget(self.ExplorationModeButton)

        qcbox = QtGui.QVBoxLayout()
        qcbox.addWidget(self.CheckingLabel)
        qcbox.addLayout(pwbox)
        qcbox.addLayout(cdbox)

        self.ProcessingLabel = QtGui.QLabel("Processing")
        self.ProcessingLabel.setEnabled(False)
        self.CodeAdapterList = QtGui.QComboBox()
        self.CodeAdapterList.setEnabled(False)
        self.CodeAdapterList.addItem("General")
        self.CodeAdapterList.addItem("QuaLiKiz")
        self.CodeAdapterList.setCurrentIndex(0)

        cabox = QtGui.QFormLayout()
        cabox.addRow("Target Code",self.CodeAdapterList)

        self.ProcessDataButton = QtGui.QPushButton("Process Window")
        self.ProcessDataButton.setEnabled(False)
        self.ProcessDataButton.clicked.connect(self._post_process_data)
        self.SaveProcessDataButton = QtGui.QPushButton("Print Processed Data")
        self.SaveProcessDataButton.setEnabled(False)
        self.SaveProcessDataButton.clicked.connect(self._save_post_processed_data)
        self.PlotProcessButton = QtGui.QPushButton("Plot Processed Data")
        self.PlotProcessButton.setEnabled(False)
        self.PlotProcessButton.clicked.connect(self._plot_processed_gradient_data)

        pdbox = QtGui.QHBoxLayout()
        pdbox.addWidget(self.ProcessDataButton)
        pdbox.addWidget(self.SaveProcessDataButton)
        pdbox.addWidget(self.PlotProcessButton)

        ppbox = QtGui.QVBoxLayout()
        ppbox.addWidget(self.ProcessingLabel)
        ppbox.addLayout(cabox)
        ppbox.addLayout(pdbox)

        self.SavingLabel = QtGui.QLabel("Saving")
        self.SavingLabel.setEnabled(False)
        self.SaveDatabaseButton = QtGui.QPushButton("Save List as Pickle")
        self.SaveDatabaseButton.setEnabled(False)
        self.SaveDatabaseButton.clicked.connect(self._save_database)

        sobox = QtGui.QVBoxLayout()
        sobox.addWidget(self.SavingLabel)
        sobox.addWidget(self.SaveDatabaseButton)

        layout = QtGui.QVBoxLayout()
        layout.addLayout(qqbox)
        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(dobox)
        layout.addItem(QtGui.QSpacerItem(0,10,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(pbbox)
        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(qcbox)
        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(ppbox)
        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(sobox)
        layout.addStretch(1)

        return layout

    def DataSpecificationUI(self):

        self.DataSpecificationList = QtGui.QTableWidget()
        self.DataSpecificationList.setEnabled(False)
        self.DataSpecificationList.setMinimumSize(10,100)
        self.DataSpecificationList.setColumnCount(3)
        self.DataSpecificationList.setHorizontalHeaderLabels(["DDA","Seq. No.","User ID"])
        self.DataSpecificationList.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.DataSpecificationList.verticalHeader().setVisible(False)

        self._recompile_data_list()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.DataSpecificationList)

        return layout

    def ModificationUI(self):

        self.WarpFunctionList = QtGui.QComboBox()
        self.WarpFunctionList.setEnabled(False)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.WarpFunctionList)

        return layout

    def AdvancedUI(self):

        self.AcceptRiskBox = QtGui.QCheckBox("I solemnly swear that I am up to no good.")
        self.AcceptRiskBox.toggled.connect(self._enable_advanced_menu)

        self.BasicDebugBox = QtGui.QCheckBox("Print basic debug statements")
        self.BasicDebugBox.setEnabled(False)
        self.DebugEquilibriumBox = QtGui.QCheckBox("Print coordinate system data")
        self.DebugEquilibriumBox.setEnabled(False)

        self.NonFlatTopBox = QtGui.QCheckBox("Disable current flat-top options")
        self.NonFlatTopBox.setEnabled(False)
        self.UseRMajorShiftBox = QtGui.QCheckBox("Use major radius shift")
        self.UseRMajorShiftBox.setEnabled(False)
        self.UseRMajorShiftBox.setChecked(False)

        opbox = QtGui.QVBoxLayout()
        opbox.addWidget(self.NonFlatTopBox)
        opbox.addWidget(self.UseRMajorShiftBox)

#        self.NEErrorMultiplierEntry = QtGui.QLineEdit()
#        self.NEErrorMultiplierEntry.setEnabled(False)
#        self.NEErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.NEErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.TEErrorMultiplierEntry = QtGui.QLineEdit()
#        self.TEErrorMultiplierEntry.setEnabled(False)
#        self.TEErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.TEErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.NIErrorMultiplierEntry = QtGui.QLineEdit()
#        self.NIErrorMultiplierEntry.setEnabled(False)
#        self.NIErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.NIErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.TIErrorMultiplierEntry = QtGui.QLineEdit()
#        self.TIErrorMultiplierEntry.setEnabled(False)
#        self.TIErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.TIErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.NIMPErrorMultiplierEntry = QtGui.QLineEdit()
#        self.NIMPErrorMultiplierEntry.setEnabled(False)
#        self.NIMPErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.NIMPErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TIMPErrorMultiplierEntry = QtGui.QLineEdit()
        self.TIMPErrorMultiplierEntry.setEnabled(False)
        self.TIMPErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TIMPErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.AFErrorMultiplierEntry = QtGui.QLineEdit()
#        self.AFErrorMultiplierEntry.setEnabled(False)
#        self.AFErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.AFErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.QErrorMultiplierEntry = QtGui.QLineEdit()
#        self.QErrorMultiplierEntry.setEnabled(False)
#        self.QErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.QErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)

        embox = QtGui.QFormLayout()
#        embox.addRow("NE error mult.:",self.NEErrorMultiplierEntry)
#        embox.addRow("TE error mult.:",self.TEErrorMultiplierEntry)
#        embox.addRow("NI error mult.:",self.NIErrorMultiplierEntry)
#        embox.addRow("TI error mult.:",self.TIErrorMultiplierEntry)
#        embox.addRow("NIMP error mult.:",self.NIMPErrorMultiplierEntry)
        embox.addRow("TIMP error mult.:",self.TIMPErrorMultiplierEntry)
#        embox.addRow("AF error mult.:",self.AFErrorMultiplierEntry)
#        embox.addRow("Q error mult.:",self.QErrorMultiplierEntry)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.AcceptRiskBox)
        layout.addItem(QtGui.QSpacerItem(0,20,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addWidget(self.BasicDebugBox)
        layout.addWidget(self.DebugEquilibriumBox)
#        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
#        layout.addLayout(cqbox)
#        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
#        layout.addWidget(self.GrabMainTIBox)
#        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
#        layout.addLayout(csbox)
        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(opbox)
        layout.addItem(QtGui.QSpacerItem(0,15,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        layout.addLayout(embox)
        layout.addStretch(1)

        return layout

    def _add_data_specification_entry(self,key):
        if isinstance(key,str):
            tidx = self.DataSpecificationList.rowCount()
            DDAItem = QtGui.QTableWidgetItem(key+'/*')
            DDAItem.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            SEQItem = QtGui.QTableWidgetItem('0')
            UIDItem = QtGui.QTableWidgetItem('augd')
            self.DataSpecificationList.insertRow(tidx)
            self.DataSpecificationList.setItem(tidx,0,DDAItem)
            self.DataSpecificationList.setItem(tidx,1,SEQItem)
            self.DataSpecificationList.setItem(tidx,2,UIDItem)

    def _delete_data_specification_entry(self,key):
        if isinstance(key,str):
            idxv = []
            tidx = self.DataSpecificationList.rowCount()
            for idx in np.arange(0,tidx):
                ientry = self.DataSpecificationList.item(idx,0).text()
                ikey = ientry.split('/')[0]
                if re.match(ikey,key,flags=re.IGNORECASE):
                    idxv.append(idx)
            for ii in np.arange(len(idxv),0,-1):
                self.DataSpecificationList.removeRow(idxv[ii-1])

    def _get_data_specification_entry(self,key):
        seq = '0'
        uid = 'augd'
        if isinstance(key,str):
            idxv = []
            tidx = self.DataSpecificationList.rowCount()
            for idx in np.arange(0,tidx):
                ientry = self.DataSpecificationList.item(idx,0).text()
                ikey = ientry.split('/')[0]
                if re.match(ikey,key,flags=re.IGNORECASE):
                    seq = self.DataSpecificationList.item(idx,1).text().strip()
                    if not re.match(r'^[0-9]+$',seq):
                        seq = '0'
                    uid = self.DataSpecificationList.item(idx,2).text().strip().lower()
                    if not re.match(r'^[a-z0-9]+$',uid):
                        uid = 'augd'
        if not seq:
            seq = '0'
        if not uid:
            uid = 'augd'
        return (seq,uid)

    def _recompile_data_list(self):
        clist = []
        tidx = self.DataSpecificationList.rowCount()
        for idx in np.arange(0,tidx):
            ientry = self.DataSpecificationList.item(idx,0).text()
            clist.append(ientry.split('/')[0].lower())
        nlist = ['ks3',self.EquilibriumList.currentText().lower()]
        fchecked = False
        if self.GrabNEBox.isChecked() and "NE" in self._preset_fields:
            for key in self._preset_fields["NE"]:
                nlist.append(key.lower())
        if self.GrabTEBox.isChecked() and "TE" in self._preset_fields:
            for key in self._preset_fields["TE"]:
                nlist.append(key.lower())
        if self.GrabTIBox.isChecked() and "TIMP" in self._preset_fields:
            for key in self._preset_fields["TIMP"]:
                nlist.append(key.lower())
        if self.GrabNIBox.isChecked() and "NIMP" in self._preset_fields:
            for key in self._preset_fields["NIMP"]:
                nlist.append(key.lower())
        if self.GrabAFBox.isChecked() and "AF" in self._preset_fields:
            for key in self._preset_fields["AF"]:
                nlist.append(key.lower())
        if self.GrabFIBox.isChecked() and "FI" in self._preset_fields:
            for key in self._preset_fields["FI"]:
                nlist.append(key.lower())
        if self.GrabSourceBox.isChecked() and "QSTJ" in self._preset_fields:
            for key in self._preset_fields["QSTJ"]:
                nlist.append(key.lower())
        for ckey in clist:
            if ckey not in nlist:
                self._delete_data_specification_entry(ckey)
        for nkey in nlist:
            if nkey not in clist:
                self._add_data_specification_entry(nkey)

    def _enable_advanced_menu(self):
        self.BasicDebugBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.DebugEquilibriumBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.NonFlatTopBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseRMajorShiftBox.setEnabled(False)
#        self.NEErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.TEErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.NIErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.TIErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.NIMPErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
        self.TIMPErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.AFErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.QErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
        self.DataSpecificationList.setEnabled(self.AcceptRiskBox.isChecked())

    def _enable_all_quantities(self):
        self.GrabNEBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabTEBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabTIBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabNIBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabAFBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabFIBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabSourceBox.setChecked(self.GrabAllBox.isChecked())

    def _reset_all_box(self):
        self.GrabAllBox.setChecked(self.GrabNEBox.isChecked() and self.GrabTEBox.isChecked() and \
                                   self.GrabTIBox.isChecked() and self.GrabNIBox.isChecked() and \
                                   self.GrabAFBox.isChecked() and self.GrabFIBox.isChecked() and \
                                   self.GrabSourceBox.isChecked())

    def _enable_fast_ions(self):
        self.UseLinearFastIonFitBox.setEnabled(self.sdata is not None and self.GrabFIBox.isChecked())

    def _enable_sources(self):
        self.UseLinearSourceFitBox.setEnabled(self.sdata is not None and self.GrabSourceBox.isChecked())

    def _extract_time_data(self):
        textn = self.ShotNumberEntry.text()
        if textn:
            snum = int(textn)
            if self.tdata is None or (self.tdata is not None and self.tdata["SHOT"] != snum):
                exnamelist = dict()
                exnamelist["MACHINE"] = 'AUG'
                exnamelist["SHOTINFO"] = "%10d" % (snum)
                try:
                    print("Time trace extraction started...")
                    tic = time.perf_counter()
                    tdata = EX2GK.extract_time_traces(exnamelist["MACHINE"],namelist=exnamelist)
                    if isinstance(tdata,dict) and "TIP" in tdata and tdata["TIP"] is not None:
                        self.tdata = copy.deepcopy(tdata)
                    else:
                        raise TypeError("Unknown extraction failure within machine-specific adapter.")
                    toc = time.perf_counter()
                    print("Time trace extraction finished. Time elapsed: %.6f" % (toc - tic))
                except Exception as e:
                    print(repr(e))
                    msg = QtGui.QMessageBox()
                    msg.setIcon(QtGui.QMessageBox.Critical)
                    msg.setWindowTitle("Fatal Error")
                    msg.setText("Time trace extraction procedure failed for selected discharge.")
                    msg.exec_()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Entry Error")
            msg.setText("Unrecognized input data.")
            msg.exec_()

    def _make_get_list(self):
        getlist = None
        qextract = []
        if self.GrabNEBox.isChecked():
            qextract.append("NE")
        if self.GrabTEBox.isChecked():
            qextract.append("TE")
        if self.GrabTIBox.isChecked():
            qextract.append("TI")
        if self.GrabNIBox.isChecked():
            qextract.append("NIMP")
        if self.GrabAFBox.isChecked():
            qextract.append("AF")
        if self.GrabFIBox.isChecked():
            qextract.append("FI")
        if self.GrabSourceBox.isChecked():
            qextract.append("QSTJ")
        if len(qextract) > 0:
            qextract.append("*")
            qextract.append("EQ")
        eqtag = self.EquilibriumList.currentText().lower()
        for var in qextract:
            if var == "EQ" and eqtag in self._preset_fields[var]:
                value = self._preset_fields[var][eqtag]
                (seq,uid) = self._get_data_specification_entry(eqtag) if self.AcceptRiskBox.isChecked() else ('0','augd')
                for kk in np.arange(0,len(value)):
                    getlist[eqtag+"/"+value[kk][0]] = [value[kk][1],value[kk][2],"---","%d" % (value[kk][3]),seq,uid]
            elif var in self._preset_fields:
                if getlist is None:
                    getlist = dict()
                for key, value in self._preset_fields[var].items():
                    (seq,uid) = self._get_data_specification_entry(key.lower()) if self.AcceptRiskBox.isChecked() else ('0','augd')
                    for kk in np.arange(0,len(value)):
                        getlist[key+"/"+value[kk][0]] = [value[kk][1],value[kk][2],"---","%d" % (value[kk][3]),seq,uid]
        return getlist

    def _extract_data(self):
        textn = self.ShotNumberEntry.text()
        text1 = self.TimeStartEntry.text()
        text2 = self.TimeEndEntry.text()
        textt = self.NTimeWindowsEntry.text()
        if textn and text1 and text2:
            snum = int(textn)
            t1 = float(text1)
            t2 = float(text2)
            tt = int(textt)
            retval = QtGui.QMessageBox.Yes
            if t2 < t1:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Critical)
                msg.setWindowTitle("Invalid Time Window")
                msg.setText("Time window end must be larger than time window start.")
                retval = QtGui.QMessageBox.No
            elif (t2 - t1) / float(tt) < 0.099:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Question)
                msg.setWindowTitle("Time Windows Too Short")
                msg.setText("It is not recommended to have time windows covering less than 100 ms, due to general limited availability of equilibrium reconstruction data. Proceed anyway?")
                msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                retval = msg.exec_()
            if retval == QtGui.QMessageBox.Yes:
                fielddict = self._make_get_list()
                if fielddict is not None:
                    exnamelist = dict()
                    exnamelist["MACHINE"] = 'AUG'
                    exnamelist["INTERFACE"] = self.InterfaceList.currentText().lower()
                    exnamelist["DATADICT"] = fielddict
                    exnamelist["NWINDOWS"] = tt if tt > 0 else 1
                    exnamelist["OUTPUTLEVEL"] = 0
                    exnamelist["SHOTPHASE"] = 1 if self.AcceptRiskBox.isChecked() and self.NonFlatTopBox.isChecked() else 0
                    exnamelist["SRCOPT"] = 0
                    exnamelist["SELECTEQ"] = self.EquilibriumList.currentText().lower()
                    exnamelist["GPCOORDFLAG"] = True
#                    exnamelist["RSHIFTFLAG"] = True
#                    exnamelist["RHOEBFLAG"] = False
                    if self.AcceptRiskBox.isChecked():
                        if self.BasicDebugBox.isChecked():
                            exnamelist["DEBUGFLAG"] = True
                        if self.DebugEquilibriumBox.isChecked():
                            exnamelist["CDEBUG"] = True
#                        exnamelist["RSHIFTFLAG"] = self.UseRMajorShiftBox.isChecked()
#                        if self.NEErrorMultiplierEntry.text() and "NE" in qextract:
#                            exnamelist["NEEBMULT"] = float(self.NEErrorMultiplierEntry.text())
#                        if self.TEErrorMultiplierEntry.text() and "TE" in qextract:
#                            exnamelist["TEEBMULT"] = float(self.TEErrorMultiplierEntry.text())
#                        if self.NIErrorMultiplierEntry.text() and "NI" in qextract:
#                            exnamelist["NIEBMULT"] = float(self.NIErrorMultiplierEntry.text())
#                        if self.TIErrorMultiplierEntry.text() and "TI" in qextract:
#                            exnamelist["TIEBMULT"] = float(self.TIErrorMultiplierEntry.text())
#                        if self.NIMPErrorMultiplierEntry.text() and "NI" in qextract:
#                            exnamelist["NIMPEBMULT"] = float(self.NIMPErrorMultiplierEntry.text())
                        if self.TIMPErrorMultiplierEntry.text() and "TI" in qextract:
                            exnamelist["TIMPEBMULT"] = float(self.TIMPErrorMultiplierEntry.text())
#                        if self.AFErrorMultiplierEntry.text() and "ANGF" in qextract:
#                            exnamelist["AFEBMULT"] = float(self.AFErrorMultiplierEntry.text())
#                        if self.QErrorMultiplierEntry.text() and "Q" in qextract:
#                            exnamelist["QEBMULT"] = float(self.QErrorMultiplierEntry.text())
                    if fcustomincurrent and self.filtsrcdir != 'N/A':
                        exnamelist["CUSTOMPATH"] = self.filtsrcdir
                    try:
                        print("Data extraction started...")
                        tic = time.perf_counter()
                        exnamelist["SHOTINFO"] = "%8d %10.6f %10.6f" % (snum,t1,t2)
                        slist = EX2GK.extract_and_standardize_data(exnamelist["MACHINE"],namelist=exnamelist)
                        if isinstance(slist,list):
                            for jj in np.arange(0,len(slist)):
                                self._add_to_list(slist[jj])
#                            tidx = self.DatabaseList.rowCount()
#                            self.DatabaseList.setCurrentCell(tidx-1,0)
                            self._select_data()
                        else:
                            raise TypeError("Unknown extraction failure within machine-specific adapter.")
                        toc = time.perf_counter()
                        print("Data extraction finished. Time elapsed: %.6f" % (toc - tic))
                    except Exception as e:
                        print(repr(e))
                        msg = QtGui.QMessageBox()
                        msg.setIcon(QtGui.QMessageBox.Critical)
                        msg.setWindowTitle("Fatal Error")
                        msg.setText("Extraction procedure failed for selected time window configuration.")
                        msg.exec_()
            else:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Warning)
                msg.setWindowTitle("Entry Error")
                msg.setText("At least one fit quantity must be selected.")
                msg.exec_()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Entry Error")
            msg.setText("Unrecognized input time window data.")
            msg.exec_()

    def _fit_data(self):
        if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady() and self.didx is not None and self.didx < self.DatabaseList.rowCount():
            self.DatabaseList.setCurrentCell(self.didx,0)
            textn = self.ShotNumberEntry.text()
            text1 = self.TimeStartEntry.text()
            text2 = self.TimeEndEntry.text()
            snum = int(textn) if textn else -1
            t1 = float(text1) if text1 else -9999.9999
            t2 = float(text2) if text2 else 9999.9999
            retval = QtGui.QMessageBox.Yes
            if self.sdata["META"]["SHOT"] != snum or np.abs(self.sdata["META"]["T1"] - t1) > 1.0e-6 or np.abs(self.sdata["META"]["T2"] - t2) > 1.0e-6:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Question)
                msg.setWindowTitle("Time Window Inconsistency")
                msg.setText("Time window inputs are inconsistent with currently stored data. Continue fitting anyway?")
                msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                retval = msg.exec_()
            if retval == QtGui.QMessageBox.Yes:
                exnamelist = dict()
                exnamelist["MACHINE"] = 'AUG'
                exnamelist["OUTPUTLEVEL"] = 0
                exnamelist["CSOMIN"] = 0.0
                exnamelist["CSOMAX"] = 1.0
                exnamelist["CSON"] = 101
                exnamelist["LINFIFLAG"] = self.UseLinearFastIonFitBox.isChecked()
                exnamelist["LINSRCFLAG"] = self.UseLinearSourceFitBox.isChecked()
                use_rhoerrs = False
                if self.AcceptRiskBox.isChecked():
                    exnamelist["DEBUGFLAG"] = self.BasicDebugBox.isChecked()
                try:
                    print("Fitting procedure started...")
                    tic = time.perf_counter()
#                    if "CS_OUT" in self.sdata and self.sdata["CS_OUT"] is not None:
#                        self.sdata = EX2GK.reset_gp_fit_settings(self.sdata,exnamelist["MACHINE"],namelist=exnamelist)
                    jj = self.CoordinateList.currentIndex()
                    cslist = ['rhotor','rhopol','psitorn','psipoln','psitor','psipol','rmajoro','rmajori','rminoro','rminori']
                    exnamelist["CSO"] = cslist[jj]
                    prefixlist = self.sdata["CD"].getPrefixes()
                    exnamelist["CSOP"] = ""
                    self.sdata = EX2GK.fit_standardized_data(self.sdata,namelist=exnamelist)
                    if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata["META"]["CSO"] is not None:
                        self.DatabaseList.currentItem().set_data(self.sdata)
                        self.csidx = self._csconv[self.clist[jj]]
                        self._select_data()
                    else:
                        raise TypeError("Unknown failure inside GPR1D fitting algorithm or given settings.")
                    toc = time.perf_counter()
                    print("Fitting procedure finished. Time elapsed: %.6f" % (toc - tic))
                except Exception as e:
                    print(repr(e))
                    msg = QtGui.QMessageBox()
                    msg.setIcon(QtGui.QMessageBox.Critical)
                    msg.setWindowTitle("Fatal Error")
                    msg.setText("Fitting procedure failed with currently stored data.")
                    msg.exec_()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Required data not extracted yet.")
            msg.exec_()

    def _fit_all_data(self):
        numitems = self.DatabaseList.rowCount()
        for ii in np.arange(0,numitems):
            self.DatabaseList.setCurrentCell(ii,0)
            tempitem = self.DatabaseList.currentItem()
            self.sdata = tempitem.get_data() if isinstance(tempitem,QTableWidgetTimeWindow) else None
            self.didx = self.DatabaseList.currentRow() if self.sdata is not None else None
            if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
                self.ShotNumberEntry.setText("%d" % (self.sdata["META"]["SHOT"]))
                self.TimeStartEntry.setText("%.4f" % (self.sdata["META"]["T1"]))
                self.TimeEndEntry.setText("%.4f" % (self.sdata["META"]["T2"]))
                self._fit_data()

    def _post_process_data(self):
        if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata.isFitted() and self.didx is not None and self.didx < self.DatabaseList.rowCount():
            self.DatabaseList.setCurrentCell(self.didx,0)
            textn = self.ShotNumberEntry.text()
            text1 = self.TimeStartEntry.text()
            text2 = self.TimeEndEntry.text()
            snum = int(textn) if textn else -1
            t1 = float(text1) if text1 else -9999.9999
            t2 = float(text2) if text2 else 9999.9999
            retval = QtGui.QMessageBox.Yes
            if self.sdata["META"]["SHOT"] != snum or np.abs(self.sdata["META"]["T1"] - t1) > 1.0e-6 or np.abs(self.sdata["META"]["T2"] - t2) > 1.0e-6:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Question)
                msg.setWindowTitle("Time Window Inconsistency")
                msg.setText("Time window inputs are inconsistent with currently stored data. Continue post-processing anyway?")
                msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                retval = msg.exec_()
            if retval == QtGui.QMessageBox.Yes and self.sdata["META"]["CODE"] is not None:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Question)
                msg.setWindowTitle("Post-processing Data Found")
                msg.setText("Currently stored data has already been post-processed according to %s. Continue post-processing anyway?" % (self.sdata["META"]["CODE"]))
                msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                retval = msg.exec_()
            if retval == QtGui.QMessageBox.Yes:
                exnamelist = dict()
                exnamelist["OUTPUTLEVEL"] = 0
                exnamelist["FINALCODE"] = self.CodeAdapterList.currentText().lower()
                exnamelist["FORCEPOST"] = True
                if self.AcceptRiskBox.isChecked():
                    exnamelist["DEBUGFLAG"] = self.BasicDebugBox.isChecked()
                try:
                    print("Post-processing procedure started...")
                    tic = time.perf_counter()
                    self.sdata = EX2GK.post_process_standardized_data(self.sdata,namelist=exnamelist)
                    if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata["META"]["CSO"] is not None:
                        self.DatabaseList.currentItem().set_data(self.sdata)
                        self._select_data()
                    else:
                        raise TypeError("Unknown post-processing failure within code-specific adapter.")
                    toc = time.perf_counter()
                    print("Post-processing procedure finished. Time elapsed: %.6f" % (toc - tic))
                except Exception as e:
                    print(repr(e))
                    msg = QtGui.QMessageBox()
                    msg.setIcon(QtGui.QMessageBox.Critical)
                    msg.setWindowTitle("Fatal Error")
                    msg.setText("Post-processing procedure failed with currently stored data.")
                    msg.exec_()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Input data not fitted yet, cannot post-process.")
            msg.exec_()

    def _select_data(self):
        tempitem = self.DatabaseList.currentItem()
        self.sdata = tempitem.get_data() if isinstance(tempitem,QTableWidgetTimeWindow) else None
        self.didx = self.DatabaseList.currentRow() if self.sdata is not None else None
        if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
            cdlist = self.sdata["CD"].coord_keys()
            rdlist = self.sdata["RD"].getPresentFields('list')
            ii = tempitem.get_time_window_number()
            self.ShotNumberEntry.setText("%d" % (self.sdata["META"]["SHOT"]))
            self.TimeStartEntry.setText("%.4f" % (self.sdata["META"]["T1"]))
            self.TimeEndEntry.setText("%.4f" % (self.sdata["META"]["T2"]))
            self.NTimeWindowsEntry.setText("1")
            eqdda = self.sdata["META"]["EQSRC"]
            self.EquilibriumList.setCurrentIndex(2)
            if re.match(r'^EFTF$',eqdda,flags=re.IGNORECASE):
                self.EquilibriumList.setCurrentIndex(1)
            elif re.match(r'^EFTM$',eqdda,flags=re.IGNORECASE):
                self.EquilibriumList.setCurrentIndex(0)
            self.DatabaseList.setEnabled(True)
            self.SortWindowsButton.setEnabled(True)
            self.DeleteWindowButton.setEnabled(True)
            self.GrabNEBox.setChecked("NE" in rdlist and self.sdata["RD"]["NE"].npoints > 0)
            self.GrabTEBox.setChecked("TE" in rdlist and self.sdata["RD"]["TE"].npoints > 0)
            self.GrabTIBox.setChecked(("TI" in rdlist and self.sdata["RD"]["TI"].npoints > 0) or ("TIMP" in rdlist and self.sdata["RD"]["TIMP"].npoints > 0))
            self.GrabNIBox.setChecked("NIMP1" in rdlist and self.sdata["RD"]["NIMP1"].npoints > 0)
            self.GrabAFBox.setChecked("AFTOR" in rdlist and self.sdata["RD"]["AFTOR"].npoints > 0)
            self.GrabFIBox.setChecked(("NFINBI" in rdlist and self.sdata["RD"]["NFINBI"].npoints > 0) or ("NFIICRH" in rdlist and self.sdata["RD"]["NFIICRH"].npoints > 0))
            self.GrabSourceBox.setChecked(("STENBI" in rdlist and self.sdata["RD"]["STENBI"].npoints > 0) or ("STEICRH" in rdlist and self.sdata["RD"]["STEICRH"].npoints > 0))
            self._reset_all_box()
            self.SaveRawDataButton.setEnabled(True)
            self._enable_fast_ions()
            self._enable_sources()
            self.CoordinateList.setEnabled(True)
            self.CoordinateList.clear()
            self.clist = []
            if "RHOTORN" in cdlist and self.sdata["CD"][""]["RHOTORN"].npoints > 0:
                self.CoordinateList.addItem("rho_tor")
                self.clist.append("RHOTORN")
            if "RHOPOLN" in cdlist and self.sdata["CD"][""]["RHOPOLN"].npoints > 0:
                self.CoordinateList.addItem("rho_pol")
                self.clist.append("RHOPOLN")
            if "PSITORN" in cdlist and self.sdata["CD"][""]["PSITORN"].npoints > 0:
                self.CoordinateList.addItem("psi_norm_tor")
                self.clist.append("PSITORN")
            if "PSIPOLN" in cdlist and self.sdata["CD"][""]["PSITORN"].npoints > 0:
                self.CoordinateList.addItem("psi_norm_pol")
                self.clist.append("PSIPOLN")
            if "PSITOR" in cdlist and self.sdata["CD"][""]["PSITOR"].npoints > 0:
                self.CoordinateList.addItem("psi_tor")
                self.clist.append("PSITOR")
            if "PSIPOL" in cdlist and self.sdata["CD"][""]["PSIPOL"].npoints > 0:
                self.CoordinateList.addItem("psi_pol")
                self.clist.append("PSIPOL")
            if "RMAJORO" in cdlist and self.sdata["CD"][""]["RMAJORO"].npoints > 0:
                self.CoordinateList.addItem("R_major_LFS @ Z_mag")
                self.clist.append("RMAJORO")
            if "RMAJORI" in cdlist and self.sdata["CD"][""]["RMAJORI"].npoints > 0:
                self.CoordinateList.addItem("R_major_HFS @ Z_mag")
                self.clist.append("RMAJORI")
            self.FittingLabel.setEnabled(True)
            self.FitDataButton.setEnabled(True)
            self.FitAllDataButton.setEnabled(True)
            self.PlottingLabel.setEnabled(True)
            self.PlotProfileButton.setEnabled(True)
            self.CheckingLabel.setEnabled(True)
            self.ExplorationModeButton.setEnabled(True)
            self.SavingLabel.setEnabled(True)
            self.SaveDatabaseButton.setEnabled(True)
#            if "NEEBMULT" in self.sdata["META"]:
#                self.NEErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["NEEBMULT"]))
#            if "TEEBMULT" in self.sdata["META"]:
#                self.TEErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["TEEBMULT"]))
#            if "NIEBMULT" in self.sdata["META"]:
#                self.NIErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["NIEBMULT"]))
#            if "TIEBMULT" in self.sdata["META"]:
#                self.TIErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["TIEBMULT"]))
#            if "NIMPEBMULT" in self.sdata["META"]:
#                self.NIMPErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["NIMPEBMULT"]))
            if "TIMPEBMULT" in self.sdata["META"]:
                self.TIMPErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["TIMPEBMULT"]))
#            if "AFEBMULT" in self.sdata["META"]:
#                self.AFErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["AFEBMULT"]))
#            if "QEBMULT" in self.sdata["META"]:
#                self.QErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["QEBMULT"]))
            pdlist = self.sdata["PD"].getPresentFields('list') if "PD" in self.sdata else []
            if self.sdata["META"]["CSO"] is not None and "X" in pdlist and self.sdata["PD"]["X"].npoints > 0:
                self.UseLinearFastIonFitBox.setChecked(bool(self.sdata["FLAG"].checkFlag("LINFI")))
                self.UseLinearSourceFitBox.setChecked(bool(self.sdata["FLAG"].checkFlag("LINSRC")))
                self.CoordinateList.setCurrentIndex(0)
                for jj in np.arange(0,len(self.clist)):
                    if self.clist[jj] == self.sdata["META"]["CSO"].upper():
                        self.CoordinateList.setCurrentIndex(jj)
                        self.csidx = self._csconv[self.clist[jj]]
                self.SaveFitDataButton.setEnabled(True)
                self.PlotGradientButton.setEnabled(True)
                self.PlotSourceButton.setEnabled(True)
                self.PlotMiscButton.setEnabled(True)
                self.PassSigmaEntry.setEnabled(True)
                if "QCPASS" in self.sdata["META"] and self.sdata["META"]["QCPASS"] is not None:
                    self.PassSigmaEntry.setText("%.4f" % (self.sdata["META"]["QCPASS"]))
                else:
                    self.PassSigmaEntry.setText("1.96")
                self.QualityCheckButton.setEnabled(True)
                self.ProcessingLabel.setEnabled(True)
                self.CodeAdapterList.setEnabled(True)
                self.CodeAdapterList.setCurrentIndex(0)
                self.ProcessDataButton.setEnabled(True)
                if "CODE" in self.sdata["META"] and self.sdata["META"]["CODE"] is not None:
                    self.CodeAdapterList.setCurrentIndex(0)
                    if re.match(r'^qualikiz$',self.sdata["META"]["CODE"],flags=re.IGNORECASE):
                        self.CodeAdapterList.setCurrentIndex(1)
                    self.SaveProcessDataButton.setEnabled(True)
                    self.PlotProcessButton.setEnabled(True)
                else:
                    self.SaveProcessDataButton.setEnabled(False)
                    self.PlotProcessButton.setEnabled(False)
            else:
                self.UseLinearFastIonFitBox.setChecked(False)
                self.UseLinearSourceFitBox.setChecked(True)
                self.CoordinateList.setCurrentIndex(0)
                self.SaveFitDataButton.setEnabled(False)
                self.PlotGradientButton.setEnabled(False)
                self.PlotSourceButton.setEnabled(False)
                self.PlotMiscButton.setEnabled(False)
                self.PassSigmaEntry.setEnabled(False)
                self.PassSigmaEntry.setText("1.96")
                self.QualityCheckButton.setEnabled(False)
                self.ProcessingLabel.setEnabled(False)
                self.CodeAdapterList.setEnabled(False)
                self.ProcessDataButton.setEnabled(False)
                self.SaveProcessDataButton.setEnabled(False)
                self.PlotProcessButton.setEnabled(False)
#                self.UseRMajorShiftBox.setChecked("RSHIFT" in self.sdata["FLAG"] and self.sdata["FLAG"]["RSHIFT"])
        else:
            self.DatabaseList.setEnabled(False)
            self.SortWindowsButton.setEnabled(False)
            self.DeleteWindowButton.setEnabled(False)
            self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setChecked(False)
            self.GrabNIBox.setChecked(False)
            self.GrabAFBox.setChecked(False)
            self.GrabFIBox.setChecked(False)
            self.GrabSourceBox.setChecked(False)
            self._reset_all_box()
            self.SaveRawDataButton.setEnabled(False)
            self._enable_fast_ions()
            self._enable_sources()
            self.CoordinateList.setEnabled(False)
            self.CoordinateList.clear()
            self.clist = []
            self.FitDataButton.setEnabled(False)
            self.FitAllDataButton.setEnabled(False)
            self.PlottingLabel.setEnabled(False)
            self.PlotProfileButton.setEnabled(False)
            self.CheckingLabel.setEnabled(False)
            self.ExplorationModeButton.setEnabled(False)
            self.SavingLabel.setEnabled(False)
            self.SaveDatabaseButton.setEnabled(False)
            self.FittingLabel.setEnabled(False)
            self.UseLinearFastIonFitBox.setChecked(False)
            self.UseLinearSourceFitBox.setChecked(True)
            self.SaveFitDataButton.setEnabled(False)
            self.PlottingLabel.setEnabled(False)
            self.PlotProfileButton.setEnabled(False)
            self.PlotGradientButton.setEnabled(False)
            self.PlotSourceButton.setEnabled(False)
            self.PlotMiscButton.setEnabled(False)
            self.CheckingLabel.setEnabled(False)
            self.PassSigmaEntry.setEnabled(False)
            self.PassSigmaEntry.setText("1.96")
            self.QualityCheckButton.setEnabled(False)
            self.ProcessingLabel.setEnabled(False)
            self.CodeAdapterList.setEnabled(False)
            self.CodeAdapterList.setCurrentIndex(0)
            self.ProcessDataButton.setEnabled(False)
            self.SaveProcessDataButton.setEnabled(False)
            self.PlotProcessButton.setEnabled(False)
#            self.UseRMajorShiftBox.setChecked(False)

    def _class_converter(self,database):
        newdata = dict()
        if isinstance(database,dict) and not isinstance(database,EX2GK.classes.EX2GKTimeWindow):
            for snum in database:
                newdata[snum] = []
                for ii in np.arange(0,len(database[snum])):
                    if not isinstance(database[snum][ii],EX2GK.classes.EX2GKTimeWindow):
                        twobj = EX2GK.classes.EX2GKTimeWindow.importFromDict(database[snum][ii])
                        newdata[snum].append(copy.deepcopy(twobj))
                    else:
                        newdata[snum].append(copy.deepcopy(database[snum][ii]))
        return newdata

    def _generate_table(self,database):
        if isinstance(database,dict):
            self.DatabaseList.setEnabled(True)
            self.DatabaseList.clear()
            self.DatabaseList.setRowCount(0)
            self.DatabaseList.setHorizontalHeaderLabels(["Time Window"])
            for snum in sorted(database.keys()):
                tlist = []
                tbegarr = []
                tendarr = []
                for ii in np.arange(0,len(database[snum])):
                    idx = None
                    repflag = False
                    for jj in np.arange(0,len(tlist)):
                        if idx is None:
                            if np.abs(tbegarr[jj] - database[snum][ii]["META"]["T1"]) < 1.0e-6:
                                if np.abs(tendarr[jj] - database[snum][ii]["META"]["T2"]) < 1.0e-6:
                                    repflag = True
                                    idx = jj
                                elif tendarr[jj] < database[snum][ii]["META"]["T2"]:
                                    idx = jj
                            elif tbegarr[jj] > database[snum][ii]["META"]["T1"]:
                                idx = jj
                    if idx is None:
                        idx = len(tlist)
                    if repflag:
                        tlist[idx] = ii
                        tbegarr[idx] = database[snum][ii]["META"]["T1"]
                        tendarr[idx] = database[snum][ii]["META"]["T2"]
                    else:
                        tlist.insert(idx,ii)
                        tbegarr.insert(idx,database[snum][ii]["META"]["T1"])
                        tendarr.insert(idx,database[snum][ii]["META"]["T2"])
                for tt in np.arange(0,len(tlist)):
                    tdata = database[snum][tlist[tt]]
                    tableItem = QTableWidgetTimeWindow("%d : %8.4f -- %8.4f" % (tdata["META"]["SHOT"],tdata["META"]["T1"],tdata["META"]["T2"]),tdata,tt)
                    tidx = self.DatabaseList.rowCount()
                    self.DatabaseList.insertRow(tidx)
                    self.DatabaseList.setItem(tidx,0,tableItem)
            tidx = self.DatabaseList.rowCount()
            if tidx > 0:
                self.DatabaseList.setCurrentCell(tidx-1,0)

    def _delete_current_row(self):
        tidx = self.DatabaseList.currentRow()
        tdata = self.DatabaseList.currentItem()
        del tdata
        self.DatabaseList.removeRow(tidx)
        tidx = tidx - 1 if tidx > 0 else tidx
        self.DatabaseList.setCurrentCell(tidx,0)
        self._select_data()

    def _add_to_list(self,sdata):
        if isinstance(sdata,EX2GK.classes.EX2GKTimeWindow):
            retval = QtGui.QMessageBox.Yes
            didx = None
            for ii in np.arange(0,self.DatabaseList.rowCount()):
                titem = self.DatabaseList.item(ii,0)
                tdata = titem.get_data()
                if sdata["META"]["SHOT"] == tdata["META"]["SHOT"]:
                    if np.abs(sdata["META"]["T1"] - tdata["META"]["T1"]) < 1.0e-6 and np.abs(sdata["META"]["T2"] - tdata["META"]["T2"]) < 1.0e-6:
                        msg = QtGui.QMessageBox()
                        msg.setIcon(QtGui.QMessageBox.Question)
                        msg.setWindowTitle("Time Window Exists")
                        msg.setText("Extracted time window already exists in list. Overwrite?")
                        msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                        retval = msg.exec_()
                        didx = titem.get_time_window_number()
            if retval == QtGui.QMessageBox.Yes:
                if didx is None:
                    tidx = self.DatabaseList.rowCount()
                    tableItem = QTableWidgetTimeWindow("%d : %8.4f -- %8.4f" % (sdata["META"]["SHOT"],sdata["META"]["T1"],sdata["META"]["T2"]),sdata,tidx)
                    self.DatabaseList.insertRow(tidx)
                    self.DatabaseList.setItem(tidx,0,tableItem)
                    self.DatabaseList.setCurrentCell(tidx,0)
                else:
                    self.DatabaseList.setCurrentCell(didx,0)
                    self.DatabaseList.currentItem().set_data(sdata)
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Input data in unrecognized format.")
            msg.exec_()

    def _load_data(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '', 'Pickle files (*.pkl);;All files (*)')
        if filename:
            dload = EX2GK.ptools.unpickle_this(filename)
            if isinstance(dload,EX2GK.classes.EX2GKTimeWindow):
                twobj = copy.deepcopy(dload)
                self._add_to_list(twobj)
                self._select_data()
            elif isinstance(dload,dict) and "META_DEVICE" in dload:
                twobj = EX2GK.classes.EX2GKTimeWindow.importFromDict(dload)
                self._add_to_list(twobj)
                self._select_data()
            elif isinstance(dload,dict):
                retval = QtGui.QMessageBox.Yes
                if self.DatabaseList.rowCount() > 0:
                    msg = QtGui.QMessageBox()
                    msg.setIcon(QtGui.QMessageBox.Question)
                    msg.setWindowTitle("Time Window List Exists")
                    msg.setText("Time window list is already populated. Discard existing list?")
                    msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                    retval = msg.exec_()
                if retval == QtGui.QMessageBox.Yes:
                    nload = self._class_converter(dload)
                    self._generate_table(nload)
                    self._select_data()
            else:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Warning)
                msg.setWindowTitle("Load Data Error")
                msg.setText("Unrecognizable data format in file. Loaded data was discarded.")
                msg.exec_()

    def _plot_time_traces(self):
        self._extract_time_data()
        text1 = self.TimeStartEntry.text()
        text2 = self.TimeEndEntry.text()
        t1 = float(text1) if text1 else None
        t2 = float(text2) if text2 else None
        self.TracePlotWidget = time_trace_plotter(self.tdata,t1,t2)

    def _plot_profile_data(self):
        self.ProfilePlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.ProfilePlotWidget = profile_plotter(self.sdata,1.0,csidx=jj)

    def _plot_gradient_data(self):
        self.GradientPlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.GradientPlotWidget = gradient_plotter(self.sdata,1.0,csidx=jj)

    def _plot_source_data(self):
        self.SourcePlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.SourcePlotWidget = source_plotter(self.sdata,1.0,csidx=jj)

    def _plot_misc_data(self):
        self.MiscPlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.MiscPlotWidget = misc_plotter(self.sdata,1.0,csidx=jj)

    def _plot_processed_gradient_data(self):
        self.ProcessedGradientPlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.ProcessedGradientPlotWidget = gradient_plotter(self.sdata,1.0,csidx=jj,procflag=True)

    def _run_quality_checks(self):
        if self.sdata:
            text = self.PassSigmaEntry.text()
            sigma = float(text) if text else 1.96
            goodflag = False
            try:
                self.sdata["META"].addValue("QCPASS",sigma)
                self.sdata._performQualityChecks()
                goodflag = True
            except Exception as e:
                goodflag = False
                print(repr(e))
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Warning)
                msg.setWindowTitle("Data Error")
                msg.setText("Data quality check procedure failed with currently stored data.")
                msg.exec_()
            if goodflag:
                rstr = "Pass criterion:\t%.3f" % (self.sdata["META"]["QCPASS"])
                hstr = "Not performed"
                if self.sdata["FLAG"].checkFlag("HTEST") is not None:
                    hstr = "PASSED" if self.sdata["FLAG"].checkFlag("HTEST") else "FAILED"
                rstr = rstr + ("\n\n----- Heating Power Test:\t%s\t-----" % (hstr))
                if self.sdata["FLAG"].checkFlag("HTEST") is not None:
                    rstr = rstr + ("\n\n       Total input power:   \t\t%.3f MW" % (self.sdata["ZD"]["POWINJ"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:   \t%.3f MW" % (self.sdata["ZD"]["POWINJ"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Total lost power:   \t\t%.3f MW" % (self.sdata["ZD"]["POWLSS"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:   \t%.3f MW" % (self.sdata["ZD"]["POWLSS"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n\n       Total absorbed power:        \t%.3f MW" % ((self.sdata["ZD"]["POWINJ"][""] - self.sdata["ZD"]["POWLSS"][""]) * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:   \t%.3f MW" % ((self.sdata["ZD"]["POWINJ"]["EB"] + self.sdata["ZD"]["POWLSS"]["EB"]) * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Total integrated power:     \t%.3f MW" % (self.sdata["ZD"]["POWDEP"][""] * 1.0e-6))
                qstr = "Not performed"
                if self.sdata["FLAG"].checkFlag("QTEST") is not None:
                    qstr = "PASSED" if self.sdata["FLAG"].checkFlag("QTEST") else "FAILED"
                rstr = rstr + ("\n\n----- Equipartition Test:\t%s\t-----" % (qstr))
                if self.sdata["FLAG"].checkFlag("QTEST") is not None:
                    eqpow = self.sdata["ZD"]["EQLIMPEQ"][""]
                    qdir = "e -> i" if eqpow > 0.0 else "i -> e"
                    rstr = rstr + ("\n\n       Closest equipartition power:   \t%.3f MW" % (np.abs(eqpow * 1.0e-6)))
                    rstr = rstr + ("\n       Associated available power:\t%.3f MW" % (np.abs(self.sdata["ZD"]["EQLIMPIN"][""] * 1.0e-6)))
                    rstr = rstr + ("\n\tError at criterion:	\t%.3f MW" % (self.sdata["ZD"]["EQLIMPIN"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Associated location (rho_tor):\t%.3f" % (self.sdata["ZD"]["EQLIMX"][""]))
                    rstr = rstr + ("\n       Power transfer direction:     \t%s" % (qdir))
                estr = "Not performed"
                if self.sdata["FLAG"].checkFlag("ETEST") is not None:
                    estr = "PASSED" if self.sdata["FLAG"].checkFlag("ETEST") else "FAILED"
                rstr = rstr + ("\n\n----- Energy Content Test:\t%s\t-----" % (estr))
                if self.sdata["FLAG"].checkFlag("ETEST") is not None:
                    rstr = rstr + ("\n\n       Total measured energy:    \t%.3f MJ" % (self.sdata["ZD"]["WEXP"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:     \t%.3f MJ" % (self.sdata["ZD"]["WEXP"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Total integrated energy:    \t%.3f MJ" % (self.sdata["ZD"]["WTOT"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:     \t%.3f MJ" % (self.sdata["ZD"]["WTOT"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n\n\tMain:\t\t\t%.3f MJ" % (self.sdata["ZD"]["WEI"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tImpurities:\t\t%.3f MJ" % ((self.sdata["ZD"]["WTH"][""] - self.sdata["ZD"]["WEI"][""]) * 1.0e-6))
                    rstr = rstr + ("\n\tFast ions:    \t\t%.3f MJ" % ((self.sdata["ZD"]["WTOT"][""] - self.sdata["ZD"]["WTH"][""]) * 1.0e-6))
                msg = QtGui.QMessageBox()
                msg.setWindowTitle("Data Quality Check Results")
                msg.setText(rstr)
                msg.exec_()

    def _enter_exploration_mode(self):
        if isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow):
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Information)
            msg.setWindowTitle("Entering Data Exploration Mode")
            msg.setText("Starting interactive Python in the terminal which this GUI was executed from.\nSelected time window data can be found in the variable \'data\', a Python dictionary.\nAny changes to the data made in this mode are deleted upon exiting.")
            msg.exec_()
            data = self.sdata.exportToDict()
            print("\nEntering data exploration mode...")
            print("Selected time window data can be found in the variable \'data\', which is a Python dictonary.")
            print("Any changes to the data made in this mode are deleted upon exiting.\n")
            QtCore.pyqtRemoveInputHook()
            embed()
            QtCore.pyqtRestoreInputHook()
            print("\nExiting data exploration mode...")
            print("Control returned to GUI.\n")
            del data
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Unrecognized data format in storage, unable to enter data exploration mode.")
            msg.exec_()

    def _save_raw_data(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if filename and isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
            numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
            numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
            jj = self.CoordinateList.currentIndex()
            csidx = self._csconv[self.clist[jj]]
            ocp = self.sdata["META"]["CSOP"] if "CSOP" in self.sdata["META"] and self.sdata["META"]["CSOP"] is not None else None
            cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
            ocs = cslist[0]
            xlims = (0.0,1.0)
            fxlim = True
            if csidx < len(cslist) and ocp+cslist[csidx] in self.sdata["CD"].coord_keys():
                ocs = cslist[csidx]
            dlist = self.sdata["RD"].getPresentFields('list')
            plist = []
            qlist = ["NE","TE","NI","TI","NIMP","TIMP","AFTOR","UTOR","Q","QC"]
            slist = ["NBI","ICRH","ECRH","LH","OHM"]
            sqlist = ["NFI","WFI","SN","STE","STI","SP","J"]
            for qq in np.arange(0,len(qlist)):
                if qlist[qq] in dlist:
                    plist.append(qlist[qq])
                if qlist[qq] in ["NI","TI"]:
                    for nn in np.arange(0,numion):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
                elif qlist[qq] in ["NIMP","TIMP"]:
                    for nn in np.arange(0,numimp):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
            for ss in np.arange(0,len(slist)):
                for qq in np.arange(0,len(sqlist)):
                    if sqlist[qq]+slist[ss] in dlist:
                        plist.append(sqlist[qq]+slist[ss])
            with open(filename,'w') as ff:
                if self.sdata:
                    ff.write("### EX2GK - Raw Data Save File ###\n")
                    ff.write("\n")
                    ff.write("START OF HEADER\n")
                    ff.write("\n")
                    ff.write("            Machine: %20s\n" % (self.sdata["META"]["DEVICE"]))
                    ff.write("        Shot Number: %20d\n" % (self.sdata["META"]["SHOT"]))
                    ff.write("  Radial Coordinate: %20s\n" % (ocs))
                    ff.write("  Time Window Start: %18.6f s\n" % (self.sdata["META"]["T1"]))
                    ff.write("    Time Window End: %18.6f s\n" % (self.sdata["META"]["T2"]))
                    ff.write("\n")
                    ff.write("END OF HEADER\n")
                    ff.write("\n")
                for kk in np.arange(0,len(plist)):
                    qtag = plist[kk]
                    ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                    icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ""
                    cocp = icp if ocp is None else ocp
                    xraw = self.sdata["RD"][qtag]["X"].flatten()
                    xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                    if ics != ocs:
                        (xraw,csj,txeraw) = self.sdata["CD"].convert(xraw,ics,ocs,icp,cocp)
                        xeraw = np.sqrt(np.power(xeraw,2.0) + np.power(txeraw,2.0))
                    yraw = self.sdata["RD"][qtag][""].flatten()
                    yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                    ydiag = self.sdata["RD"][qtag]["MAP"].flatten()
                    ff.write("%15s%20s%20s%20s%15s" % (ocs,qtag+" Raw","Err. "+qtag,"Err. "+ocs,"Origin"))
                    zz = re.match(r'^[NT]I(MP)?([0-9]*)$',qtag,flags=re.IGNORECASE)
                    if zz and zz.group(2):
                        imptag = zz.group(1).upper()
                        ztag = zz.group(2)
                        ff.write("  Mat.: %s\n" % (self.sdata["META"]["MATI"+imptag+ztag]))
                    elif zz:
                        ff.write("  Mat.: %s\n" % ("Combined"))
                    else:
                        ff.write("\n")
                    for ii in np.arange(0,xraw.size):
                        ddiag = self.sdata["RD"][qtag]["SRC"][int(ydiag[ii])]
                        ff.write("%15.4f%20.6e%20.6e%20.4f%15s\n" % (xraw[ii],yraw[ii],yeraw[ii],xeraw[ii],ddiag))
                    ff.write("\n")
            print("Raw data written into %s." % (filename))

    def _save_fit_data(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if filename and isinstance(self.sdata,EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
            numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
            numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
            numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
            jj = self.CoordinateList.currentIndex()
            csidx = self._csconv[self.clist[jj]]
            ocp = self.sdata["META"]["CSOP"]
            cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
            ocs = cslist[0]
            xlims = (0.0,1.0)
            fxlim = True
            if csidx < len(cslist) and ocp+cslist[csidx] in self.sdata["CD"].coord_keys():
                ocs = cslist[csidx]
            ics = self.sdata["META"]["CSO"]
            icp = self.sdata["META"]["CSOP"]
            dlist = self.sdata["PD"].getPresentFields('list')
            plist = []
            qlist = ["NE","TE","NI","TI","NIMP","TIMP","NZ","TZ","AFTOR","UTOR","Q","QC","ZEFF"]
            slist = ["NBI","ICRH","ECRH","LH","OHM"]
            sqlist = ["NFI","WFI","SN","STE","STI","SP","J"]
            for qq in np.arange(0,len(qlist)):
                if qlist[qq] in dlist:
                    plist.append(qlist[qq])
                if qlist[qq] in ["NI","TI"]:
                    for nn in np.arange(0,numion):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
                elif qlist[qq] in ["NIMP","TIMP"]:
                    for nn in np.arange(0,numimp):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
                elif qlist[qq] in ["NZ","TZ"]:
                    for nn in np.arange(0,2):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
            for ss in np.arange(0,len(slist)):
                for qq in np.arange(0,len(sqlist)):
                    if sqlist[qq]+slist[ss] in dlist:
                        plist.append(sqlist[qq]+slist[ss])
            with open(filename,'w') as ff:
                if self.sdata:
                    ff.write("### EX2GK - Fit Data Save File ###\n")
                    ff.write("\n")
                    ff.write("START OF HEADER\n")
                    ff.write("\n")
                    ff.write("            Machine: %20s\n" % (self.sdata["META"]["DEVICE"]))
                    ff.write("        Shot Number: %20d\n" % (self.sdata["META"]["SHOT"]))
                    ff.write("  Radial Coordinate: %20s\n" % (ocs))
                    ff.write("  Time Window Start: %18.6f s\n" % (self.sdata["META"]["T1"]))
                    ff.write("    Time Window End: %18.6f s\n" % (self.sdata["META"]["T2"]))
                    ff.write("\n")
                    ff.write("END OF HEADER\n")
                    ff.write("\n")
                for kk in np.arange(0,len(plist)):
                    qtag = plist[kk]
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    if ocs != ics:
                        (xfit,dj,de) = self.sdata["CD"].convert(self.sdata["PD"]["X"][""].flatten(),ics,ocs,icp,ocp)
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    dyfit = self.sdata["PD"][qtag]["GRAD"].flatten() if "GRAD" in self.sdata["PD"][qtag] else np.full(yfit.shape,np.NaN)
                    dyefit = self.sdata["PD"][qtag]["GRADEB"].flatten() if "GRADEB" in self.sdata["PD"][qtag] else np.full(yfit.shape,np.NaN)
                    ff.write("%15s%20s%20s%20s%20s" % (ocs,qtag+" Fit","Err. "+qtag+" Fit","D"+qtag+" Fit","Err. D"+qtag+" Fit"))
                    zz = re.match(r'^[NT](I|IMP|Z)([0-9]*)$',qtag,flags=re.IGNORECASE)
                    if zz and zz.group(2):
                        stag = zz.group(1).upper()
                        ztag = zz.group(2)
                        ff.write("  Mat.: %s\n" % (self.sdata["META"]["MAT"+stag+ztag]))
                    elif zz:
                        ff.write("  Mat.: %s\n" % ("Combined"))
                    else:
                        ff.write("\n")
                    for ii in np.arange(0,xfit.size):
                        ff.write("%15.4f%20.6e%20.6e%20.6e%20.6e\n" % (xfit[ii],yfit[ii],yefit[ii],dyfit[ii],dyefit[ii]))
                    ff.write("\n")
            print("Fit data written into %s." % (filename))

    def _save_post_processed_data(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if filename and self.sdata:
            adapter_grad_tags = {"CMN": "IL", "QLK": "A"}
            pddict = self.sdata["PD"].getPresentFields('dict')
            if "CODE" in self.sdata["META"] and self.sdata["META"]["CODE"] is not None and "OUT" in pddict:
                jj = self.CoordinateList.currentIndex()
                csidx = self._csconv[self.clist[jj]]
                ocp = self.sdata["META"]["CSOP"]
                cslist = ["RHOTORN","RHOPOLN","PSITORN","PSIPOLN","PSITOR","PSIPOL","RMAJORO","RMAJORI","RMINORO","RMINORI"]
                ocs = cslist[0]
                xlims = (0.0,1.0)
                fxlim = True
                if csidx < len(cslist) and ocp+cslist[csidx] in self.sdata["CD"].coord_keys():
                    ocs = cslist[csidx]
                ics = self.sdata["META"]["CSO"]
                icp = self.sdata["META"]["CSOP"]
                ccb =  self.sdata["META"]["CODEBASE"]
                ccs = "OUT"+ccb
                if ccb not in pddict["OUT"]:
                    ccb = ics
                    ccs = "X"
                ctag = self.sdata["META"]["CODETAG"]
                gtag = adapter_grad_tags[ctag] if ctag in adapter_grad_tags else ""
                ctag = ctag + "_"
                plist = []
                qlist = pddict["OUT"]
                for qq in np.arange(0,len(qlist)):
                    if qlist[qq].startswith(ctag):
                        plist.append(qlist[qq])
                plist = sorted(plist)
                with open(filename,'w') as ff:
                    if self.sdata:
                        ff.write("### EX2GK - Processed Data Save File ###\n")
                        ff.write("\n")
                        ff.write("START OF HEADER\n")
                        ff.write("\n")
                        ff.write("            Machine: %20s\n" % (self.sdata["META"]["DEVICE"]))
                        ff.write("        Shot Number: %20d\n" % (self.sdata["META"]["SHOT"]))
                        ff.write("  Radial Coordinate: %20s\n" % (ocs))
                        ff.write("  Time Window Start: %18.6f s\n" % (self.sdata["META"]["T1"]))
                        ff.write("    Time Window End: %18.6f s\n" % (self.sdata["META"]["T2"]))
                        ff.write("        Target Code: %20s\n" % (self.sdata["META"]["CODE"]))
                        ff.write("\n")
                        ff.write("END OF HEADER\n")
                        ff.write("\n")
                    for kk in np.arange(0,len(plist)):
                        qtag = plist[kk]
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        if ocs != ics:
                            (xfit,dj,de) = self.sdata["CD"].convert(self.sdata["PD"]["X"][""].flatten(),ics,ocs,icp,ocp)
                        xprc = self.sdata["PD"][ccs][""].flatten()
                        yprc = self.sdata["PD"]["OUT"+qtag][""].flatten()
                        yeprc = self.sdata["PD"]["OUT"+qtag]["EB"].flatten()
                        ff.write("%15s%15s%20s%20s\n" % (ocs,ccb,qtag+" Proc.","Err. "+qtag+" Proc."))
                        for ii in np.arange(0,xprc.size):
                            ff.write("%15.4f%15.4f%20.6e%20.6e\n" % (xfit[ii],xprc[ii],yprc[ii],yeprc[ii]))
                        ff.write("\n")
                print("Post-processed data written into %s." % (filename))

    def _make_database(self,exportflag=False):
        database = dict()
        for ii in np.arange(0,self.DatabaseList.rowCount()):
            sdata = self.DatabaseList.item(ii,0).get_data()
            snum = sdata["META"]["SHOT"]
            if snum not in database:
                database[snum] = []
            tdata = sdata.exportToDict() if exportflag else copy.deepcopy(sdata)
            database[snum].append(tdata)
        return database

    def _save_database(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Pickle files (*.pkl);;All files (*)')
        if filename:
            database = self._make_database(exportflag=True)
            EX2GK.ptools.pickle_this(database,filename,'/')
            print("Database saved in %s." % (filename))


def main():

    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('EX2GK_AUG')
    ex = EX2GK_AUG()

    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
