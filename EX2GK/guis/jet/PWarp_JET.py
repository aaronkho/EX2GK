#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

# Required imports
import os
import sys
import socket
import datetime
import re
import pwd
import time
import copy
import pickle
import collections
import numpy as np

ppfIsHere = True
try:
    import ppf
except ImportError:
    ppfIsHere = False

import matplotlib
matplotlib.use("Qt4Agg")

from PyQt4 import QtCore, QtGui
from matplotlib.backends import backend_qt4agg as mplqt

from EX2GK.tools.general import proctools as ptools, read_EX2GK_output as rtxt, profile_warper as pwarp


class _WarpFunctionWidget(QtGui.QWidget):

    def __init__(self,name,fOn=True):
        super(_WarpFunctionWidget,self).__init__()
        self.name = name
        self.aflag = True if fOn else False
        self.pkeys = []
        self.plabels = dict()
        self.pwidgets = dict()

    def add_parameter(self,key,widget,label=None):
        if isinstance(widget,QtGui.QLineEdit):
            self.pkeys.append(key)
            self.pwidgets[key] = widget
            if isinstance(label,QtGui.QLabel):
                self.plabels[key] = label
            elif label is None:
                self.plabels[key] = None
            else:
                raise TypeError("Invalid input type for parameter label")
        else:
            raise TypeError("Input parameter to WarpFunctionWidget must be a QtGui.QLineEdit widget")

    def _remove_parameter(self,key):
        try:
            self.pkeys.remove(key)
            del self.pwidgets[key]
            del self.plabels[key]
        except ValueError:
            print("Parameter key %s not found in WarpFunctionWidget" % (key))

    def make_layout(self):
        pbox = None
        if len(self.pkeys) > 0:
            pbox = QtGui.QFormLayout()
            for ii in np.arange(0,len(self.pkeys)):
                if isinstance(self.plabels[self.pkeys[ii]],QtGui.QLabel):
                    pbox.addRow(self.plabels[self.pkeys[ii]],self.pwidgets[self.pkeys[ii]])
                else:
                    pbox.addRow(self.pkeys[ii],self.pwidgets[self.pkeys[ii]])

        layoutBox = None
        if isinstance(pbox,QtGui.QLayout):
            layoutBox = QtGui.QVBoxLayout()
            layoutBox.addLayout(pbox)
        else:
            print("No parameters added to WarpFunctionWidget, layout cannot be made")
        if isinstance(layoutBox,QtGui.QVBoxLayout):
            layoutBox.addStretch(1)

        return layoutBox

    def toggle_all(self,tOn=None):
        if tOn is None:
            self.aflag = (not self.aflag)
        else:
            self.aflag = True if tOn else False
        for ii in np.arange(0,len(self.pkeys)):
            if isinstance(self.pwidgets[self.pkeys[ii]],QtGui.QWidget):
                self.pwidgets[self.pkeys[ii]].setEnabled(self.aflag)
            if isinstance(self.plabels[self.pkeys[ii]],QtGui.QWidget):
                self.plabels[self.pkeys[ii]].setEnabled(self.aflag)

    def get_name(self):
        name = self.name if self.aflag else None
        return name

    def get_parameters(self):
        pars = None
        if self.aflag:
            pars = []
            for ii in np.arange(0,len(self.pkeys)):
                if isinstance(self.pwidgets[self.pkeys[ii]],QtGui.QLineEdit):
                    pars.append(float(self.pwidgets[self.pkeys[ii]].text()))
            pars = np.array(pars).flatten()
        return pars


##### Custom implementations to be placed below #####


class ConstWarpFunctionWidget(_WarpFunctionWidget):

    def __init__(self,fOn=True):
        super(ConstWarpFunctionWidget,self).__init__("const",fOn)
        self.ConstWarpFunctionUI()

    def ConstWarpFunctionUI(self):

        ConstParLabel = QtGui.QLabel("Amplitude:")
        ConstParLabel.setEnabled(self.aflag)
        ConstParLabel.setAlignment(QtCore.Qt.AlignRight)
        ConstParEntry = QtGui.QLineEdit("0.0e0")
        ConstParEntry.setEnabled(self.aflag)
        ConstParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('constant',ConstParEntry,label=ConstParLabel)

        kbox = self.make_layout()
        self.setLayout(kbox)


class LinWarpFunctionWidget(_WarpFunctionWidget):

    def __init__(self,fOn=True):
        super(LinWarpFunctionWidget,self).__init__("lin",fOn)
        self.LinWarpFunctionUI()

    def LinWarpFunctionUI(self):

        SlopeParLabel = QtGui.QLabel("Slope:")
        SlopeParLabel.setEnabled(self.aflag)
        SlopeParLabel.setAlignment(QtCore.Qt.AlignRight)
        SlopeParEntry = QtGui.QLineEdit("1.0e0")
        SlopeParEntry.setEnabled(self.aflag)
        SlopeParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('slope',SlopeParEntry,label=SlopeParLabel)

        XInterceptParLabel = QtGui.QLabel("X Intercept:")
        XInterceptParLabel.setEnabled(self.aflag)
        XInterceptParLabel.setAlignment(QtCore.Qt.AlignRight)
        XInterceptParEntry = QtGui.QLineEdit("0.0e0")
        XInterceptParEntry.setEnabled(self.aflag)
        XInterceptParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('xint',XInterceptParEntry,label=XInterceptParLabel)

        kbox = self.make_layout()
        self.setLayout(kbox)


class TanhWarpFunctionWidget(_WarpFunctionWidget):

    def __init__(self,fOn=True):
        super(TanhWarpFunctionWidget,self).__init__("tanh",fOn)
        self.TanhWarpFunctionUI()

    def TanhWarpFunctionUI(self):

        YOffsetParLabel = QtGui.QLabel("Y Offset:")
        YOffsetParLabel.setEnabled(self.aflag)
        YOffsetParLabel.setAlignment(QtCore.Qt.AlignRight)
        YOffsetParEntry = QtGui.QLineEdit("0.0e0")
        YOffsetParEntry.setEnabled(self.aflag)
        YOffsetParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('yoff',YOffsetParEntry,label=YOffsetParLabel)

        XOffsetParLabel = QtGui.QLabel("X Offset:")
        XOffsetParLabel.setEnabled(self.aflag)
        XOffsetParLabel.setAlignment(QtCore.Qt.AlignRight)
        XOffsetParEntry = QtGui.QLineEdit("0.0e0")
        XOffsetParEntry.setEnabled(self.aflag)
        XOffsetParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('xoff',XOffsetParEntry,label=XOffsetParLabel)

        YScaleParLabel = QtGui.QLabel("Y Scaling:")
        YScaleParLabel.setEnabled(self.aflag)
        YScaleParLabel.setAlignment(QtCore.Qt.AlignRight)
        YScaleParEntry = QtGui.QLineEdit("1.0e0")
        YScaleParEntry.setEnabled(self.aflag)
        YScaleParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('yscale',YScaleParEntry,label=YScaleParLabel)

        XScaleParLabel = QtGui.QLabel("X Scaling:")
        XScaleParLabel.setEnabled(self.aflag)
        XScaleParLabel.setAlignment(QtCore.Qt.AlignRight)
        XScaleParEntry = QtGui.QLineEdit("1.0e0")
        XScaleParEntry.setEnabled(self.aflag)
        XScaleParEntry.setValidator(QtGui.QDoubleValidator(0.0,np.Inf,100,None))
        self.add_parameter('xscale',XScaleParEntry,label=XScaleParLabel)

        kbox = self.make_layout()
        self.setLayout(kbox)


class DblTanhWarpFunctionWidget(_WarpFunctionWidget):

    def __init__(self,fOn=True):
        super(DblTanhWarpFunctionWidget,self).__init__("Sum(tanh-tanh)",fOn)
        self.DblTanhWarpFunctionUI()

    def DblTanhWarpFunctionUI(self):

        YOffset1ParLabel = QtGui.QLabel("Y Offset 1:")
        YOffset1ParLabel.setEnabled(self.aflag)
        YOffset1ParLabel.setAlignment(QtCore.Qt.AlignRight)
        YOffset1ParEntry = QtGui.QLineEdit("0.0e0")
        YOffset1ParEntry.setEnabled(self.aflag)
        YOffset1ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('yoff1',YOffset1ParEntry,label=YOffset1ParLabel)

        XOffset1ParLabel = QtGui.QLabel("X Offset 1:")
        XOffset1ParLabel.setEnabled(self.aflag)
        XOffset1ParLabel.setAlignment(QtCore.Qt.AlignRight)
        XOffset1ParEntry = QtGui.QLineEdit("0.0e0")
        XOffset1ParEntry.setEnabled(self.aflag)
        XOffset1ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('xoff1',XOffset1ParEntry,label=XOffset1ParLabel)

        YScale1ParLabel = QtGui.QLabel("Y Scaling 1:")
        YScale1ParLabel.setEnabled(self.aflag)
        YScale1ParLabel.setAlignment(QtCore.Qt.AlignRight)
        YScale1ParEntry = QtGui.QLineEdit("1.0e0")
        YScale1ParEntry.setEnabled(self.aflag)
        YScale1ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('yscale1',YScale1ParEntry,label=YScale1ParLabel)

        XScale1ParLabel = QtGui.QLabel("X Scaling 1:")
        XScale1ParLabel.setEnabled(self.aflag)
        XScale1ParLabel.setAlignment(QtCore.Qt.AlignRight)
        XScale1ParEntry = QtGui.QLineEdit("1.0e0")
        XScale1ParEntry.setEnabled(self.aflag)
        XScale1ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('xscale1',XScale1ParEntry,label=XScale1ParLabel)

        YOffset2ParLabel = QtGui.QLabel("Y Offset 2:")
        YOffset2ParLabel.setEnabled(self.aflag)
        YOffset2ParLabel.setAlignment(QtCore.Qt.AlignRight)
        YOffset2ParEntry = QtGui.QLineEdit("0.0e0")
        YOffset2ParEntry.setEnabled(self.aflag)
        YOffset2ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('yoff2',YOffset2ParEntry,label=YOffset2ParLabel)

        XOffset2ParLabel = QtGui.QLabel("X Offset 2:")
        XOffset2ParLabel.setEnabled(self.aflag)
        XOffset2ParLabel.setAlignment(QtCore.Qt.AlignRight)
        XOffset2ParEntry = QtGui.QLineEdit("0.0e0")
        XOffset2ParEntry.setEnabled(self.aflag)
        XOffset2ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('xoff2',XOffset2ParEntry,label=XOffset2ParLabel)

        YScale2ParLabel = QtGui.QLabel("Y Scaling 2:")
        YScale2ParLabel.setEnabled(self.aflag)
        YScale2ParLabel.setAlignment(QtCore.Qt.AlignRight)
        YScale2ParEntry = QtGui.QLineEdit("1.0e0")
        YScale2ParEntry.setEnabled(self.aflag)
        YScale2ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('yscale2',YScale2ParEntry,label=YScale2ParLabel)

        XScale2ParLabel = QtGui.QLabel("X Scaling 2:")
        XScale2ParLabel.setEnabled(self.aflag)
        XScale2ParLabel.setAlignment(QtCore.Qt.AlignRight)
        XScale2ParEntry = QtGui.QLineEdit("1.0e0")
        XScale2ParEntry.setEnabled(self.aflag)
        XScale2ParEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))
        self.add_parameter('xscale2',XScale2ParEntry,label=XScale2ParLabel)

        kbox = self.make_layout()
        self.setLayout(kbox)


class PWarp_JET(QtGui.QWidget):

    def __init__(self,ppfflag=False):
        super(PWarp_JET,self).__init__()
        self.ppfflag = ppfflag
        self.odata = None
        self.pdata = None
        self.lastmod = None
        self.initUI()

    def initUI(self):

        self.LoadTextDataButton = QtGui.QPushButton("Load Data from Text")
        self.LoadTextDataButton.clicked.connect(self._load_text_data)
        self.LoadPickleDataButton = QtGui.QPushButton("Load Data from Pickle")
#        self.LoadPickleDataButton.setEnabled(False)
        self.LoadPickleDataButton.clicked.connect(self._load_pickle_data)

        self.QuantityLabel = QtGui.QLabel("Quantities")
        self.QuantityLabel.setEnabled(False)
        self.GrabNEBox = QtGui.QCheckBox("NE")
        self.GrabNEBox.setEnabled(False)
        self.GrabNEBox.clicked.connect(self._select_ne)
        self.GrabTEBox = QtGui.QCheckBox("TE")
        self.GrabTEBox.setEnabled(False)
        self.GrabTEBox.clicked.connect(self._select_te)
        self.GrabTIBox = QtGui.QCheckBox("TI")
        self.GrabTIBox.setEnabled(False)
        self.GrabTIBox.clicked.connect(self._select_ti)
        self.GrabNIMPBox = QtGui.QCheckBox("NIMP")
        self.GrabNIMPBox.setEnabled(False)
        self.GrabNIMPBox.clicked.connect(self._select_nimp)
        self.GrabANGFBox = QtGui.QCheckBox("ANGF")
        self.GrabANGFBox.setEnabled(False)
        self.GrabANGFBox.clicked.connect(self._select_angf)
        self.GrabQBox = QtGui.QCheckBox("Q")
        self.GrabQBox.setEnabled(False)
        self.GrabQBox.clicked.connect(self._select_q)

        qgbox = QtGui.QGridLayout()
        qgbox.addWidget(self.GrabNEBox,1,0)
        qgbox.addWidget(self.GrabTEBox,1,1)
        qgbox.addWidget(self.GrabTIBox,1,2)
        qgbox.addWidget(self.GrabNIMPBox,2,0)
        qgbox.addWidget(self.GrabANGFBox,2,1)
        qgbox.addWidget(self.GrabQBox,2,2)

        self.OperationLabel = QtGui.QLabel("Operation")
        self.OperationLabel.setEnabled(False)
        self.ApplyShiftBox = QtGui.QCheckBox("Shift")
        self.ApplyShiftBox.setEnabled(False)
        self.ApplyShiftBox.clicked.connect(self._select_shift)
        self.ApplyScaleBox = QtGui.QCheckBox("Scale")
        self.ApplyScaleBox.setEnabled(False)
        self.ApplyScaleBox.clicked.connect(self._select_scale)
        self.ApplySigmaBox = QtGui.QCheckBox("Sigma")
        self.ApplySigmaBox.setEnabled(False)
        self.ApplySigmaBox.clicked.connect(self._select_sigma)

        opbox = QtGui.QGridLayout()
        opbox.addWidget(self.ApplyShiftBox,1,0)
        opbox.addWidget(self.ApplyScaleBox,1,1)
        opbox.addWidget(self.ApplySigmaBox,1,2)

        self.WarpFunctionSelectionLabel = QtGui.QLabel("Warp Function:")
        self.WarpFunctionSelectionLabel.setEnabled(False)
        self.WarpFunctionSelectionList = QtGui.QComboBox()
        self.WarpFunctionSelectionList.setEnabled(False)
        self.WarpFunctionSelectionList.addItem("Constant")
        self.WarpFunctionSelectionList.addItem("Linear")
        self.WarpFunctionSelectionList.addItem("Tanh")
        self.WarpFunctionSelectionList.addItem("Tanh + Tanh")
        self.WarpFunctionSelectionList.setCurrentIndex(2)
        self.WarpFunctionSelectionList.currentIndexChanged.connect(self._switch_warp_function)

        self.ConstWarpFunctionSettings = ConstWarpFunctionWidget(False)
        self.LinWarpFunctionSettings = LinWarpFunctionWidget(False)
        self.TanhWarpFunctionSettings = TanhWarpFunctionWidget(False)
        self.DblTanhWarpFunctionSettings = DblTanhWarpFunctionWidget(False)

        self.WarpFunctionSettings = QtGui.QStackedLayout()
        self.WarpFunctionSettings.addWidget(self.ConstWarpFunctionSettings)
        self.WarpFunctionSettings.addWidget(self.LinWarpFunctionSettings)
        self.WarpFunctionSettings.addWidget(self.TanhWarpFunctionSettings)
        self.WarpFunctionSettings.addWidget(self.DblTanhWarpFunctionSettings)
        self.WarpFunctionSettings.setCurrentIndex(self.WarpFunctionSelectionList.currentIndex())

        self.MinimumValueLabel = QtGui.QLabel("Min. Value:")
        self.MinimumValueLabel.setEnabled(False)
        self.MinimumValueEntry = QtGui.QLineEdit("0.0e0")
        self.MinimumValueEntry.setEnabled(False)
        self.MinimumValueEntry.setValidator(QtGui.QDoubleValidator(-np.Inf,np.Inf,100,None))

        wfbox = QtGui.QFormLayout()
        wfbox.addRow(self.WarpFunctionSelectionLabel,self.WarpFunctionSelectionList)
        wfbox.addRow(self.WarpFunctionSettings)
        wfbox.addRow(self.MinimumValueLabel,self.MinimumValueEntry)

        self.ViewModButton = QtGui.QPushButton("View")
        self.ViewModButton.setEnabled(False)
        self.ViewModButton.clicked.connect(self._plot_profile)
        self.ApplyModButton = QtGui.QPushButton("Apply")
        self.ApplyModButton.setEnabled(False)
        self.ApplyModButton.clicked.connect(self._apply_profile)

        mpbox = QtGui.QHBoxLayout()
        mpbox.addWidget(self.ViewModButton)
        mpbox.addWidget(self.ApplyModButton)

        self.SpecifyShotBox = QtGui.QCheckBox("Specify Profile Metadata")
        self.SpecifyShotBox.setEnabled(False)
        self.SpecifyShotBox.clicked.connect(self._enable_shot_specification)
        self.ShotNumberLabel = QtGui.QLabel("Shot Number")
        self.ShotNumberLabel.setEnabled(False)
        self.ShotNumberEntry = QtGui.QLineEdit()
        self.ShotNumberEntry.setEnabled(False)
        self.ShotNumberEntry.setValidator(QtGui.QIntValidator(None))
        self.ShotNumberEntry.setMaxLength(6)
        self.ShotNumberEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeStartLabel = QtGui.QLabel("Time Start (s)")
        self.TimeStartLabel.setEnabled(False)
        self.TimeStartEntry = QtGui.QLineEdit()
        self.TimeStartEntry.setEnabled(False)
        self.TimeStartEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeStartEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeEndLabel = QtGui.QLabel("Time End (s)")
        self.TimeEndLabel.setEnabled(False)
        self.TimeEndEntry = QtGui.QLineEdit()
        self.TimeEndEntry.setEnabled(False)
        self.TimeEndEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeEndEntry.setAlignment(QtCore.Qt.AlignLeft)

        ssbox = QtGui.QFormLayout()
        ssbox.addRow(self.ShotNumberLabel,self.ShotNumberEntry)
        ssbox.addRow(self.TimeStartLabel,self.TimeStartEntry)
        ssbox.addRow(self.TimeEndLabel,self.TimeEndEntry)

        self.SaveProfilesButton = QtGui.QPushButton("Save Modification(s) in Text")
        self.SaveProfilesButton.setEnabled(False)
        self.SaveProfilesButton.clicked.connect(self._save_profiles)
        self.WritePPFButton = QtGui.QPushButton("Save Modification(s) in PPF")
        self.WritePPFButton.setEnabled(False)
        self.WritePPFButton.clicked.connect(self._write_profiles_to_ppf)

        dobox = QtGui.QVBoxLayout()
        dobox.addLayout(mpbox)
        dobox.addWidget(self.SpecifyShotBox)
        dobox.addLayout(ssbox)
        dobox.addWidget(self.SaveProfilesButton)
        if self.ppfflag:
            dobox.addWidget(self.WritePPFButton)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.LoadTextDataButton)
        vbox.addWidget(self.LoadPickleDataButton)
        vbox.addItem(QtGui.QSpacerItem(0,10,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        vbox.addWidget(self.QuantityLabel)
        vbox.addLayout(qgbox)
        vbox.addItem(QtGui.QSpacerItem(0,10,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        vbox.addWidget(self.OperationLabel)
        vbox.addLayout(opbox)
        vbox.addItem(QtGui.QSpacerItem(0,10,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        vbox.addLayout(wfbox)
        vbox.addItem(QtGui.QSpacerItem(0,10,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum))
        vbox.addLayout(dobox)

        pfig = matplotlib.figure.Figure(figsize=(10,5))
        pax = pfig.add_subplot(111)
        pax.set_xlim([0.0,1.0])
        pax.set_ylim([0.0,1.0])
        pax.ticklabel_format(style='sci',axis='both',scilimits=(-2,2))
        self.PlotCanvas = mplqt.FigureCanvasQTAgg(pfig)
        self.PlotCanvas.figure.patch.set_facecolor('w')
        self.PlotCanvas.figure.tight_layout()
        wfig = matplotlib.figure.Figure(figsize=(10,5))
        wax = wfig.add_subplot(111)
        wax.set_xlim([0.0,1.0])
        wax.set_ylim([-1.0,1.0])
        wax.ticklabel_format(style='sci',axis='both',scilimits=(-2,2))
        self.WarpCanvas = mplqt.FigureCanvasQTAgg(wfig)
        self.WarpCanvas.figure.patch.set_facecolor('w')
        self.WarpCanvas.figure.tight_layout()

        pbox = QtGui.QVBoxLayout()
        pbox.addWidget(self.PlotCanvas)
        pbox.addWidget(self.WarpCanvas)

        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(vbox)
        hbox.addLayout(pbox)

        self.setLayout(hbox)

        self.setGeometry(20, 20, 1200, 700)
        self.setWindowTitle("Profile Modification GUI (JET)")
        self.show()

    def _set_profiles(self):

        if isinstance(self.pdata,dict):
            self.QuantityLabel.setEnabled(True)
            self.GrabNEBox.setEnabled("PD_NE" in self.pdata and self.pdata["PD_NE"] is not None)
            if not self.GrabNEBox.isEnabled():
                self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setEnabled("PD_TE" in self.pdata and self.pdata["PD_TE"] is not None)
            if not self.GrabTEBox.isEnabled():
                self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setEnabled("PD_TI1" in self.pdata and self.pdata["PD_TI1"] is not None)
            if not self.GrabTIBox.isEnabled():
                self.GrabTIBox.setChecked(False)
            self.GrabNIMPBox.setEnabled("PD_NIMP1" in self.pdata and self.pdata["PD_NIMP1"] is not None)
            if not self.GrabNIMPBox.isEnabled():
                self.GrabNIMPBox.setChecked(False)
            self.GrabANGFBox.setEnabled("PD_AFTOR" in self.pdata and self.pdata["PD_AFTOR"] is not None)
            if not self.GrabANGFBox.isEnabled():
                self.GrabANGFBox.setChecked(False)
            self.GrabQBox.setEnabled("PD_Q" in self.pdata and self.pdata["PD_Q"] is not None)
            if not self.GrabQBox.isEnabled():
                self.GrabQBox.setChecked(False)
        else:
            self.QuantityLabel.setEnabled(False)
            self.GrabNEBox.setEnabled(False)
            self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setEnabled(False)
            self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setEnabled(False)
            self.GrabTIBox.setChecked(False)
            self.GrabNIMPBox.setEnabled(False)
            self.GrabNIMPBox.setChecked(False)
            self.GrabANGFBox.setEnabled(False)
            self.GrabANGFBox.setChecked(False)
            self.GrabQBox.setEnabled(False)
            self.GrabQBox.setChecked(False)
        self._set_operations()

    def _set_operations(self):

        if isinstance(self.pdata,dict):
            qcheck = (self.GrabNEBox.isChecked() or self.GrabTEBox.isChecked() or \
                      self.GrabTIBox.isChecked() or self.GrabNIMPBox.isChecked() or \
                      self.GrabANGFBox.isChecked() or self.GrabQBox.isChecked())
            self.OperationLabel.setEnabled(qcheck)
            self.ApplyShiftBox.setEnabled(qcheck)
            self.ApplyScaleBox.setEnabled(qcheck)
            self.ApplySigmaBox.setEnabled(qcheck)
            self.ViewModButton.setEnabled(qcheck)
        else:
            self.OperationLabel.setEnabled(False)
            self.ApplyShiftBox.setEnabled(False)
            self.ApplyShiftBox.setChecked(False)
            self.ApplyScaleBox.setEnabled(False)
            self.ApplyScaleBox.setChecked(False)
            self.ApplySigmaBox.setEnabled(False)
            self.ApplySigmaBox.setChecked(False)
            self.ViewModButton.setEnabled(False)
        self._set_warp_functions()

    def _set_warp_functions(self):

        if isinstance(self.pdata,dict):
            ocheck = (self.ApplyShiftBox.isChecked() or self.ApplyScaleBox.isChecked() or self.ApplySigmaBox.isChecked())
            self.WarpFunctionSelectionLabel.setEnabled(ocheck)
            self.WarpFunctionSelectionList.setEnabled(ocheck)
            self.WarpFunctionSettings.setEnabled(ocheck)
            for ii in np.arange(0,self.WarpFunctionSettings.count()):
                self.WarpFunctionSettings.widget(ii).toggle_all(ocheck)
            self.MinimumValueLabel.setEnabled(ocheck)
            self.MinimumValueEntry.setEnabled(ocheck)
            self.ApplyModButton.setEnabled(ocheck)
        else:
            self.WarpFunctionSelectionLabel.setEnabled(False)
            self.WarpFunctionSelectionList.setEnabled(False)
            self.WarpFunctionSettings.setEnabled(False)
            for ii in np.arange(0,self.WarpFunctionSettings.count()):
                self.WarpFunctionSettings.widget(ii).toggle_all(False)
            self.MinimumValueLabel.setEnabled(False)
            self.MinimumValueEntry.setEnabled(False)
            self.ApplyModButton.setEnabled(False)

    def _enable_save_functions(self):

        self.SpecifyShotBox.setEnabled(isinstance(self.pdata,dict))
        self.SaveProfilesButton.setEnabled(isinstance(self.pdata,dict))
        self.WritePPFButton.setEnabled(isinstance(self.pdata,dict))

    def _enable_shot_specification(self):

        self.ShotNumberLabel.setEnabled(self.SpecifyShotBox.isEnabled() and self.SpecifyShotBox.isChecked())
        self.ShotNumberEntry.setEnabled(self.SpecifyShotBox.isEnabled() and self.SpecifyShotBox.isChecked())
        self.TimeStartLabel.setEnabled(self.SpecifyShotBox.isEnabled() and self.SpecifyShotBox.isChecked())
        self.TimeStartEntry.setEnabled(self.SpecifyShotBox.isEnabled() and self.SpecifyShotBox.isChecked())
        self.TimeEndLabel.setEnabled(self.SpecifyShotBox.isEnabled() and self.SpecifyShotBox.isChecked())
        self.TimeEndEntry.setEnabled(self.SpecifyShotBox.isEnabled() and self.SpecifyShotBox.isChecked())

    def _load_text_data(self):

        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '', 'Text files (*.txt);;All files (*)')
        if filename:
            retval = QtGui.QMessageBox.Yes
            if isinstance(self.pdata,dict):
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Question)
                msg.setWindowTitle("Profile Data Exists")
                msg.setText("Profile data already loaded, conflicting profiles will be overwritten with new data. Proceed anyway?")
                msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                retval = msg.exec_()
            if retval == QtGui.QMessageBox.Yes:
                tdata = rtxt.read_data_file(filename)
                if isinstance(tdata,dict) and tdata:
                    self.pdata = dict()
                    if "META_SHOT" not in self.pdata:
                        self.pdata["META_SHOT"] = tdata["META_SHOT"] if "META_SHOT" in tdata else None
                    if "META_CSO" not in self.pdata:
                        self.pdata["META_CSO"] = tdata["META_CSO"] if "META_CSO" in tdata else "RHOTORN"
                    if "META_T1" not in self.pdata:
                        self.pdata["META_T1"] = tdata["META_T1"] if "META_T1" in tdata else None
                    if "META_T2" not in self.pdata:
                        self.pdata["META_T2"] = tdata["META_T2"] if "META_T2" in tdata else None
                    quantities = ["NE","TE","TI1","NIMP1","AFTOR","Q","ZEFF"]
                    for qtag in quantities:
                        if "PD_"+qtag in tdata:
                            pxx = copy.deepcopy(tdata["PD_"+qtag+"X"])
                            pyy = copy.deepcopy(tdata["PD_"+qtag])
                            pye = copy.deepcopy(tdata["PD_"+qtag+"EB"]) if "PD_"+qtag+"EB" in tdata else np.zeros(pxx.shape)
#                            pdx = copy.deepcopy(tdata["PD_"+qtag+"XGRAD"]) if "PD_"+qtag+"XGRAD" in tdata else None
#                            pdy = copy.deepcopy(tdata["PD_"+qtag+"GRAD"]) if "PD_"+qtag+"GRAD" in tdata else None
                            if "PD_"+qtag+"X" in self.pdata and "PD_"+qtag in self.pdata:
                                ixvec = self.pdata["PD_"+qtag+"X"].copy()
                                yfunc = interp1d(pxx,pyy,bounds_error=False,fill_value='extrapolate')
                                pyy = yfunc(ixvec)
                                efunc = interp1d(pxx,pye,bounds_error=False,fill_value='extrapolate')
                                pye = efunc(ixvec)
                            self.pdata["PD_"+qtag+"X"] = copy.deepcopy(pxx)
                            self.pdata["PD_"+qtag] = copy.deepcopy(pyy)
                            self.pdata["PD_"+qtag+"EB"] = copy.deepcopy(pye)
                            if "PD_X" not in self.pdata:
                                self.pdata["PD_X"] = copy.deepcopy(self.pdata["PD_"+qtag+"X"])
                    self.lastmod = None
                    self.odata = copy.deepcopy(self.pdata)
                    self._set_profiles()
                self._enable_save_functions()

    def _load_pickle_data(self):

        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '', 'Pickle files (*.pkl)')
        if filename:
            retval = QtGui.QMessageBox.Yes
            if isinstance(self.pdata,dict):
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Question)
                msg.setWindowTitle("Profile Data Exists")
                msg.setText("Profile data already loaded, conflicting profiles will be overwritten with new data. Proceed anyway?")
                msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                retval = msg.exec_()
            if retval == QtGui.QMessageBox.Yes:
                bdata = ptools.unpickle_this(filename)
                dstr = None
                flag = False
                if isinstance(bdata,(dict,collections.OrderedDict)) and "META_SHOT" not in bdata:
                    twlist = []
                    for snum in bdata:
                        for tt in np.arange(0,len(bdata[snum])):
                            sdata = bdata[snum][tt]
                            if isinstance(sdata,dict) and "META_SHOT" in sdata and sdata["META_SHOT"] is not None and "META_T1" in sdata and sdata["META_T1"] is not None and "META_T2" in sdata and sdata["META_T2"] is not None:
                                twlist.append("%d/%d : %8.4f -- %8.4f" % (sdata["META_SHOT"],tt,sdata["META_T1"],sdata["META_T2"]))
                    (dstr,flag) = QtGui.QInputDialog.getItem(self, 'Select Time Window', '', twlist, 0, False)
                elif isinstance(bdata,(dict,collections.OrderedDict)) and "META_SHOT" in bdata and bdata["META_SHOT"] is not None and "META_T1" in bdata and bdata["META_T1"] is not None and "META_T2" in bdata and bdata["META_T2"] is not None:
                    dstr = "TOP"
                    flag = True
                if isinstance(dstr,str) and dstr and flag:
                    tdata = None
                    mm = re.search(r'^([0-9]+)/([0-9]+)\s+:',dstr)
                    if mm:
                        snum = int(mm.group(1))
                        twnum = int(mm.group(2))
                        tdata = bdata[snum][twnum]
                    elif re.match(r'^TOP$',dstr,flags=re.IGNORECASE):
                        tdata = bdata
                    if isinstance(tdata,(dict,collections.OrderedDict)) and tdata:
                        self.pdata = dict()
                        if "META_SHOT" not in self.pdata:
                            self.pdata["META_SHOT"] = tdata["META_SHOT"] if "META_SHOT" in tdata else None
                        if "META_CSO" not in self.pdata:
                            self.pdata["META_CSO"] = tdata["META_CSO"] if "META_CSO" in tdata else "RHOTORN"
                        if "META_T1" not in self.pdata:
                            self.pdata["META_T1"] = tdata["META_T1"] if "META_T1" in tdata else None
                        if "META_T2" not in self.pdata:
                            self.pdata["META_T2"] = tdata["META_T2"] if "META_T2" in tdata else None
                        quantities = ["NE","TE","TI1","NIMP1","AFTOR","Q","ZEFF"]
                        for qtag in quantities:
                            if "PD_"+qtag in tdata:
                                pxx = copy.deepcopy(tdata["PD_X"])
                                pyy = copy.deepcopy(tdata["PD_"+qtag])
                                pye = copy.deepcopy(tdata["PD_"+qtag+"EB"]) if "PD_"+qtag+"EB" in tdata else np.zeros(pxx.shape)
                                if "PD_"+qtag+"X" in self.pdata and "PD_"+qtag in self.pdata:
                                    ixvec = self.pdata["PD_"+qtag+"X"].copy()
                                    yfunc = interp1d(pxx,pyy,bounds_error=False,fill_value='extrapolate')
                                    pyy = yfunc(ixvec)
                                    efunc = interp1d(pxx,pye,bounds_error=False,fill_value='extrapolate')
                                    pye = efunc(ixvec)
                                self.pdata["PD_"+qtag+"X"] = copy.deepcopy(pxx)
                                self.pdata["PD_"+qtag] = copy.deepcopy(pyy)
                                self.pdata["PD_"+qtag+"EB"] = copy.deepcopy(pye)
                                if "PD_X" not in self.pdata:
                                    self.pdata["PD_X"] = copy.deepcopy(self.pdata["PD_"+qtag+"X"])
                        self.lastmod = None
                        self.odata = copy.deepcopy(self.pdata)
                        self._set_profiles()
                    self._enable_save_functions()

    def _select_ne(self):

        if self.GrabNEBox.isChecked():
            self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setChecked(False)
            self.GrabNIMPBox.setChecked(False)
            self.GrabANGFBox.setChecked(False)
            self.GrabQBox.setChecked(False)
        self._set_operations()

    def _select_te(self):

        if self.GrabTEBox.isChecked():
            self.GrabNEBox.setChecked(False)
            self.GrabTIBox.setChecked(False)
            self.GrabNIMPBox.setChecked(False)
            self.GrabANGFBox.setChecked(False)
            self.GrabQBox.setChecked(False)
        self._set_operations()

    def _select_ti(self):

        if self.GrabTIBox.isChecked():
            self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setChecked(False)
            self.GrabNIMPBox.setChecked(False)
            self.GrabANGFBox.setChecked(False)
            self.GrabQBox.setChecked(False)
        self._set_operations()

    def _select_nimp(self):

        if self.GrabNIMPBox.isChecked():
            self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setChecked(False)
            self.GrabANGFBox.setChecked(False)
            self.GrabQBox.setChecked(False)
        self._set_operations()

    def _select_angf(self):

        if self.GrabANGFBox.isChecked():
            self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setChecked(False)
            self.GrabNIMPBox.setChecked(False)
            self.GrabQBox.setChecked(False)
        self._set_operations()

    def _select_q(self):

        if self.GrabQBox.isChecked():
            self.GrabNEBox.setChecked(False)
            self.GrabTEBox.setChecked(False)
            self.GrabTIBox.setChecked(False)
            self.GrabNIMPBox.setChecked(False)
            self.GrabANGFBox.setChecked(False)
        self._set_operations()

    def _select_shift(self):

        if self.ApplyShiftBox.isChecked():
            self.ApplyScaleBox.setChecked(False)
            self.ApplySigmaBox.setChecked(False)
        self._set_warp_functions()

    def _select_scale(self):

        if self.ApplyScaleBox.isChecked():
            self.ApplyShiftBox.setChecked(False)
            self.ApplySigmaBox.setChecked(False)
        self._set_warp_functions()

    def _select_sigma(self):

        if self.ApplySigmaBox.isChecked():
            self.ApplyShiftBox.setChecked(False)
            self.ApplyScaleBox.setChecked(False)
        self._set_warp_functions()

    def _switch_warp_function(self):

        self.WarpFunctionSettings.setCurrentIndex(self.WarpFunctionSelectionList.currentIndex())

    def _warp_profile(self):

        mdata = None
        wdata = None
        qtag = None
        mtype = None
        if isinstance(self.pdata,dict):
            data = copy.deepcopy(self.pdata)
            if self.GrabNEBox.isChecked():
                qtag = "NE"
            if self.GrabTEBox.isChecked():
                qtag = "TE"
            if self.GrabTIBox.isChecked():
                qtag = "TI1"
            if self.GrabNIMPBox.isChecked():
                qtag = "NIMP1"
            if self.GrabANGFBox.isChecked():
                qtag = "AFTOR"
            if self.GrabQBox.isChecked():
                qtag = "Q"
            if qtag is not None and "PD_"+qtag in data:
                if self.ApplyShiftBox.isChecked():
                    mtype = 'shift'
                if self.ApplyScaleBox.isChecked():
                    mtype = 'scale'
                if self.ApplySigmaBox.isChecked():
                    mtype = 'sigma'
                if mtype is not None:
                    wfname = self.WarpFunctionSettings.widget(self.WarpFunctionSelectionList.currentIndex()).get_name()
                    wfpars = self.WarpFunctionSettings.widget(self.WarpFunctionSelectionList.currentIndex()).get_parameters()
                    mtext = self.MinimumValueEntry.text()
                    minval = float(mtext) if mtext else None
                    pxx = data["PD_"+qtag+"X"]
                    pyy = data["PD_"+qtag]
                    pss = data["PD_"+qtag+"EB"] if "PD_"+qtag+"EB" in data else np.zeros(pxx.shape)
                    wxx = pxx.copy()
                    wyy = np.ones(pxx.shape) if mtype == 'scale' else np.zeros(pxx.shape)
                    wss = np.ones(pxx.shape) if mtype == 'sigma' else np.zeros(pxx.shape)
                    wdata = pwarp.modify_profile(xinput=wxx,yinput=wyy,sinput=wss,warpname=wfname,parlist=wfpars,modtype=mtype,plotflag=False)
                    mdata = pwarp.modify_profile(xinput=pxx,yinput=pyy,sinput=pss,warpname=wfname,parlist=wfpars,minval=minval,modtype=mtype,plotflag=False)
        return (mdata, wdata, qtag, mtype)

    def _plot_profile(self):

        if isinstance(self.pdata,dict):
            data = copy.deepcopy(self.pdata)
            (mdata, wdata, qtag, mtype) = self._warp_profile()
            if qtag is not None and mtype is not None:
                sigma = 1.0
                pfig = self.PlotCanvas.figure
                pfig.clear()
                pax = pfig.add_subplot(111)
                xfit = data["PD_"+qtag+"X"].flatten()
                yfit = data["PD_"+qtag].flatten()
                yefit = data["PD_"+qtag+"EB"].flatten()
                pax.plot(xfit,yfit,ls='-',color='g')
                ylower = yfit - sigma * yefit
                yupper = yfit + sigma * yefit
                pax.fill_between(xfit,ylower,yupper,facecolor='g',edgecolor='None',alpha=0.2)
                xmod = None
                ymod = None
                if mdata is not None:
                    xmod = data["PD_"+qtag+"X"].flatten()
                    ymod = mdata.flatten()
                    pax.plot(xmod,ymod,ls='-',color='r')
                    self.lastmod = (qtag,mtype,mdata)
                ymin = np.amin([np.amin(yfit-sigma*yefit),np.amin(ymod)]) if ymod is not None else np.amin(yfit-sigma*yefit)
                ymax = np.amax([np.amax(yfit+sigma*yefit),np.amax(ymod)]) if ymod is not None else np.amax(yfit+sigma*yefit)
                ybuf = 0.025 * np.abs(ymax - ymin)
                if ybuf > 0.0:
                    pax.set_ylim([ymin-ybuf,ymax+ybuf])
                pax.ticklabel_format(style='sci',axis='both',scilimits=(-2,2))
                self.PlotCanvas.draw()
                wfig = self.WarpCanvas.figure
                wfig.clear()
                wax = wfig.add_subplot(111)
                xwarp = None
                ywarp = None
                if wdata is not None:
                    xwarp = data["PD_"+qtag+"X"].flatten()
                    ywarp = wdata.flatten()
                    wax.plot(xwarp,ywarp,ls='-',color='b')
                ymin = -1.0 
                ymax = 1.0
                if ywarp is not None and not (np.abs(np.sum(ywarp)) < 1.0e-6 and np.abs(np.sum(np.cumsum(ywarp))) < 1.0e-6):
                    ymin = np.amin(ywarp)
                    ymax = np.amax(ywarp)
                ybuf = 0.025 * np.abs(ymax - ymin)
                if ybuf > 0.0:
                    pax.set_ylim([ymin-ybuf,ymax+ybuf])
                wax.ticklabel_format(style='sci',axis='both',scilimits=(-2,2))
                self.WarpCanvas.draw()

    def _apply_profile(self):

        if isinstance(self.pdata,dict) and self.lastmod is not None:
            qtag = self.lastmod[0]
            mtype = self.lastmod[1]
            mdata = self.lastmod[2]
            self.pdata["PD_"+qtag] = copy.deepcopy(mdata)
            self.pdata["PD_"+qtag+"EB"] = np.zeros(mdata.shape)

    def _save_profiles(self):

        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if filename and isinstance(self.pdata,dict):
            snum = self.pdata["META_SHOT"] if "META_SHOT" in self.pdata and self.pdata["META_SHOT"] is not None else -1
            if self.SpecifyShotBox.isChecked():
                textn = self.ShotNumberEntry.text()
                if textn:
                    snum = int(textn)
            ocs = self.pdata["META_CSO"] if "META_CSO" in self.pdata else "RHOTORN"
            tbeg = self.pdata["META_T1"] if "META_T1" in self.pdata and self.pdata["META_T1"] is not None else -1.0
            if self.SpecifyShotBox.isChecked():
                text1 = self.TimeStartEntry.text()
                if text1:
                    tbeg = float(text1)
            tend = self.pdata["META_T2"] if "META_T2" in self.pdata and self.pdata["META_T2"] is not None else -1.0
            if self.SpecifyShotBox.isChecked():
                text2 = self.ShotNumberEntry.text()
                if text2:
                    tend = float(text2)
            with open(filename,'w') as ff:
                if self.pdata:
                    ff.write("### EX2GK - Modified Profile Save File ###\n")
                    ff.write("\n")
                    ff.write("START OF HEADER\n")
                    ff.write("\n")
                    ff.write("        Shot Number: %20d\n" % (snum))
                    ff.write("  Radial Coordinate: %20s\n" % (ocs))
                    ff.write("  Time Window Start: %18.6f s\n" % (tbeg))
                    ff.write("    Time Window End: %18.6f s\n" % (tend))
                    ff.write("\n")
                    ff.write("END OF HEADER\n")
                    ff.write("\n")
                quantities = ["NE","TE","TI1","NIMP1","AFTOR","Q"]
                for qq in np.arange(0,len(quantities)):
                    if "PD_"+quantities[qq] in self.pdata:
                        xfit = self.pdata["PD_"+quantities[qq]+"X"].flatten()
                        yfit = self.pdata["PD_"+quantities[qq]].flatten()
                        yefit = self.pdata["PD_"+quantities[qq]+"EB"].flatten()
                        ff.write("%15s%15s%15s\n" % (ocs,quantities[qq]+" Mod","Err. "+quantities[qq]))
                        for ii in np.arange(0,xfit.size):
                            ff.write("%15.4f%15.6e%15.6e\n" % (xfit[ii],yfit[ii],yefit[ii]))
                        ff.write("\n")
            print("Modified profile data written into %s." % (filename))

    def _write_profiles_to_ppf(self):

        dda = "PRFL"
        vals = ["NE","TE","TI1","AFTOR","Q"]
#        vals = ["NE","TE","TI","ANGF","Q"]
        if isinstance(self.pdata,dict):
            snum = self.pdata["META_SHOT"] if "META_SHOT" in self.pdata and self.pdata["META_SHOT"] is not None else None
            if self.SpecifyShotBox.isChecked():
                textn = self.ShotNumberEntry.text()
                if textn:
                    snum = int(textn)
            tbeg = self.pdata["META_T1"] if "META_T1" in self.pdata and self.pdata["META_T1"] is not None else None
            if self.SpecifyShotBox.isChecked():
                text1 = self.TimeStartEntry.text()
                if text1:
                    tbeg = float(text1)
            tend = self.pdata["META_T2"] if "META_T2" in self.pdata and self.pdata["META_T2"] is not None else None
            if self.SpecifyShotBox.isChecked():
                text2 = self.TimeEndEntry.text()
                if text2:
                    tend = float(text2)
            print(snum,tbeg,tend)
            if snum is not None and tbeg is not None and tend is not None:
                data = copy.deepcopy(self.pdata)
                pulse = int(snum)
                tval = np.array([tbeg + ((tend - tbeg) / 2.0)])
                uid = pwd.getpwuid(os.getuid())[0]
                print("User ID:",uid,"   ","Pulse No.:",pulse)
                ppf.ppfuid(uid,"W")
                time,date,ier = ppf.pdstd(pulse)
                datestring = datetime.datetime.now().strftime("%B %d, %Y - %X %z")
                ier = ppf.ppfopn(pulse,date,time,"Modified profiles from PWarp: "+datestring)
                print("PPF Open:",ier)

                pcs = data["CS_OUT"] if "CS_OUT" in data else "RHOTORN"
                ocs = "none"
                if re.match("RHOTORN?",pcs,flags=re.IGNORECASE):
                    ocs = "rho_tor"
                    data["SPNT"] = data["PD_X"]
                elif re.match("RHOPOLN?",pcs,flags=re.IGNORECASE):
                    ocs = "rho_pol"
                    data["PSIN"] = np.power(data["PD_X"],2.0)
                elif re.match("TORFLUXN",pcs,flags=re.IGNORECASE):
                    ocs = "psi_tor_norm"
                    data["SPNT"] = np.sqrt(data["PD_X"])
                elif re.match("POLFLUXN",pcs,flags=re.IGNORECASE):
                    ocs = "psi_pol_norm"
                    data["PSIN"] = data["PD_X"]
                elif re.match("TORFLUX",pcs,flags=re.IGNORECASE):
                    ocs = "psi_tor"
                    xmin = np.amin(np.abs(data["PD_X"]))
                    xmax = np.amax(np.abs(data["PD_X"]))
                    data["SPNT"] = np.sqrt((np.abs(data[data["META_CSO"]]) - xmin) / (xmax - xmin))
                elif re.match("POLFLUX",pcs,flags=re.IGNORECASE):
                    ocs = "psi_pol"
                    xmin = np.amin(np.abs(data["PD_X"]))
                    xmax = np.amax(np.abs(data["PD_X"]))
                    data["PSIN"] = (np.abs(data["PD_X"]) - xmin) / (xmax - xmin)
                elif re.match("RMAJORO",pcs,flags=re.IGNORECASE):
                    ocs = "R_lfs"
                    data["RLFS"] = data["PD_X"]

                fwritten = False
                for val in vals:
                    dtyp = None
                    unit = None
                    if re.match(r'^NE$',val):
                        dtyp = "NE"
                        unit = "m**-3"
                    if re.match(r'^TE$',val):
                        dtyp = "TE"
                        unit = "eV"
                    if re.match(r'^TI[0-9]*$',val):
                        dtyp = "TI"
                        unit = "eV"
                    if re.match(r'^NIMP[0-9]*$',val):
                        dtyp = "NX"
                        unit = "m**-3"
                    if re.match(r'^AFTOR$',val):
                        dtyp = "AF"
                        unit = "rad/s"
                    if re.match(r'^Q$',val):
                        dtyp = "Q"
                        unit = ""
#                    if re.match(r'^ZEFF$',val):
#                        dtyp = "Z"
#                        unit = ""

                    if "PD_"+val in data and data["PD_"+val] is not None:
                        xvec = data["PD_"+val+"X"].flatten()
                        indat = data["PD_"+val].flatten()
                        irdat = ppf.ppfwri_irdat(xvec.size,tval.size,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","Modified "+val)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,xvec,tval)
                        print(dtyp+" PPF Write:",ier)
                        if ier == 0:
                            fwritten = True

                        indat = data["PD_"+val].flatten() - data["PD_"+val+"EB"].flatten()
                        ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","Modified "+val+" lower - 1 sigma")
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"LO",irdat,ihdat,indat,xvec,tval)
                        print(dtyp+"LO PPF Write:",ier)

                        indat = data["PD_"+val].flatten() + data["PD_"+val+"EB"].flatten()
                        ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","Modified "+val+" upper - 1 sigma")
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"HI",irdat,ihdat,indat,xvec,tval)
                        print(dtyp+"HI PPF Write:",ier)

                        indat = data["PD_"+val+"EB"].flatten()
                        ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","Modified "+val+" uncertainty - 1 sigma")
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"EB",irdat,ihdat,indat,xvec,tval)
                        print(dtyp+"EB PPF Write:",ier)

                if fwritten:
                    if "SPNT" in data and data["SPNT"] is not None:
                        dtyp = "SPNT"
                        unit = "rho_tor"
                        indat = data["SPNT"]
                        irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                        print(dtyp+" PPF Write:",ier)
                    if "PSIN" in data and data["PSIN"] is not None:
                        dtyp = "PSIN"
                        unit = "psi_pol_norm"
                        indat = data["PSIN"]
                        irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                        print(dtyp+" PPF Write:",ier)
                    if "RLFS" in data and data["RLFS"] is not None:
                        dtyp = "RLFS"
                        unit = "R_lfs"
                        indat = data["RLFS"]
                        irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                        print(dtyp+" PPF Write:",ier)

                seq,ier = ppf.ppfclo(pulse,"PWARP",1)
                print("PPF Sequence:",seq)

                if ier == 0 and seq >= 0:
                    msg = QtGui.QMessageBox()
                    msg.setIcon(QtGui.QMessageBox.Information)
                    msg.setWindowTitle("PPF Write Successful")
                    msg.setText("Data has been written to:\nshot = %6d, uid = %15s, seq = %5d" % (pulse,uid,seq))
                    msg.exec_()
            else:
                msg = QtGui.QMessageBox()
                msg.setIcon(QtGui.QMessageBox.Warning)
                msg.setWindowTitle("Data Error")
                msg.setText("Required metadata is missing. Please specify shot number / time window and try again.")
                msg.exec_()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Required data not loaded. PPF writing aborted.")
            msg.exec_()

def main():

    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('Profile Warper for JET')
    ex = PWarp_JET(ppfIsHere)

    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
