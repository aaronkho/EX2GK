#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

# Required imports
import os
import sys
import socket
import datetime
import re
import pwd
import time
import copy
import pickle
import collections
import importlib
import inspect
import packaging.version as pversion
import numpy as np

ppfIsHere = True
try:
    import ppf
except ImportError:
    ppfIsHere = False

from IPython import embed
QtCore = None
QtGui = None
QtWidgets = None
try:
    from PyQt4 import QtCore, QtGui
    QtWidgets = QtGui
except ImportError:
    from PyQt5 import QtCore, QtGui, QtWidgets
pyqtversion = QtCore.PYQT_VERSION_STR if QtCore is not None else "0"

import matplotlib
old_mpl = pversion.parse(matplotlib.__version__) <= pversion.parse("2.0.0")
fqt5 = pversion.parse(pyqtversion) >= pversion.parse("5.0.0")
mplqt = None
if fqt5:
    matplotlib.use("Qt5Agg")
    from matplotlib.backends import backend_qt5agg as mplqt
else:
    matplotlib.use("Qt4Agg")
    from matplotlib.backends import backend_qt4agg as mplqt
from matplotlib import figure as mplfig

if QtCore is None or QtGui is None or QtWidgets is None or mplqt is None:
    raise ImportError("Python install does not have Qt support! GUI cannot be used!")

from EX2GK import __version__, __location__
from EX2GK.tools.general import proctools as ptools
from EX2GK.machine_adapters.jet import extract_jet_sal as gmod
from EX2GK.machine_adapters.jet import process_jet as pmod
from EX2GK.machine_adapters.jet import standardize_jet as smod
from EX2GK.machine_adapters.jet import time_selector as tmod


currenthost = socket.getfqdn()
currentdir = os.getcwd()
customfilters = None
fcustomincurrent = False
if os.path.isfile(currentdir+'/EX2GK_custom_filters.py'):
    spec = importlib.util.spec_from_file_location('EX2GK_custom_filters', currentdir+'/EX2GK_custom_filters.py')
    customfilters = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(customfilters)
    fcustomincurrent = True
if customfilters is None:
    try:
        customfilters = importlib.import_module('EX2GK_custom_filters')
    except (ImportError, AttributeError):
        try:
            customfilters = importlib.import_module('.EX2GK_custom_filters', package='EX2GK')
        except:
            pass


data_fields = {
    # ELM detection via edge radiation spectra
    "edg7": [("dai",2), ("dao",2), ("be2o",2)],
    "edg8": [("tbeo",2)],
    # Equilibria
    "efit": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("fbnd",1), ("faxs",1), ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",0),
             ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("psi",0), ("psir",0), ("psiz",0), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1),
             ("f",0), ("p",0), ("dfdp",0), ("dpdp",0)],
    "eftp": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("fbnd",1), ("faxs",1), ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",0),
             ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("psi",0), ("psir",0), ("psiz",0), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1),
             ("f",0), ("p",0), ("dfdp",0), ("dpdp",0)],
    "eftf": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("fbnd",1), ("faxs",1), ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",0),
             ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("psi",0), ("psir",0), ("psiz",0), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1),
             ("f",0), ("p",0), ("dfdp",0), ("dpdp",0)],
    "eftm": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("fbnd",1), ("faxs",1), ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",0),
             ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("psi",0), ("psir",0), ("psiz",0), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1),
             ("f",0), ("p",0), ("dfdp",0), ("dpdp",0)],
    "ehtr": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("fbnd",1), ("faxs",1), ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",0),
             ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("psi",0), ("psir",0), ("psiz",0), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1),
             ("f",0), ("p",0), ("dfdp",0), ("dpdp",0)],
    # Diagnostic data
    "hrts": [("rmid",0), ("z",2), ("ne",1), ("dne",1), ("te",1), ("dte",1)],
    "hrtx": [("lid3",0)],
    "kg1v": [("lid3",0)],
    "lidr": [("rmid",0), ("z",2), ("ne",1), ("nel",1), ("neu",1), ("te",1), ("tel",1), ("teu",1)],
    "kg10": [("r",0), ("z",2), ("ne",1)],
    "refl": [("nepr",1), ("xvec",1)],
    "kk3" : [("rc01",2), ("ra01",2), ("te01",1), ("rc02",2), ("ra02",2), ("te02",1), ("rc03",2), ("ra03",2), ("te03",1), ("rc04",2), ("ra04",2), ("te04",1),
             ("rc05",2), ("ra05",2), ("te05",1), ("rc06",2), ("ra06",2), ("te06",1), ("rc07",2), ("ra07",2), ("te07",1), ("rc08",2), ("ra08",2), ("te08",1),
             ("rc09",2), ("ra09",2), ("te09",1), ("rc10",2), ("ra10",2), ("te10",1), ("rc11",2), ("ra11",2), ("te11",1), ("rc12",2), ("ra12",2), ("te12",1),
             ("rc13",2), ("ra13",2), ("te13",1), ("rc14",2), ("ra14",2), ("te14",1), ("rc15",2), ("ra15",2), ("te15",1), ("rc16",2), ("ra16",2), ("te16",1),
             ("rc17",2), ("ra17",2), ("te17",1), ("rc18",2), ("ra18",2), ("te18",1), ("rc19",2), ("ra19",2), ("te19",1), ("rc20",2), ("ra20",2), ("te20",1),
             ("rc21",2), ("ra21",2), ("te21",1), ("rc22",2), ("ra22",2), ("te22",1), ("rc23",2), ("ra23",2), ("te23",1), ("rc24",2), ("ra24",2), ("te24",1),
             ("rc25",2), ("ra25",2), ("te25",1), ("rc26",2), ("ra26",2), ("te26",1), ("rc27",2), ("ra27",2), ("te27",1), ("rc28",2), ("ra28",2), ("te28",1),
             ("rc29",2), ("ra29",2), ("te29",1), ("rc30",2), ("ra30",2), ("te30",1), ("rc31",2), ("ra31",2), ("te31",1), ("rc32",2), ("ra32",2), ("te32",1),
             ("rc33",2), ("ra33",2), ("te33",1), ("rc34",2), ("ra34",2), ("te34",1), ("rc35",2), ("ra35",2), ("te35",1), ("rc36",2), ("ra36",2), ("te36",1),
             ("rc37",2), ("ra37",2), ("te37",1), ("rc38",2), ("ra38",2), ("te38",1), ("rc39",2), ("ra39",2), ("te39",1), ("rc40",2), ("ra40",2), ("te40",1),
             ("rc41",2), ("ra41",2), ("te41",1), ("rc42",2), ("ra42",2), ("te42",1), ("rc43",2), ("ra43",2), ("te43",1), ("rc44",2), ("ra44",2), ("te44",1),
             ("rc45",2), ("ra45",2), ("te45",1), ("rc46",2), ("ra46",2), ("te46",1), ("rc47",2), ("ra47",2), ("te47",1), ("rc48",2), ("ra48",2), ("te48",1),
             ("rc49",2), ("ra49",2), ("te49",1), ("rc50",2), ("ra50",2), ("te50",1), ("rc51",2), ("ra51",2), ("te51",1), ("rc52",2), ("ra52",2), ("te52",1),
             ("rc53",2), ("ra53",2), ("te53",1), ("rc54",2), ("ra54",2), ("te54",1), ("rc55",2), ("ra55",2), ("te55",1), ("rc56",2), ("ra56",2), ("te56",1),
             ("rc57",2), ("ra57",2), ("te57",1), ("rc58",2), ("ra58",2), ("te58",1), ("rc59",2), ("ra59",2), ("te59",1), ("rc60",2), ("ra60",2), ("te60",1),
             ("rc61",2), ("ra61",2), ("te61",1), ("rc62",2), ("ra62",2), ("te62",1), ("rc63",2), ("ra63",2), ("te63",1), ("rc64",2), ("ra64",2), ("te64",1),
             ("rc65",2), ("ra65",2), ("te65",1), ("rc66",2), ("ra66",2), ("te66",1), ("rc67",2), ("ra67",2), ("te67",1), ("rc68",2), ("ra68",2), ("te68",1),
             ("rc69",2), ("ra69",2), ("te69",1), ("rc70",2), ("ra70",2), ("te70",1), ("rc71",2), ("ra71",2), ("te71",1), ("rc72",2), ("ra72",2), ("te72",1),
             ("rc73",2), ("ra73",2), ("te73",1), ("rc74",2), ("ra74",2), ("te74",1), ("rc75",2), ("ra75",2), ("te75",1), ("rc76",2), ("ra76",2), ("te76",1),
             ("rc77",2), ("ra77",2), ("te77",1), ("rc78",2), ("ra78",2), ("te78",1), ("rc79",2), ("ra79",2), ("te79",1), ("rc80",2), ("ra80",2), ("te80",1),
             ("rc81",2), ("ra81",2), ("te81",1), ("rc82",2), ("ra82",2), ("te82",1), ("rc83",2), ("ra83",2), ("te83",1), ("rc84",2), ("ra84",2), ("te84",1),
             ("rc85",2), ("ra85",2), ("te85",1), ("rc86",2), ("ra86",2), ("te86",1), ("rc87",2), ("ra87",2), ("te87",1), ("rc88",2), ("ra88",2), ("te88",1),
             ("rc89",2), ("ra89",2), ("te89",1), ("rc90",2), ("ra90",2), ("te90",1), ("rc91",2), ("ra91",2), ("te91",1), ("rc92",2), ("ra92",2), ("te92",1),
             ("rc93",2), ("ra93",2), ("te93",1), ("rc94",2), ("ra94",2), ("te94",1), ("rc95",2), ("ra95",2), ("te95",1), ("rc96",2), ("ra96",2), ("te96",1)],
    "ecm1": [("prfl",1)],
    "ecm2": [("tece",1), ("tecl",1), ("tech",1), ("pece",1)],
    "cxsm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxdm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxfm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxgm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxhm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxjm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxkm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxlm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxs4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxd4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxf4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxg4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxh4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxs6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxd6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxf6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxg6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxh6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxs8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxd8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxf8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxg8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxh8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cx7a": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cx7b": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cx7c": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cx7d": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1), ("dens",1), ("conc",1), ("colo",1), ("cohi",1), ("angf",1), ("aflo",1), ("afhi",1)],
    "cxse": [("rt",0), ("z",2), ("mass",2), ("ti",1), ("tie",1), ("nz",1), ("nze",1), ("angf",1), ("ange",1)],
    "wsxp": [("lzel",3), ("melz", 1), ("lzav",1), ("ozel",3), ("meoz", 1), ("ozav",1), ("hzel",3), ("mehz", 1), ("hzav",1), ("zeff",1)]
}

class QTableWidgetTimeWindow(QtWidgets.QTableWidgetItem):

    def __init__(self, shot_number, time_initial, time_final, index):
        dtext = "%d : %8.4f -- %8.4f" % (int(shot_number), float(time_initial), float(time_final))
        super(QTableWidgetTimeWindow,self).__init__(dtext)
        self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self._shot = int(shot_number)
        self._ti = float(time_initial)
        self._tf = float(time_final)
        self._tw = int(index) if int(index) >= 0 else -1
        self._datalist = None
        self._data = None

    def __eq__(self, other):
        if (isinstance(other, self.__class__)):
            return (self._shot == other._shot and self._ti == other._ti and self._tf == other._tf)
        else:
            return super(QTableWidgetTimeWindow,self).__eq__(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_data_list(self):
        return self._datalist

    def get_data(self):
        return self._data

    def get_shot_number(self):
        return self._shot

    def get_time_window_start(self):
        return self._ti

    def get_time_window_end(self):
        return self._tf

    def get_time_window_number(self):
        return self._tw

    def set_data_list(self, gdata):
        if isinstance(gdata, dict):
            self._datalist = copy.deepcopy(gdata)

    def set_data(self, edata):
        if isinstance(edata, dict):
            self._data = copy.deepcopy(edata)

    def set_time_window_number(self, index):
        if int(index) >= 0:
            self._tw = int(index)


class GenericPlotter(QtWidgets.QWidget):

    def __init__(self, data):
        super(GenericPlotter, self).__init__()
        self.data = None
        self.datalist = None
        self.canvas = None
        self.points = None
        self.press = None
        self.rect = None
        self.rectimage = None
        self.bg = None
        self.xlims = None
        self.ylims = None
        self.note = None
        self.labels = None
        self.styles = ['+', 'x', 'o', 's', 'd', '^', 'v', '<', '>', 'H', '*', 'p', 'h', 'D', '8']
        self.colors = ['r', 'b', 'g', 'm', 'c', 'y']
        self.axis_enter = None
        self.axis_leave = None
        self.button_press = None
        self.mouse_motion = None
        self.button_release = None
        if isinstance(data, dict):
            self.data = data
        elif isinstance(data, list) and len(data) > 0 and isinstance(data[0], dict):
            self.datalist = data

    def _set_reference_limits(self):
        if isinstance(self.canvas, mplqt.FigureCanvas):
            axes = self.canvas.figure.axes
            self.xlims = [None] * len(axes)
            self.ylims = [None] * len(axes)
            for axid in range(0, len(axes)):
                self.xlims[axid] = axes[axid].get_xlim()
                self.ylims[axid] = axes[axid].get_ylim()

    def _connect_canvas(self):
        if isinstance(self.canvas,mplqt.FigureCanvas):
            self.axis_enter = self.canvas.mpl_connect('axes_enter_event', self._create_background)
            self.axis_leave = self.canvas.mpl_connect('axes_leave_event', self._delete_background)
            self.button_press = self.canvas.mpl_connect('button_press_event', self._init_rectangle)
            self.mouse_motion = self.canvas.mpl_connect('motion_notify_event', self._blit_rectangle)
            self.button_release = self.canvas.mpl_connect('button_release_event', self._zoom_axis)

    def _disconnect_canvas(self):
        if isinstance(self.canvas, mplqt.FigureCanvas):
            self.canvas.mpl_disconnect(self.axis_enter)
            self.canvas.mpl_disconnect(self.axis_leave)
            self.canvas.mpl_disconnect(self.button_press)
            self.canvas.mpl_disconnect(self.mouse_motion)
            self.canvas.mpl_disconnect(self.button_release)

    def _create_background(self, event):
        if self.press is None:
            self.bg = self.canvas.copy_from_bbox(event.inaxes.bbox)

    def _delete_background(self, event):
        if self.bg is not None and self.press is None:
            self.bg = None

    def _init_rectangle(self, event):
        if event.inaxes is not None:
            if event.dblclick:
                self._reset_axis(event)
            else:
                self._create_background(event)
                self.press = (event.inaxes, event.xdata, event.ydata)
                self.rect = matplotlib.patches.Rectangle((event.xdata, event.ydata), 0, 0, linewidth=0, edgecolor='b', fill=False, facecolor='none')
                self.rectimage = matplotlib.patches.Rectangle((event.xdata, event.ydata), 0, 0, linewidth=1, edgecolor='r', fill=False, facecolor='none')
                event.inaxes.add_patch(self.rectimage)
                event.inaxes.draw_artist(self.rectimage)
                self.canvas.blit(event.inaxes.bbox)

    def _blit_rectangle(self, event):
        if self.press is not None and self.rect is not None and self.rectimage is not None:
            axes, xx, yy = self.press
            if event.inaxes == axes:
                dx = event.xdata - xx
                dy = event.ydata - yy
                self.rect.set_width(dx)
                self.rect.set_height(dy)
                self.rectimage.set_width(dx)
                self.rectimage.set_height(dy)
            else:
                xdata, ydata = axes.transData.inverted().transform((event.x, event.y))
                xmin, xmax = axes.get_xlim()
                ymin, ymax = axes.get_ylim()
                dx = xdata - xx
                dy = ydata - yy
                dxi = dx
                dyi = dy
                if xdata < xmin:
                    dxi = xmin - xx
                elif xdata > xmax:
                    dxi = xmax - xx
                if ydata < ymin:
                    dyi = ymin - yy
                elif ydata > ymax:
                    dyi = ymax - yy
                self.rect.set_width(dx)
                self.rect.set_height(dy)
                self.rectimage.set_width(dxi)
                self.rectimage.set_height(dyi)
        if event.inaxes is not None and isinstance(self.points, list) and isinstance(self.note, list):
            axid = self.canvas.figure.axes.index(event.inaxes)
            if self.note[axid] is not None and self.points[axid] is not None:
                visible = self.note[axid].get_visible()
                cont = False
                pos = None
                text = None
                for diag in self.points[axid]:
                    if not cont:
                        if isinstance(diag, matplotlib.container.ErrorbarContainer):
                            cont, ind = diag[0].contains(event)
                            if cont:
                                pos = diag[0].get_xydata()[ind["ind"][0]]
                                text = diag.get_label()
                        elif isinstance(diag, matplotlib.collections.PathCollection):
                            cont, ind = diag.contains(event)
                            if cont:
                                pos = diag.get_offsets()[ind["ind"][0]]
                                text = diag.get_label()
                if pos is not None and text is not None:
                    if self.bg is None:
                        self._create_background(event)
                    self.note[axid].xy = pos
                    self.note[axid].set_text(text)
                    self.note[axid].set_visible(True)
                if not cont:
                    self.note[axid].set_visible(False)
        if self.bg is not None:
            self.canvas.figure.canvas.restore_region(self.bg)
            if self.press is not None and self.rect is not None and self.rectimage is not None:
                axes, xx, yy = self.press
                axes.draw_artist(self.rectimage)
                self.canvas.blit(axes.bbox)
            if event.inaxes is not None and isinstance(self.note, list):
                axid = self.canvas.figure.axes.index(event.inaxes)
                if self.note[axid] is not None:
                    event.inaxes.draw_artist(self.note[axid])
                    self.canvas.blit(event.inaxes.bbox)

    def _zoom_axis(self, event):
        if self.press is not None and self.rect is not None and self.rectimage is not None and self.bg is not None:
            if self.rect.get_width() != 0 and self.rect.get_height() != 0:
                axes, xx, yy = self.press
                self.canvas.figure.canvas.restore_region(self.bg)
                xmin = np.nanmin([xx, xx + self.rect.get_width()])
                xmax = np.nanmax([xx, xx + self.rect.get_width()])
                ymin = np.nanmin([yy, yy + self.rect.get_height()])
                ymax = np.nanmax([yy, yy + self.rect.get_height()])
                axes.set_xlim([xmin, xmax])
                axes.set_ylim([ymin, ymax])
                self.rectimage.remove()
        self.rect = None
        self.rectimage = None
        self.press = None
        self._delete_background(event)
        self.canvas.draw_idle()

    def _reset_axis(self, event):
        self._delete_background(event)
        if isinstance(event.inaxes, matplotlib.axes.Axes):
            axid = self.canvas.figure.axes.index(event.inaxes)
            event.inaxes.set_xlim(self.xlims[axid])
            event.inaxes.set_ylim(self.ylims[axid])
        self.canvas.draw_idle()

class time_trace_plotter(GenericPlotter):

    def __init__(self, data, ti, tf, fontsize=None):
        super(time_trace_plotter, self).__init__(data)
        self.ti = None
        self.tf = None
        self.fs = 10
        if isinstance(ti, (int, float)):
            self.ti = float(ti)
        if isinstance(tf, (int, float)):
            self.tf = float(tf)
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.data is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 1100, 500)
            self.setWindowTitle('Time Trace Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        fig = mplfig.Figure(tight_layout=True)
        ti = -9999.9999 if self.ti is None else self.ti
        tf = 9999.9999 if self.tf is None else self.tf

        ax1 = fig.add_subplot(121)
        if "IP" in self.data and self.data["IP"] is not None and "TIP" in self.data and self.data["TIP"] is not None:
            tfilt = np.all([self.data["TIP"] >= ti, self.data["TIP"] <= tf], axis=0)
            ax1.plot(self.data["TIP"][tfilt], np.abs(self.data["IP"][tfilt])*1.0e-6, color='r', label=r'I_p [MA]')
            if "BMAG" in self.data and self.data["BMAG"] is not None and "TBMAG" in self.data and self.data["TBMAG"] is not None:
                tfilt = np.all([self.data["TBMAG"] >= ti, self.data["TBMAG"] <= tf], axis=0)
                ax1.plot(self.data["TBMAG"][tfilt], np.abs(self.data["BMAG"][tfilt]), color='b', label=r'B_{mag} [T]')
            if "NEAX" in self.data and self.data["NEAX"] is not None and "TNEAX" in self.data and self.data["TNEAX"] is not None:
                tfilt = np.all([self.data["TNEAX"] >= ti, self.data["TNEAX"] <= tf], axis=0)
                ax1.plot(self.data["TNEAX"][tfilt], self.data["NEAX"][tfilt]*1.0e-19, color='g', label=r'n_{e,0} [10^19 m^{-3}]')
            if "TEAX" in self.data and self.data["TEAX"] is not None and "TTEAX" in self.data and self.data["TTEAX"] is not None:
                tfilt = np.all([self.data["TTEAX"] >= ti, self.data["TTEAX"] <= tf], axis=0)
                ax1.plot(self.data["TTEAX"][tfilt], self.data["TEAX"][tfilt]*1.0e-3, color='m', label=r'T_{e,0} [keV]')
            if "ZEFV" in self.data and self.data["ZEFV"] is not None and "TZEFV" in self.data and self.data["TZEFV"] is not None:
                tfilt = np.all([self.data["TZEFV"] >= ti, self.data["TZEFV"] <= tf], axis=0)
                ax1.plot(self.data["TZEFV"][tfilt], self.data["ZEFV"][tfilt], color='c', label=r'Z_{eff,v}')
            if "ZEFH" in self.data and self.data["ZEFH"] is not None and "TZEFH" in self.data and self.data["TZEFH"] is not None:
                tfilt = np.all([self.data["TZEFH"] >= ti, self.data["TZEFH"] <= tf], axis=0)
                ax1.plot(self.data["TZEFH"][tfilt], self.data["ZEFH"][tfilt], color='y', label=r'Z_{eff,h}')
            ax1.legend(loc='best', prop={'size': self.fs-1})
        ax1.set_ylabel(r'Plasma Parameters', fontsize=self.fs)
        ax1.set_xlabel(r'Time [s]', fontsize=self.fs)
        ax1.tick_params(axis='both', which='major', labelsize=self.fs)

        ax2 = fig.add_subplot(122)
        if "OHM" in self.data and self.data["OHM"] is not None and "TOHM" in self.data and self.data["TOHM"] is not None:
            sc = 1.0e-3 if np.mean(np.abs(self.data["OHM"])) < 1.0e4 else 1.0e-6
            unit = 'kW' if np.mean(np.abs(self.data["OHM"])) < 1.0e4 else 'MW'
            tfilt = np.all([self.data["TOHM"] >= ti, self.data["TOHM"] <= tf], axis=0)
            ax2.plot(self.data["TOHM"][tfilt], self.data["OHM"][tfilt]*sc, color='k', label=r'P_{ohm} ['+unit+']')
            if "ICRH" in self.data and self.data["ICRH"] is not None and "TICRH" in self.data and self.data["TICRH"] is not None:
                tfilt = np.all([self.data["TICRH"] >= ti, self.data["TICRH"] <= tf], axis=0)
                ax2.plot(self.data["TICRH"][tfilt], self.data["ICRH"][tfilt]*1.0e-6, color='b', label=r'P_{IC} [MW]')
            if "NBI" in self.data and self.data["NBI"] is not None and "TNBI" in self.data and self.data["TNBI"] is not None:
                tfilt = np.all([self.data["TNBI"] >= ti, self.data["TNBI"] <= tf], axis=0)
                ax2.plot(self.data["TNBI"][tfilt], self.data["NBI"][tfilt]*1.0e-6, color='r', label=r'P_{NBI} [MW]')
            if "LHCD" in self.data and self.data["LHCD"] is not None and "TLHCD" in self.data and self.data["TLHCD"] is not None:
                tfilt = np.all([self.data["TLHCD"] >= ti, self.data["TLHCD"] <= tf], axis=0)
                ax2.plot(self.data["TLHCD"][tfilt], self.data["LHCD"][tfilt]*1.0e-6, color='g', label=r'P_{LH} [MW]')
            ax2.legend(loc='best', prop={'size': self.fs-1})
        ax2.set_ylabel(r'Plasma Heating', fontsize=self.fs)
        ax2.set_xlabel(r'Time [s]', fontsize=self.fs)
        ax2.tick_params(axis='both', which='major', labelsize=self.fs)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)
        self.toolbar = mplqt.NavigationToolbar2QT(self.canvas, self)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)
        gbox.addWidget(self.toolbar)

        return gbox

class profile_plotter(GenericPlotter):

    def __init__(self, datalist, plot_vars, sigma=None, csplot=None, exponents=None, fontsize=None, labels=None, styles=None, colors=None):
        super(profile_plotter, self).__init__(datalist)
        if isinstance(plot_vars, list):
            self.vars = plot_vars
        self.sigma = 1.0
        self.csplot = "RHOTORN"
        self.escales = [0]
        self.fs = 10
        if isinstance(sigma, (int, float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if self.datalist is not None and isinstance(csplot, str):
            if "CS_"+csplot.upper() in self.datalist[0]:
                self.csplot = csplot.upper()
        if isinstance(exponents, list):
            self.escales = exponents
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if isinstance(labels, list):
            self.labels = labels
        if isinstance(styles, list):
            self.styles = styles
        if isinstance(colors, list):
            self.colors = colors
            
        if self.datalist is not None and self.vars is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 600, 650)
            self.setWindowTitle('Profile Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data for profile plotting tool.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        ocs = self.csplot
        xlims = [9999.9, -9999.9]
        (xt, xlabel, xunit, fl, fu) = ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + r' [' + xunit + r']' if xunit else xlabel

        fig = mplfig.Figure()
        ax = fig.add_subplot(1, 1, 1)
        self.points = [[]]
        self.note = [ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})]
        self.note[0].set_visible(False)
        ulabels = []
        lartists = []
        ylabel = None
        yunit = ""
        scale = 1.0

        for qq in range(len(self.vars)):

            var = self.vars[qq]
            exp = self.escales[qq]
            if ylabel is None:
                (qtag, ylabel, yunit, fqlbl, fqunit) = ptools.define_quantity(var, exponent=exp)
                scale = np.power(10.0, -exp)
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel

            for dd in range(len(self.datalist)):

                self.data = self.datalist[dd]
                ics = self.data["CS_DIAG"]

                if "CS_"+ocs in self.data:
                    xlims[0] = np.nanmin([xlims[0], np.nanmin(self.data["CS_"+ocs])])
                    xlims[1] = np.nanmax([xlims[1], np.nanmax(self.data["CS_"+ocs])])
                    #if ocs == "RMAJORO":
                    #    xlims[0] = 3.0
                    #if ocs == "RMAJORI":
                    #    xlims[1] = 3.0
                ulabel = self.labels[dd] if self.labels is not None and dd < len(self.labels) else None
                ulabels.append(ulabel)
                ucolor = self.colors[dd % len(self.colors)] if self.colors is not None else 'k'
                ustyle = self.styles[dd % len(self.styles)] if self.styles is not None else '.'

                if var+"RAW" in self.data and len(self.data[var+"RAW"]) > 1:
                    xraw = self.data[var+"RAWX"].flatten()
                    xeraw = self.data[var+"RAWXEB"].flatten()
                    yraw = self.data[var+"RAW"].flatten()
                    yeraw = self.data[var+"RAWEB"].flatten()
                    if ics != ocs:
                        jcs = self.data["CJ_BASE"]
                        ivvec = self.data["CS_"+ics]
                        ovvec = self.data["CS_"+ocs]
                        ijvec = self.data["CJ_"+jcs+"_"+ics] if "CJ_"+jcs+"_"+ics in self.data else None
                        ojvec = self.data["CJ_"+jcs+"_"+ocs] if "CJ_"+jcs+"_"+ocs in self.data else None
                        oevec = None
                        (xraw, dj, txeraw) = ptools.convert_base_coords(xraw, ivvec, ovvec, ijvec, ojvec, oevec)
#                        xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                    for ii in range(len(self.data[var+"DIAG"])):
                        filt = (self.data[var+"DMAP"] == ii)
                        if np.any(filt):
                            xplot = xraw[filt]
                            yplot = yraw[filt]
#                            xperr = self.sigma * xeraw[filt]
                            yperr = self.sigma * yeraw[filt]
                            dlabel = self.data[var+"DIAG"][ii]
#                            if self.xerrflag:
#                                eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color=ucolor, ls='', marker=ustyle, label=dlabel)
#                                self.points[qq].append(eba)
#                                if np.any(filt) and dd >= len(lartists):
#                                    lartists.append(eba)
                            eba = ax.errorbar(xplot, yplot*scale, yerr=yperr*scale, color=ucolor, ls='', marker=ustyle, label=dlabel)
                            self.points[0].append(eba)
                            if np.any(filt) and dd >= len(lartists):
                                lartists.append(eba)

            ylims = ax.get_ylim()
            ax.set_xlim(xlims[0], xlims[1])
            ax.set_ylim(0.0, ylims[1])
            ax.set_ylabel(pylabel, fontsize=self.fs)
            ax.set_xlabel(pxlabel, fontsize=self.fs)
            ax.tick_params(axis='both', which='major', labelsize=self.fs)
            ax.legend(lartists, ulabels, loc='best')

#        sid = "%s #%d: %.4f - %.4f" % (self.data["META"]["DEVICE"], self.data["META"]["SHOT"], self.data["META"]["T1"], self.data["META"]["T2"])
#        rshift = self.data["ZD"]["RSHIFT"][""] if "ZD" in self.data and "RSHIFT" in self.data["ZD"] else 0.0
#        shift_warning = "A radial shift of %.2f cm (on mid-plane R major) was applied to measurements." % (rshift * 100.0)
#        nscale = self.data["ZD"]["NESCALE"][""] if "ZD" in self.data and "NESCALE" in self.data["ZD"] and self.data["ZD"]["NESCALE"] is not None else 0.0
#        scale_warning = "HRTS/NE was scaled by a factor of %.3f based on KG1V/LID3." % (nscale) if self.data["FLAG"]["NSCALE"] else ""
#        fig.suptitle(sid+"\n"+shift_warning+"\n"+scale_warning, fontsize=self.fs+2)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)
        self.toolbar = mplqt.NavigationToolbar2QT(self.canvas, self)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)
        gbox.addWidget(self.toolbar)

        return gbox

class JETExperimentDataViewer(QtWidgets.QWidget):

#    styles = ['+', 'x', 'o', 's', 'd', '^', 'v', '<', '>', 'H', '*', 'p', 'h', 'D', '8']
#    colors = ['r', 'b', 'g', 'm', 'c', 'y']
    cx_fields = ["CXSM", "CXDM", "CXFM", "CXGM", "CXHM",
                 "CXJM", "CXKM", "CXLM",
                 "CXS4", "CXD4", "CXF4", "CXG4", "CXH4",
                 "CXS6", "CXD6", "CXF6", "CXG6", "CXH6",
                 "CXS8", "CXD8", "CXF8", "CXG8", "CXH8",
                 "CX7A", "CX7B", "CX7C", "CX7D", "CXSE"
                ]

    def __init__(self, ppfflag=False):
        super(JETExperimentDataViewer, self).__init__()
        self.pyversion = ".".join(str(i) for i in sys.version_info[:2])
        print("Using Python version: %s" % (self.pyversion))
        location = __location__ #inspect.getsourcefile(ajet)
        self.exsrcdir = os.path.dirname(location) + '/'
        print("Using EX2GK version: %s" % (__version__))
        print("Using EX2GK definition from: %s" % (self.exsrcdir))
        location = inspect.getsourcefile(customfilters.apply_custom_filters) if customfilters is not None else None
        self.filtsrcdir = os.path.dirname(location) + '/' if location is not None else "N/A"
        print("Using EX2GK custom filter definition from: %s" % (self.filtsrcdir))
        uid = pwd.getpwuid(os.getuid())[0]
        self._user = uid if isinstance(uid, str) else ""
        self.device = "JET"
        self.sdata = None
        self.ppfflag = ppfflag
        self.didx = None
        self.clist = []
        self.tdata = None
        self._preset_fields = data_fields
        self._font = QtGui.QFont(self.font().family())
        self._font.setPixelSize(11)
        self._font_height = QtGui.QFontMetrics(self._font).height()
        layout = self.initUI()
        self.setLayout(layout)
        self.setGeometry(5, 0, 750, 500)
        self.setWindowTitle('JET Experimental Data Plotter')
        self.setFont(self._font)
        self.show()

    def initUI(self):

        self.ViewTraceButton = QtWidgets.QPushButton("View Time Traces")
        self.ViewTraceButton.clicked.connect(self._plot_time_traces)
        self.ShotNumberEntry = QtWidgets.QLineEdit()
        self.ShotNumberEntry.setValidator(QtGui.QIntValidator(None))
        self.ShotNumberEntry.setMaxLength(6)
        self.ShotNumberEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeStartEntry = QtWidgets.QLineEdit()
        self.TimeStartEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeStartEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeEndEntry = QtWidgets.QLineEdit()
        self.TimeEndEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeEndEntry.setAlignment(QtCore.Qt.AlignLeft)

        twbox = QtWidgets.QFormLayout()
        twbox.addRow("Shot Number", self.ShotNumberEntry)
        twbox.addRow("Time Start (s)", self.TimeStartEntry)
        twbox.addRow("Time End (s)", self.TimeEndEntry)

        self.ListInstructions = QtWidgets.QLabel("Double-click a window entry to load it into the right panel.")
        self.ListInstructions.setWordWrap(True)
        self.DatabaseList = QtWidgets.QTableWidget()
        self.DatabaseList.setColumnCount(1)
        self.DatabaseList.setHorizontalHeaderLabels(["Time Window"])
        if fqt5:
            self.DatabaseList.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        else:
            self.DatabaseList.horizontalHeader().setResizeMode(QtWidgets.QHeaderView.Stretch)
        self.DatabaseList.verticalHeader().setDefaultSectionSize(int(self._font_height * 2))
        self.DatabaseList.verticalHeader().setVisible(False)
        self.DatabaseList.cellDoubleClicked.connect(self._select_data)
        self.AddWindowButton = QtWidgets.QPushButton("Add Window")
        self.AddWindowButton.clicked.connect(self._add_time_window)
        self.DeleteWindowButton = QtWidgets.QPushButton("Delete Window")
        self.DeleteWindowButton.setEnabled(False)
        self.DeleteWindowButton.clicked.connect(self._delete_time_window)
        self.ExtractAllWindowsButton = QtWidgets.QPushButton("Extract All Windows")
        self.ExtractAllWindowsButton.setEnabled(False)
        self.ExtractAllWindowsButton.clicked.connect(self._extract_all_time_windows)
        self.ExtractWindowButton = QtWidgets.QPushButton("Extract Window")
        self.ExtractWindowButton.setEnabled(False)
        self.ExtractWindowButton.clicked.connect(self._extract_time_window)

        wmbox = QtWidgets.QGridLayout()        
        wmbox.addWidget(self.AddWindowButton, 0, 0)
        wmbox.addWidget(self.DeleteWindowButton, 0, 1)
        wmbox.addWidget(self.ExtractAllWindowsButton, 1, 0)
        wmbox.addWidget(self.ExtractWindowButton, 1, 1)

        dlbox = QtWidgets.QVBoxLayout()
        dlbox.addWidget(self.ListInstructions)
        dlbox.addWidget(self.DatabaseList)
        dlbox.addLayout(wmbox)

        svbox = QtWidgets.QVBoxLayout()
        svbox.addWidget(self.ViewTraceButton)
        svbox.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        svbox.addLayout(twbox)
        svbox.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        svbox.addLayout(dlbox)

        self.TabPanel = QtWidgets.QTabWidget()

        self.DiagnosticsTab = QtWidgets.QWidget()
        self.DiagnosticsTab.setLayout(self.DiagnosticsUI())
        self.PlottingTab = QtWidgets.QWidget()
        self.PlottingTab.setLayout(self.PlottingUI())

        self.TabPanel.addTab(self.DiagnosticsTab, "Diagnostic Selection")
        self.TabPanel.addTab(self.PlottingTab, "Plot Options")

        layout = QtWidgets.QHBoxLayout()
        layout.addLayout(svbox, 40)
        layout.addItem(QtWidgets.QSpacerItem(int(self._font_height / 2), 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        layout.addWidget(self.TabPanel, 70)

        self._set_gui_default_extraction_settings()

        return layout

    def DiagnosticsUI(self):

        self.EquilibriumLabel = QtWidgets.QLabel("Equilibrium")
        self.EquilibriumShiftBox = QtWidgets.QCheckBox("Apply automated radial shift")

        leqbox = QtWidgets.QHBoxLayout()
        leqbox.addWidget(self.EquilibriumLabel)
        leqbox.addWidget(self.EquilibriumShiftBox)

        self.EquilibriumList = QtWidgets.QComboBox()
        self.EquilibriumList.addItem("EHTR")
        self.EquilibriumList.addItem("EFTM")
        self.EquilibriumList.addItem("EFTF")
        self.EquilibriumList.addItem("EFTP")
        self.EquilibriumList.addItem("EFIT")
        self.EquilibriumUserIDEntry = QtWidgets.QLineEdit()
        self.EquilibriumSequenceEntry = QtWidgets.QLineEdit()
        self.EquilibriumSequenceEntry.setValidator(QtGui.QIntValidator(None))

        deqbox = QtWidgets.QHBoxLayout()
        deqbox.addWidget(self.EquilibriumList)
        deqbox.addWidget(self.EquilibriumUserIDEntry)
        deqbox.addWidget(self.EquilibriumSequenceEntry)

        self.TSLabel = QtWidgets.QLabel("Thomson Scattering")

        self.GrabHRTSBox = QtWidgets.QCheckBox("HRTS")
        self.GrabHRTSBox.toggled.connect(self._enable_hrts_fields)
        self.HRTSUserIDEntry = QtWidgets.QLineEdit()
        self.HRTSUserIDEntry.setEnabled(False)
        self.HRTSSequenceEntry = QtWidgets.QLineEdit()
        self.HRTSSequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.HRTSSequenceEntry.setEnabled(False)

        hrtsbox = QtWidgets.QHBoxLayout()
        hrtsbox.addWidget(self.GrabHRTSBox)
        hrtsbox.addWidget(self.HRTSUserIDEntry)
        hrtsbox.addWidget(self.HRTSSequenceEntry)

        self.GrabLIDRBox = QtWidgets.QCheckBox("LIDR")
        self.GrabLIDRBox.toggled.connect(self._enable_lidr_fields)
        self.LIDRUserIDEntry = QtWidgets.QLineEdit()
        self.LIDRUserIDEntry.setEnabled(False)
        self.LIDRSequenceEntry = QtWidgets.QLineEdit()
        self.LIDRSequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.LIDRSequenceEntry.setEnabled(False)

        lidrbox = QtWidgets.QHBoxLayout()
        lidrbox.addWidget(self.GrabLIDRBox)
        lidrbox.addWidget(self.LIDRUserIDEntry)
        lidrbox.addWidget(self.LIDRSequenceEntry)

        self.REFLabel = QtWidgets.QLabel("Reflectometry")

        self.GrabKG10Box = QtWidgets.QCheckBox("KG10")
        self.GrabKG10Box.toggled.connect(self._enable_kg10_fields)
        self.KG10UserIDEntry = QtWidgets.QLineEdit()
        self.KG10UserIDEntry.setEnabled(False)
        self.KG10SequenceEntry = QtWidgets.QLineEdit()
        self.KG10SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.KG10SequenceEntry.setEnabled(False)

        kg10box = QtWidgets.QHBoxLayout()
        kg10box.addWidget(self.GrabKG10Box)
        kg10box.addWidget(self.KG10UserIDEntry)
        kg10box.addWidget(self.KG10SequenceEntry)

#        self.GrabREFLBox = QtWidgets.QCheckBox("REFL")
#        self.GrabREFLBox.toggled.connect(self._enable_refl_fields)
#        self.REFLUserIDEntry = QtWidgets.QLineEdit()
#        self.REFLUserIDEntry.setEnabled(False)
#        self.REFLSequenceEntry = QtWidgets.QLineEdit()
#        self.REFLSequenceEntry.setValidator(QtGui.QIntValidator(None))
#        self.REFLSequenceEntry.setEnabled(False)

#        reflbox = QtWidgets.QHBoxLayout()
#        reflbox.addWidget(self.GrabREFLBox)
#        reflbox.addWidget(self.REFLUserIDEntry)
#        reflbox.addWidget(self.REFLSequenceEntry)

        self.ECELabel = QtWidgets.QLabel("Electron Cyclotron Emission")

        self.GrabKK3Box = QtWidgets.QCheckBox("KK3")
        self.GrabKK3Box.toggled.connect(self._enable_kk3_fields)
        self.KK3UserIDEntry = QtWidgets.QLineEdit()
        self.KK3UserIDEntry.setEnabled(False)
        self.KK3SequenceEntry = QtWidgets.QLineEdit()
        self.KK3SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.KK3SequenceEntry.setEnabled(False)

        kk3tbox = QtWidgets.QHBoxLayout()
        kk3tbox.addWidget(self.GrabKK3Box)
        kk3tbox.addWidget(self.KK3UserIDEntry)
        kk3tbox.addWidget(self.KK3SequenceEntry)

        self.KK3EquilibriumList = QtWidgets.QComboBox()
        self.KK3EquilibriumList.addItem("EHTR")
        self.KK3EquilibriumList.addItem("EFTM")
        self.KK3EquilibriumList.addItem("EFTF")
        self.KK3EquilibriumList.addItem("EFTP")
        self.KK3EquilibriumList.addItem("EFIT")
        self.KK3EquilibriumUserIDEntry = QtWidgets.QLineEdit()
        self.KK3EquilibriumUserIDEntry.setEnabled(False)
        self.KK3EquilibriumSequenceEntry = QtWidgets.QLineEdit()
        self.KK3EquilibriumSequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.KK3EquilibriumSequenceEntry.setEnabled(False)

        kk3eqbox = QtWidgets.QHBoxLayout()
        kk3eqbox.addWidget(self.KK3EquilibriumList)
        kk3eqbox.addWidget(self.KK3EquilibriumUserIDEntry)
        kk3eqbox.addWidget(self.KK3EquilibriumSequenceEntry)

        kk3box = QtWidgets.QVBoxLayout()
        kk3box.addLayout(kk3tbox)
        kk3box.addLayout(kk3eqbox)

        self.CXLabel = QtWidgets.QLabel("Charge Exchange")

        self.GrabCX1Box = QtWidgets.QCheckBox("")
        self.GrabCX1Box.toggled.connect(self._enable_cx1_fields)
        self.CX1List = QtWidgets.QComboBox()
        self.CX1List.setEnabled(False)
        for cx in self.cx_fields:
            self.CX1List.addItem(cx)
        self.CX1UserIDEntry = QtWidgets.QLineEdit()
        self.CX1UserIDEntry.setEnabled(False)
        self.CX1SequenceEntry = QtWidgets.QLineEdit()
        self.CX1SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.CX1SequenceEntry.setEnabled(False)

        cx1box = QtWidgets.QHBoxLayout()
        cx1box.addWidget(self.GrabCX1Box)
        cx1box.addWidget(self.CX1List)
        cx1box.addWidget(self.CX1UserIDEntry)
        cx1box.addWidget(self.CX1SequenceEntry)

        self.GrabCX2Box = QtWidgets.QCheckBox("")
        self.GrabCX2Box.toggled.connect(self._enable_cx2_fields)
        self.CX2List = QtWidgets.QComboBox()
        self.CX2List.setEnabled(False)
        for cx in self.cx_fields:
            self.CX2List.addItem(cx)
        self.CX2UserIDEntry = QtWidgets.QLineEdit()
        self.CX2UserIDEntry.setEnabled(False)
        self.CX2SequenceEntry = QtWidgets.QLineEdit()
        self.CX2SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.CX2SequenceEntry.setEnabled(False)

        cx2box = QtWidgets.QHBoxLayout()
        cx2box.addWidget(self.GrabCX2Box)
        cx2box.addWidget(self.CX2List)
        cx2box.addWidget(self.CX2UserIDEntry)
        cx2box.addWidget(self.CX2SequenceEntry)

        self.GrabCX3Box = QtWidgets.QCheckBox("")
        self.GrabCX3Box.toggled.connect(self._enable_cx3_fields)
        self.CX3List = QtWidgets.QComboBox()
        self.CX3List.setEnabled(False)
        for cx in self.cx_fields:
            self.CX3List.addItem(cx)
        self.CX3UserIDEntry = QtWidgets.QLineEdit()
        self.CX3UserIDEntry.setEnabled(False)
        self.CX3SequenceEntry = QtWidgets.QLineEdit()
        self.CX3SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.CX3SequenceEntry.setEnabled(False)

        cx3box = QtWidgets.QHBoxLayout()
        cx3box.addWidget(self.GrabCX3Box)
        cx3box.addWidget(self.CX3List)
        cx3box.addWidget(self.CX3UserIDEntry)
        cx3box.addWidget(self.CX3SequenceEntry)

        self.GrabCX4Box = QtWidgets.QCheckBox("")
        self.GrabCX4Box.toggled.connect(self._enable_cx4_fields)
        self.CX4List = QtWidgets.QComboBox()
        self.CX4List.setEnabled(False)
        for cx in self.cx_fields:
            self.CX4List.addItem(cx)
        self.CX4UserIDEntry = QtWidgets.QLineEdit()
        self.CX4UserIDEntry.setEnabled(False)
        self.CX4SequenceEntry = QtWidgets.QLineEdit()
        self.CX4SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.CX4SequenceEntry.setEnabled(False)

        cx4box = QtWidgets.QHBoxLayout()
        cx4box.addWidget(self.GrabCX4Box)
        cx4box.addWidget(self.CX4List)
        cx4box.addWidget(self.CX4UserIDEntry)
        cx4box.addWidget(self.CX4SequenceEntry)

        self.GrabCX5Box = QtWidgets.QCheckBox("")
        self.GrabCX5Box.toggled.connect(self._enable_cx5_fields)
        self.CX5List = QtWidgets.QComboBox()
        self.CX5List.setEnabled(False)
        for cx in self.cx_fields:
            self.CX5List.addItem(cx)
        self.CX5UserIDEntry = QtWidgets.QLineEdit()
        self.CX5UserIDEntry.setEnabled(False)
        self.CX5SequenceEntry = QtWidgets.QLineEdit()
        self.CX5SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.CX5SequenceEntry.setEnabled(False)

        cx5box = QtWidgets.QHBoxLayout()
        cx5box.addWidget(self.GrabCX5Box)
        cx5box.addWidget(self.CX5List)
        cx5box.addWidget(self.CX5UserIDEntry)
        cx5box.addWidget(self.CX5SequenceEntry)

        self.GrabCX6Box = QtWidgets.QCheckBox("")
        self.GrabCX6Box.toggled.connect(self._enable_cx6_fields)
        self.CX6List = QtWidgets.QComboBox()
        self.CX6List.setEnabled(False)
        for cx in self.cx_fields:
            self.CX6List.addItem(cx)
        self.CX6UserIDEntry = QtWidgets.QLineEdit()
        self.CX6UserIDEntry.setEnabled(False)
        self.CX6SequenceEntry = QtWidgets.QLineEdit()
        self.CX6SequenceEntry.setValidator(QtGui.QIntValidator(None))
        self.CX6SequenceEntry.setEnabled(False)

        cx6box = QtWidgets.QHBoxLayout()
        cx6box.addWidget(self.GrabCX6Box)
        cx6box.addWidget(self.CX6List)
        cx6box.addWidget(self.CX6UserIDEntry)
        cx6box.addWidget(self.CX6SequenceEntry)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(leqbox)
        layout.addLayout(deqbox)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.TSLabel)
        layout.addLayout(hrtsbox)
        layout.addLayout(lidrbox)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.REFLabel)
        layout.addLayout(kg10box)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.ECELabel)
        layout.addLayout(kk3box)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.CXLabel)
        layout.addLayout(cx1box)
        layout.addLayout(cx2box)
        layout.addLayout(cx3box)
        layout.addLayout(cx4box)
        layout.addLayout(cx5box)
        layout.addLayout(cx6box)
        layout.addStretch()

        return layout

    def PlottingUI(self):

        self.AddAllWindowsPlotButton = QtWidgets.QPushButton("Add All Windows")
        self.AddAllWindowsPlotButton.clicked.connect(self._add_all_windows_to_plot)
        self.AddWindowPlotButton = QtWidgets.QPushButton("Add Selected Window")
        self.AddWindowPlotButton.clicked.connect(self._add_selected_window_to_plot)
        self.DeleteWindowPlotButton = QtWidgets.QPushButton("Delete Selected Window")
        self.DeleteWindowPlotButton.clicked.connect(self._delete_selected_window_from_plot)

        wpbox = QtWidgets.QHBoxLayout()
        wpbox.addWidget(self.AddAllWindowsPlotButton)
        wpbox.addWidget(self.AddWindowPlotButton)
        wpbox.addWidget(self.DeleteWindowPlotButton)

        self.PlotSettingsList = QtWidgets.QTableWidget()
        self.PlotSettingsList.setColumnCount(4)
        self.PlotSettingsList.setHorizontalHeaderLabels(["Time Window", "Label", "Marker", "Color"])
        if fqt5:
            self.PlotSettingsList.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        else:
            self.PlotSettingsList.horizontalHeader().setResizeMode(QtWidgets.QHeaderView.Stretch)
        self.PlotSettingsList.verticalHeader().setDefaultSectionSize(int(self._font_height * 2))
        self.PlotSettingsList.verticalHeader().setVisible(False)

        self.PlotLabel = QtWidgets.QLabel("Plotting")
        self.CoordinateList = QtWidgets.QComboBox()
        self.CoordinateList.setEnabled(False)
        self.clist = []
        self.CoordinateList.addItem("rho_tor")
        self.clist.append("RHOTORN")
        self.CoordinateList.addItem("rho_pol")
        self.clist.append("RHOPOLN")
        self.CoordinateList.addItem("psi_norm_tor")
        self.clist.append("TORFLUXN")
        self.CoordinateList.addItem("psi_norm_pol")
        self.clist.append("POLFLUXN")
        self.CoordinateList.addItem("psi_tor")
        self.clist.append("TORFLUX")
        self.CoordinateList.addItem("psi_pol")
        self.clist.append("POLFLUX")
        self.CoordinateList.addItem("R_major_LFS @ Z_mag")
        self.clist.append("RMAJORO")
        self.CoordinateList.addItem("R_major_HFS @ Z_mag")
        self.clist.append("RMAJORI")
        self.CoordinateList.addItem("r_minor_averaged @ Z_mag")
        self.clist.append("RMINORA")

        csbox = QtWidgets.QFormLayout()
        csbox.addRow("Radial Coordinate", self.CoordinateList)

        self.PlotNEButton = QtWidgets.QPushButton("NE")
        self.PlotNEButton.setEnabled(False)
        self.PlotNEButton.clicked.connect(self._plot_ne_data)
        self.PlotTEButton = QtWidgets.QPushButton("TE")
        self.PlotTEButton.setEnabled(False)
        self.PlotTEButton.clicked.connect(self._plot_te_data)
        self.PlotTIButton = QtWidgets.QPushButton("TI")
        self.PlotTIButton.setEnabled(False)
        self.PlotTIButton.clicked.connect(self._plot_ti_data)
        self.PlotANGFButton = QtWidgets.QPushButton("ANGF")
        self.PlotANGFButton.setEnabled(False)
        self.PlotANGFButton.clicked.connect(self._plot_angf_data)
        self.PlotQButton = QtWidgets.QPushButton("Q")
        self.PlotQButton.setEnabled(False)
        self.PlotQButton.clicked.connect(self._plot_q_data)

        pbbox = QtWidgets.QHBoxLayout()
        pbbox.addWidget(self.PlotNEButton)
        pbbox.addWidget(self.PlotTEButton)
        pbbox.addWidget(self.PlotTIButton)
        pbbox.addWidget(self.PlotANGFButton)
        pbbox.addWidget(self.PlotQButton)

        self.SaveLabel = QtWidgets.QLabel("Saving to ASCII")
        self.SaveNEButton = QtWidgets.QPushButton("NE")
        self.SaveNEButton.setEnabled(False)
        self.SaveNEButton.clicked.connect(self._save_ne_data)
        self.SaveTEButton = QtWidgets.QPushButton("TE")
        self.SaveTEButton.setEnabled(False)
        self.SaveTEButton.clicked.connect(self._save_te_data)
        self.SaveTIButton = QtWidgets.QPushButton("TI")
        self.SaveTIButton.setEnabled(False)
        self.SaveTIButton.clicked.connect(self._save_ti_data)
        self.SaveANGFButton = QtWidgets.QPushButton("ANGF")
        self.SaveANGFButton.setEnabled(False)
        self.SaveANGFButton.clicked.connect(self._save_angf_data)
        self.SaveQButton = QtWidgets.QPushButton("Q")
        self.SaveQButton.setEnabled(False)
        self.SaveQButton.clicked.connect(self._save_q_data)

        sbbox = QtWidgets.QHBoxLayout()
        sbbox.addWidget(self.SaveNEButton)
        sbbox.addWidget(self.SaveTEButton)
        sbbox.addWidget(self.SaveTIButton)
        sbbox.addWidget(self.SaveANGFButton)
        sbbox.addWidget(self.SaveQButton)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(wpbox)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.PlotSettingsList)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(csbox)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.PlotLabel)
        layout.addLayout(pbbox)
        layout.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.SaveLabel)
        layout.addLayout(sbbox)
        layout.addStretch()

        return layout

    def _set_gui_default_extraction_settings(self):

        self.EquilibriumShiftBox.setChecked(False)
        self.EquilibriumList.setCurrentIndex(3)
        self.EquilibriumUserIDEntry.setText('jetppf')
        self.EquilibriumSequenceEntry.setText('0')
        self.GrabHRTSBox.setChecked(True)
        self.HRTSUserIDEntry.setText('')
        self.HRTSSequenceEntry.setText('')
        self.GrabLIDRBox.setChecked(False)
        self.LIDRUserIDEntry.setText('')
        self.LIDRSequenceEntry.setText('')
        self.GrabKG10Box.setChecked(False)
        self.KG10UserIDEntry.setText('')
        self.KG10SequenceEntry.setText('')
#        self.GrabREFLBox.setChecked(False)
#        self.REFLUserIDEntry.setText('')
#        self.REFLSequenceEntry.setText('')
        self.GrabKK3Box.setChecked(False)
        self.KK3UserIDEntry.setText('')
        self.KK3SequenceEntry.setText('')
        self.KK3EquilibriumList.setCurrentIndex(4)
        self.KK3EquilibriumUserIDEntry.setText('')
        self.KK3EquilibriumSequenceEntry.setText('')
        self.GrabCX1Box.setChecked(False)
        self.CX1List.setCurrentIndex(0)
        self.CX1UserIDEntry.setText('')
        self.CX1SequenceEntry.setText('')
        self.GrabCX2Box.setChecked(False)
        self.CX2List.setCurrentIndex(0)
        self.CX2UserIDEntry.setText('')
        self.CX2SequenceEntry.setText('')
        self.GrabCX3Box.setChecked(False)
        self.CX3List.setCurrentIndex(0)
        self.CX3UserIDEntry.setText('')
        self.CX3SequenceEntry.setText('')
        self.GrabCX4Box.setChecked(False)
        self.CX4List.setCurrentIndex(0)
        self.CX4UserIDEntry.setText('')
        self.CX4SequenceEntry.setText('')
        self.GrabCX5Box.setChecked(False)
        self.CX5List.setCurrentIndex(0)
        self.CX5UserIDEntry.setText('')
        self.CX5SequenceEntry.setText('')
        self.GrabCX6Box.setChecked(False)
        self.CX6List.setCurrentIndex(0)
        self.CX6UserIDEntry.setText('')
        self.CX6SequenceEntry.setText('')

    def _set_gui_default_plot_settings(self):
        self.CoordinateList.setCurrentIndex(0)

    def _enable_hrts_fields(self):
        self.HRTSUserIDEntry.setEnabled(self.GrabHRTSBox.isChecked())
        self.HRTSSequenceEntry.setEnabled(self.GrabHRTSBox.isChecked())

    def _enable_lidr_fields(self):
        self.LIDRUserIDEntry.setEnabled(self.GrabLIDRBox.isChecked())
        self.LIDRSequenceEntry.setEnabled(self.GrabLIDRBox.isChecked())

    def _enable_kg10_fields(self):
        self.KG10UserIDEntry.setEnabled(self.GrabKG10Box.isChecked())
        self.KG10SequenceEntry.setEnabled(self.GrabKG10Box.isChecked())

#    def _enable_refl_fields(self):
#        self.REFLUserIDEntry.setEnabled(self.GrabREFLBox.isChecked())
#        self.REFLSequenceEntry.setEnabled(self.GrabREFLBox.isChecked())

    def _enable_kk3_fields(self):
        self.KK3UserIDEntry.setEnabled(self.GrabKK3Box.isChecked())
        self.KK3SequenceEntry.setEnabled(self.GrabKK3Box.isChecked())
        self.KK3EquilibriumList.setEnabled(self.GrabKK3Box.isChecked())
        self.KK3EquilibriumUserIDEntry.setEnabled(self.GrabKK3Box.isChecked())
        self.KK3EquilibriumSequenceEntry.setEnabled(self.GrabKK3Box.isChecked())

    def _enable_cx1_fields(self):
        self.CX1List.setEnabled(self.GrabCX1Box.isChecked())
        self.CX1UserIDEntry.setEnabled(self.GrabCX1Box.isChecked())
        self.CX1SequenceEntry.setEnabled(self.GrabCX1Box.isChecked())

    def _enable_cx2_fields(self):
        self.CX2List.setEnabled(self.GrabCX2Box.isChecked())
        self.CX2UserIDEntry.setEnabled(self.GrabCX2Box.isChecked())
        self.CX2SequenceEntry.setEnabled(self.GrabCX2Box.isChecked())

    def _enable_cx3_fields(self):
        self.CX3List.setEnabled(self.GrabCX3Box.isChecked())
        self.CX3UserIDEntry.setEnabled(self.GrabCX3Box.isChecked())
        self.CX3SequenceEntry.setEnabled(self.GrabCX3Box.isChecked())

    def _enable_cx4_fields(self):
        self.CX4List.setEnabled(self.GrabCX4Box.isChecked())
        self.CX4UserIDEntry.setEnabled(self.GrabCX4Box.isChecked())
        self.CX4SequenceEntry.setEnabled(self.GrabCX4Box.isChecked())

    def _enable_cx5_fields(self):
        self.CX5List.setEnabled(self.GrabCX5Box.isChecked())
        self.CX5UserIDEntry.setEnabled(self.GrabCX5Box.isChecked())
        self.CX5SequenceEntry.setEnabled(self.GrabCX5Box.isChecked())

    def _enable_cx6_fields(self):
        self.CX6List.setEnabled(self.GrabCX6Box.isChecked())
        self.CX6UserIDEntry.setEnabled(self.GrabCX6Box.isChecked())
        self.CX6SequenceEntry.setEnabled(self.GrabCX6Box.isChecked())

    def _extract_time_data(self):
        tdata = None
        textn = self.ShotNumberEntry.text()
        if textn:
            snum = int(textn)
            uname = None
            passwd = None
            if not currenthost.endswith(".jet.uk"):
                dialog = QtWidgets.QInputDialog()
                (value, ok) = dialog.getText(self, "Login required", "UKAEA username:")
                if ok and value:
                    uname = value
                    (value, ok) = dialog.getText(self, "Password required", "UKAEA SecureID:", QtWidgets.QLineEdit.Password)
                    if ok and value:
                        passwd = value
            exnamelist = dict()
            exnamelist["SHOTINFO"] = "%8d" % (snum)
            exnamelist["INTERFACE"] = 'sal'
            exnamelist["USERNAME"] = uname
            exnamelist["PASSWORD"] = passwd
            try:
                print("Time trace extraction started...")
                tic = time.perf_counter()
                tdatatemp = tmod.get_time_data(namelist=exnamelist)
                if isinstance(tdatatemp, dict) and "TIP" in tdatatemp and tdatatemp["TIP"] is not None:
                    tdata = copy.deepcopy(tdatatemp)
                else:
                    raise TypeError("Unknown extraction failure within machine-specific adapter.")
                toc = time.perf_counter()
                print("Time trace extraction finished. Time elapsed: %.6f" % (toc - tic))
            except Exception as e:
                print(repr(e))
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setWindowTitle("Fatal Error")
                msg.setText("Time trace extraction procedure failed for selected discharge.")
                msg.setFont(self._font)
                msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Entry Error")
            msg.setText("Unrecognized input data.")
            msg.setFont(self._font)
            msg.exec_()
        return tdata

    def _plot_time_traces(self):
        self.TimeTracePlot = None
        tdata = self._extract_time_data()
        text1 = self.TimeStartEntry.text()
        text2 = self.TimeEndEntry.text()
        t1 = float(text1) if text1 else None
        t2 = float(text2) if text2 else None
        if tdata is not None:
            self.TimeTracePlot = time_trace_plotter(tdata, t1, t2, fontsize=int(self._font_height / 1.5))
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Critical)
            msg.setWindowTitle("Fatal Error")
            msg.setText("Data extraction failed!")
            msg.setFont(self._font)
            msg.exec_()

    def _select_data(self):
        item = self.DatabaseList.currentItem()
        pidx = self.TabPanel.currentIndex()
        if pidx == 0:
            data_list = item.get_data_list()
            default_uid = 'jetppf'
            default_seq = '0'
            if data_list is not None:
                self._set_gui_default_extraction_settings()
                self.ShotNumberEntry.setText("%d" % (item.get_shot_number()))
                self.TimeStartEntry.setText("%.4f" % (item.get_time_window_start()))
                self.TimeEndEntry.setText("%.4f" % (item.get_time_window_end()))
                eqdict = {'efit': 4, 'eftp': 3, 'eftf': 2, 'eftm': 1, 'ehtr': 0}
                eqidx = 3
                equid = default_uid
                eqseq = default_seq
                for key, idx in eqdict.items():
                    if key+'/xip' in data_list:
                        eqidx = eqdict[key]
                        eqseq = data_list[key+'/xip'][2]
                        equid = data_list[key+'/xip'][3]
                self.EquilibriumList.setCurrentIndex(eqidx)
                self.EquilibriumUserIDEntry.setText(equid.strip().lower())
                self.EquilibriumSequenceEntry.setText(eqseq.strip())
                hrtskey = None
                lidrkey = None
                kg10key = None
                reflkey = None
                kk3key = None
                cxsmkey = None
                cxdmkey = None
                cxfmkey = None
                cxgmkey = None
                cxhmkey = None
                cxjmkey = None
                cxkmkey = None
                cxlmkey = None
                cxs4key = None
                cxd4key = None
                cxf4key = None
                cxg4key = None
                cxh4key = None
                cxs6key = None
                cxd6key = None
                cxf6key = None
                cxg6key = None
                cxh6key = None
                cxs8key = None
                cxd8key = None
                cxf8key = None
                cxg8key = None
                cxh8key = None
                cx7akey = None
                cx7bkey = None
                cx7ckey = None
                cx7dkey = None
                cxsekey = None
                for key in data_list:
                    if key.startswith('hrts/'):
                        hrtskey = key
                    if key.startswith('lidr/'):
                        lidrkey = key
                    if key.startswith('kg10/'):
                        kg10key = key
                    if key.startswith('refl/'):
                        reflkey = key
                    if key.startswith('kk3/'):
                        kk3key = key
                    if key.startswith('cxsm/'):
                        cxsmkey = key
                    if key.startswith('cxdm/'):
                        cxdmkey = key
                    if key.startswith('cxfm/'):
                        cxfmkey = key
                    if key.startswith('cxgm/'):
                        cxgmkey = key
                    if key.startswith('cxhm/'):
                        cxhmkey = key
                    if key.startswith('cxjm/'):
                        cxjmkey = key
                    if key.startswith('cxkm/'):
                        cxkmkey = key
                    if key.startswith('cxlm/'):
                        cxlmkey = key
                    if key.startswith('cxs4/'):
                        cxs4key = key
                    if key.startswith('cxd4/'):
                        cxd4key = key
                    if key.startswith('cxf4/'):
                        cxf4key = key
                    if key.startswith('cxg4/'):
                        cxg4key = key
                    if key.startswith('cxh4/'):
                        cxh4key = key
                    if key.startswith('cxs6/'):
                        cxs6key = key
                    if key.startswith('cxd6/'):
                        cxd6key = key
                    if key.startswith('cxf6/'):
                        cxf6key = key
                    if key.startswith('cxg6/'):
                        cxg6key = key
                    if key.startswith('cxh6/'):
                        cxh6key = key
                    if key.startswith('cxs8/'):
                        cxs8key = key
                    if key.startswith('cxd8/'):
                        cxd8key = key
                    if key.startswith('cxf8/'):
                        cxf8key = key
                    if key.startswith('cxg8/'):
                        cxg8key = key
                    if key.startswith('cxh8/'):
                        cxh8key = key
                    if key.startswith('cx7a/'):
                        cx7akey = key
                    if key.startswith('cx7b/'):
                        cx7bkey = key
                    if key.startswith('cx7c/'):
                        cx7ckey = key
                    if key.startswith('cx7d/'):
                        cx7dkey = key
                    if key.startswith('cxse/'):
                        cxsekey = key
                if hrtskey is not None:
                    self.GrabHRTSBox.setChecked(True)
                    self.HRTSUserIDEntry.setText(data_list[hrtskey][3].strip().lower())
                    self.HRTSSequenceEntry.setText(data_list[hrtskey][2].strip().lower())
                if lidrkey is not None:
                    self.GrabLIDRBox.setChecked(True)
                    self.LIDRUserIDEntry.setText(data_list[lidrkey][3].strip().lower())
                    self.LIDRSequenceEntry.setText(data_list[lidrkey][2].strip().lower())
                if kg10key is not None:
                    self.GrabKG10Box.setChecked(True)
                    self.KG10UserIDEntry.setText(data_list[kg10key][3].strip().lower())
                    self.KG10SequenceEntry.setText(data_list[kg10key][2].strip().lower())
#                if reflkey is not None:
#                    self.GrabREFLBox.setChecked(True)
#                    self.REFLUserIDEntry.setText(data_list[reflkey][3].strip().lower())
#                    self.REFLSequenceEntry.setText(data_list[reflkey][2].strip().lower())
                if kk3key is not None:
                    self.GrabKK3Box.setChecked(True)
                    self.KK3UserIDEntry.setText(data_list[kk3key][3].strip().lower())
                    self.KK3SequenceEntry.setText(data_list[kk3key][2].strip().lower())
                    keqdict = {'ehtr': 0, 'eftm': 1, 'eftf': 2, 'eftp': 3, 'efit': 4}
                    keqidx = 4
                    kequid = default_uid
                    keqseq = default_seq
                    for key, idx in keqdict.items():
                        if key+'/xip' in data_list:
                            keqidx = keqdict[key]
                            keqseq = data_list[key+'/xip'][2]
                            kequid = data_list[key+'/xip'][3]
                    self.KK3EquilibriumList.setCurrentIndex(keqidx)
                    self.KK3EquilibriumUserIDEntry.setText(kequid.strip().lower())
                    self.KK3EquilibriumSequenceEntry.setText(keqseq.strip().lower())
                cxwidgets = [(self.GrabCX1Box, self.CX1List, self.CX1UserIDEntry, self.CX1SequenceEntry),
                             (self.GrabCX2Box, self.CX2List, self.CX2UserIDEntry, self.CX2SequenceEntry),
                             (self.GrabCX3Box, self.CX3List, self.CX3UserIDEntry, self.CX3SequenceEntry),
                             (self.GrabCX4Box, self.CX4List, self.CX4UserIDEntry, self.CX4SequenceEntry),
                             (self.GrabCX5Box, self.CX5List, self.CX5UserIDEntry, self.CX5SequenceEntry),
                             (self.GrabCX6Box, self.CX6List, self.CX6UserIDEntry, self.CX6SequenceEntry)]
                ncx = 0
                if cxsmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(0)
                    cxwidgets[ncx][2].setText(data_list[cxsmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxsmkey][2].strip().lower())
                    ncx += 1
                if cxdmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(1)
                    cxwidgets[ncx][2].setText(data_list[cxdmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxdmkey][2].strip().lower())
                    ncx += 1
                if cxfmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(2)
                    cxwidgets[ncx][2].setText(data_list[cxfmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxfmkey][2].strip().lower())
                    ncx += 1
                if cxgmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(3)
                    cxwidgets[ncx][2].setText(data_list[cxgmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxgmkey][2].strip().lower())
                    ncx += 1
                if cxhmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(4)
                    cxwidgets[ncx][2].setText(data_list[cxhmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxhmkey][2].strip().lower())
                    ncx += 1
                if cxjmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(5)
                    cxwidgets[ncx][2].setText(data_list[cxjmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxjmkey][2].strip().lower())
                    ncx += 1
                if cxkmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(6)
                    cxwidgets[ncx][2].setText(data_list[cxkmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxkmkey][2].strip().lower())
                    ncx += 1
                if cxlmkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(7)
                    cxwidgets[ncx][2].setText(data_list[cxlmkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxlmkey][2].strip().lower())
                    ncx += 1
                if cxs4key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(8)
                    cxwidgets[ncx][2].setText(data_list[cxs4key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxs4key][2].strip().lower())
                    ncx += 1
                if cxd4key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(9)
                    cxwidgets[ncx][2].setText(data_list[cxd4key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxd4key][2].strip().lower())
                    ncx += 1
                if cxf4key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(10)
                    cxwidgets[ncx][2].setText(data_list[cxf4key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxf4key][2].strip().lower())
                    ncx += 1
                if cxg4key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(11)
                    cxwidgets[ncx][2].setText(data_list[cxg4key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxg4key][2].strip().lower())
                    ncx += 1
                if cxh4key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(12)
                    cxwidgets[ncx][2].setText(data_list[cxh4key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxh4key][2].strip().lower())
                    ncx += 1
                if cxs6key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(13)
                    cxwidgets[ncx][2].setText(data_list[cxs6key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxs6key][2].strip().lower())
                    ncx += 1
                if cxd6key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(14)
                    cxwidgets[ncx][2].setText(data_list[cxd6key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxd6key][2].strip().lower())
                    ncx += 1
                if cxf6key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(15)
                    cxwidgets[ncx][2].setText(data_list[cxf6key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxf6key][2].strip().lower())
                    ncx += 1
                if cxg6key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(16)
                    cxwidgets[ncx][2].setText(data_list[cxg6key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxg6key][2].strip().lower())
                    ncx += 1
                if cxh6key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(17)
                    cxwidgets[ncx][2].setText(data_list[cxh6key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxh6key][2].strip().lower())
                    ncx += 1
                if cxs8key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(18)
                    cxwidgets[ncx][2].setText(data_list[cxs8key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxs8key][2].strip().lower())
                    ncx += 1
                if cxd8key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(19)
                    cxwidgets[ncx][2].setText(data_list[cxd8key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxd8key][2].strip().lower())
                    ncx += 1
                if cxf8key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(20)
                    cxwidgets[ncx][2].setText(data_list[cxf8key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxf8key][2].strip().lower())
                    ncx += 1
                if cxg8key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(21)
                    cxwidgets[ncx][2].setText(data_list[cxg8key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxg8key][2].strip().lower())
                    ncx += 1
                if cxh8key is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(22)
                    cxwidgets[ncx][2].setText(data_list[cxh8key][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxh8key][2].strip().lower())
                    ncx += 1
                if cx7akey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(23)
                    cxwidgets[ncx][2].setText(data_list[cx7akey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cx7akey][2].strip().lower())
                    ncx += 1
                if cx7bkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(24)
                    cxwidgets[ncx][2].setText(data_list[cx7bkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cx7bkey][2].strip().lower())
                    ncx += 1
                if cx7ckey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(25)
                    cxwidgets[ncx][2].setText(data_list[cx7ckey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cx7ckey][2].strip().lower())
                    ncx += 1
                if cx7dkey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(26)
                    cxwidgets[ncx][2].setText(data_list[cx7dkey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cx7dkey][2].strip().lower())
                    ncx += 1
                if cxsekey is not None and ncx < len(cxwidgets):
                    cxwidgets[ncx][0].setChecked(True)
                    cxwidgets[ncx][1].setCurrentIndex(27)
                    cxwidgets[ncx][2].setText(data_list[cxsekey][3].strip().lower())
                    cxwidgets[ncx][3].setText(data_list[cxsekey][2].strip().lower())
                    ncx += 1
            else:
                self._set_gui_default_extraction_settings()
        elif pidx == 1:
            data = item.get_data()
            if data is not None:
                tidx = self.PlotSettingsList.rowCount()
                NameItem = QtWidgets.QTableWidgetItem(item.text())
                NameItem.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                LabelItem = QtWidgets.QTableWidgetItem('')
                MarkerItem = QtWidgets.QTableWidgetItem('')
                ColorItem = QtWidgets.QTableWidgetItem('')
                self.PlotSettingsList.insertRow(tidx)
                self.PlotSettingsList.setItem(tidx, 0, NameItem)
                self.PlotSettingsList.setItem(tidx, 1, LabelItem)
                self.PlotSettingsList.setItem(tidx, 2, MarkerItem)
                self.PlotSettingsList.setItem(tidx, 3, ColorItem)
                self._enable_plotting()
            else:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setWindowTitle("Input Error")
                msg.setText("Selected time window data not extracted yet.")
                msg.setFont(self._font)
                msg.exec_()

    def _add_time_window(self):
        shot_str = self.ShotNumberEntry.text()
        ti_str = self.TimeStartEntry.text()
        tf_str = self.TimeEndEntry.text()
        data_list = self._make_get_list()
        if shot_str and ti_str and tf_str:
            if data_list is not None:
                idx = self.DatabaseList.rowCount()
                shot = int(shot_str)
                ti = float(ti_str)
                tf = float(tf_str)
                item = QTableWidgetTimeWindow(shot, ti, tf, idx)
                item.set_data_list(data_list)
                self.DatabaseList.insertRow(idx)
                self.DatabaseList.setItem(idx, 0, item)
                self.DatabaseList.setCurrentCell(idx, 0)
            else:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setWindowTitle("Input Error")
                msg.setText("No experimental data was requested.")
                msg.setFont(self._font)
                msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Input Error")
            msg.setText("Shot specification is incomplete.")
            msg.setFont(self._font)
            msg.exec_()
        window_present = self.DatabaseList.rowCount() > 0
        self.DeleteWindowButton.setEnabled(window_present)
        self.ExtractAllWindowsButton.setEnabled(window_present)
        self.ExtractWindowButton.setEnabled(window_present)

    def _delete_time_window(self):
        idx = self.DatabaseList.currentRow()
        item = self.DatabaseList.currentItem()
        del item
        self.DatabaseList.removeRow(idx)
        total_rows = self.DatabaseList.rowCount()
        window_present = total_rows > 0
        if window_present:
            nidx = idx if idx < total_rows else idx - 1
            self.DatabaseList.setCurrentCell(nidx, 0)
        self.DeleteWindowButton.setEnabled(window_present)
        self.ExtractAllWindowsButton.setEnabled(window_present)
        self.ExtractWindowButton.setEnabled(window_present)

    def _extract_all_time_windows(self):
        orig_idx = self.DatabaseList.currentRow()
        numitems = self.DatabaseList.rowCount()
        for ii in np.arange(0, numitems):
            self.DatabaseList.setCurrentCell(ii, 0)
            self._extract_time_window()
        self.DatabaseList.setCurrentCell(orig_idx, 0)

    def _extract_time_window(self):
        shot = self.DatabaseList.currentItem().get_shot_number()
        ti = self.DatabaseList.currentItem().get_time_window_start()
        tf = self.DatabaseList.currentItem().get_time_window_end()
        getdict = self.DatabaseList.currentItem().get_data_list()
        fulldict= None
        if shot is not None and ti is not None and tf is not None and getdict is not None:
            uname = None
            passwd = None
            if not currenthost.endswith(".jet.uk"):
                dialog = QtWidgets.QInputDialog()
                (value, ok) = dialog.getText(self, "Login required", "UKAEA username:")
                if ok and value:
                    uname = value
                    (value, ok) = dialog.getText(self, "Password required", "UKAEA SecureID:", QtWidgets.QLineEdit.Password)
                    if ok and value:
                        passwd = value
            exnamelist = dict()
            exnamelist["GPCOORDFLAG"] = True
            exnamelist["RSHIFTFLAG"] = self.EquilibriumShiftBox.isChecked()
            exnamelist["SELECTEQ"] = self.EquilibriumList.currentText().lower()
            try:
                print("Time window extraction started...")
                tic = time.perf_counter()
                extras = {}
                if not currenthost.endswith(".jet.uk"):
                    extras["uid"] = uname
                    extras["sid"] = passwd
                rawdict = gmod.get_data_with_dict(shot, ti, tf, getdict, **extras)[0]
                gpc = exnamelist["GPCOORDFLAG"] if "GPCOORDFLAG" in exnamelist else False
                sf = exnamelist["RSHIFTFLAG"] if "RSHIFTFLAG" in exnamelist else False
                eqselect = exnamelist["SELECTEQ"] if "SELECTEQ" in exnamelist else False
                datadict = pmod.transfer_generic_data(rawdict, newstruct=None, userz=True, gpcoord=gpc, useshift=sf, usenscale=True, useece=True, userefl=True)
                datadict = pmod.define_equilibrium(rawdict, newstruct=datadict, equilibrium=eqselect)
                datadict = pmod.define_time_filter(rawdict, newstruct=datadict, elmfilt=True)
                datadict = pmod.unpack_general_data(rawdict, newstruct=datadict)
                datadict = pmod.unpack_coord_data(rawdict, newstruct=datadict)
                datadict = pmod.calculate_coordinate_systems(datadict)
                datadict = pmod.unpack_profile_data(rawdict, newstruct=datadict, fdebug=True)
                datadict = pmod.apply_data_corrections(datadict)
                diagdict = smod.standardize_diagnostic_data(datadict, newstruct=None, useexp=False)
                stddict = smod.transfer_generic_data(datadict, newstruct=None, usepol=False, usexeb=False, useexp=False)
                stddict = smod.filter_standardized_diagnostic_data(diagdict, newstruct=stddict, userscales={}, fdebug=True)
                stddict = smod.combine_edge_diagnostic_data(stddict)
                data_fields_keep = ['DEVICE', 'SHOT', 'T1', 'T2', 'HMODEFLAG', 'USERZFLAG', 'ELMFILTFLAG', 'RSHIFTFLAG', 'GPCOORDFLAG', 'USENSCALEFLAG', \
                                    'RMAG', 'RMAGEB', 'RMAGDS', 'ZMAG', 'ZMAGEB', 'ZMAGDS', 'RGEO', 'RGEOEB', 'RGEODS', 'ZGEO', 'ZGEOEB', 'ZGEODS', \
                                    'IP', 'IPEB', 'IPDS', 'CRMAG', 'CRMAGEB', 'CRGEO', 'CRGEOEB', 'RVAC', 'RVACEB', 'RVACDS', 'BVAC', 'BVACEB', 'BVACDS', \
                                    'FBND', 'FBNDEB', 'FBNDDS', 'FAXS', 'FAXSEB', 'FAXSDS', 'VLOOP', 'VLOOPEB', 'VLOOPDS', 'RMAJSHIFT', 'RMAJSHIFTEB',
                                    'PNBI', 'PNBIEB', 'PNBIDS', 'LNBI', 'LNBIEB', 'LNBIDS',
                                    'PICRH', 'PICRHEB', 'PICRHDS', 'LICRH', 'LICRHEB', 'LICRHDS',
                                    'PECRH', 'PECRHEB', 'PECRHDS', 'LECRH', 'LECRHEB', 'LECRHDS',
                                    'PLH', 'PLHEB', 'PLHDS', 'LLH', 'LLHEB', 'LLHDS',
                                    'POHM', 'POHMEB', 'POHMDS', 'LOHM', 'LOHMEB', 'LOHMDS', 'PRAD', 'PRADEB', 'PRADDS', \
                                    'ZEFV', 'ZEFVEB', 'ZEFVDS', 'ZEFH', 'ZEFHEB', 'ZEFHDS', 'NEUT', 'NEUTEB', 'NEUTDS', 'PHTHR', 'PHTHRDS', \
                                    'NEDIAG', 'NIDIAG', 'TEDIAG', 'TIDIAG', 'AFDIAG', 'TIMPDIAG', 'QDIAG', 'ZEFFDIAG', 'NIMPLIST', \
                                    'NEEBMULT', 'TEEBMULT', 'NIEBMULT', 'TIEBMULT', 'NIMPEBMULT', 'TIMPEBMULT', 'AFEBMULT', 'QEBMULT', 'ZEFFEBMULT', \
                                    'NEDS', 'NIDS', 'TEDS', 'TIDS', 'AFDS', 'TIMPDS', 'QDS', 'ZEFFDS', 'NIMPDSLIST', 'ZIMPDSLIST', \
                                    'NERAW', 'NERAWEB', 'NERAWX', 'NERAWXEB', 'NEDMAP', \
                                    'TERAW', 'TERAWEB', 'TERAWX', 'TERAWXEB', 'TEDMAP', \
                                    'TIRAW', 'TIRAWEB', 'TIRAWX', 'TIRAWXEB', 'TIDMAP', \
                                    'TIMPRAW', 'TIMPRAWEB', 'TIMPRAWX', 'TIMPRAWXEB', 'TIMPDMAP', \
                                    'AFRAW', 'AFRAWEB', 'AFRAWX', 'AFRAWXEB', 'AFDMAP', \
                                    'QRAW', 'QRAWEB', 'QRAWX', 'QRAWXEB', 'QDMAP']
                fulldict = {}
                for key in stddict:
                    if key in data_fields_keep or key.startswith('CS_') or key.startswith('CJ_') or key.startswith('FS_'):
                        fulldict[key] = stddict[key]
                toc = time.perf_counter()
                print("Time window extraction finished. Time elapsed: %.6f" % (toc - tic))
            except Exception as e:
                print(repr(e))
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setWindowTitle("Fatal Error")
                msg.setText("Profile extraction procedure failed for selected discharge.")
                msg.setFont(self._font)
                msg.exec_()
        if fulldict is not None:
            self.DatabaseList.currentItem().set_data(fulldict)
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Entry Error")
            msg.setText("Unrecognized input data.")
            msg.setFont(self._font)
            msg.exec_()

    def _make_get_list(self):
        getlist = None
        extract_list = []
        if self.GrabHRTSBox.isChecked():
            uid = self.HRTSUserIDEntry.text()
            if not uid:
               uid = 'jetppf'
            seq = self.HRTSSequenceEntry.text()
            if not seq:
                seq = '0'
            extract_list.extend([('hrts', uid, seq), ('hrtx', 'jetppf', '0'), ('kg1v', 'jetppf', '0')])
        if self.GrabLIDRBox.isChecked():
            uid = self.LIDRUserIDEntry.text()
            if not uid:
                uid = 'jetppf'
            seq = self.LIDRSequenceEntry.text()
            if not seq:
                seq = '0'
            extract_list.extend([('lidr', uid, seq)])
        if self.GrabKG10Box.isChecked():
            uid = self.KG10UserIDEntry.text()
            if not uid:
                uid = 'jetppf'
            seq = self.KG10SequenceEntry.text()
            if not seq:
                seq = '0'
            extract_list.extend([('kg10', uid, seq)])
#        if self.GrabREFLBox.isChecked():
#            uid = self.REFLUserIDEntry.text()
#            if not uid:
#                uid = 'jetppf'
#            seq = self.REFLSequenceEntry.text()
#            if not seq:
#                seq = '0'
#            extract_list.extend([('refl', uid, seq)])
        if self.GrabKK3Box.isChecked():
            uid = self.KK3UserIDEntry.text()
            if not uid:
                uid = 'jetppf'
            seq = self.KK3SequenceEntry.text()
            if not seq:
                seq = '0'
            kk3eq = self.KK3EquilibriumList.currentText().lower()
            euid = self.KK3EquilibriumUserIDEntry.text()
            if not euid:
                euid = 'jetppf'
            eseq = self.KK3EquilibriumSequenceEntry.text()
            if not eseq:
                eseq = '0'
            extract_list.extend([('kk3', uid, seq), (kk3eq, euid, eseq)])
        cx_entry_list = []
        if self.GrabCX1Box.isChecked():
            cxtag = self.CX1List.currentText().lower()
            if cxtag in data_fields:
                cx_entry_list.append((cxtag, self.CX1UserIDEntry.text(), self.CX1SequenceEntry.text()))
        if self.GrabCX2Box.isChecked():
            cxtag = self.CX2List.currentText().lower()
            if cxtag in data_fields:
                cx_entry_list.append((cxtag, self.CX2UserIDEntry.text(), self.CX2SequenceEntry.text()))
        if self.GrabCX3Box.isChecked():
            cxtag = self.CX3List.currentText().lower()
            if cxtag in data_fields:
                cx_entry_list.append((cxtag, self.CX3UserIDEntry.text(), self.CX3SequenceEntry.text()))
        if self.GrabCX4Box.isChecked():
            cxtag = self.CX4List.currentText().lower()
            if cxtag in data_fields:
                cx_entry_list.append((cxtag, self.CX4UserIDEntry.text(), self.CX4SequenceEntry.text()))
        if self.GrabCX5Box.isChecked():
            cxtag = self.CX5List.currentText().lower()
            if cxtag in data_fields:
                cx_entry_list.append((cxtag, self.CX5UserIDEntry.text(), self.CX5SequenceEntry.text()))
        if self.GrabCX6Box.isChecked():
            cxtag = self.CX6List.currentText().lower()
            if cxtag in data_fields:
                cx_entry_list.append((cxtag, self.CX6UserIDEntry.text(), self.CX6SequenceEntry.text()))
        for key, uid, seq in cx_entry_list:
            xuid = uid if uid else 'jetppf'
            xseq = seq if seq else '0'
            extract_list.append((key, xuid, xseq))
        if len(extract_list) > 0:
            eqtag = self.EquilibriumList.currentText().lower()
            uid = self.EquilibriumUserIDEntry.text()
            if not uid:
                uid = 'jetppf'
            seq = self.EquilibriumSequenceEntry.text()
            if not seq:
                seq = '0'
            extract_list.extend([(eqtag, uid, seq), ('edg7', 'jetppf', '0'), ('edg8', 'jetppf', '0')])
        for item in extract_list:
            key = item[0]
            uid = item[1]
            seq = item[2]
            if key in self._preset_fields:
                value = self._preset_fields[key]
                if getlist is None:
                    getlist = dict()
                for kk in range(len(value)):
                    getlist[key+"/"+value[kk][0]] = ["---", "%d" % (value[kk][1]), seq, uid]
        return getlist

    def _enable_plotting(self):
        plot_ready = self.PlotSettingsList.rowCount() > 0
        self.CoordinateList.setEnabled(plot_ready)
        self.PlotNEButton.setEnabled(plot_ready)
        self.PlotTEButton.setEnabled(plot_ready)
        self.PlotTIButton.setEnabled(plot_ready)
        self.PlotANGFButton.setEnabled(plot_ready)
        self.PlotQButton.setEnabled(plot_ready)
        self.SaveNEButton.setEnabled(plot_ready)
        self.SaveTEButton.setEnabled(plot_ready)
        self.SaveTIButton.setEnabled(plot_ready)
        self.SaveANGFButton.setEnabled(plot_ready)
        self.SaveQButton.setEnabled(plot_ready)

    def _add_all_windows_to_plot(self):
        orig_idx = self.DatabaseList.currentRow()
        numitems = self.DatabaseList.rowCount()
        for ii in np.arange(0, numitems):
            self.DatabaseList.setCurrentCell(ii, 0)
            self._add_selected_window_to_plot()
        self.DatabaseList.setCurrentCell(orig_idx, 0)
        self._enable_plotting()

    def _add_selected_window_to_plot(self):
        item = self.DatabaseList.currentItem()
        data = item.get_data()
        if data is not None:
            tidx = self.PlotSettingsList.rowCount()
            NameItem = QtWidgets.QTableWidgetItem(item.text())
            NameItem.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            LabelItem = QtWidgets.QTableWidgetItem('')
            MarkerItem = QtWidgets.QTableWidgetItem('')
            ColorItem = QtWidgets.QTableWidgetItem('')
            self.PlotSettingsList.insertRow(tidx)
            self.PlotSettingsList.setItem(tidx, 0, NameItem)
            self.PlotSettingsList.setItem(tidx, 1, LabelItem)
            self.PlotSettingsList.setItem(tidx, 2, MarkerItem)
            self.PlotSettingsList.setItem(tidx, 3, ColorItem)
            self._enable_plotting()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Input Error")
            msg.setText("Selected time window data not extracted yet.")
            msg.setFont(self._font)
            msg.exec_()

    def _delete_selected_window_from_plot(self):
        idx = self.PlotSettingsList.currentRow()
        tidx = self.PlotSettingsList.currentColumn()
        cidx = self.PlotSettingsList.columnCount()
        for ii in range(cidx):
            self.PlotSettingsList.setCurrentCell(idx, ii)
            item = self.PlotSettingsList.currentItem()
            del item
        self.PlotSettingsList.removeRow(idx)
        total_rows = self.PlotSettingsList.rowCount()
        if total_rows > 0:
            nidx = idx if idx < total_rows else idx - 1
            self.PlotSettingsList.setCurrentCell(nidx, tidx)
        self._enable_plotting()

    def _plot_experimental_data(self, varlist=None, explist=None):
        if isinstance(varlist, (list, tuple)):
            datalist = []
            labellist = []
            stylelist = []
            colorlist = []
            jj = self.CoordinateList.currentIndex()
            nprows = self.PlotSettingsList.rowCount()
            nwrows = self.DatabaseList.rowCount()
            elist = [0]*len(varlist) if explist is None else explist
            for ii in range(nprows):
                idata = None
                for dd in range(nwrows):
                    if self.DatabaseList.item(dd, 0).text() == self.PlotSettingsList.item(ii, 0).text().strip():
                        idata = self.DatabaseList.item(dd, 0).get_data()
                if idata is not None:
                    datalist.append(idata)
                    ilabel = self.PlotSettingsList.item(ii, 1).text().strip()
                    ulabel = ilabel if ilabel else self.PlotSettingsList.item(ii, 0).text().strip()
                    labellist.append(ulabel)
                    istyle = self.PlotSettingsList.item(ii, 2).text().strip()
                    ustyle = istyle if istyle else None
                    stylelist.append(ustyle)
                    icolor = self.PlotSettingsList.item(ii, 3).text().strip()
                    ucolor = icolor if icolor else None
                    colorlist.append(ucolor)
            fstyle = False
            for item in stylelist:
                if item is not None:
                    fstyle = True
            if not fstyle:
                stylelist = None
            fcolor = False
            for item in colorlist:
                if item is not None:
                    fcolor = True
            if not fcolor:
                colorlist = None
            self.ProfilePlot = profile_plotter(datalist, varlist, sigma=1.0, csplot=self.clist[jj], exponents=elist, fontsize=int(self._font_height / 1.5), labels=labellist, styles=stylelist, colors=colorlist)

    def _save_experimental_data(self, varlist=None):
        nprows = self.PlotSettingsList.rowCount()
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename and isinstance(varlist, (list, tuple)) and nprows > 0:
            jj = self.CoordinateList.currentIndex()
            nwrows = self.DatabaseList.rowCount()
            datalist = []
            for ii in range(nprows):
                idata = None
                for dd in range(nwrows):
                    if self.DatabaseList.item(dd, 0).text() == self.PlotSettingsList.item(ii, 0).text().strip():
                        idata = self.DatabaseList.item(dd, 0).get_data()
                fhere = False
                for var in varlist:
                    if idata is not None and var+"RAW" in idata and len(idata[var+"RAW"]) > 1:
                        fhere = True
                if fhere:
                    datalist.append(idata)
            ocs = self.clist[jj]
            with open(filename, 'w') as ff:
                ff.write("### EX2GK - Raw Data Save File ###\n")
                ff.write("\n")
                for kk in range(len(datalist)):
                    data = datalist[kk]
                    ff.write("### %s %d : %.4f - %.4f ###\n" % (data["DEVICE"], data["SHOT"], data["T1"], data["T2"]))
                    for var in varlist:
                        if isinstance(var, str) and var+"RAW" in data:
                            ics = data["CS_DIAG"]
                            xraw = data[var+"RAWX"].flatten()
#                            xeraw = data[var+"RAWXEB"].flatten()
                            yraw = data[var+"RAW"].flatten()
                            yeraw = data[var+"RAWEB"].flatten()
                            ydiag = data[var+"DMAP"].flatten()
                            if ics != ocs:
                                jcs = data["CJ_BASE"]
                                ivvec = data["CS_"+ics]
                                ovvec = data["CS_"+ocs]
                                ijvec = data["CJ_"+jcs+"_"+ics] if "CJ_"+jcs+"_"+ics in data else None
                                ojvec = data["CJ_"+jcs+"_"+ocs] if "CJ_"+jcs+"_"+ocs in data else None
                                oevec = None
                                (xraw, dj, txeraw) = ptools.convert_base_coords(xraw, ivvec, ovvec, ijvec, ojvec, oevec)
#                                xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                            ff.write("%20s%20s%20s%20s\n" % (ocs, var+" Raw", "Err. "+var, "Origin"))
                            for ii in range(len(xraw)):
                                ddiag = data[var+"DIAG"][int(ydiag[ii])]
                                ff.write("%20.6f%20.6e%20.6e%20s\n" % (xraw[ii], yraw[ii], yeraw[ii], ddiag))
                    ff.write("\n")
            print(" Experimental %s data written into %s." % (", ".join(varlist), filename))

    def _plot_ne_data(self):
        self._plot_experimental_data(["NE"], [19])

    def _plot_te_data(self):
        self._plot_experimental_data(["TE"], [3])

    def _plot_ti_data(self):
        self._plot_experimental_data(["TI", "TIMP"], [3, 3])

    def _plot_angf_data(self):
        self._plot_experimental_data(["AF"], [3])

    def _plot_q_data(self):
        self._plot_experimental_data(["Q"], [0])

    def _save_ne_data(self):
        self._save_experimental_data(["NE"])

    def _save_te_data(self):
        self._save_experimental_data(["TE"])

    def _save_ti_data(self):
        self._save_experimental_data(["TI", "TIMP"])

    def _save_angf_data(self):
        self._save_experimental_data(["AF"])

    def _save_q_data(self):
        self._save_experimental_data(["Q"])

def main():

    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName('JET Experimental Data Viewer')
    tfont = QtGui.QFont('Sans Serif')
    if fqt5:
        tfont = QtGui.QFont('FreeSans')
    tfont.setStyleHint(QtGui.QFont.SansSerif)
    ifont = QtGui.QFontInfo(tfont)
    print("Default font: %s" % (ifont.family()))
    app.setFont(tfont)
    ex = JETExperimentDataViewer(ppfIsHere)

    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
