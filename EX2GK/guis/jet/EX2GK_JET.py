#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

# Required imports
import os
import sys
import socket
import datetime
import re
import pwd
import time
import copy
import pickle
import collections
import importlib
import inspect
import packaging.version as pversion
import numpy as np

ppfIsHere = True
try:
    import ppf
except ImportError:
    ppfIsHere = False

imasIsHere = True
try:
    import imas
except ImportError:
    imasIsHere = False

from IPython import embed
QtCore = None
QtGui = None
QtWidgets = None
try:
    from PyQt4 import QtCore, QtGui
    QtWidgets = QtGui
except ImportError:
    from PyQt5 import QtCore, QtGui, QtWidgets
pyqtversion = QtCore.PYQT_VERSION_STR if QtCore is not None else "0"

import matplotlib
old_mpl = pversion.parse(matplotlib.__version__) <= pversion.parse("2.0.0")
fqt5 = pversion.parse(pyqtversion) >= pversion.parse("5.0.0")
mplqt = None
if fqt5:
    matplotlib.use("Qt5Agg")
    from matplotlib.backends import backend_qt5agg as mplqt
else:
    try:
        matplotlib.use("Qt4Agg")
        from matplotlib.backends import backend_qt4agg as mplqt
    except ValueError:
        matplotlib.use("QtAgg")
        from matplotlib.backends import backend_qtagg as mplqt
from matplotlib import figure as mplfig

if QtCore is None or QtGui is None or QtWidgets is None or mplqt is None:
    raise ImportError("Python install does not have Qt support! GUI cannot be used!")

from EX2GK import EX2GK, __version__


currenthost = socket.getfqdn()
currentdir = os.getcwd()
customfilters = None
fcustomincurrent = False
if os.path.isfile(currentdir+'/EX2GK_custom_filters.py'):
    spec = importlib.util.spec_from_file_location('EX2GK_custom_filters', currentdir+'/EX2GK_custom_filters.py')
    customfilters = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(customfilters)
    fcustomincurrent = True
if customfilters is None:
    try:
        customfilters = importlib.import_module('EX2GK_custom_filters')
    except (ImportError, AttributeError):
        try:
            customfilters = importlib.import_module('.EX2GK_custom_filters', package='EX2GK')
        except:
            pass


jpf_fields = ['ah', 'da', 'db', 'dc', 'dd', 'de', 'df', 'dg', 'dh', 'di', 'dj',
              'gs', 'pf', 'pl', 'rf', 'sa', 'sc', 'ss', 'tf', 'vc', 'yc']

data_fields = {"*"   : {# Line-integrated effective charge
                        "ks3" : [("zefv",1), ("zefh",1), ("dzfv",1), ("dzfh",1)],
                        # TRANSP effective charge
                        "trau": [("zef",1)],
                        # Effective charge profile from soft x-ray analysis
                        "wsxp": [("zeff",1)],
                        # Hydrogen fuel composition
                        "ks3b": [("hthd",1), ("dthd",1), ("tttd",1), ("dhth",1), ("ddth",1), ("dttt",1)],
                        "kt5p": [("hthd",1), ("dthd",1), ("tttd",1), ("dhth",1), ("ddth",1), ("dttt",1)],
                        # General magnetic diagnostics
                        "magn": [("ipla",1), ("bvac",1)],
                        # External heating configuration
                        "nbi4": [("gasa",2), ("gasz",2), ("powt",1),
                                 ("eng1",2), ("pfr1",2), ("eng2",2), ("pfr2",2), ("eng3",2), ("pfr3",2), ("eng4",2), ("pfr4",2),
                                 ("eng5",2), ("pfr5",2), ("eng6",2), ("pfr6",2), ("eng7",2), ("pfr7",2), ("eng8",2), ("pfr8",2)],
                        "nbi8": [("gasa",2), ("gasz",2), ("powt",1),
                                 ("eng1",2), ("pfr1",2), ("eng2",2), ("pfr2",2), ("eng3",2), ("pfr3",2), ("eng4",2), ("pfr4",2),
                                 ("eng5",2), ("pfr5",2), ("eng6",2), ("pfr6",2), ("eng7",2), ("pfr7",2), ("eng8",2), ("pfr8",2)],
                        "nbi" : [("nblm",1)],
                        "nbp2": [("shid",1)],
                        "icrh": [("fra",0), ("pha",2), ("prfa",1), ("prae",1),
                                 ("frb",0), ("phb",2), ("prfb",1), ("prbe",1),
                                 ("frc",0), ("phc",2), ("prfc",1), ("prce",1),
                                 ("frd",0), ("phd",2), ("prfd",1), ("prde",1),
                                 ("fre",0), ("phe",2), ("prfe",1),
                                 ("ptot",1)],
                        "pion": [("plss",1)],
                        "lhcd": [("ptot",1)],
                        # Neturon rate measurements
                        "tin" : [("rnt",1)],
                        # Total radiation measurements
                        "bara": [("topo",1)],
                        "bolt": [("topo",1), ("tobu",1)],
                        "bolp": [("tobp",1), ("topo",1)],
                        "bolo": [("tobu",1), ("tobh",1), ("topi",1), ("topo",1)],
                        # ELM detection via edge radiation spectra
                        "edg7": [("dai",2), ("dao",2), ("be2o",2)],
                        "edg8": [("tbeo",2)],
                        # Generic gas fuelling
                        "gasm": [("maja",2), ("majz",2), ("majc",1), ("majr",1),
                                 ("mn1a",2), ("mn1z",2), ("mn1c",1), ("mn1r",1),
                                 ("mn2a",2), ("mn2z",2), ("mn2c",1), ("mn2r",1),
                                 ("mn3a",2), ("mn3z",2), ("mn3c",1), ("mn3r",1)],
                        "gasi": [("maja",2), ("majz",2), ("majc",1), ("majr",1),
                                 ("mn1a",2), ("mn1z",2), ("mn1c",1), ("mn1r",1),
                                 ("mn2a",2), ("mn2z",2), ("mn2c",1), ("mn2r",1),
                                 ("mn3a",2), ("mn3z",2), ("mn3c",1), ("mn3r",1)],
                        # Generic pellet fuelling (currently only used to determine potential presence)
                        "pl"  : [("hf-pellet_out_vol",2)]
                       },
               "EQ"  : {"efit": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("wp",1), ("wdia",1), ("btnm",1), ("btnd",1), ("btpm",1), ("btpd",1), ("bttm",1), ("bttd",1), ("fbnd",1), ("faxs",1),
                                 ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",1), ("psin",1), ("rpre",1), ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("elon",1), ("tril",1), ("triu",1),
                                 ("psi",0), ("psir",2), ("psiz",2), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1), ("f",0), ("p",0), ("dfdp",0), ("dpdp",0), ("li3m", 1), ("li3d", 1), ("xlim", 1), ("xlid", 1),
                                 ("rsil",0), ("zsil",0), ("rsiu",0), ("zsiu",0), ("rsol",0), ("zsol",0), ("rsou",0), ("zsou",0), ("rxpl",0), ("zxpl",0), ("rxpu",0), ("zxpu",0)],
                        "eftp": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("wp",1), ("wdia",1), ("btnm",1), ("btnd",1), ("btpm",1), ("btpd",1), ("bttm",1), ("bttd",1), ("fbnd",1), ("faxs",1),
                                 ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",1), ("psin",1), ("rpre",1), ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("elon",1), ("tril",1), ("triu",1),
                                 ("psi",0), ("psir",2), ("psiz",2), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1), ("f",0), ("p",0), ("dfdp",0), ("dpdp",0), ("li3m", 1), ("li3d", 1), ("xlim", 1), ("xlid", 1),
                                 ("rsil",0), ("zsil",0), ("rsiu",0), ("zsiu",0), ("rsol",0), ("zsol",0), ("rsou",0), ("zsou",0), ("rxpl",0), ("zxpl",0), ("rxpu",0), ("zxpu",0)],
                        "eftf": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("wp",1), ("wdia",1), ("btnm",1), ("btnd",1), ("btpm",1), ("btpd",1), ("bttm",1), ("bttd",1), ("fbnd",1), ("faxs",1),
                                 ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",1), ("psin",1), ("rpre",1), ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("elon",1), ("tril",1), ("triu",1),
                                 ("psi",0), ("psir",2), ("psiz",2), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1), ("f",0), ("p",0), ("dfdp",0), ("dpdp",0), ("li3m", 1), ("li3d", 1), ("xlim", 1), ("xlid", 1),
                                 ("rsil",0), ("zsil",0), ("rsiu",0), ("zsiu",0), ("rsol",0), ("zsol",0), ("rsou",0), ("zsou",0), ("rxpl",0), ("zxpl",0), ("rxpu",0), ("zxpu",0)],
                        "eftm": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("wp",1), ("wdia",1), ("btnm",1), ("btnd",1), ("btpm",1), ("btpd",1), ("bttm",1), ("bttd",1), ("fbnd",1), ("faxs",1),
                                 ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",1), ("psin",1), ("rpre",1), ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("elon",1), ("tril",1), ("triu",1),
                                 ("psi",0), ("psir",2), ("psiz",2), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1), ("f",0), ("p",0), ("dfdp",0), ("dpdp",0), ("li3m", 1), ("li3d", 1), ("xlim", 1), ("xlid", 1),
                                 ("rsil",0), ("zsil",0), ("rsiu",0), ("zsiu",0), ("rsol",0), ("zsol",0), ("rsou",0), ("zsou",0), ("rxpl",0), ("zxpl",0), ("rxpu",0), ("zxpu",0)],
                        "ehtr": [("xip",0), ("ajac",1), ("vjac",1), ("pohm",1), ("jsur",0), ("wp",1), ("wdia",1), ("btnm",1), ("btnd",1), ("btpm",1), ("btpd",1), ("bttm",1), ("bttd",1), ("fbnd",1), ("faxs",1),
                                 ("rmji",1), ("rmjo",1), ("psnm",0), ("psno",1), ("psni",1), ("psin",1), ("rpre",1), ("q",1), ("sq",0), ("ftor",1), ("btax",0), ("rmag",0), ("zmag",0), ("elon",1), ("tril",1), ("triu",1),
                                 ("psi",0), ("psir",2), ("psiz",2), ("sspr",1), ("sspi",1), ("rbnd",1), ("zbnd",1), ("f",0), ("p",0), ("dfdp",0), ("dpdp",0), ("li3m", 1), ("li3d", 1), ("xlim", 1), ("xlid", 1),
                                 ("rsil",0), ("zsil",0), ("rsiu",0), ("zsiu",0), ("rsol",0), ("zsol",0), ("rsou",0), ("zsou",0), ("rxpl",0), ("zxpl",0), ("rxpu",0), ("zxpu",0)]
                       },
               "B"   : {"efit": [("xip",0), ("fbnd",1), ("faxs",1), ("q",1), ("rmag",0), ("zmag",0), ("psi",0), ("psir",0), ("psiz",0), ('sspr',1), ('sspi',1), ("rbnd",1), ("zbnd",1), ("f",0)]
                       },
               "AEQ" : {#"equi": [("rho",0), ("f",0), ("p",0), ("bpol",0), ("bpo2",0), ("grho",0), ("gro2",0), ("riav",0), ("r2iv",0)]
                       },
               "NE"  : {"hrts": [("rmid",0), ("z",2), ("ne",1), ("dne",1)],
                        "lidr": [("rmid",0), ("z",2), ("ne",1), ("nel",1), ("neu",1)],
                        "hrtx": [("lid3",0)],
                        "kg1v": [("lid3",0)],
                        "kg10": [("r",0), ("z",2), ("ne",1)],
			"refl": [("nepr",1), ("xvec",1)]
                       },
               "TE"  : {"hrts": [("rmid",0), ("z",2), ("te",1), ("dte",1)],
                        "lidr": [("rmid",0), ("z",2), ("te",1), ("tel",1), ("teu",1)],
                        "kk3" : [("rc01",2), ("ra01",2), ("te01",1), ("rc02",2), ("ra02",2), ("te02",1), ("rc03",2), ("ra03",2), ("te03",1), ("rc04",2), ("ra04",2), ("te04",1),
                                 ("rc05",2), ("ra05",2), ("te05",1), ("rc06",2), ("ra06",2), ("te06",1), ("rc07",2), ("ra07",2), ("te07",1), ("rc08",2), ("ra08",2), ("te08",1),
                                 ("rc09",2), ("ra09",2), ("te09",1), ("rc10",2), ("ra10",2), ("te10",1), ("rc11",2), ("ra11",2), ("te11",1), ("rc12",2), ("ra12",2), ("te12",1),
                                 ("rc13",2), ("ra13",2), ("te13",1), ("rc14",2), ("ra14",2), ("te14",1), ("rc15",2), ("ra15",2), ("te15",1), ("rc16",2), ("ra16",2), ("te16",1),
                                 ("rc17",2), ("ra17",2), ("te17",1), ("rc18",2), ("ra18",2), ("te18",1), ("rc19",2), ("ra19",2), ("te19",1), ("rc20",2), ("ra20",2), ("te20",1),
                                 ("rc21",2), ("ra21",2), ("te21",1), ("rc22",2), ("ra22",2), ("te22",1), ("rc23",2), ("ra23",2), ("te23",1), ("rc24",2), ("ra24",2), ("te24",1),
                                 ("rc25",2), ("ra25",2), ("te25",1), ("rc26",2), ("ra26",2), ("te26",1), ("rc27",2), ("ra27",2), ("te27",1), ("rc28",2), ("ra28",2), ("te28",1),
                                 ("rc29",2), ("ra29",2), ("te29",1), ("rc30",2), ("ra30",2), ("te30",1), ("rc31",2), ("ra31",2), ("te31",1), ("rc32",2), ("ra32",2), ("te32",1),
                                 ("rc33",2), ("ra33",2), ("te33",1), ("rc34",2), ("ra34",2), ("te34",1), ("rc35",2), ("ra35",2), ("te35",1), ("rc36",2), ("ra36",2), ("te36",1),
                                 ("rc37",2), ("ra37",2), ("te37",1), ("rc38",2), ("ra38",2), ("te38",1), ("rc39",2), ("ra39",2), ("te39",1), ("rc40",2), ("ra40",2), ("te40",1),
                                 ("rc41",2), ("ra41",2), ("te41",1), ("rc42",2), ("ra42",2), ("te42",1), ("rc43",2), ("ra43",2), ("te43",1), ("rc44",2), ("ra44",2), ("te44",1),
                                 ("rc45",2), ("ra45",2), ("te45",1), ("rc46",2), ("ra46",2), ("te46",1), ("rc47",2), ("ra47",2), ("te47",1), ("rc48",2), ("ra48",2), ("te48",1),
                                 ("rc49",2), ("ra49",2), ("te49",1), ("rc50",2), ("ra50",2), ("te50",1), ("rc51",2), ("ra51",2), ("te51",1), ("rc52",2), ("ra52",2), ("te52",1),
                                 ("rc53",2), ("ra53",2), ("te53",1), ("rc54",2), ("ra54",2), ("te54",1), ("rc55",2), ("ra55",2), ("te55",1), ("rc56",2), ("ra56",2), ("te56",1),
                                 ("rc57",2), ("ra57",2), ("te57",1), ("rc58",2), ("ra58",2), ("te58",1), ("rc59",2), ("ra59",2), ("te59",1), ("rc60",2), ("ra60",2), ("te60",1),
                                 ("rc61",2), ("ra61",2), ("te61",1), ("rc62",2), ("ra62",2), ("te62",1), ("rc63",2), ("ra63",2), ("te63",1), ("rc64",2), ("ra64",2), ("te64",1),
                                 ("rc65",2), ("ra65",2), ("te65",1), ("rc66",2), ("ra66",2), ("te66",1), ("rc67",2), ("ra67",2), ("te67",1), ("rc68",2), ("ra68",2), ("te68",1),
                                 ("rc69",2), ("ra69",2), ("te69",1), ("rc70",2), ("ra70",2), ("te70",1), ("rc71",2), ("ra71",2), ("te71",1), ("rc72",2), ("ra72",2), ("te72",1),
                                 ("rc73",2), ("ra73",2), ("te73",1), ("rc74",2), ("ra74",2), ("te74",1), ("rc75",2), ("ra75",2), ("te75",1), ("rc76",2), ("ra76",2), ("te76",1),
                                 ("rc77",2), ("ra77",2), ("te77",1), ("rc78",2), ("ra78",2), ("te78",1), ("rc79",2), ("ra79",2), ("te79",1), ("rc80",2), ("ra80",2), ("te80",1),
                                 ("rc81",2), ("ra81",2), ("te81",1), ("rc82",2), ("ra82",2), ("te82",1), ("rc83",2), ("ra83",2), ("te83",1), ("rc84",2), ("ra84",2), ("te84",1),
                                 ("rc85",2), ("ra85",2), ("te85",1), ("rc86",2), ("ra86",2), ("te86",1), ("rc87",2), ("ra87",2), ("te87",1), ("rc88",2), ("ra88",2), ("te88",1),
                                 ("rc89",2), ("ra89",2), ("te89",1), ("rc90",2), ("ra90",2), ("te90",1), ("rc91",2), ("ra91",2), ("te91",1), ("rc92",2), ("ra92",2), ("te92",1),
                                 ("rc93",2), ("ra93",2), ("te93",1), ("rc94",2), ("ra94",2), ("te94",1), ("rc95",2), ("ra95",2), ("te95",1), ("rc96",2), ("ra96",2), ("te96",1)],
                        "ecm1": [("prfl",1)],
                        "ecm2": [("tece",1), ("tecl",1), ("tech",1), ("pece",1)]
                       },
               "TI":   {"cx"  : [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxsm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxdm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxfm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxgm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxhm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxkm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxs4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxd4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxf4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxg4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxh4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxs6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxd6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxf6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxg6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxh6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxs8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxd8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxf8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxg8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxh8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cx7a": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cx7b": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cx7c": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cx7d": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("ti",1), ("tilo",1), ("tihi",1)],
                        "cxse": [("rt",0), ("z",2), ("mass",2), ("ti",1), ("tie",1)]
                       },
               "NI"  : {},
               "NIMP": {"cx"  : [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxsm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxdm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxfm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxgm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxhm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxkm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxs4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxd4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxf4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxg4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxh4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxs6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxd6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxf6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxg6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxh6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxs8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxd8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxf8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxg8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxh8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cx7a": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cx7b": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cx7c": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cx7d": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("dens",1), ("conc",1), ("colo",1), ("cohi",1)],
                        "cxse": [("rt",0), ("z",2), ("mass",2), ("nz",1), ("nze",1)],
                        "wsxp": [("lzel",3), ("melz", 1), ("lzav",1), ("ozel",3), ("meoz", 1), ("ozav",1), ("hzel",3), ("mehz", 1), ("hzav",1), ("zeff",1)]
                       },
               "AF"  : {"cx"  : [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxsm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxdm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxfm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxgm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxhm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxkm": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxs4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxd4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxf4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxg4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxh4": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxs6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxd6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxf6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxg6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxh6": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxs8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxd8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxf8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxg8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxh8": [("rcor",0), ("rave",0), ("z",2), ("rpos",0), ("pos",0), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cx7a": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cx7b": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cx7c": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cx7d": [("rcor",0), ("rave",0), ("z",2), ("mass",2), ("angf",1), ("aflo",1), ("afhi",1)],
                        "cxse": [("rt",0), ("z",2), ("mass",2), ("angf",1), ("ange",1)]
                       },
               "FI"  : {"nbp2": [("nf",0), ("wpar",0), ("wper",0)],
                        "pion": [("am1",0), ("am2",0), ("ach1",0), ("ach2",0), ("nf1",0), ("nf2",0), ("wpar",0), ("wper",0), ("wdf1",0), ("wdf2",0)],
                        "tra0": [("nb",1), ("wbpa",1), ("wbpp",1), ("nmin",1), ("wmpa",1), ("wmpp",1)],
                        "trau": [("bdn",1), ("bdnh",1), ("bdnd",1), ("bdnt",1), ("upah",1), ("upph",1), ("upad",1), ("uppd",1), ("upat",1), ("uppt",1), ("nmin",1), ("umpa",1), ("umpp",1)]
                       },
               "QSTJ": {"nbp2": [("ge",0), ("gi",0), ("sv",0), ("jbdc",0), ("torp",0)],
                        "pion": [("am1",0), ("am2",0), ("ach1",0), ("ach2",0), ("pde",0), ("pdim",0), ("pdce",0), ("pdci",0), ("pd1",0), ("pd2",0)],
                        "bara": [("avfl",0), ("psi",0)],
                        "bolt": [("avfl",0), ("psi",0)],
                        "tra0": [("cur",1), ("qoh",1), ("qbe",1), ("qbi",1), ("sbe",1), ("tqin",1), ("cb",1), ("qrfe",1), ("qrfi",1)],
                        "trau": [("pbe",1), ("pbi",1), ("pbth",1), ("sbe",1), ("sbh",1), ("sbd",1), ("sbt",1), ("tqin",1), ("curb",1), ("qrfe",1), ("qrfi",1)]
                       }
             }

class QTableWidgetTimeWindow(QtWidgets.QTableWidgetItem):

    def __init__(self, value, sdata, index):
        super(QTableWidgetTimeWindow,self).__init__('%s' % (value))
        self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self._sdata = copy.deepcopy(sdata) if isinstance(sdata,EX2GK.classes.EX2GKTimeWindow) else dict()
        self._tw = int(index) if int(index) >= 0 else -1

    def __eq__(self, other):
        if (isinstance(other, self.__class__)):
            eqsnum = False
            eqtbeg = False
            eqtend = False
            if self._sdata and other._sdata:
                eqsnum = ("SHOT" in self._sdata["META"] and "SHOT" in other._sdata["META"] and self._sdata["META"]["SHOT"] == other._sdata["META"]["SHOT"])
                eqtbeg = ("T1" in self._sdata["META"] and "T1" in other._sdata["META"] and np.abs(self._sdata["META"]["T1"] - other._sdata["META"]["T1"]) < 1.0e6)
                eqtend = ("T2" in self._sdata["META"] and "T2" in other._sdata["META"] and np.abs(self._sdata["META"]["T2"] - other._sdata["META"]["T2"]) < 1.0e6)
            return (eqsnum and eqtbeg and eqtend)
        else:
            return super(QTableWidgetTimeWindow,self).__eq__(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_data(self):
        return self._sdata

    def get_shot_number(self):
        val = self._sdata["META"]["SHOT"] if "SHOT" in self._sdata["META"] else None
        return val

    def get_time_window_number(self):
        return self._tw

    def set_data(self, sdata):
        if isinstance(sdata, dict):
            self._sdata = copy.deepcopy(sdata)

    def set_time_window_number(self, index):
        if int(index) >= 0:
            self._tw = int(index)


class GenericPlotter(QtWidgets.QWidget):

    def __init__(self, data):
        super(GenericPlotter, self).__init__()
        self.sdata = None
        self.canvas = None
        self.points = None
        self.press = None
        self.rect = None
        self.rectimage = None
        self.bg = None
        self.xlims = None
        self.ylims = None
        self.note = None
        self.axis_enter = None
        self.axis_leave = None
        self.button_press = None
        self.mouse_motion = None
        self.button_release = None
        if isinstance(data, EX2GK.classes.EX2GKTimeWindow):
            self.sdata = data
        self._font = QtGui.QFont(self.font().family())
        self._font.setPixelSize(11)
        self.setFont(self._font)

    def _set_reference_limits(self):
        if isinstance(self.canvas, mplqt.FigureCanvas):
            axes = self.canvas.figure.axes
            self.xlims = [None] * len(axes)
            self.ylims = [None] * len(axes)
            for axid in range(0, len(axes)):
                self.xlims[axid] = axes[axid].get_xlim()
                self.ylims[axid] = axes[axid].get_ylim()

    def _connect_canvas(self):
        if isinstance(self.canvas, mplqt.FigureCanvas):
            self.axis_enter = self.canvas.mpl_connect('axes_enter_event', self._create_background)
            self.axis_leave = self.canvas.mpl_connect('axes_leave_event', self._delete_background)
            self.button_press = self.canvas.mpl_connect('button_press_event', self._init_rectangle)
            self.mouse_motion = self.canvas.mpl_connect('motion_notify_event', self._blit_rectangle)
            self.button_release = self.canvas.mpl_connect('button_release_event', self._zoom_axis)

    def _disconnect_canvas(self):
        if isinstance(self.canvas, mplqt.FigureCanvas):
            self.canvas.mpl_disconnect(self.axis_enter)
            self.canvas.mpl_disconnect(self.axis_leave)
            self.canvas.mpl_disconnect(self.button_press)
            self.canvas.mpl_disconnect(self.mouse_motion)
            self.canvas.mpl_disconnect(self.button_release)

    def _create_background(self, event):
        if self.press is None:
            self.bg = self.canvas.copy_from_bbox(event.inaxes.bbox)

    def _delete_background(self, event):
        if self.bg is not None and self.press is None:
            self.bg = None

    def _init_rectangle(self, event):
        if event.inaxes is not None:
            if event.dblclick:
                self._reset_axis(event)
            else:
                self._create_background(event)
                self.press = (event.inaxes, event.xdata, event.ydata)
                self.rect = matplotlib.patches.Rectangle((event.xdata, event.ydata), 0, 0, linewidth=0, edgecolor='b', fill=False, facecolor='none')
                self.rectimage = matplotlib.patches.Rectangle((event.xdata, event.ydata), 0, 0, linewidth=1, edgecolor='r', fill=False, facecolor='none')
                event.inaxes.add_patch(self.rectimage)
                event.inaxes.draw_artist(self.rectimage)
                self.canvas.blit(event.inaxes.bbox)

    def _blit_rectangle(self, event):
        if self.press is not None and self.rect is not None and self.rectimage is not None:
            axes, xx, yy = self.press
            if event.inaxes == axes:
                dx = event.xdata - xx
                dy = event.ydata - yy
                self.rect.set_width(dx)
                self.rect.set_height(dy)
                self.rectimage.set_width(dx)
                self.rectimage.set_height(dy)
            else:
                xdata, ydata = axes.transData.inverted().transform((event.x, event.y))
                xmin, xmax = axes.get_xlim()
                ymin, ymax = axes.get_ylim()
                dx = xdata - xx
                dy = ydata - yy
                dxi = dx
                dyi = dy
                if xdata < xmin:
                    dxi = xmin - xx
                elif xdata > xmax:
                    dxi = xmax - xx
                if ydata < ymin:
                    dyi = ymin - yy
                elif ydata > ymax:
                    dyi = ymax - yy
                self.rect.set_width(dx)
                self.rect.set_height(dy)
                self.rectimage.set_width(dxi)
                self.rectimage.set_height(dyi)
        if event.inaxes is not None and isinstance(self.points, list) and isinstance(self.note, list):
            axid = self.canvas.figure.axes.index(event.inaxes)
            if axid < len(self.note) and axid < len(self.points) and self.note[axid] is not None and self.points[axid] is not None:
                visible = self.note[axid].get_visible()
                cont = False
                pos = None
                text = None
                for diag in self.points[axid]:
                    if not cont:
                        if isinstance(diag, matplotlib.container.ErrorbarContainer):
                            cont, ind = diag[0].contains(event)
                            if cont:
                                pos = diag[0].get_xydata()[ind["ind"][0]]
                                text = diag.get_label()
                        elif isinstance(diag, matplotlib.collections.PathCollection):
                            cont, ind = diag.contains(event)
                            if cont:
                                pos = diag.get_offsets()[ind["ind"][0]]
                                text = diag.get_label()
                if pos is not None and text is not None:
                    if self.bg is None:
                        self._create_background(event)
                    self.note[axid].xy = pos
                    self.note[axid].set_text(text)
                    self.note[axid].set_visible(True)
                if not cont:
                    self.note[axid].set_visible(False)
        if self.bg is not None:
            self.canvas.figure.canvas.restore_region(self.bg)
            if self.press is not None and self.rect is not None and self.rectimage is not None:
                axes, xx, yy = self.press
                axes.draw_artist(self.rectimage)
                self.canvas.blit(axes.bbox)
            if event.inaxes is not None and isinstance(self.note, list):
                axid = self.canvas.figure.axes.index(event.inaxes)
                if axid < len(self.note) and self.note[axid] is not None:
                    event.inaxes.draw_artist(self.note[axid])
                    self.canvas.blit(event.inaxes.bbox)

    def _zoom_axis(self, event):
        if self.press is not None and self.rect is not None and self.rectimage is not None and self.bg is not None:
            if self.rect.get_width() != 0 and self.rect.get_height() != 0:
                axes, xx, yy = self.press
                self.canvas.figure.canvas.restore_region(self.bg)
                xmin = np.nanmin([xx, xx + self.rect.get_width()])
                xmax = np.nanmax([xx, xx + self.rect.get_width()])
                ymin = np.nanmin([yy, yy + self.rect.get_height()])
                ymax = np.nanmax([yy, yy + self.rect.get_height()])
                axes.set_xlim([xmin, xmax])
                axes.set_ylim([ymin, ymax])
                self.rectimage.remove()
        self.rect = None
        self.rectimage = None
        self.press = None
        self._delete_background(event)
        self.canvas.draw_idle()

    def _reset_axis(self, event):
        self._delete_background(event)
        if isinstance(event.inaxes, matplotlib.axes.Axes):
            axid = self.canvas.figure.axes.index(event.inaxes)
            event.inaxes.set_xlim(self.xlims[axid])
            event.inaxes.set_ylim(self.ylims[axid])
        self.canvas.draw_idle()


class time_trace_plotter(GenericPlotter):

    def __init__(self, data, ti, tf, fontsize=None):
        super(time_trace_plotter, self).__init__(data)
        self.ti = None
        self.tf = None
        self.fs = 10
        # Overrides initialization of base class, since it type checks for EX2GKTimeWindow
        if isinstance(data, dict):
            self.sdata = data
        if isinstance(ti, (int, float)):
            self.ti = float(ti)
        if isinstance(tf, (int, float)):
            self.tf = float(tf)
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.sdata is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 1100, 500)
            self.setWindowTitle('Time Trace Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        fig = mplfig.Figure(tight_layout=True)
        ti = -9999.9999 if self.ti is None else self.ti
        tf = 9999.9999 if self.tf is None else self.tf

        ax1 = fig.add_subplot(121)
        if "IP" in self.sdata and self.sdata["IP"] is not None and "TIP" in self.sdata and self.sdata["TIP"] is not None:
            tfilt = np.all([self.sdata["TIP"] >= ti, self.sdata["TIP"] <= tf], axis=0)
            ax1.plot(self.sdata["TIP"][tfilt], np.abs(self.sdata["IP"][tfilt])*1.0e-6, color='r', label=r'I_p [MA]')
            if "BMAG" in self.sdata and self.sdata["BMAG"] is not None and "TBMAG" in self.sdata and self.sdata["TBMAG"] is not None:
                tfilt = np.all([self.sdata["TBMAG"] >= ti, self.sdata["TBMAG"] <= tf], axis=0)
                ax1.plot(self.sdata["TBMAG"][tfilt], np.abs(self.sdata["BMAG"][tfilt]), color='b', label=r'B_{mag} [T]')
            if "NEAX" in self.sdata and self.sdata["NEAX"] is not None and "TNEAX" in self.sdata and self.sdata["TNEAX"] is not None:
                tfilt = np.all([self.sdata["TNEAX"] >= ti, self.sdata["TNEAX"] <= tf], axis=0)
                ax1.plot(self.sdata["TNEAX"][tfilt], self.sdata["NEAX"][tfilt]*1.0e-19, color='g', label=r'n_{e,0} [10^19 m^{-3}]')
            if "TEAX" in self.sdata and self.sdata["TEAX"] is not None and "TTEAX" in self.sdata and self.sdata["TTEAX"] is not None:
                tfilt = np.all([self.sdata["TTEAX"] >= ti, self.sdata["TTEAX"] <= tf], axis=0)
                ax1.plot(self.sdata["TTEAX"][tfilt], self.sdata["TEAX"][tfilt]*1.0e-3, color='m', label=r'T_{e,0} [keV]')
            if "ZEFV" in self.sdata and self.sdata["ZEFV"] is not None and "TZEFV" in self.sdata and self.sdata["TZEFV"] is not None:
                tfilt = np.all([self.sdata["TZEFV"] >= ti, self.sdata["TZEFV"] <= tf], axis=0)
                ax1.plot(self.sdata["TZEFV"][tfilt], self.sdata["ZEFV"][tfilt], color='c', label=r'Z_{eff,v}')
            if "ZEFH" in self.sdata and self.sdata["ZEFH"] is not None and "TZEFH" in self.sdata and self.sdata["TZEFH"] is not None:
                tfilt = np.all([self.sdata["TZEFH"] >= ti, self.sdata["TZEFH"] <= tf], axis=0)
                ax1.plot(self.sdata["TZEFH"][tfilt], self.sdata["ZEFH"][tfilt], color='y', label=r'Z_{eff,h}')
            ax1.legend(loc='best', prop={'size': self.fs-1})
        ax1.set_ylabel(r'Plasma Parameters', fontsize=self.fs)
        ax1.set_xlabel(r'Time [s]', fontsize=self.fs)
        ax1.tick_params(axis='both', which='major', labelsize=self.fs)

        ax2 = fig.add_subplot(122)
        if "OHM" in self.sdata and self.sdata["OHM"] is not None and "TOHM" in self.sdata and self.sdata["TOHM"] is not None:
            sc = 1.0e-3 if np.mean(np.abs(self.sdata["OHM"])) < 1.0e4 else 1.0e-6
            unit = 'kW' if np.mean(np.abs(self.sdata["OHM"])) < 1.0e4 else 'MW'
            tfilt = np.all([self.sdata["TOHM"] >= ti, self.sdata["TOHM"] <= tf], axis=0)
            ax2.plot(self.sdata["TOHM"][tfilt], self.sdata["OHM"][tfilt]*sc, color='k', label=r'P_{ohm} ['+unit+']')
            if "ICRH" in self.sdata and self.sdata["ICRH"] is not None and "TICRH" in self.sdata and self.sdata["TICRH"] is not None:
                tfilt = np.all([self.sdata["TICRH"] >= ti, self.sdata["TICRH"] <= tf], axis=0)
                ax2.plot(self.sdata["TICRH"][tfilt], self.sdata["ICRH"][tfilt]*1.0e-6, color='b', label=r'P_{IC} [MW]')
            if "NBI" in self.sdata and self.sdata["NBI"] is not None and "TNBI" in self.sdata and self.sdata["TNBI"] is not None:
                tfilt = np.all([self.sdata["TNBI"] >= ti, self.sdata["TNBI"] <= tf], axis=0)
                ax2.plot(self.sdata["TNBI"][tfilt], self.sdata["NBI"][tfilt]*1.0e-6, color='r', label=r'P_{NBI} [MW]')
            if "LHCD" in self.sdata and self.sdata["LHCD"] is not None and "TLHCD" in self.sdata and self.sdata["TLHCD"] is not None:
                tfilt = np.all([self.sdata["TLHCD"] >= ti, self.sdata["TLHCD"] <= tf], axis=0)
                ax2.plot(self.sdata["TLHCD"][tfilt], self.sdata["LHCD"][tfilt]*1.0e-6, color='g', label=r'P_{LH} [MW]')
            ax2.legend(loc='best', prop={'size': self.fs-1})
        ax2.set_ylabel(r'Plasma Heating', fontsize=self.fs)
        ax2.set_xlabel(r'Time [s]', fontsize=self.fs)
        ax2.tick_params(axis='both', which='major', labelsize=self.fs)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)

        return gbox


class profile_plotter(GenericPlotter):

    def __init__(self, data, sigma=None, csplot=None, xerrflag=False, fontsize=None):
        super(profile_plotter, self).__init__(data)
        self.sigma = 1.0
        self.csplot = "RHOTORN"
        self.xerrflag = False
        self.fs = 10
        if isinstance(sigma, (int, float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if self.sdata is not None and isinstance(csplot, str):
            prefix = self.sdata["META"]["CSOP"]
            if prefix+csplot.upper() in self.sdata["CD"].coord_keys():
                self.csplot = csplot.upper()
        if xerrflag:
            self.xerrflag = True
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.sdata is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 1100, 1100)
            self.setWindowTitle('Profile Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data for profile plotting tool.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        ics = self.sdata["META"]["CSO"]
        ocp = self.sdata["META"]["CSOP"]
        ocs = self.csplot if ocp+self.csplot in self.sdata["CD"].coord_keys() else ics
        xlims = (0.0, 1.0)
        fxlim = True
        if ocp+ocs in self.sdata["CD"].coord_keys():
            xlims = (np.nanmin(self.sdata["CD"][ocp][ocs]["V"]), np.nanmax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX", "POLFLUX", "RMAJORO", "RMAJORI"]:
                fxlim = False
        (xt, xlabel, xunit, fl, fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + r' [' + xunit + r']' if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["NE", "TE", "TIMP", "NIMP", "NFINBI", "WFINBI", "AFTOR", "NFIICRH", "WFIICRH"]
        qqextra = {"NIMP": "N", "TIMP": "T"}
        qscales = {"NE" : 19, "TE" : 3, "NIMP": 17, "TIMP" : 3, "NFINBI": 18, "WFINBI" : 3, "AFTOR" : 3, "NFIICRH" : 18, "WFIICRH" : 3}
        styles = ['+', 'x', 'o', 's', 'd', '^', 'v', '<', '>', 'H', '*', 'p', 'h', 'D', '8']
        colors = ['r', 'b', 'g', 'm', 'c', 'y']
        fig = mplfig.Figure()
        ny = 3
        nx = 3
        self.points = [None] * len(qlist)
        self.note = [None] * len(qlist)

        for qq in np.arange(0, len(qlist)):
            (qtag, ylabel, yunit, fqlbl, fqunit) = EX2GK.ptools.define_quantity(qlist[qq], exponent=qscales[qlist[qq]])
            scale = np.power(10.0, -qscales[qlist[qq]])
            nplot = qq + 1
            ax = fig.add_subplot(ny, nx, nplot)
            si = 0
            ci = 0
            self.points[qq] = []
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1:
                ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ocp
                xraw = self.sdata["RD"][qtag]["X"].flatten()
                xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                if ics != ocs:
                    (xraw, dj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, ocp)
                    xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                yraw = self.sdata["RD"][qtag][""].flatten()
                yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                for ii in np.arange(0, len(self.sdata["RD"][qtag]["SRC"])):
                    filt = (self.sdata["RD"][qtag]["MAP"] == ii)
                    if np.any(filt):
                        xerr = self.sigma * xeraw[filt]
                        yerr = self.sigma * yeraw[filt]
                        dlabel = self.sdata["RD"][qtag]["SRC"][ii]
                        if self.xerrflag:
                            eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                            self.points[qq].append(eba)
                        else:
                            eba = ax.errorbar(xraw[filt], yraw[filt]*scale, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                            self.points[qq].append(eba)
                        si = si + 1
                        if self.note[qq] is None:
                            self.note[qq] = ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})
                            self.note[qq].set_visible(False)
            if qlist[qq] in qqextra:
                nqtag = qqextra[qlist[qq]]
                for idx in np.arange(0, numimp):
                    itag = "%d" % (idx+1)
                    if nqtag+"IMP"+itag in self.sdata["RD"] and self.sdata["RD"][nqtag+"IMP"+itag].npoints > 0:
                        ics = self.sdata["RD"][nqtag+"IMP"+itag].coordinate if self.sdata["RD"][nqtag+"IMP"+itag].coordinate is not None else ocs
                        icp = self.sdata["RD"][nqtag+"IMP"+itag].coord_prefix if self.sdata["RD"][nqtag+"IMP"+itag].coordinate is not None else ocp
                        xraw = self.sdata["RD"][nqtag+"IMP"+itag]["X"].flatten()
                        xeraw = self.sdata["RD"][nqtag+"IMP"+itag]["XEB"].flatten()
                        if ics != ocs:
                            (xraw, dj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, ocp)
                            xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                        yraw = self.sdata["RD"][nqtag+"IMP"+itag][""].flatten()
                        yeraw = self.sdata["RD"][nqtag+"IMP"+itag]["EB"].flatten()
                        for ii in np.arange(0, len(self.sdata["RD"][nqtag+"IMP"+itag]["SRC"])):
                            filt = (self.sdata["RD"][nqtag+"IMP"+itag]["MAP"] == ii)
                            if np.any(filt):
                                xerr = self.sigma * xeraw[filt]
                                yerr = self.sigma * yeraw[filt]
                                dlabel = self.sdata["RD"][nqtag+"IMP"+itag]["SRC"][ii]
                                if self.xerrflag:
                                    eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color='k', ls='',marker=styles[si], label=dlabel)
                                    self.points[qq].append(eba)
                                else:
                                    eba = ax.errorbar(xraw[filt], yraw[filt]*scale, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                    self.points[qq].append(eba)
                                si = si + 1
                                if self.note[qq] is None:
                                    self.note[qq] = ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})
                                    self.note[qq].set_visible(False)
                    if "PD" in self.sdata and nqtag+"IMP"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"IMP"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                        yfit = self.sdata["PD"][nqtag+"IMP"+itag][""].flatten()
                        yefit = self.sdata["PD"][nqtag+"IMP"+itag]["EB"].flatten()
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
                for idx in np.arange(0, numz):
                    itag = "%d" % (idx+1)
                    if "PD" in self.sdata and nqtag+"Z"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"Z"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                        yfit = self.sdata["PD"][nqtag+"Z"+itag][""].flatten()
                        yefit = self.sdata["PD"][nqtag+"Z"+itag]["EB"].flatten()
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
                if qlist[qq] == "TIMP":
                    minsize = 0 if self.sdata["FLAG"].checkFlag("TISCALE") else 3
                    if "TI" in self.sdata["RD"] and self.sdata["RD"]["TI"].npoints > minsize:
                        ics = self.sdata["RD"]["TI"].coordinate if self.sdata["RD"]["TI"].coordinate is not None else ocs
                        icp = self.sdata["RD"]["TI"].coord_prefix if self.sdata["RD"]["TI"].coordinate is not None else ocp
                        xraw = self.sdata["RD"]["TI"]["X"].flatten()
                        xeraw = self.sdata["RD"]["TI"]["XEB"].flatten()
                        if ics != ocs:
                            (xraw, dj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, ocp)
                            xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                        yraw = self.sdata["RD"]["TI"][""].flatten()
                        yeraw = self.sdata["RD"]["TI"]["EB"].flatten()
                        for ii in np.arange(0, len(self.sdata["RD"]["TI"]["SRC"])):
                            filt = (self.sdata["RD"]["TI"]["MAP"] == ii)
                            if np.any(filt):
                                xerr = self.sigma * xeraw[filt]
                                yerr = self.sigma * yeraw[filt]
                                dlabel = self.sdata["RD"]["TI"]["SRC"][ii]
                                if self.xerrflag:
                                    eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                    self.points[qq].append(eba)
                                else:
                                    eba = ax.errorbar(xraw[filt], yraw[filt]*scale, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                    self.points[qq].append(eba)
                                si = si + 1
                                if self.note[qq] is None:
                                    self.note[qq] = ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})
                                    self.note[qq].set_visible(False)
                        if "PD" in self.sdata and "TI1" in self.sdata["PD"] and self.sdata["PD"]["TI1"] is not None:
                            xfit = self.sdata["PD"]["X"][""].flatten()
                            xefit = self.sdata["PD"]["X"]["EB"].flatten()
                            if self.sdata["META"]["CSO"] != ocs:
                                (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                                xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                            yfit = self.sdata["PD"]["TI1"][""].flatten()
                            yefit = self.sdata["PD"]["TI1"]["EB"].flatten()
                            ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                            yl = yfit - self.sigma * yefit
                            yu = yfit + self.sigma * yefit
                            ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                            ci = ci + 1
            else:
                if "PD" in self.sdata and qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                        xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                    ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0], xlims[1])
            if qtag == "TIMP":
                qlabel = r'Ion Temperature'
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel, fontsize=self.fs)
            ax.set_xlabel(pxlabel, fontsize=self.fs)
            ax.tick_params(axis='both', which='major', labelsize=self.fs)

        sid = "%s #%d: %.4f - %.4f" % (self.sdata["META"]["DEVICE"], self.sdata["META"]["SHOT"], self.sdata["META"]["T1"], self.sdata["META"]["T2"])
        rshift = self.sdata["ZD"]["RSHIFT"][""] if "ZD" in self.sdata and "RSHIFT" in self.sdata["ZD"] else 0.0
        shift_warning = "A radial shift of %.2f cm (on mid-plane R major) was applied to measurements." % (rshift * 100.0)
        nscale = self.sdata["ZD"]["NESCALE"][""] if "ZD" in self.sdata and "NESCALE" in self.sdata["ZD"] and self.sdata["ZD"]["NESCALE"] is not None else 0.0
        scale_warning = "HRTS/NE was scaled by a factor of %.3f based on KG1V/LID3." % (nscale) if self.sdata["FLAG"]["NSCALE"] else ""
        fig.suptitle(sid+"\n"+shift_warning+"\n"+scale_warning, fontsize=self.fs+2)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)

        return gbox


class gradient_plotter(GenericPlotter):

    def __init__(self, data, sigma=None, csplot=None, xerrflag=False, procflag=False, jacobflag=False, fontsize=None):
        super(gradient_plotter, self).__init__(data)
        self.sigma = 1.0
        self.csplot = "RHOTORN"
        self.xerrflag = False
        self.procflag = False
        self.jacobflag = False
        self.fs = 10
        if isinstance(sigma, (int, float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if self.sdata is not None and isinstance(csplot, str):
            prefix = self.sdata["META"]["CSOP"]
            if prefix+csplot.upper() in self.sdata["CD"].coord_keys():
                self.csplot = csplot.upper()
        if xerrflag:
            self.xerrflag = True
        if procflag:
            self.procflag = True
        if jacobflag:
            self.jacobflag = True
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.sdata is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 1100, 1100)
            self.setWindowTitle('Profile Gradient Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        ics = self.sdata["META"]["CSO"]
        ocp = self.sdata["META"]["CSOP"]
        ocs = self.csplot if ocp+self.csplot in self.sdata["CD"].coord_keys() else ics
        xlims = (0.0, 1.0)
        fxlim = True
        if ocp+ocs in self.sdata["CD"].coord_keys():
            xlims = (np.nanmin(self.sdata["CD"][ocp][ocs]["V"]), np.nanmax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX", "POLFLUX", "RMAJORO", "RMAJORI"]:
                fxlim = False
        (xt, xlabel, xunit, fl, fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + r' [' + xunit + r']' if xunit else xlabel
        dcs = ocs if self.jacobflag else ics
        (dxt, dxlabel, dxunit, fdl, fdu) = EX2GK.ptools.define_coordinate_system(self.sdata["META"]["CODEBASE"]) if self.procflag else EX2GK.ptools.define_coordinate_system(dcs)

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["NE", "TE", "TIMP", "TI", "NIMP", "AFTOR"]
        qqextra = {"NIMP": "N", "TIMP": "T"}
        qscales = {"NE" : 19, "TE" : 3, "NIMP": 17, "TIMP" : 3, "TI": 3, "AFTOR" : 3}
        qlims = {"NE": (-3.0, 10.0), "TE": (-3.0, 15.0), "NIMP": (-3.0, 10.0), "TIMP": (-3.0, 15.0), "TI": (-3.0, 15.0), "AFTOR": (-10.0, 40.0)}
        styles = ['+', 'x', 'o', 's', 'd', '^', 'v', '<', '>', 'H', '*', 'p', 'h', 'D', '8']
        colors = ['r', 'b', 'g', 'm', 'c', 'y']
        fig = mplfig.Figure()
        ny = 3
        nx = 2

        dq_opts = {"CMN":    {"NE": "CMN_DLOGNE", "TE":   "CMN_DLOGTE", "TIMP": "CMN_DLOGTIMP", "TI":   "CMN_DLOGTI", "NIMP": "CMN_DLOGNIMP", "AFTOR":   "CMN_DLOGAFTOR"}, \
                   "QLK":    {"NE":    "QLK_ANE", "TE":      "QLK_ATE", "TIMP":    "QLK_ATIMP", "TI":      "QLK_ATI", "NIMP":    "QLK_ANIMP", "AFTOR":       "QLK_AUTOR"}   }
        ctag = self.sdata["META"]["CODETAG"] if self.procflag and self.sdata["META"]["CODETAG"] in dq_opts else "None"
        yunitd = r'1' if ctag in dq_opts else r''
        yunitd = yunitd + r' / ' + dxunit if dxunit else r''

        for qq in np.arange(0, len(qlist)):
            qexp = qscales[qlist[qq]] if ctag not in dq_opts else 0
            (qtag, ylabel, yunit, fqlbl, fqunit) = EX2GK.ptools.define_quantity(qlist[qq], exponent=qexp)
            if ctag in dq_opts:
                qtag = "OUT"+dq_opts[ctag][qlist[qq]]
            ylabel = r'Log. ' + ylabel + r' Grad.' if ctag in dq_opts else ylabel + r' Grad.'
            gtag = "" if ctag in dq_opts else "GRAD"
            scale = np.power(10.0, -qexp)
            nplot = qq + 1
            ax = fig.add_subplot(ny, nx, nplot)
            ci = 0
            if qlist[qq] in qqextra:
                nqtag = qqextra[qlist[qq]]
                if ctag in dq_opts:
                    mm = re.match(r'^(.*)'+qlist[qq]+r'$', dq_opts[ctag][qlist[qq]])
                    if mm:
                        nqtag = "OUT"+mm.group(1).upper()+qqextra[qlist[qq]]
                for idx in np.arange(0, numimp):
                    itag = "%d" % (idx+1)
                    if nqtag+"IMP"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"IMP"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        jfit = np.ones(xfit.shape)
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                            if ctag not in dq_opts and ocs != ics and not self.KeepFitGradientsBox.isChecked():
                                jfit = dj
                        yfit = self.sdata["PD"][nqtag+"IMP"+itag][gtag+""].flatten() * jfit
                        yefit = self.sdata["PD"][nqtag+"IMP"+itag][gtag+"EB"].flatten() * jfit
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
                for idx in np.arange(0, numz):
                    itag = "%d" % (idx+1)
                    if nqtag+"Z"+itag in self.sdata["PD"] and self.sdata["PD"][nqtag+"Z"+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        jfit = np.ones(xfit.shape)
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                            if ctag not in dq_opts and ocs != ics and self.jacobflag:
                                jfit = dj
                        yfit = self.sdata["PD"][nqtag+"Z"+itag][gtag+""].flatten() * jfit
                        yefit = self.sdata["PD"][nqtag+"Z"+itag][gtag+"EB"].flatten() * jfit
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
            elif qlist[qq] == "TI":
                for idx in np.arange(0, numion):
                    itag = "%d" % (idx+1)
                    if qtag+itag in self.sdata["PD"] and self.sdata["PD"][qtag+itag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        jfit = np.ones(xfit.shape)
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                            if ctag not in dq_opts and ocs != ics and self.jacobflag:
                                jfit = dj
                        yfit = self.sdata["PD"][qtag+itag][gtag+""].flatten() * jfit
                        yefit = self.sdata["PD"][qtag+itag][gtag+"EB"].flatten() * jfit
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
            else:
                if qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    jfit = np.ones(xfit.shape)
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                        xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                        if ctag not in dq_opts and ocs != ics and self.jacobflag:
                            jfit = dj
                    yfit = self.sdata["PD"][qtag][gtag+""].flatten() * jfit
                    yefit = self.sdata["PD"][qtag][gtag+"EB"].flatten() * jfit
                    ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                    ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0], xlims[1])
            if ctag in dq_opts:
                ax.set_ylim(qlims[qlist[qq]][0], qlims[qlist[qq]][1])
            pylabel = ylabel + r' [' + yunit + yunitd + r']' if yunit else ylabel
            if ctag in dq_opts:
                pylabel = ylabel + r' [' + yunitd + r']' if yunitd else ylabel
            ax.set_ylabel(pylabel, fontsize=self.fs)
            ax.set_xlabel(pxlabel, fontsize=self.fs)
            ax.tick_params(axis='both', which='major', labelsize=self.fs)

        sid = "%s #%d: %.4f - %.4f" % (self.sdata["META"]["DEVICE"], self.sdata["META"]["SHOT"], self.sdata["META"]["T1"], self.sdata["META"]["T2"])
        derivative_coordinate_warning = "Gradients dy/dx calculated with x = %s" % (dxlabel)
        if ctag in dq_opts:
            derivative_coordinate_warning += " (%s adapter)" % (self.sdata["META"]["CODE"])
        fig.suptitle(sid+"\n"+derivative_coordinate_warning, fontsize=self.fs+2)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)

        return gbox


class source_plotter(GenericPlotter):

    def __init__(self, data, sigma=None, csplot=None, xerrflag=False, fontsize=None):
        super(source_plotter, self).__init__(data)
        self.sigma = 1.0
        self.csplot = "RHOTORN"
        self.xerrflag = False
        self.fs = 10
        if isinstance(sigma, (int, float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if self.sdata is not None and isinstance(csplot, str):
            prefix = self.sdata["META"]["CSOP"]
            if prefix+csplot.upper() in self.sdata["CD"].coord_keys():
                self.csplot = csplot.upper()
        if xerrflag:
            self.xerrflag = True
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.sdata is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 1100, 1100)
            self.setWindowTitle('Source Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        ics = self.sdata["META"]["CSO"]
        ocp = self.sdata["META"]["CSOP"]
        ocs = self.csplot if ocp+self.csplot in self.sdata["CD"].coord_keys() else ics
        xlims = (0.0, 1.0)
        fxlim = True
        if ocp+ocs in self.sdata["CD"].coord_keys():
            xlims = (np.nanmin(self.sdata["CD"][ocp][ocs]["V"]), np.nanmax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX", "POLFLUX", "RMAJORO", "RMAJORI"]:
                fxlim = False
        (xt, xlabel, xunit, fl, fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + r' [' + xunit + r']' if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["SNINBI", "SPNBI", "STENBI", "STINBI", "STEICRH", "STIICRH"]
        qscales = {"SNINBI": 19, "SPNBI": 0, "STENBI": 6, "STINBI": 6, "STEICRH": 6, "STIICRH": 6}
        styles = ['+', 'x', 'o', 's', 'd', '^', 'v', '<', '>', 'H', '*', 'p', 'h', 'D', '8']
        colors = ['r', 'b', 'g', 'm', 'c', 'y']
        fig = mplfig.Figure()
        ny = 3
        nx = 2
        self.points = [None] * len(qlist)
        self.note = [None] * len(qlist)

        for qq in np.arange(0, len(qlist)):
            scale = np.power(10.0, -qscales[qlist[qq]])
            nplot = qq + 1
            ax = fig.add_subplot(ny, nx, nplot)
            si = 0
            ci = 0
            self.points[qq] = []
            quantities = []
            if qlist[qq] == "SNINBI":
                for ii in range(numion):
                    itag = "%d" % (ii+1)
                    quantities.append("SNI"+itag+"NBI")
            else:
                quantities.append(qlist[qq])
            for item in quantities:
                (qtag, ylabel, yunit, fqlbl, fqunit) = EX2GK.ptools.define_quantity(item, exponent=qscales[qlist[qq]])
                if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1:
                    ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                    icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ocp
                    xraw = self.sdata["RD"][qtag]["X"].flatten()
                    xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                    if ics != ocs:
                        (xraw, dj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, ocp)
                        xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                    yraw = self.sdata["RD"][qtag][""].flatten()
                    yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                    for ii in np.arange(0, len(self.sdata["RD"][qtag]["SRC"])):
                        filt = (self.sdata["RD"][qtag]["MAP"] == ii)
                        if np.any(filt):
                            xerr = self.sigma * xeraw[filt]
                            yerr = self.sigma * yeraw[filt]
                            dlabel = self.sdata["RD"][qtag]["SRC"][ii]
                            if self.xerrflag:
                                eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                self.points[qq].append(eba)
                            else:
                                eba = ax.errorbar(xraw[filt], yraw[filt]*scale, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                self.points[qq].append(eba)
                            si = si + 1
                            if self.note[qq] is None:
                                self.note[qq] = ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})
                                self.note[qq].set_visible(False)
                    if "PD" in self.sdata and qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                        yfit = self.sdata["PD"][qtag][""].flatten()
                        yefit = self.sdata["PD"][qtag]["EB"].flatten()
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
            (qtag, ylabel, yunit, fqlbl, fqunit) = EX2GK.ptools.define_quantity(qlist[qq], exponent=qscales[qlist[qq]])
            if fxlim:
                ax.set_xlim(xlims[0], xlims[1])
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel, fontsize=self.fs)
            ax.set_xlabel(pxlabel, fontsize=self.fs)
            ax.tick_params(axis='both', which='major', labelsize=self.fs)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)

        return gbox


class misc_plotter(GenericPlotter):

    def __init__(self, data, sigma=None, csplot=None, xerrflag=False, fontsize=None):
        super(misc_plotter, self).__init__(data)
        self.sigma = 1.0
        self.csplot = "RHOTORN"
        self.xerrflag = False
        self.fs = 10
        if isinstance(sigma, (int, float)) and float(sigma) > 0.0:
            self.sigma = float(sigma)
        if self.sdata is not None and isinstance(csplot, str):
            prefix = self.sdata["META"]["CSOP"]
            if prefix+csplot.upper() in self.sdata["CD"].coord_keys():
                self.csplot = csplot.upper()
        if xerrflag:
            self.xerrflag = True
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.sdata is not None:
            layout = self.initUI()
            self._set_reference_limits()
            self._connect_canvas()
            self.setLayout(layout)
            self.setGeometry(10, 10, 1100, 1100)
            self.setWindowTitle('Miscellaneous Plots')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        prefix_list = self.sdata["CD"].getPrefixes()
        ics = self.sdata["META"]["CSO"]
        ocp = self.sdata["META"]["CSOP"]
        ocs = self.csplot if ocp+self.csplot in self.sdata["CD"].coord_keys() else ics
        xlims = (0.0, 1.0)
        fxlim = True
        if ocp+ocs in self.sdata["CD"].coord_keys():
            xlims = (np.nanmin(self.sdata["CD"][ocp][ocs]["V"]), np.nanmax(self.sdata["CD"][ocp][ocs]["V"]))
            if ocs in ["TORFLUX", "POLFLUX", "RMAJORO", "RMAJORI"]:
                fxlim = False
        (xt, xlabel, xunit, fl, fu) = EX2GK.ptools.define_coordinate_system(ocs)
        pxlabel = xlabel + " [" + xunit + "]" if xunit else xlabel

        numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
        numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
        numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
        qlist = ["Q", "ZEFF"]
        qscales = {"Q" : 0, "ZEFF" : 0}
        styles = ['+', 'x', 'o', 's', 'd', '^', 'v', '<', '>', 'H', '*', 'p', 'h', 'D', '8']
        colors = ['r', 'b', 'g', 'm', 'c', 'y']
        fig = mplfig.Figure()
        ny = 2
        nx = 2
        self.points = [None] * len(qlist)
        self.note = [None] * len(qlist)

        for qq in np.arange(0, len(qlist)):
            (qtag, ylabel, yunit, fqlbl, fqunit) = EX2GK.ptools.define_quantity(qlist[qq], exponent=qscales[qlist[qq]])
            scale = np.power(10.0, -qscales[qlist[qq]])
            nplot = 2 * qq + 1
            ax = fig.add_subplot(ny, nx, nplot)
            si = 0
            ci = 0
            self.points[qq] = []
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1:
                ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ocp
                xraw = self.sdata["RD"][qtag]["X"].flatten()
                xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                if ics != ocs:
                    (xraw, dj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, ocp)
                    xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                yraw = self.sdata["RD"][qtag][""].flatten()
                yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                for ii in np.arange(0, len(self.sdata["RD"][qtag]["SRC"])):
                    filt = (self.sdata["RD"][qtag]["MAP"] == ii)
                    if np.any(filt):
                        xerr = self.sigma * xeraw[filt]
                        yerr = self.sigma * yeraw[filt]
                        dlabel = self.sdata["RD"][qtag]["SRC"][ii]
                        if self.xerrflag:
                            eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                            self.points[qq].append(eba)
                        else:
                            eba = ax.errorbar(xraw[filt], yraw[filt]*scale, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                            self.points[qq].append(eba)
                        si = si + 1
                        if self.note[qq] is None:
                            self.note[qq] = ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})
                            self.note[qq].set_visible(False)
                if qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0:
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    xefit = self.sdata["PD"]["X"]["EB"].flatten()
                    if self.sdata["META"]["CSO"] != ocs:
                        (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                        xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                    yl = yfit - self.sigma * yefit
                    yu = yfit + self.sigma * yefit
                    ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                    ci = ci + 1
            if qtag == "Q" and len(prefix_list) > 1:
                for jj in np.arange(0, len(prefix_list)):
                    ctag = prefix_list[jj]
                    if ctag and ctag+qtag in self.sdata["RD"] and self.sdata["RD"][ctag+qtag].npoints > 1:
                        ics = self.sdata["RD"][ctag+qtag].coordinate if self.sdata["RD"][ctag+qtag].coordinate is not None else ocs
                        icp = self.sdata["RD"][ctag+qtag].coord_prefix if self.sdata["RD"][ctag+qtag].coordinate is not None else ocp
                        xraw = self.sdata["RD"][ctag+qtag]["X"].flatten()
                        xeraw = self.sdata["RD"][ctag+qtag]["XEB"].flatten()
                        if ics != ocs:
                            (xraw, dj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, ocp)
                            xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                        yraw = self.sdata["RD"][ctag+qtag][""].flatten()
                        yeraw = self.sdata["RD"][ctag+qtag]["EB"].flatten()
                        for ii in np.arange(0, len(self.sdata["RD"][ctag+qtag]["SRC"])):
                            filt = (self.sdata["RD"][ctag+qtag]["MAP"] == ii)
                            if np.any(filt):
                                xerr = self.sigma * xeraw[filt]
                                yerr = self.sigma * yeraw[filt]
                                dlabel = self.sdata["RD"][ctag+qtag]["SRC"][ii]
                                if self.xerrflag:
                                    eba = ax.errorbar(xraw[filt], yraw[filt]*scale, xerr=xerr, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                    self.points[qq].append(eba)
                                else:
                                    eba = ax.errorbar(xraw[filt], yraw[filt]*scale, yerr=yerr*scale, color='k', ls='', marker=styles[si], label=dlabel)
                                    self.points[qq].append(eba)
                                si = si + 1
                                if self.note[qq] is None:
                                    self.note[qq] = ax.annotate("", xy=(0,0), xytext=(20,20), textcoords="offset points", bbox={"boxstyle":"round", "fc":"w"})
                                    self.note[qq].set_visible(False)
                        if qtag in self.sdata["PD"] and self.sdata["PD"][ctag+qtag].npoints > 0:
                            xfit = self.sdata["PD"]["X"][""].flatten()
                            xefit = self.sdata["PD"]["X"]["EB"].flatten()
                            if self.sdata["META"]["CSO"] != ocs:
                                (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                                xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                            yfit = self.sdata["PD"][ctag+qtag][""].flatten()
                            yefit = self.sdata["PD"][ctag+qtag]["EB"].flatten()
                            ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                            yl = yfit - self.sigma * yefit
                            yu = yfit + self.sigma * yefit
                            ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                            ci = ci + 1
            elif qtag == "ZEFF" and qtag+"P" in self.sdata["PD"] and self.sdata["PD"][qtag+"P"].npoints > 0:
                xfit = self.sdata["PD"]["X"][""].flatten()
                xefit = self.sdata["PD"]["X"]["EB"].flatten()
                if self.sdata["META"]["CSO"] != ocs:
                    (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                    xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                yfit = self.sdata["PD"][qtag+"P"][""].flatten()
                yefit = self.sdata["PD"][qtag+"P"]["EB"].flatten()
                ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                yl = yfit - self.sigma * yefit
                yu = yfit + self.sigma * yefit
                ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0], xlims[1])
            pylabel = ylabel + r' [' + yunit + r']' if yunit else ylabel
            ax.set_ylabel(pylabel, fontsize=self.fs)
            ax.set_xlabel(pxlabel, fontsize=self.fs)
            ax.tick_params(axis='both', which='major', labelsize=self.fs)

            nplot = 2 * qq + 2
            ax = fig.add_subplot(ny, nx, nplot)
            si = 0
            ci = 0
            if qtag in self.sdata["RD"] and self.sdata["RD"][qtag].npoints > 1 and qtag in self.sdata["PD"] and self.sdata["PD"][qtag].npoints > 0 and "GRAD" in self.sdata["PD"][qtag]:
                xfit = self.sdata["PD"]["X"][""].flatten()
                xefit = self.sdata["PD"]["X"]["EB"].flatten()
                if self.sdata["META"]["CSO"] != ocs:
                    (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                    xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                yfit = self.sdata["PD"][qtag]["GRAD"].flatten()
                yefit = self.sdata["PD"][qtag]["GRADEB"].flatten()
                ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                yl = yfit - self.sigma * yefit
                yu = yfit + self.sigma * yefit
                ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                ci = ci + 1
            if qtag == "Q" and len(prefix_list) > 1:
                for jj in np.arange(0, len(prefix_list)):
                    ctag = prefix_list[jj]
                    if ctag+qtag in self.sdata["RD"] and self.sdata["RD"][ctag+qtag].npoints > 1 and ctag+qtag in self.sdata["PD"] and self.sdata["PD"][ctag+qtag].npoints > 0 and "GRAD" in self.sdata["PD"][ctag+qtag]:
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        xefit = self.sdata["PD"]["X"]["EB"].flatten()
                        if self.sdata["META"]["CSO"] != ocs:
                            (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                            xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                        yfit = self.sdata["PD"][ctag+qtag]["GRAD"].flatten()
                        yefit = self.sdata["PD"][ctag+qtag]["GRADEB"].flatten()
                        ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                        yl = yfit - self.sigma * yefit
                        yu = yfit + self.sigma * yefit
                        ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                        ci = ci + 1
            elif qtag == "ZEFF" and qtag+"P" in self.sdata["PD"] and self.sdata["PD"][qtag+"P"].npoints > 0 and "GRAD" in self.sdata["PD"][qtag+"P"]:
                xfit = self.sdata["PD"]["X"][""].flatten()
                xefit = self.sdata["PD"]["X"]["EB"].flatten()
                if self.sdata["META"]["CSO"] != ocs:
                    (xfit, dj, txefit) = self.sdata["CD"].convert(xfit, self.sdata["META"]["CSO"], ocs, self.sdata["META"]["CSOP"], ocp)
                    xefit = np.sqrt(np.power(xefit, 2.0) + np.power(txefit, 2.0))
                yfit = self.sdata["PD"][qtag+"P"]["GRAD"].flatten()
                yefit = self.sdata["PD"][qtag+"P"]["GRADEB"].flatten()
                ax.plot(xfit, yfit*scale, color=colors[ci], ls='-')
                yl = yfit - self.sigma * yefit
                yu = yfit + self.sigma * yefit
                ax.fill_between(xfit, yl*scale, yu*scale, facecolor=colors[ci], edgecolor='None', alpha=0.2)
                ci = ci + 1
            if fxlim:
                ax.set_xlim(xlims[0], xlims[1])
            pylabel = ylabel + r' Grad. [' if yunit or xunit else ylabel + r' Grad.'
            if yunit or xunit:
                pylabel = pylabel + yunit if yunit else pylabel + r'1'
                pylabel = pylabel + r' / ' + xunit + r']' if xunit else pylabel + r']'
            ax.set_ylabel(pylabel, fontsize=self.fs)
            ax.set_xlabel(pxlabel, fontsize=self.fs)
            ax.tick_params(axis='both', which='major', labelsize=self.fs)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)

        return gbox


class equilibrium_plotter(QtWidgets.QWidget):

    def __init__(self, data, fontsize=None):
        super(equilibrium_plotter, self).__init__()
        self.sdata = None
        self.canvas = None
        self.fs = 10
        if isinstance(data, dict):
            self.sdata = data
        if isinstance(fontsize, (float, int)):
            self.fs = int(fontsize)
        if self.sdata is not None:
            layout = self.initUI()
            self.setLayout(layout)
            self.setGeometry(10, 10, 500, 800)
            self.setWindowTitle('Equilibrium Plot')
            self.show()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Invalid input data.")
            msg.setFont(self._font)
            msg.exec_()

    def initUI(self):

        fig = mplfig.Figure(tight_layout=True)
        if "ED" in self.sdata and "PSI" in self.sdata["ED"] and self.sdata["ED"]["PSI"] is not None:
            ax = fig.add_subplot(111)
            cs = ax.contour(self.sdata["ED"]["PSI"]["X"], self.sdata["ED"]["PSI"]["Y"], self.sdata["ED"]["PSI"][""], 20)
            ax.clabel(cs, inline=True, fontsize=self.fs)
            ax.set_xlabel('Major Radius [m]', fontsize=self.fs)
            ax.set_ylabel('Height [m]', fontsize=self.fs)
            if "BND" in self.sdata["ED"] and self.sdata["ED"]["BND"] is not None:
                ax.plot(self.sdata["ED"]["BND"][""], self.sdata["ED"]["BND"]["X"], color='k')
            if "LIM" in self.sdata["ED"] and self.sdata["ED"]["LIM"] is not None:
                ax.plot(self.sdata["ED"]["LIM"][""], self.sdata["ED"]["LIM"]["X"], color='r')
            ax.tick_params(axis='both', which='major', labelsize=self.fs)

        self.canvas = mplqt.FigureCanvasQTAgg(fig)

        gbox = QtWidgets.QVBoxLayout()
        gbox.addWidget(self.canvas)

        return gbox


class EX2GK_JET(QtWidgets.QWidget):

    def __init__(self, ppfflag=False, imasflag=False):
        super(EX2GK_JET, self).__init__()
        self.pyversion = ".".join(str(i) for i in sys.version_info[:2])
        print("Using Python version: %s" % (self.pyversion))
        location = inspect.getsourcefile(EX2GK.extract_and_standardize_data)
        self.exsrcdir = os.path.dirname(location) + '/'
        print("Using EX2GK version: %s" % (__version__))
        print("Using EX2GK definition from: %s" % (self.exsrcdir))
        location = inspect.getsourcefile(customfilters.apply_custom_filters) if customfilters is not None else None
        self.filtsrcdir = os.path.dirname(location) + '/' if location is not None else "N/A"
        print("Using EX2GK custom filter definition from: %s" % (self.filtsrcdir))
        location = inspect.getsourcefile(EX2GK.ptools.gp.GaussianProcessRegression1D)
        self.gpsrcdir = os.path.dirname(location) + '/'
        print("Using GPR1D definition from: %s" % (self.gpsrcdir))
        uid = pwd.getpwuid(os.getuid())[0]
        self._user = uid if isinstance(uid, str) else ""
        self.device = "JET"
        self.sdata = None
        self.ppfflag = ppfflag
        self.imasflag = imasflag
#        self.dlist = dict()
        self.didx = None
        self.clist = []
        self.tdata = None
        self._preset_fields = data_fields
#        print("Using font: %s" % (self.font().family()))
        self._font = QtGui.QFont(self.font().family())
        self._font.setPixelSize(11)
        self._font_height = QtGui.QFontMetrics(self._font).height()
        layout = self.initUI()
        self.setLayout(layout)
        self.setGeometry(5, 0, 750, 300)
        self.setWindowTitle('EX2GK GUI (JET)')
        self.setFont(self._font)
        self.show()

    def initUI(self):

        self.LoadDataButton = QtWidgets.QPushButton("Load Data")
        self.LoadDataButton.clicked.connect(self._load_data)
        self.ViewTraceButton = QtWidgets.QPushButton("View Time Traces")
        self.ViewTraceButton.clicked.connect(self._plot_time_traces)
        self.ShotNumberEntry = QtWidgets.QLineEdit()
        self.ShotNumberEntry.setValidator(QtGui.QIntValidator(None))
        self.ShotNumberEntry.setMaxLength(6)
        self.ShotNumberEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeStartEntry = QtWidgets.QLineEdit()
        self.TimeStartEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeStartEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TimeEndEntry = QtWidgets.QLineEdit()
        self.TimeEndEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TimeEndEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.NTimeWindowsEntry = QtWidgets.QLineEdit("1")
        self.NTimeWindowsEntry.setValidator(QtGui.QIntValidator(None))
        self.NTimeWindowsEntry.setMaxLength(3)
        self.NTimeWindowsEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.InterfaceList = QtWidgets.QComboBox()
        self.InterfaceList.addItem("SAL")
        self.InterfaceList.addItem("MDSplus")
        self.InterfaceList.addItem("PPF")
        self.EquilibriumList = QtWidgets.QComboBox()
        self.EquilibriumList.addItem("EHTR")
        self.EquilibriumList.addItem("EFTM")
        self.EquilibriumList.addItem("EFTF")
        self.EquilibriumList.addItem("EFTP")
        self.EquilibriumList.addItem("EFIT")
        self.EquilibriumList.currentIndexChanged.connect(self._recompile_data_list)

        twbox = QtWidgets.QFormLayout()
        twbox.addRow("Shot Number", self.ShotNumberEntry)
        twbox.addRow("Time Start (s)", self.TimeStartEntry)
        twbox.addRow("Time End (s)", self.TimeEndEntry)
        twbox.addRow("No. of Windows", self.NTimeWindowsEntry)
        twbox.addRow("Data Interface", self.InterfaceList)
        twbox.addRow("Equilibrium", self.EquilibriumList)

        self.ListInstructions = QtWidgets.QLabel("Double-click a window entry to load it into the right panel. Otherwise, the currently loaded entry is highlighted.")
        self.ListInstructions.setWordWrap(True)
        self.DatabaseList = QtWidgets.QTableWidget()
        self.DatabaseList.setEnabled(False)
        self.DatabaseList.setColumnCount(1)
        self.DatabaseList.setHorizontalHeaderLabels(["Time Window"])
        if fqt5:
            self.DatabaseList.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        else:
            self.DatabaseList.horizontalHeader().setResizeMode(QtWidgets.QHeaderView.Stretch)
        self.DatabaseList.verticalHeader().setDefaultSectionSize(int(self._font_height * 2))
        self.DatabaseList.verticalHeader().setVisible(False)
        self.DatabaseList.cellDoubleClicked.connect(self._select_data)
        self.SortWindowsButton = QtWidgets.QPushButton("Sort Windows")
        self.SortWindowsButton.setEnabled(False)
        self.SortWindowsButton.clicked.connect(self._generate_table)
        self.DeleteWindowButton = QtWidgets.QPushButton("Delete Window")
        self.DeleteWindowButton.setEnabled(False)
        self.DeleteWindowButton.clicked.connect(self._delete_current_row)

        wmbox = QtWidgets.QHBoxLayout()
        wmbox.addWidget(self.SortWindowsButton)
        wmbox.addWidget(self.DeleteWindowButton)

        dlbox = QtWidgets.QVBoxLayout()
        dlbox.addWidget(self.ListInstructions)
        dlbox.addWidget(self.DatabaseList)
        dlbox.addLayout(wmbox)

        svbox = QtWidgets.QVBoxLayout()
        svbox.addWidget(self.LoadDataButton)
        svbox.addWidget(self.ViewTraceButton)
        svbox.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 3), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        svbox.addLayout(twbox)
        svbox.addSpacerItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        svbox.addLayout(dlbox)

        self.TabPanel = QtWidgets.QTabWidget()

        self.BasicOptionsTab = QtWidgets.QWidget()
        self.BasicOptionsTab.setLayout(self.BasicUI())
        self.DataSpecificationTab = QtWidgets.QWidget()
        self.DataSpecificationTab.setLayout(self.DataSpecificationUI())
#        self.ProfileModificationTab = QtWidgets.QWidget()
#        self.ProfileModificationTab.setLayout(self.ModificationUI())
        self.AdvancedOptionsTab = QtWidgets.QWidget()
        self.AdvancedOptionsTab.setLayout(self.AdvancedUI())

        self.TabPanel.addTab(self.BasicOptionsTab, "Standard")
        self.TabPanel.addTab(self.DataSpecificationTab, "Data List")
#        self.TabPanel.addTab(self.ProfileModificationTab, "Modification")
        self.TabPanel.addTab(self.AdvancedOptionsTab, "Unsupported")

        hbox = QtWidgets.QHBoxLayout()
        hbox.addLayout(svbox, 40)
        hbox.addItem(QtWidgets.QSpacerItem(int(self._font_height / 2), 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        hbox.addWidget(self.TabPanel, 70)

        self._set_gui_default_settings()

        return hbox

    def BasicUI(self):

        self.ExtractingLabel = QtWidgets.QLabel("Extracting")
        self.GrabAllBox = QtWidgets.QCheckBox("All")
        self.GrabAllBox.clicked.connect(self._enable_all_quantities)
        self.GrabElectronBox = QtWidgets.QCheckBox("Electrons")
        self.GrabElectronBox.clicked.connect(self._reset_all_box)
        self.GrabElectronBox.toggled.connect(self._recompile_data_list)
        self.GrabIonBox = QtWidgets.QCheckBox("Ions")
        self.GrabIonBox.clicked.connect(self._reset_all_box)
        self.GrabIonBox.toggled.connect(self._recompile_data_list)
        self.GrabFastIonBox = QtWidgets.QCheckBox("Fast ions")
        self.GrabFastIonBox.clicked.connect(self._reset_all_box)
        self.GrabFastIonBox.toggled.connect(self._recompile_data_list)
        self.GrabMomentumBox = QtWidgets.QCheckBox("Momentum")
        self.GrabMomentumBox.clicked.connect(self._reset_all_box)
        self.GrabMomentumBox.toggled.connect(self._recompile_data_list)
        self.GrabSourceBox = QtWidgets.QCheckBox("Sources")
        self.GrabSourceBox.clicked.connect(self._reset_all_box)
        self.GrabSourceBox.toggled.connect(self._recompile_data_list)

        qgbox = QtWidgets.QGridLayout()
        qgbox.addWidget(self.GrabAllBox, 1, 0)
        qgbox.addWidget(self.GrabElectronBox, 1, 1)
        qgbox.addWidget(self.GrabIonBox, 1, 2)
        qgbox.addWidget(self.GrabFastIonBox, 2, 0)
        qgbox.addWidget(self.GrabMomentumBox, 2, 1)
        qgbox.addWidget(self.GrabSourceBox, 2, 2)

        self.ExtractDataButton = QtWidgets.QPushButton("Extract Data")
        self.ExtractDataButton.clicked.connect(self._extract_data)
        self.SaveRawDataButton = QtWidgets.QPushButton("Print Raw Data")
        self.SaveRawDataButton.setEnabled(False)
        self.SaveRawDataButton.clicked.connect(self._save_raw_data)

        edbox = QtWidgets.QHBoxLayout()
        edbox.addWidget(self.ExtractDataButton)
        edbox.addWidget(self.SaveRawDataButton)

        qqbox = QtWidgets.QVBoxLayout()
        qqbox.addWidget(self.ExtractingLabel)
        qqbox.addLayout(qgbox)
        qqbox.addLayout(edbox)

        self.FittingLabel = QtWidgets.QLabel("Fitting (Fit using rho_tor_norm or psi_pol_norm and change afterwards for visualization)")
        self.FittingLabel.setEnabled(False)
        self.CoordinateList = QtWidgets.QComboBox()
        self.CoordinateList.setEnabled(False)

        csbox = QtWidgets.QFormLayout()
        csbox.addRow("Output Coordinate", self.CoordinateList)

        self.UseLinearFastIonFitBox = QtWidgets.QCheckBox("Linear interp. on fast ions")
        self.UseLinearFastIonFitBox.setEnabled(False)
        self.UseLinearSourceFitBox = QtWidgets.QCheckBox("Linear interp. on sources")
        self.UseLinearSourceFitBox.setEnabled(False)
        self.UseLinearSourceFitBox.setChecked(True)

        libox = QtWidgets.QHBoxLayout()
        libox.addWidget(self.UseLinearFastIonFitBox)
        libox.addWidget(self.UseLinearSourceFitBox)

        self.FitDataButton = QtWidgets.QPushButton("Fit Window")
        self.FitDataButton.setEnabled(False)
        self.FitDataButton.clicked.connect(self._fit_data)
        self.FitAllDataButton = QtWidgets.QPushButton("Fit All Windows")
        self.FitAllDataButton.setEnabled(False)
        self.FitAllDataButton.clicked.connect(self._fit_all_data)
        self.SaveFitDataButton = QtWidgets.QPushButton("Print Fit Data")
        self.SaveFitDataButton.setEnabled(False)
        self.SaveFitDataButton.clicked.connect(self._save_fit_data)

        fdbox = QtWidgets.QHBoxLayout()
        fdbox.addWidget(self.FitDataButton)
        fdbox.addWidget(self.FitAllDataButton)
        fdbox.addWidget(self.SaveFitDataButton)

        dobox = QtWidgets.QVBoxLayout()
        dobox.addWidget(self.FittingLabel)
        dobox.addLayout(libox)
        dobox.addLayout(csbox)
        dobox.addLayout(fdbox)

        self.PlottingLabel = QtWidgets.QLabel("Plotting")
        self.PlottingLabel.setEnabled(False)
        self.KeepFitGradientsBox = QtWidgets.QCheckBox("Show gradients computed w.r.t. fit coordinate")
        self.KeepFitGradientsBox.setEnabled(False)

        self.PlotEquilibriumButton = QtWidgets.QPushButton("Equilibrium")
        self.PlotEquilibriumButton.setEnabled(False)
        self.PlotEquilibriumButton.clicked.connect(self._plot_equilibrium_data)
        self.PlotProfileButton = QtWidgets.QPushButton("Profiles")
        self.PlotProfileButton.setEnabled(False)
        self.PlotProfileButton.clicked.connect(self._plot_profile_data)
        self.PlotGradientButton = QtWidgets.QPushButton("Gradients")
        self.PlotGradientButton.setEnabled(False)
        self.PlotGradientButton.clicked.connect(self._plot_gradient_data)
        self.PlotSourceButton = QtWidgets.QPushButton("Sources")
        self.PlotSourceButton.setEnabled(False)
        self.PlotSourceButton.clicked.connect(self._plot_source_data)
        self.PlotMiscButton = QtWidgets.QPushButton("Misc.")
        self.PlotMiscButton.setEnabled(False)
        self.PlotMiscButton.clicked.connect(self._plot_misc_data)

        pbbox = QtWidgets.QGridLayout()        
        pbbox.addWidget(self.PlotEquilibriumButton, 0, 0)
        pbbox.addWidget(self.PlotProfileButton, 0, 1)
        pbbox.addWidget(self.PlotGradientButton, 0, 2)
        pbbox.addWidget(self.PlotSourceButton, 0, 3)
        pbbox.addWidget(self.PlotMiscButton, 0, 4)

        plbox = QtWidgets.QVBoxLayout()
        plbox.addWidget(self.PlottingLabel)
        plbox.addWidget(self.KeepFitGradientsBox)
        plbox.addLayout(pbbox)

        self.CheckingLabel = QtWidgets.QLabel("Checking")
        self.CheckingLabel.setEnabled(False)
        self.PassSigmaEntry = QtWidgets.QLineEdit()
        self.PassSigmaEntry.setEnabled(False)
        self.PassSigmaEntry.setValidator(QtGui.QDoubleValidator(None))
        self.PassSigmaEntry.setAlignment(QtCore.Qt.AlignLeft)

        pwbox = QtWidgets.QFormLayout()
        pwbox.addRow("Pass Width Criterion", self.PassSigmaEntry)

        self.QualityCheckButton = QtWidgets.QPushButton("Perform Basic Checks")
        self.QualityCheckButton.setEnabled(False)
        self.QualityCheckButton.clicked.connect(self._run_quality_checks)
        self.ExplorationModeButton = QtWidgets.QPushButton("Data Exploration Mode")
        self.ExplorationModeButton.setEnabled(False)
        self.ExplorationModeButton.clicked.connect(self._enter_exploration_mode)

        cdbox = QtWidgets.QHBoxLayout()
        cdbox.addWidget(self.QualityCheckButton)
        cdbox.addWidget(self.ExplorationModeButton)

        qcbox = QtWidgets.QVBoxLayout()
        qcbox.addWidget(self.CheckingLabel)
        qcbox.addLayout(pwbox)
        qcbox.addLayout(cdbox)

        self.ProcessingLabel = QtWidgets.QLabel("Processing")
        self.ProcessingLabel.setEnabled(False)
        self.CodeAdapterList = QtWidgets.QComboBox()
        self.CodeAdapterList.setEnabled(False)
        self.CodeAdapterList.addItem("General - Use output coordinate")
        self.CodeAdapterList.addItem("QuaLiKiz")

        cabox = QtWidgets.QFormLayout()
        cabox.addRow("Target Code", self.CodeAdapterList)

        self.ProcessDataButton = QtWidgets.QPushButton("Process Window")
        self.ProcessDataButton.setEnabled(False)
        self.ProcessDataButton.clicked.connect(self._post_process_data)
        self.SaveProcessDataButton = QtWidgets.QPushButton("Print Processed Data")
        self.SaveProcessDataButton.setEnabled(False)
        self.SaveProcessDataButton.clicked.connect(self._save_post_processed_data)
        self.PlotProcessButton = QtWidgets.QPushButton("Plot Processed Data")
        self.PlotProcessButton.setEnabled(False)
        self.PlotProcessButton.clicked.connect(self._plot_processed_gradient_data)

        pdbox = QtWidgets.QHBoxLayout()
        pdbox.addWidget(self.ProcessDataButton)
        pdbox.addWidget(self.SaveProcessDataButton)
        pdbox.addWidget(self.PlotProcessButton)

        ppbox = QtWidgets.QVBoxLayout()
        ppbox.addWidget(self.ProcessingLabel)
        ppbox.addLayout(cabox)
        ppbox.addLayout(pdbox)

        self.SavingLabel = QtWidgets.QLabel("Saving Current Item")
        self.SavingLabel.setEnabled(False)
        self.MakeEQDSKButton = QtWidgets.QPushButton("EQDSK")
        self.MakeEQDSKButton.setEnabled(False)
        self.MakeEQDSKButton.clicked.connect(self._generate_eqdsk)
        self.MakeMATLABButton = QtWidgets.QPushButton("MAT File")
        self.MakeMATLABButton.setEnabled(False)
        self.MakeMATLABButton.clicked.connect(self._generate_matfile)
        self.MakeIMASButton = QtWidgets.QPushButton("IMAS IDS")
        self.MakeIMASButton.setEnabled(False)
        self.MakeIMASButton.clicked.connect(self._generate_imas)

        self.SavingAllLabel = QtWidgets.QLabel("Saving Entire List")
        self.SavingAllLabel.setEnabled(False)
        self.WritePickleButton = QtWidgets.QPushButton("Pickle")
        self.WritePickleButton.setEnabled(False)
        self.WritePickleButton.clicked.connect(self._save_database_to_pickle)
        self.WritePPFButton = QtWidgets.QPushButton("PPF")
        self.WritePPFButton.setEnabled(False)
        self.WritePPFButton.clicked.connect(self._save_database_to_ppf)
        self.WriteIMASButton = QtWidgets.QPushButton("IMAS")
        self.WriteIMASButton.setEnabled(False)
        self.WriteIMASButton.clicked.connect(self._save_database_to_imas)

        svbox = QtWidgets.QHBoxLayout()
        svbox.addWidget(self.MakeEQDSKButton)
        svbox.addWidget(self.MakeMATLABButton)
        svbox.addWidget(self.MakeIMASButton)

        sdbox = QtWidgets.QHBoxLayout()
        sdbox.addWidget(self.WritePickleButton)
        sdbox.addWidget(self.WritePPFButton)
        sdbox.addWidget(self.WriteIMASButton)

        sobox = QtWidgets.QVBoxLayout()
        sobox.addWidget(self.SavingLabel)
        sobox.addLayout(svbox)
        sobox.addWidget(self.SavingAllLabel)
        sobox.addLayout(sdbox)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(qqbox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(dobox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(plbox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(qcbox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(ppbox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(sobox)
        layout.addStretch(1)

        return layout

    def DataSpecificationUI(self):

        self.DataSpecificationList = QtWidgets.QTableWidget()
        self.DataSpecificationList.setEnabled(False)
        self.DataSpecificationList.setColumnCount(3)
        self.DataSpecificationList.setHorizontalHeaderLabels(["DDA", "Seq. No.", "User ID"])
        if fqt5:
            self.DataSpecificationList.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        else:
            self.DataSpecificationList.horizontalHeader().setResizeMode(QtWidgets.QHeaderView.Stretch)
        self.DataSpecificationList.verticalHeader().setDefaultSectionSize(int(self._font_height * 1.5))
        self.DataSpecificationList.verticalHeader().setVisible(False)

        self._recompile_data_list()

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.DataSpecificationList)

        return layout

    def ModificationUI(self):

        self.WarpFunctionList = QtWidgets.QComboBox()
        self.WarpFunctionList.setEnabled(False)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.WarpFunctionList)

        return layout

    def AdvancedUI(self):

        self.AcceptRiskBox = QtWidgets.QCheckBox("I solemnly swear that I am up to no good.")
        self.AcceptRiskBox.toggled.connect(self._enable_advanced_menu)

        self.BasicDebugBox = QtWidgets.QCheckBox("Print basic debug statements")
        self.BasicDebugBox.setEnabled(False)
        self.DebugEquilibriumBox = QtWidgets.QCheckBox("Print coordinate system data")
        self.DebugEquilibriumBox.setEnabled(False)

        self.NonFlatTopBox = QtWidgets.QCheckBox("Disable current flat-top options")
        self.NonFlatTopBox.setEnabled(False)
        self.UsePresetHypsBox = QtWidgets.QCheckBox("Use preset hyperparamters, no optimization")
        self.UsePresetHypsBox.setEnabled(False)
        self.UseELMFilterBox = QtWidgets.QCheckBox("Use ELM filtering on edge data")
        self.UseELMFilterBox.setEnabled(False)
        self.UseRZRemappingBox = QtWidgets.QCheckBox("Use (r,z) coordinates for remapping")
        self.UseRZRemappingBox.setEnabled(False)
        self.UseRMajorShiftBox = QtWidgets.QCheckBox("Use major radius shift")
        self.UseRMajorShiftBox.setEnabled(False)
        self.UseDensityScaleBox = QtWidgets.QCheckBox("Use density scale from synthetic line-integrated signals")
        self.UseDensityScaleBox.setEnabled(False)
        self.UseEdgeQScaleBox = QtWidgets.QCheckBox("Use edge safety factor scale from total plasma current")
        self.UseEdgeQScaleBox.setEnabled(False)
        self.UsePureExpDataBox = QtWidgets.QCheckBox("Ignore all heuristic filters / mods")
        self.UsePureExpDataBox.setEnabled(False)
        self.UsePureExpDataBox.toggled.connect(self._set_all_filters)
        self.UseOnlyExpFiltersBox = QtWidgets.QCheckBox("Ignore heuristic filters / mods for tuning GP fits")
        self.UseOnlyExpFiltersBox.setEnabled(False)
        self.UseOnlyExpFiltersBox.toggled.connect(self._set_advanced_filters)
        self.PasteEdgeTEBox = QtWidgets.QCheckBox("Copy edge electron temperature as edge ion temperature")
        self.PasteEdgeTEBox.setEnabled(False)
        self.UseStdOfMeanBox = QtWidgets.QCheckBox("Use standard deviation of mean as errors")
        self.UseStdOfMeanBox.setEnabled(False)
        self.KeepRhoErrorBox = QtWidgets.QCheckBox("Keep x-errors from mapping correction")
        self.KeepRhoErrorBox.setEnabled(False)
        self.ForceLModeBox = QtWidgets.QCheckBox("Force use of L-mode processing options")
        self.ForceLModeBox.setEnabled(False)
        self.ForceSmoothNEBox = QtWidgets.QCheckBox("Force smooth kernel on electron density GP fit")
        self.ForceSmoothNEBox.setEnabled(False)
        self.UseECEDiagnosticBox = QtWidgets.QCheckBox("Include ECE diagnostics for electron temperature") 
        self.UseECEDiagnosticBox.setEnabled(False)
        self.UseECEDiagnosticBox.toggled.connect(self._add_ece_diagnostics)
        self.MirrorECEDiagnosticBox = QtWidgets.QCheckBox("Remap HFS ECE measurements onto LFS radial coordinates") 
        self.MirrorECEDiagnosticBox.setEnabled(False)

        opbox = QtWidgets.QVBoxLayout()
        opbox.addWidget(self.UsePresetHypsBox)
        opbox.addWidget(self.NonFlatTopBox)
        opbox.addWidget(self.UseELMFilterBox)
        opbox.addWidget(self.UseRZRemappingBox)
        opbox.addWidget(self.UseRMajorShiftBox)
        opbox.addWidget(self.UseDensityScaleBox)
        opbox.addWidget(self.UseEdgeQScaleBox)
        opbox.addWidget(self.UsePureExpDataBox)
        #opbox.addWidget(self.UseOnlyExpFiltersBox)
        opbox.addWidget(self.PasteEdgeTEBox)
        opbox.addWidget(self.UseStdOfMeanBox)
        #opbox.addWidget(self.KeepRhoErrorBox)
        opbox.addWidget(self.ForceLModeBox)
        opbox.addWidget(self.ForceSmoothNEBox)
        opbox.addWidget(self.UseECEDiagnosticBox)
        opbox.addWidget(self.MirrorECEDiagnosticBox)

        self.NEErrorMultiplierEntry = QtWidgets.QLineEdit()
        self.NEErrorMultiplierEntry.setEnabled(False)
        self.NEErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
        self.NEErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TEErrorMultiplierEntry = QtWidgets.QLineEdit()
        self.TEErrorMultiplierEntry.setEnabled(False)
        self.TEErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TEErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.NIErrorMultiplierEntry = QtWidgets.QLineEdit()
#        self.NIErrorMultiplierEntry.setEnabled(False)
#        self.NIErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.NIErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.TIErrorMultiplierEntry = QtWidgets.QLineEdit()
#        self.TIErrorMultiplierEntry.setEnabled(False)
#        self.TIErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.TIErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.NIMPErrorMultiplierEntry = QtWidgets.QLineEdit()
#        self.NIMPErrorMultiplierEntry.setEnabled(False)
#        self.NIMPErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.NIMPErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.TIMPErrorMultiplierEntry = QtWidgets.QLineEdit()
        self.TIMPErrorMultiplierEntry.setEnabled(False)
        self.TIMPErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
        self.TIMPErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
        self.AFErrorMultiplierEntry = QtWidgets.QLineEdit()
        self.AFErrorMultiplierEntry.setEnabled(False)
        self.AFErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
        self.AFErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.QErrorMultiplierEntry = QtWidgets.QLineEdit()
#        self.QErrorMultiplierEntry.setEnabled(False)
#        self.QErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.QErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)
#        self.ZEFFErrorMultiplierEntry = QtWidgets.QLineEdit()
#        self.ZEFFErrorMultiplierEntry.setEnabled(False)
#        self.ZEFFErrorMultiplierEntry.setValidator(QtGui.QDoubleValidator(None))
#        self.ZEFFErrorMultiplierEntry.setAlignment(QtCore.Qt.AlignLeft)

        embox = QtWidgets.QFormLayout()
        embox.addRow("NE error mult.:", self.NEErrorMultiplierEntry)
        embox.addRow("TE error mult.:", self.TEErrorMultiplierEntry)
#        embox.addRow("NI error mult.:", self.NIErrorMultiplierEntry)
#        embox.addRow("TI error mult.:", self.TIErrorMultiplierEntry)
#        embox.addRow("NIMP error mult.:", self.NIMPErrorMultiplierEntry)
        embox.addRow("TIMP error mult.:", self.TIMPErrorMultiplierEntry)
        embox.addRow("AF error mult.:", self.AFErrorMultiplierEntry)
#        embox.addRow("Q error mult.:", self.QErrorMultiplierEntry)
#        embox.addRow("ZEFF error mult.:", self.ZEFFErrorMultiplierEntry)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.AcceptRiskBox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height * 1.25), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addWidget(self.BasicDebugBox)
        layout.addWidget(self.DebugEquilibriumBox)
#        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
#        layout.addLayout(cqbox)
#        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
#        layout.addWidget(self.GrabMainTIBox)
#        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
#        layout.addLayout(csbox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(opbox)
        layout.addItem(QtWidgets.QSpacerItem(0, int(self._font_height / 2), QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        layout.addLayout(embox)
        layout.addStretch(1)

        return layout

    def _set_gui_default_settings(self):
        self.InterfaceList.setCurrentIndex(0)
        self.EquilibriumList.setCurrentIndex(4)
        self.GrabElectronBox.setChecked(True)
        self.PassSigmaEntry.setText("")
        self.CodeAdapterList.setCurrentIndex(0)
        self.UseELMFilterBox.setChecked(True)
        self.UseRZRemappingBox.setChecked(True)
        self.UseRMajorShiftBox.setChecked(True)
        self.UseDensityScaleBox.setChecked(True)
        self.UseEdgeQScaleBox.setChecked(True)
        self.PasteEdgeTEBox.setChecked(True)
        self.UseECEDiagnosticBox.setChecked(True)

    def _add_data_specification_entry(self, key):
        if isinstance(key, str) and key not in jpf_fields:
            tidx = self.DataSpecificationList.rowCount()
            DDAItem = QtWidgets.QTableWidgetItem(key+'/*')
            DDAItem.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            SEQItem = QtWidgets.QTableWidgetItem('0')
            UIDItem = QtWidgets.QTableWidgetItem('jetppf')
            self.DataSpecificationList.insertRow(tidx)
            self.DataSpecificationList.setItem(tidx, 0, DDAItem)
            self.DataSpecificationList.setItem(tidx, 1, SEQItem)
            self.DataSpecificationList.setItem(tidx, 2, UIDItem)

    def _delete_data_specification_entry(self, key):
        if isinstance(key, str):
            idxv = []
            tidx = self.DataSpecificationList.rowCount()
            for idx in np.arange(0, tidx):
                ientry = self.DataSpecificationList.item(idx, 0).text()
                ikey = ientry.split('/')[0]
                if re.match(ikey, key, flags=re.IGNORECASE):
                    idxv.append(idx)
            for ii in np.arange(len(idxv), 0, -1):
                self.DataSpecificationList.removeRow(idxv[ii-1])

    def _get_data_specification_entry(self, key):
        seq = '0'
        uid = 'jetppf'
        if isinstance(key, str):
            idxv = []
            tidx = self.DataSpecificationList.rowCount()
            for idx in np.arange(0, tidx):
                ientry = self.DataSpecificationList.item(idx, 0).text()
                ikey = ientry.split('/')[0]
                if re.match(r'^'+ikey+r'$', key, flags=re.IGNORECASE):
                    seq = self.DataSpecificationList.item(idx, 1).text().strip()
                    if not re.match(r'^[0-9]+$', seq):
                        seq = '0'
                    uid = self.DataSpecificationList.item(idx, 2).text().strip().lower()
                    if not re.match(r'^[a-z0-9]+$', uid):
                        uid = 'jetppf'
        if not seq:
            seq = '0'
        if not uid:
            uid = 'jetppf'
        return (seq, uid)

    def _recompile_data_list(self):
        clist = []
        tidx = self.DataSpecificationList.rowCount()
        for idx in np.arange(0, tidx):
            ientry = self.DataSpecificationList.item(idx, 0).text()
            clist.append(ientry.split('/')[0].lower())
        nlist = list(data_fields["*"].keys())
        nlist.append(self.EquilibriumList.currentText().lower())
        fchecked = False
        if self.GrabElectronBox.isChecked():
            if "NE" in self._preset_fields:
                for key in self._preset_fields["NE"]:
                    if key.lower() not in nlist:
                        nlist.append(key.lower())
            if "TE" in self._preset_fields:
                for key in self._preset_fields["TE"]:
                    if key == 'kk3':
                        if self.UseECEDiagnosticBox.isChecked():
                            if key.lower() not in nlist:
                                nlist.append(key.lower())
                            if 'efit' not in nlist:
                                nlist.append('efit')
                    elif key.lower() not in nlist:
                        nlist.append(key.lower())
        if self.GrabIonBox.isChecked():
            if "TI" in self._preset_fields:
                for key in self._preset_fields["TI"]:
                    if key.lower() not in nlist:
                        nlist.append(key.lower())
            if "NIMP" in self._preset_fields:
                for key in self._preset_fields["NIMP"]:
                    if key.lower() not in nlist:
                        nlist.append(key.lower())
        if self.GrabFastIonBox.isChecked():
            if "FI" in self._preset_fields:
                for key in self._preset_fields["FI"]:
                    if key.lower() not in nlist:
                        nlist.append(key.lower())
        if self.GrabMomentumBox.isChecked():
            if "AF" in self._preset_fields:
                for key in self._preset_fields["AF"]:
                    if key.lower() not in nlist:
                        nlist.append(key.lower())
        if self.GrabSourceBox.isChecked():
            if "QSTJ" in self._preset_fields:
                for key in self._preset_fields["QSTJ"]:
                    if key.lower() not in nlist:
                        nlist.append(key.lower())
        for ckey in clist:
            if ckey not in nlist:
                self._delete_data_specification_entry(ckey)
        for nkey in nlist:
            if nkey not in clist:
                self._add_data_specification_entry(nkey)

    def _enable_advanced_menu(self):
        self.BasicDebugBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.DebugEquilibriumBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.NonFlatTopBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UsePresetHypsBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseELMFilterBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseRZRemappingBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseRMajorShiftBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseDensityScaleBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseEdgeQScaleBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UsePureExpDataBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseOnlyExpFiltersBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.PasteEdgeTEBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseStdOfMeanBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.KeepRhoErrorBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.ForceLModeBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.ForceSmoothNEBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.UseECEDiagnosticBox.setEnabled(self.AcceptRiskBox.isChecked())
        self.MirrorECEDiagnosticBox.setEnabled(self.AcceptRiskBox.isChecked() and self.UseECEDiagnosticBox.isChecked())
        self.NEErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
        self.TEErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.NIErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.TIErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.NIMPErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
        self.TIMPErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
        self.AFErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.QErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
#        self.ZEFFErrorMultiplierEntry.setEnabled(self.AcceptRiskBox.isChecked())
        self.DataSpecificationList.setEnabled(self.AcceptRiskBox.isChecked())

    def _enable_all_quantities(self):
        self.GrabElectronBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabIonBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabFastIonBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabMomentumBox.setChecked(self.GrabAllBox.isChecked())
        self.GrabSourceBox.setChecked(self.GrabAllBox.isChecked())

    def _reset_all_box(self):
        self.GrabAllBox.setChecked(self.GrabElectronBox.isChecked() and \
                                   self.GrabIonBox.isChecked() and \
                                   self.GrabFastIonBox.isChecked() and \
                                   self.GrabMomentumBox.isChecked() and \
                                   self.GrabSourceBox.isChecked())

    def _enable_fast_ions(self):
        self.UseLinearFastIonFitBox.setEnabled(self.sdata is not None and self.GrabFastIonBox.isChecked())

    def _enable_sources(self):
        self.UseLinearSourceFitBox.setEnabled(self.sdata is not None and self.GrabSourceBox.isChecked())

    def _set_all_filters(self):
        if self.UsePureExpDataBox.isChecked() and not self.UseOnlyExpFiltersBox.isChecked():
            self.UseOnlyExpFiltersBox.setChecked(True)

    def _set_advanced_filters(self):
        if self.UsePureExpDataBox.isChecked() and not self.UseOnlyExpFiltersBox.isChecked():
            self.UsePureExpDataBox.setChecked(False)

    def _add_ece_diagnostics(self):
        self.MirrorECEDiagnosticBox.setEnabled(self.AcceptRiskBox.isChecked() and self.UseECEDiagnosticBox.isChecked())
        self._recompile_data_list()

    def _extract_time_data(self):
        textn = self.ShotNumberEntry.text()
        if textn:
            snum = int(textn)
            uname = None
            passwd = None
            if not currenthost.endswith(".jet.uk") and not currenthost.endswith(".jetdata.eu"):
                dialog = QtWidgets.QInputDialog()
                dialog.setFont(self._font)
                (value, ok) = dialog.getText(self, "Login required", "UKAEA username:")
                if ok and value:
                    uname = value
                if self.InterfaceList.currentText().lower() == 'sal':
                    (value, ok) = dialog.getText(self, "Password required", "UKAEA SecureID:", QtWidgets.QLineEdit.Password)
                    if ok and value:
                        passwd = value
            if self.tdata is None or (self.tdata is not None and self.tdata["SHOT"] != snum):
                exnamelist = dict()
                exnamelist["MACHINE"] = 'JET'
                exnamelist["SHOTINFO"] = "%8d" % (snum)
                exnamelist["INTERFACE"] = self.InterfaceList.currentText().lower()
                exnamelist["USERNAME"] = uname
                exnamelist["PASSWORD"] = passwd
                try:
                    print("Time trace extraction started...")
                    tic = time.perf_counter()
                    tdata = EX2GK.extract_time_traces(exnamelist["MACHINE"], namelist=exnamelist)
                    if isinstance(tdata, dict) and "TIP" in tdata and tdata["TIP"] is not None:
                        self.tdata = copy.deepcopy(tdata)
                    else:
                        raise TypeError("Unknown extraction failure within machine-specific adapter.")
                    toc = time.perf_counter()
                    print("Time trace extraction finished. Time elapsed: %.6f" % (toc - tic))
                except Exception as e:
                    print(repr(e))
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Critical)
                    msg.setWindowTitle("Fatal Error")
                    msg.setText("Time trace extraction procedure failed for selected discharge.")
                    msg.setFont(self._font)
                    msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Entry Error")
            msg.setText("Unrecognized input data.")
            msg.setFont(self._font)
            msg.exec_()

    def _make_get_list(self):
        getlist = None
        qextract = []
        if self.GrabElectronBox.isChecked():
            qextract.append("NE")
            qextract.append("TE")
        if self.GrabIonBox.isChecked():
            qextract.append("TI")
            qextract.append("NIMP")
        if self.GrabMomentumBox.isChecked():
            qextract.append("AF")
        if self.GrabFastIonBox.isChecked():
            qextract.append("FI")
        if self.GrabSourceBox.isChecked():
            qextract.append("QSTJ")
        if len(qextract) > 0:
            qextract.append("*")
            qextract.append("EQ")
#            qextract.append("AEQ")
        eqtag = self.EquilibriumList.currentText().lower()
        for var in qextract:
            if var == "EQ" and eqtag in self._preset_fields[var]:
                value = self._preset_fields[var][eqtag]
                (seq, uid) = self._get_data_specification_entry(eqtag) if self.AcceptRiskBox.isChecked() else ('0', 'jetppf')
                for kk in np.arange(0, len(value)):
                    getlist[eqtag+"/"+value[kk][0]] = ["---", "%d" % (value[kk][1]), seq, uid]
            elif var in self._preset_fields:
                if getlist is None:
                    getlist = dict()
                for key, value in self._preset_fields[var].items():
                    (seq, uid) = self._get_data_specification_entry(key.lower()) if self.AcceptRiskBox.isChecked() else ('0', 'jetppf')
                    if key in ["kk3", "ecm1", "ecm2"]:
                        if self.UseECEDiagnosticBox.isChecked():
                            for kk in np.arange(0, len(value)):
                                getlist[key+"/"+value[kk][0]] = ["---", "%d" % (value[kk][1]), seq, uid]
                            if eqtag != "efit":
                                for key, value in self._preset_fields["B"].items():
                                    (seq, uid) = self._get_data_specification_entry(key.lower()) if self.AcceptRiskBox.isChecked() else ('0', 'jetppf')
                                    for kk in np.arange(0, len(value)):
                                        getlist[key+"/"+value[kk][0]] = ["---", "%d" % (value[kk][1]), seq, uid]
                    else:
                        for kk in np.arange(0, len(value)):
                            getlist[key+"/"+value[kk][0]] = ["---", "%d" % (value[kk][1]), seq, uid]
        return getlist

    def _extract_data(self):
        textn = self.ShotNumberEntry.text()
        text1 = self.TimeStartEntry.text()
        text2 = self.TimeEndEntry.text()
        textt = self.NTimeWindowsEntry.text()
        if textn and text1 and text2:
            snum = int(textn)
            t1 = float(text1)
            t2 = float(text2)
            tt = int(textt)
            uname = None
            passwd = None
            retval = QtWidgets.QMessageBox.Yes
            if not currenthost.endswith(".jet.uk") and not currenthost.endswith(".jetdata.eu"):
                dialog = QtWidgets.QInputDialog()
                dialog.setFont(self._font)
                (value, ok) = dialog.getText(self, "Login required", "UKAEA username:")
                if ok and value:
                    uname = value
                if self.InterfaceList.currentText().lower() == 'sal':
                    (value, ok) = dialog.getText(self, "Password required", "UKAEA SecureID:", QtWidgets.QLineEdit.Password)
                    if ok and value:
                        passwd = value
            if t2 < t1:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setWindowTitle("Invalid Time Window")
                msg.setText("Time window end must be larger than time window start.")
                msg.setFont(self._font)
                retval = QtWidgets.QMessageBox.No
            elif (t2 - t1) / float(tt) < 0.099:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Question)
                msg.setWindowTitle("Time Windows Too Short")
                msg.setText("It is not recommended to have time windows covering less than 100 ms, due to general limited availability of equilibrium reconstruction data. Proceed anyway?")
                msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                msg.setFont(self._font)
                retval = msg.exec_()
            if retval == QtWidgets.QMessageBox.Yes:
                fielddict = self._make_get_list()
                if fielddict is not None:
                    exnamelist = dict()
                    exnamelist["MACHINE"] = 'JET'
                    exnamelist["INTERFACE"] = self.InterfaceList.currentText().lower()
                    exnamelist["USERNAME"] = uname
                    exnamelist["PASSWORD"] = passwd
                    exnamelist["DATADICT"] = fielddict
                    exnamelist["NWINDOWS"] = tt if tt > 0 else 1
                    exnamelist["OUTPUTLEVEL"] = 0
                    exnamelist["SHOTPHASE"] = 1 if self.AcceptRiskBox.isChecked() and self.NonFlatTopBox.isChecked() else 0
                    exnamelist["SRCOPT"] = 0
                    exnamelist["SELECTEQ"] = self.EquilibriumList.currentText().lower()
                    exnamelist["ELMFILTFLAG"] = True
                    exnamelist["USERZFLAG"] = True
                    exnamelist["GPCOORDFLAG"] = True
                    exnamelist["RSHIFTFLAG"] = True
                    exnamelist["NSCALEFLAG"] = True
                    exnamelist["PURERAWFLAG"] = False
                    exnamelist["EXPFILTFLAG"] = True
                    exnamelist["EDGETEFLAG"] = True
                    exnamelist["STDMFLAG"] = False
                    exnamelist["RHOEBFLAG"] = False
                    exnamelist["USELMODEFLAG"] = False
                    exnamelist["USEECEFLAG"] = False
                    exnamelist["ABSECEFLAG"] = False
                    exnamelist["USEQEDGEFLAG"] = False
                    if self.AcceptRiskBox.isChecked():
                        if self.BasicDebugBox.isChecked():
                            exnamelist["DEBUGFLAG"] = True
                        if self.DebugEquilibriumBox.isChecked():
                            exnamelist["CDEBUG"] = True
                        exnamelist["NOOPTFLAG"] = self.UsePresetHypsBox.isChecked()
                        exnamelist["ELMFILTFLAG"] = self.UseELMFilterBox.isChecked()
                        exnamelist["USERZFLAG"] = self.UseRZRemappingBox.isChecked()
                        exnamelist["RSHIFTFLAG"] = self.UseRMajorShiftBox.isChecked()
                        exnamelist["NSCALEFLAG"] = self.UseDensityScaleBox.isChecked()
                        exnamelist["PURERAWFLAG"] = self.UsePureExpDataBox.isChecked()
                        exnamelist["EXPFILTFLAG"] = not self.UseOnlyExpFiltersBox.isChecked()
                        exnamelist["EDGETEFLAG"] = self.PasteEdgeTEBox.isChecked()
                        exnamelist["STDMFLAG"] = self.UseStdOfMeanBox.isChecked()
                        exnamelist["RHOEBFLAG"] = self.KeepRhoErrorBox.isChecked()
                        exnamelist["USELMODEFLAG"] = self.ForceLModeBox.isChecked()
                        exnamelist["USENESMOOTHFLAG"] = self.ForceSmoothNEBox.isChecked()
                        exnamelist["USEECEFLAG"] = self.UseECEDiagnosticBox.isChecked()
                        exnamelist["ABSECEFLAG"] = self.UseECEDiagnosticBox.isChecked() and self.MirrorECEDiagnosticBox.isChecked()
                        exnamelist["USEQEDGEFLAG"] = self.UseEdgeQScaleBox.isChecked()
                        if self.NEErrorMultiplierEntry.text() and self.GrabNEBox.isChecked():
                            exnamelist["NEEBMULT"] = float(self.NEErrorMultiplierEntry.text())
                        if self.TEErrorMultiplierEntry.text() and self.GrabTEBox.isChecked():
                            exnamelist["TEEBMULT"] = float(self.TEErrorMultiplierEntry.text())
#                        if self.NIErrorMultiplierEntry.text() and self.GrabNIBox.isChecked():
#                            exnamelist["NIEBMULT"] = float(self.NIErrorMultiplierEntry.text())
#                        if self.TIErrorMultiplierEntry.text() and self.GrabTIBox.isChecked():
#                            exnamelist["TIEBMULT"] = float(self.TIErrorMultiplierEntry.text())
#                        if self.NIMPErrorMultiplierEntry.text() and self.GrabNIBox.isChecked():
#                            exnamelist["NIMPEBMULT"] = float(self.NIMPErrorMultiplierEntry.text())
                        if self.TIMPErrorMultiplierEntry.text() and self.GrabTIBox.isChecked():
                            exnamelist["TIMPEBMULT"] = float(self.TIMPErrorMultiplierEntry.text())
                        if self.AFErrorMultiplierEntry.text() and self.GrabAFBox.isChecked():
                            exnamelist["AFEBMULT"] = float(self.AFErrorMultiplierEntry.text())
#                        if self.QErrorMultiplierEntry.text():
#                            exnamelist["QEBMULT"] = float(self.QErrorMultiplierEntry.text())
#                        if self.ZEFFErrorMultiplierEntry.text():
#                            exnamelist["ZEFFEBMULT"] = float(self.ZEFFErrorMultiplierEntry.text())
                    if fcustomincurrent and self.filtsrcdir != 'N/A':
                        exnamelist["CUSTOMPATH"] = self.filtsrcdir
                    try:
                        print("Data extraction started...")
                        tic = time.perf_counter()
                        exnamelist["SHOTINFO"] = "%8d %10.6f %10.6f" % (snum, t1, t2)
                        slist = EX2GK.extract_and_standardize_data(exnamelist["MACHINE"], namelist=exnamelist)
                        if isinstance(slist, list):
                            for jj in np.arange(0, len(slist)):
                                self._add_to_list(slist[jj])
                            self._select_data()
                        else:
                            raise TypeError("Unknown extraction failure within machine-specific adapter.")
                        toc = time.perf_counter()
                        print("Data extraction finished. Time elapsed: %.6f" % (toc - tic))
                    except Exception as e:
                        print(repr(e))
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setWindowTitle("Fatal Error")
                        msg.setText("Extraction procedure failed for selected time window configuration.")
                        msg.setFont(self._font)
                        msg.exec_()
            else:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setWindowTitle("Entry Error")
                msg.setText("At least one fit quantity must be selected.")
                msg.setFont(self._font)
                msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Entry Error")
            msg.setText("Unrecognized input time window data.")
            msg.setFont(self._font)
            msg.exec_()

    def _fit_data(self):
        if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady() and self.didx is not None and self.didx < self.DatabaseList.rowCount():
            self.DatabaseList.setCurrentCell(self.didx, 0)
            textn = self.ShotNumberEntry.text()
            text1 = self.TimeStartEntry.text()
            text2 = self.TimeEndEntry.text()
            snum = int(textn) if textn else -1
            t1 = float(text1) if text1 else -9999.9999
            t2 = float(text2) if text2 else 9999.9999
            retval = QtWidgets.QMessageBox.Yes
            if self.sdata["META"]["SHOT"] != snum or np.abs(self.sdata["META"]["T1"] - t1) > 1.0e-6 or np.abs(self.sdata["META"]["T2"] - t2) > 1.0e-6:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Question)
                msg.setWindowTitle("Time Window Inconsistency")
                msg.setText("Time window inputs are inconsistent with currently stored data. Continue fitting anyway?")
                msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                msg.setFont(self._font)
                retval = msg.exec_()
            if retval == QtWidgets.QMessageBox.Yes:
                exnamelist = dict()
                exnamelist["MACHINE"] = 'JET'
                exnamelist["OUTPUTLEVEL"] = 0
                exnamelist["CSOMIN"] = 0.0
                exnamelist["CSOMAX"] = 1.0
                exnamelist["CSON"] = 101
                exnamelist["LINFIFLAG"] = self.UseLinearFastIonFitBox.isChecked()
                exnamelist["LINSRCFLAG"] = self.UseLinearSourceFitBox.isChecked()
                exnamelist["QCPASS"] = 1.96
                use_rhoerrs = False
                if self.AcceptRiskBox.isChecked():
                    use_rhoerrs = self.KeepRhoErrorBox.isChecked()              # Could be used to toggle use of NIGP, but opted to never use NIGP
                    exnamelist["DEBUGFLAG"] = self.BasicDebugBox.isChecked()
                try:
                    print("Fitting procedure started...")
                    tic = time.perf_counter()
                    jj = self.CoordinateList.currentIndex()
                    exnamelist["CSO"] = self.clist[jj].lower()
                    prefixlist = self.sdata["CD"].getPrefixes()
                    exnamelist["CSOP"] = ""
                    self.sdata = EX2GK.fit_standardized_data(self.sdata, namelist=exnamelist)
                    if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata["META"]["CSO"] is not None:
                        self.DatabaseList.currentItem().set_data(self.sdata)
                        self._select_data()
                    else:
                        raise TypeError("Unknown failure inside GPR1D fitting algorithm or given settings.")
                    toc = time.perf_counter()
                    print("Fitting procedure finished. Time elapsed: %.6f" % (toc - tic))
                except Exception as e:
                    print(repr(e))
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Critical)
                    msg.setWindowTitle("Fatal Error")
                    msg.setText("Fitting procedure failed with currently stored data.")
                    msg.setFont(self._font)
                    msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Required data not extracted yet.")
            msg.setFont(self._font)
            msg.exec_()

    def _fit_all_data(self):
        numitems = self.DatabaseList.rowCount()
        for ii in np.arange(0, numitems):
            self.DatabaseList.setCurrentCell(ii, 0)
            tempitem = self.DatabaseList.currentItem()
            self.sdata = tempitem.get_data() if isinstance(tempitem, QTableWidgetTimeWindow) else None
            self.didx = self.DatabaseList.currentRow() if self.sdata is not None else None
            if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
                self.ShotNumberEntry.setText("%d" % (self.sdata["META"]["SHOT"]))
                self.TimeStartEntry.setText("%.4f" % (self.sdata["META"]["T1"]))
                self.TimeEndEntry.setText("%.4f" % (self.sdata["META"]["T2"]))
                self._fit_data()

    def _post_process_data(self):
        if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata.isFitted() and self.didx is not None and self.didx < self.DatabaseList.rowCount():
            self.DatabaseList.setCurrentCell(self.didx, 0)
            textn = self.ShotNumberEntry.text()
            text1 = self.TimeStartEntry.text()
            text2 = self.TimeEndEntry.text()
            snum = int(textn) if textn else -1
            t1 = float(text1) if text1 else -9999.9999
            t2 = float(text2) if text2 else 9999.9999
            retval = QtWidgets.QMessageBox.Yes
            if self.sdata["META"]["SHOT"] != snum or np.abs(self.sdata["META"]["T1"] - t1) > 1.0e-6 or np.abs(self.sdata["META"]["T2"] - t2) > 1.0e-6:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Question)
                msg.setWindowTitle("Time Window Inconsistency")
                msg.setText("Time window inputs are inconsistent with currently stored data. Continue post-processing anyway?")
                msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                msg.setFont(self._font)
                retval = msg.exec_()
            if retval == QtWidgets.QMessageBox.Yes and self.sdata["META"]["CODE"] is not None:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Question)
                msg.setWindowTitle("Post-processing Data Found")
                msg.setText("Currently stored data has already been post-processed according to %s. Continue post-processing anyway?" % (self.sdata["META"]["CODE"]))
                msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                msg.setFont(self._font)
                retval = msg.exec_()
            if retval == QtWidgets.QMessageBox.Yes:
                exnamelist = dict()
                exnamelist["OUTPUTLEVEL"] = 0
                exnamelist["FINALCODE"] = self.CodeAdapterList.currentText().lower()
                if exnamelist["FINALCODE"].startswith("general"):
                    exnamelist["FINALCODE"] = 'general'
                    jj = self.CoordinateList.currentIndex()
                    exnamelist["CSPROC"] = self.clist[jj].lower()
                exnamelist["FORCEPOST"] = True
                if self.AcceptRiskBox.isChecked():
                    exnamelist["DEBUGFLAG"] = self.BasicDebugBox.isChecked()
                try:
                    print("Post-processing procedure started...")
                    tic = time.perf_counter()
                    self.sdata = EX2GK.compute_code_specific_data(self.sdata, namelist=exnamelist)
                    if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata["META"]["CSO"] is not None:
                        self.DatabaseList.currentItem().set_data(self.sdata)
                        self._select_data()
                    else:
                        raise TypeError("Unknown post-processing failure within code-specific adapter.")
                    toc = time.perf_counter()
                    print("Post-processing procedure finished. Time elapsed: %.6f" % (toc - tic))
                except Exception as e:
                    print(repr(e))
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Critical)
                    msg.setWindowTitle("Fatal Error")
                    msg.setText("Post-processing procedure failed with currently stored data.")
                    msg.setFont(self._font)
                    msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Input data not fitted yet, cannot post-process.")
            msg.setFont(self._font)
            msg.exec_()

    # Need to modify this here
    def _select_data(self):
        tempitem = self.DatabaseList.currentItem()
        self.sdata = tempitem.get_data() if isinstance(tempitem, QTableWidgetTimeWindow) else None
        self.didx = self.DatabaseList.currentRow() if self.sdata is not None else None
        if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
            cdlist = self.sdata["CD"].coord_keys()
            rdlist = self.sdata["RD"].getPresentFields('list')
            ii = tempitem.get_time_window_number()
            self.ShotNumberEntry.setText("%d" % (self.sdata["META"]["SHOT"]))
            self.TimeStartEntry.setText("%.4f" % (self.sdata["META"]["T1"]))
            self.TimeEndEntry.setText("%.4f" % (self.sdata["META"]["T2"]))
            self.NTimeWindowsEntry.setText("1")
            eqdda = self.sdata["META"]["EQSRC"]
            self.EquilibriumList.setCurrentIndex(4)
            if re.match(r'^EFTP$', eqdda, flags=re.IGNORECASE):
                self.EquilibriumList.setCurrentIndex(3)
            elif re.match(r'^EFTF$', eqdda, flags=re.IGNORECASE):
                self.EquilibriumList.setCurrentIndex(2)
            elif re.match(r'^EFTM$', eqdda, flags=re.IGNORECASE):
                self.EquilibriumList.setCurrentIndex(1)
            elif re.match(r'^EHTR$', eqdda, flags=re.IGNORECASE):
                self.EquilibriumList.setCurrentIndex(0)
            self.DatabaseList.setEnabled(True)
            self.SortWindowsButton.setEnabled(True)
            self.DeleteWindowButton.setEnabled(True)
            self.GrabElectronBox.setChecked(("NE" in rdlist and self.sdata["RD"]["NE"].npoints > 0) or ("TE" in rdlist and self.sdata["RD"]["TE"].npoints > 0))
            self.GrabIonBox.setChecked(("TI" in rdlist and self.sdata["RD"]["TI"].npoints > 0) or ("TIMP" in rdlist and self.sdata["RD"]["TIMP"].npoints > 0) or ("NIMP1" in rdlist and self.sdata["RD"]["NIMP1"].npoints > 0))
            self.GrabFastIonBox.setChecked(("NFI1NBI" in rdlist and self.sdata["RD"]["NFI1NBI"].npoints > 0) or ("NFI1ICRH" in rdlist and self.sdata["RD"]["NFI1ICRH"].npoints > 0))
            self.GrabMomentumBox.setChecked("AFTOR" in rdlist and self.sdata["RD"]["AFTOR"].npoints > 0)
            self.GrabSourceBox.setChecked(("STENBI" in rdlist and self.sdata["RD"]["STENBI"].npoints > 0) or ("STEICRH" in rdlist and self.sdata["RD"]["STEICRH"].npoints > 0))
            self._reset_all_box()
            self.SaveRawDataButton.setEnabled(True)
            self._enable_fast_ions()
            self._enable_sources()
            self.CoordinateList.setEnabled(True)
            self.CoordinateList.clear()
            self.clist = []
            if "RHOTORN" in cdlist and self.sdata["CD"][""]["RHOTORN"].npoints > 0:
                self.CoordinateList.addItem("rho_tor_norm")
                self.clist.append("RHOTORN")
            if "RHOPOLN" in cdlist and self.sdata["CD"][""]["RHOPOLN"].npoints > 0:
                self.CoordinateList.addItem("rho_pol_norm")
                self.clist.append("RHOPOLN")
            if "PSITORN" in cdlist and self.sdata["CD"][""]["PSITORN"].npoints > 0:
                self.CoordinateList.addItem("psi_tor_norm")
                self.clist.append("PSITORN")
            if "PSIPOLN" in cdlist and self.sdata["CD"][""]["PSITORN"].npoints > 0:
                self.CoordinateList.addItem("psi_pol_norm")
                self.clist.append("PSIPOLN")
            if "PSITOR" in cdlist and self.sdata["CD"][""]["PSITOR"].npoints > 0:
                self.CoordinateList.addItem("psi_tor")
                self.clist.append("PSITOR")
            if "PSIPOL" in cdlist and self.sdata["CD"][""]["PSIPOL"].npoints > 0:
                self.CoordinateList.addItem("psi_pol")
                self.clist.append("PSIPOL")
            if "RMAJORO" in cdlist and self.sdata["CD"][""]["RMAJORO"].npoints > 0:
                self.CoordinateList.addItem("R_major_LFS @ Z_mag")
                self.clist.append("RMAJORO")
            if "RMAJORI" in cdlist and self.sdata["CD"][""]["RMAJORI"].npoints > 0:
                self.CoordinateList.addItem("R_major_HFS @ Z_mag")
                self.clist.append("RMAJORI")
            if "RMINORA" in cdlist and self.sdata["CD"][""]["RMINORA"].npoints > 0:
                self.CoordinateList.addItem("r_minor_averaged @ Z_mag")
                self.clist.append("RMINORA")
            self.FittingLabel.setEnabled(True)
            self.FitDataButton.setEnabled(True)
            self.FitAllDataButton.setEnabled(True)
            self.PlottingLabel.setEnabled(True)
            if "ED" in self.sdata:
                self.PlotEquilibriumButton.setEnabled(True)
            self.PlotProfileButton.setEnabled(True)
            self.PlotSourceButton.setEnabled(True)
            self.CheckingLabel.setEnabled(True)
            self.ExplorationModeButton.setEnabled(True)
            self.SavingLabel.setEnabled(True)
            self.MakeEQDSKButton.setEnabled(True)
            self.MakeMATLABButton.setEnabled(True)
            self.MakeIMASButton.setEnabled(self.imasflag)
            self.SavingAllLabel.setEnabled(True)
            self.WritePickleButton.setEnabled(True)
            self.WritePPFButton.setEnabled(self.ppfflag)
            self.WriteIMASButton.setEnabled(self.imasflag)
            if "NEEBMULT" in self.sdata["META"]:
                self.NEErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["NEEBMULT"]))
            if "TEEBMULT" in self.sdata["META"]:
                self.TEErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["TEEBMULT"]))
#            if "NIEBMULT" in self.sdata["META"]:
#                self.NIErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["NIEBMULT"]))
#            if "TIEBMULT" in self.sdata["META"]:
#                self.TIErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["TIEBMULT"]))
#            if "NIMPEBMULT" in self.sdata["META"]:
#                self.NIMPErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["NIMPEBMULT"]))
            if "TIMPEBMULT" in self.sdata["META"]:
                self.TIMPErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["TIMPEBMULT"]))
            if "AFEBMULT" in self.sdata["META"]:
                self.AFErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["AFEBMULT"]))
#            if "QEBMULT" in self.sdata["META"]:
#                self.QErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["QEBMULT"]))
#            if "ZEFFEBMULT" in self.sdata["META"]:
#                self.ZEFFErrorMultiplierEntry.setText("%.4f" % (self.sdata["META"]["ZEFFEBMULT"]))
            pdlist = self.sdata["PD"].getPresentFields('list') if "PD" in self.sdata else []
            if self.sdata["META"]["CSO"] is not None and "X" in pdlist and self.sdata["PD"]["X"].npoints > 0:
                self.UseLinearFastIonFitBox.setChecked(bool(self.sdata["FLAG"].checkFlag("LINFI")))
                self.UseLinearSourceFitBox.setChecked(bool(self.sdata["FLAG"].checkFlag("LINSRC")))
                self.CoordinateList.setCurrentIndex(0)
                jout = None
                jproc = None
                for jj in np.arange(0, len(self.clist)):
                    if self.clist[jj] == self.sdata["META"]["CSO"].upper():
                        jout = jj
                    if self.sdata["META"]["CODEBASE"] is not None and self.clist[jj] == self.sdata["META"]["CODEBASE"].upper():
                        jproc = jj
                if jproc is not None:
                    self.CoordinateList.setCurrentIndex(jproc)
                elif jout is not None:
                    self.CoordinateList.setCurrentIndex(jout)
                self.SaveFitDataButton.setEnabled(True)
                self.KeepFitGradientsBox.setEnabled(True)
                self.PlotGradientButton.setEnabled(True)
                self.PlotMiscButton.setEnabled(True)
                self.PassSigmaEntry.setEnabled(True)
                if "QCPASS" in self.sdata["META"] and self.sdata["META"]["QCPASS"] is not None:
                    self.PassSigmaEntry.setText("%.4f" % (self.sdata["META"]["QCPASS"]))
                else:
                    self.PassSigmaEntry.setText("")
                self.QualityCheckButton.setEnabled(True)
                self.ProcessingLabel.setEnabled(True)
                self.CodeAdapterList.setEnabled(True)
                self.CodeAdapterList.setCurrentIndex(0)
                self.ProcessDataButton.setEnabled(True)
                if "CODE" in self.sdata["META"] and self.sdata["META"]["CODE"] is not None:
                    self.CodeAdapterList.setCurrentIndex(0)
                    if re.match(r'^qualikiz$', self.sdata["META"]["CODE"], flags=re.IGNORECASE):
                        self.CodeAdapterList.setCurrentIndex(1)
                    self.SaveProcessDataButton.setEnabled(True)
                    self.PlotProcessButton.setEnabled(True)
                else:
                    self.SaveProcessDataButton.setEnabled(False)
                    self.PlotProcessButton.setEnabled(False)
            else:
                self.UseLinearFastIonFitBox.setChecked(False)
                self.UseLinearSourceFitBox.setChecked(True)
                self.CoordinateList.setCurrentIndex(0)
                self.SaveFitDataButton.setEnabled(False)
                self.KeepFitGradientsBox.setChecked(False)
                self.KeepFitGradientsBox.setEnabled(False)
                self.PlotGradientButton.setEnabled(False)
                self.PlotMiscButton.setEnabled(False)
                self.PassSigmaEntry.setEnabled(False)
                self.PassSigmaEntry.setText("")
                self.QualityCheckButton.setEnabled(False)
                self.ProcessingLabel.setEnabled(False)
                self.CodeAdapterList.setEnabled(False)
                self.ProcessDataButton.setEnabled(False)
                self.SaveProcessDataButton.setEnabled(False)
                self.PlotProcessButton.setEnabled(False)
            self.UsePresetHypsBox.setChecked("NOOPT" in self.sdata["FLAG"] and self.sdata["FLAG"]["NOOPT"])
            self.UseELMFilterBox.setChecked("ELMFILT" in self.sdata["FLAG"] and self.sdata["FLAG"]["ELMFILT"])
            self.UseRZRemappingBox.setChecked("USERZ" in self.sdata["FLAG"] and self.sdata["FLAG"]["USERZ"])
            self.UseRMajorShiftBox.setChecked("RSHIFT" in self.sdata["FLAG"] and self.sdata["FLAG"]["RSHIFT"])
            self.UseDensityScaleBox.setChecked(("NSCALE" in self.sdata["FLAG"] and self.sdata["FLAG"]["NSCALE"]) or ("NESCALE" in self.sdata["ZD"] and self.sdata["ZD"]["NESCALE"][""] is not None))
            self.UseEdgeQScaleBox.setChecked("USEQEDGE" in self.sdata["FLAG"] and self.sdata["FLAG"]["USEQEDGE"] and "QEDGE" in self.sdata["ZD"] and self.sdata["ZD"]["QEDGE"][""] is not None)
            self.UsePureExpDataBox.setChecked("PURERAW" in self.sdata["FLAG"] and self.sdata["FLAG"]["PURERAW"])
            self.PasteEdgeTEBox.setChecked(("EDGETE" in self.sdata["FLAG"] and self.sdata["FLAG"]["EDGETE"]) or ("EDGETE" not in self.sdata["FLAG"]))
            self.UseStdOfMeanBox.setChecked("STDMEAN" in self.sdata["FLAG"] and self.sdata["FLAG"]["STDMEAN"])
            self.KeepRhoErrorBox.setChecked("KEEPXEB" in self.sdata["FLAG"] and self.sdata["FLAG"]["KEEPXEB"])
            self.ForceLModeBox.setChecked("USELMODE" in self.sdata["FLAG"] and self.sdata["FLAG"]["USELMODE"])
            self.ForceSmoothNEBox.setChecked("USERQNE" in self.sdata["FLAG"] and self.sdata["FLAG"]["USERQNE"])
            self.UseECEDiagnosticBox.setChecked("USEECE" in self.sdata["FLAG"] and self.sdata["FLAG"]["USEECE"])
        else:
            self.DatabaseList.setEnabled(False)
            self.SortWindowsButton.setEnabled(False)
            self.DeleteWindowButton.setEnabled(False)
            self.GrabElectronBox.setChecked(False)
            self.GrabIonBox.setChecked(False)
            self.GrabFastIonBox.setChecked(False)
            self.GrabMomentumBox.setChecked(False)
            self.GrabSourceBox.setChecked(False)
            self._reset_all_box()
            self.SaveRawDataButton.setEnabled(False)
            self._enable_fast_ions()
            self._enable_sources()
            self.CoordinateList.setEnabled(False)
            self.CoordinateList.clear()
            self.clist = []
            self.FitDataButton.setEnabled(False)
            self.FitAllDataButton.setEnabled(False)
            self.CheckingLabel.setEnabled(False)
            self.ExplorationModeButton.setEnabled(False)
            self.SavingLabel.setEnabled(False)
            self.MakeEQDSKButton.setEnabled(False)
            self.MakeMATLABButton.setEnabled(False)
            self.MakeIMASButton.setEnabled(False)
            self.SavingAllLabel.setEnabled(False)
            self.WritePickleButton.setEnabled(False)
            self.WritePPFButton.setEnabled(False)
            self.WriteIMASButton.setEnabled(False)
            self.FittingLabel.setEnabled(False)
            self.UseLinearFastIonFitBox.setChecked(False)
            self.UseLinearSourceFitBox.setChecked(True)
            self.SaveFitDataButton.setEnabled(False)
            self.PlottingLabel.setEnabled(False)
            self.KeepFitGradientsBox.setChecked(False)
            self.KeepFitGradientsBox.setEnabled(False)
            self.PlotEquilibriumButton.setEnabled(False)
            self.PlotProfileButton.setEnabled(False)
            self.PlotGradientButton.setEnabled(False)
            self.PlotSourceButton.setEnabled(False)
            self.PlotMiscButton.setEnabled(False)
            self.CheckingLabel.setEnabled(False)
            self.PassSigmaEntry.setEnabled(False)
            self.PassSigmaEntry.setText("")
            self.QualityCheckButton.setEnabled(False)
            self.ProcessingLabel.setEnabled(False)
            self.CodeAdapterList.setEnabled(False)
            self.CodeAdapterList.setCurrentIndex(0)
            self.ProcessDataButton.setEnabled(False)
            self.SaveProcessDataButton.setEnabled(False)
            self.PlotProcessButton.setEnabled(False)
            self.UsePresetHypsBox.setChecked(False)
            self.UseELMFilterBox.setChecked(True)
            self.UseRZRemappingBox.setChecked(True)
            self.UseRMajorShiftBox.setChecked(True)
            self.UseDensityScaleBox.setChecked(True)
            self.UseEdgeQScaleBox.setChecked(True)
            self.UsePureExpDataBox.setChecked(False)
            self.PasteEdgeTEBox.setChecked(True)
            self.UseStdOfMeanBox.setChecked(False)
            self.KeepRhoErrorBox.setChecked(False)
            self.ForceLModeBox.setChecked(False)
            self.ForceSmoothNEBox.setChecked(False)
            self.UseECEDiagnosticBox.setChecked(False)

    def _class_converter(self, database):
        newdata = dict()
        if isinstance(database, dict) and not isinstance(database, EX2GK.classes.EX2GKTimeWindow):
            for snum in database:
                newdata[snum] = []
                for ii in np.arange(0, len(database[snum])):
                    if not isinstance(database[snum][ii], EX2GK.classes.EX2GKTimeWindow):
                        twobj = EX2GK.classes.EX2GKTimeWindow.importFromDict(database[snum][ii])
                        newdata[snum].append(copy.deepcopy(twobj))
                    else:
                        newdata[snum].append(copy.deepcopy(database[snum][ii]))
        return newdata

    def _generate_table(self, database):
        if isinstance(database, dict):
            self.DatabaseList.setEnabled(True)
            self.DatabaseList.clear()
            self.DatabaseList.setRowCount(0)
            self.DatabaseList.setHorizontalHeaderLabels(["Time Window"])
            for snum in sorted(database.keys()):
                tlist = []
                tbegarr = []
                tendarr = []
                for ii in np.arange(0, len(database[snum])):
                    idx = None
                    repflag = False
                    for jj in np.arange(0, len(tlist)):
                        if idx is None:
                            if np.abs(tbegarr[jj] - database[snum][ii]["META"]["T1"]) < 1.0e-6:
                                if np.abs(tendarr[jj] - database[snum][ii]["META"]["T2"]) < 1.0e-6:
                                    repflag = True
                                    idx = jj
                                elif tendarr[jj] < database[snum][ii]["META"]["T2"]:
                                    idx = jj
                            elif tbegarr[jj] > database[snum][ii]["META"]["T1"]:
                                idx = jj
                    if idx is None:
                        idx = len(tlist)
                    if repflag:
                        tlist[idx] = ii
                        tbegarr[idx] = database[snum][ii]["META"]["T1"]
                        tendarr[idx] = database[snum][ii]["META"]["T2"]
                    else:
                        tlist.insert(idx, ii)
                        tbegarr.insert(idx, database[snum][ii]["META"]["T1"])
                        tendarr.insert(idx, database[snum][ii]["META"]["T2"])
                for tt in np.arange(0, len(tlist)):
                    tdata = database[snum][tlist[tt]]
                    tableItem = QTableWidgetTimeWindow("%d : %8.4f -- %8.4f" % (tdata["META"]["SHOT"], tdata["META"]["T1"], tdata["META"]["T2"]), tdata, tt)
                    tidx = self.DatabaseList.rowCount()
                    self.DatabaseList.insertRow(tidx)
                    self.DatabaseList.setItem(tidx, 0, tableItem)
            tidx = self.DatabaseList.rowCount()
            if tidx > 0:
                self.DatabaseList.setCurrentCell(tidx-1, 0)

    def _delete_current_row(self):
        tidx = self.DatabaseList.currentRow()
        tdata = self.DatabaseList.currentItem()
        del tdata
        self.DatabaseList.removeRow(tidx)
        tidx = tidx - 1 if tidx > 0 else tidx
        self.DatabaseList.setCurrentCell(tidx, 0)
        self._select_data()

    def _add_to_list(self, sdata):
        if isinstance(sdata, EX2GK.classes.EX2GKTimeWindow):
            retval = QtWidgets.QMessageBox.Yes
            didx = None
            for ii in np.arange(0, self.DatabaseList.rowCount()):
                titem = self.DatabaseList.item(ii, 0)
                tdata = titem.get_data()
                if sdata["META"]["SHOT"] == tdata["META"]["SHOT"]:
                    if np.abs(sdata["META"]["T1"] - tdata["META"]["T1"]) < 1.0e-6 and np.abs(sdata["META"]["T2"] - tdata["META"]["T2"]) < 1.0e-6:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Question)
                        msg.setWindowTitle("Time Window Exists")
                        msg.setText("Extracted time window already exists in list. Overwrite?")
                        msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                        msg.setFont(self._font)
                        retval = msg.exec_()
                        didx = titem.get_time_window_number()
            if retval == QtWidgets.QMessageBox.Yes:
                if didx is None:
                    tidx = self.DatabaseList.rowCount()
                    tableItem = QTableWidgetTimeWindow("%d : %8.4f -- %8.4f" % (sdata["META"]["SHOT"], sdata["META"]["T1"], sdata["META"]["T2"]), sdata, tidx)
                    self.DatabaseList.insertRow(tidx)
                    self.DatabaseList.setItem(tidx, 0, tableItem)
                    self.DatabaseList.setCurrentCell(tidx, 0)
                else:
                    self.DatabaseList.setCurrentCell(didx, 0)
                    self.DatabaseList.currentItem().set_data(sdata)
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Input data in unrecognized format.")
            msg.setFont(self._font)
            msg.exec_()

    def _load_data(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', '', 'Pickle files (*.pkl);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename:
            dload = EX2GK.ptools.unpickle_this(filename)
            if isinstance(dload, EX2GK.classes.EX2GKTimeWindow):
                twobj = copy.deepcopy(dload)
                self._add_to_list(twobj)
                self._select_data()
            elif isinstance(dload, dict) and "META_DEVICE" in dload:
                twobj = EX2GK.classes.EX2GKTimeWindow.importFromDict(dload)
                self._add_to_list(twobj)
                self._select_data()
            elif isinstance(dload, dict):
                nload = self._class_converter(dload)
                retval = QtWidgets.QMessageBox.Yes
                retval2 = QtWidgets.QMessageBox.No
                if self.DatabaseList.rowCount() > 0:
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Question)
                    msg.setWindowTitle("Time Window List Exists")
                    msg.setText("Time window list is already populated. Discard existing list?")
                    msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                    msg.setFont(self._font)
                    retval = msg.exec_()
                if retval == QtWidgets.QMessageBox.Yes:
                    self._generate_table(nload)
                    self._select_data()
                else:
                    for snum in nload:
                        for ii in range(0, len(nload[snum])):
                            self._add_to_list(nload[snum][ii])
                    self._select_data()
            else:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setWindowTitle("Load Data Error")
                msg.setText("Unrecognizable data format in file. Loaded data was discarded.")
                msg.setFont(self._font)
                msg.exec_()

    def _plot_time_traces(self):
        self._extract_time_data()
        text1 = self.TimeStartEntry.text()
        text2 = self.TimeEndEntry.text()
        t1 = float(text1) if text1 else None
        t2 = float(text2) if text2 else None
        self.TracePlotWidget = time_trace_plotter(self.tdata, t1, t2, fontsize=int(self._font_height / 1.5))

    def _plot_equilibrium_data(self):
        self.EquilibriumPlotWidget = None
        self.EquilibriumPlotWidget = equilibrium_plotter(self.sdata, fontsize=int(self._font_height / 1.5))

    def _plot_profile_data(self):
        self.ProfilePlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.ProfilePlotWidget = profile_plotter(self.sdata, 1.0, csplot=self.clist[jj], fontsize=int(self._font_height / 1.5))

    def _plot_gradient_data(self):
        self.GradientPlotWidget = None
        jj = self.CoordinateList.currentIndex()
        usejac = not self.KeepFitGradientsBox.isChecked()
        self.GradientPlotWidget = gradient_plotter(self.sdata, 1.0, csplot=self.clist[jj], jacobflag=usejac, fontsize=int(self._font_height / 1.5))

    def _plot_source_data(self):
        self.SourcePlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.SourcePlotWidget = source_plotter(self.sdata, 1.0, csplot=self.clist[jj], fontsize=int(self._font_height / 1.5))

    def _plot_misc_data(self):
        self.MiscPlotWidget = None
        jj = self.CoordinateList.currentIndex()
        self.MiscPlotWidget = misc_plotter(self.sdata, 1.0, csplot=self.clist[jj], fontsize=int(self._font_height / 1.5))

    def _plot_processed_gradient_data(self):
        self.ProcessedGradientPlotWidget = None
        jj = self.CoordinateList.currentIndex()
        usejac = not self.KeepFitGradientsBox.isChecked()
        self.ProcessedGradientPlotWidget = gradient_plotter(self.sdata, 1.0, csplot=self.clist[jj], procflag=True, jacobflag=usejac, fontsize=int(self._font_height / 1.5))

    def _run_quality_checks(self):
        if self.sdata:
            text = self.PassSigmaEntry.text()
            sigma = float(text) if text else None
            goodflag = False
            try:
                self.sdata._performQualityChecks(sigma)
                goodflag = True
            except Exception as e:
                goodflag = False
                print(repr(e))
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setWindowTitle("Data Error")
                msg.setText("Data quality check procedure failed with currently stored data.")
                msg.setFont(self._font)
                msg.exec_()
            if goodflag:
                rstr = "Pass criterion:\t%.3f" % (self.sdata["META"]["QCPASS"])
                hstr = "Not performed"
                if self.sdata["FLAG"].checkFlag("HTEST") is not None:
                    hstr = "PASSED" if self.sdata["FLAG"].checkFlag("HTEST") else "FAILED"
                rstr = rstr + ("\n\n----- Heating Power Test:\t%s\t-----" % (hstr))
                if self.sdata["FLAG"].checkFlag("HTEST") is not None:
                    rstr = rstr + ("\n\n       Total input power:   \t\t%.3f MW" % (self.sdata["ZD"]["POWINJ"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:   \t%.3f MW" % (self.sdata["ZD"]["POWINJ"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Total lost power:   \t\t%.3f MW" % (self.sdata["ZD"]["POWLSS"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:   \t%.3f MW" % (self.sdata["ZD"]["POWLSS"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n\n       Total absorbed power:        \t%.3f MW" % ((self.sdata["ZD"]["POWINJ"][""] - self.sdata["ZD"]["POWLSS"][""]) * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:   \t%.3f MW" % ((self.sdata["ZD"]["POWINJ"]["EB"] + self.sdata["ZD"]["POWLSS"]["EB"]) * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Total integrated power:     \t%.3f MW" % (self.sdata["ZD"]["POWDEP"][""] * 1.0e-6))
                qstr = "Not performed"
                if self.sdata["FLAG"].checkFlag("QTEST") is not None:
                    qstr = "PASSED" if self.sdata["FLAG"].checkFlag("QTEST") else "FAILED"
                rstr = rstr + ("\n\n----- Equipartition Test:\t%s\t-----" % (qstr))
                if self.sdata["FLAG"].checkFlag("QTEST") is not None:
                    eqpow = self.sdata["ZD"]["EQLIMPEQ"][""]
                    qdir = "e -> i" if eqpow > 0.0 else "i -> e"
                    rstr = rstr + ("\n\n       Closest equipartition power:   \t%.3f MW" % (np.abs(self.sdata["ZD"]["EQLIMPEQ"][""] * 1.0e-6)))
                    rstr = rstr + ("\n\n         Error at criterion:   \t%.3f MW" % (np.abs(self.sdata["ZD"]["EQLIMPEQ"]["EB"] * sigma * 1.0e-6)))
                    rstr = rstr + ("\n       Associated available power:\t%.3f MW" % (np.abs(self.sdata["ZD"]["EQLIMPIN"][""] * 1.0e-6)))
                    rstr = rstr + ("\n           Error at criterion:	\t%.3f MW" % (self.sdata["ZD"]["EQLIMPIN"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Associated location (rho_tor_norm):\t%.3f" % (self.sdata["ZD"]["EQLIMX"][""]))
                    rstr = rstr + ("\n       Power transfer direction:     \t%s" % (qdir))
                estr = "Not performed"
                if self.sdata["FLAG"].checkFlag("ETEST") is not None:
                    estr = "PASSED" if self.sdata["FLAG"].checkFlag("ETEST") else "FAILED"
                rstr = rstr + ("\n\n----- Energy Content Test:\t%s\t-----" % (estr))
                if self.sdata["FLAG"].checkFlag("ETEST") is not None:
                    rstr = rstr + ("\n\n       Total measured energy:    \t%.3f MJ" % (self.sdata["ZD"]["WEXP"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:     \t%.3f MJ" % (self.sdata["ZD"]["WEXP"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n       Total integrated energy:    \t%.3f MJ" % (self.sdata["ZD"]["WTOT"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tError at criterion:     \t%.3f MJ" % (self.sdata["ZD"]["WTOT"]["EB"] * sigma * 1.0e-6))
                    rstr = rstr + ("\n\n\tMain:\t\t\t%.3f MJ" % (self.sdata["ZD"]["WEI"][""] * 1.0e-6))
                    rstr = rstr + ("\n\tImpurities:\t\t%.3f MJ" % ((self.sdata["ZD"]["WTH"][""] - self.sdata["ZD"]["WEI"][""]) * 1.0e-6))
                    rstr = rstr + ("\n\tFast ions:    \t\t%.3f MJ" % ((self.sdata["ZD"]["WTOT"][""] - self.sdata["ZD"]["WTH"][""]) * 1.0e-6))
                msg = QtWidgets.QMessageBox()
                msg.setWindowTitle("Data Quality Check Results")
                msg.setText(rstr)
                msg.setFont(self._font)
                msg.exec_()

    def _enter_exploration_mode(self):
        if isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow):
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            msg.setWindowTitle("Entering Data Exploration Mode")
            msg.setText("Starting interactive Python in the terminal which this GUI was executed from.\nSelected time window data can be found in the variable \'data\', a Python dictionary.\nAny changes to the data made in this mode are deleted upon exiting.")
            msg.setFont(self._font)
            msg.exec_()
            data = self.sdata.exportToDict()
            print("\nEntering data exploration mode...")
            print("Selected time window data can be found in the variable \'data\', which is a Python dictonary.")
            print("Any changes to the data made in this mode are deleted upon exiting.\n")
            embed()
            print("\nExiting data exploration mode...")
            print("Control returned to GUI.\n")
            del data
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Unrecognized data format in storage, unable to enter data exploration mode.")
            msg.setFont(self._font)
            msg.exec_()

    def _save_raw_data(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename and isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
            numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
            numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
            jj = self.CoordinateList.currentIndex()
            ocs = self.clist[jj]
            ocp = self.sdata["META"]["CSOP"] if "CSOP" in self.sdata["META"] and self.sdata["META"]["CSOP"] is not None else None
            xlims = (0.0, 1.0)
            fxlim = True
            if ocp+ocs not in self.sdata["CD"].coord_keys():
                ocs = self.sdata["CD"][ocp].base
            dlist = self.sdata["RD"].getPresentFields('list')
            plist = []
            qlist = ["NE", "TE", "NI", "TI", "NIMP", "TIMP", "AFTOR", "UTOR", "Q", "QC"]
            slist = ["NBI", "ICRH", "ECRH", "LH", "OHM"]
            sqlist = ["NFI", "WFI", "SN", "STE", "STI", "SP", "J"]
            for qq in np.arange(0, len(qlist)):
                if qlist[qq] in dlist:
                    plist.append(qlist[qq])
                if qlist[qq] in ["NI", "TI"]:
                    for nn in np.arange(0, numion):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
                elif qlist[qq] in ["NIMP", "TIMP"]:
                    for nn in np.arange(0, numimp):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
            for ss in np.arange(0, len(slist)):
                for qq in np.arange(0, len(sqlist)):
                    if sqlist[qq]+slist[ss] in dlist:
                        plist.append(sqlist[qq]+slist[ss])
            with open(filename, 'w') as ff:
                if self.sdata:
                    ff.write("### EX2GK - Raw Data Save File ###\n")
                    ff.write("\n")
                    ff.write("START OF HEADER\n")
                    ff.write("\n")
                    ff.write("            Shot Number: %20d\n" % (self.sdata["META"]["SHOT"]))
                    ff.write("      Radial Coordinate: %20s\n" % (ocs))
                    ff.write("      Time Window Start: %18.6f s\n" % (self.sdata["META"]["T1"]))
                    ff.write("        Time Window End: %18.6f s\n" % (self.sdata["META"]["T2"]))
                    ff.write("\n")
                    ff.write("END OF HEADER\n")
                    ff.write("\n")
                for kk in np.arange(0, len(plist)):
                    qtag = plist[kk]
                    ics = self.sdata["RD"][qtag].coordinate if self.sdata["RD"][qtag].coordinate is not None else ocs
                    icp = self.sdata["RD"][qtag].coord_prefix if self.sdata["RD"][qtag].coordinate is not None else ""
                    cocp = icp if ocp is None else ocp
                    xraw = self.sdata["RD"][qtag]["X"].flatten()
                    xeraw = self.sdata["RD"][qtag]["XEB"].flatten()
                    if ics != ocs:
                        (xraw, csj, txeraw) = self.sdata["CD"].convert(xraw, ics, ocs, icp, cocp)
                        xeraw = np.sqrt(np.power(xeraw, 2.0) + np.power(txeraw, 2.0))
                    yraw = self.sdata["RD"][qtag][""].flatten()
                    yeraw = self.sdata["RD"][qtag]["EB"].flatten()
                    ydiag = self.sdata["RD"][qtag]["MAP"].flatten()
                    ff.write("%15s%20s%20s%20s%15s" % (ocs, qtag+" Raw", "Err. "+qtag, "Err. "+ocs, "Origin"))
                    zz = re.match(r'^[NT]I(MP)?([0-9]*)$', qtag, flags=re.IGNORECASE)
                    if zz and zz.group(2):
                        imptag = zz.group(1).upper()
                        ztag = zz.group(2)
                        ff.write("  Mat.: %s\n" % (self.sdata["META"]["MATI"+imptag+ztag]))
                    elif zz:
                        ff.write("  Mat.: %s\n" % ("Combined"))
                    else:
                        ff.write("\n")
                    for ii in np.arange(0, xraw.size):
                        ddiag = self.sdata["RD"][qtag]["SRC"][int(ydiag[ii])]
                        ff.write("%15.4f%20.6e%20.6e%20.4f%15s\n" % (xraw[ii], yraw[ii], yeraw[ii], xeraw[ii], ddiag))
                    ff.write("\n")
            print("Raw data written into %s." % (filename))

    def _save_fit_data(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename and isinstance(self.sdata, EX2GK.classes.EX2GKTimeWindow) and self.sdata.isReady():
            numion = self.sdata["META"]["NUMI"] if "NUMI" in self.sdata["META"] else 0
            numimp = self.sdata["META"]["NUMIMP"] if "NUMIMP" in self.sdata["META"] else 0
            numz = self.sdata["META"]["NUMZ"] if "NUMZ" in self.sdata["META"] else 0
            jj = self.CoordinateList.currentIndex()
            ocs = self.clist[jj]
            ocp = self.sdata["META"]["CSOP"]
            xlims = (0.0, 1.0)
            fxlim = True
            if ocp+ocs not in self.sdata["CD"].coord_keys():
                ocs = self.sdata["CD"][ocp].base
            ics = self.sdata["META"]["CSO"]
            icp = self.sdata["META"]["CSOP"]
            dlist = self.sdata["PD"].getPresentFields('list')
            plist = []
            qlist = ["NE", "TE", "NI", "TI", "NIMP", "TIMP", "NZ", "TZ", "AFTOR", "UTOR", "Q", "QC", "ZEFF"]
            slist = ["NBI", "ICRH", "ECRH", "LH", "OHM"]
            sqlist = ["NFI", "WFI", "SN", "STE", "STI", "SP", "J"]
            for qq in np.arange(0, len(qlist)):
                if qlist[qq] in dlist:
                    plist.append(qlist[qq])
                if qlist[qq] in ["NI", "TI"]:
                    for nn in np.arange(0, numion):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
                elif qlist[qq] in ["NIMP", "TIMP"]:
                    for nn in np.arange(0, numimp):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
                elif qlist[qq] in ["NZ", "TZ"]:
                    for nn in np.arange(0, 2):
                        itag = "%d" % (nn+1)
                        if qlist[qq]+itag in dlist:
                            plist.append(qlist[qq]+itag)
            for ss in np.arange(0, len(slist)):
                for qq in np.arange(0, len(sqlist)):
                    if sqlist[qq]+slist[ss] in dlist:
                        plist.append(sqlist[qq]+slist[ss])
            with open(filename, 'w') as ff:
                dcs = ocs if ocs != ics and not self.KeepFitGradientsBox.isChecked() else ics
                if self.sdata:
                    ff.write("### EX2GK - Fit Data Save File ###\n")
                    ff.write("\n")
                    ff.write("START OF HEADER\n")
                    ff.write("\n")
                    ff.write("            Shot Number: %20d\n" % (self.sdata["META"]["SHOT"]))
                    ff.write("      Radial Coordinate: %20s\n" % (ocs))
                    ff.write("  Derivative Coordinate: %20s\n" % (dcs))
                    ff.write("      Time Window Start: %18.6f s\n" % (self.sdata["META"]["T1"]))
                    ff.write("        Time Window End: %18.6f s\n" % (self.sdata["META"]["T2"]))
                    ff.write("\n")
                    ff.write("END OF HEADER\n")
                    ff.write("\n")
                for kk in np.arange(0, len(plist)):
                    qtag = plist[kk]
                    xfit = self.sdata["PD"]["X"][""].flatten()
                    jfit = np.ones(xfit.shape)
                    if ocs != ics:
                        (xfit, dj, de) = self.sdata["CD"].convert(self.sdata["PD"]["X"][""].flatten(), ics, ocs, icp, ocp)
                        if not self.KeepFitGradientsBox.isChecked():
                            jfit = dj
                    yfit = self.sdata["PD"][qtag][""].flatten()
                    yefit = self.sdata["PD"][qtag]["EB"].flatten()
                    dyfit = self.sdata["PD"][qtag]["GRAD"].flatten() * jfit if "GRAD" in self.sdata["PD"][qtag] else np.full(yfit.shape, np.NaN)
                    dyefit = self.sdata["PD"][qtag]["GRADEB"].flatten() * jfit if "GRADEB" in self.sdata["PD"][qtag] else np.full(yfit.shape, np.NaN)
                    ff.write("%15s%20s%20s%20s%20s" % (ocs, qtag+" Fit", "Err. "+qtag+" Fit", "D"+qtag+" Fit", "Err. D"+qtag+" Fit"))
                    zz = re.match(r'^[NT](I|IMP|Z)([0-9]*)$', qtag, flags=re.IGNORECASE)
                    if zz and zz.group(2):
                        stag = zz.group(1).upper()
                        ztag = zz.group(2)
                        ff.write("  Mat.: %s\n" % (self.sdata["META"]["MAT"+stag+ztag]))
                    elif zz:
                        ff.write("  Mat.: %s\n" % ("Combined"))
                    else:
                        ff.write("\n")
                    for ii in np.arange(0, xfit.size):
                        ff.write("%15.4f%20.6e%20.6e%20.6e%20.6e\n" % (xfit[ii], yfit[ii], yefit[ii], dyfit[ii], dyefit[ii]))
                    ff.write("\n")
            print("Fit data written into %s." % (filename))

    def _save_post_processed_data(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Text files (*.txt);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename and self.sdata:
            pddict = self.sdata["PD"].getPresentFields('dict')
            if "CODE" in self.sdata["META"] and self.sdata["META"]["CODE"] is not None and "OUT" in pddict:
                jj = self.CoordinateList.currentIndex()
                ocs = self.clist[jj]
                ocp = self.sdata["META"]["CSOP"]
                xlims = (0.0, 1.0)
                fxlim = True
                if ocp+ocs not in self.sdata["CD"].coord_keys():
                    ocs = self.sdata["CD"][ocp].base
                ics = self.sdata["META"]["CSO"]
                ccs = self.sdata["META"]["CODEBASE"]
                if ccs in pddict["OUT"]:
                    ccs = "OUT"+ccs
                elif ocp+ccs not in self.sdata["CD"].coord_keys():
                    ccs = ics
                ctag = self.sdata["META"]["CODETAG"]
                ctag = ctag + "_"
                plist = []
                qlist = pddict["OUT"]
                for qq in np.arange(0, len(qlist)):
                    if qlist[qq].startswith(ctag):
                        plist.append(qlist[qq])
                plist = sorted(plist)
                with open(filename, 'w') as ff:
                    if self.sdata:
                        ff.write("### EX2GK - Processed Data Save File ###\n")
                        ff.write("\n")
                        ff.write("START OF HEADER\n")
                        ff.write("\n")
                        ff.write("            Shot Number: %20d\n" % (self.sdata["META"]["SHOT"]))
                        ff.write("      Radial Coordinate: %20s\n" % (ocs))
                        ff.write("  Derivative Coordinate: %20s\n" % (ccs))
                        ff.write("      Time Window Start: %18.6f s\n" % (self.sdata["META"]["T1"]))
                        ff.write("        Time Window End: %18.6f s\n" % (self.sdata["META"]["T2"]))
                        ff.write("            Target Code: %20s\n" % (self.sdata["META"]["CODE"]))
                        ff.write("\n")
                        ff.write("END OF HEADER\n")
                        ff.write("\n")
                    for kk in np.arange(0, len(plist)):
                        qtag = plist[kk]
                        xfit = self.sdata["PD"]["X"][""].flatten()
                        if ocs != ics:
                            (xfit, dj, de) = self.sdata["CD"].convert(xfit, ics, ocs, ocp, ocp)
                        xprc = self.sdata["PD"]["X"][""].flatten()
                        if ccs.startswith("OUT") and ccs[3:] in pddict["OUT"]:
                            xprc = self.sdata["PD"][ccs][""].flatten()
                        elif ccs != ics:
                            (xprc, dj, de) = self.sdata["CD"].convert(xprc, ics, ccs, ocp, ocp)
                        yprc = self.sdata["PD"]["OUT"+qtag][""].flatten()
                        yeprc = self.sdata["PD"]["OUT"+qtag]["EB"].flatten()
                        ff.write("%15s%15s%40s%40s\n" % (ocs, ccs, qtag+" Proc.", "Err. "+qtag+" Proc."))
                        for ii in np.arange(0, xprc.size):
                            ff.write("%15.4f%15.4f%40.6e%40.6e\n" % (xfit[ii], xprc[ii], yprc[ii], yeprc[ii]))
                        ff.write("\n")
                print("Post-processed data written into %s." % (filename))

    def _generate_eqdsk(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'EQDSK files (*.eqdsk);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename and self.sdata:
            self.sdata.generateEQDSKFile(filename)

    def _generate_matfile(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'MAT files (*.mat);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename and self.sdata:
            self.sdata.exportToMATLAB(filename, include_equilibrium=True, include_raw_data=True, include_fit_settings=False, include_flags=True, include_descriptions=True)

    def _generate_imas(self):
        #filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'All files (*)')
        filename = QtWidgets.QFileDialog.getExistingDirectory(self, 'Save In...', '')
        if fqt5 and isinstance(filename, (list, tuple)):
            extension = filename[1]
            filename = filename[0]
        if filename and self.sdata:
            location = filename
            dbname = None
            #mm = re.search(r'^(.*)/([^/]*)$', filename)
            #if mm and mm.group(2):
            #    location = mm.group(1)
            #    dbname = mm.group(2)
            backend = "hdf5"
            use_imp = False
            self.sdata.exportToIMAS(self._user, dbname=dbname, run_number=1, path=location, backend=backend, use_preset_impurities=use_imp)

    def _make_database(self, use_class=True, export_to_dict=False):
        database = None
        for ii in np.arange(0, self.DatabaseList.rowCount()):
            sdata = self.DatabaseList.item(ii, 0).get_data()
            if use_class:
                if database is None:
                    database = EX2GK.classes.EX2GKShotCollection()
                database.mergeTimeWindow(sdata)
            else:
                if database is None:
                    database = {}
                snum = sdata["META"]["SHOT"]
                if snum not in database:
                    database[snum] = []
                tdata = sdata.exportToDict() if export_to_dict else copy.deepcopy(sdata)
                database[snum].append(tdata)
        return database

    def _save_database_to_pickle(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'Pickle files (*.pkl);;All files (*)')
        if fqt5:
            extension = filename[1]
            filename = filename[0]
        if filename:
            database = self._make_database(use_class=False, export_to_dict=True)
            EX2GK.ptools.pickle_this(database, filename, '/')
            if not filename.endswith(".pkl"):
                filename = filename + ".pkl"
            print("Database saved in %s." % (filename))

    def _save_database_to_ppf(self):
        dda = "PRFL"
#        vals = ["NE", "TE", "TI1", "AFTOR", "Q", "ZEFF", "NFI", "WFI"]
        vals = ["NE", "TE", "TI1", "AFTOR", "Q", "ZEFF"]
        wdata = dict()
        rdata = dict()
        fdata = False
        frdata = False
        database = self._make_database(use_class=False, export_to_dict=False)
#   NIMP implementation not yet ready, requires some intelligent way to ensure identical species from different time windows get put together properly
#        num_impurities = database["NUMIMP"] if "NUMIMP" in database and database["NUMIMP"] is not None else 0
#        for ss in np.arange(0, num_impurities):
#            vals.append("NIMP%d" % (ss+1))
        for snum in sorted(database.keys()):
            wthere = dict()
            wlist = dict()
            for val in vals:
                wthere[val] = False
                wlist[val] = []
            wmlist = []
            for val in vals:
                for ii in np.arange(0, len(database[snum])):
                    if "PD" in database[snum][ii]:
                        dlist = database[snum][ii]["PD"].getPresentFields('list')
                        if val in dlist:
                            wthere[val] = True
                        else:
                            wlist[val].append(ii)
                    else:
                        print("Fit data not found for time slice [%.5f, %.5f] from shot #%8d.")
                        wlist[val].append(ii)
            nvals = []
            for nval in vals:
                if len(wlist[nval]) < len(database[snum]):
                    nvals.append(nval)
            for val in nvals:
                if wthere[val]:
                    for idx in wlist[val]:
                        if idx not in wmlist:
                            wmlist.append(idx)
            if snum not in wdata:
                wdata[snum] = dict()
            if snum not in rdata:
                rdata[snum] = dict()
            xdatas = None
            xdatap = None
            xdatar = None
            jdatas = None
            jdatap = None
            jdatar = None
            wcs = None
            wcp = None
            for val in nvals:
                ydata = None
                edata = None
                ddata = None
                gdata = None
                tdata = np.array([])
                for ii in np.arange(0, len(database[snum])):
                    if ii not in wmlist:
                        sdata = database[snum][ii]
                        if xdatas is None or xdatap is None or xdatar is None:
                            wcs = sdata["META"]["CSO"]
                            wcp = sdata["META"]["CSOP"]
                            tempx = sdata["PD"]["X"][""]
                            (xdatas, jdatas, ed) = sdata["CD"].convert(tempx, wcs, "RHOTORN", wcp, wcp)
                            (xdatap, jdatap, ed) = sdata["CD"].convert(tempx, wcs, "PSIPOLN", wcp, wcp)
                            (xdatar, jdatar, ed) = sdata["CD"].convert(tempx, wcs, "RMAJORO", wcp, wcp)
                        tval = np.mean([sdata["META"]["T1"], sdata["META"]["T2"]])
                        tv = np.where(tdata > tval)[0]
                        if tv.size > 0 and tdata.size > 0:
                            tdata = np.insert(tdata, tv[0], tval)
                            ydata = np.insert(ydata, tv[0], sdata["PD"][val][""], axis=0)
                            edata = np.insert(edata, tv[0], sdata["PD"][val]["EB"], axis=0)
                            ddata = np.insert(ddata, tv[0], sdata["PD"][val]["GRAD"], axis=0)
                            gdata = np.insert(gdata, tv[0], sdata["PD"][val]["GRADEB"], axis=0)
                        else:
                            tdata = np.hstack((tdata, tval))
                            ydata = np.atleast_2d(sdata["PD"][val][""]) if ydata is None else np.vstack((ydata, sdata["PD"][val][""]))
                            edata = np.atleast_2d(sdata["PD"][val]["EB"]) if edata is None else np.vstack((edata, sdata["PD"][val]["EB"]))
                            ddata = np.atleast_2d(sdata["PD"][val]["GRAD"]) if ddata is None else np.vstack((ddata, sdata["PD"][val]["GRAD"]))
                            gdata = np.atleast_2d(sdata["PD"][val]["GRADEB"]) if gdata is None else np.vstack((gdata, sdata["PD"][val]["GRADEB"]))
                wdata[snum][val] = copy.deepcopy(ydata)
                wdata[snum][val+"EB"] = copy.deepcopy(edata) if ydata is not None else None
                wdata[snum]["D"+val] = copy.deepcopy(ddata) if ydata is not None else None
                wdata[snum]["D"+val+"EB"] = copy.deepcopy(gdata) if ydata is not None and ddata is not None else None
                wdata[snum]["X"+val] = copy.deepcopy(xdatas) if ydata is not None else None
                wdata[snum]["T"+val] = copy.deepcopy(tdata) if ydata is not None else None
                if ydata is not None:
                    fdata = True
            wdata[snum]["CS"] = wcs if xdatas is not None else None
            wdata[snum]["CSP"] = wcp if xdatas is not None else None
            wdata[snum]["SPNT"] = copy.deepcopy(xdatas)
            wdata[snum]["PSIN"] = copy.deepcopy(xdatap)
            wdata[snum]["RLFS"] = copy.deepcopy(xdatar)
            wdata[snum]["JSPNT"] = copy.deepcopy(jdatas)
            wdata[snum]["JPSIN"] = copy.deepcopy(jdatap)
            wdata[snum]["JRLFS"] = copy.deepcopy(jdatar)
            rcs = None
            rcp = None
            if len(database[snum]) == 1:
                for val in nvals:
                    rtag = val if not re.match(r'^TI1$', val) else "TIMP"
                    rtdata = np.array([])
                    rxdata = None
                    rydata = None
                    redata = None
                    rxdatas = None
                    rxdatap = None
                    rxdatar = None
                    for ii in np.arange(0, len(database[snum])):
                        sdata = database[snum][ii]
                        rdlist = sdata["RD"].getPresentFields('list')
                        if rtag in rdlist and ii not in wmlist:
                            if rcs is None:
                                rcs = sdata["RD"][rtag].coordinate if sdata["RD"][rtag].coordinate is not None else "RHOTORN"
                                rcp = sdata["RD"][rtag].coord_prefix if sdata["RD"][rtag].coordinate is not None else ""
                            tval = np.mean([sdata["META"]["T1"], sdata["META"]["T2"]])
                            rtdata = np.hstack((rtdata, tval))
                            rxdata = np.atleast_2d(sdata["RD"][rtag]["X"]) # if rxdata is None else np.vstack((rxdata, sdata["RD"][rtag]["X"]))
                            rydata = np.atleast_2d(sdata["RD"][rtag][""]) # if rydata is None else np.vstack((rydata, sdata["RD"][rtag][""]))
                            redata = np.atleast_2d(sdata["RD"][rtag]["EB"]) # if redata is None else np.vstack((redata, sdata["RD"][rtag]["EB"]))
                            (rxdatas, jd, ed) = sdata["CD"].convert(rxdata, rcs, "RHOTORN", rcp, rcp)
                            (rxdatap, jd, ed) = sdata["CD"].convert(rxdata, rcs, "PSIPOLN", rcp, rcp)
                            (rxdatar, jd, ed) = sdata["CD"].convert(rxdata, rcs, "RMAJORO", rcp, rcp)
                    rdata[snum][val] = copy.deepcopy(rydata)
                    rdata[snum][val+"EB"] = copy.deepcopy(redata) if rydata is not None else None
                    rdata[snum]["X"+val] = copy.deepcopy(rxdata) if rydata is not None else None
                    rdata[snum]["T"+val] = copy.deepcopy(rtdata) if rydata is not None else None
                    rdata[snum][val+"SPNT"] = copy.deepcopy(rxdatas)
                    rdata[snum][val+"PSIN"] = copy.deepcopy(rxdatap)
                    rdata[snum][val+"RLFS"] = copy.deepcopy(rxdatar)
                    if ydata is not None:
                        frdata = True
                xrdata = rdata[snum]["X"+val].flatten() if rdata[snum]["X"+val] is not None else None
                rdata[snum]["CS"] = rcs if xrdata is not None else None
                rdata[snum]["CSP"] = rcp if xrdata is not None else None
            else:
                print("Raw data cannot be written to PPF for discharges which have more than one time window in the list")

        if fdata:
            uid = pwd.getpwuid(os.getuid())[0]
            for snum in sorted(wdata.keys()):
                pulse = int(snum)
                print("User ID:", uid, "   ", "Pulse No.:", pulse)
                ppf.ppfuid(uid, "W")
                time, date, ier = ppf.pdstd(pulse)
                datestring = datetime.datetime.now().strftime("%B %d, %Y - %X %z")
                ier = ppf.ppfopn(pulse, date, time, "Fit profile from EX2GK: "+datestring)
                print("PPF Open:", ier)

                ocs = "none"
                if re.match("RHOTORN?", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "rho_tor_norm"
                elif re.match("RHOPOLN?", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "rho_pol_norm"
                elif re.match("PSITORN", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "psi_tor_norm"
                elif re.match("PSIPOLN", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "psi_pol_norm"
                elif re.match("PSITOR", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "psi_tor"
                elif re.match("PSIPOL", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "psi_pol"
                elif re.match("RMAJORO", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "R_lfs"
                elif re.match("RMAJORI", wdata[snum]["CS"], flags=re.IGNORECASE):
                    ocs = "R_hfs"

                fwritten = False
                for val in vals:
                    dtyp = None
                    unit = None
                    if re.match(r'^NE$', val):
                        dtyp = "NE"
                        unit = "m**-3"
                    if re.match(r'^TE$', val):
                        dtyp = "TE"
                        unit = "eV"
                    if re.match(r'^TI1$', val):
                        dtyp = "TI"
                        unit = "eV"
#                    if re.match(r'^NIMP[0-9]*$', val):
#                        mm = re.search(r'^NIMP([0-9]+)$', val)
#                        ntag = mm.group(1) if mm else ""
#                        dtyp = "NX"+ntag
#                        unit = "m**-3"
                    if re.match(r'^AFTOR$', val):
                        dtyp = "AF"
                        unit = "rad/s"
                    if re.match(r'^Q$', val):
                        dtyp = "Q"
                        unit = ""
#                    if re.match(r'^NFI$', val):
#                        dtyp = "NFI"
#                        unit = "m**-3"
#                    if re.match(r'^WFI$', val):
#                        dtyp = "WFI"
#                        unit = "J m**-3"
                    if re.match(r'^ZEFF$', val):
                        dtyp = "Z"
                        unit = ""

                    if val in wdata[snum] and wdata[snum][val] is not None:
                        dtypi = dtyp if not re.match(r'^Z$', dtyp) else "ZEFF"
                        indat = wdata[snum][val].copy()
                        irdat = ppf.ppfwri_irdat(wdata[snum]["X"+val].size, wdata[snum]["T"+val].size, -1, -1, 0, 0)
                        ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR "+val+" fit")
                        iwdat, ier = ppf.ppfwri(pulse, dda, dtypi, irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                        print(dtypi+" PPF Write:", ier)
                        if ier == 0:
                            fwritten = True

                        if wdata[snum][val+"EB"] is not None:
                            indat = wdata[snum][val] - wdata[snum][val+"EB"]
                            ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR "+val+" fit lower - 1 sigma")
                            iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"LO", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                            print(dtyp+"LO PPF Write:", ier)

                            indat = wdata[snum][val] + wdata[snum][val+"EB"]
                            ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR "+val+" fit upper - 1 sigma")
                            iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"HI", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                            print(dtyp+"HI PPF Write:", ier)

                            indat = wdata[snum][val+"EB"].copy()
                            ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR "+val+" fit uncertainty - 1 sigma")
                            iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"EB", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                            print(dtyp+"EB PPF Write:", ier)

                        if wdata[snum]["D"+val] is not None:
                            indat = wdata[snum]["D"+val] * wdata[snum]["JSPNT"]
                            ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR d"+val+"/drho_tor fit")
                            iwdat, ier = ppf.ppfwri(pulse, dda, "D"+dtyp, irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                            print("D"+dtyp+" PPF Write:", ier)

                            indat = wdata[snum]["D"+val] * wdata[snum]["JPSIN"]
                            ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR d"+val+"/dpsi_pol_norm fit")
                            iwdat, ier = ppf.ppfwri(pulse, dda, "D"+dtyp+"P", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                            print("D"+dtyp+"P PPF Write:", ier)

                            indat = wdata[snum]["D"+val] * wdata[snum]["JRLFS"]
                            ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR d"+val+"/dR_lfs fit")
                            iwdat, ier = ppf.ppfwri(pulse, dda, "D"+dtyp+"R", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                            print("D"+dtyp+"R PPF Write:", ier)

                            if wdata[snum]["D"+val+"EB"] is not None:
                                indat = (wdata[snum]["D"+val] - wdata[snum]["D"+val+"EB"]) * wdata[snum]["JSPNT"]
                                ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR d"+val+"/drho_tor fit lower - 1 sigma")
                                iwdat, ier = ppf.ppfwri(pulse, dda, "D"+dtyp+"L", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                                print("D"+dtyp+"L PPF Write:", ier)

                                indat = (wdata[snum]["D"+val] + wdata[snum]["D"+val+"EB"]) * wdata[snum]["JSPNT"]
                                ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR d"+val+"/drho_tor fit upper - 1 sigma")
                                iwdat, ier = ppf.ppfwri(pulse, dda, "D"+dtyp+"H", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                                print("D"+dtyp+"H PPF Write:", ier)

                                indat = wdata[snum]["D"+val+"EB"] * wdata[snum]["JSPNT"]
                                ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "GPR d"+val+"/drho_tor fit uncertainty - 1 sigma")
                                iwdat, ier = ppf.ppfwri(pulse, dda, "D"+dtyp+"E", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                                print("D"+dtyp+"E PPF Write:", ier)

                        if frdata:

                            if rdata[snum][val] is not None:
                                indat = rdata[snum][val].copy()
                                irdat = ppf.ppfwri_irdat(rdata[snum]["X"+val].size, rdata[snum]["T"+val].size, -1, -1, 0, 0)
                                ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "Raw "+val+" used in GPR")
                                iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"R", irdat, ihdat, indat, rdata[snum]["X"+val], rdata[snum]["T"+val])
                                print(dtyp+"R PPF Write:", ier)
                                if ier == 0:
                                    fwritten = True

                                if rdata[snum][val+"EB"] is not None:
                                    indat = rdata[snum][val] - rdata[snum][val+"EB"]
                                    ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "Raw "+val+" lower used in GPR - 1 sigma")
                                    iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"RL", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                                    print(dtyp+"RL PPF Write:", ier)

                                    indat = rdata[snum][val] + rdata[snum][val+"EB"]
                                    ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "Raw "+val+" upper used in GPR - 1 sigma")
                                    iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"RH", irdat, ihdat, indat, wdata[snum]["X"+val], wdata[snum]["T"+val])
                                    print(dtyp+"RH PPF Write:", ier)

                                    indat = rdata[snum][val+"EB"].copy()
                                    irdat = ppf.ppfwri_irdat(rdata[snum]["X"+val].size, rdata[snum]["T"+val].size, -1, -1, 0, 0)
                                    ihdat = ppf.ppfwri_ihdat(unit, ocs, "s", "f", "f", "f", "Raw "+val+" uncertainty used in GPR - 1 sigma")
                                    iwdat, ier = ppf.ppfwri(pulse, dda, dtyp+"RE", irdat, ihdat, indat, rdata[snum]["X"+val], rdata[snum]["T"+val])
                                    print(dtyp+"RE PPF Write:", ier)

                                if rdata[snum][val+"SPNT"] is not None:
                                    iunit = ""
                                    xcs = "rho_tor"
                                    indat = rdata[snum][val+"SPNT"].copy()
                                    irdat = ppf.ppfwri_irdat(rdata[snum]["X"+val].size, rdata[snum]["T"+val].size, -1, -1, 0, 0)
                                    ihdat = ppf.ppfwri_ihdat(iunit, xcs, "s", "f", "f", "f", xcs+" (SPNT) for "+dtyp+"R")
                                    iwdat, ier = ppf.ppfwri(pulse, dda, "S"+dtyp+"R", irdat, ihdat, indat, rdata[snum]["X"+val], rdata[snum]["T"+val])
                                    print("S"+dtyp+"R PPF Write:", ier)
                                if rdata[snum][val+"PSIN"] is not None:
                                    iunit = ""
                                    xcs = "psi_pol_norm"
                                    indat = rdata[snum][val+"PSIN"].copy()
                                    irdat = ppf.ppfwri_irdat(rdata[snum]["X"+val].size, rdata[snum]["T"+val].size, -1, -1, 0, 0)
                                    ihdat = ppf.ppfwri_ihdat(iunit, xcs, "s", "f", "f", "f", xcs+" (PSIN) for "+dtyp+"R")
                                    iwdat, ier = ppf.ppfwri(pulse, dda, "P"+dtyp+"R", irdat, ihdat, indat, rdata[snum]["X"+val], rdata[snum]["T"+val])
                                    print("P"+dtyp+"R PPF Write:", ier)
                                if rdata[snum][val+"RLFS"] is not None:
                                    iunit = "m"
                                    xcs = "R_lfs"
                                    indat = rdata[snum][val+"RLFS"].copy()
                                    irdat = ppf.ppfwri_irdat(rdata[snum]["X"+val].size, rdata[snum]["T"+val].size, -1, -1, 0, 0)
                                    ihdat = ppf.ppfwri_ihdat(iunit, xcs, "s", "f", "f", "f", xcs+" (RLFS) for "+dtyp+"R")
                                    iwdat, ier = ppf.ppfwri(pulse, dda, "R"+dtyp+"R", irdat, ihdat, indat, rdata[snum]["X"+val], rdata[snum]["T"+val])
                                    print("R"+dtyp+"R PPF Write:", ier)

                if fwritten:
                    if "SPNT" in wdata[snum] and wdata[snum]["SPNT"] is not None:
                        dtyp = "SPNT"
                        xcs = "rho_tor_norm"
                        unit = ""
                        indat = wdata[snum]["SPNT"]
                        irdat = ppf.ppfwri_irdat(indat.size, 1, -1, -1, 0, 0)
                        ihdat = ppf.ppfwri_ihdat(unit, xcs, "s", "f", "f", "f", xcs)
                        iwdat, ier = ppf.ppfwri(pulse, dda, dtyp, irdat, ihdat, indat, indat, [0.0])
                        print(dtyp+" PPF Write:", ier)
                    if "PSIN" in wdata[snum] and wdata[snum]["PSIN"] is not None:
                        dtyp = "PSIN"
                        xcs = "psi_pol_norm"
                        unit = ""
                        indat = wdata[snum]["PSIN"]
                        irdat = ppf.ppfwri_irdat(indat.size, 1, -1, -1, 0, 0)
                        ihdat = ppf.ppfwri_ihdat(unit, xcs, "s", "f", "f", "f", xcs)
                        iwdat, ier = ppf.ppfwri(pulse, dda, dtyp, irdat, ihdat, indat, indat, [0.0])
                        print(dtyp+" PPF Write:", ier)
                    if "RLFS" in wdata[snum] and wdata[snum]["RLFS"] is not None:
                        dtyp = "RLFS"
                        xcs = "R_lfs"
                        unit = "m"
                        indat = wdata[snum]["RLFS"]
                        irdat = ppf.ppfwri_irdat(indat.size, 1, -1, -1, 0, 0)
                        ihdat = ppf.ppfwri_ihdat(unit, xcs, "s", "f", "f", "f", xcs)
                        iwdat, ier = ppf.ppfwri(pulse, dda, dtyp, irdat, ihdat, indat, indat, [0.0])
                        print(dtyp+" PPF Write:", ier)

                seq, ier = ppf.ppfclo(pulse, "EX2GK", 1)
                print("PPF Sequence:", seq)

                if ier == 0 and seq >= 0:
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Information)
                    msg.setWindowTitle("PPF Write Successful")
                    msg.setText("Data has been written to:\nshot = %6d, uid = %15s, seq = %5d" % (pulse, uid, seq))
                    msg.setFont(self._font)
                    msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Data Error")
            msg.setText("Extracted data not found. PPF writing aborted.")
            msg.setFont(self._font)
            msg.exec_()

    def _save_database_to_imas(self):
        #filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save As...', '', 'All files (*)')
        filename = QtWidgets.QFileDialog.getExistingDirectory(self, 'Save In...', '')
        if fqt5 and isinstance(filename, (list, tuple)):
            extension = filename[1]
            filename = filename[0]
        database = self._make_database(use_class=True, export_to_dict=False)
        if filename and isinstance(database, EX2GK.classes.EX2GKShotCollection):
            location = filename
            backend = "hdf5"
            use_imp = False
            database.exportToIMAS(self._user, path=location, backend=backend, use_preset_impurities=use_imp)

def main():

    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName('EX2GK_JET')
    tfont = QtGui.QFont('Sans Serif')
    if fqt5:
        tfont = QtGui.QFont('FreeSans')
    tfont.setStyleHint(QtGui.QFont.SansSerif)
    ifont = QtGui.QFontInfo(tfont)
    print("Default font: %s" % (ifont.family()))
    app.setFont(tfont)
    ex = EX2GK_JET(ppfIsHere, imasIsHere)

    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
