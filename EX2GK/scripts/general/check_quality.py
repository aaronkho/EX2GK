#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

# This file is completely malleable, it is NOT an example of a standard call, though can be used as inspiration

# Required imports
import os
import sys
import argparse
import numpy as np
import multiprocessing as mp
import matplotlib.pyplot as plt

from EX2GK.tools.general import proctools as ptools, quality_checks as qc

def check_data(infile,pdir='./',foverwrite=False):
    eflag = None
    hflag = None
    wflag = None
    exdata = [None] * 4
    if infile is not None and os.path.isfile(pdir+infile):
        sdata = ptools.unpickle_this(pdir+infile)
        sdata = qc.perform_quality_checks(sdata,do_pickle=False)
        if foverwrite:
            ptools.pickle_this(sdata,infile,pdir)
        if "SRCTPOW" in sdata and sdata["SRCTPOW"] is not None and "TPOW" in sdata and sdata["TPOW"] is not None:
            exdata[0] = float(sdata["TPOW"]) - float(sdata["SRCTPOW"])
        if "TPOWEB" in sdata and sdata["TPOWEB"] is not None:
            exdata[1] = float(sdata["TPOWEB"])
        if "TWK" in sdata and sdata["TWK"] is not None and "TWD" in sdata and sdata["TWD"] is not None:
            exdata[2] = float(sdata["TWD"]) - float(sdata["TWK"])
        if "TWKEB" in sdata and sdata["TWKEB"] is not None:
            exdata[3] = float(sdata["TWKEB"])
        eflag = sdata["ETESTFLAG"] if "ETESTFLAG" in sdata and sdata["ETESTFLAG"] is not None else None
        hflag = sdata["HTESTFLAG"] if "HTESTFLAG" in sdata and sdata["HTESTFLAG"] is not None else None
        wflag = sdata["WTESTFLAG"] if "WTESTFLAG" in sdata and sdata["WTESTFLAG"] is not None else None
    return (eflag,hflag,wflag,exdata)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Perform suite of sanity and consistency checks, and save results.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-i', dest='infile', default='', metavar='FILENAME', type=str, action='store', help='Name of specific file containing extracted data')
    parser.add_argument('-d', dest='pdir', default='', metavar='FILEDIR', type=str, action='store', help='Name of directory containing extracted data')
    parser.add_argument('-n', '--ncores', dest='mpincores', default=0, metavar='N', type=int, action='store', help='Number of cores to use in multiprocessing (N <= 0 disables this function)')
    parser.add_argument('-f', '--overwrite', dest='foverwrite', default=False, action='store_true', help='Flag to force overwriting of any previously stored matching data')
    args = parser.parse_args()

    files = None
    if args.pdir:
        if not os.path.isdir(args.pdir):
            raise IOError("Specified input directory, %s, not found" % (args.pdir))
            sys.exit(2)
        else:
            if args.infile:
                if not os.path.isfile(args.pdir+args.infile):
                    raise IOError("Specified input file, %s, not found" % (args.pdir+args.infile))
                    sys.exit(2)
                else:
                    files = [args.infile]
            else:
                files = [f for f in os.listdir(args.pdir) if os.path.isfile(os.path.join(args.pdir,f))]

    if files is not None and len(files) > 0:
        if args.mpincores > 0:
            tasks = int(np.nanmin((args.mpincores,len(files))))
            pool = mp.Pool(processes=tasks)
            outlist = pool.map(partial(check_data,pdir=args.pdir,foverwrite=args.foverwrite),files)
        else:
            outlist = []
            for nn in np.arange(0,len(files)):
                out = check_data(files[nn],pdir=args.pdir,foverwrite=args.foverwrite)
                outlist.append(out)
        etestpass = 0
        htestpass = 0
        wtestpass = 0
        total = 0
        allpass = 0
        htestdata = None
        wtestdata = None
        for item in outlist:
            aflag = True
            total = total + 1
            if item[0]:
                etestpass = etestpass + 1
            else:
                aflag = False
            if item[1]:
                htestpass = htestpass + 1
            else:
                aflag = False
            if item[2]:
                wtestpass = wtestpass + 1
            else:
                aflag = False
            if aflag:
                allpass = allpass + 1
            if item[3][0] is not None and item[3][1] is not None:
                htestdata = np.hstack((item[3][0],item[3][1])) if htestdata is None else np.vstack((htestdata,np.hstack((item[3][0],item[3][1]))))
            if item[3][2] is not None and item[3][3] is not None:
                wtestdata = np.hstack((item[3][2],item[3][3])) if wtestdata is None else np.vstack((wtestdata,np.hstack((item[3][2],item[3][3]))))
        print(etestpass,htestpass,wtestpass,total,allpass)
        hbins = np.linspace(-2.0,8.0,101)
        wbins = np.linspace(-0.1,1.5,51)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        hfilt = (htestdata[:,0] != 0.0)
        ax.hist(htestdata[hfilt,:]*1.0e-6,hbins,label=["Difference","Error Bar"])
        ax.set_xlabel(r'Heating Energy Deposited [MJ]')
        ax.set_ylabel(r'Counts')
        ax.legend(loc='best')
        fig.savefig('./bhtest_hist.png')
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.hist(wtestdata*1.0e-6,wbins,label=["Difference","Error Bar"])
        ax.set_xlabel(r'Total Plasma Energy [MJ]')
        ax.set_ylabel(r'Counts')
        ax.legend(loc='best')
        fig.savefig('./bwtest_hist.png')
