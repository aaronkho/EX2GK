#!/usr/bin/env python3

import os
import sys
import re
import argparse

from EX2GK.tools.general import proctools as ptools
from EX2GK.tools.general.classes import EX2GKTimeWindow, EX2GKShot, EX2GKShotCollection

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process EX2GK time window for various other codes.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-d', '--device', dest='device', type=str, default=None, action='store', help='Device name to be used when working with joined data')
    parser.add_argument('-s', '--shot', dest='shot', type=int, default=None, action='store', help='Shot identification number to be used when working with joined data')
    parser.add_argument('-t', '--twidx', dest='twi', type=int, default=None, action='store', help='Time window index to be used when working with joined data')
    parser.add_argument('--eqdsk', dest='feqdsk', default=False, action='store_true', help='Toggle creation of EQDSK file from stored equilibrium')
    parser.add_argument('--matlab', dest='fmat', default=False, action='store_true', help='Toggle creation of .mat file from stored profiles and meta data')
    parser.add_argument('fname', type=str, action='store', help='Name of EX2GK pickle file to process')
    args = parser.parse_args()

    bname = args.fname
    mm = re.match(r'^(.+)\.p(kl)?$',bname)
    if mm:
        bname = mm.group(1)

    if not os.path.exists(bname+'.pkl'):
        raise IOError('Could not find desired input file, %s.pkl' % (bname))
    if not args.feqdsk and not args.fmat:
        raise IOError('No operations were specified, run with -h for list of operations')

    data = ptools.unpickle_this(bname+'.pkl')
    twobj = None
    if "META_SHOT" in data:
        twobj = EX2GKTimeWindow.importFromDict(data)
    else:
        if args.shot is None or args.twi is None:
            raise IOError('Provided input file contains multiple time windows so the desired window must be specified, run with -h to see how to specify this')
        scobj = EX2GKShotCollection.importFromDict(data)
        if args.device is None:
            twobj = scobj.getByShotNumber(args.shot).getByIndex(args.twi)
        else:
            twobj = scobj[(args.device,args.shot)].getByIndex(args.twi)

    if twobj is not None:
        if args.feqdsk:
            twobj.generateEQDSKFile(bname+'.eqdsk')
        if args.fmat:
            twobj.exportToMATLAB(bname+'.mat',minimal=True)

