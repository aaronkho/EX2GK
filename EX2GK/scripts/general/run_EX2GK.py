#!/usr/bin/env python3

# This file is completely malleable, it is NOT an example of a standard call, though can be used as inspiration

# Required imports
import os
import sys
import re
import copy
import argparse
import numpy as np
import multiprocessing as mp
import functools
import inspect
import logging
import subprocess

from EX2GK import EX2GK

def create_argument_dict(argfile):
    outdata = dict()
    if isinstance(argfile,str) and os.path.isfile(argfile):
        with open(argfile,'r') as argf:
            for line in argf:
                cline = line.strip()
                if not re.search(r'^#',cline,flags=re.IGNORECASE):
                    sline = cline.split()
                    if len(sline) >= 2 and not re.search(r'^#',sline[1],flags=re.IGNORECASE) and sline[0]:
                        val = None
                        if re.match(r'^True$',sline[1],flags=re.IGNORECASE):
                            val = True
                        elif re.match(r'^False$',sline[1],flags=re.IGNORECASE):
                            val = False
                        elif re.match(r'^[0-9+\-]+$',sline[1],flags=re.IGNORECASE):
                            val = int(float(sline[1]))
                        elif re.match(r'^[0-9+\-.eE]+$',sline[1],flags=re.IGNORECASE):
                            val = float(sline[1])
                        else:
                            val = sline[1]
                        if val is not None:
                            outdata[sline[0]] = val
    return outdata


def construct_input_list(indata,snums,nbatches=0):
    outdata = []
    nb = 0
    if isinstance(nbatches,(float,int)) and int(nbatches) > 0:
        nb = int(nbatches)
    for snum in snums:
        snfilt = (indata[:,0] == snum)
        if np.any(snfilt):
            data = indata[snfilt,:]
            if nb > 0:
                nn = data.shape[0]
                ne = int(nn / nb) + 1
                for ii in np.arange(0,nb):
                    tdata = None
                    if (ii + 1) * ne < nn:
                        tdata = data[ii*ne:(ii+1)*ne,:]
                    else:
                        tdata = data[ii*ne:,:]
                    if tdata is not None and tdata.shape[1] < 5:
                        bnum = np.atleast_2d(np.zeros((tdata.shape[0],))).T + int(ii + 1)
                        tdata = np.hstack((tdata,bnum))
                    outdata.append(tdata)
            else:
                outdata.append(data)
    return outdata


def parallel_extraction(indata,argdict=None,odir='./',fdir=None,foverwrite=False,suppress_out=False,fnofit=False):

    namelist = dict()
    numentries = 0
    if isinstance(indata,np.ndarray):
        numentries = indata.shape[0]
    if isinstance(argdict,dict):
        namelist = copy.deepcopy(argdict)

    oodir = odir
    ffdir = fdir
    if not isinstance(odir,str):
        oodir = './'
    elif not odir.endswith('/'):
        oodir = odir + '/'
    if isinstance(fdir,str) and os.path.isdir(fdir):
        ffdir = fdir if fdir.endswith('/') else fdir + '/'
    if ffdir is None:
        ffdir = './'
    rmflag = False

    if os.path.isdir(oodir) and numentries > 0:
        oflist = []
        for jj in np.arange(0,numentries):
            exnamelist = copy.deepcopy(namelist)
            spar = indata[jj,:].flatten()
            replaceflag = False
            goodflag = True
            sstr = '%06d' % (int(spar[0]))
            faillog = 'EX2GK_fail_'+sstr+'.log'
            ii = 1
            tstr = '%02d' % (int(ii))
            bstr = '_B%02d' % (int(spar[4])) if spar.size > 4 else ''
            rstr = '_raw' if fnofit else ''
            while os.path.isfile(oodir+'S'+sstr+'_T'+tstr+bstr+rstr+'.pkl') and goodflag and not replaceflag:
                tdb = EX2GK.ptools.unpickle_this(oodir+'S'+sstr+'_T'+tstr+bstr+rstr+'.pkl')
                if "SHOT" in tdb and "T1" in tdb and "T2" in tdb:
                    if tdb["SHOT"] is None:
                        replaceflag = True
                    elif spar[1] is None and tdb["T1"] is None and spar[2] is None and tdb["T2"] is None:
                        goodflag = False
                    elif np.abs(spar[1] - tdb["T1"]) < 1.0e-3 and np.abs(spar[2] - tdb["T2"]) < 1.0e-3:
                        goodflag = False
                if goodflag and not replaceflag:
                    ii = ii + 1
                    tstr = '%02d' % (int(ii))
            if (not goodflag and foverwrite) or replaceflag:
                goodflag = True
                print('Overwriting time slice #%2d for shot #%10d.' % (ii,snum))

            if goodflag:
                oldstd = sys.stdout
                olderr = sys.stderr
                if suppress_out:
                    sys.stdout = open(os.devnull,'w')
                    sys.stderr = open(os.devnull,'w')
                try:
                    ofname = 'S'+sstr+'_T'+tstr+bstr+rstr+'.pkl'
                    exnamelist["SHOTINFO"] = "%8d %10.6f %10.6f" % (spar[0],spar[1],spar[2])
                    exnamelist["SHOTPHASE"] = int(spar[3]) if spar.size > 3 else None
                    exnamelist["TWNUM"] = int(ii)
                    sdata = EX2GK.extract_and_standardize_data(exnamelist["MACHINE"],exnamelist) if fnofit else EX2GK.grab_and_fit(exnamelist)
                    if isinstance(sdata,(list,tuple)) and len(sdata) > 0:
                        sdata = sdata[0]
                    odata = sdata.exportToDict()
                    EX2GK.ptools.pickle_this(odata,ofname,oodir)
                    oflist.append(ofname)
                except Exception as e:
                    logger = logging.Logger('EX2GK')
                    logger.addHandler(logging.FileHandler(filename=ffdir+faillog))
                    logger.exception("Fatal error processing %10d%10.4f%10.4f" % (int(spar[0]),spar[1],spar[2]))
                if suppress_out:
                    sys.stdout = oldstd
                    sys.stderr = olderr
        if rmflag:
            for name in oflist:
                subprocess.run(["rm","-f",oodir+name])


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Extract experimental data, apply Gaussian process regression fit, calculate GK input parameters, and save results.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-i', dest='shotlist', default='shotlist.txt', metavar='FILENAME', type=str, action='store', help='Name of file containing shot numbers and time windows to extract')
    parser.add_argument('-x', dest='argfile', default='argdict.txt', metavar='FILENAME', type=str, action='store', help='Name of file containing extra arguments and options for the program.')
    parser.add_argument('-n', '--ncores', dest='mpincores', default=0, metavar='N', type=int, action='store', help='Number of cores to use in multiprocessing (N <= 0 disables this function)')
    parser.add_argument('-b', '--nbatches', dest='mpinbatches', default=0, metavar='N', type=int, action='store', help='Number of batches to split each shot (N <= 1 disables this function)')
    parser.add_argument('-f', '--overwrite', dest='foverwrite', default=False, action='store_true', help='Flag to force overwriting of any previously stored matching data')
    parser.add_argument('-o', '--output_dir', dest='odir', default='./Brine/', metavar='OUTDIR', type=str, action='store', help='Name of directory where extracted data will be saved')
    parser.add_argument('-l', '--log_dir', dest='fdir', default='', metavar='FAILDIR', type=str, action='store', help='Name of directory where failure logs will be saved')
    parser.add_argument('--nofit', dest='fnofit', default=False, action='store_true', help='Flag to toggle data extraction without fitting routine, does not compute estimated quantities')
    parser.add_argument('sidx', nargs='?', type=int, action='store', default=0, help='Optional start index to process in provided shot list')
    parser.add_argument('nidx', nargs='?', type=int, action='store', default=-1, help='Optional number of items to process in provided shot list')
    args = parser.parse_args()

    if not os.path.isfile(args.shotlist):
        raise IOError("Mandatory time window list file, %s, not found. Scripted aborted." % (args.shotlist))
        sys.exit(2)
    if not os.path.isfile(args.argfile):
        raise IOError("Mandatory namelist file, %s, not found. Script aborted."% (args.argfile))

    odir = args.odir
    if not args.odir:
        raise IOError("Output directory not defined. Script aborted.")
    elif not args.odir.endswith('/'):
        odir = odir + '/'
    if not os.path.exists(odir):
        os.makedirs(odir)

    fdir = None
    if args.fdir:
        fdir = args.fdir
        if not fdir.endswith('/'):
            fdir = fdir + '/'
        if not os.path.exists(fdir):
            os.makedirs(fdir)

    fdebug = False
    if fdebug:
        location = inspect.getsourcefile(EX2GK.extract_and_standardize_data)
        exsrcdir = os.path.dirname(location) + '/'
        with open('./debug.txt','w') as ff:
            ff.write("EX2GK definition file: %s" % (exsrcdir))

    data = np.atleast_2d(np.genfromtxt(args.shotlist))
    snums = []
    for jj in np.arange(0,data.shape[0]):
        snum = int(data[jj,0])
        if snum not in snums:
            snums.append(snum)
    argdict = create_argument_dict(args.argfile)

    si = 0
    ni = 0
    if args.sidx < 0:
        si = 0
    if args.nidx < 0:
        ni = len(snums)

    if ni <= 0 and si >= 0 and si < len(snums):
        bidx = si
        snlist = snums[bidx]
        if not isinstance(snlist,list):
            snlist = [snlist]
        inputdata = construct_input_list(data,snlist,nbatches=args.mpinbatches)
        if args.mpincores > 0 and args.mpinbatches > 1:
            tasks = int(np.nanmin((args.mpincores,len(inputdata))))
            pool = mp.Pool(processes=tasks)
            pool.map(functools.partial(parallel_extraction,argdict=argdict,odir=odir,fdir=fdir,foverwrite=args.foverwrite,suppress_out=True,fnofit=args.fnofit),inputdata)
        else:
            for nn in np.arange(0,len(inputdata)):
                print(nn + bidx,snums[nn+bidx])
                parallel_extraction(inputdata[nn],argdict=argdict,odir=odir,fdir=fdir,foverwrite=args.foverwrite,fnofit=args.fnofit)
    elif si >= 0 and si < len(snums):
        bidx = si
        eidx = si + ni
        snlist = snums[bidx:eidx] if eidx < len(snums) else snums[bidx:]
        if not isinstance(snlist,list):
            snlist = [snlist]
        inputdata = construct_input_list(data,snlist,nbatches=args.mpinbatches)
        if args.mpincores > 0:
            tasks = int(np.nanmin((args.mpincores,len(inputdata))))
            pool = mp.Pool(processes=tasks)
            pool.map(functools.partial(parallel_extraction,argdict=argdict,odir=odir,fdir=fdir,foverwrite=args.foverwrite,suppress_out=True,fnofit=args.fnofit),inputdata)
        else:
            for nn in np.arange(0,len(inputdata)):
                print(nn + bidx,snums[nn+bidx])
                parallel_extraction(inputdata[nn],argdict=argdict,odir=odir,fdir=fdir,foverwrite=args.foverwrite,fnofit=args.fnofit)
