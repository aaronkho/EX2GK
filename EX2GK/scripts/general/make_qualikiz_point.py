#!/usr/bin/env python3

from pathlib import Path
import argparse
import numpy as np

from EX2GK.tools.general import proctools as ptools
from EX2GK.code_adapters.qualikiz import pdb2qlk

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generate a QuaLiKiz run from a EX2GK pickle file.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-k', dest='ktrsfile', default='', type=str, action='store', help='File containing list of kthetarhos to use in QuaLiKiz run')
    parser.add_argument('-o', dest='oname', default='ex2gk', type=str, action='store', help='Name for the QuaLiKiz run folder')
    parser.add_argument('xpath', nargs=1, type=str, action='store', help='Full path to QuaLiKiz executable that should be linked to input folder')
    parser.add_argument('ipkl', nargs=1, type=str, action='store', help='Path to EX2GK pickle file containing the data to convert into a QuaLiKiz run')
    parser.add_argument('rfile', nargs=1, type=str, action='store', help='File containing list of radial points, in rho, to define QuaLiKiz inputs')
    args = parser.parse_args()

    xpath = Path(args.xpath[0])
    dpath = Path(args.ipkl[0])
    rpath = Path(args.rfile[0])
    rho = None
    if rpath.is_file():
        rho = np.genfromtxt(str(rpath.resolve())).flatten()

    ktrs = np.array([ 0.1, 0.175, 0.25, 0.325, 0.4, 0.5, 0.7, 1.0, 1.8, 3.0, 9.0, 15.0, 21.0, 27.0, 36.0, 45.0 ])
    kpath = Path(args.ktrsfile)
    if kpath.is_file():
        ktrs = np.genfromtxt(str(kpath.resolve())).flatten()

    data = None
    if dpath.is_file():
        data = ptools.unpickle_this(str(dpath.resolve()))

    if xpath.exists() and rho is not None and data is not None and ktrs.size > 0:
        (dd,xobj,dl) = pdb2qlk.get_QLK_input_data(data,rho,kthetarhos=ktrs,make_xpoint=True,use_imp_ions=True)
        if xobj is not None:
            sdict = dd
            xobj['rot_flag'] = 2
            xobj['phys_meth'] = 1
            xobj['numsols'] = 2
            xobj['separateflux'] = 1
            pobj = pdb2qlk.make_QLK_scan(xobj,'parallel',sdict)
            robj = pdb2qlk.make_QLK_run(pobj,args.oname,rundir='./',execpath=str(xpath.absolute()))
            robj.prepare()
            robj.generate_input()
