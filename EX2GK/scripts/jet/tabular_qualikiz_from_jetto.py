#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import os
import sys
import pwd
import re
import copy
import argparse
import inspect
import warnings
import json
import numpy as np
import pandas as pd
from pathlib import Path

from EX2GK import EX2GK
from EX2GK.tools.jet import jetto_adapter as jmod

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generate QuaLiKiz-style input parameters from a time slice of a JETTO run.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('--tag', dest='otag', default=None, action='store_true', help='Name tag to use for output files, default is the run name stem')
    parser.add_argument('--tci', dest='tciflag', default=False, action='store_true', help='Flag to toggle usage of exact reproduction of JETTO TCI implementation for data processing')
    parser.add_argument('--smoothed', dest='smoothflag', default=False, action='store_true', help='Flag to toggle usage of Savitzky-Golay filter for radial smoothing')
    parser.add_argument('--radial_splines', dest='smspflag', default=False, action='store_true', help='Flag to toggle use of smoothed splines for major radius interpolations')
    parser.add_argument('--puretor', dest='fpuretor', default=False, action='store_true', help='Flag to toggle pure toroidal rotation option for data extraction')
    parser.add_argument('--kwargs', dest='fkwargs', default=None, metavar='ARGFILE', type=str, action='store', help='Name of .json formatted file containing optional arguments')
    parser.add_argument('--debug', dest='fdebug', default=False, action='store_true', help='Flag to toggle printing of debug statements in various called functions')
    parser.add_argument('--outdir', dest='outdir', default='./', metavar='OUTPATH', type=str, action='store', help='Path to desired output directory')
    parser.add_argument('baserun', type=str, action='store', help='Full path of JETTO run directory to serve as reference point for run generation')
    parser.add_argument('outname', type=str, action='store', help='Name of output HDF5 file containing tabular QuaLiKiz inputs generated from JETTO time slices')
    args = parser.parse_args()

    kwargs = {}
    if args.fkwargs is not None:
        kwpath = Path(args.fkwargs)
        if kwpath.is_file():
            with open(args.fkwargs,'r') as kwf:
                kwargs = json.load(kwf)

    # Obtain user run directory (should allow user to specify completely?)
    #uid = args.uname if args.uname else pwd.getpwuid(os.getuid())[0]
    #'/common/cmg/' + uid + '/jetto/runs/' if not args.fctg
    #'/home/' + uid + '/cmg/catalog/jetto/jet/'

    # Print location of repository (useful for debugging when user uses a modified version)
    location = inspect.getsourcefile(EX2GK.compute_code_specific_data)
    print("Using EX2GK definition from: %s" % (os.path.dirname(location) + '/'))

    runpath = Path(args.baserun)
    if runpath.exists() and runpath.is_dir():

        print("Applying process to %s..." % (str(runpath.resolve())))
        bdir = runpath.parent
        baserun = runpath.stem

        # If no time values are provided, defaults to grabbing EVERY time slice in JSP
        #   This could take a long time depending on the JETTO run, but this option is necessary for easy time trace extractions
        time_in = kwargs.get('time_vector', None)
        if time_in is not None and len(time_in) == 0:
            time_in = None

        # Only read JSP once, return list of EX2GKTimeWindow classes - will be empty if something goes wrong
        profile_option = 0
        if args.tciflag:
            profile_option = 1
        elif args.smoothflag:
            profile_option = 2
        sdatalist = jmod.import_from_JSP(
            jetto_rundir_path=str(runpath.resolve()),
            time=time_in,
            profopt=profile_option,
            smspflag=args.smspflag,
            fpuretor=args.fpuretor,
            fdebug=args.fdebug
        )
        timelist = []
        siminlist = []

        outdf = None
        for ii in range(0,len(sdatalist)):

            if isinstance(sdatalist[ii], EX2GK.classes.EX2GKTimeWindow):
                sdatalist[ii].postProcess(no_assumptions=True)
                sdatalist[ii].updateTimestamp()

                code_kwargs = {"FINALCODE": "qualikiz", "FORCEPOST": True, "DEBUGFLAG": args.fdebug}
                sdata = EX2GK.compute_code_specific_data(sdatalist[ii], namelist=code_kwargs)

                if not isinstance(sdata, EX2GK.classes.EX2GKTimeWindow):
                    raise ValueError("Unexpected error within code-specific processing step, aborting JSP conversion!")

                (rho, jrho, rhoeb) = sdata["CD"].convert(sdata["PD"]["X"][""], sdata["META"]["CSO"], "RHOTORN", sdata["META"]["CSOP"], sdata["META"]["CSOP"], fdebug=args.fdebug)
                rpath = runpath / 'rho.qlk'
                if 'rho_vector' in kwargs and isinstance(kwargs['rho_vector'], (list,tuple)):
                    rho = list(kwargs['rho_vector'])
                elif 'rho_vector' in kwargs and kwargs['rho_vector'] == "from_rundir" and rpath.is_file():
                    rho = np.genfromtxt(str(rpath.resolve())).flatten()
                    if rho[0] < 0.005:
                        rho = rho[1:]
                elif 'rho_vector' in kwargs and os.path.isfile(kwargs['rho_vector']):
                    rho = np.genfromtxt(kwargs['rho_vector']).flatten()
                cpath = Path('./rho_qlk')
                if cpath.is_file():
                    warnings.warn("rho.qlk file found in working directory, using local vector for dimx!", UserWarning)
                    rho = np.genfromtxt(str(cpath.resolve())).flatten()

                rzero = None    # Will use Rzero extracted from import_from_JSP unless specific user-defined value is given
                zpath = runpath / 'R0.qlk'
                if 'qlk_R0' in kwargs and kwargs['qlk_R0'] == "from_rundir" and zpath.is_file():     # Redundant!
                    rzero = float(np.genfromtxt(str(zpath.resolve())).flatten()[0])

                collmult = 1.0
                cpath = runpath / 'collmult.qlk'
                if 'qlk_collmult' in kwargs and kwargs['qlk_collmult'] == "from_rundir" and cpath.is_file():
                    collmult = float(np.genfromtxt(str(cpath.resolve())).flatten()[0])

                emopt = 0
                epath = runpath / 'em_stab.qlk'
                if 'em_stab_flag' in kwargs and kwargs['em_stab_flag'] == "from_rundir" and epath.is_file():
                    emopt = int(np.genfromtxt(str(epath.resolve())).flatten()[0])

                from EX2GK.code_adapters.qualikiz import converter
                df = converter.convert_time_window_to_minimal(sdata, rho)

                df['collmult'] = np.ones(len(df)) * collmult
                df['emstab'] = np.zeros(len(df)) + emopt
                if rzero is not None:
                    df['R0'] = 0.0 * df['R0'] + rzero
                df['time'] = np.ones(len(df)) * sdata["META"]["T1"]

                if outdf is None:
                    df['time_idx'] = [0] * len(df)
                    outdf = copy.deepcopy(df)
                else:
                    df['time_idx'] = [int(np.rint(np.nanmax(outdf['time_idx']) + 1))] * len(df)
                    outdf = pd.concat([outdf, df]).reset_index(drop=True)

        if outdf is not None and not outdf.empty:
            outdf = outdf.set_index(['time_idx', 'rho_idx'])
            opath = Path(args.outdir)
            if not opath.is_dir():
                opath.mkdir(parents=True)
            oname = args.outname
            if not oname.endswith('.h5'):
                oname += '.h5'
            ofile = opath / oname
            outdf.to_hdf(str(ofile), '/qualikiz')

    else:
        print("JETTO run directory %s not found!" % (runpath))

