#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import os
import sys
import pwd
import re
import copy
import argparse
import inspect
import warnings
import json
import numpy as np
from pathlib import Path

from EX2GK import EX2GK
from EX2GK.tools.jet import jetto_adapter as jmod

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generate EX2GK-formatted data file from a time slice of a JETTO run, option to process directly into code input.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('--tag', dest='otag', default=None, action='store_true', help='Name tag to use for output files, default is the run name stem')
    parser.add_argument('--code', dest='ocode', default='', metavar='FINALCODE', type=str, action='store', help='Optional target output code, uses EX2GK code adapter if available')
    parser.add_argument('--tci', dest='tciflag', default=False, action='store_true', help='Flag to toggle usage of exact reproduction of JETTO TCI implementation for data processing')
    parser.add_argument('--smoothed', dest='smoothflag', default=False, action='store_true', help='Flag to toggle usage of Savitzky-Golay filter for radial smoothing')
    parser.add_argument('--radial_splines', dest='smspflag', default=False, action='store_true', help='Flag to toggle use of smoothed splines for major radius interpolations')
    parser.add_argument('--puretor', dest='fpuretor', default=False, action='store_true', help='Flag to toggle pure toroidal rotation option for data extraction')
    parser.add_argument('--debug', dest='fdebug', default=False, action='store_true', help='Flag to toggle printing of debug statements in various called functions')
    parser.add_argument('--kwargs', dest='fkwargs', default=None, metavar='ARGFILE', type=str, action='store', help='Name of .json formatted file containing optional arguments')
    parser.add_argument('--outdir', dest='outdir', default='./', metavar='OUTPATH', type=str, action='store', help='Path to desired output directory')
    parser.add_argument('--pickle', dest='picklename', default=None, type=str, action='store', help='File name to use for saving intermediate Python pickle file, only saves if provided')
    parser.add_argument('--hdf5', dest='hdf5name', default=None, type=str, action='store', help='File name to use for exporting to HDF5 format, via pandas (broken, do not use!), only exports if provided')
    parser.add_argument('--rho', dest='radius', default=None, type=float, action='store', help='Toggles output generation of radial point as a function of time, if supported')
    parser.add_argument('baserun', type=str, action='store', help='Full path of JETTO run directory to serve as reference point for run generation')
    parser.add_argument('time', nargs='*', type=float, action='store', help='Time slices within JETTO run to convert into an EX2GK time window')
    args = parser.parse_args()

    kwargs = {}
    if args.fkwargs is not None:
        kwpath = Path(args.fkwargs)
        if kwpath.is_file():
            with open(args.fkwargs,'r') as kwf:
                kwargs = json.load(kwf)

    # Obtain user run directory (should allow user to specify completely?)
    #uid = args.uname if args.uname else pwd.getpwuid(os.getuid())[0]
    #'/common/cmg/' + uid + '/jetto/runs/' if not args.fctg
    #'/home/' + uid + '/cmg/catalog/jetto/jet/'

    # Print location of repository (useful for debugging when user uses a modified version)
    location = inspect.getsourcefile(EX2GK.compute_code_specific_data)
    print("Using EX2GK definition from: %s" % (os.path.dirname(location) + '/'))

    runpath = Path(args.baserun)
    if runpath.exists() and runpath.is_dir():

        print("Applying process to %s..." % (str(runpath.resolve())))
        bdir = runpath.parent
        baserun = runpath.stem

        # If no time values are provided, defaults to grabbing EVERY time slice in JSP
        #   This could take a long time depending on the JETTO run, but this option is necessary for easy time trace extractions
        time_in = args.time
        if len(time_in) == 0:
            time_in = None

        # Only read JSP once, return list of EX2GKTimeWindow classes - will be empty if something goes wrong
        profile_option = 0
        if args.tciflag:
            profile_option = 1
        elif args.smoothflag:
            profile_option = 2
        sdatalist = jmod.import_from_JSP(
            jetto_rundir_path=str(runpath.resolve()),
            time=time_in,
            profopt=profile_option,
            smspflag=args.smspflag,
            fpuretor=args.fpuretor,
            fdebug=args.fdebug
        )
        timelist = []
        siminlist = []

        for ii in range(0,len(sdatalist)):

            if isinstance(sdatalist[ii],EX2GK.classes.EX2GKTimeWindow):
                sdatalist[ii].postProcess(no_assumptions=True)
                sdatalist[ii].updateTimestamp()

                if not os.path.isdir(args.outdir):
                    os.makedirs(args.outdir)

                code_kwargs = {"FINALCODE": args.ocode.lower(), "FORCEPOST": True, "DEBUGFLAG": args.fdebug}
                if code_kwargs["FINALCODE"] == 'gene':
                    code_kwargs["CSPROC"] = kwargs['gene_magn_geometry'] if 'gene_magn_geometry' in kwargs and isinstance(kwargs['gene_magn_geometry'],str) else None
                sdata = EX2GK.compute_code_specific_data(sdatalist[ii],namelist=code_kwargs)

                if not isinstance(sdata,EX2GK.classes.EX2GKTimeWindow):
                    raise ValueError("Unexpected error within code-specific processing step, aborting JSP conversion!")

                # Generate QuaLiKiz run directory - REQUIRES QuaLiKiz-pythontools to be installed!
                if code_kwargs["FINALCODE"] == 'qualikiz' and 'qlk_execpath' in kwargs and os.path.exists(kwargs['qlk_execpath']):

                    ktrs = np.array([ 0.1, 0.175, 0.25, 0.325, 0.4, 0.5, 0.7, 1.0, 1.8, 3.0, 9.0, 15.0, 21.0, 27.0, 36.0, 45.0 ])
                    kpath = runpath / ' kthetarhos.qlk'
                    if 'qlk_kthetarhos' in kwargs and isinstance(kwargs['qlk_kthetarhos'],(list,tuple)):
                        ktrs = list(kwargs['qlk_kthetarhos'])
                    elif 'qlk_kthetarhos' in kwargs and kwargs['qlk_kthetarhos'] == "from_rundir" and kpath.is_file():
                        ktrs = np.genfromtxt(str(kpath.resolve())).flatten()
                    elif 'qlk_kthetarhos' in kwargs and os.path.isfile(kwargs['qlk_kthetarhos']):
                        ktrs = np.genfromtxt(kwargs['qlk_kthetarhos']).flatten()
                    elif os.path.isfile('kthetarhos.qlk'):
                        warnings.warn("kthetarhos.qlk file found in working directory, using local vector for dimn!",UserWarning)
                        ktrs = np.genfromtxt('kthetarhos.qlk').flatten()

                    (rho,jrho,rhoeb) = sdata["CD"].convert(sdata["PD"]["X"][""],sdata["META"]["CSO"],"RHOTORN",sdata["META"]["CSOP"],sdata["META"]["CSOP"],fdebug=args.fdebug)
                    rpath = runpath / 'rho.qlk'
                    if 'qlk_rho' in kwargs and isinstance(kwargs['qlk_rho'],(list,tuple)):
                        rho = list(kwargs['qlk_rho'])
                    elif 'qlk_rho' in kwargs and kwargs['qlk_rho'] == "from_rundir" and rpath.is_file():
                        rho = np.genfromtxt(str(rpath.resolve())).flatten()
                        if rho[0] < 0.005:
                            rho = rho[1:]
                    elif 'qlk_rho' in kwargs and os.path.isfile(kwargs['qlk_rho']):
                        rho = np.genfromtxt(kwargs['qlk_rho']).flatten()
                    if os.path.isfile('rho.qlk'):
                        warnings.warn("rho.qlk file found in working directory, using local vector for dimx!",UserWarning)
                        rho = np.genfromtxt('rho.qlk').flatten()

                    rzero = None    # Will use Rzero extracted from import_from_JSP unless specific user-defined value is given
                    zpath = runpath / 'R0.qlk'
                    if 'qlk_R0' in kwargs and isinstance(kwargs['qlk_R0'],(int,float)):
                        rzero = float(kwargs['qlk_R0'])
                    elif 'qlk_R0' in kwargs and kwargs['qlk_R0'] == "from_rundir" and zpath.is_file():     # Redundant!
                        rzero = float(np.genfromtxt(str(zpath.resolve())).flatten()[0])
                    elif 'qlk_R0' in kwargs and os.path.isfile(kwargs['qlk_R0']):
                        rzero = float(np.genfromtxt(kwargs['qlk_R0']).flatten()[0])
                    if os.path.isfile('R0.qlk'):
                        warnings.warn("R0.qlk file found in working directory, using local vector for Rzero!",UserWarning)
                        rzero = float(np.genfromtxt('R0.qlk').flatten()[0])

                    rotopt = 2
                    mpath = runpath / 'rot_flag.qlk'
                    if 'qlk_rot_flag' in kwargs and isinstance(kwargs['qlk_rot_flag'],(int,float)):
                        rotopt = int(kwargs['qlk_rot_flag'])
                    elif 'qlk_rot_flag' in kwargs and kwargs['qlk_rot_flag'] == "from_rundir" and mpath.is_file():
                        rotopt = int(np.genfromtxt(str(mpath.resolve())).flatten()[0])
                    elif 'qlk_rot_flag' in kwargs and os.path.isfile(kwargs['qlk_rot_flag']):
                        rotopt = int(np.genfromtxt(kwargs['qlk_rot_flag']).flatten()[0])
                    if os.path.isfile('rot_flag.qlk'):
                        warnings.warn("rot_flag.qlk file found in working directory, using local rotation settings!",UserWarning)
                        rotopt = int(np.genfromtxt('rot_flag.qlk').flatten()[0])

                    collopt = 1
                    fpath = runpath / 'coll_flag.qlk'
                    if 'qlk_coll_flag' in kwargs and isinstance(kwargs['qlk_coll_flag'],(int,float)):
                        collopt = int(kwargs['qlk_coll_flag'])
                    elif 'qlk_coll_flag' in kwargs and kwargs['qlk_coll_flag'] == "from_rundir" and fpath.is_file():
                        collopt = int(np.genfromtxt(str(fpath.resolve())).flatten()[0])
                    elif 'qlk_coll_flag' in kwargs and os.path.isfile(kwargs['qlk_coll_flag']):
                        collopt = int(np.genfromtxt(kwargs['qlk_coll_flag']).flatten()[0])
                    if os.path.isfile('coll_flag.qlk'):
                        warnings.warn("coll_flag.qlk file found in working directory, using local collision settings!",UserWarning)
                        collopt = int(np.genfromtxt('coll_flag.qlk').flatten()[0])

                    collmult = 1.0
                    cpath = runpath / 'collmult.qlk'
                    if 'qlk_collmult' in kwargs and isinstance(kwargs['qlk_collmult'],(int,float)):
                        collmult = float(kwargs['qlk_collmult'])
                    elif 'qlk_collmult' in kwargs and kwargs['qlk_collmult'] == "from_rundir" and cpath.is_file():
                        collmult = float(np.genfromtxt(str(cpath.resolve())).flatten()[0])
                    elif 'qlk_collmult' in kwargs and os.path.isfile(kwargs['qlk_collmult']):
                        collmult = float(np.genfromtxt(kwargs['qlk_collmult']).flatten()[0])
                    if os.path.isfile('collmult.qlk'):
                        warnings.warn("collmult.qlk file found in working directory, using local collision multiplier!",UserWarning)
                        collmult = float(np.genfromtxt('collmult.qlk').flatten()[0])

                    emopt = 0
                    epath = runpath / 'em_stab.qlk'
                    if 'qlk_em_stab_flag' in kwargs and isinstance(kwargs['qlk_em_stab_flag'],(int,float)):
                        emopt = int(kwargs['qlk_em_stab_flag'])
                    elif 'qlk_em_stab_flag' in kwargs and kwargs['qlk_em_stab_flag'] == "from_rundir" and epath.is_file():
                        emopt = int(np.genfromtxt(str(epath.resolve())).flatten()[0])
                    elif 'qlk_em_stab_flag' in kwargs and os.path.isfile(kwargs['qlk_em_stab_flag']):
                        emopt = int(np.genfromtxt(kwargs['qlk_em_stab_flag']).flatten()[0])
                    if os.path.isfile('em_stab.qlk'):
                        warnings.warn("em_stab.qlk file found in working directory, using local EM stabilization settings!",UserWarning)
                        emopt = int(np.genfromtxt('em_stab.qlk').flatten()[0])

                    from EX2GK.code_adapters.qualikiz import pdb2qlk
                    data = sdata.exportToDict()

                    (qlk_scan_dict,xobj) = pdb2qlk.get_QLK_input_data(data,rho,kthetarhos=ktrs,make_xpoint=True,use_imp_ions=True,set_rzero=rzero, \
                                                                      set_rot_flag=rotopt,set_coll_flag=collopt,coll_mult=collmult,set_em_stab_flag=emopt, \
                                                                      debug_flag=args.fdebug)
                    if xobj is not None and args.radius is None:
                        basename = "%s_t%d" % (baserun.strip('/'),int(args.time[ii] * 1000))
                        odir = os.path.join(args.outdir,basename)
                        xobj['phys_meth'] = 1
                        xobj['numsols'] = 2
                        xobj['separateflux'] = 1
                        for item in list(qlk_scan_dict.keys()):
                            if item in pdb2qlk.QuaLiKizXpoint.Meta.in_args or \
                               item in pdb2qlk.QuaLiKizXpoint.Options.in_args or \
                               item in ['kthetarhos', 'R0'] or \
                               item.startswith('error_'):
                                del qlk_scan_dict[item]
                        pobj = pdb2qlk.QuaLiKizPlan(scan_dict=qlk_scan_dict,scan_type='parallel',xpoint_base=xobj)
                        robj = pdb2qlk.QuaLiKizRun(odir,"qualikiz",kwargs['qlk_execpath'],qualikiz_plan=pobj)
                        robj.prepare()
                        robj.generate_input()
                        print("QuaLiKiz input file %s generated!" % (os.path.join(odir,"qualikiz","parameters.json")))

                    if qlk_scan_dict is not None and args.radius is not None:
                        timelist.append(data["META_T1"])
                        siminlist.append(copy.deepcopy(qlk_scan_dict))
                        if args.fdebug:
                            print("QuaLiKiz input parameters for t=%6.3f stored." % timelist[-1])

                # Generate GENE run directory
                if code_kwargs["FINALCODE"] == 'gene':

                    rho = 0.5
                    if 'gene_rho' in kwargs and isinstance(kwargs['gene_rho'],(int,float)):
                        rho = float(kwargs['gene_rho'])
                    elif 'gene_rho' in kwargs and os.path.isfile(kwargs['gene_rho']):
                        rho = np.genfromtxt(kwargs['gene_rho']).flatten()
                    if args.radius is not None:
                        rho = float(args.radius)
                    rundir = None
                    if 'gene_run_directory' in kwargs and isinstance(kwargs['gene_run_directory'],(str)):
                        rundir = kwargs['gene_run_directory']

                    from EX2GK.code_adapters.gene import pdb2gene
                    data = sdata.exportToDict()

                    (gene_scan_dict,gene_single) = pdb2gene.get_GENE_input_data(data,rho,make_input=True,use_imp_ions=True,run_directory=rundir,debug_flag=args.fdebug)
                    if gene_single is not None:
                        # Print GENE namelist parameters.dat file
                        basename = "%s_t%d" % (baserun.strip('/'),int(args.time[ii] * 1000))
                        if args.radius is not None:
                            basename = basename + ("_r%d" % (int(args.radius * 1000)))
                        codename = "gene_%s" % (data["META_CODEGEOM"])
                        odir = os.path.join(args.outdir,basename,codename)
                        if not os.path.isdir(odir):
                            os.makedirs(odir)
                        ofile = os.path.join(odir,"parameters.dat")
                        with open(ofile,"w+") as f:
                            for nlkey, nml in gene_single.items():
                                if len(nml) > 0:
                                    nltype = nlkey.split(':')[0]
                                    f.write("&{}\n".format(nltype))
                                    for key, value in nml.items():
                                        f.write("{} = {}\n".format(key,value))
                                    f.write("/\n\n")
                        print("GENE input file %s generated!" % (ofile))

                    if gene_scan_dict is not None and args.radius is not None:
                        timelist.append(data["META_T1"])
                        siminlist.append(copy.deepcopy(gene_scan_dict))
                        if args.fdebug:
                            print("GENE input parameters for t=%6.3f stored." % timelist[-1])

                # Optional saving of intermediate data
                if args.picklename is not None and args.picklename:
                    picklename = args.picklename + '.pkl' if not args.picklename.endswith('.pkl') else args.picklename
                    outdata = sdata.exportToDict()
                    EX2GK.ptools.pickle_this(outdata,picklename,args.outdir)
                if args.hdf5name is not None and args.hdf5name:
                    hdf5name = args.hdf5name + '.h5' if not args.hdf5name.endswith('.h5') else args.hdf5name
                    import pandas as pd
                    outdata = sdata.exportToPandas()
                    store = pd.HDFStore(os.path.join(args.outdir,hdf5name))
                    for key, var in outdata.items():
                        store[key] = var
                    store.close()

        if args.radius is not None and len(siminlist) > 0 and len(siminlist) == len(timelist):
            time_scan_dict = {'time': np.array([])}
            for key in siminlist[0]:
                time_scan_dict[key] = np.array([])

            for ii in range(0,len(siminlist)):
                # Identify radial slice inside scan dictionary vectors
                rhovec = siminlist[ii]['rho']
                idxv = np.where(rhovec > args.radius)[0]
                jidx = idxv[0] if len(idxv) > 0 else len(idxv) - 1
                if jidx > 0 and np.abs(rhovec[jidx] - args.radius) > np.abs(rhovec[jidx-1] - args.radius):
                    jidx = jidx - 1

                fwritten = False
                for key in siminlist[ii].keys():
                    if key in time_scan_dict:
                        val = siminlist[ii][key]
                        time_scan_dict[key] = np.append(time_scan_dict[key],val[jidx])
                        fwritten = True
                if fwritten:
                    time_scan_dict['time'] = np.append(time_scan_dict['time'],timelist[ii])

            if len(time_scan_dict['time']) > 0:
                if code_kwargs["FINALCODE"] == 'qualikiz' and 'qlk_execpath' in kwargs and os.path.exists(kwargs['qlk_execpath']):

                    nions = 0
                    itag = "%d" % (nions)
                    while "Zi"+itag in time_scan_dict:
                        nions += 1
                        itag = "%d" % (nions)

                    ktrs = np.array([ 0.1, 0.175, 0.25, 0.325, 0.4, 0.5, 0.7, 1.0, 1.8, 3.0, 9.0, 15.0, 21.0, 27.0, 36.0, 45.0 ])
                    kpath = runpath / ' kthetarhos.qlk'
                    if 'qlk_kthetarhos' in kwargs and isinstance(kwargs['qlk_kthetarhos'],(list,tuple)):
                        ktrs = list(kwargs['qlk_kthetarhos'])
                    elif 'qlk_kthetarhos' in kwargs and kwargs['qlk_kthetarhos'] == "from_rundir" and kpath.is_file():
                        ktrs = np.genfromtxt(str(kpath.resolve())).flatten()
                    elif 'qlk_kthetarhos' in kwargs and os.path.isfile(kwargs['qlk_kthetarhos']):
                        ktrs = np.genfromtxt(kwargs['qlk_kthetarhos']).flatten()
                    elif os.path.isfile('kthetarhos.qlk'):
                        warnings.warn("kthetarhos.qlk file found in working directory, using local vector for dimn!",UserWarning)
                        ktrs = np.genfromtxt('kthetarhos.qlk').flatten()

                    rotopt = 2
                    mpath = runpath / 'rot_flag.qlk'
                    if 'qlk_rot_flag' in kwargs and isinstance(kwargs['qlk_rot_flag'],(int,float)):
                        rotopt = int(kwargs['qlk_rot_flag'])
                    elif 'qlk_rot_flag' in kwargs and kwargs['qlk_rot_flag'] == "from_rundir" and mpath.is_file():
                        rotopt = int(np.genfromtxt(str(mpath.resolve())).flatten()[0])
                    elif 'qlk_rot_flag' in kwargs and os.path.isfile(kwargs['qlk_rot_flag']):
                        rotopt = int(np.genfromtxt(kwargs['qlk_rot_flag']).flatten()[0])
                    if os.path.isfile('rot_flag.qlk'):
                        warnings.warn("rot_flag.qlk file found in working directory, using local rotation settings!",UserWarning)
                        rotopt = int(np.genfromtxt('rot_flag.qlk').flatten()[0])

                    collopt = 1
                    fpath = runpath / 'coll_flag.qlk'
                    if 'qlk_coll_flag' in kwargs and isinstance(kwargs['qlk_coll_flag'],(int,float)):
                        collopt = int(kwargs['qlk_coll_flag'])
                    elif 'qlk_coll_flag' in kwargs and kwargs['qlk_coll_flag'] == "from_rundir" and fpath.is_file():
                        collopt = int(np.genfromtxt(str(fpath.resolve())).flatten()[0])
                    elif 'qlk_coll_flag' in kwargs and os.path.isfile(kwargs['qlk_coll_flag']):
                        collopt = int(np.genfromtxt(kwargs['qlk_coll_flag']).flatten()[0])
                    if os.path.isfile('coll_flag.qlk'):
                        warnings.warn("coll_flag.qlk file found in working directory, using local collision settings!",UserWarning)
                        collopt = int(np.genfromtxt('coll_flag.qlk').flatten()[0])

                    collmult = 1.0
                    cpath = runpath / 'collmult.qlk'
                    if 'qlk_collmult' in kwargs and isinstance(kwargs['qlk_collmult'],(int,float)):
                        collmult = float(kwargs['qlk_collmult'])
                    elif 'qlk_collmult' in kwargs and kwargs['qlk_collmult'] == "from_rundir" and cpath.is_file():
                        collmult = float(np.genfromtxt(str(cpath.resolve())).flatten()[0])
                    elif 'qlk_collmult' in kwargs and os.path.isfile(kwargs['qlk_collmult']):
                        collmult = float(np.genfromtxt(kwargs['qlk_collmult']).flatten()[0])
                    if os.path.isfile('collmult.qlk'):
                        warnings.warn("collmult.qlk file found in working directory, using local collision multiplier!",UserWarning)
                        collmult = float(np.genfromtxt('collmult.qlk').flatten()[0])

                    xobj = pdb2qlk.generate_QLK_input(time_scan_dict,kthetarhos=ktrs,set_num_ions=nions,set_rot_flag=rotopt,set_coll_flag=collopt,set_coll_mult=collmult,perform_sanity_checks=True)
                    if xobj is not None:
                        basename = "%s_r%d" % (baserun.strip('/'),int(args.radius * 1000))
                        odir = os.path.join(args.outdir,basename)
                        xobj['phys_meth'] = 1
                        xobj['numsols'] = 2
                        xobj['separateflux'] = 1
                        for item in list(time_scan_dict.keys()):
                            if item in pdb2qlk.QuaLiKizXpoint.Meta.in_args or \
                               item in pdb2qlk.QuaLiKizXpoint.Options.in_args or \
                               item in ['kthetarhos', 'time', 'rho', 'R0'] or \
                               item.startswith('error_'):
                                del time_scan_dict[item]
                        pobj = pdb2qlk.QuaLiKizPlan(scan_dict=time_scan_dict,scan_type='parallel',xpoint_base=xobj)
                        robj = pdb2qlk.QuaLiKizRun(odir,"qualikiz",kwargs['qlk_execpath'],qualikiz_plan=pobj)
                        robj.prepare()
                        robj.generate_input()
                        print("QuaLiKiz input file %s generated!" % (os.path.join(odir,"qualikiz","parameters.json")))

                if code_kwargs["FINALCODE"] == 'gene':
                    warnings.warn("Time trace run constructor not provided for GENE, since it only runs single points",UserWarning)

    else:
        print("JETTO run directory %s not found!" % (runpath))
