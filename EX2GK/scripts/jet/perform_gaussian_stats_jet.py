#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import os
import sys
import pwd
import re
import copy
import argparse
import numpy as np

from EX2GK.tools.general import profile_warper as pwarp, read_EX2GK_output as xread
from EX2GK.jetto_pythontools.jetto_tools import binary as jtools, run as rtools

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Adjust ex-file profiles using Gaussian statistics from GPR, set up JETTO run automatically and submit to queue.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-b', dest='bname', default='', metavar='BASENAME', type=str, action='store', help='Base name for generated directories containing JETTO runs')
    parser.add_argument('-u', dest='uname', default='', metavar='USERNAME', type=str, action='store', help='User ID for default input directory')
    parser.add_argument('-f', dest='foverwrite', default=False, action='store_true', help='Forces overwriting of any exfiles which clash with those in JAMS folder')
    parser.add_argument('-o', dest='oname', default='gaussgen', metavar='OUTNAME', type=str, action='store', help='Output base name of all generated files')
    parser.add_argument('-r', dest='rflag', default=False, action='store_true', help='Flag to toggle automatic submission of generated JETTO runs to queue, via llsubmit')
    parser.add_argument('baserun', nargs=1, type=str, action='store', help='Name of JETTO run directory to serve as reference point for run generation')
    parser.add_argument('infile', nargs=1, type=str, action='store', help='Name of file containing profile information')
    parser.add_argument('exfile', nargs=1, type=str, action='store', help='Input ex-file for modification with new profile')
    parser.add_argument('nruns', nargs=1, type=int, action='store', help='Number of JETTO runs to automatically generate and execute')
    parser.add_argument('qlist', nargs='*', type=str, action='store', help='Quantities to be varied within the automatic profile sampling routine')
    args = parser.parse_args()

    ivec = np.linspace(0.0,1.0,100)
    ivec = np.hstack((0.0,ivec[1::2]))

    qoptions = ['NE','TE','TI','NIMP','ANGF','Q']
    qlist = []
    for qq in args.qlist:
        if qq in qoptions:
            qlist.append(qq)

    pdata = None
    if args.infile and os.path.isfile(args.infile[0]):
        pdata = xread.read_data_file(args.infile[0])
    else:
        raise IOError("Profile data file not found, %s." % (args.infile[0]))
        sys.exit(2)
    if not args.qlist:
        raise TypeError("Unspecified quantity for modification. Please provide using -q.")
        sys.exit(2)

    if pdata is not None and args.nruns[0] > 0:
        imod = 0
        for ii in np.arange(0,args.nruns[0]):
            ndata = copy.deepcopy(pdata)
            rnlist = np.random.normal(size=len(qlist))
            for jj in np.arange(0,len(qlist)):
                plist = [0.5*rnlist[jj],0.9,-rnlist[jj],0.025]
                lb = None
                if re.match(r'^T[IE]$',qlist[jj]):
                    lb = 10.0
                if re.match(r'^N[IE]$',qlist[jj]):
                    lb = 1.0e17
                ndata = pwarp.modify_profile(inputdata=ndata,quantity=qlist[jj],warpname='tanh',parlist=plist,minval=lb,interpvec=ivec,modtype='sigma')
            nn = "%03d" % (ii+imod+1)
            ofile = args.oname + nn
            while os.path.isfile(ofile+'.ex'):
                imod = imod + 1
                nn = "%03d" % (ii+imod+1)
                ofile = args.oname + nn
            status = jtools.modify_exfile(inputdata=ndata,qlist=qlist,exfilename=args.exfile[0],outfile=ofile,globaltag='Monte Carlo Gaussian statistical study')
            if status == 0 and os.path.isfile(ofile+'.ex'):
                status = rtools.make_jetto_run(args.baserun[0],ofile,runname=args.bname,username=args.uname,foverwrite=args.foverwrite,runnumber=(ii+imod),runflag=args.rflag)

            if status != 0:
                print("Error occurred in run setup script. Please check inputs and try again.")
            else:
                print("Run %s successfully generated!" % ('run'+args.bname+nn))
                if not args.rflag:
                    print("   Reminder: Generated runs have not been submitted to the queue.")
