#!/usr/bin/env python

import os
import sys
import datetime
from pathlib import Path
import numpy as np
from scipy.interpolate import interp1d
from EX2GK.tools.general import proctools as ptools
from jetto_tools import binary

def generate_python_structure(extflag=False):
    tic = datetime.datetime.now()
    info = {}
    info['DATABASE NAME'] = {'LABEL': 'Database Name', 'FORM': 'char', 'SECNUM': 2}
    info['USER EX-FILE'] = {'LABEL': 'User EX-file', 'FORM': 'char', 'SECNUM': 2}
    info['USER PRE-MODEX EX-FILE'] = {'LABEL': 'User Pre-Modex EX-file', 'FORM': 'char', 'SECNUM': 2}
    info['SHOT'] = {'LABEL': 'Shot', 'FORM': 'int', 'SECNUM': 3}
    info['DDA NAME'] = {'LABEL': 'DDA Name', 'FORM': 'char', 'SECNUM': 3}
    if not extflag:
        info['XVEC1'] = {'UNITS': None, 'DESC': 'RHO normalised', 'SCSTR': '1.0', 'SCALE': 1.0, 'LABEL': 'XVEC1', 'FORM': 'float', 'SECNUM': 4}
    tsec = 4 if extflag else 5
    info['TVEC1'] = {'UNITS': 'secs', 'DESC': 'TIME', 'SCSTR': '1.0', 'SCALE': 1.0, 'LABEL': 'TVEC1', 'FORM': 'float', 'SECNUM': tsec}
    info['FILE FORMAT'] = {'LABEL': 'File Format', 'FORM': 'char', 'SECNUM': 1, 'FULLNAME': 'Big-endian'}
    info['FILE DESCRIPTION'] = {'LABEL': 'File Description', 'FORM': 'char', 'SECNUM': 1}
    info['VERSION'] = {'LABEL': 'Version', 'FORM': 'char', 'SECNUM': 1}
    info['CREATION_DATE'] = {'LABEL': 'Date', 'FORM': 'char', 'SECNUM': 1}
    info['CREATION_TIME'] = {'LABEL': 'Time', 'FORM': 'char', 'SECNUM': 1}
    info['INFO'] = {'DESC': 'Additional information on data fields'}
    info['SECTIONS'] = {'DESC': 'Labelled section and order within EX-FILE'}
    datablock = 'Traces' if extflag else 'Profiles'
    sections = ['File Header', 'General Info', 'PPF Attributes', 'PPF Base Vectors', datablock]
    fmt = 'b'
    desc = 'EX-FILE'
    version = '1.0'
    cdate = tic.strftime("%d/%m/%Y")
    ctime = tic.strftime("%H:%M:%S")
    dbname = 'JET PPF'
    exname = ''
    pxname = ''
    shot = np.atleast_2d([])
    dda = 'EX'

    struct = {
        "INFO": info,
        "SECTIONS": sections,
        "FILE FORMAT": fmt,
        "FILE DESCRIPTION": desc,
        "VERSION": version,
        "CREATION_DATE": cdate,
        "CREATION_TIME": ctime,
        "DATABASE NAME": dbname,
        "USER EX-FILE": exname,
        "USER PRE-MODEX EX-FILE": pxname,
        "SHOT": shot,
        "DDA NAME": dda
    }
    if not extflag:
        xvec = np.atleast_2d([])
        struct["XVEC1"] = xvec
    tvec = np.atleast_3d([])
    struct["TVEC1"] = tvec

    return struct

def populate_exfile(data, filename=None):

    xdata = generate_python_structure()
    fname = filename if isinstance(filename, str) else "ex2gk_conversion"
    if not fname.endswith(".ex"):
        fname = fname + ".ex"
    xdata["USER EX-FILE"] = fname
    refdda = "EX2GK"

    shot = np.array([data["META_SHOT"]])
    time = np.array([float(data["META_T1"] + data["META_T2"]) / 2.0])
    xvec = data["PD_X"].copy()
    rfunc = interp1d(data["CD_RHOTORN_V"], data["CD_RMAJORO_V"], kind="linear", bounds_error=False, fill_value="extrapolate")
    rvec = rfunc(xvec)
    ravec = (rvec - rvec[0]) / (rvec[-1] - rvec[0])
    pfunc = interp1d(data["CD_RHOTORN_V"], data["CD_PSIPOLN_V"], kind="linear", bounds_error=False, fill_value="extrapolate")
    pvec = pfunc(xvec)
    svec = np.sqrt(pvec)
    tfunc = interp1d(data["CD_RHOTORN_V"], data["CD_PSITOR_V"], kind="linear", bounds_error=False, fill_value="extrapolate")
    tvec = tfunc(xvec)
    jvec = np.sqrt(tvec / (np.pi * data["ZD_BVAC"]))
    jvec[0] = 0.0
    efunc = interp1d(data["ED_PX"], data["ED_P"], kind="linear", bounds_error=False, fill_value="extrapolate")
    evec = efunc(pvec)
    evec[evec < 10.0] = 10.0
    qfunc = interp1d(data["ED_QX"], data["ED_Q"], kind="linear", bounds_error=False, fill_value="extrapolate")
    qvec = qfunc(pvec)
    qvec[qvec < 0.1] = 0.1
    eqprov = data["ED_PPROV"]
    prov = eqprov.split(';')[0].strip()
    sprov = prov.split(',')
    equid = sprov[2].strip()
    eqdda = sprov[1].strip().split('/')[0].strip()
    eqseq = sprov[3].strip().split('.')[1].strip()

    refshape = xvec.shape
    xdata["SHOT"] = np.atleast_2d(shot)
    xdata["XVEC1"] = np.atleast_2d(xvec)
    xdata["TVEC1"] = np.atleast_3d(time)
    xdata["RA"] = np.atleast_2d(ravec)
    if "RA" not in xdata["INFO"]:
        xdata["INFO"]["RA"] = binary.generate_entry_info("RA")
        xdata["INFO"]["RA"]["UID"] = equid
        xdata["INFO"]["RA"]["DDA"] = eqdda
        xdata["INFO"]["RA"]["SEQ"] = eqseq
    xdata["XRHO"] = np.atleast_2d(xvec)
    if "XRHO" not in xdata["INFO"]:
        xdata["INFO"]["XRHO"] = binary.generate_entry_info("XRHO")
        xdata["INFO"]["XRHO"]["UID"] = equid
        xdata["INFO"]["XRHO"]["DDA"] = eqdda
        xdata["INFO"]["XRHO"]["SEQ"] = eqseq
    xdata["PSI"] = np.atleast_2d(pvec)
    if "PSI" not in xdata["INFO"]:
        xdata["INFO"]["PSI"] = binary.generate_entry_info("PSI")
        xdata["INFO"]["PSI"]["UID"] = equid
        xdata["INFO"]["PSI"]["DDA"] = eqdda
        xdata["INFO"]["PSI"]["SEQ"] = eqseq
        xdata["INFO"]["PSI"]["DTYPE"] = 'x-vec'
    xdata["SPSI"] = np.atleast_2d(svec)
    if "SPSI" not in xdata["INFO"]:
        xdata["INFO"]["SPSI"] = binary.generate_entry_info("SPSI")
        xdata["INFO"]["SPSI"]["UID"] = equid
        xdata["INFO"]["SPSI"]["DDA"] = eqdda
        xdata["INFO"]["SPSI"]["SEQ"] = eqseq
        xdata["INFO"]["SPSI"]["DTYPE"] = 'x-vec'
    xdata["R"] = np.atleast_2d(rvec)
    if "R" not in xdata["INFO"]:
        xdata["INFO"]["R"] = binary.generate_entry_info("R")
        xdata["INFO"]["R"]["UID"] = equid
        xdata["INFO"]["R"]["DDA"] = eqdda
        xdata["INFO"]["R"]["SEQ"] = eqseq
    xdata["RHO"] = np.atleast_2d(jvec)
    if "RHO" not in xdata["INFO"]:
        xdata["INFO"]["RHO"] = binary.generate_entry_info("RHO")
        xdata["INFO"]["RHO"]["UID"] = equid
        xdata["INFO"]["RHO"]["DDA"] = eqdda
        xdata["INFO"]["RHO"]["SEQ"] = eqseq
    xdata["PR"] = np.atleast_2d(evec)
    if "PR" not in xdata["INFO"]:
        xdata["INFO"]["PR"] = binary.generate_entry_info("PR")
        xdata["INFO"]["PR"]["UID"] = equid
        xdata["INFO"]["PR"]["DDA"] = eqdda
        xdata["INFO"]["PR"]["SEQ"] = eqseq
        xdata["INFO"]["PR"]["DTYPE"] = 'P'
    if "PD_Q" in data and data["PD_Q"] is not None:
        xdata["Q"] = np.atleast_2d(data["PD_Q"])
        if "Q" not in xdata["INFO"]:
            xdata["INFO"]["Q"] = binary.generate_entry_info("Q")
            xdata["INFO"]["Q"]["UID"] = equid
            xdata["INFO"]["Q"]["DDA"] = eqdda
            xdata["INFO"]["Q"]["SEQ"] = eqseq
            xdata["INFO"]["Q"]["DTYPE"] = 'Q'
    else:
        xdata["Q"] = np.atleast_2d(qvec)
        if "Q" not in xdata["INFO"]:
            xdata["INFO"]["Q"] = binary.generate_entry_info("Q")
            xdata["INFO"]["Q"]["UID"] = equid
            xdata["INFO"]["Q"]["DDA"] = eqdda
            xdata["INFO"]["Q"]["SEQ"] = eqseq
            xdata["INFO"]["Q"]["DTYPE"] = 'Q'
    if "PD_NE" in data and data["PD_NE"] is not None:
        xdata["NE"] = np.atleast_2d(data["PD_NE"])
        if "NE" not in xdata["INFO"]:
            xdata["INFO"]["NE"] = binary.generate_entry_info("NE")
            xdata["INFO"]["NE"]["DDA"] = refdda
    else:
        xdata["NE"] = np.atleast_2d(np.zeros(refshape))
        if "NE" not in xdata["INFO"]:
            xdata["INFO"]["NE"] = binary.generate_entry_info("NE")
            xdata["INFO"]["NE"]["DDA"] = refdda
    if "PD_TE" in data and data["PD_TE"] is not None:
        xdata["TE"] = np.atleast_2d(data["PD_TE"])
        if "TE" not in xdata["INFO"]:
            xdata["INFO"]["TE"] = binary.generate_entry_info("TE")
            xdata["INFO"]["TE"]["DDA"] = refdda
    else:
        xdata["TE"] = np.atleast_2d(np.zeros(refshape))
        if "TE" not in xdata["INFO"]:
            xdata["INFO"]["TE"] = binary.generate_entry_info("TE")
            xdata["INFO"]["TE"]["DDA"] = refdda
    if "PD_TI1" in data and data["PD_TI1"] is not None:
        xdata["TI"] = np.atleast_2d(data["PD_TI1"])
        if "TI" not in xdata["INFO"]:
            xdata["INFO"]["TI"] = binary.generate_entry_info("TI")
            xdata["INFO"]["TI"]["DDA"] = refdda
    else:
        xdata["TI"] = np.atleast_2d(np.zeros(refshape))
        if "TI" not in xdata["INFO"]:
            xdata["INFO"]["TI"] = binary.generate_entry_info("TI")
            xdata["INFO"]["TI"]["DDA"] = refdda
    if "PD_ZEFF" in data and data["PD_ZEFF"] is not None:
        xdata["ZEFF"] = np.atleast_2d(data["PD_ZEFF"])
        if "ZEFF" not in xdata["INFO"]:
            xdata["INFO"]["ZEFF"] = binary.generate_entry_info("ZEFF")
            xdata["INFO"]["ZEFF"]["DDA"] = refdda
    if "PD_AFTOR" in data and data["PD_AFTOR"] is not None:
        xdata["ANGF"] = np.atleast_2d(data["PD_AFTOR"])
        if "ANGF" not in xdata["INFO"]:
            xdata["INFO"]["ANGF"] = binary.generate_entry_info("ANGF")
            xdata["INFO"]["ANGF"]["DDA"] = refdda
    if "PD_NIMP1" in data and data["PD_NIMP1"] is not None:
        xdata["NIMP"] = np.atleast_2d(data["PD_NIMP1"])
        if "NIMP" not in xdata["INFO"]:
            xdata["INFO"]["NIMP"] = binary.generate_entry_info("NIMP")
            xdata["INFO"]["NIMP"]["DDA"] = refdda
    elif "PD_NZ1" in data and data["PD_NZ1"] is not None:
        xdata["NIMP"] = np.atleast_2d(data["PD_NZ1"])
        if "NIMP" not in xdata["INFO"]:
            xdata["INFO"]["NIMP"] = binary.generate_entry_info("NIMP")
            xdata["INFO"]["NIMP"]["DDA"] = refdda
    if "PD_NIMP2" in data and data["PD_NIMP2"] is not None:
        xdata["NIMP2"] = np.atleast_2d(data["PD_NIMP2"])
        if "NIMP2" not in xdata["INFO"]:
            xdata["INFO"]["NIMP2"] = binary.generate_entry_info("NIMP2")
            xdata["INFO"]["NIMP2"]["DDA"] = refdda
    elif "PD_NZ2" in data and data["PD_NZ2"] is not None:
        xdata["NIMP2"] = np.atleast_2d(data["PD_NZ2"])
        if "NIMP2" not in xdata["INFO"]:
            xdata["INFO"]["NIMP2"] = binary.generate_entry_info("NIMP2")
            xdata["INFO"]["NIMP2"]["DDA"] = refdda
    if "PD_NIMP3" in data and data["PD_NIMP3"] is not None:
        xdata["NIMP3"] = np.atleast_2d(data["PD_NIMP3"])
        if "NIMP3" not in xdata["INFO"]:
            xdata["INFO"]["NIMP3"] = binary.generate_entry_info("NIMP3")
            xdata["INFO"]["NIMP3"]["DDA"] = refdda
    if "PD_NIMP4" in data and data["PD_NIMP4"] is not None:
        xdata["NIMP4"] = np.atleast_2d(data["PD_NIMP4"])
        if "NIMP4" not in xdata["INFO"]:
            xdata["INFO"]["NIMP4"] = binary.generate_entry_info("NIMP4")
            xdata["INFO"]["NIMP4"]["DDA"] = refdda
    if "PD_NIMP5" in data and data["PD_NIMP5"] is not None:
        xdata["NIMP5"] = np.atleast_2d(data["PD_NIMP5"])
        if "NIMP5" not in xdata["INFO"]:
            xdata["INFO"]["NIMP5"] = binary.generate_entry_info("NIMP5")
            xdata["INFO"]["NIMP5"]["DDA"] = refdda
    if "PD_STRAD" in data and data["PD_STRAD"] is not None:
        xdata["PRAD"] = np.atleast_2d(data["PD_STRAD"])
        if "PRAD" not in xdata["INFO"]:
            xdata["INFO"]["PRAD"] = binary.generate_entry_info("PRAD")
            xdata["INFO"]["PRAD"]["DDA"] = refdda
    if "PD_STENBI" in data and data["PD_STENBI"] is not None:
        xdata["QNBE"] = np.atleast_2d(data["PD_STENBI"])
        if "QNBE" not in xdata["INFO"]:
            xdata["INFO"]["QNBE"] = binary.generate_entry_info("QNBE")
            xdata["INFO"]["QNBE"]["DDA"] = refdda
    if "PD_STINBI" in data and data["PD_STINBI"] is not None:
        xdata["QNBI"] = np.atleast_2d(data["PD_STINBI"])
        if "QNBI" not in xdata["INFO"]:
            xdata["INFO"]["QNBI"] = binary.generate_entry_info("QNBI")
            xdata["INFO"]["QNBI"]["DDA"] = refdda
    if "PD_SNI1NBI" in data and data["PD_SNI1NBI"] is not None:
        xdata["SB1"] = np.atleast_2d(data["PD_SNI1NBI"])
        if "SB1" not in xdata["INFO"]:
            xdata["INFO"]["SB1"] = binary.generate_entry_info("SB1")
            xdata["INFO"]["SB1"]["DDA"] = refdda
    elif "PD_SNINBI" in data and data["PD_SNINBI"] is not None:
        xdata["SB1"] = np.atleast_2d(data["PD_SNINBI"])
        if "SB1" not in xdata["INFO"]:
            xdata["INFO"]["SB1"] = binary.generate_entry_info("SB1")
            xdata["INFO"]["SB1"]["DDA"] = refdda
    if "PD_SNI2NBI" in data and data["PD_SNI2NBI"] is not None:
        xdata["SB2"] = np.atleast_2d(data["PD_SNI2NBI"])
        if "SB2" not in xdata["INFO"]:
            xdata["INFO"]["SB2"] = binary.generate_entry_info("SB2")
            xdata["INFO"]["SB2"]["DDA"] = refdda
    if "PD_JNBI" in data and data["PD_JNBI"] is not None:
        xdata["JZNB"] = np.atleast_2d(data["PD_JNBI"])
        if "JZNB" not in xdata["INFO"]:
            xdata["INFO"]["JZNB"] = binary.generate_entry_info("JZNB")
            xdata["INFO"]["JZNB"]["DDA"] = refdda
    if "PD_NFI1NBI" in data and data["PD_NFI1NBI"] is not None:
        xdata["NB"] = np.atleast_2d(data["PD_NFI1NBI"])
        if "PD_NFI2NBI" in data and data["PD_NFI2NBI"] is not None:
            xdata["NB"] += np.atleast_2d(data["PD_NFI2NBI"])
        if "NB" not in xdata["INFO"]:
            xdata["INFO"]["NB"] = binary.generate_entry_info("NB")
            xdata["INFO"]["NB"]["DDA"] = refdda
    elif "PD_NFI2NBI" in data and data["PD_NFI2NBI"] is not None:
        xdata["NB"] = np.atleast_2d(data["PD_NFI2NBI"])
        if "NB" not in xdata["INFO"]:
            xdata["INFO"]["NB"] = binary.generate_entry_info("NB")
            xdata["INFO"]["NB"]["DDA"] = refdda
    if "PD_WFI1NBI" in data and data["PD_WFI1NBI"] is not None:
        xdata["WFNB"] = np.atleast_2d(data["PD_WFI1NBI"])
        if "PD_WFI2NBI" in data and data["PD_WFI2NBI"] is not None:
            xdata["WFNB"] += np.atleast_2d(data["PD_WFI2NBI"])
        if "WFNB" not in xdata["INFO"]:
            xdata["INFO"]["WFNB"] = binary.generate_entry_info("WFNB")
            xdata["INFO"]["WFNB"]["DDA"] = refdda
    elif "PD_WFI2NBI" in data and data["PD_WFI2NBI"] is not None:
        xdata["WFNB"] = np.atleast_2d(data["PD_WFI2NBI"])
        if "WFNB" not in xdata["INFO"]:
            xdata["INFO"]["WFNB"] = binary.generate_entry_info("WFNB")
            xdata["INFO"]["WFNB"]["DDA"] = refdda
    if "PD_TAUNBI" in data and data["PD_TAUNBI"] is not None:
        xdata["TORQ"] = np.atleast_2d(data["PD_TAUNBI"])
        if "TORQ" not in xdata["INFO"]:
            xdata["INFO"]["TORQ"] = binary.generate_entry_info("TORQ")
            xdata["INFO"]["TORQ"]["DDA"] = refdda
    if "PD_STEICRH" in data and data["PD_STEICRH"] is not None:
        xdata["QRFE"] = np.atleast_2d(data["PD_STEICRH"])
        if "QRFE" not in xdata["INFO"]:
            xdata["INFO"]["QRFE"] = binary.generate_entry_info("QRFE")
            xdata["INFO"]["QRFE"]["DDA"] = refdda
    if "PD_STIICRH" in data and data["PD_STIICRH"] is not None:
        xdata["QRFI"] = np.atleast_2d(data["PD_STIICRH"])
        if "QRFI" not in xdata["INFO"]:
            xdata["INFO"]["QRFI"] = binary.generate_entry_info("QRFI")
            xdata["INFO"]["QRFI"]["DDA"] = refdda
    if "PD_NFI1ICRH" in data and data["PD_NFI1ICRH"] is not None:
        xdata["RF"] = np.atleast_2d(data["PD_NFI1ICRH"])
        if "PD_NFI2ICRH" in data and data["PD_NFI2ICRH"] is not None:
            xdata["RF"] += np.atleast_2d(data["PD_NFI2ICRH"])
        if "RF" not in xdata["INFO"]:
            xdata["INFO"]["RF"] = binary.generate_entry_info("RF")
            xdata["INFO"]["RF"]["DDA"] = refdda
    elif "PD_NFI2ICRH" in data and data["PD_NFI2ICRH"] is not None:
        xdata["RF"] = np.atleast_2d(data["PD_NFI2ICRH"])
        if "RF" not in xdata["INFO"]:
            xdata["INFO"]["RF"] = binary.generate_entry_info("RF")
            xdata["INFO"]["RF"]["DDA"] = refdda
    if "PD_WFI1ICRH" in data and data["PD_WFI1ICRH"] is not None:
        xdata["WFRF"] = np.atleast_2d(data["PD_WFI1ICRH"])
        if "PD_WFI2ICRH" in data and data["PD_WFI2ICRH"] is not None:
            xdata["WFRF"] += np.atleast_2d(data["PD_WFI2ICRH"])
        if "WFRF" not in xdata["INFO"]:
            xdata["INFO"]["WFRF"] = binary.generate_entry_info("WFRF")
            xdata["INFO"]["WFRF"]["DDA"] = refdda
    elif "PD_WFI2ICRH" in data and data["PD_WFI2ICRH"] is not None:
        xdata["WFRF"] = np.atleast_2d(data["PD_WFI2ICRH"])
        if "WFRF" not in xdata["INFO"]:
            xdata["INFO"]["WFRF"] = binary.generate_entry_info("WFRF")
            xdata["INFO"]["WFRF"]["DDA"] = refdda

    return xdata

def populate_extfile(data, filename=None):

    tdata = generate_python_structure(extflag=True)
    fname = filename if isinstance(filename, str) else "ex2gk_conversion"
    if not fname.endswith(".ext"):
        fname = fname + ".ext"
    tdata["USER EX-FILE"] = fname

    time = np.array([float(data["META_T1"] + data["META_T2"]) / 2.0])
    iuid = 'JETPPF'
    idda = 'EFIT'
    iseq = '0'
    idtp = 'XIP'

    refshape = time.shape
    tdata["TVEC1"] = np.atleast_2d([time])
    if "ZD_IPLA" in data and data["ZD_IPLA"] is not None:
        tdata["CUR"] = np.atleast_2d([np.abs(data["ZD_IPLA"])])
        iprov = data["ZD_IPLAPROV"]
        prov = iprov.split(';')[0].strip()
        sprov = prov.split(',')
        iuid = sprov[2].strip()
        idda = sprov[1].strip().split('/')[0].strip()
        idtp = sprov[1].strip().split('/')[1].strip()
        iseq = sprov[3].strip().split('.')[1].strip()
    else:
        tdata["CUR"] = np.atleast_2d(np.ones(refshape))
    if "CUR" not in tdata["INFO"]:
        tdata["INFO"]["CUR"] = generate_entry_info("CUR")
        tdata["INFO"]["CUR"]["UID"] = iuid
        tdata["INFO"]["CUR"]["DDA"] = idda
        tdata["INFO"]["CUR"]["SEQ"] = iseq
        tdata["INFO"]["CUR"]["DTYPE"] = idtp

    return tdata

def create_jetto_input_from_ex2gk(data, filepath='./', filename=None):

    fpath = Path(filepath)
    if not fpath.is_dir():
        fpath.mkdir(parents=True)

    xdata = None
    tdata = None
    if isinstance(data, dict) and "META_SHOT" in data:
        xdata = populate_exfile(data, filename)
        tdata = populate_extfile(data, filename)

    xstatus = -1
    tstatus = -1
    if xdata is not None:
        target_dir = str(fpath.resolve())
        target_file = xdata["USER EX-FILE"]
        xdata["USER EX-FILE"] = target_dir + "/" + target_file
        xdata["USER PRE-MODEX EX-FILE"] = target_dir + "/dummy.ex"
        target = fpath / target_file
        xstatus = binary.write_binary_exfile(xdata, str(target))
    if tdata is not None:
        target_dir = str(fpath.resolve())
        target_file = tdata["USER EX-FILE"]
        tdata["USER EX-FILE"] = target_dir + "/" + target_file
        tdata["USER PRE-MODEX EX-FILE"] = target_dir + "/dummy.ext"
        target = fpath / target_file
        tstatus = binary.write_binary_exfile(tdata, str(target))

    if xstatus != 0 or tstatus != 0:
        print("Ex-file generation failed!")

def create_jetto_input_from_pickle(pickle_filepath, filepath='./', filename=None):

    ppath = Path(pickle_filepath)
    if ppath.is_file():
        data = ptools.unpickle_this(str(ppath.resolve()))
        if "META_SHOT" in data:
            create_jetto_input_from_ex2gk(data, filepath, filename)
        else:
            for shot in data:
                for tw in data[shot]:
                    fname = filename
                    if fname is None:
                        fname = "ex2gk_conversion" + ("_%d" % (tw["META_SHOT"])) + ("_%d" % (int(tw["META_T1"] * 10000))) + ("_%d" % (int(tw["META_T2"] * 10000)))
                    create_jetto_input_from_ex2gk(tw, filepath, fname)
    else:
        print("Pickle file is not readable!")

def main(argv):
    if len(argv) > 1:
        name = argv[2] if len(argv) > 2 else None
        create_jetto_input_from_pickle(argv[1], filename=name)

if __name__ == "__main__":
    main(sys.argv)
