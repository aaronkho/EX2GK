#!/usr/bin/env python
##!/usr/local/depot/Python-3.5.1/bin/python3

# This file is completely malleable, it is NOT an example of a standard call, though can be used as inspiration

# Required imports
import os
import sys
import argparse
import re
import copy
import numpy as np
import subprocess

from EX2GK.tools.general import classes, fmt_converter as fmtconv

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Converts EX2GK merged .p file into format consistent with JETPEAK, saves in .mat file.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-f', '--force', dest='foverwrite', default=False, action='store_true', help='Flag to force overwriting of previously saved data')
    parser.add_argument('-l', '--order', dest='lfile', metavar='ORDERFILE', default=None, type=str, action='store', help='Name of .txt file containing JETPEAK index order information')
    parser.add_argument('pfile', metavar='PICKLE', type=str, action='store', help='Name of .p file containing profile information to add to JETPEAK')
    parser.add_argument('ofile', metavar='OUTNAME', type=str, action='store', help='Name of output file containing merged data')
    args = parser.parse_args()

    if os.path.isfile(args.ofile) and not args.foverwrite:
        raise IOError("Requested output file already exists. Script aborted.")

    pfile = None
    lfile = None
    if (args.pfile.endswith(".p") or args.pfile.endswith(".pkl")) and os.path.exists(args.pfile):
        pfile = args.pfile
    if args.lfile is not None and args.lfile.endswith(".txt") and os.path.exists(args.lfile):
        lfile = args.lfile

    status = False
    if pfile:

        order = None
        if lfile:
            data = np.atleast_2d(np.genfromtxt(lfile))
            if data.shape[1] >= 3:
                data[:,1] = data[:,1] * 1.0e4
                data[:,2] = data[:,2] * 1.0e4
                order = data[:,:3].tolist()

        sc = classes.EX2GKShotCollection.importFromPickle(pfile)
        mat = sc.primeForJETPEAKExport(ordered=order, tolerance=10)
        if mat:
            fmtconv.obj2mat(mat, args.ofile)
            status = True

    if status:
        print("Successful!")

