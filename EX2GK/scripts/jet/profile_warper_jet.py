#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

# Required imports
import os
import sys
import re
import pwd
import copy
import argparse
import numpy as np

from EX2GK.tools.general import profile_warper as pwarp, read_EX2GK_output as xread
from EX2GK.jetto_pythontools.jetto_tools import binary as jtools

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Extract profile data and modify it using error bounds.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-q', '--quantity', dest='quantity', default='', type=str, action='store', choices=['NE','TE','TI','NIMP','ANGF','Q'], help='Profile in file to be modified, if present')
    parser.add_argument('-v', '--visualize', dest='pflag', default=False, action='store_true', help='Flag plotting of the resulting profile and mod profile')
    parser.add_argument('-f', '--function', dest='modname', default='tanh', type=str, action='store', help='Modification function code name')
    parser.add_argument('-i', '--interpolate', dest='ifile', default='None', type=str, action='store', help='Optional input file for output grid of profile, interpolated')
    parser.add_argument('-o', '--output', dest='ofile', default='./modded_profile.txt', type=str, action='store', help='Name of output file where modified profile will be stored in ASCII')
    parser.add_argument('-m', '--mode', dest='mtype', default='sigma', type=str, action='store', choices=['shift','scale','sigma'], help='Modification mode specification')
    parser.add_argument('-b', '--minbound', dest='minval', default=None, type=float, action='store', help='Optional minimum value for new profile')
    parser.add_argument('-x', '--exfile', dest='exfile', default='None', type=str, action='store', help='Optional input ex-file for modification with new profile')
    parser.add_argument('-l', '--label', dest='ltag', default='Python profile warping tool', type=str, action='store', help='Optional label to be written in ex-file for data providence')
    parser.add_argument('infile', type=str, action='store', help='Name of file containing profile information')
    parser.add_argument('parlist', nargs='*', type=float, action='store', help='Optional parameter list for modification function')
    args = parser.parse_args()

    pdata = None
    if args.infile and os.path.isfile(args.infile):
        pdata = xread.read_data_file(args.infile)
    else:
        raise IOError("Profile data file not found, %s." % (args.infile))
        sys.exit(2)
    if not args.quantity:
        raise TypeError("Unspecified quantity for modification. Please provide using -q.")
        sys.exit(2)

    idata = None
    if args.ifile and os.path.isfile(args.ifile):
        idata = np.genfromtxt(args.ifile).flatten()
    exfile = None
    if not re.match(r'^None$',args.exfile,flags=re.IGNORECASE):
        exfile = args.exfile

    status = 1
    if pdata is not None:
        mdata = pwarp.modify_profile(inputdata=pdata,quantity=args.quantity,warpname=args.modname,parlist=args.parlist,minval=args.minval,interpvec=idata,outfile=args.ofile,modtype=args.mtype,plotflag=args.pflag)
        if exfile is not None:
            status = jtools.modify_exfile(inputdata=mdata,qlist=[args.quantity],exfilename=exfile,outfile=args.ofile,globaltag=args.ltag)
        elif mdata is not None:
            status = 0

    if status != 0:
        print("Error occurred in profile modification script. Please check inputs and try again.")

