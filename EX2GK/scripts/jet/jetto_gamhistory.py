#!/usr/bin/env python3

import os
import pwd
import numpy as np
import pandas as pd
import re
from pathlib import Path
import copy
import argparse

import matplotlib
import matplotlib.pyplot as plt

from EX2GK.tools.general import proctools as ptools
from EX2GK.tools.jet import jetto_adapter
from EX2GK.code_adapters.qualikiz import adapter as qlk_adapter
from EX2GK.jetto_pythontools.jetto_tools import binary, qualikiz
#from jetto_tools import binary, qualikiz


# Common numerical data types, for ease of type-checking
np_itypes = (np.int8, np.int16, np.int32, np.int64)
np_utypes = (np.uint8, np.uint16, np.uint32, np.uint64)
np_ftypes = (np.float16, np.float32, np.float64)

int_types = (int, np_itypes, np_utypes)
float_types = (float, np_ftypes)
number_types = (float_types, int_types)
array_types = (list, tuple, np.ndarray)


def extract_jetto_geometry_data(rundir, profile_option=1):

    geomdata = {}
    rpath = Path(rundir) if isinstance(rundir, str) else rundir
    if rpath.is_dir():

        jspdatalist = jetto_adapter.import_from_JSP(
            str(rpath.resolve()),
            None,
            profopt=profile_option
        )
        if len(jspdatalist) <= 0:
            raise IOError("Issue occurred with reading JSP data")

        for tstep in range(len(jspdatalist)):

            jspdatalist[tstep].postProcess()
            jspdatalist[tstep].updateTimestamp()
            jspdatalist[tstep] = qlk_adapter.calc_code_inputs(jspdatalist[tstep])

            if "jt" not in geomdata:
                geomdata["jt"] = np.array([jspdatalist[tstep]["META"]["T1"]])
            else:
                geomdata["jt"] = np.vstack((geomdata["jt"], jspdatalist[tstep]["META"]["T1"]))

            if "jrho" not in geomdata:
                cs = jspdatalist[tstep]["META"]["CSO"]
                csp = jspdatalist[tstep]["META"]["CSOP"]
                rhovec, jac, err = jspdatalist[tstep]["CD"].convert(jspdatalist[tstep]["PD"]["X"][""], cs, "RHOTORN", csp, csp)
                geomdata["jrho"] = rhovec.copy()
            else:
                cs = jspdatalist[tstep]["META"]["CSO"]
                csp = jspdatalist[tstep]["META"]["CSOP"]
                rhovec, jac, err = jspdatalist[tstep]["CD"].convert(jspdatalist[tstep]["PD"]["X"][""], cs, "RHOTORN", csp, csp)
                geomdata["jrho"] = np.vstack((geomdata["jrho"], rhovec))

            if "jrzero" not in geomdata:
                geomdata["jrzero"] = np.array([jspdatalist[tstep]["ZD"]["OUTQLK_RZERO"][""]])
            else:
                geomdata["jrzero"] = np.vstack((geomdata["jrzero"], jspdatalist[tstep]["ZD"]["OUTQLK_RZERO"][""]))

    return geomdata


def extract_jetto_plasma_data(rundir, profile_option=1):

    plasdata = {}
    rpath = Path(rundir) if isinstance(rundir, str) else rundir
    if rpath.is_dir():

        jspdatalist = jetto_adapter.import_from_JSP(
            str(rpath.resolve()),
            None,
            profopt=profile_option
        )
        if len(jspdatalist) <= 0:
            raise IOError("Issue occurred with reading JSP data")

        for tstep in range(len(jspdatalist)):

            jspdatalist[tstep].postProcess()
            jspdatalist[tstep].updateTimestamp()

            stag = "NE"
            if stag in jspdatalist[tstep]["PD"]:
                if stag not in plasdata:
                    plasdata[stag.lower()] = copy.deepcopy(jspdatalist[tstep]["PD"][stag][""])
                else:
                    plasdata[stag.lower()] = np.vstack((plasdata[stag.lower()], jspdatalist[tstep]["PD"][stag][""]))

            for ii in range(jspdatalist[tstep]["META"]["NUMI"]):
                stag = "NI%d" % (ii+1)
                if stag in jspdatalist[tstep]["PD"]:
                    if stag not in plasdata:
                        plasdata[stag.lower()] = copy.deepcopy(jspdatalist[tstep]["PD"][stag][""])
                    else:
                        plasdata[stag.lower()] = np.vstack((plasdata[stag.lower()], jspdatalist[tstep]["PD"][stag][""]))

            for ii in range(jspdatalist[tstep]["META"]["NUMIMP"]):
                stag = "NIMP%d" % (ii+1)
                if stag in jspdatalist[tstep]["PD"]:
                    if stag not in plasdata:
                        plasdata[stag.lower()] = copy.deepcopy(jspdatalist[tstep]["PD"][stag][""])
                    else:
                        plasdata[stag.lower()] = np.vstack((plasdata[stag.lower()], jspdatalist[tstep]["PD"][stag][""]))

    return plasdata


def extract_jetto_gradient_data(rundir, profile_option=1):

    graddata = {}
    rpath = Path(rundir) if isinstance(rundir, str) else rundir
    if rpath.is_dir():

        jspdatalist = jetto_adapter.import_from_JSP(
            str(rpath.resolve()),
            None,
            profopt=profile_option
        )
        if len(jspdatalist) <= 0:
            raise IOError("Issue occurred with reading JSP data")

        for tstep in range(len(jspdatalist)):

            jspdatalist[tstep].postProcess()
            jspdatalist[tstep].updateTimestamp()
            jspdatalist[tstep] = qlk_adapter.calc_code_inputs(jspdatalist[tstep])

            stag = "ANE"
            if "OUTQLK_"+stag in jspdatalist[tstep]["PD"]:
                if stag not in graddata:
                    graddata[stag.lower()] = copy.deepcopy(jspdatalist[tstep]["PD"]["OUTQLK_"+stag][""])
                else:
                    graddata[stag.lower()] = np.vstack((graddata[stag.lower()], jspdatalist[tstep]["PD"]["OUTQLK_"+stag][""]))

            for ii in range(jspdatalist[tstep]["META"]["NUMI"]):
                stag = "ANI%d" % (ii+1)
                if "OUTQLK_"+stag in jspdatalist[tstep]["PD"]:
                    if stag not in graddata:
                        graddata[stag.lower()] = copy.deepcopy(jspdatalist[tstep]["PD"]["OUTQLK_"+stag][""])
                    else:
                        graddata[stag.lower()] = np.vstack((graddata[stag.lower()], jspdatalist[tstep]["PD"]["OUTQLK_"+stag][""]))

            for ii in range(jspdatalist[tstep]["META"]["NUMIMP"]):
                stag = "ANIMP%d" % (ii+1)
                if "OUTQLK_"+stag in jspdatalist[tstep]["PD"]:
                    if stag not in graddata:
                        graddata[stag.lower()] = copy.deepcopy(jspdatalist[tstep]["PD"]["OUTQLK_"+stag][""])
                    else:
                        graddata[stag.lower()] = np.vstack((graddata[stag.lower()], jspdatalist[tstep]["PD"]["OUTQLK_"+stag][""]))

    return graddata


def gamhistory_raw_pipeline(rundirpath, nc_flag=False, profile_option=0, fdebug=False):

    tdata = None
    if isinstance(rundirpath, str):

        rpath = Path(rundirpath)
        if rpath.is_dir():

            qdata = qualikiz.read_qualikiz_gamhistory_data(str(rpath.resolve()))
            qdata = qualikiz.remove_duplicate_times(qdata)
            tdata = qualikiz.separate_gamhistory_species(qdata)
            if "rzero" not in qdata:
                qdata.update(extract_jetto_geometry_data(str(rpath.resolve()), profile_option=profile_option))
            if "ptopt" in tdata and tdata["ptopt"] != 0:
                tdata = qualikiz.transfer_transport_quantities(tdata)
            if nc_flag:
                tdata.update(qualikiz.read_jetto_neoclassical_data(str(rpath.resolve()), numions=tdata["numi"], numimps=tdata["numimp"]))
            if "rlne" not in tdata or "rlni1" not in tdata:
                tdata.update(extract_jetto_gradient_data(str(rpath.resolve()), profile_option=profile_option))
            if "ne" not in tdata or "ni1" not in tdata:
                tdata.update(extract_jetto_plasma_data(str(rpath.resolve()), profile_option=profile_option))

        if fdebug:
            print(tdata.keys())

    return tdata


def plot_single_density_peaking(datadict, ytag, xindex=None, ptag="plot", outdir=None, legend=None, fancy=False, time_offset=None, y_exponent=None, x_limits=None, y_limits=None, titletag=None):

    if isinstance(datadict, dict) and isinstance(ytag, str):

        f2d = True if not isinstance(xindex, int_types) else False

        toff = float(time_offset) if isinstance(time_offset, number_types) else 0.0
        exp = int(y_exponent) if isinstance(y_exponent, number_types) else 0
        scale = np.power(10.0, exp)

        #fancy = True if isinstance(ftag, str) else None
        fsize = 24
        if fancy:
            matplotlib.rc('font', family='serif')
            matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=fsize)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
        lfsize = fsize - 6

        opath = Path(outdir) if outdir is not None else Path("./")
        if not opath.exists():
            opath.mkdir(parents=True)
        if not opath.is_dir():
            raise IOError("Target output directory not found!")

        llist = []
        if isinstance(legend, (tuple, list, np.ndarray)):
            for jj in range(len(legend)):
                if isinstance(legend[jj], str):
                    llist.append(legend[jj])
                else:
                    llist.append("?")

        xrlist = None
        yrlist = None
        if isinstance(x_limits,(list,tuple)) and len(x_limits) == 2:
            xrlist = tuple(x_limits)
        if isinstance(y_limits,(list,tuple)) and len(y_limits) == 2:
            yrlist = tuple(y_limits)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        lst = ['-', '--', ':', '-.']
        titlex = 0.0
        ii = 0
        for key, val in datadict.items():
            if not f2d:
                dmask = (val.index.to_frame()["rho_idx"].values == xindex)
                data = val.loc[dmask]
                ftag = r'e'
                if not ytag.startswith("e") and "a"+ytag in data:
                    (stag, sa, sz) = ptools.define_ion_species(a=np.mean(data["a"+ytag]))
                    ftag = r'\text{' + stag + r'}'
                if "rho" in data and "t" in data:
                    xx = data["t"].values + toff
                    itemtag = ""
                    if len(datadict) > 1:
                        itemtag = " " + llist[ii] if len(llist) > ii else " %d" % (ii+1)
                    if "rln"+ytag in data:
                        yy = data["rln"+ytag].values / scale
                        glabel = r'$\left(R/L_n\right)_'+ftag+'$ TCI'+itemtag if fancy else 'R/L_n TCI'+itemtag
                        ax.plot(xx, yy, c='g', ls=lst[ii], label=glabel)
#                    if plot_jsp and "t_jetto" in data and "an"+ytag in data:
#                        jxx = data["t_jetto"].values + toff
#                        yy = data["an"+ytag].values / scale
#                        blabel = r'$\left(R/L_n\right)_'+ftag+'$ JSP '+itemtag if fancy else 'R/L_n JSP'+itemtag
#                        ax.plot(jxx, yy, c='b', ls=lst[ii], label=blabel)
                    rvdflag = True if "rvd"+ytag in data and np.sum(data["rvd"+ytag]) != np.mean(data["rvd"+ytag]) else False
                    if rvdflag and "rvd"+ytag in data:
                        yy = data["rvd"+ytag].values / scale
                        rlabel = r'$\left(-RV/D\right)_'+ftag+r'$'+itemtag if fancy else '-RV/D'+itemtag
                        ax.plot(xx, yy, c='r', ls=lst[ii], label=rlabel)
                    if rvdflag and "rvdt"+ytag in data:
                        yy = data["rvdt"+ytag].values / scale
                        klabel = r'$\left(-RV/D\right)_'+ftag+'$'+itemtag+' + NC' if fancy else '-RV/D'+itemtag+' + NC'
                        ax.plot(xx, yy, c='k', ls=lst[ii], label=klabel)
                    titlex += float(np.mean(data["rho"].values))
                    ii += 1

        (xmin, xmax) = ax.get_xlim()
        (ymin, ymax) = ax.get_ylim()
        if xrlist is not None:
            if xrlist[0] is not None:
                xmin = xrlist[0]
            if xrlist[1] is not None:
                xmax = xrlist[1]
        if yrlist is not None:
            if yrlist[0] is not None:
                ymin = yrlist[0]
            if yrlist[1] is not None:
                ymax = yrlist[1]
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])

        if ii > 0 and not f2d:
            titlex = titlex / float(ii) if not isinstance(titletag,(float,int)) else float(titletag)
            tlabel = r'$\rho$ = %6.4f' % (titlex) if fancy else "rho = %6.4f" % (titlex)
            ax.set_title(tlabel)
        xtag = r'$t$ [s]' if fancy else "time (s)"
        ax.set_xlabel(xtag)

        sctag = r'-'
        if exp != 0:
            extag = "%d" % (exp)
            sctag = r'$\times 10^{' + extag + r'}$'
        if exp == 3:
            sctag = r'k'
        if exp == 6:
            sctag = r'M'
        if exp == 9:
            sctag = r'G'
        if exp == 12:
            sctag = r'T'
        ytag = r'$-R \left(\nabla n / n\right)_' + ftag + r'$ [' + sctag + r']' if fancy else "norm_grad_dens"
        ax.set_ylabel(ytag)
        if len(datadict) > 1:
            ax.legend(loc='best',prop={'size':lfsize})
        pfmt = "pdf" if fancy else "png"
        oname = ("%s_x%03d." % (ptag, int(titlex * 100))) + pfmt
        if ii > 0:
            fig.savefig(str(opath / oname), format=pfmt, dpi=300, bbox_inches="tight")
        else:
            print("Species %s not found in any of the provided run directories." % (ytag))
        plt.close(fig)


def plot_species_density_peaking(datadict, ivec, electron=False, nion=-1, nimp=-1, outdir=None, fancy=False, time_offset=None, y_exponent=None, x_limits=None, y_limits=None, titletags=None):

    if isinstance(ivec, array_types):

        if electron is False and nion < 0 and nimp < 0:
            print("No species specified for plotting in density peaking analysis!")

        if electron:
            itag = "e"
            ptag = "electron_density_peaking"
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_density_peaking(datadict, ii, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nion >= 0:
            itag = "i%d" % (nion+1)
            ptag = "main_ion%d_density_peaking" % (nion+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_density_peaking(datadict, ii, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nimp >= 0:
            itag = "imp%d" % (nimp+1)
            ptag = "impurity_ion%d_density_peaking" % (nimp+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_density_peaking(datadict, ii, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)


def plot_single_transport_coefficient(datadict, ytag, stag, xindex=None, ptag="plot", outdir=None, legend=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletag=None):

    varlist = ["ef", "pf", "x", "d", "v", "xeff", "deff"]

    if isinstance(datadict, dict) and ytag in varlist and isinstance(stag, str):

        f2d = True if not isinstance(xindex, int_types) else False

        toff = float(time_offset) if isinstance(time_offset, number_types) else 0.0
        exp = int(y_exponent) if isinstance(y_exponent, number_types) else 0
        scale = np.power(10.0, exp)

        fsize = 24
        if fancy:
            matplotlib.rc('font', family='serif')
            matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=fsize)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
        lfsize = fsize - 6

        opath = Path(outdir) if outdir is not None else Path("./")
        if not opath.exists():
            opath.mkdir(parents=True)
        if not opath.is_dir():
            raise IOError("Target output directory not found!")

        llist = []
        if isinstance(legend, (tuple, list, np.ndarray)):
            for jj in range(len(legend)):
                if isinstance(legend[jj], str):
                    llist.append(legend[jj])
                else:
                    llist.append("?")

        xrlist = None
        yrlist = None
        if isinstance(x_limits,(list,tuple)) and len(x_limits) == 2:
            xrlist = tuple(x_limits)
        if isinstance(y_limits,(list,tuple)) and len(y_limits) == 2:
            yrlist = tuple(y_limits)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        cst = ['b', 'r', 'g', 'y', 'm', 'c']
        lst = ['-', '--', ':', '-.']
        titlex = 0.0
        ii = 0
        ftag = r'e'

        for key, val in datadict.items():
            if not stag.startswith("e") and "a"+stag in val:
                (nstag, sa, sz) = ptools.define_ion_species(a=np.mean(val["a"+stag]))
                ftag = r'\text{' + nstag + r'}' if nstag not in ["H", "D", "T"] else r'i'
            if not f2d:
                dtag = "rho_idx" if not finvert else "t_idx"
                dmask = (val.index.to_frame()[dtag].values == xindex)
                data = val.loc[dmask]
                if "rho" in data and "t" in data:
                    xx = data["t"].values + toff if not finvert else data["rho"].values
                    zz = float(np.mean(data["rho"].values)) if not finvert else float(np.mean(data["t"].values) + toff)
                    itemtag = ""
                    if len(datadict) > 1:
                        itemtag = " " + llist[ii] if len(llist) > ii else " %d" % (ii+1)
                    if ytag+"a"+stag in data:
                        yy = data[ytag+"a"+stag].values / scale
                        glabel = r'Turbulent' + itemtag
                        ax.plot(xx, yy, c=cst[ii], ls='--', label=glabel)
                    if ytag+"nc"+stag in data:
                        yy = data[ytag+"nc"+stag].values / scale
                        nlabel = r'Neoclassical' + itemtag
                        ax.plot(xx, yy, c=cst[ii], ls=':', label=nlabel)
                    if ytag+"t"+stag in data:
                        yy = data[ytag+"t"+stag].values / scale
                        klabel = r'Total' + itemtag
                        ax.plot(xx, yy, c=cst[ii], ls='-', label=klabel)
                    titlex += zz
                    ii += 1
            else:
                nx = len(val.index.levels[0])
                ny = len(val.index.levels[1])
                xx = val["t"].values.reshape(nx, ny) + toff
                yy = val["rho"].values.reshape(nx, ny)
                zz = val[ytag+"a"+stag].values.reshape(nx, ny)[:-1,:-1] / scale
                #klabel = r'Turbulent' + itemtag
                zplot = ax.pcolormesh(xx, yy, zz, cmap='hot')
                fig.colorbar(zplot, ax=ax)
                ii += 1

        (xmin, xmax) = ax.get_xlim()
        (ymin, ymax) = ax.get_ylim()
        if xrlist is not None:
            if xrlist[0] is not None:
                xmin = xrlist[0]
            if xrlist[1] is not None:
                xmax = xrlist[1]
        if yrlist is not None:
            if yrlist[0] is not None:
                ymin = yrlist[0]
            if yrlist[1] is not None:
                ymax = yrlist[1]
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])

        if ii > 0 and not f2d:
            titlex = titlex / float(ii) if not isinstance(titletag,(float,int)) else float(titletag)
            tlabel = r'$\rho_{\text{tor}}$ = %6.4f' % (titlex) if fancy else "rho = %6.4f" % (titlex)
            if finvert:
                tlabel = r'$t$ = %.2f s' % (titlex) if fancy else "time = %.2f s" % (titlex)
            ax.set_title(tlabel)
        xtag = r'$t$ [s]' if fancy else "time (s)"
        if finvert:
            xtag = r'$\rho_{\text{tor}}$' if fancy else "rho"
        ax.set_xlabel(xtag)
        if f2d:
            y2tag = r'$\rho_{\text{tor}}$' if fancy else "rho"
            if finvert:
                y2tag = r'$t$ [s]' if fancy else "time (s)"
            ax.set_ylabel(y2tag)

        sctag = r''
        if exp != 0:
            extag = "%d" % (exp)
            sctag = r'$\times 10^{' + extag + r'}$ '
        if exp == 3:
            sctag = r'k'
        if exp == 6:
            sctag = r'M'
        if exp == 9:
            sctag = r'G'
        if exp == 12:
            sctag = r'T'
        vtag = r'$y$' if fancy else "y"
        if ytag == "ef":
            vtag = r'$q_' + ftag + r'$ [' + sctag + 'W m$^{-2}$]' if fancy else "heat_flux"
        elif ytag == "pf":
            vtag = r'$\Gamma_' + ftag + r'$ [' + sctag + r'm$^{-2}$ s$^{-1}$]' if fancy else "particle_flux"
        elif ytag == "x":
            vtag = r'$\chi_' + ftag + r'$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "heat_diffusivity"
        elif ytag == "d":
            vtag = r'$D_' + ftag + r'$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "particle_diffusivity"
        elif ytag == "v":
            vtag = r'$V_' + ftag + r'$ [' + sctag + 'm s$^{-1}$]' if fancy else "particle_pinch"
        elif ytag == "xeff":
            vtag = r'$\chi_{\text{eff},' + ftag + r'}$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "effective_heat_diffusivity"
        elif ytag == "deff":
            vtag = r'$D_{\text{eff},' + ftag + r'}$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "effective_particle_diffusivity"
        if not f2d:
            ax.set_ylabel(vtag)
            ax.legend(loc='best',prop={'size':lfsize})
        else:
            ax.set_title(vtag)
        pfmt = "pdf" if fancy else "png"
        oname = ("%s_x%03d." % (ptag, int(titlex * 100))) + pfmt if not finvert else ("%s_t%04d." % (ptag, int(titlex * 1000))) + pfmt
        if f2d:
            oname = ("%s." % (ptag)) + pfmt
        if ii > 0:
            fig.tight_layout()
            fig.savefig(str(opath / oname), format=pfmt)
        else:
            print("Species %s not found in any of the provided run directories." % (ytag))
        plt.close(fig)


def plot_species_total_particle_flux(datadict, ivec=None, electron=False, nion=-1, nimp=-1, outdir=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletags=None):

    if isinstance(datadict, dict):

        if not isinstance(ivec, array_types):
            ivec = [None]

        var = "pf"

        if electron is False and nion < 0 and nimp < 0:
            print("No species specified for plotting in density peaking analysis!")

        if electron:
            itag = "e"
            ptag = "electron_total_particle_flux"
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                if tt is not None and finvert and isinstance(time_offset, number_types):
                    tt += time_offset
                plot_single_transport_coefficient(datadict, var, itag, xindex=ii, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nion >= 0:
            itag = "i%d" % (nion+1)
            ptag = "main_ion%d_total_particle_flux" % (nion+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                if tt is not None and finvert and isinstance(time_offset, number_types):
                    tt += time_offset
                plot_single_transport_coefficient(datadict, var, itag, xindex=ii, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nimp >= 0:
            itag = "imp%d" % (nimp+1)
            ptag = "impurity_ion%d_total_particle_flux" % (nimp+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                if tt is not None and finvert and isinstance(time_offset, number_types):
                    tt += time_offset
                plot_single_transport_coefficient(datadict, var, itag, xindex=ii, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)


def plot_species_heat_flux(datadict, ivec, electron=False, nion=-1, nimp=-1, outdir=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletags=None):

    if isinstance(ivec, array_types):

        var = "ef"

        if electron is False and nion < 0 and nimp < 0:
            print("No species specified for plotting in density peaking analysis!")

        if electron:
            itag = "e"
            ptag = "electron_heat_flux"
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_transport_coefficient(datadict, ii, var, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nion >= 0:
            itag = "i%d" % (nion+1)
            ptag = "main_ion%d_heat_flux" % (nion+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_transport_coefficient(datadict, ii, var, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nimp >= 0:
            itag = "imp%d" % (nimp+1)
            ptag = "impurity_ion%d_heat_flux" % (nimp+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_transport_coefficient(datadict, ii, var, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)


def plot_species_heat_diffusivity(datadict, ivec, electron=False, nion=-1, nimp=-1, outdir=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletags=None, use_effective=False):

    if isinstance(ivec, array_types):

        var = "x" if not use_effective else "xeff"

        if electron is False and nion < 0 and nimp < 0:
            print("No species specified for plotting in density peaking analysis!")

        if electron:
            itag = "e"
            ptag = "electron_heat_diffusivity"
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_transport_coefficient(datadict, ii, var, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nion >= 0:
            itag = "i%d" % (nion+1)
            ptag = "main_ion%d_heat_diffusivity" % (nion+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_transport_coefficient(datadict, ii, var, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)

        if nimp >= 0:
            itag = "imp%d" % (nimp+1)
            ptag = "impurity_ion%d_heat_diffusivity" % (nimp+1)
            for jj in range(len(ivec)):
                ii = ivec[jj]
                tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
                plot_single_transport_coefficient(datadict, ii, var, itag, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt)


def plot_multi_transport_coefficient(datadict, xindex, ytag, stags, ptag="plot", outdir=None, legend=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletag=None, ftotal=True):

    varlist = ["ef", "pf", "x", "d", "v", "xeff", "deff"]

    if isinstance(datadict, dict) and isinstance(xindex, int_types) and ytag in varlist and isinstance(stags, (list,tuple)):

        toff = float(time_offset) if isinstance(time_offset, number_types) else 0.0
        exp = int(y_exponent) if isinstance(y_exponent, number_types) else 0
        scale = np.power(10.0, exp)

        fsize = 24
        if fancy:
            matplotlib.rc('font', family='serif')
            matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=fsize)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
        lfsize = fsize - 6

        opath = Path(outdir) if outdir is not None else Path("./")
        if not opath.exists():
            opath.mkdir(parents=True)
        if not opath.is_dir():
            raise IOError("Target output directory not found!")

        llist = []
        if isinstance(legend, (tuple, list, np.ndarray)):
            for jj in range(len(legend)):
                if isinstance(legend[jj], str):
                    llist.append(legend[jj])
                else:
                    llist.append("?")

        xrlist = None
        yrlist = None
        if isinstance(x_limits,(list,tuple)) and len(x_limits) == 2:
            xrlist = tuple(x_limits)
        if isinstance(y_limits,(list,tuple)) and len(y_limits) == 2:
            yrlist = tuple(y_limits)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        cst = ['b', 'r', 'g', 'y', 'm', 'c']
        lst = ['-', '--', ':', '-.']
        titlex = 0.0
        ii = 0

        for ss in range(len(stags)):

            stag = stags[ss]
            for key, val in datadict.items():
                dtag = "rho_idx" if not finvert else "t_idx"
                dmask = (val.index.to_frame()[dtag].values == xindex)
                data = val.loc[dmask]
                ftag = r'e'
                if not stag.startswith("e") and "a"+stag in data:
                    (nstag, sa, sz) = ptools.define_ion_species(a=np.mean(data["a"+stag]))
                    ftag = r'\text{' + nstag + r'}' if nstag not in ["H", "D", "T"] else r'i'
                if "rho" in data and "t" in data:
                    xx = data["t"].values + toff if not finvert else data["rho"].values
                    zz = float(np.mean(data["rho"].values)) if not finvert else float(np.mean(data["t"].values) + toff)
                    itemtag = ""
                    if len(datadict) > 1:
                        itemtag = " " + llist[ii] if len(llist) > ii else " %d" % (ii+1)
                    if ytag+"a"+stag in data:
                        yy = data[ytag+"a"+stag].values / scale
                        glabel = r'$' + ftag + r'$ Turbulent' + itemtag
                        ax.plot(xx, yy, c=cst[ss], ls='--', label=glabel)
                    if ytag+"nc"+stag in data:
                        yy = data[ytag+"nc"+stag].values / scale
                        nlabel = r'$' + ftag + r'$ Neoclassical' + itemtag
                        ax.plot(xx, yy, c=cst[ss], ls=':', label=nlabel)
                    if ftotal and ytag+"t"+stag in data:
                        yy = data[ytag+"t"+stag].values / scale
                        klabel = r'$' + ftag + r'$ Total' + itemtag
                        ax.plot(xx, yy, c=cst[ss], ls='-', label=klabel)
                    titlex += zz
                    ii += 1

        (xomin, xomax) = ax.get_xlim()
        (qymin, qymax) = ax.get_ylim()
        if xrlist is not None:
            if xrlist[0] is not None:
                xomin = xrlist[0]
            if xrlist[1] is not None:
                xomax = xrlist[1]
        if yrlist is not None:
            if yrlist[0] is not None:
                qymin = yrlist[0]
            if yrlist[1] is not None:
                qymax = yrlist[1]
        ax.set_xlim([xomin, xomax])
        ax.set_ylim([qymin, qymax])

        if ii > 0:
            titlex = titlex / float(ii) if not isinstance(titletag,(float,int)) else float(titletag)
            tlabel = r'$\rho_{\text{tor}}$ = %6.4f' % (titlex) if fancy else "rho = %6.4f" % (titlex)
            if finvert:
                tlabel = r'$t$ = %.2f s' % (titlex) if fancy else "time = %.2f s" % (titlex)
            ax.set_title(tlabel)
        xtag = r'$t$ [s]' if fancy else "time (s)"
        if finvert:
            xtag = r'$\rho_{\text{tor}}$' if fancy else "rho_tor"
        ax.set_xlabel(xtag)

        sctag = r''
        if exp != 0:
            extag = "%d" % (exp)
            sctag = r'$\times 10^{' + extag + r'}$ '
        if exp == 3:
            sctag = r'k'
        if exp == 6:
            sctag = r'M'
        if exp == 9:
            sctag = r'G'
        if exp == 12:
            sctag = r'T'
        vtag = r'$y$' if fancy else "y"
        if ytag == "ef":
            vtag = r'$q$ [' + sctag + 'W m$^{-2}$]' if fancy else "heat_flux"
        elif ytag == "pf":
            vtag = r'$\Gamma$ [' + sctag + r'm$^{-2}$ s$^{-1}$]' if fancy else "particle_flux"
        elif ytag == "x":
            vtag = r'$\chi$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "heat_diffusivity"
        elif ytag == "d":
            vtag = r'$D$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "particle_diffusivity"
        elif ytag == "v":
            vtag = r'$V$ [' + sctag + 'm s$^{-1}$]' if fancy else "particle_pinch"
        elif ytag == "xeff":
            vtag = r'$\chi_{\text{eff}}$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "effective_heat_diffusivity"
        elif ytag == "deff":
            vtag = r'$D_{\text{eff}}$ [' + sctag + 'm$^{2}$ s$^{-1}$]' if fancy else "effective_particle_diffusivity"
        ax.set_ylabel(vtag)
        ax.legend(loc='best',prop={'size':lfsize})
        pfmt = "pdf" if fancy else "png"
        oname = ("%s_x%03d." % (ptag, int(titlex * 100))) + pfmt if not finvert else ("%s_t%04d." % (ptag, int(titlex * 1000))) + pfmt
        if ii > 0:
            fig.tight_layout()
            fig.savefig(str(opath / oname), format=pfmt)
        else:
            print("None of the requested species were found in any of the provided run directories.")
        plt.close(fig)


def plot_multi_species_heat_flux(datadict, ivec, outdir=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletags=None):

    if isinstance(ivec, array_types):

        var = "ef"
        itags = ["e", "i1"]
        ptag = "primary_heat_flux"
        for jj in range(len(ivec)):
            ii = ivec[jj]
            tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
            plot_multi_transport_coefficient(datadict, ii, var, itags, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt, ftotal=False)


def plot_multi_species_heat_diffusivity(datadict, ivec, outdir=None, fancy=False, time_offset=None, finvert=False, y_exponent=None, x_limits=None, y_limits=None, titletags=None, use_effective=False):

    if isinstance(ivec, array_types):

        var = "x" if not use_effective else "xeff"
        itags = ["e", "i1"]
        ptag = "primary_heat_diffusivity"
        for jj in range(len(ivec)):
            ii = ivec[jj]
            tt = titletags[jj] + time_offset if isinstance(titletags,(list,tuple)) and len(titletags) > jj else None
            plot_multi_transport_coefficient(datadict, ii, var, itags, ptag=ptag, outdir=outdir, fancy=fancy, time_offset=time_offset, finvert=finvert, y_exponent=y_exponent, x_limits=x_limits, y_limits=y_limits, titletag=tt, ftotal=False)


if __name__ == "__main__":

    analysis_key = {"n_peaking": 1, "total_pf": 2, "ef": 3, "chi": 4, "chieff": 5}

    parser = argparse.ArgumentParser(description='Perform time-dependent analysis on JETTO-QLK run using gamhistory flag.', \
                                     epilog='Developed by Aaron Ho at TUE - Technical University of Eindhoven. For questions or concerns, please email: a.ho@tue.nl')
#    parser.add_argument('--username', dest='uname', default='', metavar='USERNAME', type=str, action='store', help='User ID for specifying JETTO input directory, only valid in JET computing cluster')
#    parser.add_argument('--catalogue', dest='fctg', default=False, action='store_true', help='Flag to toggle search inside JETTO catalogue directory, only valid in JET computing cluster')
    parser.add_argument('--smoothed', dest='smoothflag', default=False, action='store_true', help='Flag to toggle usage of Savitzky-Golay filter for radial smoothing')
    parser.add_argument('--debug', dest='fdebug', default=False, action='store_true', help='Flag to toggle printing of debug statements in various called functions')
    parser.add_argument('-i', '--indir', dest='indir', default=None, metavar='INPATH', type=str, action='store', help='Path to directory containing .csv files, from where file names corresponding to run directories can be loaded for faster execution')
    parser.add_argument('-o', '--outdir', dest='outdir', default='./', metavar='OUTPATH', type=str, action='store', help='Path to desired output directory')
    parser.add_argument('-s', '--timestart', dest='ti', default=None, type=float, action='store', help='Starting JETTO time for extracted data')
    parser.add_argument('-e', '--timeend', dest='tf', default=None, type=float, action='store', help='Ending JETTO time for extracted data')
    parser.add_argument('-w', '--window', dest='twindow', default=None, type=float, action='store', help='Specify the width of time window for averaging, toggles averaging process')
    parser.add_argument('-x', '--rho', dest='rho', default=None, type=float, action='store', help='Specify rho value to process if <= 1, or number of equally-spaced points from 0-1 if > 1')
    parser.add_argument('-t', '--time', dest='time', nargs='+', default=None, type=float, action='store', help='Specify time value(s) to process')
    parser.add_argument('--nclass', dest='fnc', default=False, action='store_true', help='Toggles inclusion of neoclassical transport, sets time window to 0.05s by default')
    parser.add_argument('--eigenvalues', dest='feigen', default=False, action='store_true', help='Toggles printing of QuaLiKiz eigenvalue solutions, if provided in run directories')
    parser.add_argument('-a', '--analysis', dest='atype', default=None, type=str, choices=list(analysis_key.keys()), action='store', help='Specify which analysis routine to apply on the given run directory')
    parser.add_argument('--plot', dest='plotopt', default=None, type=str, action='store', help='Toggle plotting and specify the plot to make, accepted arguments depend on the analysis type')
    parser.add_argument('--fancy', dest='fancy', default=False, action='store_true', help='Toggle fancy plotting, ion tags are hardcoded')
    parser.add_argument('--offset', dest='offset', default=None, type=float, action='store', help='Add offset to all time values plotted')
    parser.add_argument('--yexp', dest='yexp', default=None, type=int, action='store', help='Set exponent of y-scale for improved plot visuals')
    parser.add_argument('--xbounds', dest='xbounds', nargs=2, default=None, type=float, action='store', help='Set x-axis limits for generated plots')
    parser.add_argument('--ybounds', dest='ybounds', nargs=2, default=None, type=float, action='store', help='Set y-axis limits for generated plots')
    parser.add_argument('rundirs', nargs='*', type=str, action='store', help='Full paths to JETTO run directories to perform analysis on')
    args = parser.parse_args()

    # Obtain user run directory (should allow user to specify completely?)
    #uid = args.uname if args.uname else pwd.getpwuid(os.getuid())[0]
    #bdir = '/common/cmg/' + uid + '/jetto/runs/' #if not args.fctg else '/home/' + uid + '/cmg/catalog/jetto/jet/'

    popt = 1 if args.smoothflag else 0

    radial_vector = None
    if args.rho is not None and args.rho >= 0.0:
        radial_vector = np.array([args.rho]) if args.rho <= 1.0 else np.linspace(0.0, 1.0, int(args.rho))
    time_vector = None
    if args.time is not None and len(args.time) > 0:
        if args.ti is not None or args.tf is not None:
            temp_vector = []
            for tt in range(len(args.time)):
                finclude = True
                if args.ti is not None and args.time[tt] < args.ti:
                    finclude = False
                if args.tf is not None and args.time[tt] > args.tf:
                    finclude = False
                if finclude:
                    temp_vector.append(args.time[tt])
            if len(temp_vector) > 0:
                time_vector = np.array(temp_vector).flatten()
        else:
            time_vector = np.array(args.time).flatten()

    atype = analysis_key[args.atype] if args.atype in analysis_key else 0

    odatadict = {}
    edatadict = {}
    numionmax = 0
    numimpmax = 0
    for rdir in args.rundirs:
        odata = None
        edata = None
        rpath = Path(rdir)
        csvname = rpath.stem + ".csv"
        ipath = Path(args.indir) if args.indir is not None else None

        # Read pre-loaded data for speed, if path is provided and file is available
        if odata is None and ipath is not None and ipath.is_dir():
            ifile = ipath / csvname
            if ifile.is_file():
                odata = pd.read_csv(ifile)
                idx_fields = ["t_idx", "rho_idx", "k_idx"]
                idxvec = []
                for idx in idx_fields:
                    if idx in odata:
                        idxvec.append(idx)
                if len(idxvec) > 0:
                    odata = odata.set_index(idxvec)

        if odata is None and rpath.is_dir():
#            (odata, edata) = gamhistory_extended_pipeline(
#                                 str(rpath.resolve()),
#                                 time_start=args.ti,
#                                 time_end=args.tf,
#                                 time_vector=time_vector,
#                                 time_window=args.twindow,
#                                 radial_vector=radial_vector,
#                                 nc_flag=args.fnc,
#                                 include_eigenvalues=args.feigen,
#                                 analysis_type=atype,
#                                 profile_option=popt
#                             )
            (odata, edata) = qualikiz.perform_gamhistory_pipeline(
                                 str(rpath.resolve()),
                                 time_start=args.ti,
                                 time_end=args.tf,
                                 time_window=args.twindow,
                                 time_vector=time_vector,
                                 radial_vector=radial_vector,
                                 nc_flag=args.fnc,
                                 include_eigenvalues=args.feigen,
                                 analysis_type=atype
                             )

        if odata is not None:
            odatadict[rdir.strip('/')] = copy.deepcopy(odata)
            if args.outdir is not None:
                opath = Path(args.outdir)
                if not opath.exists():
                    opath.mkdir(parents=True)
                if not opath.is_dir():
                    raise IOError("Target output directory not found!")
                ofile = opath / csvname
                odata.to_csv(ofile)
        if edata is not None:
            edatadict[rdir.strip('/')] = copy.deepcopy(edata)

    if args.plotopt is not None:

        if atype == 1:

            eopt = False
            iopt = -1
            zopt = -1
            if args.plotopt.startswith("e"):
                eopt = True
            if args.plotopt.startswith("ion"):
                mm = re.match(r'^ion.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    iopt = int(mm.group(1))
            if args.plotopt.startswith("imp"):
                mm = re.match(r'^imp.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    zopt = int(mm.group(1))

            if len(odatadict) > 0:
                ivec = np.arange(0, len(radial_vector)).tolist()
                vvec = radial_vector.tolist() if not finvert else time_vector.tolist()
                plot_species_density_peaking(odatadict, ivec, electron=eopt, nion=iopt, nimp=zopt, outdir=args.outdir, fancy=args.fancy, time_offset=args.offset, y_exponent=args.yexp, x_limits=args.xbounds, y_limits=args.ybounds, titletags=vvec)

        elif atype == 2:

            eopt = False
            iopt = -1
            zopt = -1
            if args.plotopt.startswith("e"):
                eopt = True
            if args.plotopt.startswith("ion"):
                mm = re.match(r'^ion.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    iopt = int(mm.group(1))
            if args.plotopt.startswith("imp"):
                mm = re.match(r'^imp.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    zopt = int(mm.group(1))

            if len(odatadict) > 0:
                finvert = time_vector is not None
                ivec = None
                vvec = None
                if radial_vector is not None or finvert:
                    ivec = np.arange(0, len(radial_vector)).tolist() if not finvert else np.arange(0, len(time_vector)).tolist()
                    vvec = radial_vector.tolist() if not finvert else time_vector.tolist()
                plot_species_total_particle_flux(odatadict, ivec=ivec, electron=eopt, nion=iopt, nimp=zopt, outdir=args.outdir, fancy=args.fancy, time_offset=args.offset, finvert=finvert, y_exponent=args.yexp, x_limits=args.xbounds, y_limits=args.ybounds, titletags=vvec)

        elif atype == 3:

            eopt = False
            iopt = -1
            zopt = -1
            multiopt = False
            if args.plotopt == "e":
                eopt = True
            if args.plotopt.startswith("ion"):
                mm = re.match(r'^ion.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    iopt = int(mm.group(1))
            if args.plotopt.startswith("imp"):
                mm = re.match(r'^imp.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    zopt = int(mm.group(1))
            if args.plotopt == "ei":
                multiopt = True

            if len(odatadict) > 0:
                finvert = time_vector is not None
                ivec = np.arange(0, len(radial_vector)).tolist() if not finvert else np.arange(0, len(time_vector)).tolist()
                vvec = radial_vector.tolist() if not finvert else time_vector.tolist()
                if multiopt:
                    plot_multi_species_heat_flux(odatadict, ivec, outdir=args.outdir, fancy=args.fancy, time_offset=args.offset, finvert=finvert, y_exponent=args.yexp, x_limits=args.xbounds, y_limits=args.ybounds, titletags=vvec)
                else:
                    plot_species_heat_flux(odatadict, ivec, electron=eopt, nion=iopt, nimp=zopt, outdir=args.outdir, fancy=args.fancy, time_offset=args.offset, finvert=finvert, y_exponent=args.yexp, x_limits=args.xbounds, y_limits=args.ybounds, titletags=vvec)

        elif atype in [4, 5]:

            eopt = False
            iopt = -1
            zopt = -1
            multiopt = False
            if args.plotopt == "e":
                eopt = True
            if args.plotopt.startswith("ion"):
                mm = re.match(r'^ion.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    iopt = int(mm.group(1))
            if args.plotopt.startswith("imp"):
                mm = re.match(r'^imp.*([0-9]+)$', args.plotopt, flags=re.IGNORECASE)
                if mm:
                    zopt = int(mm.group(1))
            if args.plotopt == "ei":
                multiopt = True
            use_effective = True if atype == 5 else False

            if len(odatadict) > 0:
                finvert = time_vector is not None
                ivec = np.arange(0, len(radial_vector)).tolist() if not finvert else np.arange(0, len(time_vector)).tolist()
                vvec = radial_vector.tolist() if not finvert else time_vector.tolist()
                if multiopt:
                    plot_multi_species_heat_diffusivity(odatadict, ivec, outdir=args.outdir, fancy=args.fancy, time_offset=args.offset, finvert=finvert, y_exponent=args.yexp, x_limits=args.xbounds, y_limits=args.ybounds, titletags=vvec, use_effective=use_effective)
                else:
                    plot_species_heat_diffusivity(odatadict, ivec, electron=eopt, nion=iopt, nimp=zopt, outdir=args.outdir, fancy=args.fancy, time_offset=args.offset, finvert=finvert, y_exponent=args.yexp, x_limits=args.xbounds, y_limits=args.ybounds, titletags=vvec, use_effective=use_effective)


    print("Script completed!")
