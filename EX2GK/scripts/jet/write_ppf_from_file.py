#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import sys
import os
import argparse
import re
import pwd
import numpy as np
import pickle
import ppf

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Writes data from .txt file from GUI into private PPF.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('filename', metavar='FILENAME', type=str, action='store', help='Name of output file containing profile data')
    args = parser.parse_args()

    basename = None
    DB = None
    if os.path.isfile(args.filename):
        basename = os.path.basename(args.filename)
        with open(args.filename,'r') as ff:
            fheader = False
            snum = None
            coord = None
            time1 = None
            time2 = None
            dlabel = None
            ii = 0
            for line in ff:
                sline = line.strip()
                if sline and not re.match('^#',sline):
                    if re.search('START OF HEADER',sline,flags=re.IGNORECASE):
                        fheader = True
                        snum = None
                        coord = None
                        time1 = None
                        time2 = None
                        dlabel = None
                        if DB is None:
                            DB = dict()
                    elif re.search('END OF HEADER',sline,flags=re.IGNORECASE):
                        fheader = False
                        if isinstance(snum,int):
                            if snum not in DB:
                                DB[snum] = []
                            ii = len(DB[snum])
                            DB[snum].append(dict())
                    elif fheader:
                        if re.search('Shot Number',sline,flags=re.IGNORECASE):
                            dline = sline.split()
                            snum = int(float(dline[-1]))
                        if re.search('Radial Coordinate',sline,flags=re.IGNORECASE):
                            dline = sline.split()
                            coord = dline[-1]
                        if re.search('Time Window Start',sline,flags=re.IGNORECASE):
                            dline = sline.split()
                            time1 = float(dline[-2])
                        if re.search('Time Window End',sline,flags=re.IGNORECASE):
                            dline = sline.split()
                            time2 = float(dline[-2])
                    elif isinstance(snum,int) and isinstance(coord,str) and isinstance(time1,(float,int)) and isinstance(time2,(float,int)):
                        mm = re.match('^'+coord+'\s+([A-Za-z0-9]+)\s+',sline)
                        if mm:
                            if dlabel is not None:
                                if "X"+dlabel in DB[snum][ii] and coord not in DB[snum][ii]:
                                    DB[snum][ii][coord] = DB[snum][ii]["X"+dlabel].copy()
                                if "T1"+dlabel in DB[snum][ii] and "T1" not in DB[snum][ii]:
                                    DB[snum][ii]["T1"] = DB[snum][ii]["T1"+dlabel]
                                if "T2"+dlabel in DB[snum][ii] and "T2" not in DB[snum][ii]:
                                    DB[snum][ii]["T2"] = DB[snum][ii]["T2"+dlabel]
                            if re.match('^NE$',mm.group(1)):
                                dlabel = "NE"
                            elif re.match('^TE$',mm.group(1)):
                                dlabel = "TE"
                            elif re.match('^TI$',mm.group(1)):
                                dlabel = "TI"
                            elif re.match('^ANGF$',mm.group(1)):
                                dlabel = "AF"
                            elif re.match('^Q$',mm.group(1)):
                                dlabel = "Q"
                            elif re.match('^ZEFF$',mm.group(1)):
                                dlabel = "ZEFF"
                            if dlabel is not None:
                                DB[snum][ii]["CS_OUT"] = coord
                                DB[snum][ii]["T1"+dlabel] = time1
                                DB[snum][ii]["T2"+dlabel] = time2
                        elif dlabel is not None and re.search('^-?[0-9]',sline):
                            dline = sline.split()
                            if len(dline) >= 2:
                                DB[snum][ii]["X"+dlabel] = np.array([float(dline[0])]) if "X"+dlabel not in DB[snum][ii] else np.hstack((DB[snum][ii]["X"+dlabel],float(dline[0])))
                                DB[snum][ii][dlabel] = np.array([float(dline[1])]) if dlabel not in DB[snum][ii] else np.hstack((DB[snum][ii][dlabel],float(dline[1])))
                            if len(dline) >= 3:
                                DB[snum][ii][dlabel+"EB"] = np.array([float(dline[2])]) if dlabel+"EB" not in DB[snum][ii] else np.hstack((DB[snum][ii][dlabel+"EB"],float(dline[2])))
    else:
        raise IOError('Input file ' + args.filename + ' not found.')

    vals = ["NE","TE","TI","AF","Q","ZEFF"]
    wdata = dict()
    if DB is not None:
        for snum in DB:
            if snum not in wdata:
                wdata[snum] = dict()
            wcs = None
            xdata = None
            for val in vals:
                ydata = None
                edata = None
                tdata = np.array([])
                for ii in np.arange(0,len(DB[snum])):
                    if val in DB[snum][ii]:
                        sdata = DB[snum][ii]
                        if xdata is None:
                            wcs = sdata["CS_OUT"]
                            xdata = sdata[sdata["CS_OUT"]]
                        tval = np.mean([sdata["T1"],sdata["T2"]])
                        tv = np.where(tdata > tval)[0]
                        if tv.size > 0 and tdata.size > 0:
                            tdata = np.insert(tdata,tv[0],tval)
                            ydata = np.insert(ydata,tv[0],sdata[val],axis=0)
                            edata = np.insert(edata,tv[0],sdata[val+"EB"],axis=0)
                        else:
                            tdata = np.hstack((tdata,tval))
                            ydata = np.atleast_2d(sdata[val]) if ydata is None else np.vstack((ydata,sdata[val]))
                            edata = np.atleast_2d(sdata[val+"EB"]) if edata is None else np.vstack((edata,sdata[val+"EB"]))
                wdata[snum][val] = ydata.copy() if ydata is not None else None
                wdata[snum][val+"EB"] = edata.copy() if ydata is not None and edata is not None else None
                wdata[snum]["X"+val] = xdata.copy() if ydata is not None else None
                wdata[snum]["T"+val] = tdata.copy() if ydata is not None else None
            if wcs is None:
                wcs = "RHOTOR"
            wdata[snum]["CS"] = wcs
            wdata[snum][wcs] = xdata.copy()

    uid = pwd.getpwuid(os.getuid())[0]
    for snum in wdata:
        pulse = int(snum)
        print("User ID:",uid,"   ","Pulse No.:",pulse)
        ppf.ppfuid(uid,"W")
        time,date,ier = ppf.pdstd(pulse)
        ier = ppf.ppfopn(pulse,date,time,"Custom profile from " + basename)
        print("PPF Open:",ier)

        ocs = "none"
        if re.match("RHOTORN?",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "rho_tor"
        elif re.match("RHOPOLN?",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "rho_pol"
        elif re.match("TORFLUXN",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "psi_tor_norm"
        elif re.match("POLFLUXN",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "psi_pol_norm"
        elif re.match("TORFLUX",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "psi_tor"
        elif re.match("POLFLUX",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "psi_pol"
        elif re.match("RMAJORO",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "R_lfs"
        elif re.match("RMAJORI",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "R_hfs"
        elif re.match("RMIDAVG",wdata[snum]["CS"],flags=re.IGNORECASE):
            ocs = "r_mid_avg"

        dda = "PRFL"
        fwritten = False
        for val in vals:
            dtyp = None
            unit = None
            if re.match(r'NE',val):
                dtyp = "NE"
                unit = "m**-3"
            if re.match(r'TE',val):
                dtyp = "TE"
                unit = "eV"
            if re.match(r'TI',val):
                dtyp = "TI"
                unit = "eV"
            if re.match(r'NIMP',val):
                dtyp = "NX"
                unit = "m**-3"
            if re.match(r'AF',val):
                dtyp = "AF"
                unit = "rads/s"
            if re.match(r'Q',val):
                dtyp = "Q"
                unit = ""
            if re.match(r'ZEFF',val):
                dtyp = "ZEFF"
                unit = ""

            if wdata[snum][val] is not None:
                indat = wdata[snum][val].copy()
                irdat = ppf.ppfwri_irdat(wdata[snum]["X"+val].size,wdata[snum]["T"+val].size,-1,-1,0,0)
                ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit (rho_tor)")
                iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                print(dtyp+" PPF Write:",ier)
                if ier == 0:
                    fwritten = True

                if wdata[snum][val+"EB"] is not None:
                    indat = wdata[snum][val] - wdata[snum][val+"EB"]
                    ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit lower (rho_tor) - 1 sigma")
                    iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"LO",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                    print(dtyp+"LO PPF Write:",ier)

                    indat = wdata[snum][val] + wdata[snum][val+"EB"]
                    ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit upper (rho_tor) - 1 sigma")
                    iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"HI",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                    print(dtyp+"HI PPF Write:",ier)

                    indat = wdata[snum][val+"EB"]
                    ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit uncertainty (rho_tor) - 1 sigma")
                    iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"EB",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                    print(dtyp+"EB PPF Write:",ier)

        if fwritten:
            if re.match("RHOTORN?",wdata[snum]["CS"],flags=re.IGNORECASE):
                dtyp = "SPNT"
                unit = "rho_tor"
                indat = wdata[snum][wdata[snum]["CS"]]
                irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                print(dtyp+" PPF Write:",ier)
            elif re.match("TORFLUXN",wdata[snum]["CS"],flags=re.IGNORECASE):
                dtyp = "SPNT"
                unit = "rho_tor"
                indat = np.sqrt(np.abs(wdata[snum][wdata[snum]["CS"]]))
                irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                print(dtyp+" PPF Write:",ier)
            elif re.match("RHOPOLN?",wdata[snum]["CS"],flags=re.IGNORECASE):
                dtyp = "PSIN"
                unit = "psi_pol_norm"
                indat = np.power(wdata[snum][wdata[snum]["CS"]],2.0)
                irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                print(dtyp+" PPF Write:",ier)
            elif re.match("TORFLUXN",wdata[snum]["CS"],flags=re.IGNORECASE):
                dtyp = "PSIN"
                unit = "psi_pol_norm"
                indat = wdata[snum][wdata[snum]["CS"]]
                irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                print(dtyp+" PPF Write:",ier)
            elif re.match("RMAJORO",wdata[snum]["CS"],flags=re.IGNORECASE):
                dtyp = "RLFS"
                unit = "R_lfs"
                indat = wdata[snum][wdata[snum]["CS"]]
                irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                print(dtyp+" PPF Write:",ier)

        seq,ier = ppf.ppfclo(pulse,"GPR1D",1)
        print("PPF Sequence:",seq)

        if ier == 0 and seq >= 0:
            print("PPF write script completed successfully!")
            print("    Data has been written to:\n   shot = %6d, uid = %15s, seq = %5d" % (pulse,uid,seq))
