#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import sys
import os
import argparse
import re
import pwd
import numpy as np
import pickle
import ppf

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Writes data from .p file into private PPF.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-i', dest='idir', default='./', metavar='INDIR', type=str, action='store', help='Name of directory where data files are kept')
    parser.add_argument('dbname', metavar='DBNAME', type=str, action='store', help='Name of output file containing merged data')
    args = parser.parse_args()

    basename = os.path.basename(args.dbname)
    indir = args.idir
    if not indir.endswith('/'):
        indir = indir + '/'
    DB = None
    if os.path.exists(indir) and os.path.isfile(indir+args.dbname):
        bf = open(indir+args.dbname,'rb')
        DB = pickle.load(bf)
        bf.close()
    elif os.path.isfile(args.dbname):
        bf = open(args.dbname,'rb')
        DB = pickle.load(bf)
        bf.close()
    else:
        raise IOError('Input file ' + basename + ' not found.')

    vals = ["NE","TE","TI","AF","Q","ZEFF"]
    wdata = dict()
    if DB is not None:
        for snum in DB:
            if snum not in wdata:
                wdata[snum] = dict()
            wcs = None
            xdata = None
            for val in vals:
                ydata = None
                edata = None
                tdata = np.array([])
                for ii in np.arange(0,len(DB[snum])):
                    if val in DB[snum][ii]:
                        sdata = DB[snum][ii]
                        if xdata is None:
                            wcs = sdata["CS_OUT"]
                            xdata = sdata[sdata["CS_OUT"]]
                        tval = np.mean([sdata["T1"],sdata["T2"]])
                        tv = np.where(tdata > tval)[0]
                        if tv.size > 0 and tdata.size > 0:
                            tdata = np.insert(tdata,tv[0],tval)
                            ydata = np.insert(ydata,tv[0],sdata[val],axis=0)
                            edata = np.insert(edata,tv[0],sdata[val+"EB"],axis=0)
                            ddata = np.insert(ddata,tv[0],sdata["D"+val],axis=0)
                            gdata = np.insert(gdata,tv[0],sdata["D"+val+"EB"],axis=0)
                        else:
                            tdata = np.hstack((tdata,tval))
                            ydata = np.atleast_2d(sdata[val]) if ydata is None else np.vstack((ydata,sdata[val]))
                            edata = np.atleast_2d(sdata[val+"EB"]) if edata is None else np.vstack((edata,sdata[val+"EB"]))
                            ddata = np.atleast_2d(sdata["D"+val]) if ddata is None else np.vstack((ddata,sdata["D"+val]))
                            gdata = np.atleast_2d(sdata["D"+val+"EB"]) if gdata is None else np.vstack((gdata,sdata["D"+val+"EB"]))
                wdata[snum][val] = ydata.copy() if ydata is not None else None
                wdata[snum][val+"EB"] = edata.copy() if ydata is not None and edata is not None else None
                wdata[snum]["D"+val] = ddata.copy() if ydata is not None and ddata is not None else None
                wdata[snum]["D"+val+"EB"] = gdata.copy() if ydata is not None and ddata is not None and gdata is not None else None
                wdata[snum]["X"+val] = xdata.copy() if ydata is not None else None
                wdata[snum]["T"+val] = tdata.copy() if ydata is not None else None
                if ydata is not None:
                    fdata = True
            if wcs is None:
                wcs = "RHOTOR"
            ctag = "C" if "CC_FLAG" in sdata and sdata["CC_FLAG"] else ""
            wdata[snum]["CS"] = wcs if xdata is not None else None
            wdata[snum]["CCS"] = corrflag if xdata is not None else None
            if "CS_"+ctag+wcs in sdata and "CS_"+ctag+"RHOTORN" in sdata: #Here
                (wdata[snum]["SPNT"],tempe,tempj) = EX2GK.ptools.convert_coords(self.sdata,xdata,wcs,"RHOTORN",corrflag=corrflag)
            (wdata[snum]["PSIN"],tempe,tempj) = EX2GK.ptools.convert_coords(self.sdata,xdata,wcs,"POLFLUXN",corrflag=corrflag)
            (wdata[snum]["RLFS"],tempe,tempj) = EX2GK.ptools.convert_coords(self.sdata,xdata,wcs,"RMAJORO",corrflag=corrflag)

        if fdata:
            uid = pwd.getpwuid(os.getuid())[0]
            for snum in sorted(wdata.keys()):
                pulse = int(snum)
                print("User ID:",uid,"   ","Pulse No.:",pulse)
                ppf.ppfuid(uid,"W")
                time,date,ier = ppf.pdstd(pulse)
                datestring = datetime.datetime.now().strftime("%B %d, %Y - %X %z")
                ier = ppf.ppfopn(pulse,date,time,"Fit profile from EX2GK: "+datestring)
                print("PPF Open:",ier)

                ocs = "none"
                if re.match("RHOTORN?",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "rho_tor"
                elif re.match("RHOPOLN?",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "rho_pol"
                elif re.match("TORFLUXN",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "psi_tor_norm"
                elif re.match("POLFLUXN",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "psi_pol_norm"
                elif re.match("TORFLUX",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "psi_tor"
                elif re.match("POLFLUX",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "psi_pol"
                elif re.match("RMAJORO",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "R_lfs"
                elif re.match("RMAJORI",wdata[snum]["CS"],flags=re.IGNORECASE):
                    ocs = "R_hfs"

                fwritten = False
                for val in vals:
                    dtyp = None
                    unit = None
                    if re.match(r'NE',val):
                        dtyp = "NE"
                        unit = "m**-3"
                    if re.match(r'TE',val):
                        dtyp = "TE"
                        unit = "eV"
                    if re.match(r'TI',val):
                        dtyp = "TI"
                        unit = "eV"
                    if re.match(r'NIMP',val):
                        dtyp = "NX"
                        unit = "m**-3"
                    if re.match(r'AF',val):
                        dtyp = "AF"
                        unit = "rad/s"
                    if re.match(r'Q',val):
                        dtyp = "Q"
                        unit = ""
                    if re.match(r'NFI',val):
                        dtyp = "NFI"
                        unit = "m**-3"
                    if re.match(r'WFI',val):
                        dtyp = "WFI"
                        unit = "J m**-3"

                    if wdata[snum][val] is not None:
                        indat = wdata[snum][val].copy()
                        irdat = ppf.ppfwri_irdat(wdata[snum]["X"+val].size,wdata[snum]["T"+val].size,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit")
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                        print(dtyp+" PPF Write:",ier)
                        if ier == 0:
                            fwritten = True

                        if wdata[snum][val+"EB"] is not None:
                            indat = wdata[snum][val] - wdata[snum][val+"EB"]
                            ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit lower - 1 sigma")
                            iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"LO",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                            print(dtyp+"LO PPF Write:",ier)

                            indat = wdata[snum][val] + wdata[snum][val+"EB"]
                            ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit upper - 1 sigma")
                            iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"HI",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                            print(dtyp+"HI PPF Write:",ier)

                            indat = wdata[snum][val+"EB"].copy()
                            ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR "+val+" fit uncertainty - 1 sigma")
                            iwdat,ier = ppf.ppfwri(pulse,dda,dtyp+"EB",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                            print(dtyp+"EB PPF Write:",ier)

                        if wdata[snum]["D"+val] is not None:
                            indat = wdata[snum]["D"+val].copy()
                            ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR d"+val+"/drho_tor fit")
                            iwdat,ier = ppf.ppfwri(pulse,dda,"D"+dtyp,irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                            print("D"+dtyp+" PPF Write:",ier)

                            if wdata[snum]["D"+val+"EB"] is not None:
                                indat = wdata[snum]["D"+val] - wdata[snum]["D"+val+"EB"]
                                ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR d"+val+"/drho_tor fit lower - 1 sigma")
                                iwdat,ier = ppf.ppfwri(pulse,dda,"D"+dtyp+"LO",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                                print("D"+dtyp+"LO PPF Write:",ier)

                                indat = wdata[snum]["D"+val] + wdata[snum]["D"+val+"EB"]
                                ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR d"+val+"/drho_tor fit upper - 1 sigma")
                                iwdat,ier = ppf.ppfwri(pulse,dda,"D"+dtyp+"HI",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                                print("D"+dtyp+"HI PPF Write:",ier)

                                indat = wdata[snum]["D"+val+"EB"].copy()
                                ihdat = ppf.ppfwri_ihdat(unit,ocs,"s","f","f","f","GPR d"+val+"/drho_tor fit uncertainty - 1 sigma")
                                iwdat,ier = ppf.ppfwri(pulse,dda,"D"+dtyp+"EB",irdat,ihdat,indat,wdata[snum]["X"+val],wdata[snum]["T"+val])
                                print("D"+dtyp+"EB PPF Write:",ier)

                if fwritten:
                    if "SPNT" in wdata[snum] and wdata[snum]["SPNT"] is not None:
                        dtyp = "SPNT"
                        unit = "rho_tor"
                        indat = wdata[snum]["SPNT"]
                        irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                        print(dtyp+" PPF Write:",ier)
                    if "PSIN" in wdata[snum] and wdata[snum]["PSIN"] is not None:
                        dtyp = "PSIN"
                        unit = "psi_pol_norm"
                        indat = wdata[snum]["PSIN"]
                        irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                        print(dtyp+" PPF Write:",ier)
                    if "RLFS" in wdata[snum] and wdata[snum]["RLFS"] is not None:
                        dtyp = "RLFS"
                        unit = "R_lfs"
                        indat = wdata[snum]["RLFS"]
                        irdat = ppf.ppfwri_irdat(indat.size,1,-1,-1,0,0)
                        ihdat = ppf.ppfwri_ihdat(unit,unit,"s","f","f","f",unit)
                        iwdat,ier = ppf.ppfwri(pulse,dda,dtyp,irdat,ihdat,indat,indat,[0.0])
                        print(dtyp+" PPF Write:",ier)

                seq,ier = ppf.ppfclo(pulse,"EX2GK",1)
                print("PPF Sequence:",seq)

        if ier == 0 and seq >= 0:
            print("PPF write script completed successfully!")
            print("    Data has been written to:\n   shot = %6d, uid = %15s, seq = %5d" % (pulse,uid,seq))
