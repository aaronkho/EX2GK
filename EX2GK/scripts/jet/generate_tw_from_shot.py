#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import os
import sys
import pwd
import re
import copy
import argparse
import numpy as np

from EX2GK import EX2GK
from EX2GK.machine_adapters.jet import time_selector as tws

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generate a list of time windows for a given JET discharge, results not guaranteed to be definitive.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-u', '--up', dest='nrampup', default=0, metavar='NRAMPUP', type=int, action='store', help='Number of desired time windows in current ramp-up phase')
    parser.add_argument('-d', '--down', dest='nrampdown', default=0, metavar='NRAMPDOWN', type=int, action='store', help='Number of desired time windows in current ramp-down phase')
    parser.add_argument('-p', '--plot', dest='fplot', default=False, action='store_true', help='Toggle plotting of zero-dimensional time traces of discharge and selected time windows.')
    parser.add_argument('-o', '--outfile', dest='oname', default=None, metavar='OUTNAME', type=str, action='store', help='Name of file into which the list of generated time windows is written, printed to console if not given.')
    parser.add_argument('shot', nargs=1, type=int, action='store', help='JET pulse number from which to auto-generate time windows')
    parser.add_argument('nwindows', nargs=1, type=int, action='store', help='Number of time windows to auto-generate')
    args = parser.parse_args()

    nrampup = args.nrampup if args.nrampup > 0 else 0
    nrampdown = args.nrampdown if args.nrampdown > 0 else 0
    nflattop = args.nwindows[0] - nrampup - nrampdown
    nwindows = [nrampup,nflattop,nrampdown]    
    data = tws.generate_time_windows(args.shot[0],nwindows,do_plot=args.fplot)
    if data is not None:
        datalist = tws.make_printable(data)
        twstr = ""
        for item in datalist:
            twstr = twstr + item + '\n'
        if args.oname is not None:
            with open(args.oname,'w') as ff:
                ff.write(twstr)
        else:
            print(twstr)
    print('Time window selection completed!')

