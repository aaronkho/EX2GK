#!/usr/bin/env python
## Recommended on Freia: /usr/local/depot/Python-3.5.1/bin/python3

import os
import sys
import pwd
import shutil
import re
import copy
import argparse
import datetime
import subprocess
import numpy as np

from EX2GK.jetto_pythontools.jetto_tools import run as rtools

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Set up JETTO run automatically and submit to queue. Only suitable for ex-file swapping, recommended to use JAMS to adjust other settings.', \
                                     epilog='Developed by Aaron Ho at DIFFER - Dutch Institute of Fundamental Energy Research. For questions or concerns, please email: a.ho@differ.nl')
    parser.add_argument('-b', dest='bname', default='', metavar='BASENAME', type=str, action='store', help='Base name for generated directories containing JETTO runs')
    parser.add_argument('-u', dest='uname', default='', metavar='USERNAME', type=str, action='store', help='User ID for default input directory')
    parser.add_argument('-f', dest='foverwrite', default=False, action='store_true', help='Forces overwriting of any exfiles which clash with those in JAMS folder')
    parser.add_argument('-n', dest='rnum', default=1, metavar='RUNNUMBER', type=int, action='store', help='Optional run number for directory, increments if it already exists')
    parser.add_argument('-r', dest='runflag', default=False, action='store_true', help='Optional flag to also submit the run to the queue as soon as it is generated')
    parser.add_argument('baserun', nargs=1, type=str, action='store', help='Name of JETTO run directory to serve as reference point for run generation')
    parser.add_argument('exfile', nargs=1, type=str, action='store', help='Name or location of exfile to be used in JETTO run')
    args = parser.parse_args()

    status = rtools.make_jetto_run(args.baserun,args.exfile,runname=args.bname,username=args.uname,foverwrite=args.foverwrite,runnumber=args.rnum,runflag=args.runflag)
    if status != 0:
        print("Error occurred in run setup script. Please check inputs and try again.")
