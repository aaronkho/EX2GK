# Script with functions to plot ex-file formats used with JETTO
# Developer: Aaron Ho - 14/12/2018

# Required imports
import os.path
import pwd
import re
import struct
import shutil
import copy
import numpy as np
from scipy.interpolate import interp1d
from scipy.stats import norm,chi2
import collections
import matplotlib
import matplotlib.pyplot as plt

from EX2GK.jetto_pythontools.jetto_tools import binary as jread
from EX2GK.tools.general import read_EX2GK_output as xread, proctools as ptools
from EX2GK.machine_adapters.jet import extract_jet_sal as xppf

np_itypes = (np.int8, np.int16, np.int32, np.int64)
np_utypes = (np.uint8, np.uint16, np.uint32, np.uint64)
np_ftypes = (np.float16, np.float32, np.float64)

number_types = (float, int, np_itypes, np_utypes, np_ftypes)
array_types = (list, tuple, np.ndarray)
jet_time_offset = -40.0


def plot_jsp(datafiles,quantities,outname,outdir='./',time=None,icfile=None,sigma=2.0,legend=None,labels=None,lines=None,colors=None,gradient=False, \
             oefile=None,rdfile=None,histfile=None,xlimit=None,ylimit=None,xmarks=None,ymarks=None,xplot=None,yplot=None, \
             fancy=False,fontsize=14,plot_format='png',plot_dpi=300,one_column=False,one_row=False,impurities=None):

    fname = 'jetto.jsp'
    flist = None
    qlist = None
    oname = 'temp'
    if isinstance(datafiles,list):
        flist = datafiles
    if isinstance(quantities,str):
        temp = quantities.split(',')
        qlist = []
        for ii in range(0,len(temp)):
            qlist.append(temp[ii].strip())
        if len(qlist) <= 0:
            qlist = None
    if isinstance(quantities,list):
        qlist = quantities
    if isinstance(outname,str):
        mm = re.match(r'^(.*)\.([a-z0-9]+)$',outname)
        if mm:
            oname = mm.group(1)
            plot_format = mm.group(2)
        else:
            oname = outname

    fgood = False
    device = 'JET'
    shot = None
    db = []
    for ii in np.arange(0,len(flist)):
        jdata = None
        if isinstance(flist[ii],str):
            fdir = flist[ii]
            if not fdir.endswith('/'):
                fdir = fdir + '/'
            if os.path.exists(flist[ii]):
                jdata = jread.read_binary_file(fdir + fname)
                fgood = True
            else:
                print("JETTO JSP file, %s, not found" % (fdir + fname))
        db.append(copy.deepcopy(jdata))
        if jdata is not None:
            if shot is None and "SHOT" in jdata:
                shot = int(jdata["SHOT"])

    if fgood and qlist is not None:
        jqdict = {"TIME":"TIME", "XVEC1":"RHOTORN", "XVEC2":"RHOTORN", "XRHO":"RHOTORN", "RHO":"PSITOR", "XPSI":"PSIPOLN", "PSI":"PSIPOL", "R":"RMAJORO", "RI":"RMAJORI", \
                  "NE":"NE", "TE":"TE", "NI":"NI1", "TI":"TI1", "ANGF":"AFTOR", "VTOR":"UTOR", "Q":"Q", "VOL":"FSVOL", "SURF":"FSAREA", "ZEFF":"ZEFF", \
                  "NIM1":"NIMP1", "NIM2":"NIMP2", "NIM3":"NIMP3", "SBD1":"NFINBI1", "SBD2":"NFINBI2", "WNBD":"STINBI", "WRFD":"STIICRH", "JZNB":"JNBI", "JZRF":"JICRH", \
                  "QOH": "STEOHM", "QRAD": "STRAD", "QTHX": "STEI"}
        qedict = {"NE":19, "TE":3, "NI":19, "NIMP":17, "TI":3, "ANGF":4, "VTOR":4, "Q":0, "ZEFF":0, "SBD1":19, "SBD2":19, "WNBD":3, "WRFD":3, "JZNB":0, "JZRF":0, "QOH": 3, "QRAD": 3, "QTHX": 3}
        qgdict = {"NE":"GRNE", "TE":"GRTE", "TI":"GRTI", "ANGF":"ANGF", "VTOR":"VTOR", "Q":"SH", "ZEFF":"ZEFF"}
        prdict = {'jpg':300, 'bmp':300, 'png':300, 'ps':1200, 'svg':1200, 'eps':1200, 'pdf':1200}

        odir = outdir if isinstance(outdir,str) else './'
        if odir is not None and not odir.endswith('/'):
            odir = odir + '/'
        if not os.path.exists(odir):
            os.makedirs(odir)

        ic = None
        sig = None
        if isinstance(icfile,str) and os.path.isfile(icfile):
            ic = xread.read_data_file(icfile)
            sig = float(sigma) if isinstance(sigma,number_types) and float(sigma) > 0.0 else 2.0

        oe = None
        if isinstance(oefile,str) and os.path.isfile(oefile):
            oe = xread.read_data_file(oefile)
            sig = float(sigma) if isinstance(sigma,number_types) and float(sigma) > 0.0 else 2.0

        rd = None
        if isinstance(rdfile,str) and os.path.isfile(rdfile):
            rd = xread.read_data_file(rdfile)

        hd = None
        if len(db) == 1 and histfile is not None and os.path.isfile(histfile):
            hd = xread.read_multi_file(histfile)

        llist = None
        if isinstance(labels,str):
            temp = labels.split(',')
            llist = []
            for ii in range(0,len(temp)):
                llist.append(temp[ii].strip())
            if len(qlist) <= 0:
                llist = None
        elif isinstance(labels,list):
            llist = labels
        if llist is not None:
            while len(llist) < len(db):
                llist.append(None)

        ilist = None
        if isinstance(impurities,str):
            temp = impurities.split(',')
            ilist = []
            for ii in range(0,len(temp)):
                ilist.append(temp[ii].strip())
            if len(ilist) <= 0:
                ilist = None
        elif isinstance(impurities,list):
            ilist = impurities

        xlist = None
        ylist = None
        if isinstance(xlimit,tuple) and len(xlimit) == 2:
            xlist = []
            while len(xlist) < len(qlist):
                xlist.append(tuple(xlimit))
        elif isinstance(xlimit,list):
            xlist = []
            for ii in range(0,len(xlimit)):
                if isinstance(xlimit[ii],(list,tuple)) and len(xlimit[ii]) == 2:
                    xlist.append(tuple(xlimit[ii]))
                else:
                    xlist.append((None,None))
            while len(xlist) < len(qlist):
                xlist.append((None,None))
        if isinstance(ylimit,tuple) and len(ylimit) == 2:
            ylist = []
            while len(ylist) < len(qlist):
                ylist.append(tuple(ylimit))
        elif isinstance(ylimit,list):
            ylist = []
            for ii in range(0,len(ylimit)):
                if isinstance(ylimit[ii],(list,tuple)) and len(ylimit[ii]) == 2:
                    ylist.append(tuple(ylimit[ii]))
                else:
                    ylist.append((None,None))
            while len(ylist) < len(qlist):
                ylist.append((None,None))

        xrlist = None
        yrlist = None
        if isinstance(xplot,tuple) and len(xplot) == 2:
            xrlist = []
            while len(xrlist) < len(qlist):
                xrlist.append(tuple(xplot))
        elif isinstance(xplot,list):
            xrlist = []
            for ii in range(0,len(xplot)):
                if isinstance(xplot[ii],(list,tuple)) and len(xplot[ii]) == 2:
                    xrlist.append(tuple(xplot[ii]))
                else:
                    xrlist.append((None,None))
            while len(xrlist) < len(qlist):
                xrlist.append((None,None))
        if isinstance(yplot,tuple) and len(yplot) == 2:
            yrlist = []
            while len(yrlist) < len(qlist):
                yrlist.append(tuple(yplot))
        elif isinstance(yplot,list):
            yrlist = []
            for ii in range(0,len(yplot)):
                if isinstance(yplot[ii],(list,tuple)) and len(yplot[ii]) == 2:
                    yrlist.append(tuple(yplot[ii]))
                else:
                    yrlist.append((None,None))
            while len(yrlist) < len(qlist):
                yrlist.append((None,None))

        hlines = None
        vlines = None
        if isinstance(ymarks, number_types):
            hlines = np.array([float(ymarks)])
        elif isinstance(ymarks, array_types):
            hlines = np.array(ymarks)
        if isinstance(xmarks, number_types):
            vlines = np.array([float(xmarks)])
        elif isinstance(xmarks, array_types):
            vlines = np.array(xmarks)

        nlegends = []
        if isinstance(legend,number_types) and int(legend) > 0:
            nlegends.append(int(legend))
        elif isinstance(legend,array_types):
            for ii in range(0,len(legend)):
                if isinstance(legend[ii],number_types) and int(legend[ii]) > 0:
                    nlegends.append(int(legend[ii]))
        elif isinstance(legend,str):
            if legend == "all":
                for ii in range(0,len(qlist)):
                    nlegends.append(ii+1)

        fsize = 14
        if isinstance(fontsize,(int,float)) and int(fontsize) > 6:
            fsize = int(fontsize)
        if fancy:
            matplotlib.rcParams['xtick.minor.visible'] = True
            matplotlib.rcParams['font.family'] = 'serif'
            matplotlib.rcParams['font.size'] = fsize
            #matplotlib.rcParams['figure.figsize'] = (16,16)
            matplotlib.rcParams['xtick.minor.visible'] = True
            matplotlib.rcParams['axes.titlepad'] = 20
            matplotlib.rcParams['axes.linewidth'] = 2
            matplotlib.rcParams['xtick.major.size'] = 15
            matplotlib.rcParams['ytick.major.size'] = 15
            matplotlib.rcParams['xtick.minor.size'] = 10
            matplotlib.rcParams['ytick.minor.size'] = 10
            matplotlib.rcParams['xtick.major.width'] = 5
            matplotlib.rcParams['ytick.major.width'] = 5
            matplotlib.rcParams['xtick.minor.width'] = 5
            matplotlib.rcParams['ytick.minor.width'] = 5
            matplotlib.rcParams['axes.titlepad'] = 20
            matplotlib.rcParams['text.usetex'] = True
            #matplotlib.rc('font', family='serif')
            #matplotlib.rc('font', serif='cm10')
            #matplotlib.rc('font', size=fsize)
            #matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
        #lfsize = fsize - 19
        lfsize = fsize - 8

        pfmt = 'png'
        pdpi = prdict[pfmt]
        if plot_format in prdict:
            pfmt = plot_format
            pdpi = prdict[plot_format]
        if isinstance(plot_dpi,(int,float)) and int(plot_dpi) > 50:
            pdpi = int(plot_dpi)

        default_height = 5
        default_width = 6
        fig = None
        figm = None
        fplot_per_qq = True
        nplots = 0
        for qq in range(0,len(qlist)):
            if qlist[qq] in jqdict or (gradient and qlist[qq] in qgdict):
                nplots += 1
        if nplots > 0:
            if one_column:
                fig = plt.figure(figsize=(default_width, default_height * nplots))
                figm = plt.figure(figsize=(default_width, default_height * nplots))
                fplot_per_qq = False
            elif one_row:
                fig = plt.figure(figsize=(default_width * nplots, default_height))
                figm = plt.figure(figsize=(default_width * nplots, default_height))
                fplot_per_qq = False

        clist = ['forestgreen','darkorange','royalblue','gold','mediumorchid','deepskyblue','firebrick','olivedrab','mediumseagreen','violet','goldenrod']
        if isinstance(colors, (list, tuple)) and len(colors) >= len(flist):
            clist = copy.deepcopy(colors)
        #clist = ['b','r','m','y','k','c','g','orange','darkkhaki','saddlebrown','olive','lime','aquamarine','teal','skyblue','gray','salmon','maroon','tan','lightgreen','navy']
        #clist = ['forestgreen','darkorange','darkorange','royalblue','royalblue']
        #clist = ['darkorange','teal','teal','teal','teal','teal']
        stlist = ['-', '-', '-', '-' ,'-', '-', '-', '-', '-', '-', '-']
        #stlist = ['-', '-', '--', '-' ,'--', '-', '-', '--', '-.', ':']
        #stlist = ['-', '-', '--', '-:', '-.']
        if isinstance(lines, (list, tuple)) and len(lines) >= len(flist):
            stlist = copy.deepcopy(lines)
        (qx,xlabel,xunit,fxlabel,fxunit) = ptools.define_quantity(jqdict["XRHO"],exponent=None,derivative=None)
        pidx = 0
        for qq in range(0,len(qlist)):
            if qlist[qq] in jqdict or (gradient and qlist[qq] in qgdict):

                qtag = qgdict[qlist[qq]] if gradient else qlist[qq]
                ptag = 'd' + qlist[qq].lower() if gradient else qtag.lower()
                exp = qedict[qlist[qq]] if qlist[qq] in qedict else 0
                iname = None
                if qlist[qq].startswith("NIM"):
                    exp = qedict["NIMP"]
                    iidx = int(float(qlist[qq][3:]))
                    if ilist is not None and len(ilist) >= iidx:
                        imptag = ilist[iidx - 1]
                        (iname, ia, iz) = ptools.define_ion_species(short_name=imptag)
                        exp = exp + int(-1.1 * np.log(iz) - 0.5) + 2
                sc = np.power(10.0,exp) if isinstance(exp,int) and exp != 0 else 1.0
                gtag = qx if gradient else None
                (qy,ylabel,yunit,fylabel,fyunit) = ptools.define_quantity(qlist[qq],exponent=exp,derivative=gtag)
                if qy == "TI":
                    qy = "TI1"
                elif qy == "TIGRAD":
                    qy = "TI1GRAD"
                if qlist[qq].startswith("NIM") and iname is not None:
                    ylabel = iname + r' Density'
                    fylabel = r'$n_{\text{' + iname + r'}}$'

                ax = None
                axm = None
                if fplot_per_qq:
                    fig = plt.figure(figsize=(default_width, default_height))
                    ax = fig.add_subplot(1,1,1)
                    figm = plt.figure(figsize=(default_width, default_height))
                    axm = figm.add_subplot(1,1,1)
                else:
                    pidx += 1
                    if one_column:
                        ax = fig.add_subplot(nplots,1,pidx)
                        axm = fig.add_subplot(nplots,1,pidx)
                    elif one_row:
                        ax = fig.add_subplot(1,nplots,pidx)
                        axm = fig.add_subplot(1,nplots,pidx)
                fom = dict()
                lflag = False
                lmflag = False

                if ax is not None and ic is not None:
                    nqy = qy[:-4] if qy.endswith("GRAD") else qy
                    xtag = "PD_" + nqy + "X"
                    ytag = "PD_" + qy
                    etag = "PD_" + qy + "EB"
                    if ytag in ic and ic[ytag] is not None:
                        xi = copy.deepcopy(ic[xtag])
                        yi = copy.deepcopy(ic[ytag]) / sc
                        ffilt = np.all([np.isfinite(xi),np.isfinite(yi)],axis=0)
                        if xlist is not None or ylist is not None:
                            qqi = 0 if qq > len(xlist) or qq > len(ylist) else qq
                            ximin = xlist[qqi][0] if xlist is not None and xlist[qqi][0] is not None else np.nanmin(xi) - 0.1
                            ximax = xlist[qqi][1] if xlist is not None and xlist[qqi][1] is not None else np.nanmax(xi) + 0.1
                            yimin = ylist[qqi][0] if ylist is not None and ylist[qqi][0] is not None else np.nanmin(yi) - 0.1
                            yimax = ylist[qqi][1] if ylist is not None and ylist[qqi][1] is not None else np.nanmax(yi) + 0.1
                            ffilt = np.all([np.isfinite(xi),np.isfinite(yi),xi >= ximin,xi <= ximax,yi >= yimin,yi <= yimax],axis=0)
                        if not np.all(ffilt):
                            xi = xi[ffilt]
                            yi = yi[ffilt]
                        ax.plot(xi,yi,color='g',ls='-',label=r'GPR Fit / Input')
                        lflag = True
                        if etag in ic and ic[etag] is not None:
                            ei = copy.deepcopy(ic[etag]) / sc
                            sigtag = "%.1f" % (sig)
                            stag = r' Unc. $\left(' + sigtag + r'\sigma\right)$' if fancy else r' Unc. (' + sigtag + r' sig.)'
                            if not np.all(ffilt):
                                ei = ei[ffilt]
                            if pfmt == 'eps':
                                ax.plot(xi,yi - sig * ei,color='g',ls='--',label=r'GPR'+stag)
                                ax.plot(xi,yi + sig * ei,color='g',ls='--')
                            else:
                                ax.fill_between(xi,yi - sig * ei,yi + sig * ei,facecolor='g',edgecolor='None',alpha=0.2,label=r'GPR'+stag)

                if ax is not None and rd is not None:
                    nqy = qy
                    if qy == "TI1":
                        nqy = "TI" if "RD_TI" in rd else "TIMP"
                    xtag = "RD_" + nqy + "X"
                    ytag = "RD_" + nqy
                    etag = "RD_" + nqy + "EB"
                    if ytag in rd and rd[ytag] is not None:
                        xi = copy.deepcopy(rd[xtag])
                        yi = copy.deepcopy(rd[ytag])
                        ffilt = np.all([np.isfinite(xi),np.isfinite(yi)],axis=0)
                        if xlist is not None or ylist is not None:
                            ximin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xi) - 0.1
                            ximax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xi) + 0.1
                            yimin = ylist[qq][0] if ylist is not None and qq < len(ylist) and ylist[qq][0] is not None else np.nanmin(yi) - 0.1
                            yimax = ylist[qq][1] if ylist is not None and qq < len(ylist) and ylist[qq][1] is not None else np.nanmax(yi) + 0.1
                            ffilt = np.all([np.isfinite(xi),np.isfinite(yi),xi >= ximin,xi <= ximax,yi >= yimin,yi <= yimax],axis=0)
                        if not np.all(ffilt):
                            xi = xi[ffilt]
                            yi = yi[ffilt]
                        yi = yi / sc
                        ei = np.zeros(yi.shape)
                        lflag = True
                        if etag in rd and rd[etag] is not None:
                            ei = copy.deepcopy(rd[etag]) / sc
                            if not np.all(ffilt):
                                ei = ei[ffilt]
                        ax.errorbar(xi,yi,yerr=ei,color='gray',ls='',marker='D',markersize=2,label=r'Exp. Data',zorder=0)

                for jj in range(0,len(flist)):
                    jdata = db[jj]
                    if ax is not None and jdata:
                        tidx = -1
                        if isinstance(time,number_types) and float(time) > 0.0:
                            treq = float(time)
                            tt = jdata["TIME"].flatten()
                            if treq < tt[-1]:
                                tidx = np.where(tt > treq)[0][0]
                                if tidx > 0 and np.abs(tt[tidx] - treq) > np.abs(treq - tt[tidx-1]):
                                    tidx = tidx - 1
                        tval = jdata["TIME"].flatten()[tidx]

                        vnum = "2" if gradient else "1"
                        xx = jdata["XVEC"+vnum].flatten()
                        yy = jdata[qtag][tidx].flatten()
                        if gradient and re.match(r'^ANGF$',qtag):
                            yy = np.diff(jdata["ANGF"][tidx].flatten()) / np.diff(jdata["XVEC1"].flatten())
                        elif gradient and re.match(r'^VTOR$',qtag):
                            yy = np.diff(jdata["VTOR"][tidx].flatten()) / np.diff(jdata["XVEC1"].flatten())
                        elif gradient and re.match(r'^ZEFF$',qtag):
                            yy = np.diff(jdata["ZEFF"][tidx].flatten()) / np.diff(jdata["XVEC1"].flatten())
                        elif gradient:
                            yy = yy * np.diff(jdata["XRHO"][tidx].flatten()) / np.diff(jdata["XVEC1"].flatten())
                        ffilt = np.all([np.isfinite(xx),np.isfinite(yy)],axis=0)
                        if xlist is not None or ylist is not None:
                            ximin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xx) - 0.1
                            ximax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xx) + 0.1
                            yimin = ylist[qq][0] if ylist is not None and qq < len(ylist) and ylist[qq][0] is not None else np.nanmin(yy) - 0.1
                            yimax = ylist[qq][1] if ylist is not None and qq < len(ylist) and ylist[qq][1] is not None else np.nanmax(yy) + 0.1
                            ffilt = np.all([ffilt,xx >= ximin,xx <= ximax,yy >= yimin,yy <= yimax],axis=0)
                        if not np.all(ffilt):
                            xx = xx[ffilt]
                            yy = yy[ffilt]
                        yy = yy / sc
                        ltag = None
                        if llist is not None:
                            ltag = llist[jj]
                        if ltag is None:
                            temp = flist[jj].split('/')
                            ltag = temp[-1] if temp[-1] else temp[-2]
                        ax.plot(xx,yy,color=clist[jj],ls=stlist[jj],label=ltag,zorder=4)
                        if ltag is not None:
                            lflag = True

                        xtag = "PD_" + qy + "X"
                        ytag = "PD_" + qy + "GRAD" if gradient else "PD_" + qy
                        etag = "PD_" + qy + "GRADEB" if gradient else "PD_" + qy + "EB"
                        if axm is not None and ic is not None and oe is None and ytag in ic and ic[ytag] is not None and etag in ic and ic[etag] is not None:
                            xo = jdata["XVEC"+vnum].flatten()
                            yo = jdata[qtag][tidx].flatten() / sc
                            xi = copy.deepcopy(ic[xtag])
                            yi = copy.deepcopy(ic[ytag]) / sc
                            ei = copy.deepcopy(ic[etag]) / sc
                            emfunc = interp1d(xi,yi,bounds_error=False,fill_value='extrapolate')
                            esfunc = interp1d(xi,ei,bounds_error=False,fill_value='extrapolate')
                            muf = emfunc(xo)
                            mug = copy.deepcopy(yo)
                            stdf = esfunc(xo)
                            varf = np.power(stdf,2.0)
                            fom["S"] = np.exp(-(np.log(2.0) / 2.0) * np.power(mug - muf,2.0) / (2.0 * varf))
                            fom["NDP"] = np.exp(-np.power(mug - muf,2.0) / (2.0 * varf)) / np.sqrt(2.0 * np.pi * varf)
                            axm.plot(xo[ffilt],fom["S"][ffilt],color=clist[jj],label=ltag)
                            if ltag is not None:
                                lmflag = True

                if oe is not None:
                    nqy = qy[:-4] if qy.endswith("GRAD") else qy
                    xtag = "PD_D" + nqy + "X" if gradient else "PD_" + nqy + "X"
                    ytag = "PD_D" + nqy if gradient else "PD_" + nqy
                    etag = "PD_D" + nqy + "EB" if gradient else "PD_" + nqy + "EB"
                    if ax is not None and ytag in oe and oe[ytag] is not None:
                        xo = copy.deepcopy(oe[xtag])
                        yo = copy.deepcopy(oe[ytag]) / sc
                        eo = None
                        ffilt = np.all([np.isfinite(xo),np.isfinite(yo)],axis=0)
                        if xlist is not None or ylist is not None:
                            xomin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xo) - 0.1
                            xomax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xo) + 0.1
                            yomin = ylist[qq][0] if ylist is not None and qq < len(ylist) and ylist[qq][0] is not None else np.nanmin(yo) - 0.1
                            yomax = ylist[qq][1] if ylist is not None and qq < len(ylist) and ylist[qq][1] is not None else np.nanmax(yo) + 0.1
                            ffilt = np.all([np.isfinite(xo),np.isfinite(yo),xo >= xomin,xo <= xomax,yo >= yomin,yo <= yomax],axis=0)
                        if not np.all(ffilt):
                            xo = xo[ffilt]
                            yo = yo[ffilt]
                        ax.plot(xo,yo,color='r',ls='-.',label=r'MC Sim. Avg.',zorder=10)
                        lflag = True
                        if etag in oe and oe[etag] is not None:
                            eo = copy.deepcopy(oe[etag]) / sc
                            sigtag = "%.1f" % (sig)
                            stag = r' Env. $\left(' + sigtag + r'\sigma\right)$' if fancy else r' Env. ('  + sigtag + r' sig.)'
                            if not np.all(ffilt):
                                eo = eo[ffilt]
                            if pfmt == 'eps':
                                ax.plot(xo,yo - sig * eo,color='r',ls='--',label=r'Sim. Env.'+stag,zorder=5)
                                ax.plot(xo,yo + sig * eo,color='r',ls='--',zorder=5)
                            else:
                                ax.fill_between(xo,yo - sig * eo,yo + sig * eo,facecolor='r',edgecolor='None',alpha=0.2,label=r'Sim.'+stag,zorder=5)

                        if eo is not None and ic is not None and ytag in ic and ic[ytag] is not None and etag in ic and ic[etag] is not None:
                            nxtag = "PD_" + nqy + "X"
                            nytag = "PD_" + qy
                            netag = "PD_" + qy + "EB"
                            xi = copy.deepcopy(ic[nxtag])
                            yi = copy.deepcopy(ic[nytag]) / sc
                            ei = copy.deepcopy(ic[netag]) / sc
                            emfunc = interp1d(xi,yi,bounds_error=False,fill_value='extrapolate')
                            esfunc = interp1d(xi,ei,bounds_error=False,fill_value='extrapolate')
                            muf = emfunc(xo)
                            mug = copy.deepcopy(yo)
                            stdf = esfunc(xo)
                            stdg = copy.deepcopy(eo)
                            varf = np.power(stdf,2.0)
                            varg = np.power(stdg,2.0)
                            mufg = (muf * varg + mug * varf) / (varf + varg)
                            varfg = varf * varg / (varf + varg)
                            mud = muf - mug
                            vart = varf + varg
                            vard = varf - varg
                            ratf = np.sqrt(varf) / (np.abs(muf) + 1.0e-10)
                            ratg = np.sqrt(varg) / (np.abs(mug) + 1.0e-10)
                            fom["RICCI"] = np.exp(-np.power(mud,2.0) / vart)
                            fom["FOMA"] = np.exp(-np.power(mud,2.0) / (2.0 * vart))
                            fom["FOMPI"] = np.exp(-3.0 * ratf)
                            fom["FOMPO"] = np.exp(-3.0 * ratg)
                            fom["M"] = fom["FOMA"] * fom["FOMPI"] * fom["FOMPO"]
                            if hd is not None and "HIST_" + qtag in hd:
                                xvec = hd["HIST_" + qtag][:,0].flatten()
                                mvec = emfunc(xvec)
                                svec = esfunc(xvec)
                                bmat = np.transpose(hd["HIST_" + qtag][:,1::2]) / sc
                                sprob = np.transpose(hd["HIST_" + qtag][:,2::2])
                                chisq = np.zeros(xvec.shape)
                                kldiv = np.zeros(xvec.shape)
                                for hbin in np.arange(0,xvec.size):
                                    eprob = np.abs(norm.cdf(bmat[1:,hbin].flatten(),loc=mvec[hbin],scale=svec[hbin]) - norm.cdf(bmat[:-1,hbin].flatten(),loc=mvec[hbin],scale=svec[hbin]))
                                    oprob = sprob[:,hbin].flatten()
                                    ezfilt = (eprob < 1.0e-20)
                                    eprob[ezfilt] = 1.0e-20
                                    ozfilt = (oprob < 1.0e-20)
                                    oprob[ozfilt] = 1.0e-20
                                    dprob = np.power(oprob - eprob,2.0)
                                    chisq[hbin] = np.sum(dprob / eprob)
                                    kldiv[hbin] = np.sum(oprob * (np.log(oprob) - np.log(eprob)))  # * 2.0 * nn
                                fom["HX"] = xvec.copy()
                                fom["CHI2"] = 1.0 - chi2.cdf(chisq,np.full(xvec.shape,bmat.shape[0] - 2))
                                fom["KLD"] = np.exp(-kldiv)
                            fom["KLC"] = np.exp(-(np.power(mud,2.0) / (2.0 * varg) + np.log(stdg / stdf) + 0.5 * (varf / varg - 1.0)))

                            figd = plt.figure(figsize=(6,5))
                            axd = figd.add_subplot(111)
                            ffilt = np.all([np.isfinite(xo),np.isfinite(yo)],axis=0)
                            if xlist is not None:
                                xomin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xo) - 0.1
                                xomax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xo) + 0.1
                                ffilt = np.all([np.isfinite(xo),xo >= xomin,xo <= xomax],axis=0)
                            alabel = r'A' if not fancy else r'$A$'
                            axd.plot(xo[ffilt],fom["FOMA"][ffilt],color='r',label=alabel)
                            pilabel = r'P_i' if not fancy else r'$P_i$'
                            axd.plot(xo[ffilt],fom["FOMPI"][ffilt],color='g',label=pilabel)
                            polabel = r'P_o' if not fancy else r'$P_o$'
                            axd.plot(xo[ffilt],fom["FOMPO"][ffilt],color='b',label=polabel)
                            mlabel = r'M' if not fancy else r'$M$'
                            axd.plot(xo[ffilt],fom["M"][ffilt],color='k',label=mlabel)
                            sxl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                            if fancy:
                                sxl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                            syl = r'$M_{' + fylabel[1:-1] + r'}$' if fancy else ylabel + r' FOM'
                            axd.set_xlabel(sxl)
                            axd.set_ylabel(syl)
                            axd.set_ylim([-0.05,1.05])
                            if qq+1 in nlegends:
                                axd.legend(loc='best',prop={'size':lfsize})
                            figd.savefig(odir + oname + '_' + ptag + '_distfom.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)
                            plt.close(figd)

                            figc = plt.figure(figsize=(6,5))
                            axc = figc.add_subplot(111)
                            mlabel = r'M' if not fancy else r'$M$'
                            axc.plot(xo[ffilt],fom["M"][ffilt],color='k',label=mlabel)
                            rlabel = r'Ricci'
                            axc.plot(xo[ffilt],fom["RICCI"][ffilt],color='r',label=rlabel)
                            if "KLD" in fom:
                                hfilt = np.isfinite(fom["HX"])
                                if xlist is not None:
                                    hxmin = xlist[jj][0] if xlist is not None and xlist[jj][0] is not None else np.nanmin(fom["HX"]) - 0.1
                                    hxmax = xlist[jj][1] if xlist is not None and xlist[jj][1] is not None else np.nanmax(fom["HX"]) + 0.1
                                    hfilt = np.all([np.isfinite(fom["HX"]),fom["HX"] >= hxmin,fom["HX"] <= hxmax],axis=0)
                                kdlabel = r'K-L disc.'
                                axc.plot(fom["HX"][hfilt],fom["KLD"][hfilt],color='y',label=kdlabel)
                            kclabel = r'K-L cont.'
                            axc.plot(xo[ffilt],fom["KLC"][ffilt],color='b',label=kclabel)
                            sxl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                            if fancy:
                                sxl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                            syl = r'$M_{' + fylabel[1:-1] + r'}$' if fancy else ylabel + r' FOM'
                            axc.set_xlabel(sxl)
                            axc.set_ylabel(syl)
                            axc.set_ylim([-0.05,1.05])
                            if qq+1 in nlegends:
                                axc.legend(loc='best',prop={'size':lfsize})
                            figc.savefig(odir + oname + '_' + ptag + '_fomcomp.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)
                            plt.close(figc)

                if ax is not None:
                    xl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                    if fancy:
                        xl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                    yl = ylabel + r' [' + yunit + r']' if yunit else ylabel
                    if fancy:
                        yl = fylabel + r' [' + fyunit + r']' if fyunit else fylabel
                    if fplot_per_qq or pidx == nplots:
                        ax.set_xlabel(xl)
                    ax.set_ylabel(yl)
                    (xomin, xomax) = ax.get_xlim()
                    (qymin, qymax) = ax.get_ylim()
                    if qlist[qq] in ["NE","TE","NI","TI","Q","ZEFF"] and qymin >= 0.0:
                        qymin = -0.05 * qymax
                    elif qlist[qq] in ["NIM1","NIM2","NIM3"] and qymin >= 0.0:
                        qymin = 0.0
                    if xlist is not None and qq < len(xlist):
                        if xlist[qq][0] is not None and xlist[qq][0] > xomin:
                            xomin = xlist[qq][0]
                        if xlist[qq][1] is not None and xlist[qq][1] < xomax:
                            xomax = xlist[qq][1]
                    if ylist is not None and qq < len(ylist):
                        if ylist[qq][0] is not None and ylist[qq][0] > qymin:
                            qymin = ylist[qq][0]
                        if ylist[qq][1] is not None and ylist[qq][1] < qymax:
                            qymax = ylist[qq][1]
                    if xrlist is not None and qq < len(xrlist):
                        if xrlist[qq][0] is not None:
                            xomin = xrlist[qq][0]
                        if xrlist[qq][1] is not None:
                            xomax = xrlist[qq][1]
                    if yrlist is not None and qq < len(yrlist):
                        if yrlist[qq][0] is not None:
                            qymin = yrlist[qq][0]
                        if yrlist[qq][1] is not None:
                            qymax = yrlist[qq][1]
                    if hlines is not None:
                        for nn in np.arange(0, hlines.size):
                            htemp = hlines[nn]
                            if htemp > qymin and htemp < qymax:
                                ax.plot([xomin, xomax], [htemp, htemp], color='k', ls='--')
                    if vlines is not None:
                        for nn in np.arange(0, vlines.size):
                            vtemp = vlines[nn]
                            if vtemp > xomin and vtemp < xomax:
                                ax.plot([vtemp, vtemp], [qymin, qymax], color='k', ls='--')
                    ax.set_xlim([xomin, xomax])
                    ax.set_ylim([qymin, qymax])
                    if lflag and qq+1 in nlegends:
                        ax.legend(loc='best',prop={'size':lfsize})
                    if isinstance(time,number_types):
                        tstr = "%.2f" % (float(time + jet_time_offset))
                        titlestr = "time = " + tstr if not fancy else r'$t = ' + tstr + r'$'
                        ax.set_title(titlestr)
                    if fplot_per_qq:
                        if not fancy:
                            snstr = "%d" % (shot)
                            fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
                        fig.savefig(odir + oname + '_' + ptag + '.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

                if axm is not None and ic is not None and oe is None:
                    sxl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                    if fancy:
                        sxl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                    syl = r'$S_{' + fylabel[1:-1] + r'}$' if fancy else ylabel + r' FOM'
                    if fplot_per_qq or pidx == nplots:
                        axm.set_xlabel(sxl)
                    axm.set_ylabel(syl)
                    axm.set_ylim(-0.05,1.05)
                    if qq+1 in nlegends:
                        axm.legend(loc='best',prop={'size':lfsize})
                    if isinstance(time,number_types):
                        tstr = "%.2f" % (float(time + jet_time_offset))
                        titlestr = "time = " + tstr if not fancy else r'$t = ' + tstr + r'$'
                        axm.set_title(titlestr)
                    if fplot_per_qq:
                        if not fancy:
                            snstr = "%d" % (shot)
                            figm.suptitle(device + ' #' + snstr, fontsize=lfsize)
                        figm.savefig(odir + oname + '_' + ptag + '_ptfom.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

                if fplot_per_qq and fig is not None:
                    plt.close(fig)
                if fplot_per_qq and figm is not None:
                    plt.close(figm)

        if not fplot_per_qq:
            snstr = "%d" % (shot)
            fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
            fig.savefig(odir + oname + '_all.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)
            figm.suptitle(device + ' #' + snstr, fontsize=lfsize)
            figm.savefig(odir + oname + '_all_ptfom.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

        if fig is not None:
            plt.close(fig)
        if figm is not None:
            plt.close(figm)


def plot_jst(datafiles,quantities,outname,outdir='./',ti=None,tf=None,sigma=2.0,legend=None,labels=None, \
             rdfields=None,xlimit=None,ylimit=None,xmarks=None,ymarks=None,xplot=None,yplot=None,scale=None, \
             fancy=False,fontsize=14,plot_format='png',plot_dpi=300,one_column=False,one_row=False,impurities=None):

    fname = 'jetto.jst'
    flist = None
    qlist = None
    oname = 'temp'
    if isinstance(datafiles,list):
        flist = datafiles
    if isinstance(quantities,str):
        temp = quantities.split(',')
        qlist = []
        for ii in range(0,len(temp)):
            qlist.append(temp[ii].strip())
        if len(qlist) <= 0:
            qlist = None
    if isinstance(quantities,list):
        qlist = quantities
    if isinstance(outname,str):
        mm = re.match(r'^(.*)\.([a-z0-9]+)$',outname)
        if mm:
            oname = mm.group(1)
            plot_format = mm.group(2)
        else:
            oname = outname

    fgood = False
    device = 'JET'
    shot = None
    db = []
    for ii in np.arange(0,len(flist)):
        jdata = None
        if isinstance(flist[ii],str):
            fdir = flist[ii]
            if not fdir.endswith('/'):
                fdir = fdir + '/'
            if os.path.exists(flist[ii]):
                jdata = jread.read_binary_file(fdir + fname)
                fgood = True
            else:
                print("JETTO JST file, %s, not found" % (fdir + fname))
        db.append(copy.deepcopy(jdata))
        if jdata is not None:
            if shot is None and "SHOT" in jdata:
                shot = int(jdata["SHOT"])

    scg = 1.0
    scgi = None
    if isinstance(scale,number_types):
        scg = float(scale)
    elif isinstance(scale,array_types) and len(scale) > 0 and isinstance(scale[0],number_types):
        scgi = int(scale[0])

    if fgood and qlist is not None:
        jqdict = {"TVEC1":"TIME", "PRAD":"PRAD", "ZEFF":"ZEFF", "LI3":"LI3", "NEAX":"NEAX", "NEAV":"NEBAR", "TEAX":"TEAX", "TEAV":"TEBAR", "Q0":"Q0", "Q95":"Q95", \
                  "IMC1":"FIMP1", "IMC2":"FIMP2", "IMC3":"FIMP3", "NEL":"NELIN", "BTTT":"BETATEXP", "BPTT":"BETAPEXP", "BNTT":"BETANEXP", "POH":"POHM"}
        qedict = {"PRAD":6, "Q":0, "ZEFF":0, "LI3":0, "NEAX":19, "NEAV":19, "TEAX":3, "TEAV":3, "Q0":0, "Q95":0, "FIMP":0, "NEL":19, "BTTT":0, "BPTT":0, "BNTT":0, "POH":6}
        prdict = {'jpg':300, 'bmp':300, 'png':300, 'ps':1200, 'svg':1200, 'eps':1200, 'pdf':1200}

        odir = outdir if isinstance(outdir,str) else './'
        if odir is not None and not odir.endswith('/'):
            odir = odir + '/'
        if not os.path.exists(odir):
            os.makedirs(odir)

        sig = float(sigma) if isinstance(sigma,number_types) and float(sigma) > 0.0 else 2.0

        rd = None
        if isinstance(rdfields,array_types) and len(rdfields) > 0:
            rd = {}
            for ii in range(0,len(rdfields)):
                if rdfields[ii] is not None and ii < len(qlist):
                    rd[qlist[ii]] = rdfields[ii]
            if len(rd) == 0:
                rd = None

        llist = None
        if isinstance(labels,str):
            temp = labels.split(',')
            llist = []
            for ii in range(0,len(temp)):
                llist.append(temp[ii].strip())
            if len(qlist) <= 0:
                llist = None
        elif isinstance(labels,list):
            llist = labels
        if llist is not None:
            while len(llist) < len(db):
                llist.append(None)

        ilist = None
        if isinstance(impurities,str):
            temp = impurities.split(',')
            ilist = []
            for ii in range(0,len(temp)):
                ilist.append(temp[ii].strip())
            if len(ilist) <= 0:
                ilist = None
        elif isinstance(impurities,list):
            ilist = impurities

        xlist = None
        ylist = None
        if isinstance(xlimit,tuple) and len(xlimit) == 2:
            xlist = []
            while len(xlist) < len(qlist):
                xlist.append(tuple(xlimit))
        elif isinstance(xlimit,list):
            for ii in range(0,len(xlimit)):
                if isinstance(xlimit[ii],(list,tuple)) and len(xlimit[ii]) == 2:
                    xlist.append(tuple(xlimit[ii]))
                else:
                    xlist.append((None,None))
            while len(xlist) < len(qlist):
                xlist.append((None,None))
        if isinstance(ylimit,tuple) and len(ylimit) == 2:
            ylist = []
            while len(ylist) < len(qlist):
                ylist.append(tuple(ylimit))
        elif isinstance(ylimit,list):
            for ii in range(0,len(ylimit)):
                if isinstance(ylimit[ii],(list,tuple)) and len(ylimit[ii]) == 2:
                    ylist.append(tuple(ylimit[ii]))
                else:
                    ylist.append((None,None))
            while len(ylist) < len(qlist):
                ylist.append((None,None))

        xrlist = None
        yrlist = None
        if isinstance(xplot,tuple) and len(xplot) == 2:
            xrlist = []
            while len(xrlist) < len(qlist):
                xrlist.append(tuple(xplot))
        elif isinstance(xplot,list):
            xrlist = []
            for ii in range(0,len(xplot)):
                if isinstance(xplot[ii],(list,tuple)) and len(xplot[ii]) == 2:
                    xrlist.append(tuple(xplot[ii]))
                else:
                    xrlist.append((None,None))
            while len(xrlist) < len(qlist):
                xrlist.append((None,None))
        if isinstance(yplot,tuple) and len(yplot) == 2:
            yrlist = []
            while len(yrlist) < len(qlist):
                yrlist.append(tuple(yplot))
        elif isinstance(yplot,list):
            yrlist = []
            for ii in range(0,len(yplot)):
                if isinstance(yplot[ii],(list,tuple)) and len(yplot[ii]) == 2:
                    yrlist.append(tuple(yplot[ii]))
                else:
                    yrlist.append((None,None))
            while len(yrlist) < len(qlist):
                yrlist.append((None,None))

        pti = ti if isinstance(ti,number_types) else None
        ptf = tf if isinstance(tf,number_types) else None

        hlines = None
        vlines = None
        if isinstance(ymarks, number_types):
            hlines = np.array([float(ymarks)])
        elif isinstance(ymarks, array_types):
            hlines = np.array(ymarks)
        if isinstance(xmarks, number_types):
            vlines = np.array([float(xmarks)])
        elif isinstance(xmarks, array_types):
            vlines = np.array(xmarks)

        nlegends = []
        if isinstance(legend,number_types) and int(legend) > 0:
            nlegends.append(int(legend))
        elif isinstance(legend,array_types):
            for ii in range(0,len(legend)):
                if isinstance(legend[ii],number_types) and int(legend[ii]) > 0:
                    nlegends.append(int(legend[ii]))
        elif isinstance(legend,str):
            if legend == "all":
                for ii in range(0,len(qlist)):
                    nlegends.append(ii+1)

        fsize = 14
        if isinstance(fontsize,(int,float)) and int(fontsize) > 6:
            fsize = int(fontsize)
        if fancy:
            matplotlib.rc('font', family='serif')
            #matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=fsize)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'
        lfsize = fsize - 8

        pfmt = 'png'
        pdpi = prdict[pfmt]
        if plot_format in prdict:
            pfmt = plot_format
            pdpi = prdict[plot_format]
        if isinstance(plot_dpi,(int,float)) and int(plot_dpi) > 50:
            pdpi = int(plot_dpi)

        default_height = 5
        default_width = 6
        fig = None
        fplot_per_qq = True
        nplots = 0
        for qq in range(0,len(qlist)):
            if qlist[qq] in jqdict:
                nplots += 1
        if nplots > 0:
            if one_column:
                fig = plt.figure(figsize=(default_width, default_height * nplots))
                fplot_per_qq = False
            elif one_row:
                fig = plt.figure(figsize=(default_width * nplots, default_height))
                fplot_per_qq = False

        clist = ['b','r','m','y','k','c','g','orange','darkkhaki','saddlebrown','olive','lime','aquamarine','teal','skyblue','gray','salmon','maroon','tan','lightgreen','navy']
        (qx,xlabel,xunit,fxlabel,fxunit) = ptools.define_quantity(jqdict["TVEC1"],exponent=None,derivative=None)
        pidx = 0
        for qq in range(0,len(qlist)):
            if qlist[qq] in jqdict:

                qtag = qlist[qq]
                ptag = qtag.lower()
                exp = qedict[qlist[qq]] if qlist[qq] in qedict else 0
                iname = None
                if qlist[qq].startswith("IMC"):
                    exp = qedict["FIMP"]
                    iidx = int(float(qlist[qq][3:]))
                    if ilist is not None and len(ilist) >= iidx:
                        imptag = ilist[iidx - 1]
                        (iname, ia, iz) = ptools.define_ion_species(short_name=imptag)
                        exp = int(-1.1 * np.log(iz) - 0.5)
                sc = np.power(10.0,exp) if isinstance(exp,int) and exp != 0 else 1.0
                if qlist[qq].startswith("IMC"):
                    sc = sc * 100.0
                (qy,ylabel,yunit,fylabel,fyunit) = ptools.define_quantity(qlist[qq],exponent=exp,derivative=None)
                if qlist[qq].startswith("IMC") and iname is not None:
                    ylabel = iname + r' Density Fraction'
                    fylabel = r'$n_{\text{' + iname + r'}} / n_e$'

                ax = None
                if fplot_per_qq:
                    fig = plt.figure(figsize=(default_width, default_height))
                    ax = fig.add_subplot(1,1,1)
                else:
                    pidx += 1
                    if one_column:
                        ax = fig.add_subplot(nplots,1,pidx)
                    elif one_row:
                        ax = fig.add_subplot(1,nplots,pidx)
                lflag = False

                for jj in range(0,len(flist)):
                    jdata = db[jj]
                    if ax is not None and jdata and qtag in jdata:

                        xx = jdata["TVEC1"].flatten()
                        yy = scg * jdata[qtag].flatten()
                        if qtag == "PRAD":
                            yy = -yy
                        if scgi is not None:
                            sc = 1.0
                            syy = scg * db[scgi][qtag].flatten() if scg != 0.0 else db[scgi][qtag].flatten()
                            yy = yy / syy
                        ffilt = np.all([np.isfinite(xx),np.isfinite(yy)],axis=0)
                        if xlist is not None or ylist is not None:
                            ximin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xx) - 0.1
                            ximax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xx) + 0.1
                            yimin = ylist[qq][0] if ylist is not None and qq < len(ylist) and ylist[qq][0] is not None else np.nanmin(yy) - 0.1
                            yimax = ylist[qq][1] if ylist is not None and qq < len(ylist) and ylist[qq][1] is not None else np.nanmax(yy) + 0.1
                            ffilt = np.all([np.isfinite(xx),np.isfinite(yy),xx >= ximin,xx <= ximax,yy >= yimin,yy <= yimax],axis=0)
                        if not np.all(ffilt):
                            xx = xx[ffilt]
                            yy = yy[ffilt]
                        yy = yy / sc
                        ltag = None
                        if llist is not None:
                            ltag = llist[jj]
                        if llist is not None and ltag is None:
                            temp = flist[jj].split('/')
                            ltag = temp[-1] if temp[-1] else temp[-2]
                        if ltag is not None:
                            lflag = True
                        ax.plot(xx + jet_time_offset,yy,color=clist[jj],label=ltag,zorder=10)

                if ax is not None and scgi is None and rd is not None and qtag in rd:
                    dda = rd[qtag]
                    seq = None
                    mm = re.search(r'^(.+):([0-9]*)$',dda,flags=re.IGNORECASE)
                    if mm:
                        dda = mm.group(1)
                        seq = int(mm.group(2))
                    sig = xppf.getsig(shot,pti,ptf,dda,favg=False,seq=seq,uid=None)
                    if sig is not None:
                        rxx = sig[0]["TVEC"].flatten()
                        ryy = scg * sig[0]["DATA"].flatten() / sc
                        bidxv = np.where(rxx > np.nanmin(xx))[0]
                        bidx = bidxv[0] if len(bidxv) > 0 else None
                        eidxv = np.where(rxx > np.nanmax(xx))[0]
                        eidx = eidxv[0] if len(eidxv) > 0 else None
                        if (bidx is not None or eidx is not None) and bidx != eidx:
                            rxx = rxx[bidx:eidx]
                            ryy = ryy[bidx:eidx]
                            ax.plot(rxx + jet_time_offset,ryy,color='gray',label=r'Exp. Data ('+dda.upper()+r')',zorder=0)

                if ax is not None:
                    if scgi is not None:
                        ylabel = ylabel + r' / ' + ylabel + r'_ref'
                        yunit = r'-'
                        fylabel = fylabel + r' / ' + fylabel + r'$_{,\text{ref}}$'
                        fyunit = r'-'
                    xl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                    if fancy:
                        xl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                    yl = ylabel + r' [' + yunit + r']' if yunit else ylabel
                    if fancy:
                        yl = fylabel + r' [' + fyunit + r']' if fyunit else fylabel
                    if fplot_per_qq or pidx == nplots:
                        ax.set_xlabel(xl)
                    ax.set_ylabel(yl)
                    (xomin, xomax) = ax.get_xlim()
                    xomin = xomin - jet_time_offset
                    xomax = xomax - jet_time_offset
                    if pti is not None:
                        xomin = pti
                    if ptf is not None:
                        xomax = ptf
                    (qymin, qymax) = ax.get_ylim()
                    if xlist is not None and qq < len(xlist):
                        if xlist[qq][0] is not None and xlist[qq][0] > xomin:
                            xomin = xlist[qq][0]
                        if xlist[qq][1] is not None and xlist[qq][1] < xomax:
                            xomax = xlist[qq][1]
                    if ylist is not None and qq < len(ylist):
                        if ylist[qq][0] is not None and ylist[qq][0] > qymin:
                            qymin = ylist[qq][0]
                        if ylist[qq][1] is not None and ylist[qq][1] < qymax:
                            qymax = ylist[qq][1]
                    if xrlist is not None and qq < len(xrlist):
                        if xrlist[qq][0] is not None:
                            xomin = xrlist[qq][0]
                        if xrlist[qq][1] is not None:
                            xomax = xrlist[qq][1]
                    if yrlist is not None and qq < len(yrlist):
                        if yrlist[qq][0] is not None:
                            qymin = yrlist[qq][0]
                        if yrlist[qq][1] is not None:
                            qymax = yrlist[qq][1]
                    xomin = xomin + jet_time_offset
                    xomax = xomax + jet_time_offset
                    if hlines is not None:
                        for nn in np.arange(0, hlines.size):
                            htemp = hlines[nn]
                            if htemp > qymin and htemp < qymax:
                                ax.plot([xomin, xomax], [htemp, htemp], color='k', ls='--')
                    if vlines is not None:
                        for nn in np.arange(0, vlines.size):
                            vtemp = vlines[nn] + jet_time_offset
                            if vtemp > xomin and vtemp < xomax:
                                ax.plot([vtemp, vtemp], [qymin, qymax], color='k', ls='--')
                    ax.set_xlim([xomin, xomax])
                    ax.set_ylim([qymin, qymax])
                    if lflag and qq+1 in nlegends:
                        ax.legend(loc='best', prop={'size':lfsize})
                    if fplot_per_qq:
                        if not fancy:
                            snstr = "%d" % (shot)
                            fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
                        fig.savefig(odir + oname + '_' + ptag + '.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

                if fplot_per_qq and fig is not None:
                    plt.close(fig)

        if not fplot_per_qq:
            if not fancy:
                snstr = "%d" % (shot)
                fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
            fig.savefig(odir + oname + '_all.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

        if fig is not None:
            plt.close(fig)


def plot_jsp_in_time(datafiles,quantities,rho,outname,outdir='./',sigma=2.0,legend=None,labels=None,gradient=False, \
                     xlimit=None,ylimit=None,xmarks=None,ymarks=None,xplot=None,yplot=None, \
                     fancy=False,fontsize=14,plot_format='png',plot_dpi=300,one_column=False,one_row=False,impurities=None):

    fname = 'jetto.jsp'
    flist = None
    qlist = None
    prho = None
    oname = 'temp'
    if isinstance(datafiles,list):
        flist = datafiles
    if isinstance(quantities,str):
        temp = quantities.split(',')
        qlist = []
        for ii in range(0,len(temp)):
            qlist.append(temp[ii].strip())
        if len(qlist) <= 0:
            qlist = None
    if isinstance(quantities,list):
        qlist = quantities
    if isinstance(rho,number_types) and float(rho) >= 0.0 and float(rho) <= 1.0:
        prho = float(rho)
    if isinstance(outname,str):
        mm = re.match(r'^(.*)\.([a-z0-9]+)$',outname)
        if mm:
            oname = mm.group(1)
            plot_format = mm.group(2)
        else:
            oname = outname

    fgood = False
    device = 'JET'
    shot = None
    db = []
    for ii in np.arange(0,len(flist)):
        jdata = None
        if isinstance(flist[ii],str):
            fdir = flist[ii]
            if not fdir.endswith('/'):
                fdir = fdir + '/'
            if os.path.exists(flist[ii]):
                jdata = jread.read_binary_file(fdir + fname)
                fgood = True
            else:
                print("JETTO JSP file, %s, not found" % (fdir + fname))
        db.append(copy.deepcopy(jdata))
        if jdata is not None:
            if shot is None and "SHOT" in jdata:
                shot = int(jdata["SHOT"])

    if fgood and qlist is not None and prho is not None:
        jqdict = {"TIME":"TIME", "XVEC1":"RHOTORN", "XVEC2":"RHOTORN", "XRHO":"RHOTORN", "RHO":"PSITOR", "XPSI":"PSIPOLN", "PSI":"PSIPOL", "R":"RMAJORO", "RI":"RMAJORI", \
                  "NE":"NE", "TE":"TE", "NI":"NI1", "TI":"TI1", "ANGF":"AFTOR", "VTOR":"UTOR", "Q":"Q", "VOL":"FSVOL", "SURF":"FSAREA", "ZEFF":"ZEFF", \
                  "NIM1":"NIMP1", "NIM2":"NIMP2", "NIM3":"NIMP3", "QRAD":"QRAD", "QTHX":"QEX", "QOH":"QOHM", \
                  "XE1":"XENC", "XE3":"XETURB", "XE6":"XETURB", "XI1":"XINC", "XI3":"XITURB", "XI6":"XITURB"}
        qedict = {"NE":19, "TE":3, "NI":19, "NIMP":17, "TI":3, "ANGF":4, "VTOR":4, "Q":0, "ZEFF":0, "QRAD":6, "QTHX":6, "QOH":6}
        qgdict = {"NE":"GRNE", "TE":"GRTE", "TI":"GRTI", "ANGF":"ANGF", "VTOR":"VTOR", "Q":"SH", "ZEFF":"ZEFF"}
        prdict = {'jpg':300, 'bmp':300, 'png':300, 'ps':1200, 'svg':1200, 'eps':1200, 'pdf':1200}

        odir = outdir if isinstance(outdir,str) else './'
        if odir is not None and not odir.endswith('/'):
            odir = odir + '/'
        if not os.path.exists(odir):
            os.makedirs(odir)

        sig = float(sigma) if isinstance(sigma,number_types) and float(sigma) > 0.0 else 2.0

        llist = None
        if isinstance(labels,str):
            temp = labels.split(',')
            llist = []
            for ii in range(0,len(temp)):
                llist.append(temp[ii].strip())
            if len(qlist) <= 0:
                llist = None
        elif isinstance(labels,list):
            llist = labels
        if llist is not None:
            while len(llist) < len(db):
                llist.append(None)

        ilist = None
        if isinstance(impurities,str):
            temp = impurities.split(',')
            ilist = []
            for ii in range(0,len(temp)):
                ilist.append(temp[ii].strip())
            if len(ilist) <= 0:
                ilist = None
        elif isinstance(impurities,list):
            ilist = impurities

        xlist = None
        ylist = None
        if isinstance(xlimit,tuple) and len(xlimit) == 2:
            xlist = []
            while len(xlist) < len(qlist):
                xlist.append(tuple(xlimit))
        elif isinstance(xlimit,list):
            for ii in range(0,len(xlimit)):
                if isinstance(xlimit[ii],(list,tuple)) and len(xlimit[ii]) == 2:
                    xlist.append(tuple(xlimit[ii]))
                else:
                    xlist.append((None,None))
            while len(xlist) < len(qlist):
                xlist.append((None,None))
        if isinstance(ylimit,tuple) and len(ylimit) == 2:
            ylist = []
            while len(ylist) < len(qlist):
                ylist.append(tuple(ylimit))
        elif isinstance(ylimit,list):
            for ii in range(0,len(ylimit)):
                if isinstance(ylimit[ii],(list,tuple)) and len(ylimit[ii]) == 2:
                    ylist.append(tuple(ylimit[ii]))
                else:
                    ylist.append((None,None))
            while len(ylist) < len(qlist):
                ylist.append((None,None))

        xrlist = None
        yrlist = None
        if isinstance(xplot,tuple) and len(xplot) == 2:
            xrlist = []
            while len(xrlist) < len(qlist):
                xrlist.append(tuple(xplot))
        elif isinstance(xplot,list):
            xrlist = []
            for ii in range(0,len(xplot)):
                if isinstance(xplot[ii],(list,tuple)) and len(xplot[ii]) == 2:
                    xrlist.append(tuple(xplot[ii]))
                else:
                    xrlist.append((None,None))
            while len(xrlist) < len(qlist):
                xrlist.append((None,None))
        if isinstance(yplot,tuple) and len(yplot) == 2:
            yrlist = []
            while len(yrlist) < len(qlist):
                yrlist.append(tuple(yplot))
        elif isinstance(yplot,list):
            yrlist = []
            for ii in range(0,len(yplot)):
                if isinstance(yplot[ii],(list,tuple)) and len(yplot[ii]) == 2:
                    yrlist.append(tuple(yplot[ii]))
                else:
                    yrlist.append((None,None))
            while len(yrlist) < len(qlist):
                yrlist.append((None,None))

        hlines = None
        vlines = None
        if isinstance(ymarks, number_types):
            hlines = np.array([float(ymarks)])
        elif isinstance(ymarks, array_types):
            hlines = np.array(ymarks)
        if isinstance(xmarks, number_types):
            vlines = np.array([float(xmarks)])
        elif isinstance(xmarks, array_types):
            vlines = np.array(xmarks)

        nlegends = []
        if isinstance(legend,number_types) and int(legend) > 0:
            nlegends.append(int(legend))
        elif isinstance(legend,array_types):
            for ii in range(0,len(legend)):
                if isinstance(legend[ii],number_types) and int(legend[ii]) > 0:
                    nlegends.append(int(legend[ii]))
        elif isinstance(legend,str):
            if legend == "all":
                for ii in range(0,len(qlist)):
                    nlegends.append(ii+1)

        fsize = 14
        if isinstance(fontsize,(int,float)) and int(fontsize) > 6:
            fsize = int(fontsize)
        if fancy:
            matplotlib.rc('font', family='serif')
            matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=fsize)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
        lfsize = fsize - 8

        pfmt = 'png'
        pdpi = prdict[pfmt]
        if plot_format in prdict:
            pfmt = plot_format
            pdpi = prdict[plot_format]
        if isinstance(plot_dpi,(int,float)) and int(plot_dpi) > 50:
            pdpi = int(plot_dpi)

        default_height = 5
        default_width = 6
        fig = None
        fplot_per_qq = True
        nplots = 0
        for qq in range(0,len(qlist)):
            if qlist[qq] in jqdict:
                nplots += 1
        if nplots > 0:
            if one_column:
                fig = plt.figure(figsize=(default_width, default_height * nplots))
                fplot_per_qq = False
            elif one_row:
                fig = plt.figure(figsize=(default_width * nplots, default_height))
                fplot_per_qq = False

        clist = ['b','r','m','y','k','c','g','orange','darkkhaki','saddlebrown','olive','lime','aquamarine','teal','skyblue','gray','salmon','maroon','tan','lightgreen','navy']
        (qx,xlabel,xunit,fxlabel,fxunit) = ptools.define_quantity(jqdict["TIME"],exponent=None,derivative=None)
        pidx = 0
        rtag = "%d" % (int(prho * 100))
        for qq in range(0,len(qlist)):
            if qlist[qq] in jqdict:

                snlist = []
                qtag = qgdict[qlist[qq]] if gradient else qlist[qq]
                ptag = 'd' + qlist[qq].lower() if gradient else qtag.lower()
                exp = qedict[qlist[qq]] if qlist[qq] in qedict else 0
                iname = None
                if qlist[qq].startswith("NIM"):
                    exp = qedict["NIMP"]
                    iidx = int(float(qlist[qq][3:]))
                    if ilist is not None and len(ilist) >= iidx:
                        imptag = ilist[iidx - 1]
                        (iname, ia, iz) = ptools.define_ion_species(short_name=imptag)
                        exp = exp + int(-1.1 * np.log(iz) - 0.5) + 2
                sc = np.power(10.0,exp) if isinstance(exp,int) and exp != 0 else 1.0
                (qg,glabel,gunit,fglabel,fgunit) = ptools.define_quantity(jqdict["XRHO"],exponent=None,derivative=None)
                gtag = qg if gradient else None
                ytemptag = qlist[qq] if not qlist[qq].startswith('X') else jqdict[qlist[qq]]
                (qy,ylabel,yunit,fylabel,fyunit) = ptools.define_quantity(ytemptag,exponent=exp,derivative=gtag)
                if qy == "TI":
                    qy = "TI1"
                elif qy == "TIGRAD":
                    qy = "TI1GRAD"
                if qlist[qq].startswith("NIM") and iname is not None:
                    ylabel = iname + r' Density'
                    fylabel = r'$n_{\text{' + iname + r'}}$'

                ax = None
                if fplot_per_qq:
                    fig = plt.figure(figsize=(default_width, default_height))
                    ax = fig.add_subplot(1,1,1)
                else:
                    pidx += 1
                    if one_column:
                        ax = fig.add_subplot(nplots,1,pidx)
                    elif one_row:
                        ax = fig.add_subplot(1,nplots,pidx)
                lflag = False

                rmajomin = None
                rmajomax = None
                for jj in range(0,len(flist)):
                    jdata = db[jj]
                    if ax is not None and jdata:

                        vnum = "2" if gradient else "1"
                        rr = jdata["XVEC"+vnum].flatten()
                        ridx = -1
                        if prho < rr[-1]:
                            ridx = np.where(rr > prho)[0][0]
                            if ridx > 0 and np.abs(rr[ridx] - prho) > np.abs(prho - rr[ridx-1]):
                                ridx = ridx - 1
                        if "R" in jdata:
                            rmajo = jdata["R"][:,ridx].flatten()
                            if rmajomin is None:
                                rmajomin = float(np.nanmin(rmajo))
                            elif np.nanmin(rmajo) < rmajomin:
                                rmajomin = float(np.nanmin(rmajo))
                            if rmajomax is None:
                                rmajomax = float(np.nanmax(rmajo))
                            elif np.nanmax(rmajo) > rmajomax:
                                rmajomax = float(np.nanmax(rmajo))

                        xx = jdata["TIME"].flatten()
                        yy = jdata[qtag][:,ridx].flatten() / sc
                        if gradient and re.match(r'^ANGF$',qtag):
                            tyy = np.diff(jdata["ANGF"],axis=1) / np.diff(jdata["XVEC1"]) / sc
                            yy = tyy[:,ridx].flatten()
                        elif gradient and re.match(r'^VTOR$',qtag):
                            tyy = np.diff(jdata["VTOR"],axis=1) / np.diff(jdata["XVEC1"]) / sc
                            yy = tyy[:,ridx].flatten()
                        elif gradient and re.match(r'^ZEFF$',qtag):
                            tyy = np.diff(jdata["ZEFF"],axis=1) / np.diff(jdata["XVEC1"]) / sc
                            yy = tyy[:,ridx].flatten()
                        elif gradient:
                            tyy = np.diff(jdata["XRHO"],axis=1) / np.diff(jdata["XVEC1"])
                            yy = yy * tyy[:,ridx].flatten()
                        ffilt = np.all([np.isfinite(xx),np.isfinite(yy)],axis=0)
                        if xlist is not None or ylist is not None:
                            ximin = xlist[jj][0] if xlist is not None and xlist[jj][0] is not None else np.nanmin(xi) - 0.1
                            ximax = xlist[jj][1] if xlist is not None and xlist[jj][1] is not None else np.nanmax(xi) + 0.1
                            yimin = ylist[jj][0] if ylist is not None and ylist[jj][0] is not None else np.nanmin(yi) - 0.1
                            yimax = ylist[jj][1] if ylist is not None and ylist[jj][1] is not None else np.nanmax(yi) + 0.1
                        if not np.all(ffilt):
                            xx = xx[ffilt]
                            yy = yy[ffilt]
                        ltag = None
                        if llist is not None:
                            ltag = llist[jj]
                        if llist is not None and ltag is None:
                            temp = flist[jj].split('/')
                            ltag = temp[-1] if temp[-1] else temp[-2]
                        if ltag is not None:
                            lflag = True
                        ax.plot(xx + jet_time_offset,yy,color=clist[jj],label=ltag)

                if ax is not None:
                    xl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                    if fancy:
                        xl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                    yl = ylabel + r' [' + yunit + r']' if yunit else ylabel
                    if fancy:
                        yl = fylabel + r' [' + fyunit + r']' if fyunit else fylabel
                    if fplot_per_qq or pidx == nplots:
                        ax.set_xlabel(xl)
                    ax.set_ylabel(yl)
                    (xomin, xomax) = ax.get_xlim()
                    xomin = xomin - jet_time_offset
                    xomax = xomax - jet_time_offset
                    (qymin, qymax) = ax.get_ylim()
                    if xlist is not None and qq < len(xlist):
                        if xlist[qq][0] is not None and xlist[qq][0] > xomin:
                            xomin = xlist[qq][0]
                        if xlist[qq][1] is not None and xlist[qq][1] < xomax:
                            xomax = xlist[qq][1]
                    if ylist is not None and qq < len(ylist):
                        if ylist[qq][0] is not None and ylist[qq][0] > qymin:
                            qymin = ylist[qq][0]
                        if ylist[qq][1] is not None and ylist[qq][1] < qymax:
                            qymax = ylist[qq][1]
                    if xrlist is not None and qq < len(xrlist):
                        if xrlist[qq][0] is not None:
                            xomin = xrlist[qq][0]
                        if xrlist[qq][1] is not None:
                            xomax = xrlist[qq][1]
                    if yrlist is not None and qq < len(yrlist):
                        if yrlist[qq][0] is not None:
                            qymin = yrlist[qq][0]
                        if yrlist[qq][1] is not None:
                            qymax = yrlist[qq][1]
                    xomin = xomin + jet_time_offset
                    xomax = xomax + jet_time_offset
                    if hlines is not None:
                        for nn in np.arange(0, hlines.size):
                            htemp = hlines[nn]
                            if htemp > qymin and htemp < qymax:
                                ax.plot([xomin, xomax], [htemp, htemp], color='k', ls='--')
                    if vlines is not None:
                        for nn in np.arange(0, vlines.size):
                            vtemp = vlines[nn] + jet_time_offset
                            if vtemp > xomin and vtemp < xomax:
                                ax.plot([vtemp, vtemp], [qymin, qymax], color='k', ls='--')
                    ax.set_xlim([xomin, xomax])
                    ax.set_ylim([qymin, qymax])
                    if lflag and qq+1 in nlegends:
                        ax.legend(loc='best',prop={'size':lfsize})
                    if isinstance(prho,number_types):
                        tstr = "%.3f" % (float(prho))
                        titlestr = "rho_tor = " + tstr if not fancy else r'$\rho_{\text{tor}} = ' + tstr + r'$'
                        if rmajomin is not None and rmajomax is not None:
                            minstr = "%5.3f" % (float(rmajomin))
                            maxstr = "%5.3f" % (float(rmajomax))
                            rmajstr = " (R_out = " + minstr + " - " + maxstr + ")" if not fancy else r' ($R_{\text{out}} = ' + minstr + r' -- ' + maxstr + r'$)'
                        ax.set_title(titlestr)
                    if fplot_per_qq:
                        if not fancy:
                            snstr = "%d" % (shot)
                            fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
                        fig.savefig(odir + oname + '_' + ptag + rtag + '.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

                if fplot_per_qq and fig is not None:
                    plt.close(fig)

        if not fplot_per_qq:
            if not fancy:
                snstr = "%d" % (shot)
                fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
            fig.savefig(odir + oname + 'rho' + rtag + '_all.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

        if fig is not None:
            plt.close(fig)


def plot_transport(datafiles,quantities,outname,outdir='./',time=None,sigma=2.0,legend=None,labels=None, \
                   add_nc=False,xlimit=None,ylimit=None,xmarks=None,ymarks=None,xplot=None,yplot=None, \
                   fancy=False,fontsize=14,plot_format='png',plot_dpi=300,one_column=False,one_row=False,impurities=None):

    fname = 'jetto.jsp'
    sname = 'jetto.ssp'
    flist = None
    clist = None
    oname = 'temp'
    if isinstance(datafiles,list):
        flist = datafiles
    if isinstance(quantities,str):
        temp = quantities.split(',')
        clist = []
        for ii in range(0,len(temp)):
            clist.append(temp[ii].strip())
        if len(qlist) <= 0:
            clist = None
    if isinstance(quantities,list):
        clist = quantities
    if isinstance(outname,str):
        mm = re.match(r'^(.*)\.([a-z0-9]+)$',outname)
        if mm:
            oname = mm.group(1)
            plot_format = mm.group(2)
        else:
            oname = outname

    fgood = False
    device = 'JET'
    shot = None
    db = []
    maximp = 0
    for ii in np.arange(0,len(flist)):
        jdata = []
        if isinstance(flist[ii],str):
            fdir = flist[ii]
            if not fdir.endswith('/'):
                fdir = fdir + '/'
            if os.path.exists(fdir  + fname):
                jdata.append(jread.read_binary_file(fdir + fname))
                fgood = True
            else:
                print("JETTO JSP file, %s, not found" % (fdir + fname))
            if jdata[0] is not None:
                jj = 1
                jtag = "%d" % (jj)
                while "NIM"+jtag in jdata[0]:
                    if os.path.exists(fdir+sname+jtag):
                        jdata.append(jread.read_binary_file(fdir + sname + jtag))
                    else:
                        print("JETTO SSP%d file, %s, not found" % (jj, fdir + fname))
                        jdata.append({})
                    if jj > maximp:
                        maximp = jj
                    jj += 1
                    jtag = "%d" % (jj)
        db.append(copy.deepcopy(jdata))
        if len(jdata) > 0 and jdata[0] is not None:
            if shot is None and "SHOT" in jdata[0]:
                shot = int(jdata[0]["SHOT"])

    qlist = None
    if jdata is not None:
        qlist = []
        if "X" in clist:
            tlist = ["XE", "XI"]
            qlist.extend(tlist)
        if "D" in clist:
            tlist = ["DI"]
            for ii in range(maximp):
                tlist.append("DIM%d" % (ii+1))
            qlist.extend(tlist)
        if "V" in clist:
            tlist = ["VI"]
            for ii in range(maximp):
                tlist.append("VIM%d" % (ii+1))
            qlist.extend(tlist)
        if len(qlist) <= 0:
            qlist = None

    if fgood and qlist is not None:
        jqdict = {"TIME":"TIME", "XVEC1":"RHOTORN", "XVEC2":"RHOTORN", "XRHO":"RHOTORN", "RHO":"PSITOR", "XPSI":"PSIPOLN", "PSI":"PSIPOL", "R":"RMAJORO", "RI":"RMAJORI"}
        qedict = {"XE": 0, "XI": 0, "DI": 0, "DIMP": 0, "VI": 0, "VIMP": 0}
        tbdict = {"XE": "XE6", "XI": "XI6", "DI": "DTCI", "DIM1": "DAVG", "DIM2": "DAVG", "DIM3": "DAVG", "VI": "VTCI", "VIM1": "VAVG", "VIM2": "VAVG", "VIM3": "VAVG"}
        ncdict = {"XE": "XE1", "XI": "XI1", "DI": "DNCI", "DIM1": "DNAV", "DIM2": "DNAV", "DIM3": "DNAV", "VI": "VNCI", "VIM1": "VNAV", "VIM2": "VNAV", "VIM3": "VNAV"}
        prdict = {'jpg':300, 'bmp':300, 'png':300, 'ps':1200, 'svg':1200, 'eps':1200, 'pdf':1200}

        odir = outdir if isinstance(outdir,str) else './'
        if odir is not None and not odir.endswith('/'):
            odir = odir + '/'
        if not os.path.exists(odir):
            os.makedirs(odir)

        llist = None
        if isinstance(labels,str):
            temp = labels.split(',')
            llist = []
            for ii in range(0,len(temp)):
                llist.append(temp[ii].strip())
            if len(qlist) <= 0:
                llist = None
        elif isinstance(labels,list):
            llist = labels
        if llist is not None:
            while len(llist) < len(db):
                llist.append(None)

        ilist = None
        if isinstance(impurities,str):
            temp = impurities.split(',')
            ilist = []
            for ii in range(0,len(temp)):
                ilist.append(temp[ii].strip())
            if len(ilist) <= 0:
                ilist = None
        elif isinstance(impurities,list):
            ilist = impurities

        xlist = None
        ylist = None
        if isinstance(xlimit,tuple) and len(xlimit) == 2:
            xlist = []
            while len(xlist) < len(qlist):
                xlist.append(tuple(xlimit))
        elif isinstance(xlimit,list):
            xlist = []
            for ii in range(0,len(xlimit)):
                if isinstance(xlimit[ii],(list,tuple)) and len(xlimit[ii]) == 2:
                    xlist.append(tuple(xlimit[ii]))
                else:
                    xlist.append((None,None))
            while len(xlist) < len(qlist):
                xlist.append((None,None))
        if isinstance(ylimit,tuple) and len(ylimit) == 2:
            ylist = []
            while len(ylist) < len(qlist):
                ylist.append(tuple(ylimit))
        elif isinstance(ylimit,list):
            ylist = []
            for ii in range(0,len(ylimit)):
                if isinstance(ylimit[ii],(list,tuple)) and len(ylimit[ii]) == 2:
                    ylist.append(tuple(ylimit[ii]))
                else:
                    ylist.append((None,None))
            while len(ylist) < len(qlist):
                ylist.append((None,None))

        xrlist = None
        yrlist = None
        if isinstance(xplot,tuple) and len(xplot) == 2:
            xrlist = []
            while len(xrlist) < len(qlist):
                xrlist.append(tuple(xplot))
        elif isinstance(xplot,list):
            xrlist = []
            for ii in range(0,len(xplot)):
                if isinstance(xplot[ii],(list,tuple)) and len(xplot[ii]) == 2:
                    xrlist.append(tuple(xplot[ii]))
                else:
                    xrlist.append((None,None))
            while len(xrlist) < len(qlist):
                xrlist.append((None,None))
        if isinstance(yplot,tuple) and len(yplot) == 2:
            yrlist = []
            while len(yrlist) < len(qlist):
                yrlist.append(tuple(yplot))
        elif isinstance(yplot,list):
            yrlist = []
            for ii in range(0,len(yplot)):
                if isinstance(yplot[ii],(list,tuple)) and len(yplot[ii]) == 2:
                    yrlist.append(tuple(yplot[ii]))
                else:
                    yrlist.append((None,None))
            while len(yrlist) < len(qlist):
                yrlist.append((None,None))

        hlines = None
        vlines = None
        if isinstance(ymarks, number_types):
            hlines = np.array([float(ymarks)])
        elif isinstance(ymarks, array_types):
            hlines = np.array(ymarks)
        if isinstance(xmarks, number_types):
            vlines = np.array([float(xmarks)])
        elif isinstance(xmarks, array_types):
            vlines = np.array(xmarks)

        nlegends = []
        if isinstance(legend,number_types) and int(legend) > 0:
            nlegends.append(int(legend))
        elif isinstance(legend,array_types):
            for ii in range(0,len(legend)):
                if isinstance(legend[ii],number_types) and int(legend[ii]) > 0:
                    nlegends.append(int(legend[ii]))
        elif isinstance(legend,str):
            if legend == "all":
                for ii in range(0,len(qlist)):
                    nlegends.append(ii+1)

        fsize = 14
        if isinstance(fontsize,(int,float)) and int(fontsize) > 6:
            fsize = int(fontsize)
        if fancy:
            matplotlib.rc('font', family='serif')
            matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=fsize)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
        lfsize = fsize - 8

        pfmt = 'png'
        pdpi = prdict[pfmt]
        if plot_format in prdict:
            pfmt = plot_format
            pdpi = prdict[plot_format]
        if isinstance(plot_dpi,(int,float)) and int(plot_dpi) > 50:
            pdpi = int(plot_dpi)

        default_height = 5
        default_width = 6
        fig = None
        fplot_per_qq = True
        nplots = 0
        for qq in range(0,len(qlist)):
            nplots += 1
        if nplots > 0:
            if one_column:
                fig = plt.figure(figsize=(default_width, default_height * nplots))
                fplot_per_qq = False
            elif one_row:
                fig = plt.figure(figsize=(default_width * nplots, default_height))
                fplot_per_qq = False

        clist = ['b','r','m','y','k','c','g','orange','darkkhaki','saddlebrown','olive','lime','aquamarine','teal','skyblue','gray','salmon','maroon','tan','lightgreen','navy']
        (qx,xlabel,xunit,fxlabel,fxunit) = ptools.define_quantity(jqdict["XRHO"],exponent=None,derivative=None)
        pidx = 0
        for qq in range(0,len(qlist)):

            if qlist[qq] in tbdict:
                qtag = qlist[qq]
                ptag = qtag.lower()
                exp = qedict[qlist[qq]] if qlist[qq] in qedict else 0
                iname = None
                iidx = 0
                if qtag.startswith("XIM"):
                    exp = qedict["XIMP"]
                    iidx = int(float(qlist[qq][3:]))
                if qtag.startswith("DIM"): 
                    exp = qedict["DIMP"]
                    iidx = int(float(qlist[qq][3:]))
                if qtag.startswith("VIM"):
                    exp = qedict["VIMP"]
                    iidx = int(float(qlist[qq][3:]))
                if iidx > 0:
                    if ilist is not None and len(ilist) >= iidx:
                        imptag = ilist[iidx - 1]
                        (iname, ia, iz) = ptools.define_ion_species(short_name=imptag)
                        #exp = exp - int(-1.1 * np.log(iz) - 0.5)
                sc = np.power(10.0,exp) if isinstance(exp,int) and exp != 0 else 1.0
                (qy,ylabel,yunit,fylabel,fyunit) = ptools.define_quantity(qlist[qq],exponent=exp,derivative=None)
                if iname is not None:
                    if qtag.startswith("X"):
                        ylabel = iname + r' Heat Diffusivity'
                        fylabel = r'$\chi_{\text{' + iname + r'}}$'
                    if qtag.startswith("D"):
                        ylabel = iname + r' Particle Diffusivity'
                        fylabel = r'$D_{\text{' + iname + r'}}$'
                    if qtag.startswith("V"):
                        ylabel = iname + r' Particle Pinch'
                        fylabel = r'$V_{\text{' + iname + r'}}$'

                ax = None
                if fplot_per_qq:
                    fig = plt.figure(figsize=(default_width, default_height))
                    ax = fig.add_subplot(1,1,1)
                else:
                    pidx += 1
                    if one_column:
                        ax = fig.add_subplot(nplots,1,pidx)
                    elif one_row:
                        ax = fig.add_subplot(1,nplots,pidx)
                lflag = False

                for jj in range(0,len(flist)):
                    datavecs = db[jj]
                    if ax is not None and len(datavecs) > 0:
                        jdata = datavecs[iidx]
                        tidx = -1
                        if isinstance(time,number_types) and float(time) > 0.0:
                            treq = float(time)
                            tt = jdata["TIME"].flatten()
                            if treq < tt[-1]:
                                tidx = np.where(tt > treq)[0][0]
                                if tidx > 0 and np.abs(tt[tidx] - treq) > np.abs(treq - tt[tidx-1]):
                                    tidx = tidx - 1
                        tval = jdata["TIME"].flatten()[tidx]

                        ttag = tbdict[qtag]
                        xx = jdata["XVEC2"].flatten() if iidx == 0 else jdata["XVEC1"].flatten()
                        yy = jdata[ttag][tidx].flatten()
                        ffilt = np.all([np.isfinite(xx),np.isfinite(yy)],axis=0)
                        if xlist is not None or ylist is not None:
                            ximin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xx) - 0.1
                            ximax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xx) + 0.1
                            yimin = ylist[qq][0] if ylist is not None and qq < len(ylist) and ylist[qq][0] is not None else np.nanmin(yy) - 0.1
                            yimax = ylist[qq][1] if ylist is not None and qq < len(ylist) and ylist[qq][1] is not None else np.nanmax(yy) + 0.1
                            ffilt = np.all([ffilt,xx >= ximin,xx <= ximax,yy >= yimin,yy <= yimax],axis=0)
                        if not np.all(ffilt):
                            xx = xx[ffilt]
                            yy = yy[ffilt]
                        yy = yy / sc
                        ltag = None
                        if llist is not None:
                            ltag = llist[jj]
                        if add_nc:
                            ltag = ltag + r' (Turbulent)' if ltag is not None else r'Turbulent'
                        ax.plot(xx,yy,color=clist[jj],label=ltag,zorder=10)
                        if ltag is not None:
                            lflag = True

                        if add_nc:
                            ntag = ncdict[qtag]
                            xx = jdata["XVEC2"].flatten() if iidx == 0 else jdata["XVEC1"].flatten()
                            yy = jdata[ntag][tidx].flatten()
                            ffilt = np.all([np.isfinite(xx),np.isfinite(yy)],axis=0)
                            if xlist is not None or ylist is not None:
                                ximin = xlist[qq][0] if xlist is not None and qq < len(xlist) and xlist[qq][0] is not None else np.nanmin(xx) - 0.1
                                ximax = xlist[qq][1] if xlist is not None and qq < len(xlist) and xlist[qq][1] is not None else np.nanmax(xx) + 0.1
                                yimin = ylist[qq][0] if ylist is not None and qq < len(ylist) and ylist[qq][0] is not None else np.nanmin(yy) - 0.1
                                yimax = ylist[qq][1] if ylist is not None and qq < len(ylist) and ylist[qq][1] is not None else np.nanmax(yy) + 0.1
                                ffilt = np.all([ffilt,xx >= ximin,xx <= ximax,yy >= yimin,yy <= yimax],axis=0)
                            if not np.all(ffilt):
                                xx = xx[ffilt]
                                yy = yy[ffilt]
                            yy = yy / sc
                            ltag = None
                            if llist is not None:
                                ltag = llist[jj]
                            ltag = ltag + r' (Neoclassical)' if ltag is not None else r'Neoclassical'
                            ax.plot(xx,yy,color=clist[jj],ls='--',label=ltag,zorder=5)

                if ax is not None:
                    xl = xlabel + r' [' + xunit + r']' if xunit else xlabel
                    if fancy:
                        xl = fxlabel + r' [' + fxunit + r']' if fxunit else fxlabel
                    yl = ylabel + r' [' + yunit + r']' if yunit else ylabel
                    if fancy:
                        yl = fylabel + r' [' + fyunit + r']' if fyunit else fylabel
                    if fplot_per_qq or pidx == nplots:
                        ax.set_xlabel(xl)
                    ax.set_ylabel(yl)
                    (xomin, xomax) = ax.get_xlim()
                    (qymin, qymax) = ax.get_ylim()
                    if qlist[qq] in ["NE","TE","NI","TI","Q","ZEFF"] and qymin >= 0.0:
                        qymin = -0.05 * qymax
                    elif qlist[qq] in ["NIM1","NIM2","NIM3"] and qymin >= 0.0:
                        qymin = 0.0
                    if xlist is not None and qq < len(xlist):
                        if xlist[qq][0] is not None and xlist[qq][0] > xomin:
                            xomin = xlist[qq][0]
                        if xlist[qq][1] is not None and xlist[qq][1] < xomax:
                            xomax = xlist[qq][1]
                    if ylist is not None and qq < len(ylist):
                        if ylist[qq][0] is not None and ylist[qq][0] > qymin:
                            qymin = ylist[qq][0]
                        if ylist[qq][1] is not None and ylist[qq][1] < qymax:
                            qymax = ylist[qq][1]
                    if xrlist is not None and qq < len(xrlist):
                        if xrlist[qq][0] is not None:
                            xomin = xrlist[qq][0]
                        if xrlist[qq][1] is not None:
                            xomax = xrlist[qq][1]
                    if yrlist is not None and qq < len(yrlist):
                        if yrlist[qq][0] is not None:
                            qymin = yrlist[qq][0]
                        if yrlist[qq][1] is not None:
                            qymax = yrlist[qq][1]
                    if hlines is not None:
                        for nn in np.arange(0, hlines.size):
                            htemp = hlines[nn]
                            if htemp > qymin and htemp < qymax:
                                ax.plot([xomin, xomax], [htemp, htemp], color='k', ls='--')
                    if vlines is not None:
                        for nn in np.arange(0, vlines.size):
                            vtemp = vlines[nn]
                            if vtemp > xomin and vtemp < xomax:
                                ax.plot([vtemp, vtemp], [qymin, qymax], color='k', ls='--')
                    ax.set_xlim([xomin, xomax])
                    ax.set_ylim([qymin, qymax])
                    if lflag and qq+1 in nlegends:
                        ax.legend(loc='best',prop={'size':lfsize})
                    if isinstance(time,number_types):
                        tstr = "%.2f" % (float(time + jet_time_offset))
                        titlestr = "time = " + tstr if not fancy else r'$t = ' + tstr + r'$'
                        ax.set_title(titlestr)
                    if fplot_per_qq:
                        if not fancy:
                            snstr = "%d" % (shot)
                            fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
                        fig.savefig(odir + oname + '_' + ptag + '.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

            if fplot_per_qq and fig is not None:
                plt.close(fig)

        if not fplot_per_qq:
            snstr = "%d" % (shot)
            fig.suptitle(device + ' #' + snstr, fontsize=lfsize)
            fig.savefig(odir + oname + '_all.' + pfmt,bbox_inches='tight',format=pfmt,dpi=pdpi)

        if fig is not None:
            plt.close(fig)
