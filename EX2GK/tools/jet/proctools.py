# Script with functions to provide easy manipulation of data within JET-specific adapter
# Developer: Aaron Ho - 09/04/2019

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re

# Internal package imports
from EX2GK.tools.general import proctools as ptools


def convert_coords(datadict,datavec,csin,csout,prein=None,preout=None,cdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Automated conversion between the unified coordinate systems defined
    in the pre-processing step, simply by providing the standardized
    names of the input and output coordinate system. Linearly
    interpolates between the standardized 101 point grid and provides
    the associated Jacobians as well.

    For any coordinate systems with applied corrections or biases, see
    the implementation-specific adapter files for relevant names. For
    conversion from uncorrected to corrected systems, by convention, the
    output coordinate system should have the letter C appended to the
    beginning. For conversion between corrected systems, simply use the
    argument corrflag with the standard names. Note that these corrected
    systems may come with an additional radial coordinate error due to
    the statistics used to formulate the applied correction.

    Standardized coordinate system names
        POLFLUXN = Normalized poloidal flux
        POLFLUX  = Absolute poloidal flux
        RHOPOLN  = Normalized poloidal rho (sqrt POLFLUXN)
        TORFLUXN = Normalized toroidal flux
        TORFLUX  = Absolute toroidal flux
        RHOTORN  = Normalized toroidal rho (sqrt TORFLUXN)
        RMAJORO  = Absolute outer major radius at height of magnetic axis
        RMAJORI  = Absolute inner major radius at height of magnetic axis
        RMINORO  = Absolute outer minor radius at height of magnetic axis
        RMINORI  = Absolute inner minor radius at height of magnetic axis
        FSVOL    = Flux surface volume
        FSAREA   = Flux surface surface area

    :arg datadict: dict. Object containing unified coordinate system information.

    :arg datavec: array. Vector of radial points corresponding to input coordinate system.

    :arg csin: str. Standardized name of input coordinate system.

    :arg csout: str. Standardized name of output coordinate system.

    :kwarg prein: str. Standardized prefix for input coordinate system. I for interface, C for corrected.

    :kwarg preout: str. Standardized prefix for output coordinate system. I for interface, C for corrected.

    :kwarg cdebug: bool. Toggles printing of select debugging statements, useful for determining issues in entire workflow.

    :returns: (array, array, array).
        Vector of radial points corresponding to the output coordinate system, vector
        of Jacobians corresponding to the conversion between input and output
        coordinate systems at the radial points, vector of radial errors corresponding
        to the conversion between input and output coordinate systems.
    """
    cslist = ['RHOTORN','TORFLUX','TORFLUXN', \
              'RHOPOLN','POLFLUX','POLFLUXN', \
              'RMAJORO','RMAJORI', \
              'RMINORO','RMINORI', \
              'FSVOL','FSAREA']

    dobj = None
    dvec = None
    ics = None         # Input coordinate system label
    ocs = None         # Output coordinate system label
    if isinstance(datadict,dict):
        dobj = datadict
    if isinstance(datavec,(float,int,np.int8,np.int16,np.int32,np.int64,np.uint8,np.uint16,np.uint32,np.uint64,np.float16,np.float32,np.float64)):
        dvec = np.array([datavec]).flatten()
    elif isinstance(datavec,(list,tuple)):
        dvec = np.array(datavec).flatten()
    elif isinstance(datavec,np.ndarray):
        dvec = datavec.flatten()
    for cs in cslist:
        if re.match(r'^'+cs+r'$',csin,flags=re.IGNORECASE):
            ics = cs
        if re.match(r'^'+cs+r'$',csout,flags=re.IGNORECASE):
            ocs = cs

    # Standardized coordinate label prefixes
    csp = 'CS'       # Coordinate system prefix
    cjp = 'CJ'       # Coordinate Jacobian prefix
    cep = 'CE'       # Coordinate error prefix

    cvec = None       # Output coordinate vector
    evec = None       # Output error vector
    jvec = None       # Output Jacobian vector

    if isinstance(dobj,dict) and ics is not None and ocs is not None:

        # Determine existence and usage of corrected coordinate systems
        icp = prein if "CS_PREFIXES" in dobj and prein in dobj["CS_PREFIXES"] else ""
        ocp = preout if "CS_PREFIXES" in dobj and preout in dobj["CS_PREFIXES"] else ""

        if cdebug:
            print(icp+csp+"_"+ics,ocp+csp+"_"+ocs)

        # Simplify process if input and output coordinate systems are identical
        if re.match(ics,ocs,flags=re.IGNORECASE) and icp == ocp:
            cvec = dvec.copy()
            jvec = np.ones(cvec.shape)
            evec = np.zeros(cvec.shape)

        else:

            # Check existence of input and output coordinate systems
            cics = None
            cocs = None
            if isinstance(dobj,dict) and ics is not None and ocs is not None:
                if icp+csp+"_"+ics not in dobj:
                    ics = None
                if ocp+csp+"_"+ocs not in dobj:
                    ocs = None

                # Additional treatment needed for conversion between corrected and uncorrected systems
                if icp != ocp:

                    # Check existence of conversion coordinate system
                    cfull = False
                    if icp+csp+"_BASECORRO" in dobj and dobj[icp+csp+"_BASECORRO"] is not None and icp+csp+"_BASECORRI" in dobj and dobj[icp+csp+"_BASECORRI"] is not None:
                        cflag = True
                        cfull = True
                    elif ocp+csp+"_BASECORRO" in dobj and dobj[ocp+csp+"_BASECORRO"] is not None and ocp+csp+"_BASECORRI" in dobj and dobj[ocp+csp+"_BASECORRI"] is not None:
                        cflag = True
                        cfull = True
                    elif (icp+csp+"_BASECORR" in dobj and dobj[icp+csp+"_BASECORR"] is not None) or (ocp+csp+"_BASECORR" in dobj and dobj[ocp+csp+"_BASECORR"] is not None):
                        cflag = True

                    if cflag:
                        if cfull:
                            ice = "I" if ics.endswith("I") else "O"
                            oce = "I" if ocs.endswith("I") else "O"
                            if icp+csp+"_BASECORR"+ice in dobj and dobj[icp+csp+"_BASECORR"+ice] is not None:
                                cics = "BASECORR"+ice
                                cocs = dobj[ocp+csp+"_BASE"] if ocp+csp+"_BASE" in dobj else None
                            elif ocp+csp+"_BASECORR"+oce in dobj and dobj[icp+csp+"_BASECORR"+ice] is not None:
                                cocs = "BASECORR"+oce
                                cics = dobj[icp+csp+"_BASE"] if icp+csp+"_BASE" in dobj else None
                        else:
                            if icp+csp+"_BASECORR" in dobj and dobj[icp+csp+"_BASECORR"] is not None:
                                cics = "BASECORR"
                                cocs = dobj[ocp+csp+"_BASE"] if ocp+csp+"_BASE" in dobj else None
                            elif ocp+csp+"_BASECORR" in dobj and dobj[ocp+csp+"_BASECORR"] is not None:
                                cocs = "BASECORR"
                                cics = dobj[icp+csp+"_BASE"] if icp+csp+"_BASE" in dobj else None
                    else:
                        icp = ""
                        ocp = ""

            else:
                ics = None
                ocs = None

            # Extract base coordinate for Jacobians
            ijb = None
            ojb = None
            if icp+cjp+"_BASE" in dobj and dobj[icp+cjp+"_BASE"] in cslist:
                ijb = dobj[icp+cjp+"_BASE"]
            if ocp+cjp+"_BASE" in dobj and dobj[ocp+cjp+"_BASE"] in cslist:
                ojb = dobj[ocp+cjp+"_BASE"]

            if ics is not None and ocs is not None and ijb is not None and ojb is not None:

                # Containers for coordinate system vectors
                civec = dobj[icp+csp+"_"+ics]
                ccivec = None
                ccovec = None
                covec = dobj[ocp+csp+"_"+ocs]

                # Containers for coordinate system Jacobian vectors
                jivec = dobj[icp+cjp+"_"+ijb+"_"+ics]
                jcivec = None
                jcovec = None
                jovec = dobj[ocp+cjp+"_"+ojb+"_"+ocs]

                # Containers for coordinate system errors
                ecvec = None
                eovec = dobj[ocp+cep+"_"+ocs]

                # Additional vectors needed for conversion between corrected and uncorrected systems
                if cics is not None and cocs is not None:

                    ccivec = dobj[icp+csp+"_"+cics]
                    ccovec = dobj[ocp+csp+"_"+cocs]
                    jcivec = dobj[icp+cjp+"_"+ijb+"_"+cics]
                    jcovec = dobj[ocp+cjp+"_"+ojb+"_"+cocs]
                    if re.match(r'^BASECORR[IO]?$',cics,flags=re.IGNORECASE):
                        ecvec = dobj[icp+cep+"_"+cics]
                    else:
                        ecvec = dobj[ocp+cep+"_"+cocs]

                if cdebug:
                    print(civec)
                    print(covec)

                # Double-pass needed for conversion between corrected and uncorrected systems, otherwise just single-pass
                if ccivec is not None and ccovec is not None and jcivec is not None and jcovec is not None and ecvec is not None:
                    (cvec,jvec1,evec1) = ptools.convert_base_coords(dvec,civec,ccivec,jivec,jcivec,ecvec)
                    (cvec,jvec2,evec2) = ptools.convert_base_coords(cvec,ccovec,covec,jcovec,jovec,eovec)
                    jvec = jvec1 * jvec2 if cvec is not None and jvec1 is not None and jvec2 is not None else None
                    evec = np.sqrt(np.power(evec1,2.0) + np.power(evec2,2.0)) if cvec is not None and evec1 is not None and evec2 is not None else None
                else:
                    (cvec,jvec,evec) = ptools.convert_base_coords(dvec,civec,covec,jivec,jovec,ecvec)

                if cdebug:
                    print(dvec)
                    print(cvec)
                    print(jvec)
                    print(evec)

                if cvec is None or jvec is None or evec is None:
                    cvec = None
                    jvec = None
                    evec = None

    return (cvec,jvec,evec)
