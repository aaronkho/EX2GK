# Adapter file for JETTO data transfer - requires .jsp and .jst files
# Developer: Aaron Ho - 05/02/2019

# Required imports
import os
import sys
import importlib
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
import warnings
from pathlib import Path
from scipy.interpolate import interp1d, splrep, splev, splint
from scipy.signal import savgol_filter

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools
from EX2GK.jetto_pythontools.jetto_tools import binary as jtools

def import_from_JSP(jetto_rundir_path,time,profopt=0,smspflag=False,fpuretor=False,fdebug=False):
    """
    JET-SPECIFIC FUNCTION
    Converts the chosen time slice in the provided JETTO run, specified via its
    run directory, into the standardized EX2GK time window object. This is
    primarily used to then convert the JETTO data into code inputs via the
    code adapter modules. Note that not every quantity has been assigned to its
    proper location!

    :arg jetto_rundir_path: str. Full path of JETTO run directory to be converted.

    :arg time: float or list. Time value to convert into a time window object, takes closest time slice if not exact.

    :kwarg profopt: int. Option number for processing of JETTO profile data into required fields. (1 = TCI, 2 = cubic spline)

    :kwarg fdebug: bool. Optional flag to print progress statements to terminal for use in debugging.

    :returns: obj. EX2GK custom class object with data corresponding to the level of processing specified with outlevel argument.
    """
    outlist = []
    tslices = None
    pprocopt = 0
    if isinstance(time,(int,float)):
        tslices = [float(time)]
    elif isinstance(time,(list,tuple)):
        tslices = list(time)
    elif isinstance(time,np.ndarray):
        tslices = time.tolist()
    elif time is None:
        tslices = 'from_rundir'
    if isinstance(profopt,int) and int(profopt) in range(3):
        pprocopt = int(profopt)
    jpath = Path(jetto_rundir_path)
    jdict = None
    tdict = None
    sdict = None
    idict = None
    zdicts = []
    rpath = None
    if jpath.is_dir():
        jdict = jtools.read_binary_file(str(jpath / "jetto.jsp"))
        tpath = jpath / "jetto.jst"
        if tpath.is_file():
            tdict = jtools.read_binary_file(str(tpath))
        spath = jpath / "jetto.jss"
        if spath.is_file():
            sdict = jtools.read_binary_file(str(spath))
        ipath = jpath / "jetto.ssp"
        if ipath.is_file():
            idict = jtools.read_binary_file(str(ipath))
        numimps = 0
        if isinstance(tdict, dict):
            impfield = "IMC%d" % (numimps+1)
            while impfield in tdict:
                numimps += 1
                impfield = "IMC%d" % (numimps+1)
        else:
            impfield = "NIM%d" % (numimps+1)
            while impfield in jdict:
                numimps += 1
                impfield = "NIM%d" % (numimps+1)
        zdicts = [None] * numimps
        for zz in range(numimps):
            fname = "jetto.ssp%d" % (zz+1)
            zpath = jpath / fname
            if zpath.is_file():
                zdicts[zz] = jtools.read_binary_file(str(zpath))
        rpath = jpath / "R0.qlk"
        if not rpath.is_file():
            rpath = None
        if tslices == 'from_rundir':
            tslices = jdict["TIME"].flatten().tolist()

    if isinstance(jdict,dict) and tslices is not None and "TIME" in jdict:

        outlist = []
        savgol_window = 11
        for tt in np.arange(0,len(tslices)):

            tslice = tslices[tt]

            # Identify time slice inside JSP file
            idxv = np.where(jdict["TIME"] > tslice)[0]
            jidx = idxv[0] if len(idxv) > 0 else len(idxv) - 1
            if jidx > 0 and np.abs(jdict["TIME"][jidx] - tslice) > np.abs(jdict["TIME"][jidx-1] - tslice):
                jidx = jidx - 1

            # Identify time index inside JST file, if provided
            tidx = -1
            if isinstance(tdict, dict):
                idxv = np.where(tdict["TVEC1"] > tslice)[0]
                tidx = idxv[0] if len(idxv) > 0 else len(idxv) - 1
                if tidx > 0 and np.abs(tdict["TVEC1"][tidx] - tslice) > np.abs(tdict["TVEC1"][tidx-1] - tslice):
                    tidx = tidx - 1

            # Initialize required EX2GK classes
            MC = classes.EX2GKMetaContainer()
            CC = classes.EX2GKCoordinateContainer()
            ZC = classes.EX2GKZeroDimensionalContainer()
#            RC = classes.EX2GKRawDataContainer()
#            EC = classes.EX2GKEquilibriumContainer()
            PC = classes.EX2GKProfileDataContainer()
            KC = classes.EX2GKFitSettingsContainer()
            FC = classes.EX2GKFlagContainer()

            # Transfer generic metadata
            device = "JETTO"
            interface = "JSP"
            shotnum = int(jdict["SHOT"]) if "SHOT" in jdict else None
            t1 = float(jdict["TIME"][jidx])
            t2 = float(jdict["TIME"][jidx])
            window = -1
            shotphase = -1
            eqsrc = jdict["EQLIST"][0].upper() if "EQLIST" in jdict and len(jdict["EQLIST"]) > 0 else "UNKNOWN"
            zz1 = float(sdict["ZIM1"][0]) if isinstance(sdict,dict) and "ZIM1" in sdict else 6.0
            zz2 = float(sdict["ZIM2"][0]) if isinstance(sdict,dict) and "ZIM2" in sdict else 26.0
            (wallmat,aa1,zz1) = ptools.define_ion_species(zz1)
            (contmat,aa2,zz2) = ptools.define_ion_species(zz2)
            pconfig = 1

            MC.setRequiredMetaData(device,interface,shotnum,t1,t2,window,shotphase,eqsrc,wallmat,contmat,pconfig)
            mtags = []
            fracs = np.array([])
            fracebs = np.array([])
            for material in ["H","D","T"]:
                if "NI" in jdict and "NI"+material in jdict:
                    mtags.append(material)
                    matfrac = np.mean(jdict["NI"+material][jidx,:] / jdict["NI"][jidx,:])
                    fracs = np.hstack((fracs,float(matfrac)))
                    fracebs = np.hstack((fracebs,0.0))
            if fracs.size > 0:
                fracs = fracs / np.sum(fracs)
            idxmax = np.where(fracs >= np.nanmax(fracs))[0][0] if fracs.size > 0 else None
            MC.addIonMetaData(name=mtags[idxmax],weight=fracs[idxmax])
            for matid in np.arange(0,len(mtags)):
                if matid != idxmax and fracs[matid] >= 1.0e-3:
                    MC.addIonMetaData(name=mtags[matid],weight=fracs[matid])
                ZC.addData("FRAC"+mtags[matid].upper(),float(fracs[matid]),float(fracebs[matid]))

            if fdebug:
                print("jetto_adapter.py: %s populated." % (type(MC).__name__))

            # Transfer coordinate system data
            csdict = {"POLFLUXN": "PSIPOLN", "POLFLUX": "PSIPOL", "RHOPOLN": "RHOPOLN", "TORFLUXN": "PSITORN", "TORFLUX": "PSITOR", "RHOTORN": "RHOTORN", \
                      "RMAJORO": "RMAJORO", "RMAJORI": "RMAJORI", "RMINORO": "RMINORO", "RMINORI": "RMINORI", "FSVOL": "FSVOL", "FSAREA": "FSAREA", \
                      "RMAJORA": "RMAJORA", "RMINORA":"RMINORA"}
            jcsdict = {"XPSI": "POLFLUXN", "PSI": "POLFLUX", "XPSQ": "RHOPOLN", "XRHO": "RHOTORN", "R": "RMAJORO", "RI": "RMAJORI", "VOL": "FSVOL", "AREA": "FSAREA"}
            jcszero = {"XPSI": 0.0, "PSI": None, "XPSQ": 0.0, "XRHO": 0.0, "R": None, "RI": None, "VOL": 0.0, "AREA": 0.0, "XA": 0.0}
            jcsbase = "XRHO"
            cdict = {"CS_BASE": jcsdict[jcsbase], "CJ_BASE": jcsdict[jcsbase]}
            for key in jcsdict:
                if jcsbase in jdict and key in jdict:
                    xvec = jdict[jcsbase][jidx,:].flatten()
                    yvec = jdict[key][jidx,:].flatten()
                    xzero = jcszero[jcsbase]
                    yzero = jcszero[key]
                    if xzero is not None and yzero is None:
                        ifunc = interp1d(xvec,yvec,kind='linear',bounds_error=False,fill_value='extrapolate')
                        yzero = float(ifunc(xzero))
                    if xzero is not None and yzero is not None:
                        xvec = np.hstack((xzero,xvec))
                        yvec = np.hstack((yzero,yvec))
                    spvals = splrep(xvec,yvec)
                    vvec = splev(xvec,spvals,der=0)
                    jvec = splev(xvec,spvals,der=1)
                    if smspflag and key in ["R","RI"]:
                        ovvec = vvec.copy()
                        wvec = 1000.0 * np.ones(yvec.shape)
                        wfilt = (xvec < 0.01)
                        wvec[wfilt] = xvec[wfilt] / 0.01
                        spvals = splrep(xvec,yvec,w=wvec)
                        vvec = splev(xvec,spvals,der=0)
                        jvec = splev(xvec,spvals,der=1)
                        if fdebug:
                            print("Weighted splines on %s" % (jcsdict[key]),np.nanmax(vvec - ovvec),np.nanmin(vvec - ovvec))
                    cdict["CS_"+jcsdict[key]] = vvec.copy()
                    cdict["CJ_"+jcsdict[jcsbase]+"_"+jcsdict[key]] = 1.0 / jvec.copy()
                    cdict["CE_"+jcsdict[key]] = np.zeros(xvec.shape)
            logfitpar = 1.0e-5
            if "CS_TORFLUX" not in cdict:
                qval = np.abs(jdict["Q"][jidx,:].flatten())
                psitemp = jdict["PSI"][jidx,:].flatten()
                logq = np.log(qval + logfitpar)
                pqfunc = interp1d(psitemp,logq,kind='cubic',bounds_error=False)
                testpsi = np.linspace(psitemp[0],psitemp[-1] - 1.0e-5,1001)
                qtemp = np.exp(pqfunc(testpsi)) - logfitpar
                tfxbnd = np.trapz(qtemp,testpsi)
                cdict["CS_TORFLUX"] = np.power(cdict["CS_RHOTORN"],2.0) * tfxbnd
                jtemp = 0.5 / (cdict["CS_RHOTORN"][1:] * tfxbnd)
                jtemp = np.hstack((0.0,jtemp))
                cdict["CJ_"+jcsdict[jcsbase]+"_TORFLUX"] = jtemp
                cdict["CE_TORFLUX"] = np.zeros(cdict["CS_RHOTORN"].shape)
                if "CS_TORFLUXN" not in cdict:
                    cdict["CS_TORFLUXN"] = np.power(cdict["CS_RHOTORN"],2.0)
                    cdict["CJ_"+jcsdict[jcsbase]+"_TORFLUXN"] = jtemp * tfxbnd
                    cdict["CE_TORFLUXN"] = np.zeros(xvec.shape)
            rmag = (cdict["CS_RMAJORO"][0] + cdict["CS_RMAJORI"][0]) / 2.0
            rgeo = (cdict["CS_RMAJORO"][-1] + cdict["CS_RMAJORI"][-1]) / 2.0
            if "CS_RMAJORA" not in cdict:
                cdict["CS_RMAJORA"] = (cdict["CS_RMAJORO"] + cdict["CS_RMAJORI"]) / 2.0
                cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORA"] = 2.0 * cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORO"] * cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORI"] / (cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORO"] + cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORI"])
                cdict["CE_RMAJORA"] = np.zeros(cdict["CS_RMAJORA"].shape)
            if "CS_RMINORO" not in cdict:
                cdict["CS_RMINORO"] = cdict["CS_RMAJORO"] - rmag
                cdict["CJ_"+jcsdict[jcsbase]+"_RMINORO"] = cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORO"].copy()
                cdict["CE_RMINORO"] = np.zeros(cdict["CS_RMAJORO"].shape)
            if "CS_RMINORI" not in csdict:
                cdict["CS_RMINORI"] = rmag - cdict["CS_RMAJORI"]
                cdict["CJ_"+jcsdict[jcsbase]+"_RMINORI"] = -cdict["CJ_"+jcsdict[jcsbase]+"_RMAJORI"].copy()
                cdict["CE_RMINORI"] = np.zeros(cdict["CS_RMAJORI"].shape)
            if "CS_RMINORA" not in csdict:
                cdict["CS_RMINORA"] = (cdict["CS_RMINORO"] + cdict["CS_RMINORI"]) / 2.0
                cdict["CJ_"+jcsdict[jcsbase]+"_RMINORA"] = 2.0 * cdict["CJ_"+jcsdict[jcsbase]+"_RMINORO"] * cdict["CJ_"+jcsdict[jcsbase]+"_RMINORI"] / (cdict["CJ_"+jcsdict[jcsbase]+"_RMINORO"] + cdict["CJ_"+jcsdict[jcsbase]+"_RMINORI"])
                cdict["CE_RMINORA"] = np.zeros(cdict["CS_RMINORA"].shape)

            cdebug = False
            if cdebug and "CS_POLFLUXN" in cdict:
                debugdir = './coord_debug/'
                if not os.path.exists(debugdir):
                    os.makedirs(debugdir)
                if "CS_TORFLUX" in cdict and "CS_POLFLUX" in cdict:
                    with open(debugdir+'tfx.txt','w') as ff1:
                        for ii in np.arange(0,cdict["CS_POLFLUX"].size):
                            ff1.write("   %15.6e   %15.6e\n" % (cdict["CS_POLFLUX"][ii],cdict["CS_TORFLUX"][ii]))
                if "CS_RMAJORO" in cdict and "CS_RMAJORI" in cdict:
                    with open(debugdir+'rmaj.txt','w') as ff2:
                        psin = np.hstack((cdict["CS_POLFLUXN"][:0:-1],cdict["CS_POLFLUXN"]))
                        rmaj = np.hstack((cdict["CS_RMAJORI"][:0:-1],cdict["CS_RMAJORO"]))
                        for ii in np.arange(0,psin.size):
                            ff2.write("   %15.6e   %15.6e\n" % (rmaj[ii],psin[ii]))
                if "CS_FSVOL" in cdict:
                    with open(debugdir+'vol.txt','w') as ff3:
                        for ii in np.arange(0,cdict["CS_FSVOL"].size):
                            ff3.write("   %15.6e   %15.6e\n" % (cdict["CS_POLFLUXN"][ii],cdict["CS_FSVOL"][ii]))
                if "CS_FSAREA" in cdict:
                    with open(debugdir+'area.txt','w') as ff4:
                        for ii in np.arange(0,cdict["CS_FSAREA"].size):
                            ff4.write("   %15.6e   %15.6e\n" % (cdict["CS_POLFLUXN"][ii],cdict["CS_FSAREA"][ii]))
                if "CS_RMIDAVG" in cdict:
                    with open(debugdir+'x.txt','w') as ff5:
                        for ii in np.arange(0,cdict["CS_RMIDAVG"].size):
                            ff5.write("   %15.6e   %15.6e\n" % (cdict["CS_POLFLUXN"][ii],cdict["CS_RMIDAVG"][ii]))

            if "CS_BASE" in cdict and cdict["CS_BASE"] is not None and cdict["CS_BASE"] in csdict:
                if "CS_"+cdict["CS_BASE"] in cdict and cdict["CS_"+cdict["CS_BASE"]] is not None:
                    jbase = cdict["CJ_BASE"] if "CJ_BASE" in cdict else cdict["CS_BASE"]
                    CS = classes.EX2GKCoordinateSystem()
                    CS.addCoordinate(csdict[cdict["CS_BASE"]],cdict["CS_"+cdict["CS_BASE"]],cs_prefix="")
                    for key in csdict:
                        if not re.match(r'^'+cdict["CS_BASE"]+r'$',key,flags=re.IGNORECASE) and "CS_"+key in cdict and cdict["CS_"+key] is not None:
                            jvec = cdict["CJ_"+jbase+"_"+key].copy() if "CJ_"+jbase+"_"+key in cdict else None
                            evec = cdict["CE_"+key].copy() if "CE_"+key in cdict else None
                            CS.addCoordinate(csdict[key],cdict["CS_"+key],jvec,evec,cs_prefix="")
                    CC.addCoordinateSystemObject(CS)

            if fdebug:
                print("jetto_adapter.py: %s populated." % (type(CC).__name__))

            # Specify base coordinate and grid for all profile data
            #     Definition of the standard grid used by QuaLiKiz inside JETTO (midpoints of XRHO grid plus extra point near magnetic axis)
            gcs = "RHOTORN"
            gcp = ""
            qlkxfull = np.hstack((jcszero["XRHO"],jdict["XRHO"][jidx,:].flatten()))
            if pprocopt == 1:
                # TCI uses this grid is used to calculate profile derivatives via central difference scheme (midplane averaged minor radius, UNNORMALISED!)
                ngcs = "RMINORA"
                tempx = (jdict["R"][jidx,:].flatten() - jdict["RI"][jidx,:].flatten()) / 2.0
                qlkxfull = np.hstack((0.0,tempx))
#                (qlkxgrid,jgrid,egrid) = CC.convert(qlkxgrid,gcs,ngcs,gcp,gcp,fdebug=fdebug)
                gcs = ngcs
            if pprocopt == 2:
                # JETTO grid is equally spaced in rho_tor except for first index, which is defined at rho=0
                hstep = float(np.diff(jdict["XRHO"][jidx,:2]))
                qlkxfull = np.hstack((jdict["XRHO"][jidx,0] - hstep,jdict["XRHO"][jidx,:].flatten()))
                gcs = "RHOTORN"
            # JETTO-QuaLiKiz grid expressed in midpoint of nominal radial coordinate
            qlkxgrid = qlkxfull[1:] - 0.5 * np.diff(qlkxfull)
            if pprocopt == 2:
                qlkxgrid[0] = qlkxfull[1] / 2.0

            # Transfer 0D data
            zmag = 0.0
            bmag = 3.0
            ip = 3.0e6
            if isinstance(tdict, dict):
                if "BTOR" in tdict:
                    bmag = float(tdict["BTOR"].flatten()[tidx])
                if "CUR" in tdict:
                    ip = float(tdict["CUR"].flatten()[tidx])
            else:
                if "F" in jdict:
                    bmag = float(jdict["F"][jidx,0] / rmag)
                if "CUR" in jdict:
                    ip = float(jdict["CUR"][jidx,-1])
            ZC.addRequiredData(rmag,zmag,ip,bmag,0.0,0.0,0.0,0.0)
            ZC.addData("RGEO",rgeo,0.0)
            ZC.addData("ZGEO",zmag,0.0)
            ZC.addData("BVAC",bmag,0.0)
            ZC.addData("RVAC",2.96,0.0)

            rzero = None
            if pprocopt == 1:
                # This is apparently the formula for Rzero set internally in JETTO... but why?
                #    This reproduction is NOT numerically identical to internal JETTO value!
                (rmajoro,dummy,dummy) = CC.convert(qlkxgrid[2],gcs,"RMAJORO","","",fdebug=fdebug)
                rmajori = jdict["RI"][jidx,1]
                rzero = float(rmajoro + rmajori) / 2.0
            if pprocopt != 0 and rpath is not None and rpath.is_file():
                # This enforces exact internal JETTO value, overrides above processing
                rztemp = np.genfromtxt(str(rpath)).flatten()
                rzero = float(rztemp[0])
            if rzero is not None:
                ZC.addData("RZERO",rzero,0.0)

            if isinstance(tdict, dict):
                if "PNB" in tdict:
                    pi = float(tdict["PNB"].flatten()[tidx])
                    if pi > 0.0:
                        ZC.addData("POWINBI",pi,0.0)
                        ZC.addData("POWLNBI",0.0,0.0)
                if "PRF" in tdict:
                    pi = float(tdict["PRF"].flatten()[tidx])
                    if pi > 0.0:
                        ZC.addData("POWIICRH",pi,0.0)
                        ZC.addData("POWLICRH",0.0,0.0)
                if "PECE" in tdict:
                    pi = float(tdict["PECE"].flatten()[tidx])
                    if pi > 0.0:
                        ZC.addData("POWIECRH",pi,0.0)
                        ZC.addData("POWLECRH",0.0,0.0)
                if "PLH" in tdict:
                    pi = float(tdict["PLH"].flatten()[tidx])
                    if pi > 0.0:
                        ZC.addData("POWILH",pi,0.0)
                        ZC.addData("POWLLH",0.0,0.0)
                if "POH" in tdict:
                    pi = float(tdict["POH"].flatten()[tidx])
                    if pi > 0.0:
                        ZC.addData("POWIOHM",pi,0.0)
                        ZC.addData("POWLOHM",0.0,0.0)

                if "WTOT" in tdict:
                    wp = float(tdict["WTOT"].flatten()[tidx])
                    ZC.addData("WEXP",wp,0.0)

                if "VLP" in tdict:
                    ZC.addData("VAVG",float(tdict["VLP"].flatten()[tidx]),0.0)
                if "BNTH" in tdict:
                    ZC.addData("BETATH",float(tdict["BNTH"].flatten()[tidx]),0.0)
                if "BNTT" in tdict:
                    ZC.addData("BETATOT",float(tdict["BNTT"].flatten()[tidx]),0.0)
                if "BPTH" in tdict:
                    ZC.addData("BETAPTH",float(tdict["BPTH"].flatten()[tidx]),0.0)
                if "BPTT" in tdict:
                    ZC.addData("BETAPTOT",float(tdict["BPTT"].flatten()[tidx]),0.0)

            # Select lowest of two Z-effective measurements as standard flat Z-effective estimate
            fzeff = 1.25
            if isinstance(tdict, dict):
                fzeff = float(tdict["ZEFF"].flatten()[tidx])
            else:
                fzeff = float(np.mean(jdict["ZEFF"][jidx,:]))
            ZC.addData("FZEFF",fzeff,0.0)
            FC.setFlag("FLATZEFF",False)

            if fdebug:
                print("jet - adapter.py: %s populated." % (type(ZC).__name__))

            varconv = {"NETF": "NE", "TE": "TE", "TI": "TI1", "VOL": "V", "Q": "Q", "F": "FBTOR", \
                       "ZEFF": "ZEFF", "SBD1": "SNINBI", "QNBE": "STENBI", "QNBI": "STINBI", "QRFE": "STEICRH", "QRFI": "STIICRH", \
                       "TORQ": "SPNBI", "JZNB": "JNBI", "DNBD": "NFINBI", "WNBD": "WFINBI", "DRFD": "NFIICRH", "WRFD": "WFIICRH"}
            # NETF used instead of NE, includes contribution of any fast ion species in JETTO
            # ANGF treated separately due to oddities in treatment of rotation vectors between codes

            MC.setFittingMetaData(gcs,fit_vector=qlkxgrid,fit_coord_prefix=gcp)
            PC.addProfileData("X",qlkxgrid,np.zeros(qlkxgrid.shape),x_coord=gcs)
            PC.addProfileDerivativeData("X",np.ones(qlkxgrid.shape),np.zeros(qlkxgrid.shape))
            FSO = classes.EX2GKFitSettings(name="Q")
            KC.addFitSettingsDataObject(FSO)

            # Specify profile data for kinetic profiles
            for key in varconv:
                if key in jdict:
                    ptemp = jdict[key][jidx,:].flatten()
                    if key == "Q":
                        ptemp = np.abs(ptemp)
                    ptemp = np.hstack((ptemp[0],ptemp))
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData(varconv[key],jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData(varconv[key],dprof,np.zeros(dprof.shape))
                    if key == "ZEFF":
                        PC.addProfileData("ZEFFP",jprof,np.zeros(jprof.shape))
                        PC.addProfileDerivativeData("ZEFFP",dprof,np.zeros(dprof.shape))

            # Specify main ion profiles
            if idxmax is not None:
                key = "NI"+mtags[idxmax]
                numi = 0
                itag = "%d" % (numi+1)
                if key in jdict:
                    ptemp = jdict[key][jidx,:].flatten()
                    ptemp = np.hstack((ptemp[0],ptemp))
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData("NI"+itag,jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("NI"+itag,dprof,np.zeros(dprof.shape))
                    numi = numi + 1
                for ii in np.arange(0,len(mtags)):
                    key = "NI"+mtags[ii]
                    if ii != idxmax and key in jdict and ZC["FRAC"+mtags[ii]][""] > 0.0:
                        itag = "%d" % (numi+1)
                        ptemp = jdict[key][jidx,:].flatten()
                        ptemp = np.hstack((ptemp[0],ptemp))
                        dprof = np.diff(ptemp) / np.diff(qlkxfull)
                        if pprocopt == 2:
                            hstep = float(np.mean(np.diff(qlkxfull)))
                            dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                            ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                            dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                        pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                        jprof = pfunc(qlkxgrid)
                        PC.addProfileData("NI"+itag,jprof,np.zeros(jprof.shape))
                        PC.addProfileDerivativeData("NI"+itag,dprof,np.zeros(dprof.shape))
                        if "TI" in jdict:
                            ptemp = jdict["TI"][jidx,:].flatten()
                            ptemp = np.hstack((ptemp[0],ptemp))
                            dprof = np.diff(ptemp) / np.diff(qlkxfull)
                            if pprocopt == 2:
                                hstep = float(np.mean(np.diff(qlkxfull)))
                                dptemp = savgol_filter(ptemp,5,3,deriv=1,delta=hstep)
                                ptemp = savgol_filter(ptemp,5,3,deriv=0,delta=hstep)
                                dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                            pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                            jprof = pfunc(qlkxgrid)
                            PC.addProfileData("TI"+itag,jprof,np.zeros(jprof.shape))
                            PC.addProfileDerivativeData("TI"+itag,dprof,np.zeros(dprof.shape))
                        numi = numi + 1
            else:
                key = "NI"
                if key in jdict:
                    ptemp = jdict[key][jidx,:].flatten()
                    ptemp = np.hstack((ptemp[0],ptemp))
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData("NI1",jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("NI1",dprof,np.zeros(dprof.shape))

            # Specify impurity ion profiles
            key = "NIM"
            for ii in range(len(zdicts)):
                itag = "%d" % (ii+1)
                if isinstance(zdicts[ii],dict) and isinstance(idict,dict):

                    # Grab nominal impurity information from .jss file, if available
                    if isinstance(sdict,dict):
                        aimp = float(sdict["AIM"+itag]) if "AIM"+itag in sdict else 2.0
                        zimp = float(sdict["ZIM"+itag]) if "ZIM"+itag in sdict else 1.0
                        MC.addImpurityMetaData(zimp,aimp)

                    pitemp = zdicts[ii]["NI"][jidx,:].flatten()
                    ritemp = idict["RMJC"][jidx,:].flatten()
                    ifunc = interp1d(ritemp,pitemp,kind='linear',bounds_error=False,fill_value='extrapolate')
                    rtemp = jdict["R"][jidx,:].flatten()
                    rtemp = np.hstack((rtemp[0],rtemp))
                    ptemp = ifunc(rtemp)
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData("NIMP"+itag,jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("NIMP"+itag,dprof,np.zeros(dprof.shape))

                    if "TI1" in PC:
                        PC.addProfileData("TIMP"+itag,PC["TI1"][""],PC["TI1"]["EB"])
                        PC.addProfileDerivativeData("TIMP"+itag,PC["TI1"]["GRAD"],PC["TI1"]["GRADEB"])
                    if "ZIA"+itag in jdict:
                        ptemp = jdict["ZIA"+itag][jidx,:].flatten()
                        ptemp = np.hstack((ptemp[0],ptemp))
                        pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                        jprof = pfunc(qlkxgrid)
                        dprof = np.diff(ptemp) / np.diff(qlkxfull)
                        PC.addProfileData("ZIMP"+itag,jprof,np.zeros(jprof.shape))
                        PC.addProfileDerivativeData("ZIMP"+itag,dprof,np.zeros(dprof.shape))

                elif key+itag in jdict:

                    # Grab nominal impurity information from .jss file, if available
                    if isinstance(sdict,dict):
                        aimp = float(sdict["AIM"+itag]) if "AIM"+itag in sdict else 2.0
                        zimp = float(sdict["ZIM"+itag]) if "ZIM"+itag in sdict else 1.0
                        MC.addImpurityMetaData(zimp,aimp)

                    ptemp = jdict[key+itag][jidx,:].flatten()
                    ptemp = np.hstack((ptemp[0],ptemp))
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData("NIMP"+itag,jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("NIMP"+itag,dprof,np.zeros(dprof.shape))
                    if "TI1" in PC:
                        PC.addProfileData("TIMP"+itag,PC["TI1"][""],PC["TI1"]["EB"])
                        PC.addProfileDerivativeData("TIMP"+itag,PC["TI1"]["GRAD"],PC["TI1"]["GRADEB"])
                    if "ZIA"+itag in jdict:
                        ptemp = jdict["ZIA"+itag][jidx,:].flatten()
                        ptemp = np.hstack((ptemp[0],ptemp))
                        pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                        jprof = pfunc(qlkxgrid)
                        dprof = np.diff(ptemp) / np.diff(qlkxfull)
                        PC.addProfileData("ZIMP"+itag,jprof,np.zeros(jprof.shape))
                        PC.addProfileDerivativeData("ZIMP"+itag,dprof,np.zeros(dprof.shape))

            # An impurity species (any impurity species) is required by EX2GK to maintain consistency with Zeff
            #     Defines carbon to be the impurity if absolutely no impurity info is found in JSP
            MC.autoSetFillerImpurityData(last_resort="C")

            # Add toroidal and poloidal magnetic fields
            if "FBTOR" not in PC and bmag is not None and rmag is not None:
                ptemp = np.full(jdict["XRHO"][jidx,:].flatten().shape,bmag*rmag)
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                # Assumed to be independent of radius due to flux surface averaging?
                PC.addProfileData("FBTOR",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("FBTOR",dprof,np.zeros(dprof.shape))
                btor = jprof.copy()
            key = "BPOL"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("BPOL",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("BPOL",dprof,np.zeros(dprof.shape))

            # Add toroidal angular frequency (not a flux function?) - rotation velocity not added due to ambiguity of which radial coordinate to use
            key = "ANGF"
            key2 = "VTOR"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("AFTOR",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("AFTOR",dprof,np.zeros(dprof.shape))
            elif key2 in jdict and gcp in CC and "RMAJORO" in CC[gcp]:
                ptemp = jdict[key2][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                (rnorm,jrnorm,ernorm) = CC.convert(qlkxfull,gcs,"RMAJORO",gcp,gcp)
                if pprocopt != 0:
                    ptemp = ptemp / rnorm
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                if pprocopt == 0:
                    # This option gives strange results for toroidal velocity gradients - not sure why
                    (rtemp,jrtemp,ertemp) = CC.convert(qlkxgrid,gcs,"RMAJORO",gcp,gcp)
                    jprof = jprof / rtemp
                    dprof = (dprof - jprof / (jrtemp * rtemp)) / rtemp
                PC.addProfileData("AFTOR",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("AFTOR",dprof,np.zeros(dprof.shape))
            # Radial electric field component corresponding to toroidal rotation - only this is required when using pure toroidal rotation option
            key = "ETOR"
            if pprocopt != 0 and key in jdict and "RZERO" in ZC and "F" in jdict and "BPOL" in jdict and "R" in jdict and "RI" in jdict and "Q" in jdict:
                btzero = float(jdict["F"][jidx,:][0] / ZC["RZERO"][""])
                btot = np.sqrt(np.power(jdict["BPOL"][jidx,:].flatten(),2.0) + np.power(btzero,2.0))
                rminvec = (jdict["R"][jidx,:].flatten() - jdict["RI"][jidx,:].flatten()) / 2.0
                rmajvec = (jdict["R"][jidx,:].flatten() + jdict["RI"][jidx,:].flatten()) / 2.0
                ptemp = (rmajvec / jdict["R"][jidx,:].flatten()) * (jdict[key][jidx,:].flatten() / btot) * jdict["Q"][jidx,:].flatten() / rminvec
                ptemp = np.hstack((0.0,ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("UTORGEO",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("UTORGEO",dprof,np.zeros(dprof.shape))
#  Trials for various radial vectors - settled on one above, no idea if it is actually consistent but it provides decent values
#            if key in jdict and "R" in jdict and "RI" in jdict:
#                rmajvec = (jdict["R"][jidx,:].flatten() + jdict["RI"][jidx,:].flatten()) / 2.0
#                ptemp = jdict[key][jidx,:].flatten() * (rmajvec / jdict["R"][jidx,:].flatten())
#                ptemp = np.hstack((0.0,ptemp))
#                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
#                jprof = pfunc(qlkxgrid)
#                dprof = np.diff(ptemp) / np.diff(qlkxfull)
#                PC.addProfileData("ETOR",jprof,np.zeros(jprof.shape))
#                PC.addProfileDerivativeData("ETOR",dprof,np.zeros(dprof.shape))
#            if key in jdict and "RZERO" in ZC and "F" in jdict and "BPOL" in jdict:
#                btzero = float(jdict["F"][jidx,:][0] / ZC["RZERO"][""])
#                btot = np.sqrt(np.power(jdict["BPOL"][jidx,:].flatten(),2.0) + np.power(btzero,2.0))
#                rmajvec = (jdict["R"][jidx,:].flatten() + jdict["RI"][jidx,:].flatten()) / 2.0
#                ptemp = (rmajvec / jdict["R"][jidx,:].flatten()) * jdict[key][jidx,:].flatten() / btot
#                ptemp = np.hstack((0.0,ptemp))
#                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
#                jprof = pfunc(qlkxgrid)
#                dprof = np.diff(ptemp) / np.diff(qlkxfull)
#                PC.addProfileData("UPER",jprof,np.zeros(jprof.shape))
#                PC.addProfileDerivativeData("UPER",dprof,np.zeros(dprof.shape))

            if not fpuretor:
                # Add poloidal angular frequency (not a flux function?) - rotation velocity not added due to ambiguity of which radial coordinate to use
                key = "VPOL"
                if key in jdict and gcp in CC and "RMINORO" in CC[gcp]:
                    ptemp = jdict[key][jidx,:].flatten()
                    ptemp = np.hstack((ptemp[0],ptemp))
                    (rnorm,jrnorm,ernorm) = CC.convert(qlkxfull,gcs,"RMINORO",gcp,gcp)
                    if pprocopt != 0:
                        ptemp = ptemp / rnorm
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    if pprocopt == 0:
                        # This option gives strange results for poloidal velocity gradients - not sure why
                        (rtemp,jrtemp,ertemp) = CC.convert(qlkxgrid,gcs,"RMAJORO",gcp,gcp)
                        jprof = jprof / rtemp
                        dprof = (dprof - jprof / (jrtemp * rtemp)) / rtemp
                    PC.addProfileData("AFPOL",jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("AFPOL",dprof,np.zeros(dprof.shape))
                key = "EPOL"
                if key in jdict and "RZERO" in ZC and "F" in jdict and "BPOL" in jdict and "R" in jdict and "RI" in jdict and "Q" in jdict:
                    btzero = float(jdict["F"][jidx,:][0] / ZC["RZERO"][""])
                    btot = np.sqrt(np.power(jdict["BPOL"][jidx,:].flatten(),2.0) + np.power(btzero,2.0))
                    rminvec = (jdict["R"][jidx,:].flatten() - jdict["RI"][jidx,:].flatten()) / 2.0
                    # Modification of radial vector of poloidal velocity from r_minor_outer to r_minor_midplane_average
                    if pprocopt == 1:
                        rminvec = jdict["R"][jidx,:].flatten() - rmag
                    ptemp = (jdict[key][jidx,:].flatten() / btot) * jdict["Q"][jidx,:].flatten() / rminvec
                    ptemp = np.hstack((0.0,ptemp))
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData("UPOLGEO",jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("UPOLGEO",dprof,np.zeros(dprof.shape))
                key = "EPRE"
                if key in jdict and "RZERO" in ZC and "F" in jdict and "BPOL" in jdict and "R" in jdict and "RI" in jdict and "Q" in jdict:
                    btzero = float(jdict["F"][jidx,:][0] / ZC["RZERO"][""])
                    btot = np.sqrt(np.power(jdict["BPOL"][jidx,:].flatten(),2.0) + np.power(btzero,2.0))
                    rminvec = (jdict["R"][jidx,:].flatten() - jdict["RI"][jidx,:].flatten()) / 2.0
                    rjacob = np.ones(rminvec.shape)
                    # Modification of coordinate system of ion pressure gradient from r_minor_outer to r_minor_midplane_average
                    if pprocopt == 1:
                        rjacob = np.diff(np.hstack((rmag,jdict["R"][jidx,:].flatten()))) / np.diff(np.hstack((0.0,rminvec)))
                    ptemp = rjacob * (jdict[key][jidx,:].flatten() / btot) * jdict["Q"][jidx,:].flatten() / rminvec
                    ptemp = np.hstack((0.0,ptemp))
                    dprof = np.diff(ptemp) / np.diff(qlkxfull)
                    if pprocopt == 2:
                        hstep = float(np.mean(np.diff(qlkxfull)))
                        dptemp = savgol_filter(ptemp,5,3,deriv=1,delta=hstep)
                        ptemp = savgol_filter(ptemp,5,3,deriv=0,delta=hstep)
                        dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                    pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                    jprof = pfunc(qlkxgrid)
                    PC.addProfileData("UDPIGEO",jprof,np.zeros(jprof.shape))
                    PC.addProfileDerivativeData("UDPIGEO",dprof,np.zeros(dprof.shape))

            # Ohmic heat source - must be divided amongst major species for consistency in EX2GK (not the best treatment, but will fix later)
            key = "QOH"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("STEOHM",jprof / 2.0,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STEOHM",dprof / 2.0,np.zeros(dprof.shape))
                PC.addProfileData("STIOHM",jprof / 2.0,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STIOHM",dprof / 2.0,np.zeros(dprof.shape))

            # Fast ion species meta-data - ignored for now
#            if "AFI1"+src in jdict and jdict["AFI1"+src] is not None and "ZFI1"+src in jdict and jdict["ZFI1"+src] is not None:
#                MC.addFastIonMetaData(jdict["ZFI1"+src],jdict["AFI1"+src],weight=0.01)
#                ntag = "%d" % MC["NUMFI"]
#                MC.addData("SRCFI"+ntag,src)
#            if "AFI2"+src in jdict and jdict["AFI2"+src] is not None and "ZFI2"+src in jdict and jdict["ZFI2"+src] is not None:
#                MC.addFastIonMetaData(jdict["ZFI2"+src],jdict["AFI2"+src],weight=0.01)
#                ntag = "%d" % MC["NUMFI"]
#                MC.addData("SRCFI"+ntag,src)

            key = "QNBE"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("STENBI",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STENBI",dprof,np.zeros(dprof.shape))

            key = "QNBI"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("STINBI",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STINBI",dprof,np.zeros(dprof.shape))

            key = "QRFE"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("STEICRH",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STEICRH",dprof,np.zeros(dprof.shape))

            key = "QRFI"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("STIICRH",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STIICRH",dprof,np.zeros(dprof.shape))

            key = "QECE"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("STEECRH",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("STEECRH",dprof,np.zeros(dprof.shape))

            key = "DNBD"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("NFINBI",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("NFINBI",dprof,np.zeros(dprof.shape))

            key = "WNBD"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("WFINBI",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("WFINBI",dprof,np.zeros(dprof.shape))

            key = "DRFD"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("NFIICRH",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("NFIICRH",dprof,np.zeros(dprof.shape))

            key = "WRFD"
            if key in jdict:
                ptemp = jdict[key][jidx,:].flatten()
                ptemp = np.hstack((ptemp[0],ptemp))
                dprof = np.diff(ptemp) / np.diff(qlkxfull)
                if pprocopt == 2:
                    hstep = float(np.mean(np.diff(qlkxfull)))
                    dptemp = savgol_filter(ptemp,savgol_window,3,deriv=1,delta=hstep)
                    ptemp = savgol_filter(ptemp,savgol_window,3,deriv=0,delta=hstep)
                    dprof = dptemp[:-1] + 0.5 * np.diff(dptemp)
                pfunc = interp1d(qlkxfull,ptemp,kind='linear')
                jprof = pfunc(qlkxgrid)
                PC.addProfileData("WFIICRH",jprof,np.zeros(jprof.shape))
                PC.addProfileDerivativeData("WFIICRH",dprof,np.zeros(dprof.shape))

            if fdebug:
                print("jetto_adapter.py: %s profiles populated." % (type(PC).__name__))

            # Transfer processing flags as metadata
            FC.setFlag("GPCOORD",False)
            FC.setFlag("JETTO",True)        # This flag is required for the EX2GK QuaLiKiz adapter to apply TCI numerical schemes
            FC.setFlag("FITDONE",True)

            if fdebug:
                print("jetto_adapter.py: %s populated." % (type(FC).__name__))

            outobj = classes.EX2GKTimeWindow(meta=MC,cd=CC,zd=ZC,pd=PC,fso=KC,flag=FC)
            outobj.postProcess(no_assumptions=True)

            if fdebug:
                print("jetto_adapter.py: %s populated." % (type(outobj).__name__))

            outlist.append(copy.deepcopy(outobj))

            if fdebug:
                print("import_from_JSP() completed for %s: %10.4f" % (jetto_rundir_path,tslice))

    return outlist
