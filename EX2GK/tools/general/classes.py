# Classes for standardization of extracted data for EX2GK program
# Developer: Aaron Ho - 12/08/2018

# Required imports
import os
import sys
import subprocess
import copy
import warnings
from operator import itemgetter
from collections import OrderedDict
from pathlib import Path
import numpy as np
import re
import json
import datetime
from collections.abc import MutableMapping
from packaging import version
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz
import pandas as pd

# Internal package imports
from EX2GK import __version__
from EX2GK.tools.general import proctools as ptools, phystools as qtools, fmt_converter as fmtconv, imas_converter as imastool

number_types = (int, float,                                # Built-in types
                np.int8, np.int16, np.int32, np.int64,     # Signed integer types
                np.uint8, np.uint16, np.uint32, np.uint64, # Unsigned integer types
                np.float16, np.float32, np.float64         # Floating point decimal types
               )

array_types = (list, tuple,                                # Built-in types
               np.ndarray,                                 # numpy array
               pd.Series                                   # pandas array
              )


class EX2GKMetaContainer(dict):

    # Class variables
    required_names = {"DEVICE": 'Experimental device identifier',
                      "INTERFACE": 'Data extraction interface identifier',
                      "MATWALL": 'Main element of first wall or plasma facing components',
                      "MATSTRIKE": 'Main element of plasma strike surfaces, limiter or divertor',
                      "CONFIG": 'Plasma configuration / heat exhaust scheme',
                      "SHOT": 'Discharge identification number',
                      "T1": 'Time window start, as referenced in experimental data repository',
                      "T2": 'Time window end, as referenced in experimental data repository',
                      "WINTYPE": 'Type identification index for averaging window',
                      "WINPHASE": 'Phase identification index of time window',
                      "EQSRC": 'Equilibrium source identifier',
                      "NUMI": 'Number of main ion species',
                      "NUMIMP": 'Number of impurity ion species which have data in time window',
                      "NUMZ": 'Number of filler impurity ion species, used to enforce quasineutrailty',
                      "NUMG": 'Number of gas injection species, used to track impurity seeding',
                      "NUMFI": 'Number of fast ion species, used to track impact of heating systems',
                      "CSO": 'Name of the requested coordinate system on which output is expressed',
                      "CSOP": 'Prefix of the requested coordinate system on which output is expressed',
                      "CSON": 'Number of points for the fit prediction output vector',
                      "CSOMIN": 'Minimum coordinate value for the fit prediction output vector',
                      "CSOMAX": 'Maximum coordinate value for the fit prediction output vector',
                      "QCPASS": 'Pass criteria of data quality checks, expressed in sigma',
                      "CODE": 'Target code for post-processing, if performed',
                      "CODETAG": 'Field prefix for post-processed quantities, if performed',
                      "CODEBASE": 'Reference coordinate for post-processed quantities, if performed',
                      "other": 'Any additional non-required quantity, use uppercase field name'}
    window_types = ["RECTANGULAR", "GAUSSIAN", "HANNING", "UNKNOWN"]
    plasma_configs = ["LIMITER", "DIVERTOR", "DOUBLENULL", "UNKNOWN"]

    def __init__(self, *args, **kwargs):
        super(EX2GKMetaContainer, self).__init__()
        for key in self.required_names:
            if key in ["NUMI", "NUMIMP", "NUMZ", "NUMG", "NUMFI"]:
                self.setdefault(key, 0)
            elif key == "CSOP":
                self.setdefault(key, "")
            elif key != "other":
                self.setdefault(key)
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        descdict = self.required_names
        dellist = []
        for key in descdict:
            if key not in self:
                dellist.append(key)
        for key in dellist:
            del descdict[key]
        return descdict

    def isReady(self, verbose=0):
        gflag = True
        for item in self.required_names:
#            if item not in self:
#                gflag = False
#                warnings.warn("%s object must have a valid value for %s: %s" % (type(self).__name__, item, self.required_names[item]))
            if item in ["DEVICE", "MATWALL", "MATSTRIKE", "SHOT", "T1", "T2", "WINTYPE", "EQSRC"] and self[item] is None:
                gflag = False
                warnings.warn("%s object must have a valid value for %s: %s" % (type(self).__name__, item, self.required_names[item]))
        if "ZZ1" not in self or self["ZZ1"] is None:
            gflag = False
            warnings.warn("%s object must have a valid value for ZZ1: Charge of first filler impurity species, used to enforce quasineutrality" % (type(self).__name__))
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def addValue(self, name, value):
        if not isinstance(name, str):
            raise TypeError("%s name argument must be a string" % (type(self).addValue.__name__))
        self[name] = value

    def autoSetFillerImpurityData(self, second_filler_zeff_limit=None, last_resort=None, leave_blank=False, verbose=0):
        if second_filler_zeff_limit is not None:
            if not isinstance(second_filler_zeff_limit, number_types):
                raise TypeError("%s second filler impurity Z-effective limit must be a number greater than unity" % (type(self).autoSetFillerImpurityData.__name__))
            elif second_filler_zeff_limit is not None and second_filler_zeff_limit > 1.0:
                raise ValueError("%s second filler impurity Z-effective limit must be a number greater than unity" % (type(self).autoSetFillerImpurityData.__name__))
        if last_resort is not None and not isinstance(last_resort, str):
            raise TypeError("%s last resort argument must be a string denoting the element to be used as a backup" % (type(self).autoSetFillerImpurityData.__name__))
        if not leave_blank:
            (nwall, awall, zwall) = ptools.define_ion_species(short_name=self["MATWALL"]) if self["MATWALL"] is not None else (None, None, None)
            (nstrike, astrike, zstrike) = ptools.define_ion_species(short_name=self["MATSTRIKE"]) if self["MATSTRIKE"] is not None else (None, None, None)
            (nlast, alast, zlast) = ptools.define_ion_species(short_name=last_resort) if last_resort is not None else (None, None, None)
            zz1 = None
            zz2 = None
            if zwall is not None:
                if zstrike is not None:
                    (matz1, az1, zz1, matz2, az2, zz2) = (nwall, awall, zwall, nstrike, astrike, zstrike) if zwall < zstrike else (nstrike, astrike, zstrike, nwall, awall, zwall)
                else:
                    (matz1, az1, zz1) = (nwall, awall, zwall)
            elif zstrike is not None:
                (matz1, az1, zz1) = (nstrike, astrike, zstrike)
            for idx in range(0, self["NUMIMP"]):
                itag = "%d" % (idx+1)
                if "ZIMP"+itag in self and isinstance(self["ZIMP"+itag], number_types):
                    if zz2 is not None and np.abs(zz2 - self["ZIMP"+itag]) < 1.0e-6:
                        (matz2, az2, zz2) = (None, None, None)
                    if zz1 is not None and np.abs(zz1 - self["ZIMP"+itag]) < 1.0e-6:
                        (matz1, az1, zz1) = (matz2, az2, zz2)
                        (matz2, az2, zz2) = (None, None, None)
            if zz1 is None and zz2 is None and zlast is not None:
                (matz1, az1, zz1) = (nlast, alast, zlast)
            if zz1 is not None:
                if "MATZ1" in self and isinstance(self["MATZ1"], str):
                    warnings.warn("%s will replace already existing first filler species %s with new species %s" % (type(self).autoSetFillerImpurityData.__name__, self["MATZ1"], matz1))
                self["MATZ1"] = matz1
                self["AZ1"] = az1
                self["ZZ1"] = zz1
            if second_filler_zeff_limit is not None:
                if zz2 is None and nlast != matz1:
                    (matz2, az2, zz2) = (nlast, alast, zlast)
                if zz2 is not None:
                    if "MATZ2" in self and isinstance(self["MATZ2"], str):
                        warnings.warn("%s will replace already existing second filler species %s with new species %s" % (type(self).autoSetFillerImpurityData.__name__, self["MATZ2"], matz2))
                    self["ZLIM1"] = float(second_filler_zeff_limit)
                    self["MATZ2"] = matz2
                    self["AZ2"] = az2
                    self["ZZ2"] = zz2
            if isinstance(verbose, int) and verbose > 0:
                if "MATZ1" in self and isinstance(self["MATZ1"], str):
                    print("%s assigned %s as the first filler impurity species." % (type(self).autoSetFillerImpurityData.__name__, self["MATZ1"]))
                if "MATZ2" in self and isinstance(self["MATZ2"], str):
                    print("%s assigned %s as the second filler impurity species." % (type(self).autoSetFillerImpurityData.__name__, self["MATZ2"]))
        else:
            self["MATZ1"] = "none"
            self["AZ1"] = 0.0
            self["ZZ1"] = 0.0
            if isinstance(verbose, int) and verbose > 0:
                if "MATZ1" in self and isinstance(self["MATZ1"], str):
                    print("%s instructed to assign no filler impurity species." % (type(self).autoSetFillerImpurityData.__name__))

    def setRequiredMetaData(self, device_name=None, extraction_interface=None, discharge_identifier=None, \
                            time_window_start=None, time_window_end=None, window_type=None, window_phase=None, \
                            equilibrium_source=None, wall_material=None, contact_material=None, \
                            plasma_configuration_index=None):
        self["DEVICE"] = device_name
        self["INTERFACE"] = extraction_interface
        self["SHOT"] = discharge_identifier
        self["T1"] = time_window_start
        self["T2"] = time_window_end
        self["WINTYPE"] = window_type
        self["WINPHASE"] = window_phase
        self["EQSRC"] = equilibrium_source
        self["MATWALL"] = wall_material
        self["MATSTRIKE"] = contact_material
        self["CONFIG"] = plasma_configuration_index

    def addIonMetaData(self, charge=None, mass=None, name=None, weight=None):
        if not isinstance(charge, number_types) and not isinstance(name, str):
            raise TypeError("%s must be given a valid charge argument or a valid name argument" % (type(self).addIonMetaData.__name__))
        if name is None and int(charge) <= 0:
            raise ValueError("%s charge argument must be an integer greater than zero" % (type(self).addIonMetaData.__name__))
        (nval, aval, zval) = ptools.define_ion_species(z=charge, a=mass, short_name=name, user_mass=True)
        if nval is not None and aval is not None and zval is not None:
            index = self["NUMI"] + 1
            itag = "%d" % (index)
            wval = 1.0
            if isinstance(mass, number_types) and float(mass) >= zval:
                aval = float(mass)
            if isinstance(weight, number_types) and float(weight) >= 0.0:
                wval = float(weight)
            self["MATI"+itag] = nval
            self["AI"+itag] = aval
            self["ZI"+itag] = zval
            self["WGTI"+itag] = wval
            self["NUMI"] = index
        else:
            warnings.warn("%s arguments did not produce a valid ion species, input not accepted" % (type(self).addIonMetaData.__name__))

    def getIonMetaData(self):
        output = []
        for index in range(self["NUMI"]):
            itag = "%d" % (index+1)
            spec = (index + 1, self["MATI"+itag], self["AI"+itag], self["ZI"+itag], self["WGTI"+itag])
            output.append(spec)
        return output

    def clearIonMetaData(self):
        warnings.warn("%s main ion meta data is being deleted, re-specify these fields with %s" % (type(self).__name__, type(self).addIonMetaData.__name__))
        for index in range(self["NUMI"]):
            itag = "%d" % (index+1)
            del self["MATI"+itag]
            del self["AI"+itag]
            del self["ZI"+itag]
            del self["WGTI"+itag]
        self["NUMI"] = 0

    def addImpurityMetaData(self, charge=None, mass=None, name=None, weight=None):
        if not isinstance(charge, number_types) and not isinstance(name, str):
            raise TypeError("%s must be given a valid charge argument or a valid name argument" % (type(self).addImpurityMetaData.__name__))
        if name is None and int(charge) <= 0:
            raise ValueError("%s charge argument must be an integer greater than zero" % (type(self).addImpurityMetaData.__name__))
        (nval, aval, zval) = ptools.define_ion_species(z=charge, a=mass, short_name=name, user_mass=True)
        if nval is not None and aval is not None and zval is not None:
            index = self["NUMIMP"] + 1
            itag = "%d" % (index)
            wval = 1.0
            if isinstance(mass, number_types) and float(mass) >= zval:
                aval = float(mass)
            if isinstance(weight, number_types) and float(weight) >= 0.0:
                wval = float(weight)
            self["MATIMP"+itag] = nval
            self["AIMP"+itag] = aval
            self["ZIMP"+itag] = zval
            self["WGTIMP"+itag] = wval
            self["NUMIMP"] = index
        else:
            warnings.warn("%s arguments did not produce a valid ion species, input not accepted" % (type(self).addImpurityMetaData.__name__))

    def clearImpurityMetaData(self):
        warnings.warn("%s impurity ion meta data is being deleted, re-specify these fields with %s" % (type(self).__name__, type(self).addImpurityMetaData.__name__))
        for index in range(self["NUMIMP"]):
            itag = "%d" % (index+1)
            del self["MATIMP"+itag]
            del self["AIMP"+itag]
            del self["ZIMP"+itag]
            del self["WGTIMP"+itag]
        self["NUMIMP"] = 0

    def addFillerImpurityMetaData(self, charge=None, mass=None, name=None, weight=None):
        if not isinstance(charge, number_types) and not isinstance(name, str):
            raise TypeError("%s must be given a valid charge argument or a valid name argument" % (type(self).addFillerImpurityMetaData.__name__))
        if name is None and int(charge) <= 0:
            raise ValueError("%s charge argument must be an integer greater than zero" % (type(self).addFillerImpurityMetaData.__name__))
        (nval, aval, zval) = ptools.define_ion_species(z=charge, a=mass, short_name=name, user_mass=True)
        if nval is not None and aval is not None and zval is not None:
            index = self["NUMZ"] + 1
            itag = "%d" % (index)
            wval = 1.0
            if isinstance(mass, number_types) and float(mass) >= zval:
                aval = float(mass)
                if isinstance(name, str):
                    (tname, ta, tz) = ptools.define_ion_species(short_name=name)
                    if tname != "e":
                        nval = tname
            if isinstance(weight, number_types) and float(weight) >= 0.0:
                wval = float(weight)
            self["MATZ"+itag] = nval
            self["AZ"+itag] = aval
            self["ZZ"+itag] = zval
            self["WGTZ"+itag] = wval
            self["NUMZ"] = index
        else:
            warnings.warn("%s arguments did not produce a valid ion species, input not accepted" % (type(self).addFillerImpurityMetaData.__name__))

    def clearFillerImpurityMetaData(self):
        warnings.warn("%s assumed impurity ion meta data is being deleted, re-specify these fields with %s" % (type(self).__name__, type(self).addFillerImpurityMetaData.__name__))
        for index in range(self["NUMZ"]):
            itag = "%d" % (index+1)
            del self["MATZ"+itag]
            del self["AZ"+itag]
            del self["ZZ"+itag]
            del self["WGTZ"+itag]
            if "ZLIM"+itag in self:
                warnings.warn("%s assumed impurity ion meta data limit is being deleted, re-specify this field with %s" % (type(self).__name__, type(self).addFillerImpurityLimitMetaData.__name__))
                del self["ZLIM"+itag]
        self["NUMZ"] = 0

    def addFillerImpurityLimitMetaData(self, limit, index=None):
        if not isinstance(limit, number_types):
            raise TypeError("%s limit argument must be a number greater than unity" % (type(self).addFillerImpurityLimitMetaData.__name__))
        if float(limit) <= 1.0:
            raise ValueError("%s limit argument must be a number greater than unity" % (type(self).addFillerImpurityLimitMetaData.__name__))
        if index is not None and not isinstance(index, number_types):
            raise TypeError("%s index argument must be an integer greater than zero" % (type(self).addFillerImpurityLimitMetaData.__name__))
            if int(index) < 1:
                raise ValueError("%s index argument must be an integer greater than zero" % (type(self).addFillerImpurityLimitMetaData.__name__))
        itag = None
        if index is not None:
            if self["NUMZ"] >= int(index):
                itag = "%d" % (index)
            else:
                limit = None
                warnings.warn("%s index argument is invalid, input not accepted" % (type(self).addFillerImpurityLimitMetaData.__name__))
        if limit is not None and itag is None:
            numz = self["NUMZ"] - 1 if self["NUMZ"] > 0 else 0
            for idx in range(0, numz):
                ttag = "%d" % (idx+1)
                if "ZLIM"+ttag not in self:
                    itag = ttag
            if itag is None:
                limit = None
                warnings.warn("All filler impurities in %s already have limits, input not accepted" % (type(self).__name__))
        if limit is not None and itag is not None:
            self["ZLIM"+itag] = limit

    def addFastIonMetaData(self, charge=None, mass=None, name=None, weight=None):
        if not isinstance(charge, number_types) and not isinstance(name, str):
            raise TypeError("%s must be given a valid charge argument or a valid name argument" % (type(self).addFastIonMetaData.__name__))
        if name is None and int(charge) <= 0:
            raise ValueError("%s charge argument must be an integer greater than zero" % (type(self).addFastIonMetaData.__name__))
        (nval, aval, zval) = ptools.define_ion_species(z=charge, a=mass, short_name=name, user_mass=True)
        if nval is not None and aval is not None and zval is not None:
            index = self["NUMFI"] + 1 # if "NUMFI" in self else 0
            itag = "%d" % (index)
            wval = 1.0
            if isinstance(mass, number_types) and float(mass) >= zval:
                aval = float(mass)
            if isinstance(weight, number_types) and float(weight) >= 0.0:
                wval = float(weight)
            self["MATFI"+itag] = nval
            self["AFI"+itag] = aval
            self["ZFI"+itag] = zval
            self["WGTFI"+itag] = wval
            self["NUMFI"] = index
        else:
            warnings.warn("%s arguments did not produce a valid ion species, input not accepted" % (type(self).addFastIonMetaData.__name__))

    def clearFastIonMetaData(self):
        if "NUMFI" in self:
            warnings.warn("%s fast ion meta data is being deleted, re-specify these fields with %s" % (type(self).__name__, type(self).addFastIonMetaData.__name__))
            for index in range(self["NUMFI"]):
                itag = "%d" % (index+1)
                del self["MATFI"+itag]
                del self["AFI"+itag]
                del self["ZFI"+itag]
                del self["WGTFI"+itag]
            self["NUMFI"] = 0

    def addGasInjectionMetaData(self, charge=None, mass=None, name=None):
        if not isinstance(charge, number_types) and not isinstance(name, str):
            raise TypeError("%s must be given a valid charge argument or a valid name argument" % (type(self).addGasInjectionMetaData.__name__))
        if name is None and int(charge) <= 0:
            raise ValueError("%s charge argument must be an integer greater than zero" % (type(self).addGasInjectionMetaData.__name__))
        (nval, aval, zval) = ptools.define_ion_species(z=charge, a=mass, short_name=name, user_mass=True)
        if nval is not None and aval is not None and zval is not None:
            index = self["NUMG"] + 1 if "NUMG" in self else 0
            itag = "%d" % (index)
            wval = 1.0
            if isinstance(mass, number_types) and float(mass) >= zval:
                aval = float(mass)
#            if isinstance(weight, number_types) and float(weight) >= 0.0:
#                wval = float(weight)
            self["MATG"+itag] = nval
            self["AG"+itag] = aval
            self["ZG"+itag] = zval
#            self["WGTG"+itag] = wval
            self["NUMG"] = index
        else:
            warnings.warn("%s arguments did not produce a valid ion species, input not accepted" % (type(self).addGasInjectionMetaData.__name__))

    def clearGasInjectionMetaData(self):
        if "NUMG" in self:
            warnings.warn("%s gas injection ion meta data is being deleted, re-specify these fields with %s" % (type(self).__name__, type(self).addGasInjectionMetaData.__name__))
            for index in range(self["NUMG"]):
                itag = "%d" % (index+1)
                del self["MATG"+itag]
                del self["AG"+itag]
                del self["ZG"+itag]
#                del self["WGTG"+itag]
            self["NUMG"] = 0

    def setFittingMetaData(self, fit_coordinate, fit_vector=None, fit_points=None, fit_maximum=None, fit_minimum=None, fit_coord_prefix=None):
        self["CSO"] = fit_coordinate
        if isinstance(fit_vector, array_types):
            self["CSON"] = len(fit_vector)
            self["CSOMIN"] = np.nanmin(fit_vector)
            self["CSOMAX"] = np.nanmax(fit_vector)
        else:
            self["CSON"] = fit_points
            self["CSOMIN"] = fit_minimum
            self["CSOMAX"] = fit_maximum
        self["CSOP"] = fit_coord_prefix.upper() if isinstance(fit_coord_prefix, str) else ""

    def setQualityCheckPassCriteria(self, pass_criteria):
        self["QCPASS"] = pass_criteria

    def setPostProcessingCodeMetaData(self, target_code, internal_tag=None, base_coordinate=None):
        self["CODE"] = target_code
        self["CODETAG"] = target_code.upper() if not isinstance(internal_tag, str) and isinstance(target_code, str) else internal_tag
        self["CODEBASE"] = base_coordinate.upper() if isinstance(base_coordinate, str) else None

    def resetPostProcessingCodeMetaData(self):
        for key in list(self.keys()):
            if key.startswith("CODE"):
                del self[key]
        self["CODE"] = None
        self["CODETAG"] = None
        self["CODEBASE"] = None

    def getTimeWindowPhase(self):
        return self["WINPHASE"]

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        val = value
        if key.upper() in self.required_names and value is not None:
            if key.upper() in ["T1", "T2", "CSOMIN", "CSOMAX"]:
                if not isinstance(value, number_types):
                    raise TypeError("%s %s field must be a number" % (type(self).__name__, key))
                val = float(value)
            elif key.upper() in ["QCPASS"]:
                if not isinstance(value, number_types):
                    raise TypeError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
                if float(value) < 0.0:
                    raise ValueError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
                val = float(value)
            elif key.upper() in ["CONFIG", "SHOT", "WINPHASE", "WINTYPE"]:
                if not isinstance(value, number_types):
                    raise TypeError("%s %s field must be an integer" % (type(self).__name__, key))
                val = int(value)
            elif key.upper() in ["CSON", "NUMI", "NUMIMP", "NUMZ", "NUMFI", "NUMG"]:
                if not isinstance(value, number_types):
                    raise TypeError("%s %s field must be an integer greater than or equal to zero" % (type(self).__name__, key))
                if int(value) < 0:
                    raise ValueError("%s %s field must be an integer greater than or equal to zero" % (type(self).__name__, key))
                val = int(value)
            elif key.upper() == "OTHER":
                raise KeyError("Funny guy... Use another uppercase field name for additional entries to %s" % (type(self).__name__))
            elif not isinstance(value, str):
                raise TypeError("%s %s field must be a string" % (type(self).__name__, key))
        elif re.match(r'^[AZ](I|IMP|Z|FI|G)[0-9]*$', key.upper()):
            if not isinstance(value, number_types):
                raise TypeError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            if float(value) < 0.0:
                raise ValueError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            val = float(value)
        elif re.match(r'^WGT(I|IMP|Z|FI)[0-9]*$', key.upper()):
            if not isinstance(value, number_types):
                raise TypeError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            if float(value) < 0.0:
                raise ValueError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            val = float(value)
        elif re.match(r'^MAT(I|IMP|Z|FI|G)[0-9]*$', key.upper()):
            if not isinstance(value, str):
                raise TypeError("%s %s field must be a string" % (type(self).__name__, key))
            val = value
        elif re.match(r'^ZLIM[0-9]+$', key.upper()):
            if not isinstance(value, number_types):
                raise TypeError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            if float(value) < 0.0:
                raise ValueError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            val = float(value)
        elif re.match(r'^.*EBMULT$', key.upper()):
            if not isinstance(value, number_types):
                raise TypeError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            if float(value) < 0.0:
                raise ValueError("%s %s field must be a number greater or equal to zero" % (type(self).__name__, key))
            val = float(value)
        elif re.match(r'^SRC.*$', key.upper()):
            val = value
        elif re.match(r'^CFG(NBI|ICRH|ECRH|LH)$', key.upper()):
            val = value
        elif key.upper() not in self.required_names:
            warnings.warn("%s field is not a standard item in %s, inserted anyway with an uppercase field name" % (key, type(self).__name__), UserWarning)
        super(EX2GKMetaContainer, self).__setitem__(key.upper(), copy.deepcopy(val))

    def update(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKMetaContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            try:
                self[key.upper()] = value
                dellist.append(key)
            except TypeError:
                pass
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        return copy.deepcopy(super(EX2GKMetaContainer, self).copy())

    @classmethod
    def importFromPandas(cls, panda):
        return cls(**panda)

    def exportToPandas(self):
        return pd.Series(self)

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = copy.deepcopy(self[key])
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKCoordinate(dict):

    # Class variables
    required_names = {"V": 'Coordinate system vector',
                      "J": 'Coordinate system Jacobian vector referenced to a base system',
                      "E": 'Coordinate system error vector'}

    def __init__(self, *args, **kwargs):
        super(EX2GKCoordinate, self).__init__()
        self.name = None
        self.prefix = ""
        self.npoints = None
        for key in self.required_names:
            self.setdefault(key)
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        desc = "Undefined"
        if isinstance(self.name, str):
            (tag, label, unit, flabel, funit) = ptools.define_quantity(self.name)
            desc = label if label is not None else ""
            if desc:
                desc += " "
            desc += "[" + unit + "]" if unit else "[-]"
        return desc

    def setRequiredCoordinateData(self, cs_name, cs_vector, cs_jacobian=None, cs_error=None, cs_prefix=None):
        if not isinstance(cs_name, str):
            raise TypeError("%s name argument must be a string" % (type(self).setRequiredCoordinateData.__name__))
        if not isinstance(cs_vector, array_types):
            raise TypeError("%s vector argument must be array-like" % (type(self).setRequiredCoordinateData.__name__))
        if cs_jacobian is None:
            cs_jacobian = np.ones(cs_vector.shape, dtype=np.float64)
        if not isinstance(cs_jacobian, array_types):
            raise TypeError("%s jacobian argument must be array-like" % (type(self).setRequiredCoordinateData.__name__))
        if cs_error is None:
            cs_error = np.zeros(cs_vector.shape, dtype=np.float64)
        elif isinstance(cs_error, number_types):
            cs_error = np.full(cs_vector.shape, cs_error, dtype=np.float64)
        if not isinstance(cs_error, array_types):
            raise TypeError("%s error argument must be a number or array-like" % (type(self).setRequiredCoordinateData.__name__))
        if cs_prefix is None:
            cs_prefix = ""
        if not isinstance(cs_prefix, str):
            raise TypeError("%s vector argument must be a string" % (type(self).setRequiredCoordinateData.__name__))
        if self.npoints is not None:
            warnings.warn("Overwriting data in %s for %s" % (type(self).__name__, name), UserWarning)
        self.reset()
        self.name = cs_name
        self.prefix = cs_prefix
        self["V"] = cs_vector
        self["J"] = cs_jacobian
        self["E"] = cs_error

    def reset(self):
        self.name = None
        self.prefix = ""
        self["V"] = None
        self["J"] = None
        self["E"] = None
        self.npoints = None

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        mm = re.match(r'^[A-Z]?(.+)$', key.upper())
        if mm:
            name = mm.group(1).upper()
            val = None
            if name in self.required_names and value is not None:
                if isinstance(value, array_types):
                    val = np.array(value, dtype=np.float64).flatten()
                    if self.npoints is not None and len(val) != self.npoints:
                        raise ValueError("%s entries must be of equal length" % (type(self).__name__))
                else:
                    raise TypeError("%s %s field must be array-like" % (type(self).__name__, name))
            self.npoints = len(val) if val is not None else None
            super(EX2GKCoordinate, self).__setitem__(name, val)
        else:
            warnings.warn("%s is not a valid field label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        if "prefix" not in self.__dict__:
            self.prefix = ""
        if "npoints" not in self.__dict__:
            self.npoints = None
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        if "prefix" in kwargs:
            if isinstance(kwargs["prefix"], str):
                self.prefix = kwargs["prefix"]
            else:
                warnings.warn("%s prefix keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["prefix"]
        if "npoints" in kwargs:
            if isinstance(kwargs["npoints"], number_types) and int(kwargs["npoints"]) > 0:
                self.npoints = int(kwargs["npoints"])
            else:
                warnings.warn("%s npoints keyword argument must be an integer greater than zero, input not accepted" % (type(self).__name__))
            del kwargs["npoints"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKCoordinate, self).__repr__())

    def __copy__(self):
        return type(self)(self, name=self.name, prefix=self.prefix, npoints=self.npoints)

    def copy(self):
        return type(self)(self, name=self.name, prefix=self.prefix, npoints=self.npoints)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.name is None:
            match &= (self.name is other.name)
        else:
            match &= (self.name == other.name)
        if self.prefix is None:
            match &= (self.prefix is other.prefix)
        else:
            match &= (self.prefix == other.prefix)
        if self.npoints is None:
            match &= (self.npoints is other.npoints)
        else:
            match &= (self.npoints == other.npoints)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            if re.match(r'^[VJE]$', key, flags=re.IGNORECASE):
                self[key.upper()] = value
                dellist.append(key)
            elif re.match(r'^P$', key, flags=re.IGNORECASE):
                self.prefix = value
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key, value in self.items():
            if value is not None:
                output[key] = copy.deepcopy(value)
        output["P"] = self.prefix
        return output

    @classmethod
    def importFromPandas(cls, panda, name=None, prefix=""):
        self = cls({key: val.values for key, val in panda.items()},
                   name=name,
                   prefix=prefix)
        return self

    def exportToPandas(self):
        df = pd.DataFrame(self)
        df = pd.concat([df], keys=[self.prefix + self.name], names=['var'], axis=1)
        df.fillna(np.NaN, inplace=True)
        return df

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = copy.deepcopy(self[key])
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKCoordinateSystem(dict):

    # Class variables
    allowed_names = {"PSIPOLN": 'Normalized poloidal flux',
                     "PSIPOL": 'Absolute poloidal flux',
                     "RHOPOLN": 'Normalized poloidal rho (sqrt norm poloidal flux)',
                     "PSITORN": 'Normalized toroidal flux',
                     "PSITOR": 'Absolute toroidal flux',
                     "RHOTORN": 'Normalized toroidal rho (sqrt norm toroidal flux)',
                     "RMAJORO": 'Absolute outer major radius at height of magnetic axis',
                     "RMAJORI": 'Absolute inner major radius at height of magnetic axis',
                     "RMINORO": 'Absolute outer minor radius at height of magnetic axis',
                     "RMINORI": 'Absolute inner minor radius at height of magnetic axis',
                     "RMAJORA": 'Averaged major radius at height of magnetic axis',
                     "RMINORA": 'Averaged minor radius at height of magnetic axis',
                     "FSVOL": 'Flux surface volume',
                     "FSAREA": 'Flux surface surface area',
                     #"BTOR": 'Absolute toroidal magnetic field',
                     #"BPOL": 'Absolute poloidal magnetic field',
                     "CONVERT": 'Conversion vector into another coordinate system base, if present'}

    def __init__(self, *args, **kwargs):
        super(EX2GKCoordinateSystem, self).__init__()
        self.base = None
        self.prefix = ""
        self.conversion_list = []
        self.update(*args, **kwargs)

    def info(self):
        return self.allowed_names

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], EX2GKCoordinate):
                descdict[key] = self[key].describe()
        return descdict

    def isReady(self, verbose=0):
        gflag = False
        for item in self.allowed_names:
            if item in self:
                gflag = True
        if self.base is None:
            gflag = False
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def isComplete(self):
        gflag = True
        for item in self.allowed_names:
            if item != "CONVERT" and item not in self:
                gflag = False
        return gflag

    def getPrefix(self):
        return self.prefix

    def addCoordinate(self, cs_name, cs_vector, cs_jacobian=None, cs_error=None, cs_prefix=None):
        if cs_prefix is None:
#            if self.base is None:
#                warnings.warn("%s prefix is not set and %s prefix argument is undefined, setting to empty" % (type(self).__name__, type(self).addCoordinate.__name__))
            cs_prefix = self.prefix
        if not isinstance(cs_prefix, str):
            raise TypeError("%s prefix argument must be a string" % (type(self).addCoordinate.__name__))
        if cs_name in self.allowed_names:
            if re.match('^[A-Z]?CONVERT[IO]?$', cs_name):
                raise ValueError("Use %s function for inserting coordinate system conversions" % (type(self).addConversionCoordinate.__name__))
            else:
                cobj = EX2GKCoordinate()
                cobj.setRequiredCoordinateData(cs_name, cs_vector, cs_jacobian, cs_error, cs_prefix)
                if self.base is None:
                    self.base = cs_name
                    self.prefix = cs_prefix
                elif cobj.npoints != self[self.base].npoints:
                    raise ValueError("%s base coordinate system set to %d points, all additional systems must contain exactly %d points" % (type(self).__name__, self[self.base].npoints, self[self.base].npoints))
                self[cs_name] = cobj
        else:
            warnings.warn("%s coordinate argument %s is invalid, coordinate not added" % (type(self).addCoordinate.__name__, cs_name))

    def addConversionCoordinate(self, cs_prefix, cs_vector, cs_jacobian=None, cs_error=None, cs_suffix=None):
        if not (isinstance(cs_prefix, str) and len(cs_prefix) == 1):
            raise TypeError("%s prefix argument must be a single alphabetical character" % (type(self).addConversionCoordinate.__name__))
        if cs_suffix is None:
            cs_suffix = ""
        if not isinstance(cs_suffix, str) and cs_suffix not in ["", "I", "O"]:
            raise TypeError("%s suffix argument must either be empty or one of the characters: I, O" % (type(self).addConversionCoordinate.__name__))
        if self.base is None:
            raise KeyError("%s object does not have a base coordinate system for conversion reference" % (type(self).__name__))
        cs_name = cs_prefix+"CONVERT"+cs_suffix
        cobj = EX2GKCoordinate()
        cobj.setRequiredCoordinateData(cs_name, cs_vector, cs_jacobian, cs_error, self.prefix)
        if cobj.npoints == self[self.base].npoints:
            self[cs_name] = cobj
            if cs_prefix not in self.conversion_list:
                self.conversion_list.append(cs_prefix)
        else:
            raise ValueError("%s base coordinate system set to %d points, all additional systems must contain exactly %d points" % (type(self).__name__, self[self.base].npoints, self[self.base].npoints))

    def addCoordinateObject(self, coordinate_object):
        if not isinstance(coordinate_object, EX2GKCoordinate):
            raise TypeError("%s object argument must be a %s object" % (type(self).addCoordinateObject.__name__, EX2GKCoordinate.__name__))
        if coordinate_object.name in self.allowed_names:
            if self.base is None:
                self.base = coordinate_object.name
                self.prefix = coordinate_object.prefix
            else:
                if coordinate_object.npoints != self[self.base].npoints:
                    raise ValueError("%s base coordinate system set to %d points, all additional systems must contain exactly %d points" % (type(self).__name__, self[self.base].npoints, self[self.base].npoints))
                if self.prefix != coordinate_object.prefix:
                    coordiante_object.prefix = self.prefix
                    warnings.warn("%s prefix does not match with that of %s object, swapping to prefix of %s object" % (type(coordinate_object).__name__, type(self).__name__, type(self).__name__))
            self[coordinate_object.name] = coordinate_object

    def computeMidplaneAveragedRadiusCoordinates(self, major_radius_magnetic_axis=None, overwrite=False):
        if "RMAJORO" in self and "RMAJORI" in self:
            if "RMAJORA" not in self or overwrite:
                rmajor_ave_v = (self["RMAJORO"]["V"] + self["RMAJORI"]["V"]) / 2.0
                jfilt = np.all([np.abs(self["RMAJORO"]["J"]) > 1.0e-6, np.abs(self["RMAJORI"]["J"]) > 1.0e-6, self["RMAJORO"]["J"] != self["RMAJORI"]["J"]], axis=0)
                rmajor_ave_j = np.ones(rmajor_ave_v.shape)
                if np.any(jfilt):
                    rmajor_ave_j[jfilt] = 2.0 * self["RMAJORO"]["J"][jfilt] * self["RMAJORI"]["J"][jfilt] / (self["RMAJORI"]["J"][jfilt] + self["RMAJORO"]["J"][jfilt])
                ifilt = np.isfinite(rmajor_ave_j)
                if np.any(np.invert(ifilt)):
                    ifunc = interp1d(rmajor_ave_v[ifilt], rmajor_ave_j[ifilt], bounds_error=False, fill_value='extrapolate')
                    rmajor_ave_j[np.invert(ifilt)] = ifunc(rmajor_ave_v[np.invert(ifilt)])
                rmajor_ave_e = np.sqrt(np.power(self["RMAJORO"]["E"], 2.0) + np.power(self["RMAJORI"]["E"], 2.0))
                self.addCoordinate("RMAJORA", rmajor_ave_v, rmajor_ave_j, rmajor_ave_e, cs_prefix=self.prefix)
            if "RMINORA" not in self or overwrite:
                rminor_ave_v = (self["RMAJORO"]["V"] - self["RMAJORI"]["V"]) / 2.0
                jfilt = np.all([np.abs(self["RMAJORO"]["J"]) > 1.0e-6, np.abs(self["RMAJORI"]["J"]) > 1.0e-6, self["RMAJORO"]["J"] != self["RMAJORI"]["J"]], axis=0)
                rminor_ave_j = np.ones(rminor_ave_v.shape)
                if np.any(jfilt):
                    rminor_ave_j[jfilt] = 2.0 * self["RMAJORO"]["J"][jfilt] * self["RMAJORI"]["J"][jfilt] / (self["RMAJORI"]["J"][jfilt] - self["RMAJORO"]["J"][jfilt])
                ifilt = np.isfinite(rminor_ave_j)
                if np.any(np.invert(ifilt)):
                    ifunc = interp1d(rminor_ave_v[ifilt], rminor_ave_j[ifilt], bounds_error=False, fill_value='extrapolate')
                    rminor_ave_j[np.invert(ifilt)] = ifunc(rminor_ave_v[np.invert(ifilt)])
                rminor_ave_e = np.sqrt(np.power(self["RMAJORO"]["E"], 2.0) + np.power(self["RMAJORI"]["E"], 2.0))
                self.addCoordinate("RMINORA", rminor_ave_v, rminor_ave_j, rminor_ave_e, cs_prefix=self.prefix)
        elif "RMINORO" in self and "RMINORI" in self:
            if isinstance(major_radius_magnetic_axis, number_types) and ("RMAJORA" not in self or overwrite):
                rmajor_ave_v = float(major_radius_magnetic_axis) + (self["RMINORO"]["V"] - self["RMINORI"]["V"]) / 2.0
                jfilt = np.all([np.abs(self["RMINORO"]["J"]) > 1.0e-6, np.abs(self["RMINORI"]["J"]) > 1.0e-6, self["RMINORO"]["J"] != self["RMINORI"]["J"]], axis=0)
                rmajor_ave_j = np.ones(rmajor_ave_v.shape)
                if np.any(jfilt):
                    rmajor_ave_j[jfilt] = 2.0 * self["RMINORO"]["J"][jfilt] * self["RMINORI"]["J"][jfilt] / (self["RMINORI"]["J"][jfilt] - self["RMINORO"]["J"][jfilt])
                ifilt = np.isfinite(rmajor_ave_j)
                if np.any(np.invert(ifilt)):
                    ifunc = interp1d(rmajor_ave_v[ifilt], rmajor_ave_j[ifilt], bounds_error=False, fill_value='extrapolate')
                    rmajor_ave_j[np.invert(ifilt)] = ifunc(rmajor_ave_v[np.invert(ifilt)])
                rmajor_ave_e = np.sqrt(np.power(self["RMINORO"]["E"], 2.0) + np.power(self["RMINORI"]["E"], 2.0))
                self.addCoordinate("RMAJORA", rmajor_ave_v, rmajor_ave_j, rmajor_ave_e, cs_prefix=self.prefix)
            if "RMINORA" not in self or overwrite:
                rminor_ave_v = (self["RMINORO"]["V"] + self["RMINORI"]["V"]) / 2.0
                jfilt = np.all([np.abs(self["RMINORO"]["J"]) > 1.0e-6, np.abs(self["RMINORI"]["J"]) > 1.0e-6, self["RMINORO"]["J"] != self["RMINORI"]["J"]], axis=0)
                rminor_ave_j = np.ones(rminor_ave_v.shape)
                if np.any(jfilt):
                    rminor_ave_j[jfilt] = 2.0 * self["RMINORO"]["J"][jfilt] * self["RMINORI"]["J"][jfilt] / (self["RMINORI"]["J"][jfilt] + self["RMINORO"]["J"][jfilt])
                ifilt = np.isfinite(rminor_ave_j)
                if np.any(np.invert(ifilt)):
                    ifunc = interp1d(rminor_ave_v[ifilt], rminor_ave_j[ifilt], bounds_error=False, fill_value='extrapolate')
                    rminor_ave_j[np.invert(ifilt)] = ifunc(rminor_ave_v[np.invert(ifilt)])
                rminor_ave_e = np.sqrt(np.power(self["RMINORO"]["E"], 2.0) + np.power(self["RMINORI"]["E"], 2.0))
                self.addCoordinate("RMINORA", rminor_ave_v, rminor_ave_j, rminor_ave_e, cs_prefix=self.prefix)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.allowed_names:
            if not isinstance(value, EX2GKCoordinate):
                raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKCoordinate.__name__))
            super(EX2GKCoordinateSystem, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            warnings.warn("%s is not a valid coordinate label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "base" not in self.__dict__:
            self.base = None
        if "prefix" not in self.__dict__:
            self.prefix = ""
        if "conversion_list" not in self.__dict__:
            self.conversion_list = []
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "base" in kwargs:
            if isinstance(kwargs["base"], str):
                self.base = kwargs["base"]
            else:
                warnings.warn("%s base keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["base"]
        if "prefix" in kwargs:
            if isinstance(kwargs["prefix"], str):
                self.prefix = kwargs["prefix"]
            else:
                warnings.warn("%s prefix keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["prefix"]
        if "conversion_list" in kwargs:
            if isinstance(kwargs["conversion_list"], list):
                self.conversion_list = kwargs["conversion_list"]
            else:
                warnings.warn("%s conversion_list keyword argument must be a list, input not accepted" % (type(self).__name__))
            del kwargs["conversion_list"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKCoordinateSystem, self).__repr__())

    def __copy__(self):
        return type(self)(self, base=self.base, prefix=self.prefix, conversion_list=self.conversion_list)

    def copy(self):
        return type(self)(self, base=self.base, prefix=self.prefix, conversion_list=self.conversion_list)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.base is None:
            match &= (self.base is other.base)
        else:
            match &= (self.base == other.base)
        if self.prefix is None:
            match &= (self.prefix is other.prefix)
        else:
            match &= (self.prefix == other.prefix)
        if self.conversion_list is None:
            match &= (self.conversion_list is other.conversion_list)
        else:
            match &= (self.conversion_list == other.conversion_list)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        base = ""
        prefix = ""
        for key, value in data_dict.items():
            mm = re.match(r'^([A-Z]?)BASE$', key, flags=re.IGNORECASE)
            if mm:
                prefix = mm.group(1).upper()
                base = value.upper()
                dellist.append(key)
        if prefix+base+"_V" in data_dict:
            tempdict = dict()
            tempdict["P"] = prefix.upper()
            tempdict["V"] = data_dict[prefix+base+"_V"]
            tempdict["J"] = data_dict[prefix+base+"_"+prefix+base+"_J"] if prefix+base+"_"+prefix+base+"_J" in data_dict else np.ones(tempdict["V"].shape, dtype=np.float64)
            tempdict["E"] = data_dict[prefix+base+"_E"] if prefix+base+"_E" in data_dict else np.zeros(tempdict["V"].shape, dtype=np.float64)
            cobj = EX2GKCoordinate.importFromDict(tempdict, True)
            cobj.name = base
            self.addCoordinateObject(cobj)
            if "V" in tempdict:
                dellist.append(prefix+base+"_V")
            if "J" in tempdict and prefix+base+"_"+prefix+base+"_J" in data_dict:
                dellist.append(prefix+base+"_"+prefix+base+"_J")
            if "E" in tempdict and prefix+base+"_E" in data_dict:
                dellist.append(prefix+base+"_E")
        for item in dellist:
            del data_dict[item]
        dellist = []
        for key in data_dict:
            mm = re.match(r'^'+prefix+'(.+)_V$', key, flags=re.IGNORECASE)
            if mm and mm.group(1):
                name = mm.group(1).upper()
                gg = re.match(r'^([A-Z])CONVERT[IO]?$', name, flags=re.IGNORECASE)
                if name in self.allowed_names or gg:
                    tempdict = dict()
                    tempdict["P"] = prefix.upper()
                    tempdict["V"] = data_dict[prefix+name+"_V"]
                    tempdict["J"] = data_dict[prefix+name+"_"+prefix+base+"_J"] if prefix+name+"_"+prefix+base+"_J" in data_dict else np.ones(tempdict["V"].shape, dtype=np.float64)
                    tempdict["E"] = data_dict[prefix+name+"_E"] if prefix+name+"_E" in data_dict else np.zeros(tempdict["V"].shape, dtype=np.float64)
                    cobj = EX2GKCoordinate.importFromDict(tempdict, True)
                    cobj.name = name
                    self.addCoordinateObject(cobj)
                    if "V" in tempdict:
                        dellist.append(prefix+name+"_V")
                    if "J" in tempdict and prefix+name+"_"+prefix+base+"_J" in data_dict:
                        dellist.append(prefix+name+"_"+prefix+base+"_J")
                    if "E" in tempdict and prefix+name+"_E" in data_dict:
                        dellist.append(prefix+name+"_E")
                if gg and gg.group(1).upper() not in self.conversion_list:
                    self.conversion_list.append(gg.group(1).upper())
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            temp = self[key].exportToDict()
            if isinstance(temp, dict):
                for nkey, value in temp.items():
                    if nkey != "P":
                        okey = self.prefix+self.base+"_"+nkey if nkey.endswith("J") else nkey
                        output[self.prefix+key+"_"+okey] = value
        if output:
            output[self.prefix+"BASE"] = self.base
        return output

    @classmethod
    def importFromPandas(cls, panda, prefix=""):
        self = cls()
        var_names = panda.columns.get_level_values(0).unique()
        for var_name in var_names:
            data_panda = panda[var_name]
            obj = EX2GKCoordinate.importFromPandas(data_panda, name=var_name, prefix=prefix)
            self.addCoordinateObject(obj)
        return self

    def exportToPandas(self):
        dfs = []
        for key in self:
            df = self[key].exportToPandas()
            dfs.append(df)
        return pd.concat(dfs, axis=1)

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            tempdict[key] = self[key].stripFromClass(with_descriptions=False)
        if tempdict:
            tempdict["BASE"] = self.base if self.base is not None else "UNDEFINED"
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
            tempdict["DESC"]["BASE"] = "Reference Coordinate"
        return tempdict


class EX2GKCoordinateContainer(dict):

    # Class variables
    allowed_names = {"PSIPOLN": 'Normalized poloidal flux',
                     "PSIPOL": 'Absolute poloidal flux',
                     "RHOPOLN": 'Normalized poloidal rho (sqrt norm poloidal flux)',
                     "PSITORN": 'Normalized toroidal flux',
                     "PSITOR": 'Absolute toroidal flux',
                     "RHOTORN": 'Normalized toroidal rho (sqrt norm toroidal flux)',
                     "RMAJORO": 'Absolute outer major radius at height of magnetic axis',
                     "RMAJORI": 'Absolute inner major radius at height of magnetic axis',
                     "RMINORO": 'Absolute outer minor radius at height of magnetic axis',
                     "RMINORI": 'Absolute inner minor radius at height of magnetic axis',
                     "RMAJORA": 'Averaged major radius at height of magnetic axis',
                     "RMINORA": 'Averaged minor radius at height of magnetic axis',
                     "FSVOL": 'Flux surface volume',
                     "FSAREA": 'Flux surface surface area',
                     #"BTOR": 'Absolute toroidal magnetic field',
                     #"BPOL": 'Absolute poloidal magnetic field',
                     "CONVERT": 'Conversion vector into another coordinate system base, if present'}

    def __init__(self, *args, **kwargs):
        super(EX2GKCoordinateContainer, self).__init__()
        self.update(*args, **kwargs)

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], EX2GKCoordinateSystem):
                desc = self[key].describe()
                descdict.update(desc)
        return descdict

    def coord_keys(self):
        outlist = []
        for key in self:
            for ckey in self[key]:
                outlist.append(key+ckey)
        return outlist

    def convert(self, data, cs_in, cs_out, prefix_in=None, prefix_out=None, fdebug=False):
        dvec = None
        ics = None         # Input coordinate system label
        cics = None        # Input conversion coordinate system label, if needed
        cics = None        # Output conversion coordinate system label, if needed
        ocs = None         # Output coordinate system label
        icp = ""           # Input coordinate system prefix
        ocp = ""           # Output coordinate system prefix
        ici = None         # Index of input coordinate system
        oci = None         # Index of output coordinate system
        ifull = False
        ofull = False
        if isinstance(data, number_types):
            dvec = np.array([data]).flatten()
        elif isinstance(data, array_types):
            dvec = np.array(data).flatten()
        else:
            warnings.warn("%s data argument must be array-like and numerical, conversion failed" % (type(self).convert.__name__))
        if isinstance(prefix_in, str) and prefix_in in self:
            icp = prefix_in
        if isinstance(prefix_out, str) and prefix_out in self:
            ocp = prefix_out
        if isinstance(cs_in, str) and isinstance(cs_out, str):
            if cs_in in self[icp]:
                ics = cs_in
            elif cs_in+"I" in self[icp] and cs_in+"O" in self[icp]:
                ics = cs_in
                ifull = True
            if cs_out in self[ocp]:
                ocs = cs_out
            elif cs_in+"I" in self[ocp] and cs_in+"O" in self[ocp]:
                ocs = cs_out
                ofull = True

        if fdebug:
            print(ics, icp, ocs, ocp)

        vvec = None
        jvec = None
        evec = None
        if dvec is not None and ics is not None and ocs is not None:

            # Filter out non-finite numbers in input vector, replace with identical values at end
            dfilt = np.isfinite(dvec)

            # Simplify process if input and output coordinate systems are identical
            if re.match(ics, ocs, flags=re.IGNORECASE) and icp == ocp:
                vvec = dvec.copy()
                jvec = np.ones(vvec.shape, dtype=np.float64)
                evec = np.zeros(vvec.shape, dtype=np.float64)

            elif np.any(dfilt):

                # Containers for coordinate system vectors
                vivec = None
                vcivec = None
                vcovec = None
                vovec = None

                # Containers for coordinate system Jacobian vectors
                jivec = None
                jcivec = None
                jcovec = None
                jovec = None

                # Containers for coordinate system errors
                ecvec = None
                eovec = None

                # Special treatment when input or output coordinate system is a full length system
                if ifull or ofull:
                    hfssigni = -1.0
                    hfssigno = -1.0

                    # Determine sign reversals required in treatment of full length systems
                    if re.match(r'^RMAJOR$', ics, flags=re.IGNORECASE):
                        hfssigni = 1.0
                    if re.match(r'^RMAJOR$', ocs, flags=re.IGNORECASE):
                        hfssigno = 1.0

                    # Separate considerations needed if input coordinate is a full length system
                    if ifull:
                        vivec = np.hstack((hfssigni * self[icp][ics+"I"]["V"][:0:-1], self[icp][ics+"O"]["V"]))
                        jivec = np.hstack((self[icp][ics+"I"]["J"][:0:-1], self[icp][ics+"O"]["J"]))
                    else:
                        vivec = np.hstack((hfssigni * self[icp][ics]["V"][:0:-1], self[icp][ics]["V"]))
                        jivec = np.hstack((self[icp][ics]["J"][:0:-1], self[icp][ics]["J"]))

                    # Separate considerations needed if output coordinate is a full length system
                    if ofull:
                        vovec = np.hstack((hfssigno * self._internal_list[oci][ocs+"I"]["V"][:0:-1], self._internal_list[oci][ocs+"O"]["V"]))
                        jovec = np.hstack((self[ocp][ocs+"I"]["J"][:0:-1], self[ocp][ocs+"O"]["J"]))
                        eovec = np.hstack((self[ocp][ocs+"I"]["E"][:0:-1], self[ocp][ocs+"O"]["E"]))
                    else:
                        vovec = np.hstack((hfssigno * self[ocp][ocs]["V"][:0:-1], self[ocp][ocs]["V"]))
                        jovec = np.hstack((self[ocp][ocs]["J"][:0:-1], self[ocp][ocs]["J"]))
                        eovec = np.hstack((self[ocp][ocs]["E"][:0:-1], self[ocp][ocs]["E"]))
                else:
                    vivec = self[icp][ics]["V"]
                    vovec = self[ocp][ocs]["V"]
                    jivec = self[icp][ics]["J"]
                    jovec = self[ocp][ocs]["J"]
                    eovec = self[ocp][ocs]["E"]

                # Additional vectors needed for conversion between corrected and uncorrected systems
                if icp != ocp:
                    # Check existence of conversion coordinate system
                    cics = None
                    cocs = None
                    cifull = False
                    cofull = False
                    if ocp+"CONVERT" in self[icp]:
                        cifull = False
                        cics = ocp+"CONVERT"
                        cocs = self[icp].base
                    elif icp+"CONVERT" in self[ocp]:
                        cofull = False
                        cocs = icp+"CONVERT"
                        cics = self[ocp].base

                    if ocp+"CONVERTI" in self[icp] and ocp+"CONVERTO" in self[icp]:
                        # Check for specification of full-length coordinate systems
                        cifull = True
                        cics = ocp+"CONVERT"
                        cocs = self[icp].base
                    elif icp+"CONVERTI" in self[ocp] and icp+"CONVERTO" in self[ocp]:
                        # Check for specification of full-length coordinate systems
                        cofull = True
                        cocs = icp+"CONVERT"
                        cics = self[ocp].base

                    if cics is not None and cocs is not None:
                        # Special treatment when input or output coordinate system is a full length system
                        if ifull or cifull or cofull or ofull:
                            hfssignc = -1.0

                            # Determine sign reversals required in treatment of full length systems
                            if re.match(r'^RMAJOR$', cics, flags=re.IGNORECASE) or re.match(r'^RMAJOR$', cocs, flags=re.IGNORECASE):
                                hfssignc = 1.0

                            # Separate considerations needed if conversion coordinate input is a full length system
                            if cifull:
                                vcivec = np.hstack((hfssignc * self[icp][cics+"I"]["V"][:0:-1], self[icp][cics+"O"]["V"]))
                                jcivec = np.hstack((self[icp][cics+"I"]["J"][:0:-1], self[icp][cics+"O"]["J"]))
                                if re.match(r'^[A-Z]CONVERT$', cics, flags=re.IGNORECASE):
                                    ecvec = np.hstack((self[icp][cics+"I"]["E"][:0:-1], self[icp][cics+"O"]["E"]))
                            else:
                                vcivec = np.hstack((hfssignc * self[icp][cics]["V"][:0:-1], self[icp][cics]["V"]))
                                jcivec = np.hstack((self[icp][cics]["J"][:0:-1], self[icp][cics]["J"]))
                                if re.match(r'^[A-Z]CONVERT$', cics, flags=re.IGNORECASE):
                                    ecvec = np.hstack((self[icp][cics]["E"][:0:-1], self[icp][cics]["E"]))

                            # Separate considerations needed if conversion coordinate output is a full length system
                            if cofull:
                                vcovec = np.hstack((hfssignc * self[ocp][cocs+"I"]["V"][:0:-1], self[ocp][cocs+"O"]["V"]))
                                jcovec = np.hstack((self[ocp][cocs+"I"]["J"][:0:-1], self[ocp][cocs+"O"]["J"]))
                                if re.match(r'^[A-Z]CONVERT$', cocs, flags=re.IGNORECASE):
                                    ecvec = np.hstack((self[ocp][cocs+"I"]["E"][:0:-1], self[ocp][cocs+"O"]["E"]))
                            else:
                                vcovec = np.hstack((hfssignc * self[ocp][cocs]["V"][:0:-1], self[ocp][cocs]["V"]))
                                jcovec = np.hstack((self[ocp][cocs]["J"][:0:-1], self[ocp][cocs]["J"]))
                                if re.match(r'^[A-Z]CONVERT$', cocs, flags=re.IGNORECASE):
                                    ecvec = np.hstack((self[ocp][cocs]["E"][:0:-1], self[ocp][cocs]["E"]))
                        else:
                            vcivec = self[icp][cics]["V"]
                            vcovec = self[ocp][cocs]["V"]
                            jcivec = self[icp][cics]["J"]
                            jcovec = self[ocp][cocs]["J"]
                            if re.match(r'^[A-Z]CONVERT$', cics, flags=re.IGNORECASE):
                                ecvec = self[icp][cics]["E"]
                            else:
                                ecvec = self[ocp][cocs]["E"]

                if fdebug:
                    print(icp+ics+" -> "+ocp+ocs)
                    print(vivec)
                    print(vovec)
                    if vcivec is not None and vcovec is not None and jcivec is not None and jcovec is not None and ecvec is not None:
                        print(icp+cics+" -> "+ocp+cocs)
                        print(vcivec)
                        print(vcovec)

                # Filter out non-finite values in interpolation vectors, only happens in fringe cases but messes with routines if present
                ifilt = np.isfinite(vivec) if vivec is not None else None
                ifilt = np.all([ifilt, np.isfinite(jivec)], axis=0) if ifilt is not None and jivec is not None else ifilt
                ofilt = np.isfinite(vovec) if vovec is not None else None
                ofilt = np.all([ofilt, np.isfinite(jovec)], axis=0) if ofilt is not None and jovec is not None else ofilt
                ofilt = np.all([ofilt, np.isfinite(eovec)], axis=0) if ofilt is not None and eovec is not None else ofilt

                if ifilt is not None and ofilt is not None and np.any(ifilt) and np.any(ofilt):

                    # Double-pass needed for conversion between corrected and uncorrected systems, otherwise just single-pass
                    if vcivec is not None and vcovec is not None:

                        # Filter out non-finite values in auxiliary interpolation vectors, only happens in fringe cases but messes with routines if present
                        ifilt = np.all([ifilt, np.isfinite(vcivec), np.isfinite(jcivec), np.isfinite(ecvec)], axis=0) if jcivec is not None and ecvec is not None else None
                        ofilt = np.all([ofilt, np.isfinite(vcovec), np.isfinite(jcovec)], axis=0) if jcovec is not None else None

                        # Note that this routine will returns None values if double-pass is specified but ANY of the required vectors are missing
                        if ifilt is not None and ofilt is not None and np.any(ifilt) and np.any(ofilt):
                            (vvec1, jvec1, evec1) = ptools.convert_base_coords(dvec[dfilt], vivec[ifilt], vcivec[ifilt], jivec[ifilt], jcivec[ifilt], ecvec[ifilt])
                            (vvec2, jvec2, evec2) = ptools.convert_base_coords(vvec1, vcovec[ofilt], vovec[ofilt], jcovec[ofilt], jovec[ofilt], eovec[ofilt])
                            if vvec2 is not None and jvec1 is not None and jvec2 is not None and evec1 is not None and evec2 is not None:
                                vvec = np.zeros(dvec.shape)
                                jvec = np.zeros(dvec.shape)
                                evec = np.zeros(dvec.shape)
                                vvec[dfilt] = vvec2
                                jvec[dfilt] = jvec1 * jvec2
                                evec[dfilt] = np.sqrt(np.power(evec1, 2.0) + np.power(evec2, 2.0))
                                if not np.all(dfilt):
                                    vvec[np.invert(dfilt)] = dvec[np.invert(dfilt)]
                                    jvec[np.invert(dfilt)] = np.NaN
                                    evec[np.invert(dfilt)] = np.NaN

                    else:

                        # Filter out non-finite values in interpolation vectors, can happen in fringe cases but messes with routines if present
                        cfilt = np.all([ifilt, ofilt], axis=0)

                        # Single-pass operation will return ones for Jacobians and zeros for errors if their respective required vectors are missing
                        if np.any(cfilt):
                            (vvec1, jvec1, evec1) = ptools.convert_base_coords(dvec[dfilt], vivec[cfilt], vovec[cfilt], jivec[cfilt], jovec[cfilt], eovec[cfilt])
                            if vvec1 is not None and jvec1 is not None and evec1 is not None:
                                vvec = np.zeros(dvec.shape)
                                jvec = np.zeros(dvec.shape)
                                evec = np.zeros(dvec.shape)
                                vvec[dfilt] = vvec1
                                jvec[dfilt] = jvec1
                                evec[dfilt] = evec1
                                if not np.all(dfilt):
                                    vvec[np.invert(dfilt)] = dvec[np.invert(dfilt)]
                                    jvec[np.invert(dfilt)] = np.NaN
                                    evec[np.invert(dfilt)] = np.NaN

                if vvec is None or jvec is None or evec is None:
                    vvec = None
                    jvec = None
                    evec = None

        return (vvec, jvec, evec)

    def getPrefixes(self):
        return list(self.keys())

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if not isinstance(value, EX2GKCoordinateSystem):
            raise TypeError("%s entries must be a %s objects" % (type(self).__name__, EX2GKCoordinateSystem.__name__))
        super(EX2GKCoordinateContainer, self).__setitem__(key.upper(), copy.deepcopy(value))

    def isReady(self, verbose=0):
        gflag = False
        if "" in self and self[""].isReady(verbose=verbose):
            gflag = True
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def update(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def addCoordinateSystemObject(self, cs_object):
        if not isinstance(cs_object, EX2GKCoordinateSystem):
            raise TypeError("%s object argument must be a %s object" % (type(self).addCoordinateSystemObject.__name__, EX2GKCoordinateSystem.__name__))
        if cs_object.isReady():
            if cs_object.prefix.upper() in self:
                warnings.warn("%s prefix already exists within %s object, overwriting currently stored data" % (type(cs_object).__name__, type(self).__name__))
            self[cs_object.prefix] = cs_object
        else:
            warnings.warn

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKCoordinateContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        prefix_list = {}
        for key in data_dict:
            mm = re.match(r'^([A-Z]?)BASE$', key, flags=re.IGNORECASE)
            if mm:
                prefix_list[mm.group(1).upper()] = data_dict[key].upper()
                dellist.append(key)
        for prefix in prefix_list:
            if prefix:
                data_dict = ptools.old_coords_to_new(data_dict)
            tempdict = {prefix+"BASE": prefix_list[prefix]}
            tempkeys = []
            for key, value in data_dict.items():
                mm = re.match(r'^'+prefix+'([^_]+).*_[VJE]$', key, flags=re.IGNORECASE)
                if mm and mm.group(1) and mm.group(1).upper() in self.allowed_names:
                    tempdict[key.upper()] = value
                    tempkeys.append(key)
            csobj = EX2GKCoordinateSystem.importFromDict(tempdict, True)
            self.addCoordinateSystemObject(csobj)
            for nkey in tempkeys:
                if nkey.upper() in tempdict:
                    dellist.append(nkey)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            temp = self[key].exportToDict()
            if isinstance(temp, dict):
                for nkey, value in temp.items():
                    output[nkey] = value
        return output

    def exportToPandas(self):
        dfs = {}
        for prefix in self.getPrefixes():
            df_prefix = []
            for name, val in self[prefix].items():
                df_prefix.append(val.exportToPandas())
            df = pd.concat(df_prefix, axis=1)
            dfs[prefix] = df
        return dfs

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            nkey = "STANDARD" if key == "" else key
            tempdict[nkey] = self[key].stripFromClass(with_descriptions=with_descriptions)
        if tempdict and with_descriptions:
            descdict = {}
            for key in tempdict:
                descdict[key] = "Default coordinate system" if key == "STANDARD" else "Custom coordinate system"
            tempdict["DESC"] = descdict
        return tempdict


class EX2GKZeroDimensionalData(dict):

    # Class variables
    required_names = {
        "": 'Raw data value',
        "EB": 'Raw data error',
        "PROV": 'Data provenance'
    }

    def __init__(self, *args, **kwargs):
        super(EX2GKZeroDimensionalData, self).__init__()
        self.name = None
        for key in self.required_names:
            self.setdefault(key)
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        desc = "Undefined"
        if isinstance(self.name, str):
            (tag, label, unit, flabel, funit) = ptools.define_quantity(self.name)
            desc = label if label is not None else ""
            if desc:
                desc += " "
            desc += "[" + unit + "]" if unit else "[-]"
        return desc

    def setData(self, quantity_name, value, error=None, force_integer=False):
        if not isinstance(quantity_name, str):
            raise TypeError("%s name argument must be a string" % (type(self).setData.__name__))
        self.reset()
        val = value
        while isinstance(val, array_types):
            val = val[0]
        err = error if error is not None else 0.0
        while isinstance(err, array_types):
            err = err[0]
        self.name = quantity_name.upper()
        if val is not None:
            self[""] = float(val) if not force_integer else int(val)
            self["EB"] = float(err) if not force_integer else int(err)

    def setProvenanceData(self, provenance_string):
        if provenance_string is not None:
            self["PROV"] = provenance_string

    def reset(self):
        self.name = None
        self[""] = None
        self["EB"] = None
        self["PROV"] = None

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.required_names:
            if value is not None:
                if key.upper() == "PROV":
                    if not isinstance(value, str):
                        raise TypeError("%s provenance data entry must be a string" % (type(self).__name__))
                elif not isinstance(value, number_types):
                    raise TypeError("%s values must be scalar numbers" % (type(self).__name__))
            super(EX2GKZeroDimensionalData, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            warnings.warn("%s is not a valid data label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKZeroDimensionalData, self).__repr__())

    def __copy__(self):
        return type(self)(self, name=self.name)

    def copy(self):
        return type(self)(self, name=self.name)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.name is None:
            match &= (self.name is other.name)
        else:
            match &= (self.name == other.name)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        # Modified relative tolerance criteria needed due to difference in numpy / scipy
                        fmatch = np.isclose(self[key], other[key], equal_nan=True)
                        if not np.all(fmatch):
                            min_ratio = np.nanmin(np.abs(self[key][~fmatch])) / np.nanmax(np.abs(self[key]))
                            new_rtol = 1.0e-5 / float(np.power(10.0, np.ceil(np.log10(min_ratio))))
                            smatch = np.isclose(self[key], other[key], rtol=new_rtol, equal_nan=True)
                            fmatch |= smatch
                        match &= np.all(fmatch)
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            if key.upper() in self.required_names:
                self[key.upper()] = value
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = copy.deepcopy(super(EX2GKZeroDimensionalData, self).copy())
        dellist = []
        for key, value in output.items():
            if value is None:
                dellist.append(key)
        for item in dellist:
            del output[item]
        if not output:
            output = None
        return output

    @classmethod
    def importFromPandas(cls, panda, name=""):
        return cls(panda, name=name)

    def exportToPandas(self):
        df = pd.DataFrame(self, index=[self.name])
        df.fillna(np.NaN, inplace=True)
        return df

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                nkey = "VAL" if key == "" else key
                tempdict[nkey] = copy.deepcopy(self[key])
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKOneDimensionalData(dict):

    # Class variables
    required_names = {
        "": 'Data y-values',
        "EB": 'Data y-errors',
        "GRAD": 'Data dy-values',
        "GRADEB": 'Data dy-errors',
        "PROV": 'Data provenance'
    }
    raw_data_requirements = {
        "X": 'Data x-values',
        "XEB": 'Data x-errors',
        "XGRAD": 'Data derivative x-values',
        "XEBGRAD": 'Data derivative x-errors',
        "SRC": 'Raw data source list',
        "MAP": 'Raw data source mapping vector'
    }
    default_cs = "PD_X"

    def __init__(self, *args, **kwargs):
        super(EX2GKOneDimensionalData, self).__init__()
        self.name = None
        self.npoints = None
        self.ndpoints = None
        self.coordinate = None
        self.coord_prefix = ""
        self.is_raw = False
        for key in self.required_names:
            self.setdefault(key)
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        desc = "Undefined"
        if isinstance(self.name, str):
            (tag, label, unit, flabel, funit) = ptools.define_quantity(self.name)
            desc = label if label is not None else ""
            if desc:
                desc += " "
            desc += "[" + unit + "]" if unit else "[-]"
        return desc

    def isRaw(self):
        return self.is_raw

    def setData(self, quantity_name, y_data, y_error, x_data=None, x_error=None, x_coord=None, x_coord_prefix=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s name argument must be a string" % (type(self).setData.__name__))
        self.reset()
        self.name = quantity_name.upper()
        self[""] = y_data
        self["EB"] = y_error
        if x_data is not None:
            if self.coordinate is None:
                if not isinstance(x_coord, str):
                    raise TypeError("%s x coordinate argument must be a string if x data is provided" % (type(self).setData.__name__))
                self.coordinate = x_coord.upper()
                if isinstance(x_coord_prefix, str):
                    self.coord_prefix = x_coord_prefix.upper()
            else:
                if isinstance(x_coord, str) and x_coord.upper() != self.coordinate:
                    warnings.warn("%s x coordinate argument does not match existing value, use reset() to start from scratch" % (type(self).setData.__name__))
                if isinstance(x_coord_prefix, str) and x_coord_prefix.upper() != self.coord_prefix:
                    warnings.warn("%s x coordinate prefix argument does not match existing value, use reset() to start from scratch" % (type(self).setData.__name__))
            self["X"] = x_data
            self["XEB"] = x_error if x_error is not None else np.zeros(self["X"].shape)
        elif x_coord is not None:
            if "X" in self or "XGRAD" in self:
                warnings.warn("%s coordinate data already exists! ignoring %s x coordinate argument, use reset() to start from scratch" % (type(self).__name__, type(self).setData.__name__))
            else:
                if not isinstance(x_coord, str):
                    raise TypeError("%s x coordinate argument must be a string" % (type(self).setData.__name__))
                self.coordinate = x_coord.upper()
                if isinstance(x_coord_prefix, str):
                    self.coord_prefix = x_coord_prefix.upper()

    def setDerivativeData(self, dy_data=None, dy_error=None, x_data=None, x_error=None, x_coord=None, x_coord_prefix=None):
        self["GRAD"] = dy_data
        self["GRADEB"] = dy_error
        if x_data is not None:
            if self.coordinate is None:
                if not isinstance(x_coord, str):
                    raise TypeError("%s x coordinate argument must be a string if x data is provided" % (type(self).setDerivativeData.__name__))
                self.coordinate = x_coord.upper()
                if isinstance(x_coord_prefix, str):
                    self.coord_prefix = x_coord_prefix.upper()
            else:
                if isinstance(x_coord, str) and x_coord.upper() != self.coordinate:
                    warnings.warn("%s x coordinate argument does not match existing value, use reset() to start from scratch" % (type(self).setDerivativeData.__name__))
                if isinstance(x_coord_prefix, str) and x_coord_prefix.upper() != self.coord_prefix:
                    warnings.warn("%s x coordinate prefix argument does not match existing value, use reset() to start from scratch" % (type(self).setDerivativeData.__name__))
            self["XGRAD"] = x_data
            self["XEBGRAD"] = x_error if x_error is not None else np.zeros(self["XGRAD"].shape)
        elif x_coord is not None:
            if "X" in self or "XGRAD" in self:
                warnings.warn("%s coordinate data already exists! ignoring %s x coordinate argument, use reset() to start from scratch" % (type(self).__name__, type(self).setData.__name__))
            else:
                if not isinstance(x_coord, str):
                    raise TypeError("%s x coordinate argument must be a string" % (type(self).setData.__name__))
                self.coordinate = x_coord.upper()
                if isinstance(x_coord_prefix, str):
                    self.coord_prefix = x_coord_prefix.upper()

    def setMappingData(self, source_list=None, source_map=None):
        if source_list is not None and source_map is not None:
            self["SRC"] = source_list
            self["MAP"] = source_map

#    def setLimits(self, min_value=None, max_value=None):
#        if min_value is None:
#            self.minimum = None
#        elif isinstance(min_value, number_types):
#            self.minimum = float(min_value)
#        elif isinstance(min_value, array_types) and len(min_value) == 1:
#            self.minimum = float(min_value[0])
#        else:
#            warnings.warn("%s minimum value argument must be a number or None, minimum not set" % (type(self).setLimits.__name__))
#        if max_value is None:
#            self.maximum = None
#        elif isinstance(max_value, number_types):
#            self.maximum = float(max_value)
#        elif isinstance(max_value, array_types) and len(max_value) == 1:
#            self.maximum = float(max_value[0])
#        else:
#            warnings.warn("%s maximum value argument must be a number or None, maximum not set" % (type(self).setLimits.__name__))

    def setProvenanceData(self, provenance_string):
        if provenance_string is not None:
            self["PROV"] = provenance_string

    def unsetRawFlag(self):
        if "SRC" in self:
            del self["SRC"]
        if "MAP" in self:
            del self["MAP"]
        self.is_raw = False

    def reset(self):
        self.name = None
        self.npoints = None
        self.ndpoints = None
        self.coordinate = None
        self.coord_prefix = ""
        self.unsetRawFlag()
        self[""] = None
        self["EB"] = None
        self["GRAD"] = None
        self["GRADEB"] = None
        self["PROV"] = None
        if "X" in self:
            del self["X"]
        if "XEB" in self:
            del self["XEB"]
        if "XGRAD" in self:
            del self["XGRAD"]
        if "XEBGRAD" in self:
            del self["XEBGRAD"]

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.required_names:
            if key.upper() in ["GRAD", "GRADEB"]:
                val = np.array([], dtype=np.float64)
                if isinstance(value, array_types):
                    val = np.array(value, dtype=np.float64).flatten()
                    if self.ndpoints is not None and len(val) != self.ndpoints:
                        raise ValueError("%s derivative data entries must be of equal length" % (type(self).__name__))
                    self.ndpoints = len(val)
                elif value is None:
                    val = np.array([0.0] * self.ndpoints, dtype=np.float64).flatten() if self.ndpoints is not None else None
                else:
                    raise TypeError("%s derivative data entries must be array-like" % (type(self).__name__))
                super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), val)
            elif key.upper() == "PROV":
                val = None
                if isinstance(value, str) or value is None:
                    val = value
                else:
                    raise TypeError("%s provenance data entry must be a string" % (type(self).__name__))
                super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), val)
            else:
                val = np.array([], dtype=np.float64)
                if isinstance(value, array_types):
                    val = np.array(value, dtype=np.float64).flatten()
                    if self.npoints is not None and len(val) != self.npoints:
                        raise ValueError("%s data entries must be of equal length" % (type(self).__name__))
                    self.npoints = len(val)
                elif value is None:
                    val = np.array([0.0] * self.npoints, dtype=np.float64).flatten() if self.npoints is not None else None
                else:
                    raise TypeError("%s data entries must be array-like" % (type(self).__name__))
                super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), val)
        elif key in self.raw_data_requirements:
            if value is not None:
                if key.upper() in ["X", "XEB"]:
                    val = None
                    if isinstance(value, array_types):
                        val = np.array(value, dtype=np.float64).flatten()
                        if self.npoints is not None and len(val) != self.npoints:
                            raise ValueError("%s data coordinate entries must be of equal length as data" % (type(self).__name__))
                        self.npoints = len(val)
                    else:
                        raise TypeError("%s data coordinate entries must be array-like" % (type(self).__name__))
                    super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), val)
                if key.upper() in ["XGRAD", "XEBGRAD"]:
                    val = None
                    if isinstance(value, array_types):
                        val = np.array(value, dtype=np.float64).flatten()
                        if self.ndpoints is not None and len(val) != self.ndpoints:
                            raise ValueError("%s derivative data coordinate entries must be of equal length as derivative data" % (type(self).__name__))
                        self.ndpoints = len(val)
                    else:
                        raise TypeError("%s derivative data coordinate entries must be array-like" % (type(self).__name__))
                    super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), val)
                if key.upper() == "SRC":
                    if not isinstance(value, list):
                        raise TypeError("%s raw data source list entry must be a list" % (type(self).__name__))
                    super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), copy.deepcopy(value))
                    self.is_raw = True
                if key.upper() == "MAP":
                    val = None
                    if isinstance(value, array_types):
                        val = np.array(value, dtype=np.int64).flatten()
                        if self.npoints is not None and len(val) != self.npoints:
                            raise ValueError("%s raw data source map must be of equal length as data" % (type(self).__name__))
                    else:
                        raise TypeError("%s raw data source map entry must be array-like" % (type(self).__name__))
                    super(EX2GKOneDimensionalData, self).__setitem__(key.upper(), val)
                    self.is_raw = True
        else:
            warnings.warn("%s is not a valid data label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        if "coordinate" not in self.__dict__:
            self.coordinate = None
        if "coord_prefix" not in self.__dict__:
            self.coord_prefix = ""
        if "npoints" not in self.__dict__:
            self.npoints = None
        if "ndpoints" not in self.__dict__:
            self.ndpoints = None
        if "is_raw" not in self.__dict__:
            self.is_raw = False
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        if "coordinate" in kwargs:
            if isinstance(kwargs["coordinate"], str):
                self.coordinate = kwargs["coordinate"]
            else:
                warnings.warn("%s coordinate keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["coordinate"]
        if "coord_prefix" in kwargs:
            if isinstance(kwargs["coord_prefix"], str):
                self.coordinate = kwargs["coord_prefix"]
            else:
                warnings.warn("%s coordinate prefix keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["coord_prefix"]
        if "npoints" in kwargs:
            if isinstance(kwargs["npoints"], number_types) and int(kwargs["npoints"]) > 0:
                self.npoints = int(kwargs["npoints"])
            else:
                warnings.warn("%s npoints keyword argument must be an integer greater than zero, input not accepted" % (type(self).__name__))
            del kwargs["npoints"]
        if "ndpoints" in kwargs:
            if isinstance(kwargs["ndpoints"], number_types) and int(kwargs["ndpoints"]) > 0:
                self.ndpoints = int(kwargs["ndpoints"])
            else:
                warnings.warn("%s ndpoints keyword argument must be an integer greater than zero, input not accepted" % (type(self).__name__))
            del kwargs["ndpoints"]
        if "is_raw" in kwargs:
            self.is_raw = True if kwargs["is_raw"] else False
            del kwargs["is_raw"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKOneDimensionalData, self).__repr__())

    def __copy__(self):
        return type(self)(self, name=self.name, npoints=self.npoints, ndpoints=self.ndpoints, is_raw=self.is_raw)

    def copy(self):
        return type(self)(self, name=self.name, npoints=self.npoints, ndpoints=self.ndpoints, is_raw=self.is_raw)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.name is None:
            match &= (self.name is other.name)
        else:
            match &= (self.name == other.name)
        if self.npoints is None:
            match &= (self.npoints is other.npoints)
        else:
            match &= (self.npoints == other.npoints)
        if self.ndpoints is None:
            match &= (self.ndpoints is other.ndpoints)
        else:
            match &= (self.ndpoints == other.ndpoints)
        if self.coordinate is None:
            match &= (self.coordinate is other.coordinate)
        else:
            match &= (self.coordinate == other.coordinate)
        if self.coord_prefix is None:
            match &= (self.coord_prefix is other.coord_prefix)
        else:
            match &= (self.coord_prefix == other.coord_prefix)
        match &= (self.is_raw == other.is_raw)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        # Modified relative tolerance criteria needed due to difference in numpy / scipy
                        fmatch = np.isclose(self[key], other[key], equal_nan=True)
                        if not np.all(fmatch):
                            min_ratio = np.nanmin(np.abs(self[key][~fmatch])) / np.nanmax(np.abs(self[key]))
                            new_rtol = 1.0e-5 / float(np.power(10.0, np.ceil(np.log10(min_ratio))))
                            smatch = np.isclose(self[key], other[key], rtol=new_rtol, equal_nan=True)
                            fmatch |= smatch
                        match &= np.all(fmatch)
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            if key == "XCS":
                self.coordinate = value.upper() if value.upper() != self.default_cs else None
                dellist.append(key)
            elif key == "XCP":
                self.coord_prefix = value.upper()
                dellist.append(key)
            elif key.upper() in self.required_names or key.upper() in self.raw_data_requirements:
                self[key.upper()] = value
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = copy.deepcopy(super(EX2GKOneDimensionalData, self).copy())
        dellist = []
        for key, value in output.items():
            if value is None:
                dellist.append(key)
        for item in dellist:
            del output[item]
        default_cs = "UNDEFINED" if self.is_raw else self.default_cs
        output["XCS"] = self.coordinate.upper() if self.coordinate is not None else default_cs
        output["XCP"] = self.coord_prefix.upper()
        if not output:
            output = None
        return output

    @classmethod
    def importFromPandas(cls, panda, name=None, coordinate=None):
        if isinstance(panda, dict):
            dict_ = panda
        else:
            dict_ = {key: val.values for key, val in panda.items()}
        self = cls(
            dict_,
            coordinate=coordinate,
            name=name
        )
        return self

    @staticmethod
    def mergeMultiIndex(df):
        if len(df) > 1:
            if 'EB' in df:
                df['EB'] = np.sqrt(df['EB'].pow(2).mean() + df[''].std() ** 2)
            if '' in df:
                df[''] = df[''].mean()
            if 'XEB' in df:
                df['XEB'] = df['XEB'].mean()
            df = df.iloc[[0], :]
        return df

    def exportToPandas(self):
        if self.isRaw():
            data = copy.deepcopy(self)
            srcmap = np.array(data.pop("SRC"))[data.pop("MAP")]
            grad = {}
            nograd = {}
            for key in list(data.keys()):
                if 'GRAD' in key:
                    grad[key] = data.pop(key)
                else:
                    nograd[key] = data.pop(key)
            if len(data) != 0:
                raise Exception("Unable to parse all values. {!s} left in dict".format(data))
            df = pd.DataFrame(nograd)
            df["SRCMAP"] = srcmap
            df.set_index(["X", "SRCMAP"], inplace=True)
            if not df.index.is_unique:
                df = df.groupby(df.index, as_index=False).apply(self.mergeMultiIndex).reset_index(level=0, drop=True)
            df.index = df.index.rename(self.coord_prefix + self.coordinate, level='X')
        else:
            df = pd.DataFrame(self)
        df = pd.concat([df], keys=[self.name], names=['var'], axis=1)
        df.fillna(np.NaN, inplace=True)
        return df

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                nkey = "VAL" if key == "" else key
                tempdict[nkey] = copy.deepcopy(self[key])
        if tempdict:
            default_cs = "UNDEFINED" if self.is_raw else self.default_cs
            tempdict["XCS"] = self.coordinate if self.coordinate is not None else default_cs
            tempdict["XCP"] = "STANDARD" if self.coord_prefix == "" else self.coord_prefix
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKTwoDimensionalData(dict):

    # Class variables
    required_names = {
        "": 'Data z-values',
        "EB": 'Data z-errors',
        "PROV": 'Data provenance'
    }
    optional_requirements = {
        "X": 'Data x-values',
        "XEB": 'Data x-errors',
        "Y": 'Data y-values',
        "YEB": 'Data y-errors'
    }

    def __init__(self, *args, **kwargs):
        super(EX2GKTwoDimensionalData, self).__init__()
        self.name = None
        self.nxpoints = None
        self.nypoints = None
        for key in self.required_names:
            self.setdefault(key)
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        desc = "Undefined"
        if isinstance(self.name, str):
            (tag, label, unit, flabel, funit) = ptools.define_quantity(self.name)
            desc = label if label is not None else ""
            if desc:
                desc += " "
            desc += "[" + unit + "]" if unit else "[-]"
        return desc

    def setData(self, quantity_name, z_data, z_error, x_data=None, x_error=None, y_data=None, y_error=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s name argument must be a string" % (type(self).setData.__name__))
        self.reset()
        self.name = quantity_name.upper()
        self[""] = z_data
        self["EB"] = z_error
        if x_data is not None and y_data is not None:
            self["X"] = x_data
            self["XEB"] = x_error if x_error is not None else np.zeros(self["X"].shape)
            self["Y"] = y_data
            self["YEB"] = y_error if y_error is not None else np.zeros(self["Y"].shape)

#    def setLimits(self, min_value=None, max_value=None):
#        if min_value is None:
#            self.minimum = None
#        elif isinstance(min_value, number_types):
#            self.minimum = float(min_value)
#        elif isinstance(min_value, array_types) and len(min_value) == 1:
#            self.minimum = float(min_value[0])
#        else:
#            warnings.warn("%s minimum value argument must be a number or None, minimum not set" % (type(self).setLimits.__name__))
#        if max_value is None:
#            self.maximum = None
#        elif isinstance(max_value, number_types):
#            self.maximum = float(max_value)
#        elif isinstance(max_value, array_types) and len(max_value) == 1:
#            self.maximum = float(max_value[0])
#        else:
#            warnings.warn("%s maximum value argument must be a number or None, maximum not set" % (type(self).setLimits.__name__))

    def setProvenanceData(self, provenance_string):
        if provenance_string is not None:
            self["PROV"] = provenance_string

    def reset(self):
        self.name = None
        self.nxpoints = None
        self.nypoints = None
        self[""] = None
        self["EB"] = None
        self["PROV"] = None
        if "X" in self:
            del self["X"]
        if "XEB" in self:
            del self["XEB"]
        if "Y" in self:
            del self["Y"]
        if "YEB" in self:
            del self["YEB"]

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.required_names:
            if key.upper() == "PROV":
                val = None
                if isinstance(value, str) or value is None:
                    val = value
                else:
                    raise TypeError("%s provenance data entry must be a string" % (type(self).__name__))
                super(EX2GKTwoDimensionalData, self).__setitem__(key.upper(), val)
            else:
                val = np.array([], dtype=np.float64)
                if isinstance(value, array_types):
                    val = np.array(np.atleast_2d(value), dtype=np.float64)
                    if self.nypoints is not None and val.shape[0] != self.nypoints and self.nxpoints is not None and val.shape[1] != self.nxpoints:
                        raise ValueError("%s data entries must be of equal length" % (type(self).__name__))
                    self.nypoints = val.shape[0]
                    self.nxpoints = val.shape[1]
                elif value is None:
                    val = np.zeros((self.nypoints, self.nxpoints), dtype=np.float64) if self.nxpoints is not None and self.nypoints is not None else None
                else:
                    raise TypeError("%s data entries must be array-like" % (type(self).__name__))
                super(EX2GKTwoDimensionalData, self).__setitem__(key.upper(), val)
        elif key in self.optional_requirements:
            if value is not None:
                if key.upper() in ["X", "XEB", "Y", "YEB"]:
                    val = None
                    if isinstance(value, array_types):
                        val = np.array(np.atleast_2d(value), dtype=np.float64)
                        if self.nypoints is not None and val.shape[0] != self.nypoints and self.nxpoints is not None and val.shape[1] != self.nxpoints:
                            raise ValueError("%s data coordinate entries must be of equal length as data" % (type(self).__name__))
                        self.nypoints = val.shape[0]
                        self.nxpoints = val.shape[1]
                    else:
                        raise TypeError("%s data coordinate entries must be array-like" % (type(self).__name__))
                    super(EX2GKTwoDimensionalData, self).__setitem__(key.upper(), val)
        else:
            warnings.warn("%s is not a valid data label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        if "nxpoints" not in self.__dict__:
            self.nxpoints = None
        if "nypoints" not in self.__dict__:
            self.nypoints = None
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        if "nxpoints" in kwargs:
            if isinstance(kwargs["nxpoints"], number_types) and int(kwargs["nxpoints"]) > 0:
                self.nxpoints = int(kwargs["nxpoints"])
            else:
                warnings.warn("%s nxpoints keyword argument must be an integer greater than zero, input not accepted" % (type(self).__name__))
            del kwargs["nxpoints"]
        if "nypoints" in kwargs:
            if isinstance(kwargs["nypoints"], number_types) and int(kwargs["nypoints"]) > 0:
                self.nypoints = int(kwargs["nypoints"])
            else:
                warnings.warn("%s nypoints keyword argument must be an integer greater than zero, input not accepted" % (type(self).__name__))
            del kwargs["nypoints"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKTwoDimensionalData, self).__repr__())

    def __copy__(self):
        return type(self)(self, name=self.name, nxpoints=self.nxpoints, nypoints=self.nypoints)

    def copy(self):
        return type(self)(self, name=self.name, nxpoints=self.nxpoints, nypoints=self.nypoints)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.name is None:
            match &= (self.name is other.name)
        else:
            match &= (self.name == other.name)
        if self.nxpoints is None:
            match &= (self.nxpoints is other.nxpoints)
        else:
            match &= (self.nxpoints == other.nxpoints)
        if self.nypoints is None:
            match &= (self.nypoints is other.nypoints)
        else:
            match &= (self.nypoints == other.nypoints)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        # Modified relative tolerance criteria needed due to difference in numpy / scipy
                        fmatch = np.isclose(self[key], other[key], equal_nan=True)
                        if not np.all(fmatch):
                            min_ratio = np.nanmin(np.abs(self[key][~fmatch])) / np.nanmax(np.abs(self[key]))
                            new_rtol = 1.0e-5 / float(np.power(10.0, np.ceil(np.log10(min_ratio))))
                            smatch = np.isclose(self[key], other[key], rtol=new_rtol, equal_nan=True)
                            fmatch |= smatch
                        match &= np.all(fmatch)
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            if key.upper() in self.required_names or key.upper() in self.optional_requirements:
                self[key.upper()] = value
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = copy.deepcopy(super(EX2GKTwoDimensionalData, self).copy())
        dellist = []
        for key, value in output.items():
            if value is None:
                dellist.append(key)
        for item in dellist:
            del output[item]
        if not output:
            output = None
        return output

    # Needs to be redone
    @classmethod
    def importFromPandas(cls, panda, name=None):
        if isinstance(panda, dict):
            dict_ = panda
        else:
            dict_ = {key: val.values for key, val in panda.items()}
        self = cls(dict_, name=name)
        return self

    def exportToPandas(self):
        df = pd.DataFrame(self, index=[self.name])
        df.fillna(np.NaN, inplace=True)
        return df

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                nkey = "VAL" if key == "" else key
                tempdict[nkey] = copy.deepcopy(self[key])
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


# Should rename this class to GlobalDataContainer
class EX2GKZeroDimensionalContainer(dict):

    # Class variables
    required_names = {"RMAG": 'Magnetic axis radial coordinate',
                      "ZMAG": 'Magnetic axis vertical coordinate',
                      "IPLA": 'Total plasma current',
                      "BMAG": 'Magnetic field at magnetic axis',
                      "other": 'Any additional non-required quantity, check extra_info() for naming conventions'}
    recommended_names = {}

    def __init__(self, *args, **kwargs):
        super(EX2GKZeroDimensionalContainer, self).__init__()
        for key in self.required_names:
            if key != "other":
                self.setdefault(key)
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], (EX2GKZeroDimensionalData, EX2GKOneDimensionalData, EX2GKTwoDimensionalData)):
                descdict[key] = self[key].describe()
        return descdict

    def isReady(self, verbose=0):
        gflag = True
        for item in self.required_names:
            if item != "other" and item not in self:
                gflag = False
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def isSourcePresent(self, source_name):
        present = False
        src = source_name.upper() if isinstance(source_name, str) else None
        if src is not None and "POWI"+src in self:
            present = True
        return present

    def addRequiredData(self, r_magnetic_axis, z_magnetic_axis, plasma_current, field_magnetic_axis, \
                        r_magnetic_axis_error=None, z_magnetic_axis_error=None, plasma_current_error=None, field_magnetic_axis_error=None, \
                        r_magnetic_axis_prov=None, z_magnetic_axis_prov=None, plasma_current_prov=None, field_magnetic_axis_prov=None):
        rmagobj = EX2GKZeroDimensionalData()
        rmagobj.setData("RMAG", r_magnetic_axis, r_magnetic_axis_error)
        rmagobj.setProvenanceData(r_magnetic_axis_prov)
        zmagobj = EX2GKZeroDimensionalData()
        zmagobj.setData("ZMAG", z_magnetic_axis, z_magnetic_axis_error)
        zmagobj.setProvenanceData(z_magnetic_axis_prov)
        iplaobj = EX2GKZeroDimensionalData()
        iplaobj.setData("IPLA", plasma_current, plasma_current_error)
        iplaobj.setProvenanceData(plasma_current_prov)
        bmagobj = EX2GKZeroDimensionalData()
        bmagobj.setData("BMAG", field_magnetic_axis, field_magnetic_axis_error)
        bmagobj.setProvenanceData(field_magnetic_axis_prov)
        self["RMAG"] = rmagobj
        self["ZMAG"] = zmagobj
        self["IPLA"] = iplaobj
        self["BMAG"] = bmagobj

    def addData(self, quantity_name, value, error=None, provenance=None, force_integer=False):
        zobj = EX2GKZeroDimensionalData()
        zobj.setData(quantity_name, value, error, force_integer=force_integer)
        zobj.setProvenanceData(provenance)
        self[zobj.name] = zobj

    def addDataObject(self, data_object):
        if not isinstance(data_object, EX2GKZeroDimensionalData):
            raise TypeError("%s object argument must be a %s object" % (type(self).addDataObject.__name__, EX2GKZeroDimensionalData.__name__))
        self[data_object.name] = data_object

    def grabEquilibriumQuantities(self):
        reqvars = ["RMAG", "ZMAG", "IPLA", "RVAC", "BVAC", "PSIBND", "PSIAXS"]
        outdict = dict()
        for item in reqvars:
            outdict[item] = copy.deepcopy(self[item][""]) if item in self else None
        return outdict

    def computeEnergyConfinementScaling(self, style=0, foverwrite=False):
        if "TAUESC" not in self or foverwrite:
            (tauesc, tauesceb) = qtools.calc_scaling_confinement_time(style=style)
            if tauesc is not None:
                self.setData("TAUESC", tauesc, tauesceb)
            else:
                warnings.warn("Scaling law %s not found in %s!" % (law, type(self).computeEnergyConfinementScaling.__name__), UserWarning)
        else:
            warnings.warn("Confinement time scaling data already present in %s. Skipping computation..." % (type(self).__name__), UserWarning)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() == "OTHER":
            raise KeyError("Funny guy... Use another uppercase field name for additional entries to %s" % (type(self).__name__))
        elif key.upper() in self.required_names:
            if value is not None and not isinstance(value, EX2GKZeroDimensionalData):
                raise TypeError("%s entries must be %s objects" % (type(self).__name__, EX2GKZeroDimensionalData.__name__))
            super(EX2GKZeroDimensionalContainer, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            if not isinstance(value, EX2GKZeroDimensionalData):
                raise TypeError("%s entries must be %s objects" % (type(self).__name__, EX2GKZeroDimensionalData.__name__))
            super(EX2GKZeroDimensionalContainer, self).__setitem__(key.upper(), copy.deepcopy(value))

    def update(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKZeroDimensionalContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key in data_dict:
            if key+"EB" in data_dict:
                tempkeys = ["", "EB"]
                tempdict = {"": data_dict[key], "EB": data_dict[key+"EB"]}
                if key+"PROV" in data_dict:
                    tempdict["PROV"] = data_dict[key+"PROV"]
                    tempkeys.append("PROV")
                zobj = EX2GKZeroDimensionalData.importFromDict(tempdict, True)
                zobj.name = key.upper()
                self.addDataObject(zobj)
                for nkey in tempkeys:
                    if nkey not in tempdict:
                        dellist.append(key+nkey)
        for item in dellist:
            if item in data_dict:
                del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            temp = self[key].exportToDict()
            if isinstance(temp, dict):
                for nkey, value in temp.items():
                    if value is not None:
                        output[key+nkey] = value
        return output

    @classmethod
    def importFromPandas(cls, panda):
        self = cls()
        for name, data_panda in panda.iterrows():
            obj = EX2GKZeroDimensionalData.importFromPandas(data_panda, name=name)
            self.addDataObject(obj)
        return self

    def exportToPandas(self):
        dfs = []
        for val in self.values():
            dfs.append(val.exportToPandas())
        return pd.concat(dfs)

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = self[key].stripFromClass(with_descriptions=False)
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


# Should rename this class to RawProfileDataContainer
class EX2GKRawDataContainer(dict):

    # Class variables
    recommended_names = {"N": 'Particle density',
                         "T": 'Temperature',
                         "P": 'Pressure',
                         "AF": 'Flow angular frequency',
                         "U": 'Flow velocity',
                         "Q": 'Safety factor',
                         "IOTA": 'Rotational transform',
                         "SN": 'Particle source density',
                         "ST": 'Heat source density',
                         "SP": 'Momentum source density',
                         "J": 'Current density',
                         "W": 'Energy density',
                         "Z": 'Particle charge',
                         "other": 'Any additional quantity, use uppercase field names'}

    def __init__(self, *args, **kwargs):
        super(EX2GKRawDataContainer, self).__init__()
        self.present_names = {}
        self.update(*args, **kwargs)

    def info(self):
        return self.recommended_names

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], (EX2GKZeroDimensionalData, EX2GKOneDimensionalData, EX2GKTwoDimensionalData)):
                descdict[key] = self[key].describe()
        return descdict

    def getPresentFields(self, style=None):
        otype = 0
        if isinstance(style, str):
            if re.match(r'^dict$', style, flags=re.IGNORECASE):
                otype = 0
            elif re.match(r'^list$', style, flags=re.IGNORECASE):
                otype = 1
            elif re.match(r'^str(ing)?$', style, flags=re.IGNORECASE):
                otype = 2
        output = None
        if otype == 0:
            output = copy.deepcopy(self.present_names)
        elif otype == 1:
            output = []
            for category in self.present_names:
                for ii in range(0, len(self.present_names[category])):
                    output.append(category+self.present_names[category][ii])
        elif otype == 2:
            output = ""
            for category in self.present_names:
                for ii in range(0, len(self.present_names[category])):
                    if output:
                        output = output + ', '
                    output = output + category + self.present_names[category][ii]
        return output

    def isReady(self, verbose=0):
        gflag = True if self else False
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def addRawData(self, quantity_name, raw_x_data, raw_y_data, source_list, source_map, raw_x_error=None, raw_y_error=None, raw_x_coord=None, raw_x_coord_prefix=None, provenance=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).addRawData.__name__))
        if isinstance(raw_x_data, array_types) and raw_x_error is None:
            raw_x_error = np.zeros(raw_x_data.shape)
        if isinstance(raw_y_data, array_types) and raw_y_error is None:
            raw_y_error = np.zeros(raw_y_data.shape)
        robj = EX2GKOneDimensionalData()
        robj.setData(quantity_name.upper(), raw_y_data, raw_y_error, raw_x_data, raw_x_error, raw_x_coord, raw_x_coord_prefix)
        robj.setMappingData(source_list, source_map)
        robj.setProvenanceData(provenance)
        self[robj.name.upper()] = robj

    def addDerivativeConstraint(self, quantity_name, raw_x_data, raw_dy_data, raw_x_error=None, raw_dy_error=None, raw_x_coord=None, raw_x_coord_prefix=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).addDerivativeConstraint.__name__))
        if quantity_name.upper() in self:
            if isinstance(raw_x_data, array_types) and raw_x_error is None:
                raw_x_error = np.zeros(raw_x_data.shape)
            if isinstance(raw_dy_data, array_types) and raw_dy_error is None:
                raw_dy_error = np.zeros(raw_dy_data.shape)
            self[quantity_name.upper()].setDerivativeData(raw_dy_data, raw_dy_error, raw_x_data, raw_x_error, raw_x_coord, raw_x_coord_prefix)
        else:
            warnings.warn("Data field %s does not exist in %s object, cannot set derivative constraint" % (quantity_name.upper(), type(self).__name__))

    def addRawDataObject(self, data_object):
        if not isinstance(data_object, EX2GKOneDimensionalData):
            raise TypeError("%s object argument must be a %s object" % (type(self).addRawProfileDataObject.__name__, EX2GKOneDimensionalData.__name__))
        elif not data_object.isRaw:
            raise AttributeError("%s object argument must be designated as a raw data container" % (type(self).addRawProfileDataObject.__name__))
        self[data_object.name.upper()] = data_object

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if value is not None:
            if key.upper() == "OTHER":
                raise KeyError("Funny guy... Use another uppercase field name for additional entries to %s" % (type(self).__name__))
            elif not isinstance(value, EX2GKOneDimensionalData):
                raise TypeError("%s entries must be a %s object" % (type(self).__name__, EX2GKOneDimensionalData.__name__))
            category = None
            identifier = ""
            for tag in self.recommended_names:
                mm = re.match(r'^'+tag+r'([A-Z0-9_]*)$', key.upper())
                if mm:
                    category = tag
                    identifier = mm.group(1).upper()
            if category is not None and isinstance(identifier, str):
                if category not in self.present_names:
                    self.present_names[category] = []
                if identifier not in self.present_names[category]:
                    self.present_names[category].append(identifier)
            else:
                warnings.warn("%s field name %s cannot be parsed for categorization, skipping categorization step" % (type(self).__name__, key))
            super(EX2GKRawDataContainer, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            warnings.warn("None-type items not allowed in %s, item not added" % (type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "present_names" not in self.__dict__:
            self.present_names = {}
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "present_names" in kwargs:
            if isinstance(kwargs["present_names"], dict):
                self.present_names = copy.deepcopy(kwargs["present_names"])
            else:
                warnings.warn("%s present_names keyword argument must be a dictionary, input not accepted" % (type(self).__name__))
            del kwargs["present_names"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKRawDataContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        match &= (self.present_names == other.present_names)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key in data_dict:
            if key+"X" in data_dict and key+"SRC" in data_dict and key+"MAP" in data_dict and key+"XCS" in data_dict and key+"XCP" in data_dict:
                tempkeys = ["", "X", "SRC", "MAP", "XCS", "XCP"]
                tempdict = dict()
                tempdict[""] = data_dict[key]
                tempdict["X"] = data_dict[key+"X"]
                tempdict["SRC"] = data_dict[key+"SRC"]
                tempdict["MAP"] = data_dict[key+"MAP"]
                tempdict["XCS"] = data_dict[key+"XCS"]
                tempdict["XCP"] = data_dict[key+"XCP"]
                if key+"EB" in data_dict:
                    tempdict["EB"] = data_dict[key+"EB"]
                    tempkeys.append("EB")
                if key+"PROV" in data_dict:
                    tempdict["PROV"] = data_dict[key+"PROV"]
                    tempkeys.append("PROV")
                if key+"XEB" in data_dict:
                    tempdict["XEB"] = data_dict[key+"XEB"]
                    tempkeys.append("XEB")
                if key+"GRAD" in data_dict:
                    tempdict["GRAD"] = data_dict[key+"GRAD"]
                    tempkeys.append("GRAD")
                if key+"XGRAD" in data_dict:
                    tempdict["XGRAD"] = data_dict[key+"XGRAD"]
                    tempkeys.append("XGRAD")
                if key+"GRADEB" in data_dict:
                    tempdict["GRADEB"] = data_dict[key+"GRADEB"]
                    tempkeys.append("GRADEB")
                if key+"XEBGRAD" in data_dict:
                    tempdict["XEBGRAD"] = data_dict[key+"XEBGRAD"]
                    tempkeys.append("XEBGRAD")
                oobj = EX2GKOneDimensionalData.importFromDict(tempdict, True)
                oobj.name = key.upper()
                self.addRawDataObject(oobj)
                for nkey in tempkeys:
                    if nkey not in tempdict:
                        dellist.append(key+nkey)
        for item in dellist:
            if item in data_dict:
                del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            temp = self[key].exportToDict()
            if isinstance(temp, dict):
                for nkey, value in temp.items():
                    if value is not None:
                        output[key+nkey] = value
        return output

    @classmethod
    def importFromPandas(cls, panda):
        self = cls()
        var_names = panda.columns.get_level_values(0).unique()
        for var_name in var_names:
            coordinate = panda.index.name
            data_panda = panda[var_name].dropna(how='all')
            data_panda.index.name = 'X'
            data_panda = data_panda.reset_index()
            data_panda["MAP"] = None
            for ii, source in enumerate(data_panda["SRCMAP"].unique()):
                data_panda.loc[data_panda["SRCMAP"] == source, "MAP"] = ii
            data_panda["MAP"] = data_panda["MAP"].astype(int)
            data_panda.sort_values(by=["MAP", "X"], inplace=True)
            src = data_panda.pop("SRCMAP").unique()
            dict_ = dict(data_panda)
            dict_['SRC'] = list(src)

            obj = EX2GKOneDimensionalData.importFromPandas(dict_,
                                                           name=var_name,
                                                           coordinate=coordinate)
            self.addRawDataObject(obj)
        return self

    def exportToPandas(self):
        dfs = {}
        for field in self.getPresentFields('list'):
            var = self[field]
            df = var.exportToPandas()
            coord = df.index.names[0]
            if coord not in dfs:
                dfs[coord] = [df]
            else:
                dfs[coord].append(df)
        return {name: pd.concat(collection, axis=1) for name, collection in dfs.items()}

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = self[key].stripFromClass(with_descriptions=False)
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKEquilibriumDataContainer(dict):

    # Class variables
    required_names = {"PSI": 'Poloidal flux map',
                      "F": 'Toroidal flux profile',
                      "P": 'Pressure profile',
                      "FFP": 'Toroidal flux derivative profile',
                      "PP": 'Pressure derivative profile',
                      "Q": 'Safety factor profile',
                      "BND": 'Plasma boundary location vector'}
    recommended_names = {"BND": 'Plasma boundary location vector',
                         "LIM": 'Plasma limiter location vector',
                         #"R": 'Flux surface average related to major radius',
                         #"G": 'Flux surface average related to gradient quantities',
                         #"B": 'Flux surface average related to magnetic fields',
                         #"F": 'Flux surface average related to magnetic flux',
                         #"P": 'Flux surface average related to pressure',
                         #"J": 'Flux surface average related to current',
                         #"X": 'Flux surface label',
                         "other": 'Any additional quantity, use uppercase field names'}

    def __init__(self, *args, **kwargs):
        super(EX2GKEquilibriumDataContainer, self).__init__()
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], (EX2GKZeroDimensionalData, EX2GKOneDimensionalData, EX2GKTwoDimensionalData)):
                descdict[key] = self[key].describe()
        return descdict

    def isReady(self, verbose=0):
        gflag = True
        for item in self.required_names:
            if item not in self:
                gflag = False
        if "BND" not in self and "LIM" not in self:
            gflag = False
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def addEquilibriumFluxMap(self, psimap, rmap, zmap, psimap_error=None, rmap_error=None, zmap_error=None, provenance=None):
        mobj = EX2GKTwoDimensionalData()
        mobj.setData("PSI", psimap, psimap_error, rmap, rmap_error, zmap, zmap_error)
        mobj.setProvenanceData(provenance)
        self[mobj.name] = mobj

    def addEquilibriumBoundaryVector(self, rbnd, zbnd, rbnd_error=None, zbnd_error=None, provenance=None):
        bobj = EX2GKOneDimensionalData()
        bobj.setData("BND", rbnd, rbnd_error, zbnd, zbnd_error, "Z")
        bobj.setProvenanceData(provenance)
        self[bobj.name] = bobj

    def addEquilibriumLimiterVector(self, rlim, zlim, rlim_error=None, zlim_error=None, provenance=None):
        lobj = EX2GKOneDimensionalData()
        lobj.setData("LIM", rlim, rlim_error, zlim, zlim_error, "Z")
        lobj.setProvenanceData(provenance)
        self[lobj.name] = lobj

    def addEquilibriumFluxFunctions(self, fluxlabel, fluxlabel_tag, fvec, pvec, fprimevec, pprimevec, qvec, \
                                    fluxlabel_error=None, fvec_error=None, pvec_error=None, fprimevec_error=None, pprimevec_error=None, qvec_error=None, \
                                    fvec_prov=None, pvec_prov=None, fprimevec_prov=None, pprimevec_prov=None, qvec_prov=None):
        if isinstance(fluxlabel, array_types) and isinstance(fvec, array_types) and len(fluxlabel) == len(fvec):
            fobj = EX2GKOneDimensionalData()
            fobj.setData("F", fvec, fvec_error, fluxlabel, fluxlabel_error, fluxlabel_tag)
            fobj.setProvenanceData(fvec_prov)
            self[fobj.name] = fobj
        if isinstance(fluxlabel, array_types) and isinstance(fprimevec, array_types) and len(fluxlabel) == len(fprimevec):
            fpobj = EX2GKOneDimensionalData()
            fpobj.setData("FFP", fprimevec, fprimevec_error, fluxlabel, fluxlabel_error, fluxlabel_tag)
            fpobj.setProvenanceData(fprimevec_prov)
            self[fpobj.name] = fpobj
        if isinstance(fluxlabel, array_types) and isinstance(pvec, array_types) and len(fluxlabel) == len(pvec):
            pobj = EX2GKOneDimensionalData()
            pobj.setData("P", pvec, pvec_error, fluxlabel, fluxlabel_error, fluxlabel_tag)
            pobj.setProvenanceData(pvec_prov)
            self[pobj.name] = pobj
        if isinstance(fluxlabel, array_types) and isinstance(pprimevec, array_types) and len(fluxlabel) == len(pprimevec):
            ppobj = EX2GKOneDimensionalData()
            ppobj.setData("PP", pprimevec, pprimevec_error, fluxlabel, fluxlabel_error, fluxlabel_tag)
            ppobj.setProvenanceData(pprimevec_prov)
            self[ppobj.name] = ppobj
        if isinstance(fluxlabel, array_types) and isinstance(qvec, array_types) and len(fluxlabel) == len(qvec):
            qobj = EX2GKOneDimensionalData()
            qobj.setData("Q", qvec, qvec_error, fluxlabel, fluxlabel_error, fluxlabel_tag)
            qobj.setProvenanceData(qvec_prov)
            self[qobj.name] = qobj

    def addEquilibriumData(self, quantity_name, y_data, y_error, x_data=None, x_error=None, x_coord=None, provenance=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).addEquilibriumData.__name__))
        robj = EX2GKOneDimensionalData()
        robj.setData(quantity_name.upper(), y_data, y_error, x_data, x_error, x_coord)
        robj.setProvenanceData(provenance)
        self[robj.name] = robj

    def addEquilibriumDataObject(self, data_object):
        if not isinstance(data_object, EX2GKOneDimensionalData):
            raise TypeError("%s object argument must be a %s object" % (type(self).addEquilibriumDataObject.__name__, EX2GKOneDimensionalData.__name__))
        elif data_object.isRaw():
            raise AttributeError("%s object argument must not be designated as a raw data container" % (type(self).addEquilibriumDataObject.__name__))
        self[data_object.name.upper()] = data_object

    def grabEquilibriumQuantities(self):
        reqvars = ["F", "P", "FFP", "PP", "PSI", "Q", "BND", "LIM"]
        outdict = dict()
        for item in reqvars:
            if item == "PSI":
                outdict["PSI"] = copy.deepcopy(self[item][""]) if item in self else None
                rmap = copy.deepcopy(self[item]["X"]) if item in self else None
                zmap = copy.deepcopy(self[item]["Y"]) if item in self else None
                outdict["NR"] = outdict["PSI"].shape[1] if outdict["PSI"] is not None else None
                outdict["NZ"] = outdict["PSI"].shape[0] if outdict["PSI"] is not None else None
                outdict["RDIM"] = float(np.nanmax(rmap) - np.nanmin(rmap)) if rmap is not None else None
                outdict["ZDIM"] = float(np.nanmax(zmap) - np.nanmin(zmap)) if zmap is not None else None
                outdict["RLEFT"] = float(np.nanmin(rmap)) if rmap is not None else None
                outdict["ZMID"] = float(np.nanmax(zmap) + np.nanmin(zmap)) / 2.0 if zmap is not None else None
                outdict["RRIGHT"] = float(np.nanmax(rmap)) if rmap is not None else None
                outdict["ZBOTTOM"] = float(np.nanmin(zmap)) if zmap is not None else None
                outdict["ZTOP"] = float(np.nanmax(zmap)) if zmap is not None else None
            elif item == "BND":
                rbnd = copy.deepcopy(self[item][""]) if item in self else None
                zbnd = copy.deepcopy(self[item]["X"]) if item in self else None
                if rbnd is not None and zbnd is not None:
                    gfilt = np.all([np.isfinite(rbnd), np.isfinite(zbnd)], axis=0)
                    for ii in range(len(rbnd)):
                        for jj in range(ii+1, len(rbnd)):
                            if gfilt[ii] and np.abs(rbnd[ii] - rbnd[jj]) < 1.0e-5 and np.abs(zbnd[ii] - zbnd[jj]) < 1.0e-5:
                                gfilt[ii] = False
                    outdict["RBND"] = rbnd[gfilt]
                    outdict["ZBND"] = zbnd[gfilt]
                    outdict["NBND"] = outdict["RBND"].size
                else:
                    outdict["RBND"] = None
                    outdict["ZBND"] = None
                    outdict["NBND"] = 0
            elif item == "LIM":
                rlim = copy.deepcopy(self[item][""]) if item in self else None
                zlim = copy.deepcopy(self[item]["X"]) if item in self else None
                if rlim is not None and zlim is not None:
                    outdict["RLIM"] = rlim
                    outdict["ZLIM"] = zlim
                    outdict["NLIM"] = outdict["RLIM"].size
                else:
                    outdict["RLIM"] = None
                    outdict["ZLIM"] = None
                    outdict["NLIM"] = 0
            else:
                outdict[item] = copy.deepcopy(self[item][""]) if item in self else None
                if "X" not in outdict and item in self:
                    outdict["X"] = copy.deepcopy(self[item]["X"])
                if "XCS" not in outdict and item in self and self[item].coordinate is not None:
                    outdict["XCS"] = self[item].coordinate
                if "XCP" not in outdict and item in self and self[item].coord_prefix is not None:
                    outdict["XCP"] = self[item].coord_prefix
        return outdict

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if value is not None:
            if key.upper() == "OTHER":
                raise KeyError("Funny guy... Use another uppercase field name for additional entries to %s" % (type(self).__name__))
            elif key.upper() == "PSI":
                if not isinstance(value, EX2GKTwoDimensionalData):
                    raise TypeError("%s entries must be a %s object" % (type(self).__name__, EX2GKTwoDimensionalData.__name__))
            elif not isinstance(value, EX2GKOneDimensionalData):
                raise TypeError("%s entries must be a %s object" % (type(self).__name__, EX2GKOneDimensionalData.__name__))
            super(EX2GKEquilibriumDataContainer, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            warnings.warn("None-type items not allowed in %s, item not added" % (type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKEquilibriumDataContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key in data_dict:
            if key+"EB" in data_dict:
                if key+"X" in data_dict:
                    twodimflag = False
                    tempkeys = ["", "EB", "X"]
                    tempdict = dict()
                    tempdict[""] = data_dict[key]
                    tempdict["EB"] = data_dict[key+"EB"]
                    tempdict["X"] = data_dict[key+"X"]
                    if key+"PROV" in data_dict:
                        tempdict["PROV"] = data_dict[key+"PROV"]
                        tempkeys.append("PROV")
                    if key+"XCS" in data_dict:
                        tempdict["XCS"] = data_dict[key+"XCS"]
                        tempkeys.append("XCS")
                    if key+"XCP" in data_dict:
                        tempdict["XCP"] = data_dict[key+"XCP"]
                        tempkeys.append("XCP")
                    if key+"XEB" in data_dict:
                        tempdict["XEB"] = data_dict[key+"XEB"]
                        tempkeys.append("XEB")
                    if key+"Y" in data_dict:
                        tempdict["Y"] = data_dict[key+"Y"]
                        tempkeys.append("Y")
                        twodimflag = True
                    if key+"YEB" in data_dict:
                        tempdict["YEB"] = data_dict[key+"YEB"]
                        tempkeys.append("YEB")
                        twodimflag = True
                    oobj = EX2GKOneDimensionalData.importFromDict(tempdict, True) if not twodimflag else EX2GKTwoDimensionalData.importFromDict(tempdict, True)
                    oobj.name = key.upper()
                    self[oobj.name] = oobj
                    for nkey in tempkeys:
                        if nkey not in tempdict:
                            dellist.append(key+nkey)
                elif isinstance(data_dict[key], number_types):
                    tempkeys = ["", "EB"]
                    tempdict = dict()
                    tempdict[""] = data_dict[key]
                    tempdict["EB"] = data_dict[key+"EB"]
                    oobj = EX2GKZeroDimensionalData.importFromDict(tempdict, True)
                    oobj.name = key.upper()
                    self[oobj.name] = oobj
                    for nkey in tempkeys:
                        if nkey not in tempdict:
                            dellist.append(key+nkey)
        for item in dellist:
            if item in data_dict:
                del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            temp = self[key].exportToDict()
            if isinstance(temp, dict):
                for nkey, value in temp.items():
                    if nkey == "EB" and value is None:
                        output[key+nkey] = np.array([0.0] * temp[""].size, dtype=np.float64)
                    elif nkey == "XEB" and value is None and "X" in temp and temp["X"] is not None:
                        output[key+nkey] = np.array([0.0] * temp["X"].size, dtype=np.float64)
                    elif nkey == "YEB" and value is None and "Y" in temp and temp["Y"] is not None:
                        output[key+nkey] = np.array([0.0] * temp["Y"].size, dtype=np.float64)
                    elif value is not None:
                        output[key+nkey] = value
        return output

    # Needs to be redone
    @classmethod
    def importFromPandas(cls, panda):
        self = cls()
#        var_names = panda.columns.get_level_values(0).unique()
#        for var_name in var_names:
#            data_panda = panda[var_name]
#            obj = EX2GKOneDimensionalData.importFromPandas(data_panda,
#                                                           name=var_name,
#                                                           coordinate=data_panda.index.name)
#            self.addProfileDataObject(obj)
        return self

    # Needs to be redone
    def exportToPandas(self):
#        dfs = []
#        for field in self.getPresentFields('list'):
#            df = self[field].exportToPandas()
#            dfs.append(df)
#        return pd.concat(dfs, axis=1)
        return None

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = self[key].stripFromClass(with_descriptions=False)
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKProfileDataContainer(dict):

    # Class variables
    recommended_names = {"N": 'Particle density',
                         "T": 'Temperature',
                         "P": 'Pressure',
                         "AF": 'Flow angular frequency',
                         "U": 'Flow angular velocity',
                         "Q": 'Safety factor',
                         "IOTA": 'Rotational transform',
                         "SN": 'Particle source density',
                         "ST": 'Heat source density',
                         "SP": 'Momentum source density',
                         "J": 'Current density',
                         "W": 'Energy density',
                         "Z": 'Particle charge',
                         "FN": 'Particle flux',
                         "FT": 'Heat flux',
                         "FP": 'Momentum flux',
                         "FB": 'Magnetic field flux function',
                         "B": 'Magnetic field',
                         "E": 'Electric field',
                         "X": 'Radial coordinate',
                         "V": 'Volume coordinate',
                         "L": 'Length scales',
                         #"A": 'Inverse length scales',
                         "OM": 'Inverse time scales',
                         "SH": 'Magnetic shear',
                         "OUT": 'Code-specific quantities',
                         "other": 'Any additional quantity, use uppercase field names'}

    def __init__(self, *args, **kwargs):
        super(EX2GKProfileDataContainer, self).__init__()
        self.present_names = {}
        self.update(*args, **kwargs)

    def info(self):
        return self.recommended_names

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], (EX2GKZeroDimensionalData, EX2GKOneDimensionalData, EX2GKTwoDimensionalData)):
                descdict[key] = self[key].describe()
        return descdict

    def isReady(self, verbose=0):
        gflag = True
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def getPresentFields(self, style=None):
        otype = 0
        if isinstance(style, str):
            if re.match(r'^dict$', style, flags=re.IGNORECASE):
                otype = 0
            elif re.match(r'^list$', style, flags=re.IGNORECASE):
                otype = 1
            elif re.match(r'^str(ing)?$', style, flags=re.IGNORECASE):
                otype = 2
        output = None
        if otype == 0:
            output = copy.deepcopy(self.present_names)
        elif otype == 1:
            output = []
            for category in self.present_names:
                for ii in range(0, len(self.present_names[category])):
                    output.append(category+self.present_names[category][ii])
        elif otype == 2:
            output = ""
            for category in self.present_names:
                for ii in range(0, len(self.present_names[category])):
                    if output:
                        output = output + ', '
                    output = output + category + self.present_names[category][ii]
        return output

    def isSourcePresent(self, source_name):
        present = False
        src = source_name.upper() if isinstance(source_name, str) else None
        if src is not None:
            pf = self.getPresentFields()
            for category in ["ST", "SN", "SP", "J"]:
                if category in pf:
                    if src in pf[category]:
                        present = True
                    if "E"+src in pf[category]:
                        present = True
                    if "I"+src in pf[category]:
                        present = True
        return present

    def addProfileData(self, quantity_name, y_data, y_error, x_data=None, x_error=None, x_coord=None, x_coord_prefix=None, provenance=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).addProfileData.__name__))
        robj = EX2GKOneDimensionalData()
        robj.setData(quantity_name.upper(), y_data, y_error, x_data, x_error, x_coord, x_coord_prefix)
        robj.setProvenanceData(provenance)
        self[robj.name] = robj

    def addProfileDerivativeData(self, quantity_name, dy_data=None, dy_error=None, x_data=None, x_error=None, x_coord=None, x_coord_prefix=None):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).addProfileDerivativeData.__name__))
        if quantity_name.upper() in self:
            self[quantity_name.upper()].setDerivativeData(dy_data, dy_error, x_data, x_error, x_coord, x_coord_prefix)
        else:
            warnings.warn("Data field %s does not exist in %s object, cannot set derivative data" % (quantity_name.upper(), type(self).__name__))

    def addProfileDataObject(self, data_object):
        if not isinstance(data_object, EX2GKOneDimensionalData):
            raise TypeError("%s object argument must be a %s object" % (type(self).addProfileDataObject.__name__, EX2GKOneDimensionalData.__name__))
        elif data_object.isRaw():
            raise AttributeError("%s object argument must not be designated as a raw data container" % (type(self).addProfileDataObject.__name__))
        self[data_object.name.upper()] = data_object

    def interpolate(self, data, x_field, y_field, error_flag=False, grad_flag=False):
        dvec = None
        if isinstance(data, number_types):
            dvec = np.array([data], dtype=np.float64).flatten()
        if isinstance(data, array_types):
            dvec = np.array(data, dtype=np.float64).flatten()
        ovec = None
        if isinstance(dvec, array_types) and x_field in self and y_field in self and self[x_field].npoints == self[y_field].npoints and "" in self[y_field]:
            dfilt = np.isfinite(dvec)
            if np.any(dfilt):
                ovec = np.full(dvec.shape, np.NaN)
                if grad_flag and "GRAD" in self[y_field]:
                    if error_flag and "GRADEB" in self[y_field]:
                        ovec = np.zeros(dvec.shape)
                        nfilt = np.all([np.isfinite(self[x_field][""]), np.isfinite(self[y_field]["GRADEB"])], axis=0)
                        if np.count_nonzero(nfilt) > 1:
                            ofunc = interp1d(self[x_field][""][nfilt], self[y_field]["GRADEB"][nfilt], kind='linear', bounds_error=False, fill_value='extrapolate')
                            ovec[dfilt] = ofunc(dvec[dfilt])
                        else:
                            warnings.warn("%s interpolation arguments do not contain sufficient data, returning None" % (type(self).__name__))
                    else:
                        nfilt = np.all([np.isfinite(self[x_field][""]), np.isfinite(self[y_field]["GRAD"])], axis=0)
                        if np.count_nonzero(nfilt) > 1:
                            ofunc = interp1d(self[x_field][""][nfilt], self[y_field]["GRAD"][nfilt], kind='linear', bounds_error=False, fill_value='extrapolate')
                            ovec[dfilt] = ofunc(dvec[dfilt])
                        else:
                            warnings.warn("%s interpolation arguments do not contain sufficient data, returning None" % (type(self).__name__))
                elif error_flag and "EB" in self[y_field]:
                    ovec = np.zeros(dvec.shape)
                    nfilt = np.all([np.isfinite(self[x_field][""]), np.isfinite(self[y_field]["EB"])], axis=0)
                    if np.count_nonzero(nfilt) > 1:
                        ofunc = interp1d(self[x_field][""][nfilt], self[y_field]["EB"][nfilt], kind='linear', bounds_error=False, fill_value='extrapolate')
                        ovec[dfilt] = ofunc(dvec[dfilt])
                    else:
                        warnings.warn("%s interpolation arguments do not contain sufficient data, returning None" % (type(self).__name__))
                else:
                    nfilt = np.all([np.isfinite(self[x_field][""]), np.isfinite(self[y_field][""])], axis=0)
                    if np.count_nonzero(nfilt) > 1:
                        ofunc = interp1d(self[x_field][""][nfilt], self[y_field][""][nfilt], kind='linear', bounds_error=False, fill_value='extrapolate')
                        ovec[dfilt] = ofunc(dvec[dfilt])
                    else:
                        warnings.warn("%s interpolation arguments do not contain sufficient data, returning None" % (type(self).__name__))
            else:
                warnings.warn("%s interpolation function was passed invalid arguments, returning None" % (type(self).__name__))
        else:
            warnings.warn("%s interpolation function was passed invalid arguments, returning None" % (type(self).__name__))
        return ovec

    def grabEquilibriumQuantities(self, reqtags=["E", "I1"], xvec=None):
        ee = 1.60217662e-19
        outdict = {"PEI": None, "DPEI": None}
        if isinstance(xvec, array_types) and "X" in self.present_names and "" in self.present_names["X"]:
            xtemp = np.array(xvec, dtype=np.float64).flatten()
            for itag in reqtags:
                if "P" in self.present_names and itag in self.present_names["P"]:
                    tempv = self.interpolate(xvec, "X", "P"+itag)
                    tempd = self.interpolate(xvec, "X", "P"+itag, grad_flag=True)
                    outdict["PEI"] = copy.deepcopy(tempv) if outdict["PEI"] is None else outdict["PEI"] + tempv
                    outdict["DPEI"] = copy.deepcopy(tempd) if outdict["DPEI"] is None else outdict["PEI"] + tempd
                elif "N" in self.present_names and itag in self.present_names["N"] and "T" in self.present_names and itag in self.present_names["T"]:
                    tempn = self.interpolate(xvec, "X", "N"+itag)
                    tempdn = self.interpolate(xvec, "X", "N"+itag, grad_flag=True)
                    tempt = self.interpolate(xvec, "X", "T"+itag)
                    tempdt = self.interpolate(xvec, "X", "T"+itag, grad_flag=True)
                    outdict["PEI"] = ee * tempn * tempt if outdict["PEI"] is None else outdict["PEI"] + ee * tempn * tempt
                    outdict["DPEI"] = ee * (tempn * tempdt + tempdn * tempn) if outdict["DPEI"] is None else outdict["PEI"] + ee * (tempn * tempdt + tempdn * tempn)
        return outdict

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if value is not None:
            if key.upper() == "OTHER":
                raise KeyError("Funny guy... Use another uppercase field name for additional entries to %s" % (type(self).__name__))
            elif not isinstance(value, EX2GKOneDimensionalData):
                raise TypeError("%s entries must be a %s object" % (type(self).__name__, EX2GKOneDimensionalData.__name__))
            category = None
            identifier = ""
            for tag in self.recommended_names:
                mm = re.search(r'^'+tag+r'([A-Z0-9_]*)$', key.upper())
                if mm:
                    category = tag
                    identifier = mm.group(1).upper()
            if category is not None and isinstance(identifier, str):
                if category not in self.present_names:
                    self.present_names[category] = []
                if identifier not in self.present_names[category]:
                    self.present_names[category].append(identifier)
            else:
                warnings.warn("%s field name %s cannot be parsed for categorization, skipping categorization step" % (type(self).__name__, key))
            super(EX2GKProfileDataContainer, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            warnings.warn("None-type items not allowed in %s, item not added" % (type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "present_names" not in self.__dict__:
            self.present_names = {}
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "present_names" in kwargs:
            if isinstance(kwargs["present_names"], dict):
                self.present_names = copy.deepcopy(kwargs["present_names"])
            else:
                warnings.warn("%s present_names keyword argument must be a dictionary, input not accepted" % (type(self).__name__))
            del kwargs["present_names"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKProfileDataContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        match &= (self.present_names == other.present_names)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key in data_dict:
            xcstemp = None
            xcptemp = None
            if key+"XCS" not in data_dict:
                xcstemp = "RHOTORN" if key == "X" else "PD_X"
                xcptemp = ""
            if not key.endswith("GRAD") and key+"EB" in data_dict:
                tempkeys = ["", "EB"] if xcstemp is not None else ["", "EB", "XCS"]
                tempdict = dict()
                tempdict[""] = data_dict[key]
                tempdict["EB"] = data_dict[key+"EB"]
                tempdict["XCS"] = xcstemp if xcstemp is not None else data_dict[key+"XCS"]
                if key+"XCP" in data_dict:
                    tempdict["XCP"] = data_dict[key+"XCP"]
                    tempkeys.append("XCP")
                elif xcptemp is not None:
                    tempdict["XCP"] = xcptemp
                if key+"PROV" in data_dict:
                    tempdict["PROV"] = data_dict[key+"PROV"]
                    tempkeys.append("PROV")
                if key+"X" in data_dict:
                    tempdict["X"] = data_dict[key+"X"]
                    tempkeys.append("X")
                if key+"XEB" in data_dict:
                    tempdict["XEB"] = data_dict[key+"XEB"]
                    tempkeys.append("XEB")
                if key+"GRAD" in data_dict:
                    tempdict["GRAD"] = data_dict[key+"GRAD"]
                    tempkeys.append("GRAD")
                if key+"XGRAD" in data_dict:
                    tempdict["XGRAD"] = data_dict[key+"XGRAD"]
                    tempkeys.append("XGRAD")
                if key+"GRADEB" in data_dict:
                    tempdict["GRADEB"] = data_dict[key+"GRADEB"]
                    tempkeys.append("GRADEB")
                if key+"XEBGRAD" in data_dict:
                    tempdict["XEBGRAD"] = data_dict[key+"XEBGRAD"]
                    tempkeys.append("XEBGRAD")
                oobj = EX2GKOneDimensionalData.importFromDict(tempdict, True)
                oobj.name = key.upper()
                self.addProfileDataObject(oobj)
                for nkey in tempkeys:
                    if nkey not in tempdict:
                        dellist.append(key+nkey)
        for item in dellist:
            if item in data_dict:
                del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            temp = self[key].exportToDict()
            if isinstance(temp, dict):
                for nkey, value in temp.items():
                    if nkey == "EB" and value is None:
                        output[key+nkey] = np.array([0.0] * temp[""].size, dtype=np.float64)
                    elif nkey == "GRADEB" and value is None and "GRAD" in temp and temp["GRAD"] is not None:
                        output[key+nkey] = np.array([0.0] * temp["GRAD"].size, dtype=np.float64)
                    elif nkey == "XEB" and value is None and "X" in temp and temp["X"] is not None:
                        output[key+nkey] = np.array([0.0] * temp["X"].size, dtype=np.float64)
                    elif nkey == "XEBGRAD" and value is None and "XGRAD" in temp and temp["XGRAD"] is not None:
                        output[key+nkey] = np.array([0.0] * temp["XGRAD"].size, dtype=np.float64)
                    if value is not None and nkey not in ["SRC", "MAP"]:
                        output[key+nkey] = value
        return output

    @classmethod
    def importFromPandas(cls, panda):
        self = cls()
        var_names = panda.columns.get_level_values(0).unique()
        for var_name in var_names:
            data_panda = panda[var_name]
            obj = EX2GKOneDimensionalData.importFromPandas(data_panda,
                                                           name=var_name,
                                                           coordinate=data_panda.index.name)
            self.addProfileDataObject(obj)
        return self

    def exportToPandas(self):
        dfs = []
        for field in self.getPresentFields('list'):
            df = self[field].exportToPandas()
            dfs.append(df)
        return pd.concat(dfs, axis=1)

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = self[key].stripFromClass(with_descriptions=False)
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
            tempdict["DESC"]["X"] = "Radial Coordinate, see X.XCS"
        return tempdict


class EX2GKFitSettings(dict):

    # Class variables
    required_names = {}

    def __init__(self, *args, **kwargs):
        super(EX2GKFitSettings, self).__init__()
        self.name = None
        self.update(*args, **kwargs)

    def info(self):
        return self.required_names

    def describe(self):
        return self.required_names

    def setQuantityName(self, quantity_name):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).setQuantityName.__name__))
        self.name = quantity_name.upper()

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        super(EX2GKFitSettings, self).__setitem__(key.upper(), copy.deepcopy(value))

    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKFitSettings, self).__repr__())

    def __copy__(self):
        return type(self)(self, name=self.name)

    def copy(self):
        return type(self)(self, name=self.name)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.name is None:
            match &= (self.name is other.name)
        else:
            match &= (self.name == other.name)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            try:
                if key.upper() == "QUANTITY":
                    self.name = value
                else:
                    self[key.upper()] = value
                dellist.append(key)
            except TypeError:
                pass
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        outdict = copy.deepcopy(super(EX2GKFitSettings, self).copy())
        if self.name is not None:
            outdict["QUANTITY"] = self.name
        return outdict

    def exportToXMLString(self):
        obj = self.exportToDict()
        xmlstr = fmtconv.obj2xmls(obj)
        xmlstr = '<fit_settings>'+xmlstr+'</fit_settings>'
        return xmlstr

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = copy.deepcopy(self[key])
        if self.name is not None:
            tempdict["NAME"] = self.name
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKPolyFitSettings(EX2GKFitSettings):

    # Class variables
    required_names = {"DEGREE": 'Polynomial degree to be used as fitting curve',
                      "ANCHORX": 'Anchor x-values to constrain the fit',
                      "ANCHORY": 'Anchor y-values to constrain the fit',
                      "OMIT_CST": 'Specifies omission of constant term in polynomial, ensures passing through origin',
                      "OMIT_LIN": 'Specifies omission of linear term in polynomial, ensures zero derivative at origin',
                      "COEFFS": 'Coefficients of fitted polynomial',
                      "RSQADJ": 'Adjusted R-squared value representing the goodness-of-fit'}

    def __init__(self, *args, **kwargs):
        super(EX2GKPolyFitSettings, self).__init__(*args, **kwargs)
        self.num_anchors = None
        self.is_optimized = False
        for key in self.required_names:
            self.setdefault(key)
        self.update(*args, **kwargs)

    def resetAnchors(self):
        self.num_anchors = None
        self["ANCHORX"] = None
        self["ANCHORY"] = None

    def isOptimized(self):
        return self.is_optimized

    def setOptimized(self, value=True):
        val = True if value else False
        self.is_optimized = val

    def definePolyFitSettings(self, poly_degree, x_anchors=None, y_anchors=None, omit_constant_flag=False, omit_linear_flag=False):
        self["DEGREE"] = poly_degree
        if x_anchors is not None and y_anchors is not None:
            self.resetAnchors()
            self["ANCHORX"] = x_anchors
            self["ANCHORY"] = y_anchors
        self["OMIT_CST"] = omit_constant_flag
        self["OMIT_LIN"] = omit_linear_flag

    def setCoefficients(self, coefficients, rsquared=None, is_optimized=True):
        if not isinstance(self["DEGREE"], int):
            raise ValueError("%s polynomial degree has not been specified, fit coefficients cannot be set" % (type(self).__name__))
        if not isinstance(coefficients, array_types):
            raise TypeError("%s coefficients argument must be array-like" % (type(self).setCoefficients.__name__))
        ncoeffs = self["DEGREE"] + 1
#        if self["OMIT_CST"]:
#            ncoeffs = ncoeffs - 1
#        if self["OMIT_LIN"]:
#            ncoeffs = ncoeffs - 1
        if len(coefficients) == ncoeffs:
            self["COEFFS"] = coefficients
            if rsquared is not None:
                self["RSQADJ"] = rsquared
            self.setOptimized(is_optimized)
        else:
            warnings.warn("%s coefficients argument must have a number of entries consistent with the stored settings" % (type(self).setCoefficients.__name__))

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.required_names:
            val = None
            if value is not None:
                if key.upper() == "DEGREE":
                    if not isinstance(value, number_types):
                        raise TypeError("%s polynomial degree must be an integer greater than zero" % (type(self).__name__))
                    if int(value) <= 0:
                        raise ValueError("%s polynomial degree must be an integer greater than zero" % (type(self).__name__))
                    val = int(value)
                if key.upper() == "ANCHORX":
                    if not isinstance(value, array_types):
                        raise TypeError("%s anchor x-values must be array-like" % (type(self).__name__))
                    if self.num_anchors is not None and len(value) != self.num_anchors:
                        raise ValueError("%s anchor x-values must be the same length as other anchor data" % (type(self).__name__))
                    val = np.array(value, dtype=np.float64).flatten()
                    self.num_anchors = val.size
                if key.upper() == "ANCHORY":
                    if not isinstance(value, array_types):
                        raise TypeError("%s anchor y-values must be array-like" % (type(self).__name__))
                    if self.num_anchors is not None and len(value) != self.num_anchors:
                        raise ValueError("%s anchor x-values must be the same length as other anchor data" % (type(self).__name__))
                    val = np.array(value, dtype=np.float64).flatten()
                    self.num_anchors = val.size
                if key.upper() == "OMIT_CST":
                    val = True if value else False
                if key.upper() == "OMIT_LIN":
                    val = True if value else False
                if key.upper() == "COEFFS":
                    if not isinstance(value, array_types):
                        raise TypeError("%s coefficients must be array-like" % (type(self).__name__))
                    val = np.array(value, dtype=np.float64).flatten()
                if key.upper() == "RSQADJ":
                    if not isinstance(value, number_types):
                        raise TypeError("%s adjusted R-squared must be a real number" % (type(self).__name__))
                    val = float(value)
            super(EX2GKPolyFitSettings, self).__setitem__(key.upper(), val)
        else:
            warnings.warn("%s is not a valid data label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)


    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        if "num_anchors" not in self.__dict__:
            self.num_anchors = None
        if "is_optimized" not in self.__dict__:
            self.is_optimized = False
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        if "num_anchors" in kwargs:
            if isinstance(kwargs["num_anchors"], number_types) and int(kwargs["num_anchors"]) >= 0:
                self.num_anchors = int(kwargs["num_anchors"])
            else:
                warnings.warn("%s num_anchors keyword argument must be an integer greater than or equal to zero, input not accepted" % (type(self).__name__))
            del kwargs["num_anchors"]
        elif "is_optimized" in kwargs:
            self.is_optimized = True if kwargs["is_optimized"] else False
            del kwargs["is_optimized"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return super(EX2GKPolyFitSettings, self).__repr__()

    def __copy__(self):
        return type(self)(self, name=self.name, is_optimized=self.is_optimized)

    def copy(self):
        return type(self)(self, name=self.name, is_optimized=self.is_optimized)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self.num_anchors is None:
            match &= (self.num_anchors is other.num_anchors)
        else:
            match &= (self.num_anchors == other.num_anchors)
        if match:
            super(EX2GKPolyFitSettings, self).__eq__(other)
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            if key in self.required_names:
                self[key.upper()] = value
                dellist.append(key)
            elif key == "FTYPE":
                dellist.append(key)
            elif key == "OPTIMIZED":
                self.is_optimized = True if value else False
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = super(EX2GKPolyFitSettings, self).exportToDict()
        output["FTYPE"] = "POLY"
        output["OPTIMIZED"] = self.is_optimized
        return output


class EX2GKGPFitSettings(EX2GKFitSettings):

    # Class variables
    required_names = {"FIT_KNAME": 'Kernel name for the data fit',
                      "FIT_KPARS": 'Initial kernel hyperparameters for the data fit',
                      "FIT_REGPAR": 'Regularization parameter for the data fit',
                      "FIT_ONAME": 'Optimizer option for the data fit',
                      "FIT_OPARS": 'Optimizer parameters for the data fit',
                      "FIT_EPSPAR": 'Convergence criterion on LML for the data fit',
                      "FIT_RESTART": 'Number of random restarts to improve robustness for the data fit',
                      "FIT_KBOUNDS": 'Kernel hyperparameter bounds for random restarts for the data fit',
                      "ERR_KNAME": 'Kernel name for the error fit',
                      "ERR_KPARS": 'Initial kernel hyperparameters for the error fit',
                      "ERR_REGPAR": 'Regularization parameter for the error fit',
                      "ERR_ONAME": 'Optimizer option for the error fit',
                      "ERR_OPARS": 'Optimizer parameters for the error fit',
                      "ERR_EPSPAR": 'Convergence criterion on LML for the error fit',
                      "ERR_RESTART": 'Number of random restarts to improve robustness for the data fit',
                      "ERR_KBOUNDS": 'Kernel hyperparameter bounds for random restarts for the error fit',
                      "RSQADJ": 'Adjusted R-squared value representing the goodness-of-fit',
                      "RSQGEN": 'Generalized (pseudo) R-squared value representing probabilistic goodness-of-fit'}

    def __init__(self, *args, **kwargs):
        super(EX2GKGPFitSettings, self).__init__(*args, **kwargs)
        self.is_optimized = False
        for key in self.required_names:
            self.setdefault(key)
        self.update(*args, **kwargs)

    def isOptimized(self):
        return self.is_optimized

    def setOptimized(self, value=True):
        val = True if value else False
        self.is_optimized = val

    def defineFitKernelSettings(self, kernel_name, kernel_hyperparameters, regularization_parameter, \
                                optimizer, optimizer_parameters, convergence_criterion, \
                                number_of_restarts=None, kernel_hyperparameter_restart_bounds=None):
        self["FIT_KNAME"] = kernel_name
        self["FIT_KPARS"] = kernel_hyperparameters
        self["FIT_REGPAR"] = regularization_parameter
        self["FIT_ONAME"] = optimizer
        self["FIT_OPARS"] = optimizer_parameters
        self["FIT_EPSPAR"] = convergence_criterion
        self["FIT_RESTART"] = number_of_restarts
        self["FIT_KBOUNDS"] = kernel_hyperparameter_restart_bounds

    def defineErrorKernelSettings(self, kernel_name, kernel_hyperparameters, regularization_parameter, \
                                  optimizer, optimizer_parameters, convergence_criterion, \
                                  number_of_restarts=None, kernel_hyperparameter_restart_bounds=None):
        self["ERR_KNAME"] = kernel_name
        self["ERR_KPARS"] = kernel_hyperparameters
        self["ERR_REGPAR"] = regularization_parameter
        self["ERR_ONAME"] = optimizer
        self["ERR_OPARS"] = optimizer_parameters
        self["ERR_EPSPAR"] = convergence_criterion
        self["ERR_RESTART"] = number_of_restarts
        self["ERR_KBOUNDS"] = kernel_hyperparameter_restart_bounds

    def setFitMetrics(self, adjusted_rsq, generalized_rsq=None, is_optimized=True):
        self["RSQADJ"] = adjusted_rsq
        self["RSQGEN"] = generalized_rsq
        self.setOptimized(is_optimized)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.required_names:
            val = None
            if value is not None:
                if key.upper().endswith("KNAME"):
                    if not isinstance(value, str):
                        raise TypeError("%s kernel name must be a string" % (type(self).__name__))
                    val = value
                if key.upper().endswith("KPARS"):
                    if not isinstance(value, array_types):
                        raise TypeError("%s kernel hyperparameters must be array-like" % (type(self).__name__))
                    val = np.array(value, dtype=np.float64).flatten()
                if key.upper().endswith("KBOUNDS"):
                    if not isinstance(value, array_types):
                        raise TypeError("%s kernel hyperparameter bounds must be 2D array-like" % (type(self).__name__))
                    val = np.array(np.atleast_2d(value), dtype=np.float64)
                    if val.shape[0] < 2:
                        raise TypeError("%s kernel hyperparameter bounds must be 2D array-like" % (type(self).__name__))
                if key.upper().endswith("REGPAR"):
                    if not isinstance(value, number_types):
                        raise TypeError("%s regularization parameter must be a number" % (type(self).__name__))
                    val = float(value)
                if key.upper().endswith("ONAME"):
                    if not isinstance(value, (number_types, str)):
                        raise TypeError("%s optimizer selection must be an integer or string" % (type(self).__name__))
                    val = value if isinstance(value, str) else int(value)
                if key.upper().endswith("OPARS"):
                    if not isinstance(value, array_types):
                        raise TypeError("%s optimizer parameters must be array-like" % (type(self).__name__))
                    val = np.array(value, dtype=np.float64).flatten()
                if key.upper().endswith("EPSPAR"):
                    if isinstance(value, number_types):
                        val = float(value)
                    elif isinstance(value, str):
                        val = value
                    else:
                        raise TypeError("%s convergence criterion must be a number" % (type(self).__name__))
                if key.upper().endswith("RESTART"):
                    if not isinstance(value, number_types):
                        raise TypeError("%s number of restarts must be an integer" % (type(self).__name__))
                    val = int(value)
                if key.upper().startswith("RSQ"):
                    if not isinstance(value, number_types):
                        raise TypeError("%s R-squared value must be a real number" % (type(self).__name__))
                    val = float(value)
            super(EX2GKGPFitSettings, self).__setitem__(key.upper(), val)
        else:
            warnings.warn("%s is not a valid data label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "name" not in self.__dict__:
            self.name = None
        if "is_optimized" not in self.__dict__:
            self.is_optimized = False
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "name" in kwargs:
            if isinstance(kwargs["name"], str):
                self.name = kwargs["name"]
            else:
                warnings.warn("%s name keyword argument must be a string, input not accepted" % (type(self).__name__))
            del kwargs["name"]
        if "is_optimized" in kwargs:
            self.is_optimized = True if kwargs["is_optimized"] else False
            del kwargs["is_optimized"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return super(EX2GKGPFitSettings, self).__repr__()

    def __copy__(self):
        return type(self)(self, name=self.name, is_optimized=self.is_optimized)

    def copy(self):
        return type(self)(self, name=self.name, is_optimized=self.is_optimized)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        match &= (self.is_optimized == other.is_optimized)
        if match:
            super(EX2GKGPFitSettings, self).__eq__(other)
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            if key in self.required_names:
                self[key.upper()] = value
                dellist.append(key)
            elif key == "FTYPE":
                dellist.append(key)
            elif key == "OPTIMIZED":
                self.is_optimized = True if value else False
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = super(EX2GKGPFitSettings, self).exportToDict()
        output["FTYPE"] = "GPR"
        output["OPTIMIZED"] = self.is_optimized
        return output


class EX2GKFitSettingsOrderedList(MutableMapping):

    def __init__(self, *args, **kwargs):
        self._internal_list = []
        if args:
            if len(args) > 1:
                raise TypeError("%s update expected at most 1 arguments, got %d" % (type(self).__name__, len(args)))
            self.extend(args[0])

    def __len__(self):
        return len(self._internal_list)

    def __iter__(self):
        return iter(self._internal_list)

    def __getitem__(self, index):
        return self._internal_list.__getitem__(index)

    def __setitem__(self, index, value):
        if not isinstance(value, EX2GKFitSettings):
            raise TypeError("%s entries must be a %s objects" % (type(self).__name__, EX2GKFitSettings.__name__))
        self._internal_list.__setitem__(index, copy.deepcopy(value))

    def __delitem__(self, index):
        self._internal_list.__delitem__(index)

    def append(self, value):
        if not isinstance(value, EX2GKFitSettings):
            raise TypeError("%s entries must be a %s objects" % (type(self).__name__, EX2GKFitSettings.__name__))
        self._internal_list.append(copy.deepcopy(value))

    def extend(self, value):
        for item in value:
            if not isinstance(item, EX2GKFitSettings):
                raise TypeError("%s entries must be a %s objects" % (type(self).__name__, EX2GKFitSettings.__name__))
        self._internal_list.extend(copy.deepcopy(value))

    def insert(self, index, value):
        if not isinstance(value, EX2GKFitSettings):
            raise TypeError("%s entries must be a %s objects" % (type(self).__name__, EX2GKFitSettings.__name__))
        self._internal_list.insert(index, copy.deepcopy(value))

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, self._internal_list.__repr__())

    def __copy__(self):
        return type(self)(copy.deepcopy(self._internal_list))

    def copy(self):
        return type(self)(copy.deepcopy(self._internal_list))

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for ii in range(0, len(self)):
                match &= (self[ii] == other[ii])
                if not match:
                    print("%s index %d mismatch" % (type(self).__name__, ii))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromList(cls, data_list, suppress_warnings=False):
        self = cls()
        if not isinstance(data_list, (list, tuple)):
            raise TypeError("%s data argument must be a Python list" % (type(self).importFromList.__name__))
        index_list = []
        for ii in range(0, len(data_list)):
            if isinstance(data_list[ii], dict):
                fobj = None
                if "FTYPE" in data_list[ii]:
                    if data_list[ii]["FTYPE"] == "POLY":
                        fobj = EX2GKPolyFitSettings.importFromDict(data_list[ii], True)
                    elif data_list[ii]["FTYPE"] == "GPR":
                        fobj = EX2GKGPFitSettings.importFromDict(data_list[ii], True)
                if fobj is not None:
                    self.append(fobj)
                else:
                    index_list.append(ii)
        if not suppress_warnings:
            for idx in range(0, len(index_list)):
                warnings.warn("Data index %d could not be imported by %s object" % (index_list[idx], type(self).__name__))
        return self

    def exportToList(self):
        output = []
        for ii in range(0, len(self._internal_list)):
             output.append(self._internal_list[ii].exportToDict())
        return output

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for ii in range(0, len(self._internal_list)):
            if self._internal_list[ii] is not None:
                key = "FS%d" % (ii)
                tempdict[key] = self._internal_list[ii].stripFromClass(with_descriptions=with_descriptions)
        return tempdict


class EX2GKFitSettingsContainer(dict):

    recommended_names = {"N": 'Particle density',
                         "T": 'Temperature',
                         "P": 'Pressure',
                         "AF": 'Flow angular frequency',
                         "U": 'Flow angular velocity',
                         "Q": 'Safety factor',
                         "IOTA": 'Rotational transform',
                         "SN": 'Particle source density',
                         "ST": 'Heat source density',
                         "SP": 'Momentum source density',
                         "J": 'Current density',
                         "W": 'Energy density',
                         "Z": 'Particle charge',
                         "other": 'Any additional quantity, use uppercase field names'}

    def __init__(self, *args, **kwargs):
        super(EX2GKFitSettingsContainer, self).__init__()
        self.present_names = {}
        self.update(*args, **kwargs)

    def info(self):
        return self.recommended_names

    def describe(self):
        descdict = {}
        for key in self:
            if isinstance(self[key], EX2GKFitSettingsOrderedList):
                descdict[key] = self[key].describe()
        return descdict

    def isReady(self, verbose=0):
        gflag = True if self else False
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def getPresentFields(self, style=None):
        otype = 0
        if isinstance(style, str):
            if re.match(r'^dict$', style, flags=re.IGNORECASE):
                otype = 0
            elif re.match(r'^list$', style, flags=re.IGNORECASE):
                otype = 1
            elif re.match(r'^str(ing)?$', style, flags=re.IGNORECASE):
                otype = 2
        output = None
        if otype == 0:
            output = copy.deepcopy(self.present_names)
        elif otype == 1:
            output = []
            for category in self.present_names:
                for ii in range(0, len(self.present_names[category])):
                    output.append(category+self.present_names[category][ii])
        elif otype == 2:
            output = ""
            for category in self.present_names:
                for ii in range(0, len(self.present_names[category])):
                    if output:
                        output = output + ', '
                    output = output + category + self.present_names[category][ii]
        return output

    def addFitSettingsDataObject(self, data_object, index=None):
        if not isinstance(data_object, EX2GKFitSettings):
            raise TypeError("%s settings object argument must be a %s object" % (type(self).addFitSettingsDataObject.__name__, EX2GKFitSettings.__name__))
        name = data_object.name.upper()
        if name not in self:
            self[name] = EX2GKFitSettingsOrderedList()
        if isinstance(index, number_types) and int(index) >= 0 and int(index) < len(self[name]):
            self[name].insert(int(index), data_object)
        else:
            self[name].append(data_object)

    def addFitSettingsListObject(self, quantity_name, data_list):
        if not isinstance(quantity_name, str):
            raise TypeError("%s quantity name argument must be a string" % (type(self).addFitSettingsListObject.__name__))
        if not isinstance(data_list, EX2GKFitSettingsOrderedList):
            raise TypeError("%s settings list argument must be a %s object" % (type(self).addFitSettingsListObject.__name__, EX2GKFitSettingsOrderedList.__name__))
        for item in data_list:
            item.setQuantityName(quantity_name)
        self[quantity_name.upper()] = data_list

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if not isinstance(value, EX2GKFitSettingsOrderedList) and value is not None:
            raise TypeError("%s entries must be a %s object" % (type(self).__name__, EX2GKFitSettingsOrderedList.__name__))
        category = None
        identifier = ""
        for tag in self.recommended_names:
            mm = re.search(r'^'+tag+r'([A-Z0-9]*)$', key.upper())
            if mm:
                category = tag
                identifier = mm.group(1).upper()
        if category is not None and isinstance(identifier, str):
            if category not in self.present_names:
                self.present_names[category] = []
            if identifier.upper() not in self.present_names[category]:
                self.present_names[category].append(identifier)
        else:
            warnings.warn("%s field name %s cannot be parsed for categorization, skipping categorization step" % (type(self).__name__, key))
        super(EX2GKFitSettingsContainer, self).__setitem__(key.upper(), copy.deepcopy(value))

    def update(self, *args, **kwargs):
        if "present_names" not in self.__dict__:
            self.present_names = {}
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        if "present_names" in kwargs:
            if isinstance(kwargs["present_names"], dict):
                self.present_names = copy.deepcopy(kwargs["present_names"])
            else:
                warnings.warn("%s present_names keyword argument must be a dictionary, input not accepted" % (type(self).__name__))
            del kwargs["present_names"]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKFitSettingsContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        match &= (self.present_names == other.present_names)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key in data_dict:
            if isinstance(data_dict[key], (list, tuple)):
                lobj = EX2GKFitSettingsOrderedList.importFromList(data_dict[key], True)
                self.addFitSettingsListObject(key.upper(), lobj)
                dellist.append(key)
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            output[key] = self[key].exportToList()
        return output

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = self[key].stripFromClass(with_descriptions=with_descriptions)
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKFlagContainer(dict):

    standard_names = {"NIGP": 'Specifies use of x-errors inside GPR fit routine',
                      "KEEPGPS": 'Specifies retention of original GPR1D settings lists',
                      "LINFI": 'Specifies use of linear interpolation for fast ion density and energy density',
                      "LINSRC": 'Specifies use of linear interpolation for heat, particle, momentum, current sources',
                      "TISCALE": 'Specifies scaling of T_imp profile to create T_i profile if only sparse data is available',
                      "other": 'Any additional flag, use uppercase field names'}

    def __init__(self, *args, **kwargs):
        super(EX2GKFlagContainer, self).__init__()
        self.update(*args, **kwargs)

    def info(self):
        return self.standard_names

    def describe(self):
        descdict = {}
        # Maybe implement a EX2GKFlag class to store descriptions with flag status?
        for key in self:
            descdict[key] = self.standard_names[key] if key in self.standard_names else ""
        return descdict

    def isReady(self, verbose=0):
        gflag = True
        if isinstance(verbose, int) and verbose > 0:
            if gflag:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return gflag

    def checkFlag(self, name):
        check = False if name in self else None
        if check is not None:
            check = self[name]
        return check

    def setFlag(self, name, value):
        if not isinstance(name, str):
            raise TypeError("%s name argument must be a string" % (type(self).setFlag.__name__))
        self[name] = True if value else False

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() == "OTHER":
            raise KeyError("Funny guy... Use another uppercase field name for additional entries to %s" % (type(self).__name__))
        val = True if value else False
        super(EX2GKFlagContainer, self).__setitem__(key.upper(), val)

    def update(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        return '{0}({1})'.format(type(self).__name__, super(EX2GKFlagContainer, self).__repr__())

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (type(self).importFromDict.__name__))
        dellist = []
        for key, value in data_dict.items():
            try:
                self[key.upper()] = value
                dellist.append(key)
            except TypeError:
                pass
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, type(self).__name__))
        return self

    def exportToDict(self):
        return copy.deepcopy(super(EX2GKFlagContainer, self).copy())

    @classmethod
    def importFromPandas(cls, panda):
        self = cls({key: bool(val) for key, val in panda.items()})
        return self

    def exportToPandas(self):
        return pd.Series(self)

    def stripFromClass(self, with_descriptions=False):
        tempdict = {}
        for key in self:
            if self[key] is not None:
                tempdict[key] = copy.deepcopy(self[key])
        if tempdict and with_descriptions:
            tempdict["DESC"] = self.describe()
        return tempdict


class EX2GKTimeWindow(dict):

    allowed_keys = {"META": 'Time window meta data',
                    "CD": 'Coordinate system data',
                    "ZD": 'Zero-dimensional data',
                    "RD": 'Raw one-dimensional experimental data',
                    "ED": 'Equilibrium reconstruction data',
                    "FSI": 'Initial fit settings',
                    "FSO": 'Optimized fit settings',
                    "PD": 'Fitted one-dimensional profile data',
                    "FLAG": 'Processing flags'}
    allowed_sources = {"OHM": 'Ohmic induction',
                       "NBI": 'Neutral beam injection',
                       "ICRH": 'Ion cyclotron resonance heating',
                       "ECRH": 'Electron cyclotron resonance heating',
                       "LH": 'Lower hybrid',
                       "RAD": 'Radiative loss'}

    def __init__(self, *args, **kwargs):
        super(EX2GKTimeWindow, self).__init__()
        self.update(*args, **kwargs)
        self._debug = False

    def describe(self):
        return self.allowed_keys

    def setDebugMode(self, state=True):
        self._debug = True if state else False
        mode = "" if self._debug else "de"
        print("%s - Debug mode %sactivated." % (type(self).__name__, mode))

    # Function to append - on pause, are they needed?
    def addMetaContainer(self, meta_object):
        if not isinstance(meta_object, EX2GKMetaContainer):
            raise TypeError("%s object argument must be an instance of the %s class" % (type(self).addMetaContainer.__name__, EX2GKMetaContainer.__name__))
        if "META" in self and isinstance(self["META"], EX2GKMetaContainer):
            warnings.warn("%s function will overwrite existing values if any conflicting entries are found" % (type(self).addMetaContainer.__name__))
            for key, value in meta_object.items():
                self["META"].addValue(key, value)
        else:
            self["META"] = meta_object
        if self._debug:
            print("%s - Metadata container added." % (type(self).__name__))

    def isReady(self, verbose=0):
        gmeta = "META" in self and self["META"].isReady(verbose=verbose)
        gcs = "CD" in self and self["CD"].isReady(verbose=verbose)
        gzd = "ZD" in self and self["ZD"].isReady(verbose=verbose)
        grd = "RD" in self and self["RD"].isReady(verbose=verbose)
#        ged = "ED" in self and self["ED"].isReady(verbose=verbose)
        gfsi = "FSI" in self and self["FSI"].isReady(verbose=verbose)
        gflag = "FLAG" in self and self["FLAG"].isReady(verbose=verbose)
        fready = gmeta and gcs and gzd and grd and gfsi and gflag
        if self._debug and verbose > 0:
            if fready:
                print("Ready check: %s is ready!" % (type(self).__name__))
            else:
                print("Ready check: %s is not ready." % (type(self).__name__))
        return fready

    def isFitted(self, verbose=0):
        gpd = "PD" in self and self["PD"].isReady(verbose=verbose)
        gfso = "FSO" in self and self["FSO"].isReady(verbose=verbose)
        gflag = "FLAG" in self and self["FLAG"].checkFlag("FITDONE")
        ffitted = gpd and gfso and gflag
        if self._debug and verbose > 0:
            if ffitted:
                print("Fitted check: %s is fitted!" % (type(self).__name__))
            else:
                print("Fitted check: %s is not fit." % (type(self).__name__))
        return ffitted

    def isProcessed(self, verbose=0):
        gcode = "META" in self and self["META"].isReady(verbose=verbose) and self["META"]["CODE"] is not None
        fprocessed = self.isFitted() and gcode
        if self._debug and verbose > 0:
            if fprocessed:
                print("Processed check: %s is processed!" % (type(self).__name__))
            else:
                print("Processed check: %s is not processed." % (type(self).__name__))
        return fprocessed

    def updateTimestamp(self):
        self["UPDATED"] = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')

    def _performLinearInterpolation(self, quantity_name, identifier1=None, identifier2=None, direct_derivative=False, verbose=0):
        result = None
        if "RD" in self and self["RD"].isReady():
            qtag = "INVALID NAME"
            itag = None
            rdtag = None
            if isinstance(quantity_name, str):
                temp = quantity_name.upper()
                if not isinstance(identifier1, str):
                    identifier1 = ""
                temp1 = temp+identifier1.upper()
                if not isinstance(identifier2, str):
                    identifier2 = ""
                temp2 = temp1+identifier2.upper()
                qtag = temp2
                itag = identifier1.upper()+identifier2.upper()
                if temp2 in self["RD"] and self["RD"][temp2].isRaw():
                    rdtag = temp2
                elif temp1 in self["RD"] and self["RD"][temp1].isRaw():
                    rdtag = temp1
                elif temp in self["RD"] and self["RD"][temp].isRaw():
                    rdtag = temp
            allowed_coords = self["CD"].coord_keys()

            if rdtag is not None:

                # Define raw data and output coordinate systems
                ocs = self["META"]["CSO"]
                preocs = self["META"]["CSOP"]
                xcs = ocs
                prexcs = preocs
                if self["RD"][rdtag].coord_prefix+self["RD"][rdtag].coordinate in allowed_coords:
                    xcs = self["RD"][rdtag].coordinate
                    prexcs = self["RD"][rdtag].coord_prefix
                (xn, xjac, xerr) = self["CD"].convert(self["PD"]["X"][""], ocs, xcs, preocs, prexcs)

                # Define data to be interpolated
                xx = self["RD"][rdtag]["X"]
                yy = self["RD"][rdtag][""]
                xe = self["RD"][rdtag]["XEB"] if self["FLAG"].checkFlag("NIGP") else None
                ye = self["RD"][rdtag]["EB"]
                nfilt = np.all([np.isfinite(xx), np.isfinite(yy)], axis=0)
                srcyfunc = None
                if np.count_nonzero(nfilt) > 1:
                    srcyfunc = interp1d(xx[nfilt], yy[nfilt], kind='linear', bounds_error=False, fill_value='extrapolate')

                # Define provenance info
                rdprov = self["RD"][rdtag]["PROV"] if "PROV" in self["RD"][rdtag] and self["RD"][rdtag]["PROV"] is not None else "EX2GK: RD_"+rdtag

                result = dict()
                result["FTYPE"] = 'linint'
                result["XRAW"] = xx
                result["YRAW"] = yy
                result["XVAL"] = xn
                result["XJAC"] = xjac
                result["XERR"] = xerr
                result["VAL"] = srcyfunc(xn) if srcyfunc is not None else np.full(xn.shape, np.nanmean(yy), dtype=np.float64)
                result["ERR"] = np.zeros(xn.shape, dtype=np.float64)
                dyvec = np.zeros(xn.shape, dtype=np.float64)
                dyerr = np.zeros(xn.shape, dtype=np.float64)
                if direct_derivative and "GRAD" in self["RD"][rdtag] and self["RD"][rdtag]["GRAD"] is not None:
                    dxx = self["RD"][rdtag]["XGRAD"]
                    dyy = self["RD"][rdtag]["GRAD"]
                    dye = self["RD"][rdtag]["GRADEB"]
                    dnfilt = np.all([np.isfinite(dxx), np.isfinite(dyy)], axis=0)
                    srcdyfunc = None
                    srcdyefunc = None
                    if np.count_nonzero(dnfilt) > 1:
                        srcdyfunc = interp1d(dxx[dnfilt], dyy[dnfilt], kind='linear', bounds_error=False, fill_value='extrapolate')
                        srcdyefunc = interp1d(dxx[dnfilt], dye[dnfilt], kind='linear', bounds_error=False, fill_value='extrapolate')
                    dyvec = srcdyfunc(xn) if srcdyfunc is not None else np.zeros(xn.shape, dtype=np.float64)
                    dyerr = srcdyefunc(xn) if srcdyefunc is not None else np.zeros(xn.shape, dtype=np.float64)
                else:
                    if xn.size > 1:
                        dyvec[0] = (result["VAL"][1] - result["VAL"][0]) / (xn[1] - xn[0])
                        dyvec[-1] = (result["VAL"][-1] - result["VAL"][-2]) / (xn[-1] - xn[-2])
                    if xn.size > 2:
                        dyvec[1:-1] = (result["VAL"][2:] - result["VAL"][:-2]) / (xn[2:] - xn[:-2])
                result["DVAL"] = dyvec
                result["DERR"] = dyerr
                result["PROV"] = "Linear interpolation of " + rdprov

            else:
                warnings.warn("%s object does not contain requested data for interpolating quantity: %s" ,type(self).__name__, qtag)

        else:
            warnings.warn("%s object is not ready for data interpolation procedure, aborted" % (type(self).__name__))

        return result

    def _performFit(self, quantity_name, identifier1=None, identifier2=None, swap_io=False, verbose=0):
        """
        Standardized fitting routine for 1D profiles using GPR1D.

        :arg xraw: array. Vector of radial points to be used in the fit routine.

        :arg ynew: array. Vector of electron density data to be used in the fit routine.

        :arg xnew: array. Vector of radial points on which the result of the fit routine will be evaluated at.

        :arg settings: dict. Dictionary containing desired GP kernels and settings to be used in fit routine.

        :kwarg xerr: array. Vector of error bars corresponding to the radial points, assumed to be expressed in 1 sigma.

        :kwarg yerr: array. Vector of error bars corresponding to the electron density data, assumed to be expressed in 1 sigma.

        :returns: obj. The GPR1D object containing the input data, fit parameters, and fit results.
        """
        result = None
        if self.isReady():
            qtag = "INVALID NAME"
            itag = None
            stag = "O" if swap_io else "I"
            rdtag = None
            fstag = None
            if isinstance(quantity_name, str):
                temp = quantity_name.upper()
                if not isinstance(identifier1, str):
                    identifier1 = ""
                temp1 = temp+identifier1.upper()
                if not isinstance(identifier2, str):
                    identifier2 = ""
                temp2 = temp1+identifier2.upper()
                qtag = temp2
                itag = identifier1.upper()+identifier2.upper()
                if temp2 in self["RD"] and self["RD"][temp2].isRaw():
                    rdtag = temp2
                elif temp1 in self["RD"] and self["RD"][temp1].isRaw():
                    rdtag = temp1
                elif temp in self["RD"] and self["RD"][temp].isRaw():
                    rdtag = temp
                if temp2 in self["FS"+stag]:
                    fstag = temp2
                elif temp1 in self["FS"+stag]:
                    fstag = temp1
                elif temp in self["FS"+stag]:
                    fstag = temp
            allowed_coords = self["CD"].coord_keys()

            if self._debug and verbose < 1:
                verbose = 1

            if rdtag is not None and fstag is not None:

                # Define raw data and output coordinate systems
                ocs = self["META"]["CSO"]
                preocs = self["META"]["CSOP"]
                xcs = ocs
                prexcs = preocs
                if self["RD"][rdtag].coord_prefix+self["RD"][rdtag].coordinate in allowed_coords:
                    xcs = self["RD"][rdtag].coordinate
                    prexcs = self["RD"][rdtag].coord_prefix
                (xn, xjac, xerr) = self["CD"].convert(self["PD"]["X"][""], ocs, xcs, preocs, prexcs)
                if prexcs+xcs != preocs+ocs:
                    (tx, xjac, xerr) = self["CD"].convert(xn, xcs, ocs, prexcs, preocs)

                # Define data to be fitted
                xx = self["RD"][rdtag]["X"]
                yy = self["RD"][rdtag][""]
                xe = self["RD"][rdtag]["XEB"] if self["FLAG"].checkFlag("NIGP") else None
                ye = self["RD"][rdtag]["EB"]

                # Define derivative constraints, if provided in proper format
                dxx = self["RD"][rdtag]["XGRAD"]
                dyy = self["RD"][rdtag]["GRAD"]
                dxe = self["RD"][rdtag]["XEBGRAD"] if self["FLAG"].checkFlag("NIGP") else None
                dye = self["RD"][rdtag]["GRADEB"]

                # Define provenance info
                rdprov = self["RD"][rdtag]["PROV"] if "PROV" in self["RD"][rdtag] and self["RD"][rdtag]["PROV"] is not None else "EX2GK: RD_"+rdtag

                ii = 0
                while result is None and ii < len(self["FS"+stag][fstag]):

                    if isinstance(self["FS"+stag][fstag][ii], EX2GKPolyFitSettings):

                        # Define fit kernel settings, using defaults if not provided
                        pdeg = self["FS"+stag][fstag][ii]["DEGREE"] if self["FS"+stag][fstag][ii]["DEGREE"] is not None else 5
                        xanch = self["FS"+stag][fstag][ii]["ANCHORX"]
                        yanch = self["FS"+stag][fstag][ii]["ANCHORY"]
                        focst = self["FS"+stag][fstag][ii]["OMIT_CST"]
                        folin = self["FS"+stag][fstag][ii]["OMIT_LIN"]

                        try:
                            coeffs = ptools.anchored_polyfit(degree=pdeg, xdata=xx, ydata=yy, xanchors=xanch, yanchors=yanch, omit_const=focst, omit_linear=folin)
                            yvec = np.zeros(xx.shape, dtype=np.float64)
                            vvec = np.zeros(xn.shape, dtype=np.float64)
                            dvec = np.zeros(xn.shape, dtype=np.float64)
                            for ii in range(0, coeffs.size):
                                vvec = vvec + coeffs[ii] * np.power(xn, ii)
                                yvec = yvec + coeffs[ii] * np.power(xx, ii)
                                if ii > 0:
                                    dvec = dvec + coeffs[ii] * np.power(xn, ii-1)
                            npars = coeffs.size - 2 if folin else coeffs.size - 1
                            myy = np.nanmean(yy)
                            sstot = np.sum(np.power(yy - myy, 2.0))
                            ssres = np.sum(np.power(yy - yvec, 2.0))
                            adjr2 = 1.0 - (ssres / sstot) * (xx.size - 1.0) / (xx.size - npars - 1.0)
                            if verbose > 0:
                                print("Successful attempt with option %d, polynomial fit of order %d." % (ii, pdeg))

                            result = dict()
                            result["FTYPE"] = 'poly'
                            result["DEGREE"] = pdeg
                            result["XANCH"] = copy.deepcopy(xanch)
                            result["YANCH"] = copy.deepcopy(yanch)
                            result["FLAG_OCST"] = focst
                            result["FLAG_OLIN"] = folin
                            result["OCOEFFS"] = coeffs
                            result["RADJ"] = adjr2
                            result["XRAW"] = xx
                            result["YRAW"] = yy
                            result["XVAL"] = xn
                            result["XJAC"] = xjac
                            result["XERR"] = xerr
                            result["VAL"] = vvec
                            result["ERR"] = np.zeros(vvec.shape, dtype=np.float64)
                            result["DVAL"] = dvec * xjac
                            result["DERR"] = np.zeros(dvec.shape, dtype=np.float64)
                            result["PROV"] = "Polynomial fit of " + rdprov
                        except Exception as e:
                            result = None
                            if verbose > 0:
                                print("Attempt with option %d, polynomial fit of order %d, failed. Trying next option..." % (ii, pdeg))
                            if verbose > 1:
                                print(repr(e))

                    elif isinstance(self["FS"+stag][fstag][ii], EX2GKGPFitSettings):

                        # Define fit kernel settings, using defaults if not provided
                        kname = self["FS"+stag][fstag][ii]["FIT_KNAME"]       # Mandatory field, checked at function entry
                        kpars = self["FS"+stag][fstag][ii]["FIT_KPARS"]   
                        kbounds = self["FS"+stag][fstag][ii]["FIT_KBOUNDS"]   # If not provided, MC kernel restarts automatically switched off
                        regpar = self["FS"+stag][fstag][ii]["FIT_REGPAR"]
                        if regpar is None:
                            regpar = 1.0
                        oname = self["FS"+stag][fstag][ii]["FIT_ONAME"]       # Default is vanilla gradient ascent search, slow but robust
                        opars = self["FS"+stag][fstag][ii]["FIT_OPARS"]
                        eps = self["FS"+stag][fstag][ii]["FIT_EPSPAR"]        # Default is to not perform hyperparameter optimization

                        # Define error kernel settings, using defaults if not provided
                        nkname = self["FS"+stag][fstag][ii]["ERR_KNAME"]      # If not provided, output error assumed constant equal to average of input errors
                        nkpars = self["FS"+stag][fstag][ii]["ERR_KPARS"]
                        nkbounds = self["FS"+stag][fstag][ii]["ERR_KBOUNDS"]  # If not provided, MC kernel restarts automatically switched off
                        nregpar = self["FS"+stag][fstag][ii]["ERR_REGPAR"]
                        if nregpar is None:
                            nregpar = 10.0
                        noname = self["FS"+stag][fstag][ii]["ERR_ONAME"]      # Default is vanilla gradient ascent search, slow but robust
                        nopars = self["FS"+stag][fstag][ii]["ERR_OPARS"]
                        neps = self["FS"+stag][fstag][ii]["ERR_EPSPAR"]       # Default is to not perform hyperparameter optimization

                        knres = self["FS"+stag][fstag][ii]["FIT_RESTART"]
                        nknres = self["FS"+stag][fstag][ii]["ERR_RESTART"]

                        # Construct kernels
                        kernel = ptools.gp.KernelReconstructor(kname, kpars)
                        nkernel = ptools.gp.KernelReconstructor(nkname, nkpars) if nkname is not None else None
                        fhsgp = True if nkernel is not None else False

                        try:
                            gprobj = ptools.gp.GaussianProcessRegression1D()
                            gprobj.set_kernel(kernel=kernel, kbounds=kbounds, regpar=regpar)
                            gprobj.set_error_kernel(kernel=nkernel, kbounds=nkbounds, regpar=nregpar, nrestarts=nknres)
                            gprobj.set_raw_data(xdata=xx, ydata=yy, yerr=ye, xerr=xe, dxdata=dxx, dydata=dyy, dyerr=dye)
                            gprobj.set_search_parameters(epsilon=eps, method=oname, spars=opars)
                            gprobj.set_error_search_parameters(epsilon=neps, method=noname, spars=nopars)
                            gprobj.GPRFit(xn, hsgp_flag=fhsgp, nrestarts=knres)
                            if verbose > 0:
                                print("Successful attempt with option %d, GPR fit using %s kernel." % (ii, kname))

                            result = dict()
                            result["FTYPE"] = 'gpr'
                            (result["OKNAME"], result["OKPARS"], result["OKRPAR"]) = gprobj.get_gp_kernel_details()
                            (result["ONKNAME"], result["ONKPARS"], result["ONKRPAR"]) = gprobj.get_gp_error_kernel_details()
                            result["RADJ"] = gprobj.get_gp_adjusted_r2()
                            result["RGEN"] = gprobj.get_gp_generalized_r2()
                            (result["XRAW"], result["YRAW"]) = itemgetter(0,1)(gprobj.get_processed_data())
                            result["XVAL"] = xn
                            result["XJAC"] = xjac
                            result["XERR"] = xerr
                            (result["VAL"], result["ERR"], result["DVAL"], result["DERR"]) = gprobj.get_gp_results()
                            result["DVAL"] = result["DVAL"] * xjac
                            result["DERR"] = result["DERR"] * xjac
                            result["PROV"] = "GPR fit of " + rdprov
                        except Exception as e:
                            result = None
                            if verbose > 0:
                                print("Attempt with option %d, GPR fit using %s kernel, failed. Trying next option..." % (ii, kname))
                            if verbose > 1:
                                print(repr(e))

                    if result is None:
                        ii = ii + 1


            else:
                warnings.warn("%s object does not contain requested data for fitting quantity: %s", type(self).__name__, qtag)

        else:
            warnings.warn("%s object is not ready for data fitting procedure, aborted" % (type(self).__name__))

        return result

    def _saveOptimizedFitSettings(self, fit_result, quantity_name):
        status = False
        if isinstance(fit_result, dict) and "FTYPE" in fit_result:
            osobj = None
            if "FSO" not in self:
                self["FSO"] = EX2GKFitSettingsContainer()

            if fit_result["FTYPE"] == 'gpr':
                osobj = EX2GKGPFitSettings()
                osobj.setQuantityName(quantity_name)
                osobj.defineFitKernelSettings(fit_result["OKNAME"], fit_result["OKPARS"], fit_result["OKRPAR"], None, None, None, None, None)
                osobj.defineErrorKernelSettings(fit_result["ONKNAME"], fit_result["ONKPARS"], fit_result["ONKRPAR"], None, None, None, None, None)
                osobj.setFitMetrics(fit_result["RADJ"], fit_result["RGEN"], True)

            elif fit_result["FTYPE"] == 'poly':
                osobj = EX2GKPolyFitSettings()
                osobj.setQuantityName(quantity_name)
                osobj.definePolyFitSettings(fit_result["DEGREE"], fit_result["XANCH"], fit_result["YANCH"], fit_result["FLAG_OCST"], fit_result["FLAG_OLIN"])
                osobj.setCoefficients(fit_result["OCOEFFS"], fit_result["RADJ"], True)

            if isinstance(osobj, EX2GKFitSettings):
                self["FSO"].addFitSettingsDataObject(osobj)
                status = True

                if self._debug:
                    print("%s - Fit settings for %s saved" % (type(self).__name__, quantity_name))

        return status


    def fitPrimaryProfiles(self, out_grid, out_coordinate=None, out_coordinate_prefix=None):
        """
        Performs the general profile fits typically used as the
        inputs for various analysis codes. This function assumes
        that the output will be in toridal rho radial coordinate
        system, although the Jacobians are provided to convert
        everything to any other system if necessary.

        :arg rhogrid: array. Vector of radial points on which the parameters are calculated.

        :arg basedata: dict. Object containing standardized data fields for performing necessary fits and calculations.

        :returns: dict. Object with identical structure as input object except with fitted profiles inserted in a standardized format.
        """
        if self._debug:
            print("Entered %s..." % (type(self).fitPrimaryProfiles.__name__))

        newgrid = None
        if isinstance(out_grid, array_types):
            newgrid = np.array(out_grid).flatten()
        if newgrid is not None:
            newgrid = newgrid[np.isfinite(newgrid)]
        else:
            raise TypeError("Invalid fit vector passed to %s, crash prevents nonsensical processing" % (type(self).fitPrimaryProfiles.__name__))

        verbose = 1 if self._debug else 0
        if newgrid is not None and newgrid.size > 0 and self.isReady(verbose=verbose):

            if "PD" not in self:
                self["PD"] = EX2GKProfileDataContainer()

            rddict = self["RD"].getPresentFields('dict')
            fsidict = self["FSI"].getPresentFields('dict')
            fsodict = self["FSO"].getPresentFields('dict') if "FSO" in self and self["FSO"].isReady() else None

            refshape = newgrid.shape

            # If no coordinate system given, assume it is rhotor
            csout = itemgetter(0)(ptools.define_coordinate_system(out_coordinate))
            prefix_list = self["CD"].getPrefixes()
            cspre = out_coordinate_prefix if out_coordinate_prefix in prefix_list else prefix_list[0]
            self["META"].setFittingMetaData(csout, fit_vector=newgrid, fit_coord_prefix=cspre)
            newgrid_error = np.zeros(refshape, dtype=np.float64)
            prov = "EX2GK: User-defined"
            self["PD"].addProfileData("X", newgrid, newgrid_error, x_coord=csout, x_coord_prefix=cspre, provenance=prov)
            self["PD"].addProfileDerivativeData("X", np.ones(refshape), np.zeros(refshape))
            cspre = self["META"]["CSOP"]

            if self._debug:
                print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, "X"))

            # Supply flux surface volume coordinate for integrated profiles
            (volgrid, voljac, volgrid_error) = self["CD"].convert(self["PD"]["X"][""], csout, "FSVOL", cspre, cspre)
            prov = "EX2GK: PD_X; EX2GK: CD_"+cspre+"FSVOL"
            self["PD"].addProfileData("V", volgrid, volgrid_error, provenance=prov)
            self["PD"].addProfileDerivativeData("V", voljac, np.zeros(volgrid.shape))

            if self._debug:
                print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, "V"))

            # Data containers for pedestal detection and estimations
            fped = None
            nept = np.NaN
            tept = np.NaN
            ftflag = False if self["META"].getTimeWindowPhase() != 0 else True

            # Fits electron density data, if present
            quantity_name = "N"
            qi1 = "E"
            qi2 = ""
            if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                verbose = 1 if self._debug else 0
                gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True, verbose=verbose)
                if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                    if self._debug:
                        print("%s - Linear interpolation performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (gout["VAL"] <= 0.0)
                    rfilt = (gout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        nelim = 1.0e16
                        nefmin = 0.0
                        nermin = 0.0
                        if not np.all(zfilt):
                            nelim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                            nefmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            nermin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                        nemin = np.nanmax([nefmin, nermin, nelim])
                        gout["VAL"][zfilt] = nemin
#                        gout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

            elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                fswap = True if oflag and not iflag else False

                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                    if self._debug:
                        print("%s - Curve fitting performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    # Records pedestal information, if found (work in progress, but more accurate here than in electron temperature)
                    if "OKNAME" in fout and re.search(r'^GwIG', fout["OKNAME"]):
                        nept = float(fout["OKPARS"][4] - 2.0 * fout["OKPARS"][3])

                    zfilt = (fout["VAL"] <= 0.0)
                    rfilt = (fout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        nelim = 1.0e16
                        nefmin = 0.0
                        nermin = 0.0
                        if not np.all(zfilt):
                            nelim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                            nefmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            nermin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                        nemin = np.nanmax([nefmin, nermin, nelim])
                        fout["VAL"][zfilt] = nemin
#                        fout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                else:
                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))

            # Fits electron temperature data, if present
            quantity_name = "T"
            qi1 = "E"
            qi2 = ""
            if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                    if self._debug:
                        print("%s - Linear interpolation performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (gout["VAL"] <= 0.0)
                    rfilt = (gout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        telim = 1.0e1
                        tefmin = 0.0
                        termin = 0.0
                        if not np.all(zfilt):
                            telim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                            tefmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            termin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                        temin = np.nanmax([tefmin, termin, telim])
                        gout["VAL"][zfilt] = temin
#                        gout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

            elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                fswap = True if oflag and not iflag else False

                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                    if self._debug:
                        print("%s - Curve fitting performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    # Records pedestal information, if found (work in progress)
                    if "OKNAME" in fout and re.search(r'^GwIG', fout["OKNAME"]):
                        tept = float(fout["OKPARS"][4] - 2.0 * fout["OKPARS"][3])

                    zfilt = (fout["VAL"] <= 0.0)
                    rfilt = (fout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        telim = 1.0e1
                        tefmin = 0.0
                        termin = 0.0
                        if not np.all(zfilt):
                            telim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                            tefmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            termin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                        temin = np.nanmax([tefmin, termin, telim])
                        fout["VAL"][zfilt] = temin
#                        fout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                else:
                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))


            # Sets pedestal flag, if found, for use in flagging decisions in future processing. Only applied with GP.
            if np.isfinite(nept) or np.isfinite(tept):
                ptloc = np.nanmin([nept, tept])
                self["ZD"].addData("PEDTOP", ptloc)

            # Fits angular frequency data, if present, and calculates toroidal velocity profile on the outer half of the magnetic axis
            quantity_name = "AF"
            qi1 = "TOR"
            qi2 = ""
            if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                    if self._debug:
                        print("%s - Linear interpolation performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    (rv, rj, rer) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORO", cspre, cspre)

                    nq = "U"
                    nqi = "TOR"
                    if rv is not None:
                        cout = dict()
                        cout["VAL"] = gout["VAL"] * rv
                        cout["ERR"] = np.abs(gout["ERR"] * rv)
#                        cout["ERR"] = np.sqrt(np.power(gout["ERR"] * rv, 2.0) + np.power(gout["VAL"] * rer, 2.0))
                        zfilt = (np.abs(rj) > 1.0e-10)
                        drv = np.zeros(rv.shape)
                        if np.any(zfilt):
                            drv[zfilt] = 1.0 / rj[zfilt]
                        cout["DVAL"] = gout["DVAL"] * rv + gout["VAL"] * drv
                        cout["DERR"] = np.sqrt(np.power(gout["DERR"] * rv, 2.0) + np.power(gout["ERR"] * drv, 2.0))
                        cout["PROV"] = "EX2GK: PD_"+quantity_name+qi1+qi2+", EX2GK: CD_"+cspre+"RMAJORO"

                        self["PD"].addProfileData(nq+nqi, cout["VAL"], cout["ERR"], provenance=cout["PROV"])
                        self["PD"].addProfileDerivativeData(nq+nqi, cout["DVAL"], cout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, nq+nqi))

                    else:
                        warnings.warn("%s calculation using fitted AFTOR was unsuccessful due to coordinate system error" % (nq+nqi))

            elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                fswap = True if oflag and not iflag else False

                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                    if self._debug:
                        print("%s - Curve fitting performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    (rv, rj, rer) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORO", cspre, cspre)

                    nq = "U"
                    nqi = "TOR"
                    if rv is not None:
                        cout = dict()
                        cout["VAL"] = fout["VAL"] * rv
                        cout["ERR"] = np.abs(fout["ERR"] * rv)
#                        cout["ERR"] = np.sqrt(np.power(fout["ERR"] * rv, 2.0) + np.power(fout["VAL"] * rer, 2.0))
                        zfilt = (np.abs(rj) > 1.0e-10)
                        drv = np.zeros(rv.shape)
                        if np.any(zfilt):
                            drv[zfilt] = 1.0 / rj[zfilt]
                        cout["DVAL"] = fout["DVAL"] * rv + fout["VAL"] * drv
                        cout["DERR"] = np.sqrt(np.power(fout["DERR"] * rv, 2.0) + np.power(fout["ERR"] * drv, 2.0))
                        cout["PROV"] = "EX2GK: PD_"+quantity_name+qi1+qi2+", EX2GK: CD_"+cspre+"RMAJORO"

                        self["PD"].addProfileData(nq+nqi, cout["VAL"], cout["ERR"], provenance=cout["PROV"])
                        self["PD"].addProfileDerivativeData(nq+nqi, cout["DVAL"], cout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, nq+nqi))

                    else:
                        warnings.warn("%s calculation using fitted AFTOR was unsuccessful due to coordinate system error" % (nq+nqi))

                else:
                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))


            # Fits the safety factor data, if present
            quantity_name = "IOTA"
            quantity_name_backup = "Q"
            qi2 = ""
            for ii in range(0, len(prefix_list)):
                qi1 = prefix_list[ii]
                if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (gout["VAL"] >= 10.0)
                        rfilt = (gout["YRAW"] >= 10.0)
                        if np.any(zfilt):
                            ilim = 10.0
                            ifmin = np.NaN
                            irmin = np.NaN
                            if not np.all(zfilt):
                                ifmin = np.nanmax(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                irmin = np.nanmax(gout["YRAW"][np.invert(rfilt)])
                            imin = np.nanmin([ifmin, irmin, ilim])
                            gout["VAL"][zfilt] = imin

                        # Adjust iota profile fit between separatrix and psi_norm = 0.95, such that separatrix iota is consistent with plasma current
                        if "USEQEDGE" in self["FLAG"] and self["FLAG"]["USEQEDGE"] and "QEDGE" in self["ZD"] and self["ZD"]["QEDGE"][""] is not None:
                            psinlim = 0.95
                            (rholim, dj, de) = self["CD"].convert(np.array([psinlim]), "PSIPOLN", self["META"]["CSO"], self["META"]["CSOP"], self["META"]["CSOP"])
                            iotafunc = interp1d(self["PD"]["X"][""], gout["VAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            iotab = iotafunc(rholim)
                            diotafunc = interp1d(self["PD"]["X"][""], gout["DVAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            diotab = diotafunc(rholim)
                            ddiotab = (diotafunc(rholim + 0.005) - diotafunc(rholim - 0.005)) / 0.001
                            imask = (self["PD"]["X"][""] > float(rholim))
                            qedge = self["ZD"]["QEDGE"][""]
                            qb = 1.0 / iotab
                            dqb = -diotab / np.power(iotab, 2.0)
                            ddqb = (-ddiotab + 2.0 * np.power(diotab, 2.0) / iotab) / np.power(iotab, 2.0)
                            newrho = self["PD"]["X"][""] - rholim
                            rhoreg = 1.0 - rholim
                            afac = (qedge - 0.5 * ddqb * np.power(rhoreg, 2.0) - dqb * rhoreg - qb) / np.power(rhoreg, 3.0)
                            qvec = np.power(newrho, 3.0) * afac + np.power(newrho, 2.0) * 0.5 * ddqb + newrho * dqb + qb
                            dqvec = 3.0 * np.power(newrho, 2.0) * afac + newrho * ddqb + dqb
                            gout["VAL"][imask] = 1.0 / qvec[imask]
                            gout["DVAL"][imask] = dqvec[imask] / np.power(qvec[imask], 2.0)

                            if self._debug:
                                print("%s - Applied edge modification to %s variable." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                        self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                        cout = dict()
                        cout["VAL"] = 1.0 / gout["VAL"]
                        cout["ERR"] = gout["ERR"] / np.power(gout["VAL"], 2.0)
                        cout["DVAL"] = -gout["DVAL"] / np.power(gout["VAL"], 2.0)
                        cout["DERR"] = np.sqrt(np.power(gout["DERR"] / gout["VAL"], 2.0) + np.power(2.0 * gout["DVAL"] * gout["ERR"] / gout["VAL"], 2.0)) / np.power(gout["VAL"], 2.0)
                        cout["PROV"] = "EX2GK: PD_"+quantity_name+qi1+qi2

                        self["PD"].addProfileData(quantity_name_backup+qi1+qi2, cout["VAL"], cout["ERR"], provenance=cout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name_backup+qi1+qi2, cout["DVAL"], cout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                    iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                    oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                    fswap = True if oflag and not iflag else False

                    fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                    if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                        self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                        if self._debug:
                            print("%s - Curve fitting performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (fout["VAL"] >= 10.0)
                        rfilt = (fout["YRAW"] >= 10.0)
                        if np.any(zfilt):
                            ilim = 10.0
                            ifmin = np.NaN
                            irmin = np.NaN
                            if not np.all(zfilt):
                                ifmin = np.nanmax(fout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                irmin = np.nanmax(fout["YRAW"][np.invert(rfilt)])
                            imin = np.nanmin([ifmin, irmin, ilim])
                            fout["VAL"][zfilt] = imin

                        # Adjust iota profile fit between separatrix and psi_norm = 0.95, such that separatrix iota is consistent with plasma current
                        if "USEQEDGE" in self["FLAG"] and self["FLAG"]["USEQEDGE"] and "QEDGE" in self["ZD"] and self["ZD"]["QEDGE"][""] is not None:
                            psinlim = 0.95
                            (rholim, dj, de) = self["CD"].convert(np.array([psinlim]), "PSIPOLN", self["META"]["CSO"], self["META"]["CSOP"], self["META"]["CSOP"])
                            iotafunc = interp1d(self["PD"]["X"][""], fout["VAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            iotab = iotafunc(rholim)
                            diotafunc = interp1d(self["PD"]["X"][""], fout["DVAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            diotab = diotafunc(rholim)
                            ddiotab = (diotafunc(rholim + 0.005) - diotafunc(rholim - 0.005)) / 0.001
                            imask = (self["PD"]["X"][""] > float(rholim))
                            qedge = self["ZD"]["QEDGE"][""]
                            qb = 1.0 / iotab
                            dqb = -diotab / np.power(iotab, 2.0)
                            ddqb = (-ddiotab + 2.0 * np.power(diotab, 2.0) / iotab) / np.power(iotab, 2.0)
                            newrho = self["PD"]["X"][""] - rholim
                            rhoreg = 1.0 - rholim
                            afac = (qedge - 0.5 * ddqb * np.power(rhoreg, 2.0) - dqb * rhoreg - qb) / np.power(rhoreg, 3.0)
                            qvec = np.power(newrho, 3.0) * afac + np.power(newrho, 2.0) * 0.5 * ddqb + newrho * dqb + qb
                            dqvec = 3.0 * np.power(newrho, 2.0) * afac + newrho * ddqb + dqb
                            fout["VAL"][imask] = 1.0 / qvec[imask]
                            fout["DVAL"][imask] = -dqvec[imask] / np.power(qvec[imask], 2.0)

                            if self._debug:
                                print("%s - Applied edge modification to %s variable." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                        self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                        cout = dict()
                        cout["VAL"] = 1.0 / fout["VAL"]
                        cout["ERR"] = fout["ERR"] / np.power(fout["VAL"], 2.0)
                        cout["DVAL"] = -fout["DVAL"] / np.power(fout["VAL"], 2.0)
                        cout["DERR"] = np.sqrt(np.power(fout["DERR"] / fout["VAL"], 2.0) + np.power(2.0 * fout["DVAL"] * fout["ERR"] / fout["VAL"], 2.0)) / np.power(fout["VAL"], 2.0)
                        cout["PROV"] = "EX2GK: PD_"+quantity_name+qi1+qi2

                        self["PD"].addProfileData(quantity_name_backup+qi1+qi2, cout["VAL"], cout["ERR"], provenance=cout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name_backup+qi1+qi2, cout["DVAL"], cout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                    else:
                        warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry." % (quantity_name+qi1+qi2))

                elif quantity_name_backup in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name_backup, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                        zfilt = (gout["VAL"] <= 0.1)
                        rfilt = (gout["YRAW"] <= 0.1)
                        if np.any(zfilt):
                            ilim = 10.0
                            ifmin = np.NaN
                            irmin = np.NaN
                            if not np.all(zfilt):
                                ifmin = np.nanmax(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                irmin = np.nanmax(gout["YRAW"][np.invert(rfilt)])
                            imin = np.nanmin([ifmin, irmin, ilim])
                            gout["VAL"][zfilt] = imin

                        # Adjust q profile fit between separatrix and psi_norm = 0.95, such that separatrix q is consistent with plasma current
                        # Note: This modification is equivalent to the iota modification, i.e. the inverse of the quadratic modification function below
                        if "USEQEDGE" in self["FLAG"] and self["FLAG"]["USEQEDGE"] and "QEDGE" in self["ZD"] and self["ZD"]["QEDGE"][""] is not None:
                            psinlim = 0.95
                            (rholim, dj, de) = self["CD"].convert(np.array([psinlim]), "PSIPOLN", self["META"]["CSO"], self["META"]["CSOP"], self["META"]["CSOP"])
                            qfunc = interp1d(self["PD"]["X"][""], gout["VAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            qb = qfunc(rholim)
                            dqfunc = interp1d(self["PD"]["X"][""], gout["DVAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            dqb = dqfunc(rholim)
                            ddqb = (dqfunc(rholim + 0.005) - dqfunc(rholim - 0.005)) / 0.001
                            qmask = (self["PD"]["X"][""] > float(rholim))
                            qedge = self["ZD"]["QEDGE"][""]
                            newrho = self["PD"]["X"][""] - rholim
                            rhoreg = 1.0 - rholim
                            afac = (qedge - 0.5 * ddqb * np.power(rhoreg, 2.0) - dqb * rhoreg - qb) / np.power(rhoreg, 3.0)
                            gout["VAL"][qmask] = np.power(newrho[qmask], 3.0) * afac + np.power(newrho[qmask], 2.0) * 0.5 * ddqb + newrho[qmask] * dqb + qb
                            gout["DVAL"][qmask] = 3.0 * np.power(newrho[qmask], 2.0) * afac + newrho[qmask] * ddqb + dqb

                            if self._debug:
                                print("%s - Applied edge modification to %s variable." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                        self["PD"].addProfileData(quantity_name_backup+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name_backup+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                        cout = dict()
                        cout["VAL"] = 1.0 / gout["VAL"]
                        cout["ERR"] = gout["ERR"] / np.power(gout["VAL"], 2.0)
                        cout["DVAL"] = -gout["DVAL"] / np.power(gout["VAL"], 2.0)
                        cout["DERR"] = np.sqrt(np.power(gout["DERR"] / gout["VAL"], 2.0) + np.power(2.0 * gout["DVAL"] * gout["ERR"] / gout["VAL"], 2.0)) / np.power(gout["VAL"], 2.0)
                        cout["PROV"] = "EX2GK: PD_"+quantity_name+qi1+qi2

                        self["PD"].addProfileData(quantity_name+qi1+qi2, cout["VAL"], cout["ERR"], provenance=cout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, cout["DVAL"], cout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                elif quantity_name_backup in rddict and qi1+qi2 in rddict[quantity_name_backup]:

                    iflag = quantity_name_backup in fsidict and (qi1 in fsidict[quantity_name_backup] or qi1+qi2 in fsidict[quantity_name_backup])
                    oflag = isinstance(fsodict, dict) and quantity_name_backup in fsodict and (qi1 in fsodict[quantity_name_backup] or qi1+qi2 in fsodict[quantity_name_backup])
                    fswap = True if oflag and not iflag else False

                    fout = self._performFit(quantity_name_backup, qi1, qi2, swap_io=fswap)
                    if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                        self._saveOptimizedFitSettings(fout, quantity_name_backup+qi1+qi2)

                        if self._debug:
                            print("%s - Curve fitting performed on %s data." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                        zfilt = (fout["VAL"] <= 0.1)
                        rfilt = (fout["YRAW"] <= 0.1)
                        if np.any(zfilt):
                            qlim = 0.1
                            qfmin = 0.0
                            qrmin = 0.0
                            if not np.all(zfilt):
                                qfmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                qrmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                            qmin = np.nanmax([qfmin, qrmin, qlim])
                            fout["VAL"][zfilt] = qmin

                        # Adjust q profile fit between separatrix and psi_norm = 0.95, such that separatrix q is consistent with plasma current
                        # Note: This modification is equivalent to the iota modification, i.e. the inverse of the quadratic modification function below
                        if "USEQEDGE" in self["FLAG"] and self["FLAG"]["USEQEDGE"] and "QEDGE" in self["ZD"] and self["ZD"]["QEDGE"][""] is not None:
                            psinlim = 0.95
                            (rholim, dj, de) = self["CD"].convert(np.array([psinlim]), "PSIPOLN", self["META"]["CSO"], self["META"]["CSOP"], self["META"]["CSOP"])
                            qfunc = interp1d(self["PD"]["X"][""], fout["VAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            qb = qfunc(rholim)
                            dqfunc = interp1d(self["PD"]["X"][""], fout["DVAL"], kind='linear', bounds_error=False, fill_value='extrapolate')
                            dqb = dqfunc(rholim)
                            ddqb = (dqfunc(rholim + 0.005) - dqfunc(rholim - 0.005)) / 0.001
                            qmask = (self["PD"]["X"][""] > float(rholim))
                            qedge = self["ZD"]["QEDGE"][""]
                            newrho = self["PD"]["X"][""] - rholim
                            rhoreg = 1.0 - rholim
                            afac = (qedge - 0.5 * ddqb * np.power(rhoreg, 2.0) - dqb * rhoreg - qb) / np.power(rhoreg, 3.0)
                            fout["VAL"][qmask] = np.power(newrho[qmask], 3.0) * afac + np.power(newrho[qmask], 2.0) * 0.5 * ddqb + newrho[qmask] * dqb + qb
                            fout["DVAL"][qmask] = 3.0 * np.power(newrho[qmask], 2.0) * afac + newrho[qmask] * ddqb + dqb

                            if self._debug:
                                print("%s - Applied edge modification to %s variable." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                        self["PD"].addProfileData(quantity_name_backup+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name_backup+qi1+qi2, fout["DVAL"], fout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name_backup+qi1+qi2))

                        cout = dict()
                        cout["VAL"] = 1.0 / fout["VAL"]
                        cout["ERR"] = fout["ERR"] / np.power(fout["VAL"], 2.0)
                        cout["DVAL"] = -fout["DVAL"] / np.power(fout["VAL"], 2.0)
                        cout["DERR"] = np.sqrt(np.power(fout["DERR"] / fout["VAL"], 2.0) + np.power(2.0 * fout["DVAL"] * fout["ERR"] / fout["VAL"], 2.0)) / np.power(fout["VAL"], 2.0)
                        cout["PROV"] = "EX2GK: PD_"+quantity_name+qi1+qi2

                        self["PD"].addProfileData(quantity_name+qi1+qi2, cout["VAL"], cout["ERR"], provenance=cout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, cout["DVAL"], cout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitPrimaryProfiles.__name__, quantity_name+qi1+qi2))

                    else:
                        warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry." % (quantity_name_backup+qi1+qi2))

            self["FLAG"].setFlag("FITDONE", True)

        else:
            warnings.warn("%s object is not ready for fitting procedure on primary profiles" % (type(self).__name__))

    def fitSecondaryProfiles(self):
        """
        """
        if self._debug:
            print("Entered %s..." % (type(self).fitSecondaryProfiles.__name__))

        verbose = 1 if self._debug else 0
        if self.isReady(verbose=verbose) and "PD" in self and "X" in self["PD"]:

            rddict = self["RD"].getPresentFields('dict')
            fsidict = self["FSI"].getPresentFields('dict')
            fsodict = self["FSO"].getPresentFields('dict') if "FSO" in self and self["FSO"].isReady() else None
            pddict = self["PD"].getPresentFields('dict')

            refshape = self["PD"]["X"][""].shape

            # Fits effective charge data, if present
            quantity_name = "Z"
            qi1 = "EFF"
            qi2 = ""
            #fzeffest = float(self["ZD"]["F"+quantity_name+qi1+qi2][""]) if "F"+quantity_name+qi1+qi2 in self["ZD"] else 1.5
            if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                    if self._debug:
                        print("%s - Linear interpolation performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (gout["VAL"] <= 0.0)
                    rfilt = (gout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        zefflim = 1.05
                        zfmin = 0.0
                        zrmin = 0.0
                        if not np.all(zfilt):
                            zfmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            zrmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                        zeffmin = np.nanmax([zfmin, zrmin, zefflim])
                        gout["VAL"][zfilt] = zeffmin
#                        gout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

            elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                fswap = True if oflag and not iflag else False

                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                    if self._debug:
                        print("%s - Curve fitting performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (fout["VAL"] <= 0.0)
                    rfilt = (fout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        zefflim = 1.05
                        zfmin = 0.0
                        zrmin = 0.0
                        if not np.all(zfilt):
                            zfmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            zrmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                        zeffmin = np.nanmax([zfmin, zrmin, zefflim])
                        fout["VAL"][zfilt] = zeffmin
#                        fout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    # Calculation of new flat Z-effective measurement based on raw profile data, assumed to be of better quality than line-integrated measurement due to possible SOL contamination
                    #zvec = np.hstack((fout["VAL"][:0:-1], fout["VAL"]))
                    #nvec = np.ones(zvec.shape)
                    #tvec = np.ones(zvec.shape)
                    #fq = "N"
                    #fqi = "E"
                    #if fq in pddict and fqi in pddict[fq]:
                    #    nvec = np.hstack((self["PD"][fq+fqi][""][:0:-1], self["PD"][fq+fqi][""])) * 1.0e-18
                    #fq = "T"
                    #fqi = "E"
                    #if fq in pddict and fqi in pddict[fq]:
                    #    tvec = np.hstack((self["PD"][fq+fqi][""][:0:-1], self["PD"][fq+fqi][""])) * 1.0e-3
                    #ocs = self["META"]["CSO"]
                    #ocp = self["META"]["CSOP"]
                    #rmajo = itemgetter(0)(self["CD"].convert(self["PD"]["X"][""], ocs, "RMAJORO", ocp, ocp))
                    #rmaji = itemgetter(0)(self["CD"].convert(self["PD"]["X"][""], ocs, "RMAJORI", ocp, ocp))
                    #rmaj = np.hstack((rmaji[:0:-1], rmajo))
                    #fzeffest = float(cumtrapz(np.power(nvec, 2.0) * zvec / np.sqrt(tvec), rmaj) / cumtrapz(np.power(nvec, 2.0) / np.sqrt(tvec), rmaj))

                else:
                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))

            numion = self["META"]["NUMI"]
            numimp = self["META"]["NUMIMP"]
            for idx in range(0, numimp):

                # Fits impurity ion densities, if any are present
                quantity_name = "N"
                qi1 = "IMP"
                qi2 = "%d" % (idx+1)
                if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (gout["VAL"] <= 0.0)
                        rfilt = (gout["YRAW"] <= 0.0)
                        if np.any(zfilt):
                            nimplim = 1.0e14
                            nimpfmin = 0.0
                            nimprmin = 0.0
                            if not np.all(zfilt):
                                nimplim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                                nimpfmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                nimprmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                            nimpmin = np.nanmax([nimpfmin, nimprmin, nimplim])
                            gout["VAL"][zfilt] = nimpmin
#                            gout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                    # Applies additional filter to impurity density data based on electron density limit assuming specified impurity charge
                    fq = "N"
                    fqi = "E"
                    if fq in pddict and fqi in pddict[fq] and "ZIMP"+qi2 in self["META"]:
                        impdata = self["RD"][quantity_name+qi1+qi2].exportToDict()
                        afilt = (np.abs(impdata["X"]) <= 1.0)
                        xxtemp = impdata["X"][afilt] if not np.all(afilt) else copy.deepcopy(impdata["X"])
                        xetemp = impdata["XEB"][afilt] if not np.all(afilt) else copy.deepcopy(impdata["XEB"])
                        yytemp = impdata[""][afilt] if not np.all(afilt) else copy.deepcopy(impdata[""])
                        yetemp = impdata["EB"][afilt] if not np.all(afilt) else copy.deepcopy(impdata["EB"])
                        maptemp = impdata["MAP"][afilt] if not np.all(afilt) else copy.deepcopy(impdata["MAP"])
                        nvec = self["PD"].interpolate(xxtemp, "X", "NE")
                        zvec = np.full(xxtemp.shape, 1.5)
                        zprov = "EX2GK: Internal assumption"
                        if "Z" in pddict and "EFF" in pddict["Z"]:
                            zvec = self["PD"].interpolate(xxtemp, "X", "ZEFF")
                            if "PROV" in self["PD"]["ZEFF"] and self["PD"]["ZEFF"]["PROV"] is not None:
                                zprov = self["PD"]["ZEFF"]["PROV"]
                        elif "FZEFF" in self["ZD"]:
                            zvec = np.full(xxtemp.shape, self["ZD"]["FZEFF"][""])
                            if "PROV" in self["ZD"]["FZEFF"] and self["ZD"]["FZEFF"]["PROV"] is not None:
                                zprov = self["ZD"]["FZEFF"]["PROV"]
                        # Calculate effective main ion charge for impurity ion density filtering, assuming equal splitting and main ion density profile shape is identical to electron density
                        itop = np.zeros(xxtemp.shape)
                        ibot = np.zeros(xxtemp.shape)
                        for iidx in range(0, numion):
                            itag = "%d" % (iidx+1)
                            ifrac = 1.0 / float(numion)
                            if fq in rddict and "I"+itag in rddict[fq]:
                                idata = self["RD"]["NI"+itag].exportToDict()
                                bfilt = (np.abs(idata["X"]) <= 1.0)
                                ixtemp = idata["X"][bfilt] if not np.all(bfilt) else copy.deepcopy(idata["X"])
                                itemp = idata[""][bfilt] if not np.all(bfilt) else copy.deepcopy(idata[""])
                                nvectemp = self["PD"].interpolate(ixtemp, "X", "NE")
                                fractemp = float(np.nanmax(itemp / nvectemp))
                                if fractemp > 0.0 and fractemp <= 1.0:
                                    ifrac = fractemp
                            if "ZI"+itag in self["META"]:
                                itop = itop + ifrac * self["PD"].interpolate(xxtemp, "X", "NE") * np.power(self["META"]["ZI"+itag], 2.0)
                                ibot = ibot + ifrac * self["PD"].interpolate(xxtemp, "X", "NE") * self["META"]["ZI"+itag]
                        ziest = float(self["META"]["ZI1"]) if "ZI1" in self["META"] else 1.0     # Assume hydogrenic, common main plasma gas
                        zivec = np.full(xxtemp.shape, ziest, dtype=np.float64)
                        zfilt = (ibot > 0.0)
                        if np.any(zfilt):
                            zivec[zfilt] = itop[zfilt] / ibot[zfilt]
                        nimplim = 1.2 * nvec * (zvec - zivec) / (self["META"]["ZIMP"+qi2] * (self["META"]["ZIMP"+qi2] - zivec))
                        lfilt = np.all([yytemp < nimplim, yytemp > 0.0], axis=0)
                        if not np.all(lfilt):
                            rxx = np.hstack((xxtemp[lfilt], impdata["X"][np.invert(afilt)])) if not np.all(afilt) else xxtemp[lfilt]
                            rxe = np.hstack((xetemp[lfilt], impdata["XEB"][np.invert(afilt)])) if not np.all(afilt) else xetemp[lfilt]
                            ryy = np.hstack((yytemp[lfilt], impdata[""][np.invert(afilt)])) if not np.all(afilt) else yytemp[lfilt]
                            rye = np.hstack((yetemp[lfilt], impdata["EB"][np.invert(afilt)])) if not np.all(afilt) else yetemp[lfilt]
                            rdxx = impdata["XGRAD"] if "XGRAD" in impdata else None
                            rdxe = impdata["XEBGRAD"] if "XEBGRAD" in impdata else None
                            rdyy = impdata["GRAD"] if "GRAD" in impdata else None
                            rdye = impdata["GRADEB"] if "GRADEB" in impdata else None
                            rmap = np.hstack((maptemp[lfilt], impdata["MAP"][np.invert(afilt)])) if not np.all(afilt) else maptemp[lfilt]
                            rsrc = impdata["SRC"]
                            rcs = impdata["XCS"] if "XCS" in impdata else None
                            rcp = impdata["XCP"] if "XCP" in impdata else None
                            rprov = "EX2GK: Internal calculation"
                            if "PROV" in impdata and impdata["PROV"] is not None and impdata["PROV"] != "Unknown":
                                rprov = impdata["PROV"]
                                if zprov is not None:
                                    rprov = rprov + "; " + zprov

                            self["RD"].addRawData(quantity_name+qi1+qi2, rxx, ryy, rsrc, rmap, rxe, rye, rcs, rcp, provenance=rprov)
                            if rdxx is not None and rdyy is not None:
                                self["RD"].addDerivativeConstraint(quantity_name+qi1+qi2, rdxx, rdyy, rdxe, rdye, rcs, rcp)

                            if self._debug:
                                print("%s - Modified %s raw data due to Z-effective limits." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    fonlybc = True
                    for kk in range(0, len(self["RD"][quantity_name+qi1+qi2][""])):
                        mapidx = self["RD"][quantity_name+qi1+qi2]["MAP"][kk]
                        if not self["RD"][quantity_name+qi1+qi2]["SRC"][mapidx].endswith("BC"):
                            fonlybc = False

                    if not fonlybc:
                        iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                        oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                        fswap = True if oflag and not iflag else False

                        fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                        if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                            self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                            if self._debug:
                                print("%s - Curve fitting performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                            zfilt = (fout["VAL"] <= 0.0)
                            rfilt = (fout["YRAW"] <= 0.0)
                            if np.any(zfilt):
                                nimplim = 1.0e14
                                nimpfmin = 0.0
                                nimprmin = 0.0
                                if not np.all(zfilt):
                                    nimplim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                                    nimpfmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                                if not np.all(rfilt):
                                    nimprmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                                nimpmin = np.nanmax([nimpfmin, nimprmin, nimplim])
                                fout["VAL"][zfilt] = nimpmin
#                                fout["DVAL"][zfilt] = 0.0

                            self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        else:
                            warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))
                            fq = "N"
                            fqi = "E"
                            nemax = float(np.nanmax(self["PD"]["NE"][""])) if fq in pddict and fqi in pddict[fq] else 1.0e16
                            fout = dict()
                            fout["VAL"] = np.full(refshape, 1.0e-3 * nemax / np.power(self["META"]["ZIMP"+qi2], 2.0))
                            fout["ERR"] = 0.4 * fout["VAL"]
                            fout["DVAL"] = np.zeros(refshape)
                            fout["DERR"] = np.zeros(refshape)
                            fout["PROV"] = "EX2GK: PD_NE, META_ZIMP"+qi2

                            if self._debug:
                                print("%s - Direct Z-effective scaling performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                            self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    else:
                        fq = "N"
                        fqi = "E"
                        nemax = float(np.nanmax(self["PD"]["NE"][""])) if fq in pddict and fqi in pddict[fq] else 1.0e16
                        fout = dict()
                        fout["VAL"] = np.full(self["PD"]["X"][""].shape, 1.0e-3 * nemax / np.power(self["META"]["ZIMP"+qi2], 2.0))
                        fout["ERR"] = 0.4 * fout["VAL"]
                        fout["DVAL"] = np.zeros(self["PD"]["X"][""].shape)
                        fout["DERR"] = np.zeros(self["PD"]["X"][""].shape)
                        fout["PROV"] = "EX2GK: PD_NE, META_ZIMP"+qi2

                        if self._debug:
                            print("%s - Direct Z-effective scaling performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                # Fits impurity ion charges, if any are present
                quantity_name = "Z"
                qi1 = "IMP"
                qi2 = "%d" % (idx+1)
                if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (gout["VAL"] <= 1.0)
                        rfilt = (gout["YRAW"] <= 1.0)
                        if np.any(zfilt):
                            zimplim = 1.0
                            zimpfmin = 0.0
                            zimprmin = 0.0
                            if not np.all(zfilt):
                                zimplim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                                zimpfmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                zimprmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                            zimpmin = np.nanmax([zimpfmin, zimprmin, zimplim])
                            gout["VAL"][zfilt] = zimpmin
#                            gout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                    fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                    if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                        self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                        if self._debug:
                            print("%s - Curve fitting performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (fout["VAL"] <= 1.0)
                        rfilt = (fout["YRAW"] <= 1.0)
                        if np.any(zfilt):
                            zimplim = 1.0
                            zimpfmin = 0.0
                            zimprmin = 0.0
                            if not np.all(zfilt):
                                zimplim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                                zimpfmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                zimprmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                            zimpmin = np.nanmax([zimpfmin, zimprmin, zimplim])
                            fout["VAL"][zfilt] = zimpmin
#                            fout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                # Fits impurity ion temperatures, if any are present
                quantity_name = "T"
                qi1 = "IMP"
                qi2 = "%d" % (idx+1)
                if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (gout["VAL"] <= 0.0)
                        rfilt = (gout["YRAW"] <= 0.0)
                        if np.any(zfilt):
                            timplim = 1.0e1
                            timpfmin = 0.0
                            timprmin = 0.0
                            if not np.all(zfilt):
                                timplim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                                timpfmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                timprmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                            timpmin = np.nanmax([timpfmin, timprmin, timplim])
                            gout["VAL"][zfilt] = timpmin
#                            gout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                    iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                    oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                    fswap = True if oflag and not iflag else False

                    fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                    if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                        self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                        if self._debug:
                            print("%s - Curve fitting performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (fout["VAL"] <= 0.0)
                        rfilt = (fout["YRAW"] <= 0.0)
                        if np.any(zfilt):
                            timplim = 1.0e1
                            timpfmin = 0.0
                            timprmin = 0.0
                            if not np.all(zfilt):
                                timplim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                                timpfmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                timprmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                            timpmin = np.nanmax([timpfmin, timprmin, timplim])
                            fout["VAL"][zfilt] = timpmin
#                            fout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    else:
                        warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry." % (quantity_name+qi1+qi2))


            # Fits combined impurity ion temperature data, if present
            quantity_name = "T"
            qi1 = "IMP"
            qi2 = ""
            if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                    if self._debug:
                        print("%s - Linear interpolation performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (gout["VAL"] <= 0.0)
                    rfilt = (gout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        timplim = 1.0e1
                        timpfmin = 0.0
                        timprmin = 0.0
                        if not np.all(zfilt):
                            timplim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                            timpfmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            timprmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                        timpmin = np.nanmax([timpfmin, timprmin, timplim])
                        gout["VAL"][zfilt] = timpmin
#                        gout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

            elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                fswap = True if oflag and not iflag else False

                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                    if self._debug:
                        print("%s - Curve fitting performed on %s data." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (fout["VAL"] <= 0.0)
                    rfilt = (fout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        timplim = 1.0e1
                        timpfmin = 0.0
                        timprmin = 0.0
                        if not np.all(zfilt):
                            timplim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                            timpfmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            timprmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                        timpmin = np.nanmax([timpfmin, timprmin, timplim])
                        fout["VAL"][zfilt] = timpmin
#                        fout["DVAL"][zfilt] = 0.0

                    for ii in range(0, numimp):
                        qit = "%d" % (ii+1)
                        if quantity_name+qi1+qi2+qit not in self["PD"]:
                            self["PD"].addProfileData(quantity_name+qi1+qi2+qit, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2+qit, fout["DVAL"], fout["DERR"])

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+qi1+qi2+qit))

                        else:
                            warnings.warn("Fitted profiles for %s already present, discarding combined impurity fit for this species" % (quantity_name+qi1+qi2+qit))

                    if numimp <= 0:
                        if "ZZ1" in self["META"]:
                            self["PD"].addProfileData(quantity_name+"Z1", fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+"Z1", fout["DVAL"], fout["DERR"])

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+"Z1"))

                        else:
                            self["PD"].addProfileData(quantity_name+"TEMP", fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+"TEMP", fout["DVAL"], fout["DERR"])
                            warnings.warn("No standard location for combined impurity ion temperature fit found, temporary field %s created" % (quantity_name+"TEMP"))

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitSecondaryProfiles.__name__, quantity_name+"TEMP"))

                else:
                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry." % (quantity_name+qi1+qi2))

            self["FLAG"].setFlag("FITDONE", True)

        else:
            warnings.warn("%s object is not ready for fitting procedure on secondary profiles" % (type(self).__name__))

    def fitTertiaryProfiles(self):
        """
        """
        if self._debug:
            print("Entered %s..." % (type(self).fitTertiaryProfiles.__name__))

        verbose = 1 if self._debug else 0
        if self.isReady(verbose=verbose) and "PD" in self and "X" in self["PD"]:

            rddict = self["RD"].getPresentFields('dict')
            fsidict = self["FSI"].getPresentFields('dict')
            fsodict = self["FSO"].getPresentFields('dict') if "FSO" in self and self["FSO"].isReady() else None
            pddict = self["PD"].getPresentFields('dict')

            refshape = self["PD"]["X"][""].shape

            numion = self["META"]["NUMI"]
            for idx in range(0, numion):

                # Fits main ion densities, if any are present
                quantity_name = "N"
                qi1 = "I"
                qi2 = "%d" % (idx+1)
                if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                        zi = self["META"]["ZI"+qi2] if "ZI"+qi2 in self["META"] else 1.0
                        zfilt = (gout["VAL"] <= 0.0)
                        rfilt = (gout["YRAW"] <= 0.0)
                        if np.any(zfilt):
                            nilim = 1.0e16 / zi if zi > 0.0 else 1.0e16
                            nifmin = 0.0
                            nirmin = 0.0
                            if not np.all(zfilt):
                                nilim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                                nifmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                nirmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                            nimin = np.nanmax([nifmin, nirmin, nilim])
                            gout["VAL"][zfilt] = nimin
#                            gout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                    # Applies additional filter to main ion density data based on electron density limit assuming specified main ion charge
                    fq = "N"
                    fqi = "E"
                    if False and fq in pddict and fqi in pddict[fq] and "ZI"+qi2 in self["META"]:
                        idata = self["RD"][quantity_name+qi1+qi2].exportToDict()
                        afilt = (np.abs(impdata["X"]) <= 1.0)
                        xxtemp = idata["X"][afilt] if not np.all(afilt) else copy.deepcopy(idata["X"])
                        xetemp = idata["XEB"][afilt] if not np.all(afilt) else copy.deepcopy(idata["XEB"])
                        yytemp = idata[""][afilt] if not np.all(afilt) else copy.deepcopy(idata[""])
                        yetemp = idata["EB"][afilt] if not np.all(afilt) else copy.deepcopy(idata["EB"])
                        maptemp = idata["MAP"][afilt] if not np.all(afilt) else copy.deepcopy(idata["MAP"])
                        nvec = self["PD"].interpolate(xxtemp, "X", "NE")
                        zvec = np.full(xxtemp.shape, 1.5)
                        zprov = "EX2GK: Internal assumption"
                        if "Z" in pddict and "EFF" in pddict["Z"]:
                            zvec = self["PD"].interpolate(xxtemp, "X", "ZEFF")
                            if "PROV" in self["PD"]["ZEFF"] and self["PD"]["ZEFF"]["PROV"] is not None:
                                zprov = self["PD"]["ZEFF"]["PROV"]
                        elif "FZEFF" in self["ZD"]:
                            zvec = np.full(xxtemp.shape, self["ZD"]["FZEFF"][""])
                            if "PROV" in self["ZD"]["FZEFF"] and self["ZD"]["FZEFF"]["PROV"] is not None:
                                zprov = self["ZD"]["FZEFF"]["PROV"]
                        # Calculation of average impurity charge for main ion density estimation
                        imptop = np.zeros(xxtemp.shape)
                        impbot = np.zeros(xxtemp.shape)
                        for iidx in range(0, numimp):
                            itag = "%d" % (iidx+1)
                            if "IMP"+itag in pddict[fq] and "ZIMP"+itag in self["META"]:
                                imptop = imptop + self["PD"].interpolate(xxtemp, "X", "NIMP"+itag) * np.power(self["META"]["ZIMP"+itag], 2.0)
                                impbot = impbot + self["PD"].interpolate(xxtemp, "X", "NIMP"+itag) * self["META"]["ZIMP"+itag]
                        zzest = float(self["META"]["ZZ1"]) if "ZZ1" in self["META"]["ZZ1"] else 7.0   # Assume nitrogen, common puff gas
                        zimpvec = np.full(xxtemp.shape, zzest, dtype=np.float64)
                        zfilt = (impbot > 0.0)
                        if np.any(zfilt):
                            zimpvec[zfilt] = imptop[zfilt] / impbot[zfilt]
                        nilim = 1.25 * (nvec * (zzest - zvec) - impbot * (zimpvec - zzest)) / (self["META"]["ZI"+qi2] * (zzest - self["META"]["ZI"+qi2]))
                        lfilt = np.all([yytemp < nilim, yytemp > 0.0], axis=0)
                        if not np.all(lfilt):
                            rxx = np.hstack((xxtemp[lfilt], idata["X"][np.invert(afilt)])) if not np.all(afilt) else xxtemp[lfilt]
                            rxe = np.hstack((xetemp[lfilt], idata["XEB"][np.invert(afilt)])) if not np.all(afilt) else xetemp[lfilt]
                            ryy = np.hstack((yytemp[lfilt], idata[""][np.invert(afilt)])) if not np.all(afilt) else yytemp[lfilt]
                            rye = np.hstack((yetemp[lfilt], idata["EB"][np.invert(afilt)])) if not np.all(afilt) else yetemp[lfilt]
                            rdxx = idata["XGRAD"] if "XGRAD" in idata else None
                            rdxe = idata["XEBGRAD"] if "XEBGRAD" in idata else None
                            rdyy = idata["GRAD"] if "GRAD" in idata else None
                            rdye = idata["GRADEB"] if "GRADEB" in idata else None
                            rmap = np.hstack((maptemp[lfilt], idata["MAP"][np.invert(afilt)])) if not np.all(afilt) else maptemp[lfilt]
                            rsrc = idata["SRC"]
                            rcs = idata["XCS"] if "XCS" in idata else None
                            rcp = idata["XCP"] if "XCP" in idata else None
                            rprov = "EX2GK: Internal calculation"
                            if "PROV" in impdata and impdata["PROV"] is not None and impdata["PROV"] != "Unknown":
                                rprov = impdata["PROV"]
                                if zprov is not None:
                                    rprov = rprov + "; " + zprov

                            self["RD"].addRawData(quantity_name+qi1+qi2, rxx, ryy, rsrc, rmap, rxe, rye, rcs, rcp, provenance=rprov)
                            if rdxx is not None and rdyy is not None:
                                self["RD"].addDerivativeConstraint(quantity_name+qi1+qi2, rdxx, rdyy, rdxe, rdye, rcs, rcp)

                            if self._debug:
                                print("%s - Modified %s raw data due to Z-effective limits." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                    fonlybc = True
                    for kk in range(0, len(self["RD"][quantity_name+qi1+qi2][""])):
                        mapidx = self["RD"][quantity_name+qi1+qi2]["MAP"][kk]
                        if not self["RD"][quantity_name+qi1+qi2]["SRC"][mapidx].endswith("BC"):
                            fonlybc = False

                    if not fonlybc:
                        iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                        oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                        fswap = True if oflag and not iflag else False

                        fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                        if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                            self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                            if self._debug:
                                print("%s - Curve fitting performed on %s data." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                            zi = self["META"]["ZI"+qi2] if "ZI"+qi2 in self["META"] else 1.0
                            zfilt = (fout["VAL"] <= 0.0)
                            rfilt = (fout["YRAW"] <= 0.0)
                            if np.any(zfilt):
                                nilim = 1.0e16 / zi if zi > 0.0 else 1.0e16
                                nifmin = 0.0
                                nirmin = 0.0
                                if not np.all(zfilt):
                                    nilim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                                    nifmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                                if not np.all(rfilt):
                                    nirmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                                nimin = np.nanmax([nifmin, nirmin, nilim])
                                fout["VAL"][zfilt] = nimin
#                                fout["DVAL"][zfilt] = 0.0

                            self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                        else:
                            warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry." % (quantity_name+qi1+qi2))


                # Fits main ion temperatures, if any are present
                quantity_name = "T"
                qi1 = "I"
                qi2 = "%d" % (idx+1)
                if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                    gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                    if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                        if self._debug:
                            print("%s - Linear interpolation performed on %s data." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (gout["VAL"] <= 0.0)
                        rfilt = (gout["YRAW"] <= 0.0)
                        if np.any(zfilt):
                            tilim = 1.0e1
                            tifmin = 0.0
                            if not np.all(zfilt):
                                tilim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                                tifmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                tirmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                            timin = np.nanmax([tifmin, tirmin, tilim])
                            gout["VAL"][zfilt] = timin
#                            gout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name] and not self["FLAG"].checkFlag("TISCALE"):

                    iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                    oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                    fswap = True if oflag and not iflag else False

                    fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                    if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                        self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                        if self._debug:
                            print("%s - Curve fitting performed on %s data." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                        zfilt = (fout["VAL"] <= 0.0)
                        rfilt = (fout["YRAW"] <= 0.0)
                        if np.any(zfilt):
                            tilim = 1.0e1
                            tifmin = 0.0
                            if not np.all(zfilt):
                                tilim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                                tifmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                            if not np.all(rfilt):
                                tirmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                            timin = np.nanmax([tifmin, tirmin, tilim])
                            fout["VAL"][zfilt] = timin
#                            fout["DVAL"][zfilt] = 0.0

                        self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                        self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                        if self._debug:
                            print("%s - Added %s variable to profile data container." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                    else:
                        warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry." % (quantity_name+qi1+qi2))


            # Fits combined main ion temperature data, if present
            quantity_name = "T"
            qi1 = "I"
            qi2 = ""
            if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                    if self._debug:
                        print("%s - Linear interpolation performed on %s data." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (gout["VAL"] <= 0.0)
                    rfilt = (gout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        tilim = 1.0e1
                        tifmin = 0.0
                        if not np.all(zfilt):
                            tilim = 1.0e-3 * np.nanmax(gout["VAL"][np.invert(zfilt)])
                            tifmin = np.nanmin(gout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            tirmin = np.nanmin(gout["YRAW"][np.invert(rfilt)])
                        timin = np.nanmax([tifmin, tirmin, tilim])
                        gout["VAL"][zfilt] = timin
#                        gout["DVAL"][zfilt] = 0.0

                    self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                    if self._debug:
                        print("%s - Added %s variable to profile data container." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

            elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name] and not self["FLAG"].checkFlag("TISCALE"):

                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                fswap = True if oflag and not iflag else False

                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                    if self._debug:
                        print("%s - Curve fitting performed on %s data." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2))

                    zfilt = (fout["VAL"] <= 0.0)
                    rfilt = (fout["YRAW"] <= 0.0)
                    if np.any(zfilt):
                        tilim = 1.0e1
                        tifmin = 0.0
                        tirmin = 0.0
                        if not np.all(zfilt):
                            tilim = 1.0e-3 * np.nanmax(fout["VAL"][np.invert(zfilt)])
                            tifmin = np.nanmin(fout["VAL"][np.invert(zfilt)])
                        if not np.all(rfilt):
                            tirmin = np.nanmin(fout["YRAW"][np.invert(rfilt)])
                        timin = np.nanmax([tifmin, tirmin, tilim])
                        fout["VAL"][zfilt] = timin
#                        fout["DVAL"][zfilt] = 0.0

                    for ii in range(0, numion):
                        qit = "%d" % (ii+1)
                        if quantity_name+qi1+qi2+qit not in self["PD"]:
                            self["PD"].addProfileData(quantity_name+qi1+qi2+qit, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                            self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2+qit, fout["DVAL"], fout["DERR"])

                            if self._debug:
                                print("%s - Added %s variable to profile data container." % (type(self).fitTertiaryProfiles.__name__, quantity_name+qi1+qi2+qit))

                        else:
                            warnings.warn("Fitted profiles for %s already present, discarding combined impurity fit for this species" % (quantity_name+qi1+qi2+qit))

                else:
                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))

            self["FLAG"].setFlag("FITDONE", True)

        else:
            warnings.warn("%s object is not ready for fitting procedure on tertiary profiles" % (type(self).__name__))

    def fitSourceProfiles(self, out_grid=None, out_coordinate=None, out_coordinate_prefix=None):
        """
        Performs fitting of provided source profiles as inputs
        for various analysis codes. This function assumes that
        the output will be in toridal rho radial coordinate
        system, although the Jacobians are provided to convert
        everything to any other system if necessary.

        :arg rhogrid: array. Vector of radial points on which the parameters are calculated.

        :arg basedata: dict. Object containing standardized data fields for source profiles.

        :returns: dict. Object with identical structure as input object except with fitted source profiles inserted in a standardized format.
        """
        if self._debug:
            print("Entered %s..." % (type(self).fitSourceProfiles.__name__))

        newgrid = None
        if "PD" in self and "X" in self["PD"]:
            newgrid = self["PD"]["X"][""].copy()
            if isinstance(out_grid, array_types):
                warnings.warn("Radial fit coordinate already present in %s object, ignoring input vector to %s" % (type(self).__name__, type(self).fitSourceProfiles.__name__))
        elif isinstance(out_grid, array_types):
            newgrid = np.array(out_grid).flatten()
        if newgrid is not None:
            newgrid = newgrid[np.isfinite(newgrid)]
        else:
            raise TypeError("Invalid fit vector passed to %s, crashing to prevent nonsensical processing" % (type(self).fitSourceProfiles.__name__))

        verbose = 1 if self._debug else 0
        if newgrid is not None and newgrid.size > 0 and self.isReady(verbose=verbose):

            if "PD" not in self:
                self["PD"] = EX2GKProfileDataContainer()

            rddict = self["RD"].getPresentFields('dict')
            fsidict = self["FSI"].getPresentFields('dict')
            fsodict = self["FSO"].getPresentFields('dict') if "FSO" in self and self["FSO"].isReady() else None

            refshape = newgrid.shape

            # If no coordinate system given, assume it is rhotor
            if "X" not in self["PD"]:
                csout = itemgetter(0)(ptools.define_coordinate_system(out_coordinate))
                prefix_list = self["CD"].getPrefixes()
                cspre = out_coordinate_prefix if out_coordinate_prefix in prefix_list else prefix_list[0]
                self["META"].setFittingMetaData(csout, np.nanmin(newgrid), np.nanmax(newgrid), cspre)
                newgrid_error = np.zeros(refshape, dtype=np.float64)
                prov = "EX2GK: User-defined"
                self["PD"].addProfileData("X", newgrid, newgrid_error, provenance=prov)
                self["PD"].addProfileDerivativeData("X", np.ones(refshape), np.zeros(refshape))

            # Calculate flux surface volume coordinate for integrated profiles
            if "V" not in self["PD"]:
                csout = self["META"]["CSO"]
                cspre = self["META"]["CSOP"]
                (volgrid, voljac, volgrid_error) = self["CD"].convert(self["PD"]["X"][""], csout, "FSVOL", cspre, cspre)
                prov = "EX2GK: PD_X; EX2GK: CD_"+cspre+"FSVOL"
                self["PD"].addProfileData("V", volgrid, volgrid_error, provenance=prov)
                self["PD"].addProfileDerivativeData("V", voljac, np.zeros(volgrid.shape))

            # Loop for fits on particle, heat, momentum and current source data, if present
            quantities = ["SN", "ST", "SP", "J"]
            identifiers = ["", "E", "I", "I1", "I2", "I3"]
            for quantity_name in quantities:
                for qi1 in identifiers:
                    for qi2 in self.allowed_sources:
                        if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                            gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                            if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                                if self._debug:
                                    print("%s - Linear interpolation performed on %s data." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                if quantity_name not in ["SP", "J"]:
                                    zfilt = (gout["VAL"] <= 0.0)
                                    srcmin = 0.0
                                    if np.any(zfilt):
                                        gout["VAL"][zfilt] = srcmin

                                self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                                self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                                if self._debug:
                                    print("%s - Added %s variable to profile data container." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                        elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                            if self["FLAG"].checkFlag("LINSRC"):

                                lout = self._performLinearInterpolation(quantity_name, qi1, qi2)
                                if lout is not None and "VAL" in lout and lout["VAL"] is not None:

                                    if self._debug:
                                        print("%s - Linear interpolation performed on %s data." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                    if quantity_name not in ["SP", "J"]:
                                        zfilt = (lout["VAL"] <= 0.0)
                                        srcmin = 0.0
                                        if np.any(zfilt):
                                            lout["VAL"][zfilt] = srcmin

                                    self["PD"].addProfileData(quantity_name+qi1+qi2, lout["VAL"], lout["ERR"], provenance=lout["PROV"])
                                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, lout["DVAL"], lout["DERR"])

                                    if self._debug:
                                        print("%s - Added %s variable to profile data container." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                            else:

                                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                                fswap = True if oflag and not iflag else False

                                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                                    if self._debug:
                                        print("%s - Curve fitting performed on %s data." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                    if quantity_name not in ["SP", "J"]:
                                        zfilt = (fout["VAL"] <= 0.0)
                                        srcmin = 0.0
                                        if np.any(zfilt):
                                            fout["VAL"][zfilt] = srcmin
#                                            fout["DVAL"][zfilt] = 0.0

                                    self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                                    if self._debug:
                                        print("%s - Added %s variable to profile data container." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                else:
                                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))

            # Separate processing loop for fast ion sources, disconnect from linear source interpolation option
            quantities = ["N", "T", "P", "W"]
            identifiers = ["FI", "FI1", "FI2", "FI3", "FI4"]
            for quantity_name in quantities:
                for qi1 in identifiers:
                    for qi2 in self.allowed_sources:
                        if quantity_name in rddict and qi1+qi2+"_" in rddict[quantity_name]:

                            gout = self._performLinearInterpolation(quantity_name, qi1, qi2+"_", direct_derivative=True)
                            if gout is not None and "VAL" in gout and gout["VAL"] is not None:

                                if self._debug:
                                    print("%s - Linear interpolation performed on %s data." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                zfilt = (gout["VAL"] <= 0.0)
                                srcmin = 0.0
                                if np.any(zfilt):
                                    gout["VAL"][zfilt] = srcmin

                                self["PD"].addProfileData(quantity_name+qi1+qi2, gout["VAL"], gout["ERR"], provenance=gout["PROV"])
                                self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, gout["DVAL"], gout["DERR"])

                                if self._debug:
                                    print("%s - Added %s variable to profile data container." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                        elif quantity_name in rddict and qi1+qi2 in rddict[quantity_name]:

                            if self["FLAG"].checkFlag("LINFI"):

                                lout = self._performLinearInterpolation(quantity_name, qi1, qi2)
                                if lout is not None and "VAL" in lout and lout["VAL"] is not None:

                                    if self._debug:
                                        print("%s - Linear interpolation performed on %s data." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                    zfilt = (lout["VAL"] <= 0.0)
                                    srcmin = 0.0
                                    if np.any(zfilt):
                                        lout["VAL"][zfilt] = srcmin

                                    self["PD"].addProfileData(quantity_name+qi1+qi2, lout["VAL"], lout["ERR"], provenance=lout["PROV"])
                                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, lout["DVAL"], lout["DERR"])

                                    if self._debug:
                                        print("%s - Added %s variable to profile data container." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                            else:

                                iflag = quantity_name in fsidict and (qi1 in fsidict[quantity_name] or qi1+qi2 in fsidict[quantity_name])
                                oflag = isinstance(fsodict, dict) and quantity_name in fsodict and (qi1 in fsodict[quantity_name] or qi1+qi2 in fsodict[quantity_name])
                                fswap = True if oflag and not iflag else False

                                fout = self._performFit(quantity_name, qi1, qi2, swap_io=fswap)
                                if fout is not None and "VAL" in fout and fout["VAL"] is not None:

                                    self._saveOptimizedFitSettings(fout, quantity_name+qi1+qi2)

                                    if self._debug:
                                        print("%s - Curve fitting performed on %s data." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                    zfilt = (fout["VAL"] <= 0.0)
                                    srcmin = 0.0
                                    if np.any(zfilt):
                                        fout["VAL"][zfilt] = srcmin
#                                        fout["DVAL"][zfilt] = 0.0

                                    self["PD"].addProfileData(quantity_name+qi1+qi2, fout["VAL"], fout["ERR"], provenance=fout["PROV"])
                                    self["PD"].addProfileDerivativeData(quantity_name+qi1+qi2, fout["DVAL"], fout["DERR"])

                                    if self._debug:
                                        print("%s - Added %s variable to profile data container." % (type(self).fitSourceProfiles.__name__, quantity_name+qi1+qi2))

                                else:
                                    warnings.warn("None of the %s fit settings successfully generated a fit, please adjust settings and retry" % (quantity_name+qi1+qi2))

            self["FLAG"].setFlag("FITDONE", True)

        else:
            warnings.warn("%s object is not ready for fitting procedure on source profiles" % (type(self).__name__))

    def _calculateAssumedProfiles(self):
        """
        Does not account for fast ion species at the moment
        """
        if "META" in self and self["META"].isReady() and "PD" in self and "X" in self["PD"]:

            pddict = self["PD"].getPresentFields('dict')
            cs_prefix = self["META"]["CSOP"]
            numimp = self["META"]["NUMIMP"]
            numion = self["META"]["NUMI"]

            refshape = self["PD"]["X"][""].shape

            # Main ion data containers for Zeff calculation
            nz_i = np.zeros(refshape)
            nzeb_i = np.zeros(refshape)
            dnz_i = np.zeros(refshape)
            dnzeb_i = np.zeros(refshape)
            nzz_i = np.zeros(refshape)
            nzzeb_i = np.zeros(refshape)
            dnzz_i = np.zeros(refshape)
            dnzzeb_i = np.zeros(refshape)
            ni_prov = None

            # Measured impurity ion data containers for Zeff calculation
            nz_imp = np.zeros(refshape)
            nzeb_imp = np.zeros(refshape)
            dnz_imp = np.zeros(refshape)
            dnzeb_imp = np.zeros(refshape)
            nzz_imp = np.zeros(refshape)
            nzzeb_imp = np.zeros(refshape)
            dnzz_imp = np.zeros(refshape)
            dnzzeb_imp = np.zeros(refshape)
            nimp_prov = None

            # Isotopic weight of missing main ion species
            itotweight = 0.0
            for idx in range(0, numion):
                itag = "%d" % (idx+1)
                if not ("N" in pddict and "I"+itag in pddict["N"]):
                    if "WGTI"+itag in self["META"]:
                        itotweight = itotweight + self["META"]["WGTI"+itag]
                    else:
                        itotweight = itotweight + (1.0 / float(numion))

            if not ("Z" in pddict and "EFF" in pddict["Z"]):

                # Provide a crude estimate of Z-effective if information is lacking, required for assumed impurity estimates
                fzeffflag = False
                quantity_name = "Z"
                qi1 = "EFF"
                if "FZEFF" in self["ZD"] and self["ZD"]["FZEFF"][""] is not None:
                    qtag = "FZEFF"
                    fzeffp = np.full(refshape, self["ZD"][qtag][""])
                    fzeffpeb = np.full(refshape, self["ZD"][qtag]["EB"])
                    prov = self["ZD"]["FZEFF"]["PROV"] if "PROV" in self["ZD"]["FZEFF"] and self["ZD"]["FZEFF"]["PROV"] is not None else "EX2GK: ZD_FZEFF"
                    self["PD"].addProfileData(quantity_name+qi1, fzeffp, fzeffpeb, provenance=prov)
                    self["PD"].addProfileDerivativeData(quantity_name+qi1, np.zeros(refshape), np.zeros(refshape))
                    fzeffflag = True

                    if self._debug:
                        print("%s - Assuming flat Z-effective profile. Added %s variable to profile data container using %s." % (type(self)._calculateAssumedProfiles.__name__, quantity_name+qi1, "flat Z-effective measurement"))

                else:
                    fout = dict()
                    fzeffp = np.full(refshape, 1.5)
                    fzeffpeb = np.full(refshape, 0.1)
                    prov = "EX2GK: Internal assumption"
                    self["PD"].addProfileData(quantity_name+qi1, fzeffp, fzeffpeb, provenance=prov)
                    self["PD"].addProfileDerivativeData(quantity_name+qi1, np.zeros(refshape), np.zeros(refshape))
                    fzeffflag = True

                    if self._debug:
                        print("%s - Assuming flat Z-effective profile. Added %s variable to profile data container using %s." % (type(self)._calculateAssumedProfiles.__name__, quantity_name+qi1, "heuristic estimate"))

                self["FLAG"].setFlag("FLATZEFF", fzeffflag)

            # Estimates assumed impurity ion and main ion density profiles
            if "N" in pddict and "E" in pddict["N"]:

                if self._debug:
                    print("%s - Calculating quasineutrality and Z-effective assuming all species have been measured..." % (type(self)._calculateAssumedProfiles.__name__))

                for idx in range(0, numion):
                    itag = "%d" % (idx+1)
                    if "I"+itag in pddict["N"] and "ZI"+itag in self["META"]:
                        zi = np.full(refshape, self["META"]["ZI"+itag])
                        zieb = np.zeros(refshape)
                        dzi = np.zeros(refshape)
                        dzieb = np.zeros(refshape)
                        zprov = "EX2GK: META_ZI"+itag
                        if "Z" in pddict and "I"+itag in pddict["Z"]:
                            zi = copy.deepcopy(self["PD"]["ZI"+itag][""])
                            zieb = copy.deepcopy(self["PD"]["ZI"+itag]["EB"])
                            dzi = copy.deepcopy(self["PD"]["ZI"+itag]["GRAD"])
                            dzieb = copy.deepcopy(self["PD"]["ZI"+itag]["GRADEB"])
                            zprov = "EX2GK: PD_ZI"+itag
                        nz_i = nz_i + self["PD"]["NI"+itag][""] * zi
                        nzeb_i = np.sqrt(np.power(nzeb_i, 2.0) + np.power(self["PD"]["NI"+itag]["EB"] * zi, 2.0) + np.power(self["PD"]["NI"+itag][""] * zieb, 2.0))
                        dnz_i = dnz_i + self["PD"]["NI"+itag]["GRAD"] * zi + self["PD"]["NI"+itag][""] * dzi
                        dnzeb_i = np.sqrt(np.power(dnzeb_i, 2.0) + np.power(self["PD"]["NI"+itag]["GRADEB"] * zi, 2.0) + np.power(self["PD"]["NI"+itag]["GRAD"] * zieb, 2.0) + np.power(self["PD"]["NI"+itag]["EB"] * dzi, 2.0) + np.power(self["PD"]["NI"+itag][""] * dzieb, 2.0))
                        nzz_i = nzz_i + self["PD"]["NI"+itag][""] * np.power(zi, 2.0)
                        nzzeb_i = np.sqrt(np.power(nzzeb_i, 2.0) + np.power(self["PD"]["NI"+itag]["EB"] * np.power(zi, 2.0), 2.0) + np.power(2.0 * self["PD"]["NI"+itag][""] * zi * zieb, 2.0))
                        dnzz_i = dnzz_i + self["PD"]["NI"+itag]["GRAD"] * np.power(zi, 2.0) + 2.0 * self["PD"]["NI"+itag][""] * zi * dzi
                        dnzzeb_i = np.sqrt(np.power(dnzzeb_i, 2.0) + np.power(self["PD"]["NI"+itag]["GRADEB"] * np.power(zi, 2.0), 2.0) + np.power(2.0 * self["PD"]["NI"+itag]["GRAD"] * zi * zieb, 2.0) + np.power(2.0 * self["PD"]["NI"+itag]["EB"] * zi * dzi, 2.0) + np.power(2.0 * self["PD"]["NI"+itag][""] * zieb * dzi, 2.0) + np.power(2.0 * self["PD"]["NI"+itag][""] * zi * dzieb, 2.0))
                        ni_prov = ni_prov + "; EX2GK: PD_NI"+itag + "; " + zprov if ni_prov is not None else "EX2GK: PD_NI"+itag + "; " + zprov
                    #elif "WGTI"+itag in self["META"] and "ZI"+itag in self["META"]:
                    #    ifrac = self["META"]["WGTI"+itag] / itotweight if itotweight > 0.0 else self["META"]["WGTI"+itag] / float(numion)
                    #    nz_i = nz_i + ifrac * self["PD"]["NE"][""] * self["META"]["ZI"+itag]
                    #    dnz_i = dnz_i + ifrac * self["PD"]["NE"]["GRAD"] * self["META"]["ZI"+itag]
                    #    nzz_i = nzz_i + ifrac * self["PD"]["NE"][""] * np.power(self["META"]["ZI"+itag], 2.0)
                    #    dnzz_i = dnzz_i + ifrac * self["PD"]["NE"]["GRAD"] * np.power(self["META"]["ZI"+itag], 2.0)
                for idx in range(0, numimp):
                    itag = "%d" % (idx+1)
                    if "IMP"+itag in pddict["N"] and "ZIMP"+itag in self["META"]:
                        zimp = np.full(refshape, self["META"]["ZIMP"+itag])
                        zimpeb = np.zeros(refshape)
                        dzimp = np.zeros(refshape)
                        dzimpeb = np.zeros(refshape)
                        zprov = "EX2GK: META_ZIMP"+itag
                        if "Z" in pddict and "IMP"+itag in pddict["Z"]:
                            zimp = copy.deepcopy(self["PD"]["ZIMP"+itag][""])
                            zimpeb = copy.deepcopy(self["PD"]["ZIMP"+itag]["EB"])
                            dzimp = copy.deepcopy(self["PD"]["ZIMP"+itag]["GRAD"])
                            dzimpeb = copy.deepcopy(self["PD"]["ZIMP"+itag]["GRADEB"])
                            zprov = "EX2GK: PD_ZIMP"+itag
                        nz_imp = nz_imp + self["PD"]["NIMP"+itag][""] * zimp
                        nzeb_imp = np.sqrt(np.power(nzeb_imp, 2.0) + np.power(self["PD"]["NIMP"+itag]["EB"] * zimp, 2.0) + np.power(self["PD"]["NIMP"+itag][""] * zimpeb, 2.0))
                        dnz_imp = dnz_imp + self["PD"]["NIMP"+itag]["GRAD"] * self["META"]["ZIMP"+itag] + self["PD"]["NIMP"+itag][""] * dzimp
                        dnzeb_imp = np.sqrt(np.power(dnzeb_imp, 2.0) + np.power(self["PD"]["NIMP"+itag]["GRADEB"] * zimp, 2.0) + np.power(self["PD"]["NIMP"+itag]["GRAD"] * zimpeb, 2.0) + np.power(self["PD"]["NIMP"+itag]["EB"] * dzimp, 2.0) + np.power(self["PD"]["NIMP"+itag][""] * dzimpeb, 2.0))
                        nzz_imp = nzz_imp + self["PD"]["NIMP"+itag][""] * np.power(zimp, 2.0)
                        nzzeb_imp = np.sqrt(np.power(nzzeb_imp, 2.0) + np.power(self["PD"]["NIMP"+itag]["EB"] * np.power(zimp, 2.0), 2.0) + np.power(2.0 * self["PD"]["NIMP"+itag][""] * zimp * zimpeb, 2.0))
                        dnzz_imp = dnzz_imp + self["PD"]["NIMP"+itag]["GRAD"] * np.power(zimp, 2.0) + 2.0 * self["PD"]["NIMP"+itag][""] * zimp * dzimp
                        dnzzeb_imp = np.sqrt(np.power(dnzzeb_imp, 2.0) + np.power(self["PD"]["NIMP"+itag]["GRADEB"] * np.power(zimp, 2.0), 2.0) + np.power(2.0 * self["PD"]["NIMP"+itag]["GRAD"] * zimp * zimpeb, 2.0) + np.power(2.0 * self["PD"]["NIMP"+itag]["EB"] * zimp * dzimp, 2.0) + np.power(2.0 * self["PD"]["NIMP"+itag][""] * zimpeb * dzimpeb, 2.0) + np.power(2.0 * self["PD"]["NIMP"+itag][""] * zimp * dzimpeb, 2.0))
                        nimp_prov = nimp_prov + "; EX2GK: PD_NIMP"+itag + "; " + zprov if ni_prov is not None else "EX2GK: PD_NIMP"+itag + "; " + zprov

                # Calculate main ion densities, if not already present due to presence of experimental data
                nz_ai = np.zeros(refshape)
                nzeb_ai = np.zeros(refshape)
                dnz_ai = np.zeros(refshape)
                dnzeb_ai = np.zeros(refshape)
                nzz_ai = np.zeros(refshape)
                nzzeb_ai = np.zeros(refshape)
                dnzz_ai = np.zeros(refshape)
                dnzzeb_ai = np.zeros(refshape)
                na_prov = None
                if itotweight > 0.0:
                    mstr = ""
                    fnzi = self["PD"]["NE"][""] - nz_i - nz_imp
                    fnzieb = np.sqrt(np.power(self["PD"]["NE"]["EB"], 2.0) + np.power(nzeb_i, 2.0) + np.power(nzeb_imp, 2.0))
                    fdnzi = self["PD"]["NE"]["GRAD"] - dnz_i - dnz_imp
                    fdnzieb = np.sqrt(np.power(self["PD"]["NE"]["GRADEB"], 2.0) + np.power(dnzeb_i, 2.0) + np.power(dnzeb_imp, 2.0))
                    for idx in range(0, numion):
                        itag = "%d" % (idx+1)
                        weight = self["META"]["WGTI"+itag] if "WGTI"+itag in self["META"] else 1.0 / float(numion)
                        if "I"+itag not in pddict["N"] and "ZI"+itag in self["META"]:
                            ifrac = self["META"]["WGTI"+itag] / itotweight
                            nz_ai += ifrac * fnzi
                            nzeb_ai += ifrac * fnzieb
                            dnz_ai = ifrac * fdnzi
                            dnzeb_ai = ifrac * fdnzieb
                            nzz_ai += ifrac * fnzi * self["META"]["ZI"+itag]
                            nzzeb_ai += ifrac * fnzieb * self["META"]["ZI"+itag]
                            dnzz_ai = ifrac * fdnzi * self["META"]["ZI"+itag]
                            dnzzeb_ai = ifrac * fdnzieb * self["META"]["ZI"+itag]
                            if mstr:
                                mstr += ", "
                            mstr += "NI"+itag
                            na_prov = na_prov + "; EX2GK: META_ZI"+itag if na_prov is not None else "EX2GK: META_ZI"+itag

                    if self._debug and mstr:
                        print("%s - Insufficient main ion density measurements found. Estimated %s using quasineutrality and weight fractions." % (type(self)._calculateAssumedProfiles.__name__, mstr))

                # Calculate Z-effective profile purely from known (and QN) main ion and known impurity profiles
                cne = copy.deepcopy(self["PD"]["NE"][""])
                zeffp = (nzz_i + nzz_imp + nzz_ai) / cne
                zeffpeb = np.sqrt((np.power(nzzeb_i / cne, 2.0) + np.power(nzzeb_imp / cne, 2.0) + np.power(nzzeb_ai / cne, 2.0)) + np.power(zeffp * self["PD"]["NE"]["EB"] / cne, 2.0))
                dzeffp = (dnzz_i + dnzz_imp + dnzz_ai - zeffp * self["PD"]["NE"]["GRAD"]) / cne
                dzeffpeb1 = np.power(dnzzeb_i / cne, 2.0) + np.power(dnzzeb_imp / cne, 2.0) + np.power(dnzzeb_ai / cne, 2.0) + np.power(self["PD"]["NE"]["EB"] / cne, 2.0) * (np.power(dnzz_i / cne, 2.0) + np.power(dnzz_imp / cne, 2.0) + np.power(dnzz_ai / cne, 2.0))
                dzeffpeb2 = np.power(zeffpeb * self["PD"]["NE"]["GRAD"] / cne, 2.0) + np.power(zeffp * self["PD"]["NE"]["GRADEB"] / cne, 2.0) + np.power(self["PD"]["NE"]["EB"] / cne, 2.0) * np.power(zeffp * self["PD"]["NE"]["GRAD"] / cne, 2.0)
                dzeffpeb = np.sqrt(dzeffpeb1 + dzeffpeb2)
                prov = "EX2GK: PD_NE"
                if ni_prov is not None:
                    prov = prov + "; " + ni_prov
                if nimp_prov is not None:
                    prov = prov + "; " + nimp_prov
                if na_prov is not None:
                    prov = prov + "; " + na_prov
                self["PD"].addProfileData("ZEFFEXP", zeffp, zeffpeb, provenance=prov)
                self["PD"].addProfileDerivativeData("ZEFFEXP", dzeffp, dzeffpeb)

                if self._debug:
                    print("%s - Recomputing Z-effective profile from experimental data. Added %s variable to profile data container." % (type(self)._calculateAssumedProfiles.__name__, "ZEFFEXP"))

                # Compute line-integrated Z-effective value based on bolometry integration, for calibration with flat Z-effective measurements
                zpvec = np.hstack((zeffp[:0:-1], zeffp))
                zpevec = np.hstack((zeffpeb[:0:-1], zeffpeb))
                nvec = np.hstack((self["PD"]["NE"][""][:0:-1], self["PD"]["NE"][""])) * 1.0e-18
                tvec = np.ones(zpvec.shape)
                fq = "T"
                fqi = "E"
                if fq in pddict and fqi in pddict[fq]:
                    tvec = np.hstack((self["PD"][fq+fqi][""][:0:-1], self["PD"][fq+fqi][""])) * 1.0e-3
                ocs = self["META"]["CSO"]
                ocp = self["META"]["CSOP"]
                rmajo = itemgetter(0)(self["CD"].convert(self["PD"]["X"][""], ocs, "RMAJORO", ocp, ocp))
                rmaji = itemgetter(0)(self["CD"].convert(self["PD"]["X"][""], ocs, "RMAJORI", ocp, ocp))
                rmaj = np.hstack((rmaji[:0:-1], rmajo))
                fzeffp = np.trapz(np.power(nvec, 2.0) * zpvec / np.sqrt(tvec), rmaj) / np.trapz(np.power(nvec, 2.0) / np.sqrt(tvec), rmaj)
                fzeffpeb = np.trapz(np.power(nvec, 2.0) * zpevec / np.sqrt(tvec), rmaj) / np.trapz(np.power(nvec, 2.0) / np.sqrt(tvec), rmaj)
                prov = "EX2GK: PD_NE; EX2GK: PD_TE; EX2GK: CD_"+cs_prefix+"RMAJORO; EX2GK: CD_"+cs_prefix+"RMAJORI"
                if ni_prov is not None:
                    prov = prov + "; " + ni_prov
                if nimp_prov is not None:
                    prov = prov + "; " + nimp_prov
                if na_prov is not None:
                    prov = prov + "; " + na_prov
                self["ZD"].addData("FZEFFEXP", fzeffp, fzeffpeb, provenance=prov)

                if self._debug:
                    print("%s - Flat Z-effective calculated using recomputed Z-effective profile." % (type(self)._calculateAssumedProfiles.__name__))

                # Adjust Z-effective profile if impurity profile Z-effective is greater than measured at any point, otherwise produces negative filler impurity densities
                zfilt = (zeffp > self["PD"]["ZEFF"][""])
                pzeffflag = True if np.any(zfilt) else False
                self["FLAG"].setFlag("ZEFFADJ", pzeffflag)
                if self["FLAG"].checkFlag("ZEFFADJ"):
                    if self["FLAG"].checkFlag("FLATZEFF"):
                        czeff = self["PD"]["ZEFF"][""] * self["ZD"]["FZEFFEXP"][""] / self["ZD"]["FZEFF"][""]
                        czeffeb = np.sqrt(np.power(self["PD"]["ZEFF"]["EB"], 2.0) + np.power(self["PD"]["ZEFF"][""] * self["ZD"]["FZEFFEXP"]["EB"], 2.0)) * self["ZD"]["FZEFFEXP"][""] / self["ZD"]["FZEFF"][""]
                        cdzeff = self["PD"]["ZEFF"]["GRAD"] * self["ZD"]["FZEFFEXP"][""] / self["ZD"]["FZEFF"][""]
                        cdzeffeb = np.sqrt(np.power(self["PD"]["ZEFF"]["GRADEB"], 2.0) + np.power(self["PD"]["ZEFF"]["GRAD"] * self["ZD"]["FZEFFEXP"]["EB"], 2.0)) * self["ZD"]["FZEFFEXP"][""] / self["ZD"]["FZEFF"][""]
                        prov = "EX2GK: PD_ZEFF; EX2GK: ZD_FZEFFEXP; EX2GK: ZD_FZEFF"
                        self["FLAG"].setFlag("FLATZEFF", False)
                        self["PD"].addProfileData("ZEFFADJ", czeff, czeffeb, provenance=prov)
                        self["PD"].addProfileDerivativeData("ZEFFADJ", cdzeff, cdzeffeb)

                        if self._debug:
                            print("%s - Flat Z-effective adjusted as recomputed value is larger than experimental value." % (type(self)._calculateAssumedProfiles.__name__))

                    else:
                        czeff = copy.deepcopy(self["PD"]["ZEFF"][""])
                        czeffeb = copy.deepcopy(self["PD"]["ZEFF"]["EB"])
                        cdzeff = copy.deepcopy(self["PD"]["ZEFF"]["GRAD"])
                        cdzeffeb = copy.deepcopy(self["PD"]["ZEFF"]["GRADEB"])
                        czeff[zfilt] = zeffp[zfilt] + 0.05    # A mild buffer to prevent zero density in the filler impurity (an unlikely event given current instrumentation)
                        czeffeb[zfilt] = zeffpeb[zfilt]
                        cdzeff[zfilt] = dzeffp[zfilt]     # This is not correct at all, but this branch is very unlikely
                        cdzeffeb[zfilt] = dzeffpeb[zfilt]
                        prov = self["PD"]["ZEFF"]["PROV"] if "PROV" in self["PD"]["ZEFF"] and self["PD"]["ZEFF"]["PROV"] is not None else "EX2GK: PD_ZEFF"
                        self["PD"].addProfileData("ZEFFADJ", czeff, czeffeb, provenance=prov)
                        self["PD"].addProfileDerivativeData("ZEFFADJ", cdzeff, cdzeffeb)

                        if self._debug:
                            print("%s - Z-effective profile adjusted as recomputed profile has points that are higher than experimental profile." % (type(self)._calculateAssumedProfiles.__name__))

            if "N" in pddict and "E" in pddict["N"] and "ZEFF" in self["PD"] and self["PD"]["ZEFF"][""] is not None and "ZZ1" in self["META"] and self["META"]["ZZ1"] >= 1.0:

                if self._debug:
                    print("%s - Calculating density profiles of assumed species, ..." % (type(self)._calculateAssumedProfiles.__name__))

                # Compute contributions to Z-effective by known impurities, i.e. (nimp Zimp^2 / ne |Ze|) for each impurity
                zmain = self["META"]["ZI1"] if "ZI1" in self["META"] else 1.0
                zivec = np.full(refshape, zmain)
                ziebvec = np.zeros(refshape)
                dzivec = np.zeros(refshape)
                dziebvec = np.zeros(refshape)
                ifilt = (nz_i > 1.0e-3)
                if np.any(ifilt):
                    zivec[ifilt] = nzz_i[ifilt] / nz_i[ifilt]
                    ziebvec[ifilt] = np.sqrt(np.power(nzzeb_i[ifilt] / nz_i[ifilt], 2.0) + np.power(nzz_i[ifilt] * nzeb_i[ifilt] / np.power(nz_i[ifilt], 2.0), 2.0))
                    dzivec[ifilt] = dnzz_i[ifilt] / nz_i[ifilt] - dnz_i[ifilt] * nzz_i[ifilt] / np.power(nz_i[ifilt], 2.0)
                    dzieb1 = np.power(dnzzeb_i[ifilt] / nz_i[ifilt], 2.0) + np.power(dnzz_i[ifilt] * nzeb_i[ifilt] / np.power(nz_i[ifilt], 2.0), 2.0)
                    dzieb2 = np.power(dnzeb_i[ifilt] * nzz_i[ifilt] / np.power(nz_i[ifilt], 2.0), 2.0) + np.power(dnz_i[ifilt] * nzzeb_i[ifilt] / np.power(nz_i[ifilt], 2.0), 2.0)
                    dzieb3 = np.power(dnz_i[ifilt] * nzz_i[ifilt] * nzeb_i[ifilt] / np.power(nz_i[ifilt], 3.0), 2.0)
                    dziebvec[ifilt] = np.sqrt(dzieb1 + dzieb2 + dzieb3)
                zimpvec = np.full(refshape, 7.0)
                zimpebvec = np.zeros(refshape)
                dzimpvec = np.zeros(refshape)
                dzimpebvec = np.zeros(refshape)
                impfilt = (nz_imp > 1.0e-3)
                if np.any(impfilt):
                    zimpvec[impfilt] = nzz_imp[impfilt] / nz_imp[impfilt]
                    zimpebvec[impfilt] = np.sqrt(np.power(nzzeb_imp[impfilt] / nz_imp[impfilt], 2.0) + np.power(nzz_imp[impfilt] * nzeb_imp[impfilt] / np.power(nz_imp[impfilt], 2.0), 2.0))
                    dzimpvec[impfilt] = dnzz_imp[impfilt] / nz_imp[impfilt] - dnz_imp[impfilt] * nzz_imp[impfilt] / np.power(nz_imp[impfilt], 2.0)
                    dzimpeb1 = np.power(dnzzeb_imp[impfilt] / nz_imp[impfilt], 2.0) + np.power(dnzz_imp[impfilt] * nzeb_imp[impfilt] / np.power(nz_imp[impfilt], 2.0), 2.0)
                    dzimpeb2 = np.power(dnzeb_imp[impfilt] * nzz_imp[impfilt] / np.power(nz_imp[impfilt], 2.0), 2.0) + np.power(dnz_imp[impfilt] * nzzeb_imp[impfilt] / np.power(nz_imp[impfilt], 2.0), 2.0)
                    dzimpeb3 = np.power(dnz_imp[impfilt] * nzz_imp[impfilt] * nzeb_imp[impfilt] / np.power(nz_imp[impfilt], 3.0), 2.0)
                    dzimpebvec[impfilt] = np.sqrt(dzimpeb1 + dzimpeb2 + dzimpeb3)

                # Calculate density of first assumed impurity species, given known species profiles
                #     Ignores errors in effective main ion charge, expected to be negligible or prohibitively large to be useful
                nz1t1 = self["PD"]["NE"][""] * (self["PD"]["ZEFF"][""] - zivec)
                nz1t2 = -nz_imp * (zimpvec - zivec)
                nz1 = (nz1t1 + nz1t2) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec))
                nz1ebt1 = np.power(self["PD"]["NE"]["EB"] * (self["PD"]["ZEFF"][""] - zivec), 2.0) + np.power(self["PD"]["NE"][""] * self["PD"]["ZEFF"]["EB"], 2.0)
                nz1ebt2 = np.power(nzeb_imp * (zimpvec - zivec), 2.0) + np.power(nz_imp * zimpebvec, 2.0)
                nz1eb = np.sqrt(nz1ebt1 + nz1ebt2) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec))
                dnz1t1 = self["PD"]["NE"]["GRAD"] * (self["PD"]["ZEFF"][""] - zivec) + self["PD"]["NE"][""] * (self["PD"]["ZEFF"]["GRAD"] - dzivec)
                dnz1t2 = -(dnz_imp * (zimpvec - zivec) + nz_imp * (dzimpvec - dzivec))
                dnz1t3 = dzivec * (self["PD"]["NE"][""] * (self["PD"]["ZEFF"][""] - zivec) - nz_imp * (zimpvec - zivec)) / (self["META"]["ZZ1"] - zivec)
                dnz1 = (dnz1t1 + dnz1t2 + dnz1t3) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec))
                dnz1ebt1 = np.power(self["PD"]["NE"]["GRADEB"] * (self["PD"]["ZEFF"][""] - zivec), 2.0) + np.power(self["PD"]["NE"]["GRAD"] * self["PD"]["ZEFF"]["EB"], 2.0)
                dnz1ebt2 = np.power(self["PD"]["NE"]["EB"] * (self["PD"]["ZEFF"]["GRAD"] - dzivec), 2.0) + np.power(self["PD"]["NE"][""] * self["PD"]["ZEFF"]["GRADEB"], 2.0)
                dnz1ebt3 = np.power(dnzeb_imp * (zimpvec - zivec), 2.0) + np.power(dnz_imp * zimpebvec, 2.0) + np.power(nzeb_imp * (dzimpvec - dzivec), 2.0) + np.power(nz_imp * dzimpebvec, 2.0)
                dnz1ebt4 = np.power(dziebvec * (self["PD"]["NE"][""] * (self["PD"]["ZEFF"][""] - zivec) - nz_imp * (zimpvec - zivec)) / (self["META"]["ZZ1"] - zivec), 2.0)
                dnz1ebt5 = np.power(dzivec * self["PD"]["NE"]["EB"] * (self["PD"]["ZEFF"][""] - zivec) / (self["META"]["ZZ1"] - zivec), 2.0) + np.power(dzivec * self["PD"]["NE"][""] * self["PD"]["ZEFF"]["EB"] / (self["META"]["ZZ1"] - zivec), 2.0)
                dnz1ebt6 = np.power(dzivec * nzeb_imp * (zimpvec - zivec) / (self["META"]["ZZ1"] - zivec), 2.0) + np.power(dzivec * nz_imp * zimpebvec / (self["META"]["ZZ1"] - zivec), 2.0)
                dnz1eb = np.sqrt(dnz1ebt1 + dnz1ebt2 + dnz1ebt3 + dnz1ebt4 + dnz1ebt5 + dnz1ebt6) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec))
                prov = "EX2GK: PD_NE; EX2GK: PD_ZEFF; EX2GK: META_ZZ1"
                if ni_prov is not None:
                    prov = prov + "; " + ni_prov
                if nimp_prov is not None:
                    prov = prov + "; " + nimp_prov
                self["PD"].addProfileData("NZ1", nz1, nz1eb, provenance=prov)
                self["PD"].addProfileDerivativeData("NZ1", dnz1, dnz1eb)

                if self._debug:
                    print("%s - Estimated first assumed impurity profile, %s. Added %s variable to profile data container." % (type(self)._calculateAssumedProfiles.__name__, self["META"]["MATZ1"], "NZ1"))

                # Calculate density of second assumed impurity species, if effective charge limit for first assumed impurity species is specified and exceeded
                if "ZLIM1" in self["META"] and "ZZ2" in self["META"] and self["META"]["ZZ2"] >= 1.0:
                    zz1vec = self["PD"]["NZ1"][""] * self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec) / self["PD"]["NE"][""]
                    zzfilt = (zz1vec > (self["META"]["ZLIM1"] - zivec))
                    if np.any(zzfilt):

                        # Adjust density of first impurity species where the limit is exceeded
                        #     Ignores errors in effective main ion charge, expected to be negligible or prohibitively large to be useful
                        cnz1 = copy.deepcopy(self["PD"]["NZ1"][""])
                        cnz1eb = copy.deepcopy(self["PD"]["NZ1"]["EB"])
                        cdnz1 = copy.deepcopy(self["PD"]["NZ1"]["GRAD"])
                        cdnz1eb = copy.deepcopy(self["PD"]["NZ1"]["GRADEB"])
                        cnz1[zzfilt] = self["PD"]["NE"][""][zzfilt] * (self["META"]["ZLIM1"] - zivec[zzfilt]) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec[zzfilt]))
                        cnz1eb[zzfilt] = self["PD"]["NE"]["EB"][zzfilt] * (self["META"]["ZLIM1"] - zivec[zzfilt]) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec[zzfilt]))
                        cdnz1[zzfilt] = (self["PD"]["NE"]["GRAD"][zzfilt] * (self["META"]["ZLIM1"] - zivec[zzfilt]) - self["PD"]["NE"][""][zzfilt] * dzivec[zzfilt]) / (self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec[zzfilt]))
                        cdnz1eb[zzfilt] = self["PD"]["NE"]["GRADEB"][zzfilt]
                        prov = "EX2GK: PD_NE; EX2GK: PD_ZEFF; EX2GK: META_ZZ1; EX2GK: META_ZLIM1"
                        if ni_prov is not None:
                            prov = prov + "; " + ni_prov
                        if nimp_prov is not None:
                            prov = prov + "; " + nimp_prov
                        self["PD"].addProfileData("NZ1", cnz1, cnz1eb, provenance=prov)
                        self["PD"].addProfileDerivativeData("NZ1", cdnz1, cdnz1eb)

                        # Calculate density of second assumed impurity species, given known species profiles and 
                        #     Ignores errors in effective main ion charge, expected to be negligible or prohibitively large to be useful
                        nz2t1 = self["PD"]["NE"][""] * (self["PD"]["ZEFF"][""] - zivec)
                        nz2t2 = -nz_imp * (zimpvec - zivec)
                        nz2t3 = -self["PD"]["NZ1"][""] * self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec)
                        nz2 = (nz2t1 + nz2t2 + nz2t3) / (self["META"]["ZZ2"] * (self["META"]["ZZ2"] - zivec))
                        nz2ebt1 = np.power(self["PD"]["NE"]["EB"] * (self["PD"]["ZEFF"][""] - zivec), 2.0) + np.power(self["PD"]["NE"][""] * self["PD"]["ZEFF"]["EB"], 2.0)
                        nz2ebt2 = np.power(nzeb_imp * (zimpvec - zivec), 2.0) + np.power(nz_imp * zimpebvec, 2.0)
                        nz2ebt3 = np.power(self["PD"]["NZ1"]["EB"] * self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec), 2.0)
                        nz2eb = np.sqrt(nz2ebt1 + nz2ebt2 + nz2ebt3) / (self["META"]["ZZ2"] * (self["META"]["ZZ2"] - zivec))
                        dnz2t1 = self["PD"]["NE"]["GRAD"] * (self["PD"]["ZEFF"][""] - zivec) + self["PD"]["NE"][""] * (self["PD"]["ZEFF"]["GRAD"] - dzivec)
                        dnz2t2 = -(dnz_imp * (zimpvec - zivec) + nz_imp * (dzimpvec - dzivec))
                        dnz2t3 = -(self["PD"]["NZ1"]["GRAD"] * self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec) - self["PD"]["NZ1"][""] * self["META"]["ZZ1"] * dzivec)
                        dnz2t4 = dzivec * (self["PD"]["NE"][""] * (self["PD"]["ZEFF"][""] - zivec) - nz_imp * (zimpvec - zivec)) / (self["META"]["ZZ1"] - zivec)
                        dnz2 = (dnz2t1 + dnz2t2 + dnz2t3 + dnz2t4) / (self["META"]["ZZ2"] * (self["META"]["ZZ2"] - zivec))
                        dnz2ebt1 = np.power(self["PD"]["NE"]["GRADEB"] * (self["PD"]["ZEFF"][""] - zivec), 2.0) + np.power(self["PD"]["NE"]["GRAD"] * self["PD"]["ZEFF"]["EB"], 2.0)
                        dnz2ebt2 = np.power(self["PD"]["NE"]["EB"] * (self["PD"]["ZEFF"]["GRAD"] - dzivec), 2.0) + np.power(self["PD"]["NE"][""] * self["PD"]["ZEFF"]["GRADEB"], 2.0)
                        dnz2ebt3 = np.power(dnzeb_imp * (zimpvec - zivec), 2.0) + np.power(dnz_imp * zimpebvec, 2.0) + np.power(nzeb_imp * (dzimpvec - dzivec), 2.0) + np.power(nz_imp * dzimpebvec, 2.0)
                        dnz2ebt4 = np.power(self["PD"]["NZ1"]["GRADEB"] * self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec), 2.0) + np.power(self["PD"]["NZ1"]["EB"] * self["META"]["ZZ1"] * dzivec, 2.0)
                        dnz2ebt5 = np.power(dziebvec * (self["PD"]["NE"][""] * (self["PD"]["ZEFF"][""] - zivec) - nz_imp * (zimpvec - zivec)) / (self["META"]["ZZ2"] - zivec), 2.0)
                        dnz2ebt6 = np.power(dzivec * self["PD"]["NE"]["EB"] * (self["PD"]["ZEFF"][""] - zivec) / (self["META"]["ZZ2"] - zivec), 2.0) + np.power(dzivec * self["PD"]["NE"][""] * self["PD"]["ZEFF"]["EB"] / (self["META"]["ZZ2"] - zivec), 2.0)
                        dnz2ebt7 = np.power(dzivec * nzeb_imp * (zimpvec - zivec) / (self["META"]["ZZ2"] - zivec), 2.0) + np.power(dzivec * nz_imp * zimpebvec / (self["META"]["ZZ2"] - zivec), 2.0)
                        dnz2ebt8 = np.power(dzivec * self["PD"]["NZ1"]["EB"] * self["META"]["ZZ1"] * (self["META"]["ZZ1"] - zivec) / (self["META"]["ZZ2"] - zivec), 2.0)
                        dnz2eb = np.sqrt(dnz2ebt1 + dnz2ebt2 + dnz2ebt3 + dnz2ebt4 + dnz2ebt5 + dnz2ebt6 + dnz2ebt7 + dnz2ebt8) / (self["META"]["ZZ2"] * (self["META"]["ZZ2"] - zivec))
                        prov = "EX2GK: PD_NE; EX2GK: PD_ZEFF; EX2GK: PD_NZ1; EX2GK: META_ZZ2"
                        if ni_prov is not None:
                            prov = prov + "; " + ni_prov
                        if nimp_prov is not None:
                            prov = prov + "; " + nimp_prov
                        self["PD"].addProfileData("NZ2", nz2, nz2eb, provenance=prov)
                        self["PD"].addProfileDerivativeData("NZ2", dnz2, dnz2eb)

                    else:
                        nz2 = np.zeros(refshape)
                        nz2eb = np.zeros(refshape)
                        dnz2 = np.zeros(refshape)
                        dnz2eb = np.zeros(refshape)
                        prov = "EX2GK: Internal assumption"
                        self["PD"].addProfileData("NZ2", nz2, nz2eb)
                        self["PD"].addProfileDerivativeData("NZ2", dnz2, dnz2eb)

                    if self._debug:
                        print("%s - Estimated second assumed impurity profile, %s. Added %s variable to profile data container." % (type(self)._calculateAssumedProfiles.__name__, self["META"]["MATZ2"], "NZ2"))
#You are here
            if "N" in pddict and "E" in pddict["N"]:

                if self._debug:
                    print("%s - Recalculating quasineutrality and Z-effective including measured and assumed species..." % (type(self)._calculateAssumedProfiles.__name__))

                # Assumed impurity ion data containers for Zeff calculation
                nz_z = np.zeros(refshape)
                nzeb_z = np.zeros(refshape)
                dnz_z = np.zeros(refshape)
                dnzeb_z = np.zeros(refshape)
                nzz_z = np.zeros(refshape)
                nzzeb_z = np.zeros(refshape)
                dnzz_z = np.zeros(refshape)
                dnzzeb_z = np.zeros(refshape)
                nz_prov = None

                for zz in range(0, 2):
                    ztag = "%d" % (zz+1)
                    if "ZZ"+ztag in self["META"] and "NZ"+ztag in self["PD"]:
                        nz_z += self["PD"]["NZ"+ztag][""] * self["META"]["ZZ"+ztag]
                        nzeb_z += self["PD"]["NZ"+ztag]["EB"] * self["META"]["ZZ"+ztag]
                        dnz_z += self["PD"]["NZ"+ztag]["GRAD"] * self["META"]["ZZ"+ztag]
                        dnzeb_z += self["PD"]["NZ"+ztag]["GRADEB"] * self["META"]["ZZ"+ztag]
                        nzz_z += self["PD"]["NZ"+ztag][""] * np.power(self["META"]["ZZ"+ztag], 2.0)
                        nzzeb_z += self["PD"]["NZ"+ztag]["EB"] * np.power(self["META"]["ZZ"+ztag], 2.0)
                        dnzz_z += self["PD"]["NZ"+ztag]["GRAD"] * np.power(self["META"]["ZZ"+ztag], 2.0)
                        dnzzeb_z += self["PD"]["NZ"+ztag]["GRADEB"] * np.power(self["META"]["ZZ"+ztag], 2.0)
                        nz_prov = nz_prov + "; EX2GK: PD_NZ"+ztag + "; META_ZZ"+ztag if nz_prov is not None else "EX2GK: PD_NZ"+ztag + "; EX2GK: META_ZZ"+ztag

                # Calculate main ion densities, if not already present due to presence of experimental data
                nz_ai = np.zeros(refshape)
                nzeb_ai = np.zeros(refshape)
                dnz_ai = np.zeros(refshape)
                dnzeb_ai = np.zeros(refshape)
                nzz_ai = np.zeros(refshape)
                nzzeb_ai = np.zeros(refshape)
                dnzz_ai = np.zeros(refshape)
                dnzzeb_ai = np.zeros(refshape)
                na_prov = None
                if itotweight > 0.0:
                    fnzi = self["PD"]["NE"][""] - nz_i - nz_imp - nz_z
                    fnzieb = np.sqrt(np.power(self["PD"]["NE"]["EB"], 2.0) + np.power(nzeb_i, 2.0) + np.power(nzeb_imp, 2.0) + np.power(nzeb_z, 2.0))
                    fdnzi = self["PD"]["NE"]["GRAD"] - dnz_i - dnz_imp - dnz_z
                    fdnzieb = np.sqrt(np.power(self["PD"]["NE"]["GRADEB"], 2.0) + np.power(dnzeb_i, 2.0) + np.power(dnzeb_imp, 2.0) + np.power(dnzeb_z, 2.0))
                    fprov = ni_prov
                    if nimp_prov is not None:
                        fprov = fprov + "; " + nimp_prov if fprov is not None else nimp_prov
                    if nz_prov is not None:
                        fprov = fprov + "; " + nz_prov if fprov is not None else nz_prov
                    for idx in range(0, numion):
                        itag = "%d" % (idx+1)
                        weight = self["META"]["WGTI"+itag] if "WGTI"+itag in self["META"] else 1.0 / float(numion)
                        if "I"+itag not in pddict["N"] and "ZI"+itag in self["META"]:
                            ifrac = self["META"]["WGTI"+itag] / itotweight
                            nzi = ifrac * fnzi
                            nzieb = ifrac * fnzieb
                            dnzi = ifrac * fdnzi
                            dnzieb = ifrac * fdnzieb
                            nz_ai += nzi
                            nzeb_ai += nzieb
                            dnz_ai += dnzi
                            dnzeb_ai += dnzieb
                            nzz_ai += nzi * self["META"]["ZI"+itag]
                            nzzeb_ai += nzieb * self["META"]["ZI"+itag]
                            dnzz_ai += dnzi * self["META"]["ZI"+itag]
                            dnzzeb_ai += dnzieb * self["META"]["ZI"+itag]
                            na_prov = na_prov + "; EX2GK: META_ZI"+itag if na_prov is not None else "EX2GK: META_ZI"+itag
                            prov = "EX2GK: META_ZI"+itag
                            if fprov is not None:
                                prov = prov + "; " + fprov
                            self["PD"].addProfileData("NI"+itag, nzi, nzieb, provenance=prov)
                            self["PD"].addProfileDerivativeData("NI"+itag, dnzi, dnzieb)

                            if self._debug:
                                print("%s - Estimated main ion species density with weight fraction. Added %s variable to profile data container." % (type(self)._calculateAssumedProfiles.__name__, "NI"+itag))

                #fnzit1 = self["PD"]["NE"][""] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""])
                #fnzit2 = -nz_imp * (self["META"]["ZZ1"] - zimpvec)
                #fnzit3 = self["PD"]["NZ2"][""] * self["META"]["ZZ2"] * (self["META"]["ZZ2"] - self["META"]["ZZ1"]) if "NZ2" in self["PD"] else np.zeros(refshape)
                #fnzi = (fnzit1 + fnzit2 + fnzit3) / (self["META"]["ZZ1"] - zivec)
                #fnziebt1 = np.power(self["PD"]["NE"]["EB"] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""]), 2.0) + np.power(self["PD"]["NE"][""] * self["PD"]["ZEFF"]["EB"], 2.0)
                #fnziebt2 = np.power(nzeb_imp * (self["META"]["ZZ1"] - zimpvec), 2.0) + np.power(nz_imp * zimpebvec, 2.0)
                #fnziebt3 = np.power(self["PD"]["NZ2"]["EB"] * self["META"]["ZZ2"] * (self["META"]["ZZ2"] - self["META"]["ZZ1"]), 2.0) if "NZ2" in self["PD"] else np.zeros(self["PD"]["X"][""].shape)
                #fnzieb = np.sqrt(fnziebt1 + fnziebt2 + fnziebt3) / (self["META"]["ZZ1"] - zivec)
                #fdnzit1 = self["PD"]["NE"]["GRAD"] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""]) - self["PD"]["NE"][""] * self["PD"]["ZEFF"]["GRAD"]
                #fdnzit2 = -(dnz_imp * (self["META"]["ZZ1"] - zimpvec) - nz_imp * dzimpvec)
                #fdnzit3 = self["PD"]["NZ2"]["GRAD"] * self["META"]["ZZ2"] * (self["META"]["ZZ2"] - self["META"]["ZZ1"]) if "NZ2" in self["PD"] else np.zeros(self["PD"]["X"][""].shape)
                #fdnzit4 = dzivec * (self["PD"]["NE"][""] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""]) - nz_imp * (self["META"]["ZZ1"] - zimpvec)) / (self["META"]["ZZ1"] - zivec) 
                #fdnzit5 = dzivec * self["PD"]["NZ2"][""] * self["META"]["ZZ2"] * (self["META"]["ZZ2"] - self["META"]["ZZ1"]) / (self["META"]["ZZ1"] - zivec) if "NZ2" in self["PD"] else np.zeros(self["PD"]["X"][""].shape)
                #fdnzi = (fdnzit1 + fdnzit2 + fdnzit3 + fdnzit4 + fdnzit5) / (self["META"]["ZZ1"] - zivec)
                #fdnziebt1 = np.power(self["PD"]["NE"]["GRADEB"] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""]), 2.0) + np.power(self["PD"]["NE"]["GRAD"] * self["PD"]["ZEFF"]["EB"], 2.0)
                #fdnziebt2 = np.power(self["PD"]["NE"]["EB"] * self["PD"]["ZEFF"]["GRAD"], 2.0) + np.power(self["PD"]["NE"][""] * self["PD"]["ZEFF"]["GRADEB"], 2.0)
                #fdnziebt3 = np.power(dnzeb_imp * (self["META"]["ZZ1"] - zimpvec), 2.0) + np.power(dnz_imp * zimpebvec, 2.0) + np.power(nzeb_imp * dzimpvec, 2.0) + np.power(nz_imp * dzimpebvec, 2.0)
                #fdnziebt4 = np.power(self["PD"]["NZ2"]["GRADEB"] * self["META"]["ZZ2"] * (self["META"]["ZZ2"] - self["META"]["ZZ1"]), 2.0) if "NZ2" in self["PD"] else np.zeros(self["PD"]["X"][""].shape)
                #fdnziebt5 = np.power(dziebvec * (self["PD"]["NE"][""] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""]) - nz_imp * (self["META"]["ZZ1"] - zimpvec)) / (self["META"]["ZZ1"] - zivec), 2.0)
                #fdnziebt6 = np.power(dzivec * self["PD"]["NE"]["EB"] * (self["META"]["ZZ1"] - self["PD"]["ZEFF"][""]) / (self["META"]["ZZ1"] - zivec), 2.0) + np.power(dzivec * self["PD"]["NE"][""] * self["PD"]["ZEFF"]["EB"] / (self["META"]["ZZ1"] - zivec), 2.0)
                #fdnziebt7 = np.power(dzivec * nzeb_imp * (self["META"]["ZZ1"] - zimpvec) / (self["META"]["ZZ1"] - zivec), 2.0) + np.power(dzivec * nz_imp * zimpebvec / (self["META"]["ZZ1"] - zivec), 2.0)
                #fdnziebt8 = np.power(dzivec * self["PD"]["NZ2"]["EB"] * self["META"]["ZZ2"] * (self["META"]["ZZ2"] - self["META"]["ZZ1"]) / (self["META"]["ZZ1"] - zivec), 2.0) if "NZ2" in self["PD"] else np.zeros(self["PD"]["X"][""].shape)
                #fdnzieb = np.sqrt(fdnziebt1 + fdnziebt2 + fdnziebt3 + fdnziebt4 + fdnziebt5 + fdnziebt6 + fdnziebt7 + fdnziebt8) / (self["META"]["ZZ1"] - zivec)

                #for idx in range(0, numion):
                #    itag = "%d" % (idx+1)
                #    if "I"+itag not in pddict["N"] and "WGTI"+itag in self["META"] and "ZI"+itag in self["META"]:
                #        ifrac = self["META"]["WGTI"+itag] / itotweight if itotweight > 0.0 else self["META"]["WGTI"+itag] / float(numion)
                #        nzi = ifrac * fnzi
                #        nzieb = ifrac * fnzieb
                #        dnzi = ifrac * fdnzi
                #        dnzieb = ifrac * fdnzieb
                #        self["PD"].addProfileData("NI"+itag, nzi, nzieb)
                #        self["PD"].addProfileDerivativeData("NI"+itag, dnzi, dnzieb)

                # Calculate Z-effective profile from known and assumed profiles, computes line-integrated value for calibration with flat Z-effective measurements
                cne = copy.deepcopy(self["PD"]["NE"][""])
                zeffp = (nzz_i + nzz_imp + nzz_z + nzz_ai) / cne
                zeffpeb = np.sqrt((np.power(nzzeb_i / cne, 2.0) + np.power(nzzeb_imp / cne, 2.0) + np.power(nzzeb_z / cne, 2.0) + np.power(nzzeb_ai / cne, 2.0)) + np.power(zeffp * self["PD"]["NE"]["EB"] / cne, 2.0))
                dzeffp = (dnzz_i + dnzz_imp + dnzz_z + dnzz_ai - zeffp * self["PD"]["NE"]["GRAD"]) / cne
                dzeffp[np.abs(dzeffp) < 1.0e-12] = 0.0   # Enforce zero when near flat to avoid numerical issues
                dzeffpeb1 = np.power(dnzzeb_i / cne, 2.0) + np.power(dnzzeb_imp / cne, 2.0) + np.power(dnzzeb_z / cne, 2.0) + np.power(dnzzeb_ai / cne, 2.0) + np.power(((dnzz_i + dnzz_imp + dnzz_z + dnzz_ai) / cne) * self["PD"]["NE"]["EB"] / cne, 2.0)
                dzeffpeb2 = np.power(zeffpeb * self["PD"]["NE"]["GRAD"] / cne, 2.0) + np.power(zeffp * self["PD"]["NE"]["GRADEB"] / cne, 2.0) + np.power((zeffp * self["PD"]["NE"]["GRAD"] / cne) * self["PD"]["NE"]["EB"] / cne, 2.0)
                dzeffpeb = np.sqrt(dzeffpeb1 + dzeffpeb2)
                prov = "EX2GK: PD_NE"
                if ni_prov is not None:
                    prov = prov + "; " + ni_prov
                if nimp_prov is not None:
                    prov = prov + "; " + nimp_prov
                if nz_prov is not None:
                    prov = prov + "; " + nz_prov
                if na_prov is not None:
                    prov = prov + "; " + na_prov
                self["PD"].addProfileData("ZEFFP", zeffp, zeffpeb, provenance=prov)
                self["PD"].addProfileDerivativeData("ZEFFP", dzeffp, dzeffpeb)

                # Enforce self-consistent Z-effective profile if no assumed impurities are given, overwrites existing Z-effective profile
                if not ("ZZ1" in self["META"] and self["META"]["ZZ1"] >= 1.0):
                    self["PD"].addProfileData("ZEFF", zeffp, zeffpeb, provenance=prov)
                    self["PD"].addProfileDerivativeData("ZEFF", dzeffp, dzeffpeb)

                    if self._debug:
                        print("%s - Overwrite %s variable in profile data container with recomputed Z-effective profile." % (type(self)._calculateAssumedProfiles.__name__, "ZEFF"))

            else:
                warnings.warn("Electron density data not found, %s ion density estimation calculations not performed" % (type(self).__name__))

            # Estimates assumed impurity ion and main ion temperature profiles
            if "T" in pddict and "E" in pddict["T"]:

                if self._debug:
                    print("%s - Determining ion temperatures for main ion and impurity species where missing..." % (type(self)._calculateAssumedProfiles.__name__))

                rddict = self["RD"].getPresentFields('dict') if "RD" in self else {"T": ["IMP"]}

                trep = None
                trepeb = None
                dtrep = None
                dtrepeb = None
                rstr = ""
                tprov = None
                # Reference temperature profile set to first available impurity ion temperature profile, if any, else electron temperature profile
                for idx in range(0, numimp):
                    itag = "%d" % (idx+1)
                    if trep is None and "IMP"+itag in pddict["T"] and ("IMP" in rddict["T"] or "IMP"+itag in rddict["T"]):
                        trep = copy.deepcopy(self["PD"]["TIMP"+itag][""])
                        trepeb = copy.deepcopy(self["PD"]["TIMP"+itag]["EB"])
                        dtrep = copy.deepcopy(self["PD"]["TIMP"+itag]["GRAD"])
                        dtrepeb = copy.deepcopy(self["PD"]["TIMP"+itag]["GRADEB"])
                        rstr = "measured impurity species %d" % (idx)
                        if "PROV" in self["PD"]["TIMP"+itag] and self["PD"]["TIMP"+itag]["PROV"] is not None:
                            tprov = self["PD"]["TIMP"+itag]["PROV"]
                if trep is None and "Z1" in pddict["T"] and "IMP" in rddict["T"]:
                    trep = copy.deepcopy(self["PD"]["TZ1"][""])
                    trepeb = copy.deepcopy(self["PD"]["TZ1"]["EB"])
                    dtrep = copy.deepcopy(self["PD"]["TZ1"]["GRAD"])
                    dtrepeb = copy.deepcopy(self["PD"]["TZ1"]["GRADEB"])
                    rstr = "assumed impurity species 1"
                    if "PROV" in self["PD"]["TZ1"] and self["PD"]["TZ1"]["PROV"] is not None:
                        tprov = self["PD"]["TZ1"]["PROV"]
                if trep is None and "TEMP" in pddict["T"]:
                    trep = copy.deepcopy(self["PD"]["TTEMP"][""])
                    trepeb = copy.deepcopy(self["PD"]["TTEMP"]["EB"])
                    dtrep = copy.deepcopy(self["PD"]["TTEMP"]["GRAD"])
                    dtrepeb = copy.deepcopy(self["PD"]["TTEMP"]["GRADEB"])
                    rstr = "combined impurity species measurements"
                    if "PROV" in self["PD"]["TTEMP"] and self["PD"]["TTEMP"]["PROV"] is not None:
                        tprov = self["PD"]["TTEMP"]["PROV"]
                    del self["PD"]["TTEMP"]
                    warnings.warn("Temporary impurity temperature field, %s, deleted to not cause future processing issues" % ("TTEMP"))
                if trep is None:
                    trep = copy.deepcopy(self["PD"]["TE"][""])
                    trepeb = copy.deepcopy(self["PD"]["TE"]["EB"])
                    dtrep = copy.deepcopy(self["PD"]["TE"]["GRAD"])
                    dtrepeb = copy.deepcopy(self["PD"]["TE"]["GRADEB"])
                    rstr = "electron temperature measurements"
                    if "PROV" in self["PD"]["TE"] and self["PD"]["TE"]["PROV"] is not None:
                        tprov = self["PD"]["TE"]["PROV"]
                if tprov is None:
                    tprov = "EX2GK: Internal assumption"

                if trep is not None:
                    # Set unspecified impurity ion temperatures to reference temperature profile
                    mstr = ""
                    for idx in range(0, numimp):
                        itag = "%d" % (idx+1)
                        if "IMP"+itag not in pddict["T"]:
                            self["PD"].addProfileData("TIMP"+itag, trep, trepeb, provenance=tprov)
                            self["PD"].addProfileDerivativeData("TIMP"+itag, dtrep, dtrepeb)
                            if mstr:
                                mstr += ", "
                            mstr += "TIMP"+itag

                    if self._debug and mstr:
                        print("%s - Insufficient impurity temperature measurements found. Estimated %s using %s." % (type(self)._calculateAssumedProfiles.__name__, mstr, rstr))

                    mstr = ""
                    if "ZZ1" in self["META"] and self["META"]["ZZ1"] >= 1.0 and "Z1" not in pddict["T"]:
                        self["PD"].addProfileData("TZ1", trep, trepeb, provenance=tprov)
                        self["PD"].addProfileDerivativeData("TZ1", dtrep, dtrepeb)
                        if mstr:
                            mstr += ", "
                        mstr += "TZ1"
                    if "ZZ2" in self["META"] and self["META"]["ZZ2"] >= 1.0 and "Z2" not in pddict["T"]:
                        self["PD"].addProfileData("TZ2", trep, trepeb, provenance=tprov)
                        self["PD"].addProfileDerivativeData("TZ2", dtrep, dtrepeb)
                        if mstr:
                            mstr += ", "
                        mstr += "TZ2"

                    if self._debug and mstr:
                        print("%s - Setting temperature profiles for assumed impurity species. Estimated %s using %s." % (type(self)._calculateAssumedProfiles.__name__, mstr, rstr))

                    # Scale reference temperature profile if main ion temperature scaling measurements are provided
                    ti = trep.copy()
                    tieb = trepeb.copy()
                    dti = dtrep.copy()
                    dtieb = dtrepeb.copy()
                    if "RD" in self and numion > 0 and self["FLAG"].checkFlag("TISCALE") and "T" in rddict:
                        vtag = "TI1" if "I1" in rddict["T"] else "TI"
                        ocs = self["META"]["CSO"]
                        preocs = self["META"]["CSOP"]
                        xcs = ocs
                        prexcs = preocs
                        if self["RD"][vtag].coord_prefix+self["RD"][vtag].coordinate in self["CD"].coord_keys():
                            xcs = self["RD"][vtag].coordinate
                            prexcs = self["RD"][vtag].coord_prefix
                        (xn, xjac, xerr) = self["CD"].convert(self["RD"][vtag]["X"], xcs, ocs, prexcs, preocs)
                        nfilt = np.all([np.isfinite(self["PD"]["X"][""]), np.isfinite(ti)], axis=0)
                        if np.count_nonzero(nfilt) > 1:
                            tfunc = interp1d(self["PD"]["X"][""][nfilt], ti[nfilt], bounds_error=False, fill_value='extrapolate')
                            tibase = tfunc(xn)
                            tisc = np.average(self["RD"][vtag][""] / tibase)
                            tiebav = np.average(self["RD"][vtag]["EB"]) / 2.0
                            ti = ti * tisc
                            tieb = np.sqrt(np.power(tieb, 2.0) + np.power(tiebav, 2.0))
                            dti = dti * tisc
                            rstr = "scaled " + rstr
                            tprov = tprov + "; " + self["RD"][vtag]["PROV"]
                        else:
                            warnings.warn("Reference temperature data appears corrupted, %s ion temperature estimation calculations not performed" % (type(self).__name__))

                    # Set unspecified main ion temperatures to reference temperature profile
                    mstr = ""
                    for idx in range(0, numion):
                        itag = "%d" % (idx+1)
                        if "I"+itag not in pddict["T"]:
                            self["PD"].addProfileData("TI"+itag, ti, tieb, provenance=tprov)
                            self["PD"].addProfileDerivativeData("TI"+itag, dti, dtieb)
                            if mstr:
                                mstr += ", "
                            mstr += "TI"+itag

                    if self._debug and mstr:
                        print("%s - Insufficient main ion temperature measurements found. Estimated %s using %s." % (type(self)._calculateAssumedProfiles.__name__, mstr, rstr))

                else:
                    warnings.warn("Reference temperature data not identified, %s ion temperature estimation calculations not performed" % (type(self).__name__))

            else:
                warnings.warn("Electron temperature data not found, %s ion temperature estimation calculations not performed" % (type(self).__name__))

        else:
            warnings.warn("%s object is not ready for calculation of assumed density and temperature profiles" % (type(self).__name__))

    def _calculateMiscellaneousQuantities(self):
        """
        Calculate common miscellaneous quantities derived from
        fitted profile quantities. Should be filled sparingly
        due to prevent unnecessary cluttering of data object.

        Developer note: If possible, defer all quantities to
        the code adapters. Only place here things that are
        effectively universal in all foreseen adapters. All
        quantities to check the data sanity should be placed
        into phystools.py.

        :returns: dict. Object with identical structure as input object except with standardized fields for miscellaneous quantities.
        """
        pddict = self["PD"].getPresentFields('dict')

        if "META" in self and self["META"].isReady() and pddict and "X" in pddict:

            # Required constants
            ee = 1.60217662e-19
            eps0 = 8.85418782e-12
            mu0 = 4.0e-7 * np.pi
            me = 9.10938356e-31

            numimp = self["META"]["NUMIMP"]
            numion = self["META"]["NUMI"]
            numz = self["META"]["NUMZ"]
            numfi = self["META"]["NUMFI"]

            refshape = self["PD"]["X"][""].shape
            cs_prefix = self["META"]["CSOP"]

            # Calculates temperature ratio, Ti/Te
#            if "TI" in DAT and "TE" in DAT and DAT["TI"] is not None and DAT["TE"] is not None:
#                zfilt = (DAT["TE"] > 0.0)
#                DAT["TRATIO"] = np.zeros(DAT["TE"].shape)
#                DAT["TRATIO"][zfilt] = DAT["TI"][zfilt] / DAT["TE"][zfilt]
#                DAT["TRATIO"] = DAT["TI"] / DAT["TE"]

            # Determines shape parameters from magnetic geometry data, if present (not implemented yet)
#            if "ELON" in DAT and DAT["ELON"] is not None and "ELAX" in DAT and DAT["ELAX"] is not None:
#                mkappa = DAT["ELON"] - DAT["ELAX"]
#                DAT["KAPPA"] = mkappa * DAT[gcs] + DAT["ELAX"]
#                DAT["SKAPPA"] = rgeo * mkappa / DAT["KAPPA"]
#            if "TRIU" in DAT and DAT["TRIU"] is not None:
#                if "TRIL" in DAT and DAT["TRIL"] is not None:
#                    DAT["DELTA"] = np.ones(DAT[gcs].shape) * (DAT["TRIU"] + DAT["TRIL"]) / 2.0
#                    DAT["SDELTA"] = np.full(DAT[gcs].shape, 0.0)
#                else:
#                    DAT["DELTA"] = np.ones(DAT[gcs].shape) * DAT["TRIU"]
#                    DAT["SDELTA"] = np.full(DAT[gcs].shape, 0.0)
#            elif "TRIL" in DAT and DAT["TRIL"] is not None:
#                DAT["DELTA"] = np.ones(DAT[gcs].shape) * DAT["TRIL"]
#                DAT["SDELTA"] = np.full(DAT[gcs].shape, 0.0)
#            DAT["ZETA"] = np.full(DAT[gcs].shape, 0.0)
#            DAT["SZETA"] = np.full(DAT[gcs].shape, 0.0)

            # Calculate Debye length
            if "N" in pddict and "E" in pddict["N"] and "T" in pddict and "E" in pddict["T"]:
                debye = np.zeros(refshape)
                debyeeb = np.zeros(refshape)
                ddebye = np.zeros(refshape)
                ddebyeeb = np.zeros(refshape)
                prov = "EX2GK: PD_NE; EX2GK: PD_TE"
                zfilt = (self["PD"]["TE"][""] > 0.0)
                if np.any(zfilt):
                    z2 = 1.0
                    tdebye = ee * z2 * self["PD"]["NE"][""][zfilt] / self["PD"]["TE"][""][zfilt]
                    tdebyeeb = np.sqrt(np.power(ee * z2 * self["PD"]["NE"]["EB"][zfilt] / self["PD"]["TE"][""][zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NE"][""][zfilt] * self["PD"]["TE"]["EB"][zfilt] / np.power(self["PD"]["TE"][""][zfilt], 2.0), 2.0))
                    debye[zfilt] = debye[zfilt] + tdebye
                    debyeeb[zfilt] = np.sqrt(np.power(debyeeb[zfilt], 2.0) + np.power(tdebyeeb, 2.0))
                    ddebye[zfilt] = ddebye[zfilt] + ee * z2 * self["PD"]["NE"]["GRAD"][zfilt] / self["PD"]["TE"][""][zfilt] - tdebye * self["PD"]["TE"]["GRAD"][zfilt] / self["PD"]["TE"][""][zfilt]
                    ddebyeeb[zfilt] = np.sqrt(np.power(ddebyeeb[zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NE"]["GRADEB"][zfilt] / self["PD"]["TE"][""][zfilt], 2.0) \
	                                      + np.power(ee * z2 * self["PD"]["NE"]["GRAD"][zfilt] * self["PD"]["TE"]["EB"][zfilt] / np.power(self["PD"]["TE"][""][zfilt], 2.0), 2.0) \
	                                      + np.power(tdebyeeb * self["PD"]["TE"]["GRAD"][zfilt] / self["PD"]["TE"][""][zfilt], 2.0) \
                                              + np.power(tdebye * self["PD"]["TE"]["GRADEB"][zfilt] / self["PD"]["TE"][""][zfilt], 2.0) \
                                              + np.power(tdebye * self["PD"]["TE"]["GRAD"][zfilt] * self["PD"]["TE"]["EB"][zfilt] / np.power(self["PD"]["TE"][""][zfilt], 2.0), 2.0))
                for idx in range(0, numion):
                    itag = "%d" % (idx+1)
                    if "I"+itag in pddict["N"] and "I"+itag in pddict["T"] and "ZI"+itag in self["META"]:
                        zfilt = (self["PD"]["TI"+itag][""] > 0.0)
                        if np.any(zfilt):
                            z2 = np.power(self["META"]["ZI"+itag], 2.0)
                            tdebye = ee * z2 * self["PD"]["NI"+itag][""][zfilt] / self["PD"]["TI"+itag][""][zfilt]
                            tdebyeeb = np.sqrt(np.power(ee * z2 * self["PD"]["NI"+itag]["EB"][zfilt] / self["PD"]["TI"+itag][""][zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NI"+itag][""][zfilt] * self["PD"]["TI"+itag]["EB"][zfilt] / np.power(self["PD"]["TI"+itag][""][zfilt], 2.0), 2.0))
                            debye[zfilt] = debye[zfilt] + tdebye
                            debyeeb[zfilt] = np.sqrt(np.power(debyeeb[zfilt], 2.0) + np.power(tdebyeeb, 2.0))
                            ddebye[zfilt] = ddebye[zfilt] + ee * z2 * self["PD"]["NI"+itag]["GRAD"][zfilt] / self["PD"]["TI"+itag][""][zfilt] - tdebye * self["PD"]["TI"+itag]["GRAD"][zfilt] / self["PD"]["TI"+itag][""][zfilt]
                            ddebyeeb[zfilt] = np.sqrt(np.power(ddebyeeb[zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NI"+itag]["GRADEB"][zfilt] / self["PD"]["TI"+itag][""][zfilt], 2.0) \
	                                              + np.power(ee * z2 * self["PD"]["NI"+itag]["GRAD"][zfilt] * self["PD"]["TI"+itag]["EB"][zfilt] / np.power(self["PD"]["TI"+itag][""][zfilt], 2.0), 2.0) \
	                                              + np.power(tdebyeeb * self["PD"]["TI"+itag]["GRAD"][zfilt] / self["PD"]["TI"+itag][""][zfilt], 2.0) \
                                                      + np.power(tdebye * self["PD"]["TI"+itag]["GRADEB"][zfilt] / self["PD"]["TI"+itag][""][zfilt], 2.0) \
                                                      + np.power(tdebye * self["PD"]["TI"+itag]["GRAD"][zfilt] * self["PD"]["TI"+itag]["EB"][zfilt] / np.power(self["PD"]["TI"+itag][""][zfilt], 2.0), 2.0))
                            prov = prov + "; EX2GK: PD_NI"+itag + "; EX2GK: PD_TI"+itag + "; EX2GK: META_ZI"+itag
                for idx in range(0, numimp):
                    itag = "%d" % (idx+1)
                    if "IMP"+itag in pddict["N"] and "IMP"+itag in pddict["T"] and "ZIMP"+itag in self["META"]:
                        zfilt = (self["PD"]["TIMP"+itag][""] > 0.0)
                        if np.any(zfilt):
                            z2 = np.power(self["META"]["ZIMP"+itag], 2.0)
                            tdebye = ee * z2 * self["PD"]["NIMP"+itag][""][zfilt] / self["PD"]["TIMP"+itag][""][zfilt]
                            tdebyeeb = np.sqrt(np.power(ee * z2 * self["PD"]["NIMP"+itag]["EB"][zfilt] / self["PD"]["TIMP"+itag][""][zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NIMP"+itag][""][zfilt] * self["PD"]["TIMP"+itag]["EB"][zfilt] / np.power(self["PD"]["TIMP"+itag][""][zfilt], 2.0), 2.0))
                            debye[zfilt] = debye[zfilt] + tdebye
                            debyeeb[zfilt] = np.sqrt(np.power(debyeeb[zfilt], 2.0) + np.power(tdebyeeb, 2.0))
                            ddebye[zfilt] = ddebye[zfilt] + ee * z2 * self["PD"]["NIMP"+itag]["GRAD"][zfilt] / self["PD"]["TIMP"+itag][""][zfilt] - tdebye * self["PD"]["TIMP"+itag]["GRAD"][zfilt] / self["PD"]["TIMP"+itag][""][zfilt]
                            ddebyeeb[zfilt] = np.sqrt(np.power(ddebyeeb[zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NIMP"+itag]["GRADEB"][zfilt] / self["PD"]["TIMP"+itag][""][zfilt], 2.0) \
	                                              + np.power(ee * z2 * self["PD"]["NIMP"+itag]["GRAD"][zfilt] * self["PD"]["TIMP"+itag]["EB"][zfilt] / np.power(self["PD"]["TIMP"+itag][""][zfilt], 2.0), 2.0) \
	                                              + np.power(tdebyeeb * self["PD"]["TIMP"+itag]["GRAD"][zfilt] / self["PD"]["TIMP"+itag][""][zfilt], 2.0) \
                                                      + np.power(tdebye * self["PD"]["TIMP"+itag]["GRADEB"][zfilt] / self["PD"]["TIMP"+itag][""][zfilt], 2.0) \
                                                      + np.power(tdebye * self["PD"]["TIMP"+itag]["GRAD"][zfilt] * self["PD"]["TIMP"+itag]["EB"][zfilt] / np.power(self["PD"]["TIMP"+itag][""][zfilt], 2.0), 2.0))
                            prov = prov + "; EX2GK: PD_NIMP"+itag + "; EX2GK: PD_TIMP"+itag + "; EX2GK: META_ZIMP"+itag
                for idx in range(0, numz):
                    itag = "%d" % (idx+1)
                    if "Z"+itag in pddict["N"] and "Z"+itag in pddict["T"] and "ZZ"+itag in self["META"]:
                        zfilt = (self["PD"]["TZ"+itag][""] > 0.0)
                        if np.any(zfilt):
                            z2 = np.power(self["META"]["ZZ"+itag], 2.0)
                            tdebye = ee * z2 * self["PD"]["NZ"+itag][""][zfilt] / self["PD"]["TZ"+itag][""][zfilt]
                            tdebyeeb = np.sqrt(np.power(ee * z2 * self["PD"]["NZ"+itag]["EB"][zfilt] / self["PD"]["TZ"+itag][""][zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NZ"+itag][""][zfilt] * self["PD"]["TZ"+itag]["EB"][zfilt] / np.power(self["PD"]["TZ"+itag][""][zfilt], 2.0), 2.0))
                            debye[zfilt] = debye[zfilt] + tdebye
                            debyeeb[zfilt] = np.sqrt(np.power(debyeeb[zfilt], 2.0) + np.power(tdebyeeb, 2.0))
                            ddebye[zfilt] = ddebye[zfilt] + ee * z2 * self["PD"]["NZ"+itag]["GRAD"][zfilt] / self["PD"]["TZ"+itag][""][zfilt] - tdebye * self["PD"]["TZ"+itag]["GRAD"][zfilt] / self["PD"]["TZ"+itag][""][zfilt]
                            ddebyeeb[zfilt] = np.sqrt(np.power(ddebyeeb[zfilt], 2.0) + np.power(ee * z2 * self["PD"]["NZ"+itag]["GRADEB"][zfilt] / self["PD"]["TZ"+itag][""][zfilt], 2.0) \
	                                              + np.power(ee * z2 * self["PD"]["NZ"+itag]["GRAD"][zfilt] * self["PD"]["TZ"+itag]["EB"][zfilt] / np.power(self["PD"]["TZ"+itag][""][zfilt], 2.0), 2.0) \
	                                              + np.power(tdebyeeb * self["PD"]["TZ"+itag]["GRAD"][zfilt] / self["PD"]["TZ"+itag][""][zfilt], 2.0) \
                                                      + np.power(tdebye * self["PD"]["TZ"+itag]["GRADEB"][zfilt] / self["PD"]["TZ"+itag][""][zfilt], 2.0) \
                                                      + np.power(tdebye * self["PD"]["TZ"+itag]["GRAD"][zfilt] * self["PD"]["TZ"+itag]["EB"][zfilt] / np.power(self["PD"]["TZ"+itag][""][zfilt], 2.0), 2.0))
                            prov = prov + "; EX2GK: PD_NZ"+itag + "; EX2GK: PD_TZ"+itag + "; EX2GK: META_ZZ"+itag
                fdebye = np.full(refshape, 1.0e-4)
                fdebyeeb = np.full(refshape, 1.0e-5)
                dfdebye = np.zeros(refshape)
                dfdebyeeb = np.zeros(refshape)
                zfilt = (debye > 0.0)
                if np.any(zfilt):
                    fdebye[zfilt] = np.sqrt(eps0 / debye[zfilt])     # since T is already given in eV
                    fdebyeeb[zfilt] = 0.5 * fdebye[zfilt] * debyeeb[zfilt] / debye[zfilt]
                    dfdebye[zfilt] = -0.5 * fdebye[zfilt] * ddebye[zfilt] / debye[zfilt]
                    dfdebyeeb[zfilt] = np.sqrt(np.power(0.5 * fdebyeeb[zfilt] * ddebye[zfilt] / debye[zfilt], 2.0) + np.power(0.5 * fdebye[zfilt] * ddebyeeb[zfilt] / debye[zfilt], 2.0) + np.power(0.5 * fdebye[zfilt] * ddebye[zfilt] * debyeeb[zfilt] / np.power(debye[zfilt], 2.0), 2.0))
                self["PD"].addProfileData("LDEBYE", fdebye, fdebyeeb, provenance=prov)
                self["PD"].addProfileDerivativeData("LDEBYE", dfdebye, dfdebyeeb)

                if self._debug:
                    print("%s - Added %s variable to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "LDEBYE"))
            else:
                warnings.warn("Electron density and temperature data not identified, %s Debye length estimation calculations not performed" % (type(self).__name__))

            if "LDEBYE" in self["PD"] and self["PD"]["LDEBYE"][""] is not None:
                collei = np.full(refshape, 1.0e4)
                colleieb = np.full(refshape, 1.0e4)
                dcollei = np.zeros(refshape)
                dcolleieb = np.zeros(refshape)
                lfilt = (self["PD"]["TE"][""] > 0.0)
                if np.any(lfilt):
                    zi = self["META"]["ZI1"] if "ZI1" in self["META"] else 1.0
                    b90inv = 4.0 * np.pi * eps0 * self["PD"]["TE"][""][lfilt] / (ee * zi)
                    plaspar = b90inv / self["PD"]["LDEBYE"][""][lfilt]
                    plaspareb = plaspar * np.sqrt(np.power(self["PD"]["TE"]["EB"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0) + np.power(self["PD"]["LDEBYE"]["EB"][lfilt] / self["PD"]["LDEBYE"][""][lfilt], 2.0))
                    dplaspar = plaspar * (self["PD"]["TE"]["GRAD"][lfilt] / self["PD"]["TE"][""][lfilt] - self["PD"]["LDEBYE"]["GRAD"][lfilt] / self["PD"]["LDEBYE"][""][lfilt])
                    dplaspareb = dplaspar * np.sqrt(np.power(plaspareb / plaspar, 2.0) + np.power(self["PD"]["TE"]["GRADEB"][lfilt] / self["PD"]["TE"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["TE"]["EB"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0) \
                                                    + np.power(self["PD"]["LDEBYE"]["GRADEB"][lfilt] / self["PD"]["LDEBYE"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["LDEBYE"]["EB"][lfilt] / self["PD"]["LDEBYE"][""][lfilt], 2.0))
                    lncoul = np.log(plaspar)
                    lncouleb = np.abs(plaspareb / plaspar)
                    dlncoul = dplaspar / plaspar
                    dlncouleb = np.sqrt(np.power(dplaspareb / plaspar, 2.0) + np.power(dplaspar * plaspareb / np.power(plaspar, 2.0), 2.0))
                    varfac = (4.0 / 3.0)    # Some other literature suggests pi / 2.0  ???
                    prefactor = varfac * np.sqrt(2.0 * np.pi * ee / me) / np.power(4.0 * np.pi * eps0 / ee, 2.0)   # equivalent to constant value of 919.0
                    collei[lfilt] = prefactor * lncoul[lfilt] * zi * self["PD"]["ZEFF"][""][lfilt] * self["PD"]["NE"][""][lfilt] / np.power(self["PD"]["TE"][""][lfilt],1.5)
                    colleieb[lfilt] = collei[lfilt] * np.sqrt(np.power(lncouleb[lfilt] / lncoul[lfilt], 2.0) + np.power(self["PD"]["ZEFF"]["EB"][lfilt] / self["PD"]["ZEFF"][""][lfilt], 2.0) + np.power(self["PD"]["NE"]["EB"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0))
                    # Removed electron temperature error due to potential double counting with lncoul
#                    colleieb[lfilt] = collei[lfilt] * np.sqrt(np.power(lncouleb[lfilt] / lncoul[lfilt], 2.0) + np.power(self["PD"]["ZEFF"]["EB"][lfilt] / self["PD"]["ZEFF"][""][lfilt], 2.0) + np.power(self["PD"]["NE"]["EB"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0) + np.power(1.5 * self["PD"]["TE"]["EB"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0))
                    dcollei[lfilt] = collei[lfilt] * (dlncoul[lfilt] / lncoul[lfilt] + self["PD"]["ZEFF"]["GRAD"][lfilt] / self["PD"]["ZEFF"][""][lfilt] + self["PD"]["NE"]["GRAD"][lfilt] / self["PD"]["NE"][""][lfilt] - 1.5 * self["PD"]["TE"]["GRAD"][lfilt] / self["PD"]["TE"][""][lfilt])
                    dcolleieb[lfilt] = np.sqrt(np.power(dcollei[lfilt] * colleieb[lfilt] / collei[lfilt], 2.0) \
                                               + np.power(collei[lfilt] * dlncoul[lfilt] / lncoul[lfilt], 2.0) * (np.power(dlncouleb[lfilt] / dlncoul[lfilt], 2.0) + np.power(lncouleb[lfilt] / lncoul[lfilt], 2.0)) \
                                               + np.power(collei[lfilt] * self["PD"]["ZEFF"]["GRADEB"][lfilt] / self["PD"]["ZEFF"][""][lfilt], 2.0) + np.power(collei[lfilt] * self["PD"]["ZEFF"]["GRAD"][lfilt] * self["PD"]["ZEFF"]["EB"][lfilt] / np.power(self["PD"]["ZEFF"][""][lfilt], 2.0), 2.0) \
                                               + np.power(collei[lfilt] * self["PD"]["NE"]["GRAD"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0) * (np.power(self["PD"]["NE"]["GRADEB"][lfilt] / self["PD"]["NE"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["NE"]["EB"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0)) \
                                               + np.power(1.5 * collei[lfilt] * self["PD"]["TE"]["GRAD"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0) * (np.power(self["PD"]["TE"]["GRADEB"][lfilt] / self["PD"]["TE"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["TE"]["EB"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0)))
                prov = "EX2GK: PD_LDEBYE; EX2GK: PD_NE; EX2GK: PD_TE; EX2GK: PD_ZEFF; EX2GK: META_ZI1"
                self["PD"].addProfileData("OMCOLLEI", collei, colleieb, provenance=prov)
                self["PD"].addProfileDerivativeData("OMCOLLEI", dcollei, dcolleieb)

                if self._debug:
                    print("%s - Added %s variable to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "OMCOLLEI"))

                collnormei = np.full(refshape, 1.0e-2)
                collnormeieb = np.full(refshape, 1.0e-2)
                dcollnormei = np.zeros(refshape)
                dcollnormeieb = np.zeros(refshape)
                prov = "EX2GK: Internal assumption"
                if np.any(lfilt):
                    rmajor = None
                    rminor = None
                    drmajor = np.ones(refshape)
                    drminor = np.zeros(refshape)
                    rmaj_prov = None
                    rmin_prov = None
                    if "RMAJORA" in self["CD"][cs_prefix]:
                        (rmajor, jrmajor, rmajoreb) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORA", cs_prefix, cs_prefix, fdebug=False)
                        jfilt = (jrmajor != 0.0)
                        if np.any(jfilt):
                            drmajor[jfilt] = 1.0 / jrmajor[jfilt]
                        rmaj_prov = "EX2GK: CD_"+cs_prefix+"RMAJORA"
                    elif "RMAJORO" in self["CD"][cs_prefix] and "RMAJORI" in self["CD"][cs_prefix]:
                        (rmajo, rmajoj, rmajoe) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORO", cs_prefix, cs_prefix, fdebug=False)
                        (rmaji, rmajij, rmajie) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORI", cs_prefix, cs_prefix, fdebug=False)
                        rmajor = (rmajo + rmaji) / 2.0
                        jfilt = np.all([rmajoj != 0.0, rmajij != 0.0], axis=0)
                        if np.any(jfilt):
                            drmajor[jfilt] = (rmajij[jfilt] + rmajoj[jfilt]) / (2.0 * rmajoj[jfilt] * rmajij[jfilt])
                        rmaj_prov = "EX2GK: CD_"+cs_prefix+"RMAJORO; EX2GK: CD_"+cs_prefix+"RMAJORI"
                    if "RMINORA" in self["CD"][cs_prefix]:
                        (rminor, jrminor, rminoreb) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMINORA", cs_prefix, cs_prefix, fdebug=False)
                        jfilt = (jrminor != 0.0)
                        if np.any(jfilt):
                            drminor[jfilt] = 1.0 / jrminor[jfilt]
                        rmin_prov = "EX2GK: CD_"+cs_prefix+"RMINORA"
                    elif "RMINORO" in self["CD"][cs_prefix] and "RMINORI" in self["CD"][cs_prefix]:
                        (rmino, rminoj, rminoe) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMINORO", cs_prefix, cs_prefix, fdebug=False)
                        (rmini, rminij, rminie) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMINORI", cs_prefix, cs_prefix, fdebug=False)
                        rminor = (rmino - rmini) / 2.0
                        jfilt = np.all([rminoj != 0.0, rminij != 0.0], axis=0)
                        if np.any(jfilt):
                            drminor[jfilt] = (rminij[jfilt] + rminoj[jfilt]) / (2.0 * rminoj[jfilt] * rminij[jfilt])
                        rmin_prov = "EX2GK: CD_"+cs_prefix+"RMINORO; EX2GK: CD_"+cs_prefix+"RMINORI"
                    if "Q" in pddict and cs_prefix in pddict["Q"] and rmajor is not None and rminor is not None:
                        rminor[rminor < 5.0e-3] = 5.0e-3
                        epsilon = rminor[lfilt] / rmajor[lfilt]
                        depsilon = epsilon * (drminor[lfilt] / rminor[lfilt] - drmajor[lfilt] / rmajor[lfilt])
                        fbounce = np.full(refshape, 1.0e6)
                        fbounceeb = np.zeros(refshape)
                        dfbounce = np.zeros(refshape)
                        dfbounceeb = np.zeros(refshape)
                        fbounce[lfilt] = np.sqrt(ee * self["PD"]["TE"][""][lfilt] / me) * np.power(epsilon, 1.5) / (self["PD"]["Q"+cs_prefix][""][lfilt] * rmajor[lfilt])
                        fbounceeb[lfilt] = fbounce * np.sqrt(np.power(self["PD"]["TE"]["EB"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0) + np.power(self["PD"]["Q"+cs_prefix]["EB"][lfilt] / self["PD"]["Q"+cs_prefix][""][lfilt], 2.0))
                        dfbounce[lfilt] = fbounce[lfilt] * (0.5 * self["PD"]["TE"]["GRAD"][lfilt] / self["PD"]["TE"][""] + 1.5 * depsilon / epsilon - self["PD"]["Q"+cs_prefix]["GRAD"][lfilt] / self["PD"]["Q"+cs_prefix][""][lfilt] - drmajor[lfilt] / rmajor[lfilt])
                        dfbounceeb[lfilt] = np.sqrt(np.power(dfbounce[lfilt] * fbounceeb[lfilt] / fbounce[lfilt], 2.0) \
                                                    + np.power(0.5 * fbounce[lfilt] * self["PD"]["TE"]["GRAD"][lfilt] / self["PD"]["TE"][""], 2.0) * (np.power(self["PD"]["TE"]["GRADEB"][lfilt] / self["PD"]["TE"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["TE"]["EB"][lfilt] / self["PD"]["TE"][""][lfilt], 2.0)) \
                                                    + np.power(fbounce[lfilt] * self["PD"]["Q"+cs_prefix]["GRAD"][lfilt] / self["PD"]["Q"+cs_prefix][""][lfilt], 2.0) * (np.power(self["PD"]["Q"+cs_prefix]["GRADEB"][lfilt] / self["PD"]["Q"+cs_prefix]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["Q"+cs_prefix]["EB"][lfilt] / self["PD"]["Q"+cs_prefix][""][lfilt], 2.0)))
                        collnormei[lfilt] = collei[lfilt] / fbounce[lfilt]
                        collnormeieb[lfilt] = collnormei[lfilt] * np.sqrt(np.power(colleieb[lfilt] / collei[lfilt], 2.0) + np.power(fbounceeb[lfilt] / fbounce[lfilt], 2.0))
                        dcollnormei[lfilt] = (dcollei[lfilt] - collei[lfilt] * dfbounce[lfilt] / fbounce[lfilt]) / fbounce[lfilt]
                        dcollnormeieb[lfilt] = np.sqrt(np.power(dcollnormei[lfilt] * collnormeieb[lfilt] / collnormei[lfilt], 2.0) \
                                                       + np.power(collnormei[lfilt] * dcollei[lfilt] / collei[lfilt], 2.0) * (np.power(dcolleieb[lfilt] / dcollei[lfilt], 2.0) + np.power(colleieb[lfilt] / collei[lfilt], 2.0)) \
                                                       + np.power(collnormei[lfilt] * dfbounce[lfilt] / fbounce[lfilt], 2.0) * (np.power(dfbounceeb[lfilt] / dfbounce[lfilt], 2.0) + np.power(fbounceeb[lfilt] / fbounce[lfilt], 2.0)))
                        prov = "EX2GK: PD_TE; EX2GK: PD_Q"+cs_prefix
                        if rmaj_prov is not None:
                            prov = prov + "; " + rmaj_prov
                        if rmin_prov is not None:
                            prov = prov + "; " + rmin_prov
                self["PD"].addProfileData("OMCOLLEINORM", collnormei, collnormeieb, provenance=prov)
                self["PD"].addProfileDerivativeData("OMCOLLEINORM", dcollnormei, dcollnormeieb)

                if self._debug:
                    print("%s - Added %s variable to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "OMCOLLEINORM"))
            else:
                warnings.warn("Debye length estimate not provided, %s collision frequency estimation calculations not performed" % (type(self).__name__))

            if "J" in pddict and "OHM" in pddict["J"] and "N" in pddict and "E" in pddict["N"] and "OMCOLLEI" in self["PD"] and self["PD"]["OMCOLLEI"][""] is not None:
                pohm = np.zeros(refshape)
                pohmeb = np.zeros(refshape)
                dpohm = np.zeros(refshape)
                dpohmeb = np.zeros(refshape)
                lfilt = (self["PD"]["NE"][""] > 0.0)
                if np.any(lfilt):
                    pohm[lfilt] = (me / ee**2.0) * np.power(self["PD"]["JOHM"][""][lfilt], 2.0) * self["PD"]["OMCOLLEI"][""][lfilt] / self["PD"]["NE"][""][lfilt]
                    pohmeb[lfilt] = pohm[lfilt] * np.sqrt(np.power(2.0 * self["PD"]["JOHM"]["EB"][lfilt] / self["PD"]["JOHM"][""][lfilt], 2.0) + np.power(self["PD"]["OMCOLLEI"]["EB"][lfilt] / self["PD"]["OMCOLLEI"][""][lfilt], 2.0) + np.power(self["PD"]["NE"]["EB"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0))
                    dpohm[lfilt] = pohm[lfilt] * (2.0 * self["PD"]["JOHM"]["GRAD"][lfilt] / self["PD"]["JOHM"][""][lfilt] + self["PD"]["OMCOLLEI"]["GRAD"][lfilt] / self["PD"]["OMCOLLEI"][""][lfilt] - self["PD"]["NE"]["GRAD"][lfilt] / self["PD"]["NE"][""][lfilt])
                    dpohmeb[lfilt] = np.sqrt(np.power(dpohm[lfilt] * pohmeb[lfilt] / pohm[lfilt], 2.0) \
                                             + np.power(pohm[lfilt] * 2.0 * self["PD"]["JOHM"]["GRAD"][lfilt] / self["PD"]["JOHM"][""][lfilt], 2.0) * (np.power(self["PD"]["JOHM"]["GRADEB"][lfilt] / self["PD"]["JOHM"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["JOHM"]["EB"][lfilt] / self["PD"]["JOHM"][""][lfilt], 2.0)) \
                                             + np.power(pohm[lfilt] * self["PD"]["OMCOLLEI"]["GRAD"][lfilt] / self["PD"]["OMCOLLEI"][""][lfilt], 2.0) * (np.power(self["PD"]["OMCOLLEI"]["GRADEB"][lfilt] / self["PD"]["OMCOLLEI"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["OMCOLLEI"]["EB"][lfilt] / self["PD"]["OMCOLLEI"][""][lfilt], 2.0)) \
                                             + np.power(pohm[lfilt] * self["PD"]["NE"]["GRAD"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0) * (np.power(self["PD"]["NE"]["GRADEB"][lfilt] / self["PD"]["NE"]["GRAD"][lfilt], 2.0) + np.power(self["PD"]["NE"]["EB"][lfilt] / self["PD"]["NE"][""][lfilt], 2.0)))
                efrac = 0.5
                ifrac = 0.5       # Assume ohmic power is equally split between species
                prov = "EX2GK: PD_JOHM; EX2GK: PD_OMCOLLEI; EX2GK: PD_NE"
                self["PD"].addProfileData("STECOLL", efrac * pohm, efrac * pohmeb, provenance=prov)
                self["PD"].addProfileDerivativeData("STECOLL", efrac * dpohm, efrac * dpohmeb)
                self["PD"].addProfileData("STICOLL", ifrac * pohm, ifrac * pohmeb, provenance=prov)
                self["PD"].addProfileDerivativeData("STICOLL", ifrac * dpohm, ifrac * dpohmeb)

                if self._debug:
                    print("%s - Added %s and %s variables to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "STEOHM", "STIOHM"))
            else:
                warnings.warn("Collision frequency estimate not provided, %s ohmic power deposition estimation calculations not performed" % (type(self).__name__))

            # Collapse fast ion profiles into single fast ion profile (per heating source) for pressure calculation
            for src in self.allowed_sources:
                mstr = ""
                if "N" in pddict and "FI"+src not in pddict["N"]:
                    nfi = np.zeros(refshape)
                    nfieb = np.zeros(refshape)
                    dnfi = np.zeros(refshape)
                    dnfieb = np.zeros(refshape)
                    prov = ""
                    for idx in range(0, numfi):
                        itag = "%d" % (idx+1)
                        if "N" in pddict and "FI"+itag+src in pddict["N"]:
                            nfi = nfi + self["PD"]["NFI"+itag+src][""]
                            nfieb = np.sqrt(np.power(nfieb, 2.0) + np.power(self["PD"]["NFI"+itag+src]["EB"], 2.0))
                            dnfi = dnfi + self["PD"]["NFI"+itag+src]["GRAD"]
                            dnfieb = np.sqrt(np.power(dnfieb, 2.0) + np.power(self["PD"]["NFI"+itag+src]["GRADEB"], 2.0))
                            prov += "EX2GK: PD_NFI"+itag+src
                    if not np.all(nfi == 0.0):
                        self["PD"].addProfileData("NFI"+src, nfi, nfieb, provenance=prov)
                        self["PD"].addProfileDerivativeData("NFI"+src, dnfi, dnfieb)
                        mstr += "NFI"+src
                if "W" in pddict and "FI"+src not in pddict["W"]:
                    wfi = np.zeros(refshape)
                    wfieb = np.zeros(refshape)
                    dwfi = np.zeros(refshape)
                    dwfieb = np.zeros(refshape)
                    prov = ""
                    for idx in range(0, numfi):
                        itag = "%d" % (idx+1)
                        if "W" in pddict and "FI"+itag+src in pddict["W"]:
                            wfi = wfi + self["PD"]["WFI"+itag+src][""]
                            wfieb = np.sqrt(np.power(wfieb, 2.0) + np.power(self["PD"]["WFI"+itag+src]["EB"], 2.0))
                            dwfi = dwfi + self["PD"]["WFI"+itag+src]["GRAD"]
                            dwfieb = np.sqrt(np.power(dwfieb, 2.0) + np.power(self["PD"]["WFI"+itag+src]["GRADEB"], 2.0))
                            prov += "EX2GK: PD_WFI"+itag+src
                    if not np.all(wfi == 0.0):
                        self["PD"].addProfileData("WFI"+src, wfi, wfieb, provenance=prov)
                        self["PD"].addProfileDerivativeData("WFI"+src, dwfi, dwfieb)
                        mstr += "WFI"+src

                if self._debug and mstr:
                    print("%s - Added %s variable(s) to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, mstr))

            # Pressure calculations - 3/2 factor not needed for pressure, only for kinetic energy (ie. for stored energy calculation) assuming isotropic 3D system
            mstr = ""
            if "N" in pddict and "E" in pddict["N"] and "T" in pddict and "E" in pddict["T"]:
                pe = ee * self["PD"]["NE"][""] * self["PD"]["TE"][""]
                peeb = np.sqrt(np.power(ee * self["PD"]["NE"]["EB"] * self["PD"]["TE"][""], 2.0) + np.power(ee * self["PD"]["NE"][""] * self["PD"]["TE"]["EB"], 2.0))
                dpe = ee * (self["PD"]["NE"]["GRAD"] * self["PD"]["TE"][""] + self["PD"]["NE"][""] * self["PD"]["TE"]["GRAD"])
                dpeebt1 = np.power(ee * self["PD"]["NE"]["GRADEB"] * self["PD"]["TE"][""], 2.0) + np.power(ee * self["PD"]["NE"]["GRAD"] * self["PD"]["TE"]["EB"], 2.0)
                dpeebt2 = np.power(ee * self["PD"]["NE"]["EB"] * self["PD"]["TE"]["GRAD"], 2.0) + np.power(ee * self["PD"]["NE"][""] * self["PD"]["TE"]["GRADEB"], 2.0)
                dpeeb = np.sqrt(dpeebt1 + dpeebt2)
                prov = "EX2GK: PD_NE; EX2GK: PD_TE"
                self["PD"].addProfileData("PE", pe, peeb, provenance=prov)
                self["PD"].addProfileDerivativeData("PE", dpe, dpeeb)
                mstr += "PE"

            if self._debug and mstr:
                print("%s - Added %s variable to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, mstr))

            mstr = ""
            for idx in range(0, numion):
                itag = "%d" % (idx+1)
                if "N" in pddict and "I"+itag in pddict["N"] and "T" in pddict and "I"+itag in pddict["T"]:
                    pi = ee * self["PD"]["NI"+itag][""] * self["PD"]["TI"+itag][""]
                    pieb = np.sqrt(np.power(ee * self["PD"]["NI"+itag]["EB"] * self["PD"]["TI"+itag][""], 2.0) + np.power(ee * self["PD"]["NI"+itag][""] * self["PD"]["TI"+itag]["EB"], 2.0))
                    dpi = ee * (self["PD"]["NI"+itag]["GRAD"] * self["PD"]["TI"+itag][""] + self["PD"]["NI"+itag][""] * self["PD"]["TI"+itag]["GRAD"])
                    dpiebt1 = np.power(ee * self["PD"]["NI"+itag]["GRADEB"] * self["PD"]["TI"+itag][""], 2.0) + np.power(ee * self["PD"]["NI"+itag]["GRAD"] * self["PD"]["TI"+itag]["EB"], 2.0)
                    dpiebt2 = np.power(ee * self["PD"]["NI"+itag]["EB"] * self["PD"]["TI"+itag]["GRAD"], 2.0) + np.power(ee * self["PD"]["NI"+itag][""] * self["PD"]["TI"+itag]["GRADEB"], 2.0)
                    dpieb = np.sqrt(dpiebt1 + dpiebt2)
                    prov = "EX2GK: PD_NI"+itag + "; EX2GK: PD_TI"+itag
                    self["PD"].addProfileData("PI"+itag, pi, pieb, provenance=prov)
                    self["PD"].addProfileDerivativeData("PI"+itag, dpi, dpieb)
                    if mstr:
                        mstr += ", "
                    mstr += "PI"+itag

            if self._debug and mstr:
                print("%s - Added %s variable(s) to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, mstr))

            mstr = ""
            for idx in range(0, numimp):
                itag = "%d" % (idx+1)
                if "N" in pddict and "IMP"+itag in pddict["N"] and "T" in pddict and "IMP"+itag in pddict["T"]:
                    pimp = ee * self["PD"]["NIMP"+itag][""] * self["PD"]["TIMP"+itag][""]
                    pimpeb = np.sqrt(np.power(ee * self["PD"]["NIMP"+itag]["EB"] * self["PD"]["TIMP"+itag][""], 2.0) + np.power(ee * self["PD"]["NIMP"+itag][""] * self["PD"]["TIMP"+itag]["EB"], 2.0))
                    dpimp = ee * (self["PD"]["NIMP"+itag]["GRAD"] * self["PD"]["TIMP"+itag][""] + self["PD"]["NIMP"+itag][""] * self["PD"]["TIMP"+itag]["GRAD"])
                    dpimpebt1 = np.power(ee * self["PD"]["NIMP"+itag]["GRADEB"] * self["PD"]["TIMP"+itag][""], 2.0) + np.power(ee * self["PD"]["NIMP"+itag]["GRAD"] * self["PD"]["TIMP"+itag]["EB"], 2.0)
                    dpimpebt2 = np.power(ee * self["PD"]["NIMP"+itag]["EB"] * self["PD"]["TIMP"+itag]["GRAD"], 2.0) + np.power(ee * self["PD"]["NIMP"+itag][""] * self["PD"]["TIMP"+itag]["GRADEB"], 2.0)
                    dpimpeb = np.sqrt(dpimpebt1 + dpimpebt2)
                    prov = "EX2GK: PD_NIMP"+itag + "; EX2GK: PD_TIMP"+itag
                    self["PD"].addProfileData("PIMP"+itag, pimp, pimpeb, provenance=prov)
                    self["PD"].addProfileDerivativeData("PIMP"+itag, dpimp, dpimpeb)
                    if mstr:
                        mstr += ", "
                    mstr += "PIMP"+itag

            if self._debug and mstr:
                print("%s - Added %s variable(s) to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, mstr))

            mstr = ""
            for idx in range(0, numz):
                itag = "%d" % (idx+1)
                if "N" in pddict and "Z"+itag in pddict["N"] and "T" in pddict and "Z"+itag in pddict["T"]:
                    pz = ee * self["PD"]["NZ"+itag][""] * self["PD"]["TZ"+itag][""]
                    pzeb = np.sqrt(np.power(ee * self["PD"]["NZ"+itag]["EB"] * self["PD"]["TZ"+itag][""], 2.0) + np.power(ee * self["PD"]["NZ"+itag][""] * self["PD"]["TZ"+itag]["EB"], 2.0))
                    dpz = ee * (self["PD"]["NZ"+itag]["GRAD"] * self["PD"]["TZ"+itag][""] + self["PD"]["NZ"+itag][""] * self["PD"]["TZ"+itag]["GRAD"])
                    dpzebt1 = np.power(ee * self["PD"]["NZ"+itag]["GRADEB"] * self["PD"]["TZ"+itag][""], 2.0) + np.power(ee * self["PD"]["NZ"+itag]["GRAD"] * self["PD"]["TZ"+itag]["EB"], 2.0)
                    dpzebt2 = np.power(ee * self["PD"]["NZ"+itag]["EB"] * self["PD"]["TZ"+itag]["GRAD"], 2.0) + np.power(ee * self["PD"]["NZ"+itag][""] * self["PD"]["TZ"+itag]["GRADEB"], 2.0)
                    dpzeb = np.sqrt(dpzebt1 + dpzebt2)
                    prov = "EX2GK: PD_NZ"+itag + "; EX2GK: PD_TZ"+itag
                    self["PD"].addProfileData("PZ"+itag, pz, pzeb, provenance=prov)
                    self["PD"].addProfileDerivativeData("PZ"+itag, dpz, dpzeb)
                    if mstr:
                        mstr += ", "
                    mstr += "PZ"+itag

            if self._debug and mstr:
                print("%s - Added %s variable(s) to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, mstr))

            mstr = ""
            for src in self.allowed_sources:
                if not ("P" in pddict and "FI"+src in pddict["P"]):
                    if "W" in pddict and "FI"+src in pddict["W"]:
                        pfi = (2.0 / 3.0) * self["PD"]["WFI"+src][""]
                        pfieb = (2.0 / 3.0) * self["PD"]["WFI"+src]["EB"]
                        dpfi = (2.0 / 3.0) * self["PD"]["WFI"+src]["GRAD"]
                        dpfieb = (2.0 / 3.0) * self["PD"]["WFI"+src]["GRADEB"]
                        prov = "EX2GK: PD_WFI"+src
                        self["PD"].addProfileData("PFI"+src, pfi, pfieb, provenance=prov)
                        self["PD"].addProfileDerivativeData("PFI"+src, dpfi, dpfieb)
                        if mstr:
                            mstr += ", "
                        mstr += "PFI"+itag

                    elif "N" in pddict and "FI"+src in pddict["N"] and "T" in pddict and "FI"+src in pddict["T"]:
                        pfi = ee * self["PD"]["NFI"+src][""] * self["PD"]["TFI"+src][""]
                        pfieb = np.sqrt(np.power(ee * self["PD"]["NFI"+src]["EB"] * self["PD"]["TFI"+src][""], 2.0) + np.power(ee * self["PD"]["NFI"+src][""] * self["PD"]["TFI"+src]["EB"], 2.0))
                        dpfi = ee * (self["PD"]["NFI"+src]["GRAD"] * self["PD"]["TFI"+src][""] + self["PD"]["NFI"+src][""] * self["PD"]["TFI"+src]["GRAD"])
                        dpfiebt1 = np.power(ee * self["PD"]["NFI"+src]["GRADEB"] * self["PD"]["TFI"+src][""], 2.0) + np.power(ee * self["PD"]["NFI"+src]["GRAD"] * self["PD"]["TFI"+src]["EB"], 2.0)
                        dpfiebt2 = np.power(ee * self["PD"]["NFI"+src]["EB"] * self["PD"]["TFI"+src]["GRAD"], 2.0) + np.power(ee * self["PD"]["NFI"+src][""] * self["PD"]["TFI"+src]["GRADEB"], 2.0)
                        dpfieb = np.sqrt(dpfiebt1 + dpfiebt2)
                        prov = "EX2GK: PD_NFI"+src + "; EX2GK: PD_TFI"+src
                        self["PD"].addProfileData("PFI"+src, pfi, pfieb, provenance=prov)
                        self["PD"].addProfileDerivativeData("PFI"+src, dpfi, dpfieb)
                        if mstr:
                            mstr += ", "
                        mstr += "PFI"+itag

            if self._debug and mstr:
                print("%s - Added %s variable(s) to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, mstr))

            # Normalized pressure density - requires integration with respect to volume in order to get normalized pressure
            if "PE" in self["PD"] and "BMAG" in self["ZD"]:
                betac = 2.51327412e-6                    # (2 mu_0)  -- since pressure is already given in N m^-2
                pei = self["PD"]["PE"][""].copy()
                peieb = self["PD"]["PE"]["EB"].copy()
                eiprov = "EX2GK: PD_PE"
                for idx in range(0, numion):
                    itag = "%d" % (idx+1)
                    if "PI"+itag in self["PD"]:
                        pei = pei + self["PD"]["PI"+itag][""]
                        peieb = np.sqrt(np.power(peieb, 2.0) + np.power(self["PD"]["PI"+itag][""], 2.0))
                        eiprov = eiprov + "; EX2GK: PD_PI"+itag
                pth = pei.copy()
                ptheb = peieb.copy()
                thprov = eiprov
                for idx in range(0, numimp):
                    itag = "%d" % (idx+1)
                    if "PIMP"+itag in self["PD"]:
                        pth = pth + self["PD"]["PIMP"+itag][""]
                        ptheb = np.sqrt(np.power(ptheb, 2.0) + np.power(self["PD"]["PIMP"+itag][""], 2.0))
                        thprov = thprov + "; EX2GK: PD_PIMP"+itag
                for idx in range(0, numz):
                    itag = "%d" % (idx+1)
                    if "PZ"+itag in self["PD"]:
                        pth = pth + self["PD"]["PZ"+itag][""]
                        ptheb = np.sqrt(np.power(ptheb, 2.0) + np.power(self["PD"]["PZ"+itag][""], 2.0))
                        thprov = thprov + "; EX2GK: PD_PZ"+itag
                ptot = pth.copy()
                ptoteb = ptheb.copy()
                totprov = thprov
                for src in self.allowed_sources:
                    if "PFI"+src in self["PD"]:
                        ptot = ptot + self["PD"]["PFI"+src][""]
                        ptoteb = np.sqrt(np.power(ptoteb, 2.0) + np.power(self["PD"]["PFI"+src]["EB"], 2.0))
                        totprov = totprov + "; EX2GK: PD_PFI"+itag

                bsq = np.power(self["ZD"]["BMAG"][""], 2.0)
                volume = self["PD"]["V"][""]
                betaei = np.trapz(betac * pei / bsq, volume)
                betaeieb = np.sqrt(np.trapz(np.power(betac * peieb / bsq, 2.0), volume))
                self["ZD"].addData("BETAEI", betaei, betaeieb, provenance=eiprov)
                betath = np.trapz(betac * pth / bsq, volume)
                betatheb = np.sqrt(np.trapz(np.power(betac * ptheb / bsq, 2.0), volume))
                self["ZD"].addData("BETATH", betath, betatheb, provenance=thprov)
                betatot = np.trapz(betac * ptot / bsq, volume)
                betatoteb = np.sqrt(np.trapz(np.power(betac * ptoteb / bsq, 2.0), volume))
                self["ZD"].addData("BETATOT", betatot, betatoteb, provenance=totprov)

                if self._debug:
                    print("%s - Added %s variables to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "BETAEI, BETATH, BETATOT"))

            else:
                warnings.warn("%s global normalized pressure estimation calculation not performed, required data not found" % (type(self).__name__))

            bptag = "BETAPEXP" if "BETAPEXP" in self["ZD"] else "BETAPTOT"
            if bptag in self["ZD"] and "IPLA" in self["ZD"] and "VAVG" in self["ZD"] and "RGEO" in self["ZD"]:
                rtag = cs_prefix+"RGEO" if cs_prefix+"RGEO" in self["ZD"] else "RGEO"
                taue = (3.0 / 8.0) * mu0 * self["ZD"][rtag][""] * self["ZD"][bptag][""] * np.abs(self["ZD"]["IPLA"][""] / self["ZD"]["VAVG"][""])
                taueebt1 = np.power(self["ZD"][rtag]["EB"] * self["ZD"][bptag][""] * self["ZD"]["IPLA"][""] / self["ZD"]["VAVG"][""], 2.0)
                taueebt2 = np.power(self["ZD"][rtag][""] * self["ZD"][bptag]["EB"] * self["ZD"]["IPLA"][""] / self["ZD"]["VAVG"][""], 2.0)
                taueebt3 = np.power(self["ZD"][rtag][""] * self["ZD"][bptag][""] * self["ZD"]["IPLA"]["EB"] / self["ZD"]["VAVG"][""], 2.0)
                taueebt4 = np.power(self["ZD"][rtag][""] * self["ZD"][bptag][""] * self["ZD"]["IPLA"][""] * self["ZD"]["VAVG"]["EB"] / np.power(self["ZD"]["VAVG"][""], 2.0), 2.0)
                taueeb = (3.0 / 8.0) * mu0 * np.sqrt(taueebt1 + taueebt2 + taueebt3 + taueebt4)
                prov = "EX2GK: ZD_IPLA; EX2GK: ZD_VAVG; EX2GK: ZD_"+bptag + "; EX2GK: ZD_"+rtag
                self["ZD"].addData("TAUE", taue, taueeb, provenance=prov)

                if self._debug:
                    print("%s - Added %s variable to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "TAUE"))

            else:
                warnings.warn("%s global energy confinement time estimation calculation not performed, required data not found" % (type(self).__name__))

            # Normalized pressure density - requires integration with respect to volume in order to get normalized pressure
            if "Q"+cs_prefix in self["PD"] and cs_prefix in self["CD"] and "RMINORA" in self["CD"][cs_prefix]:
                rminor = None
                jrminor = None
                rprov = None
                if "RMINORA" in self["CD"][cs_prefix]:
                    (rminor, jrminor, rminoreb) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMINORA", cs_prefix, cs_prefix, fdebug=False)
                    rprov = "EX2GK: CD_"+cs_prefix+"RMINORA"
                elif "RMAJORO" in self["CD"][cs_prefix] and "RMAJORI" in self["CD"][cs_prefix]:
                    (rmajo, rmajoj, rmajoe) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORO", cs_prefix, cs_prefix, fdebug=False)
                    (rmaji, rmajij, rmajie) = self["CD"].convert(self["PD"]["X"][""], self["META"]["CSO"], "RMAJORI", cs_prefix, cs_prefix, fdebug=False)
                    jfilt = np.all([np.abs(rmajoj) > 1.0e-6, np.abs(rmajij) > 1.0e-6, rmajoj != rmajij], axis=0)
                    rminor = (rmajo - rmaji) / 2.0
                    jrminor = np.zeros(rminor.shape)
                    if np.any(jfilt):
                        #jrmaja[jfilt] = 2.0 * rmajoj[jfilt] * rmajij[jfilt] / (rmajij[jfilt] + rmajoj[jfilt])
                        jrminor[jfilt] = 2.0 * rmajoj[jfilt] * rmajij[jfilt] / (rmajij[jfilt] - rmajoj[jfilt])
                    rprov = "EX2GK: CD_"+cs_prefix+"RMAJORO; EX2GK: CD_"+cs_prefix+"RMAJORI"
                if rminor is not None and jrminor is not None:
                    smag = jrminor * rminor * self["PD"]["Q"+cs_prefix]["GRAD"] / self["PD"]["Q"+cs_prefix][""]
                    smageb = np.abs(smag) * np.sqrt(np.power(self["PD"]["Q"+cs_prefix]["GRADEB"] / self["PD"]["Q"+cs_prefix]["GRAD"], 2.0) + np.power(self["PD"]["Q"+cs_prefix]["EB"] / self["PD"]["Q"+cs_prefix][""], 2.0))
                    prov = "EX2GK: PD_Q"+cs_prefix
                    if rprov is not None:
                        prov = prov + "; " + rprov
                    self["PD"].addProfileData("SH"+cs_prefix, smag, smageb, provenance=prov)

            # Total injected power and total coupled power (via uncoupled or lost power)
            srct = None
            srcteb = None
            lsst = None
            lssteb = None
            iprov = None
            lprov = None
            for src in self.allowed_sources:
                if "POWI"+src in self["ZD"]:
                    srct = srct + float(self["ZD"]["POWI"+src][""]) if srct is not None else float(self["ZD"]["POWI"+src][""])
                    srcteb = float(np.sqrt(np.power(srcteb, 2.0) + np.power(self["ZD"]["POWI"+src]["EB"], 2.0))) if srcteb is not None else float(np.abs(self["ZD"]["POWI"+src]["EB"]))
                    iprov = iprov + "; EX2GK: ZD_POWI"+src if iprov is not None else "EX2GK: ZD_POWI"+src
                if "POWL"+src in self["ZD"]:
                    lsst = lsst + float(self["ZD"]["POWL"+src][""]) if lsst is not None else float(self["ZD"]["POWL"+src][""])
                    lssteb = float(np.sqrt(np.power(lssteb, 2.0) + np.power(self["ZD"]["POWL"+src]["EB"], 2.0))) if lssteb is not None else float(np.abs(self["ZD"]["POWL"+src]["EB"]))
                    lprov = lprov + "; EX2GK: ZD_POWL"+src if lprov is not None else "EX2GK: ZD_POWL"+src
            if srct is not None:
                self["ZD"].addData("POWINJ", srct, srcteb, provenance=iprov)
                if lsst is None:
                    lsst = 0.0
                    lssteb = 0.0
                    lprov = "EX2GK: Internal assumption"
                self["ZD"].addData("POWLSS", lsst, lssteb, provenance=lprov)

                if self._debug:
                    print("%s - Added %s variables to profile data container." % (type(self)._calculateMiscellaneousQuantities.__name__, "POWINJ, POWLSS"))

            else:
                warnings.warn("%s total injected auxiliary power estimation calculation not performed, required data not found" % (type(self).__name__))

        else:
            warnings.warn("%s object is not ready for calculation of miscellaneous quantities and profiles" % (type(self).__name__))

    def _performQualityChecks(self, sigma=None):
        """
        Perform basic data consistency and sanity checks thus
        far identified, assuming that the various source
        profiles are available and the kinetic profiles have
        been successfully fitted.

        :returns: dict. Object with identical structure as input object except with standardized fields with test results.
        """

        pddict = self["PD"].getPresentFields('dict')
        if isinstance(sigma, number_types):
            self["META"].setQualityCheckPassCriteria(sigma)

        if "META" in self and self["META"].isReady() and pddict and "V" in pddict and self["FLAG"].checkFlag("FITDONE") and "QCPASS" in self["META"] and self["META"]["QCPASS"] is not None:
#            rhoerrflag = True if self["FLAG"].checkFlag("RHOEB") else False
            numimp = self["META"]["NUMIMP"]
            numion = self["META"]["NUMI"]
            pw = self["META"]["QCPASS"]
            volume = self["PD"]["V"][""]

            refshape = self["PD"]["X"][""].shape

            htestflag = None
            qtestflag = None
            etestflag = None

            srce = None
            srceeb = None
            srci = None
            srcieb = None
            eprov = None
            iprov = None
            for src in self.allowed_sources:
                if "ST" in pddict and "E"+src in pddict["ST"]:
                    srce = srce + self["PD"]["STE"+src][""] if srce is not None else self["PD"]["STE"+src][""].copy()
                    srceeb = np.sqrt(np.power(srceeb, 2.0) + np.power(self["PD"]["STE"+src]["EB"], 2.0)) if srceeb is not None else np.abs(self["PD"]["STE"+src]["EB"])
                    eprov = eprov + "; EX2GK: PD_STE"+src if eprov is not None else "EX2GK: PD_STE"+src
                if "ST" in pddict and "I"+src in pddict["ST"]:
                    srci = srci + self["PD"]["STI"+src][""] if srci is not None else self["PD"]["STI"+src][""].copy()
                    srcieb = np.sqrt(np.power(srcieb, 2.0) + np.power(self["PD"]["STI"+src]["EB"], 2.0)) if srcieb is not None else np.abs(self["PD"]["STI"+src]["EB"])
                    iprov = iprov + "; EX2GK: PD_STI"+src if iprov is not None else "EX2GK: PD_STI"+src
            if srce is not None and srci is not None and "POWINJ" in self["ZD"] and "POWLSS" in self["ZD"]:
                pinput = self["ZD"]["POWINJ"][""] - self["ZD"]["POWLSS"][""]
                pebinput = np.sqrt(np.power(self["ZD"]["POWINJ"]["EB"], 2.0) + np.power(self["ZD"]["POWLSS"]["EB"], 2.0))
                if pebinput < 1.0e-3 * pinput:
                    pebinput = 0.05 * pinput + pebinput
                (htestflag, srctint) = qtools.heating_power_test(volume, srce, srci, srceeb, srcieb, totalpow=pinput, totalpoweb=pebinput, passwidth=pw)
                if htestflag is not None:
                    srctinteb = 0.0
                    prov = "EX2GK: PD_V"
                    if eprov is not None:
                        prov = prov + "; " + eprov
                    if iprov is not None:
                        prov = prov + "; " + iprov
                    self["ZD"].addData("POWDEP", srctint, srctinteb, provenance=prov)
                else:
                    warnings.warn("Unexpected error occurred during heating power test, aborted", stacklevel=2)

            if self._debug and "POWDEP" in self["ZD"]:
                print("%s - Added %s variables to zero-dimensional data container." % (type(self)._performQualityChecks.__name__, "POWDEP"))

            if "T" in pddict and "E" in pddict["T"] and "N" in pddict and "E" in pddict["N"]:
                ti = None
                ni = None
                tieb = None
                nieb = None
                ai = 2.0
                zi = 1.0
                zprov = None
                for idx in range(0, numion):
                    itag = "%d" % (idx+1)
                    if "I"+itag in pddict["T"] and "I"+itag in pddict["N"] and ti is None and ni is None:
                        ti = self["PD"]["TI"+itag][""]
                        ni = self["PD"]["NI"+itag][""]
                        tieb = self["PD"]["TI"+itag]["EB"]
                        nieb = self["PD"]["NI"+itag]["EB"]
                        ai = self["META"]["AI"+itag]
                        zi = self["META"]["ZI"+itag]
                        zprov = zprov + "; EX2GK: PD_NI"+itag + "; EX2GK: PD_TI"+itag + "; EX2GK: META_AI"+itag + "; EX2GK: META_ZI"+itag if zprov is not None else "EX2GK: PD_NI"+itag + "; EX2GK: PD_TI"+itag + "; EX2GK: META_AI"+itag + "; EX2GK: META_ZI"+itag
                if ti is not None and ni is not None:
                    debye = self["PD"]["LDEBYE"][""] if "L" in pddict and "DEBYE" in pddict["L"] else None
                    debyeeb = self["PD"]["LDEBYE"]["EB"] if debye is not None else None
                    (eqpow, eqpoweb) = qtools.calc_equipartition_power(self["PD"]["TE"][""], self["PD"]["NE"][""], ts2raw=ti, ns2raw=ni, ts1raweb=self["PD"]["TE"]["EB"], ns1raweb=self["PD"]["NE"]["EB"], ts2raweb=tieb, ns2raweb=nieb, as2=ai, zs2=zi, debye=debye, debyeeb=debyeeb)
                    if eqpow is not None:
                        if eqpoweb is None:
                            eqpoweb = np.zeros(eqpow.shape)
                        prov = "EX2GK: PD_NE; EX2GK: PD_TE"
                        if zprov is not None:
                            prov = prov + "; " + zprov
                        self["PD"].addProfileData("STEI", eqpow, eqpoweb, provenance=prov)   # Negative indicates transfer from ions to electrons

            if self._debug and "STEI" in self["PD"]:
                print("%s - Added %s variables to profile data container." % (type(self)._performQualityChecks.__name__, "STEI"))

            if srce is not None and srci is not None and "STEI" in self["PD"]:
                (qtestflag, idxmax, eqpowmax, eqpowebmax, srcpowmax, srcpowebmax) = qtools.equipartition_test(volume, self["PD"]["STEI"][""], srce, srci, self["PD"]["STEI"]["EB"], srceeb, srcieb, passwidth=pw)
                if qtestflag is not None:
                    prov = "EX2GK: PD_V; EX2GK: PD_STEI"
                    if eprov is not None:
                        prov = prov + "; " + eprov
                    if iprov is not None:
                        prov = prov + "; " + iprov
                    self["ZD"].addData("EQLIMX", self["PD"]["X"][""][idxmax], self["PD"]["X"]["EB"][idxmax], provenance=prov)
                    self["ZD"].addData("EQLIMPIN", srcpowmax, srcpowebmax, provenance=prov)
                    self["ZD"].addData("EQLIMPEQ", eqpowmax, eqpowebmax, provenance=prov)
                else:
                    warnings.warn("Unexpected error occurred during equipartition power test, aborted", stacklevel=2)

            if "WEXP" in self["ZD"] and "P" in pddict and "E" in pddict["P"]:
                pe = self["PD"]["PE"][""].copy()
                peeb = self["PD"]["PE"]["EB"].copy()
                pi = None
                pieb = None
                eiprov = "EX2GK: PD_PE"
                for idx in range(0, numion):
                    itag = "%d" % (idx+1)
                    if "I"+itag in pddict["P"]:
                        if pi is None:
                            pi = np.zeros(refshape)
                        if pieb is None:
                            pieb = np.zeros(refshape)
                        pi = pi + self["PD"]["PI"+itag][""]
                        pieb = np.sqrt(np.power(pieb, 2.0) + np.power(self["PD"]["PI"+itag]["EB"], 2.0))
                        eiprov = eiprov + "; EX2GK: PD_PI"+itag

                pimp = None
                pimpeb = None
                thprov = eiprov
                for idx in range(0, numimp):
                    itag = "%d" % (idx+1)
                    if "IMP"+itag in pddict["P"]:
                        if pimp is None:
                            pimp = np.zeros(refshape)
                        if pimpeb is None:
                            pimpeb = np.zeros(refshape)
                        pimp = pimp + self["PD"]["PIMP"+itag][""]
                        pimpeb = np.sqrt(np.power(pimpeb, 2.0) + np.power(self["PD"]["PIMP"+itag]["EB"], 2.0))
                        thprov = thprov + "; EX2GK: PD_PIMP"+itag
                if "Z1" in pddict["P"]:
                    if pimp is None:
                        pimp = np.zeros(refshape)
                    if pimpeb is None:
                        pimpeb = np.zeros(refshape)
                    pimp = pimp + self["PD"]["PZ1"][""]
                    pimpeb = np.sqrt(np.power(pimpeb, 2.0) + np.power(self["PD"]["PZ1"]["EB"], 2.0))
                    thprov = thprov + "; EX2GK: PD_PZ1"
                if "Z2" in pddict["P"]:
                    if pimp is None:
                        pimp = np.zeros(refshape)
                    if pimpeb is None:
                        pimpeb = np.zeros(refshape)
                    pimp = pimp + self["PD"]["PZ2"][""]
                    pimpeb = np.sqrt(np.power(pimpeb, 2.0) + np.power(self["PD"]["PZ2"]["EB"], 2.0))
                    thprov = thprov + "; EX2GK: PD_PZ2"

                pfi = None
                pfieb = None
                totprov = thprov
                for src in self.allowed_sources:
                    if "FI"+src in pddict["P"]:
                        if pfi is None:
                            pfi = np.zeros(refshape)
                        if pfieb is None:
                            pfieb = np.zeros(refshape)
                        pfi = pfi + self["PD"]["PFI"+src][""]
                        pfieb = np.sqrt(np.power(pfieb, 2.0) + np.power(self["PD"]["PFI"+src]["EB"], 2.0))
                        totprov = totprov + "; EX2GK: PD_PFI"+src

                (etestflag, wei, weieb, wth, wtheb, wtot, wtoteb) = qtools.energy_content_test(volume, pe, peeb, pi, pieb, pimp, pimpeb, pfi, pfieb, self["ZD"]["WEXP"][""], self["ZD"]["WEXP"]["EB"], passwidth=pw)
                if etestflag is not None:
                    self["ZD"].addData("WEI", wei, weieb, provenance=eiprov)
                    self["ZD"].addData("WTH", wth, wtheb, provenance=thprov)
                    self["ZD"].addData("WTOT", wtot, wtoteb, provenance=totprov)
                else:
                    warnings.warn("Unexpected error occurred during energy content test, aborted", stacklevel=2)

            if self._debug:
                vstr = ""
                if "WEI" in self["ZD"]:
                    vstr = vstr + ", WEI" if vstr else "WEI"
                if "WTH" in self["ZD"]:
                    vstr = vstr + ", WTH" if vstr else "WTH"
                if "WTOT" in self["ZD"]:
                    vstr = vstr + ", WTOT" if vstr else "WTOT"
                if vstr:
                    print("%s - Added %s variables to zero-dimensional data container." % (type(self)._performQualityChecks.__name__, vstr))

            if htestflag is not None:
                self["FLAG"].setFlag("HTEST", htestflag)
            if qtestflag is not None:
                self["FLAG"].setFlag("QTEST", qtestflag)
            if etestflag is not None:
                self["FLAG"].setFlag("ETEST", etestflag)

        else:
            warnings.warn("%s object is not ready for data quality checks" % (type(self).__name__))

# Need to figure out what to do with this QCPASS field
    def postProcess(self, no_assumptions=False, quality_check_sigma=None):

        if self._debug:
            print("%s - Starting post-processing routines..." % (type(self).postProcess.__name__))

        if not no_assumptions:
            self._calculateAssumedProfiles()
        self._calculateMiscellaneousQuantities()
        self._performQualityChecks(quality_check_sigma)

        if self._debug:
            print("%s - Post-processing routines finished." % (type(self).postProcess.__name__))

    def generateEQDSKFile(self, eqdsk_filename, use_fitted_pressures=False):
        if not isinstance(eqdsk_filename, str):
            raise TypeError("%s filename argument must be a string." % (type(self).generateEQDSKFile.__name__))
        if "ZD" in self and self["ZD"].isReady() and "ED" in self and self["ED"].isReady():
            zddata = self["ZD"].grabEquilibriumQuantities()
            eddata = self["ED"].grabEquilibriumQuantities()
            pddata = {}
            if use_fitted_pressures and "X" in eddata and eddata["X"] is not None and "CD" in self and "PD" in self:
                tags = ["E"]
                for ii in range(self["META"]["NUMI"]):
                    tags.append("I%d" % (ii+1))
                edx = eddata["X"]
                zfilt = np.abs(edx) >= 0.1     # Empirical choice as Jacobian causes pressure gradient to diverge to infinity at magnetic axis
                (xvec, jvec, evec) = self["CD"].convert(eddata["X"], eddata["XCS"], self["META"]["CSO"], eddata["XCP"], self["META"]["CSOP"])
                pddata = self["PD"].grabEquilibriumQuantities(tags, xvec)
                pddata["DPEI"][zfilt] = pddata["DPEI"][zfilt] / jvec[zfilt]
            stag = 'EX2GK EQDSK Conversion'
            itag = 42
            nw = eddata["NR"] if "NR" in eddata else None
            nh = eddata["NZ"] if "NZ" in eddata else None
            rdim = eddata["RDIM"] if "RDIM" in eddata else None
            zdim = eddata["ZDIM"] if "ZDIM" in eddata else None
            rcentr = zddata["RVAC"] if "RVAC" in zddata else None
            rleft = eddata["RLEFT"] if "RLEFT" in eddata else None
            zmid = eddata["ZMID"] if "ZMID" in eddata else None
            rmaxis = zddata["RMAG"] if "RMAG" in zddata else None
            zmaxis = zddata["ZMAG"] if "ZMAG" in zddata else None
            simag = zddata["PSIAXS"] if "PSIAXS" in zddata else None
            sibry = zddata["PSIBND"] if "PSIBND" in zddata else None
            bcentr = zddata["BVAC"] if "BVAC" in zddata else None
            current = zddata["IPLA"] if "IPLA" in zddata else None
            fpol = eddata["F"] if "F" in eddata else None
            pres = eddata["P"] if "P" in eddata else None
            if "PEI" in pddata and pddata["PEI"] is not None:
                pres = pddata["PEI"]
            ffprim = eddata["FFP"] if "FFP" in eddata else None
            pprime = eddata["PP"] if "PP" in eddata else None
            if "DPEI" in pddata and pddata["DPEI"] is not None:
                pprime = pddata["DPEI"]
            psirz = eddata["PSI"] if "PSI" in eddata else None
            qpsi = eddata["Q"] if "Q" in eddata else None
            #if qpsi is not None and qpsi[-1] > 0.0:   # No longer needed since default is COCOS=1
            #    qpsi = -qpsi
            nbbbs = eddata["NBND"] if "NBND" in eddata else None
            limitr = eddata["NLIM"] if "NLIM" in eddata else None
            rbbbs = eddata["RBND"] if "RBND" in eddata else None
            zbbbs = eddata["ZBND"] if "ZBND" in eddata else None
            rlim = eddata["RLIM"] if "RLIM" in eddata else None
            zlim = eddata["ZLIM"] if "ZLIM" in eddata else None
            ptools.write_eqdsk_file(fname=eqdsk_filename, \
                                    case=stag, idum=itag, nw=nw, nh=nh, rdim=rdim, zdim=zdim, rcentr=rcentr, rleft=rleft, zmid=zmid, \
                                    rmaxis=rmaxis, zmaxis=zmaxis, simag=simag, sibry=sibry, bcentr=bcentr, current=current, \
                                    fpol=fpol, pres=pres, ffprim=ffprim, pprime=pprime, psirz=psirz, qpsi=qpsi, \
                                    nbbbs=nbbbs, limitr=limitr, rbbbs=rbbbs, zbbbs=zbbbs, rlim=rlim, zlim=zlim)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError("%s keys must be strings" % (type(self).__name__))
        if key.upper() in self.allowed_keys:
            if value is not None:
                if key.upper() == "META":
                    if not isinstance(value, EX2GKMetaContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKMetaContainer.__name__))
                    if not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                if key.upper() == "CS":
                    if not isinstance(value, EX2GKCoordinateContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKCoordinateContainer.__name__))
                    if not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                if key.upper() == "ZD":
                    if not isinstance(value, EX2GKZeroDimensionalContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKZeroDimensionalContainer.__name__))
                    if not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                if key.upper() == "RD":
                    if not isinstance(value, EX2GKRawDataContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKRawDataContainer.__name__))
                    if not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                if key.upper() == "ED":
                    if not isinstance(value, EX2GKEquilibriumDataContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKEquilibriumDataContainer.__name__))
                    if not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                if key.upper() == "FSI" or key.upper() == "FSO":
                    if not isinstance(value, EX2GKFitSettingsContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKFitSettingsContainer.__name__))
                    if key == "FSI" and not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                if key.upper() == "PD":
                    if not isinstance(value, EX2GKProfileDataContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKProfileDataContainer.__name__))
                if key.upper() == "FLAG":
                    if not isinstance(value, EX2GKFlagContainer):
                        raise TypeError("%s %s field must be a %s object" % (type(self).__name__, key, EX2GKFlagContainer.__name__))
                    if not value.isReady():
                        warnings.warn("Passed %s object is not yet ready for processing" % (type(value).__name__))
                super(EX2GKTimeWindow, self).__setitem__(key.upper(), copy.deepcopy(value))
                super(EX2GKTimeWindow, self).__setitem__("UPDATED", datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S'))
            else:
                warnings.warn("None-type items not allowed in %s, item not added" % (type(self).__name__), UserWarning)
        elif key == "UPDATED":
            super(EX2GKTimeWindow, self).__setitem__(key.upper(), copy.deepcopy(value))
        else:
            warnings.warn("%s is not a valid data label in %s, item not added" % (key.upper(), type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, dict):
                self.update(**arg)
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]

    def __repr__(self):
        t1 = self["META"]["T1"] if "META" in self and "T1" in self["META"] else None
        t2 = self["META"]["T2"] if "META" in self and "T2" in self["META"] else None
        return '{0}({1},{2},({3},{4}))'.format(type(self).__name__, repr(self.device), repr(self.shot), repr(t1), repr(t2))

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key == "UPDATED":
                    pass
                elif key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    @classmethod
    def importFromDict(cls, data_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(data_dict, dict):
            raise TypeError("%s data argument must be a Python dictionary" % (cls.importFromDict.__name__))
        dellist = []
        meta_keys = []
        coord_keys = []
        zero_keys = []
        raw_keys = []
        equil_keys = []
        prof_keys = []
        fsi_keys = []
        fso_keys = []
        flag_keys = []
        meta_dict = dict()
        coord_dict = dict()
        zero_dict = dict()
        raw_dict = dict()
        equil_dict = dict()
        prof_dict = dict()
        fsi_dict = dict()
        fso_dict = dict()
        flag_dict = dict()
        update_str = None
        for key, value in data_dict.items():
            mm = re.match(r'^([A-Z]+)_(.+)$', key, flags=re.IGNORECASE)
            if mm and mm.group(1) in self.allowed_keys:
                if mm.group(1).upper() == "META":
                    meta_dict[mm.group(2).upper()] = value
                    meta_keys.append(mm.group(2))
                elif mm.group(1).upper() == "CD":
                    coord_dict[mm.group(2).upper()] = value
                    coord_keys.append(mm.group(2))
                elif mm.group(1).upper() == "ZD":
                    zero_dict[mm.group(2).upper()] = value
                    zero_keys.append(mm.group(2))
                elif mm.group(1).upper() == "RD":
                    raw_dict[mm.group(2).upper()] = value
                    raw_keys.append(mm.group(2))
                elif mm.group(1).upper() == "ED":
                    equil_dict[mm.group(2).upper()] = value
                    equil_keys.append(mm.group(2))
                elif mm.group(1).upper() == "PD":
                    prof_dict[mm.group(2).upper()] = value
                    prof_keys.append(mm.group(2))
                elif mm.group(1).upper() == "FSI":
                    fsi_dict[mm.group(2).upper()] = value
                    fsi_keys.append(mm.group(2))
                elif mm.group(1).upper() == "FSO":
                    fso_dict[mm.group(2).upper()] = value
                    fso_keys.append(mm.group(2))
                elif mm.group(1).upper() == "FLAG":
                    flag_dict[mm.group(2).upper()] = value
                    flag_keys.append(mm.group(2))
            elif key.upper() == "UPDATED":
                update_str = value
        if meta_dict:
            mobj = EX2GKMetaContainer.importFromDict(meta_dict, True)
            self["META"] = mobj
            for nkey in meta_keys:
                if nkey not in meta_dict:
                    dellist.append("META_"+nkey)
        if coord_dict:
            cobj = EX2GKCoordinateContainer.importFromDict(coord_dict, True)
            self["CD"] = cobj
            for nkey in coord_keys:
                if nkey not in coord_dict:
                    dellist.append("CD_"+nkey)
        if zero_dict:
            zobj = EX2GKZeroDimensionalContainer.importFromDict(zero_dict, True)
            self["ZD"] = zobj
            for nkey in zero_keys:
                if nkey not in zero_dict:
                    dellist.append("ZD_"+nkey)
        if raw_dict:
            robj = EX2GKRawDataContainer.importFromDict(raw_dict, True)
            self["RD"] = robj
            for nkey in raw_keys:
                if nkey not in raw_dict:
                    dellist.append("RD_"+nkey)
        if equil_dict:
            eobj = EX2GKEquilibriumDataContainer.importFromDict(equil_dict, True)
            self["ED"] = eobj
            for nkey in equil_keys:
                if nkey not in equil_dict:
                    dellist.append("ED_"+nkey)
        if prof_dict:
            pobj = EX2GKProfileDataContainer.importFromDict(prof_dict, True)
            self["PD"] = pobj
            for nkey in prof_keys:
                if nkey not in prof_dict:
                    dellist.append("PD_"+nkey)
        if fsi_dict:
            iobj = EX2GKFitSettingsContainer.importFromDict(fsi_dict, True)
            self["FSI"] = iobj
            for nkey in fsi_keys:
                if nkey not in fsi_dict:
                    dellist.append("FSI_"+nkey)
        if fso_dict:
            oobj = EX2GKFitSettingsContainer.importFromDict(fso_dict, True)
            self["FSO"] = oobj
            for nkey in fso_keys:
                if nkey not in fso_dict:
                    dellist.append("FSO_"+nkey)
        if flag_dict:
            fobj = EX2GKFlagContainer.importFromDict(flag_dict, True)
            self["FLAG"] = fobj
            for nkey in flag_keys:
                if nkey not in flag_dict:
                    dellist.append("FLAG_"+nkey)
        if update_str is not None:
            self["UPDATED"] = update_str
            if "UPDATED" in data_dict:
                dellist.append("UPDATED")
        for item in dellist:
            del data_dict[item]
        if not suppress_warnings:
            for key in data_dict:
                warnings.warn("Data field %s could not be imported by %s object" % (key, cls.__name__))
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            if key in ["UPDATED"]:
                output[key] = copy.deepcopy(self[key])
            else:
                temp = self[key].exportToDict()
                for nkey, value in temp.items():
                     output[key+"_"+nkey] = value
        return output

    @classmethod
    def importFromPickle(cls, pickle_file):
        if not isinstance(pickle_file, (str, Path)):
            raise TypeError("Pickle import function of %s requires a string file name argument" % (cls.__name__))
        ppath = Path(pickle_file)
        data = {}
        if ppath.is_file():
            data = ptools.unpickle_this(str(ppath.resolve()))
        else:
            raise IOError("Pickle file %s not found. Aborting import into %s object!" % (pickle_file, cls.__name__))
        self = cls.importFromDict(data, suppress_warnings=True)
        return self

    @property
    def device(self):
        device = None
        if "META" in self and "DEVICE" in self["META"]:
            device = self["META"]["DEVICE"]
        return device

    @property
    def shot(self):
        shot_number = None
        if "META" in self and "SHOT" in self["META"]:
            shot_number = self["META"]["SHOT"]
        return shot_number

    @staticmethod
    def getClass(name):
        if name.endswith("CD"):
            this_class = EX2GKCoordinateSystem
        elif name.startswith("RD/"):
            this_class = EX2GKRawDataContainer
        elif name == "ED":
            this_class = EX2GKEquilibriumDataContainer
        elif name == "PD":
            this_class = EX2GKProfileDataContainer
        elif name == "META":
            this_class = EX2GKMetaContainer
        elif name == "ZD":
            this_class = EX2GKZeroDimensionalContainer
        elif name == "FLAG":
            this_class = EX2GKFlagContainer
        else:
            raise ValueError("Unknown name %s" % (name))

        return this_class

    @classmethod
    def importFromPandas(cls, pandas):
        self = cls()
        cses = {}
        for key, val in pandas.items():
            this_class = cls.getClass(key)
            if this_class == EX2GKCoordinateSystem:
                prefix = key[:-2]
                obj = this_class.importFromPandas(val, prefix=prefix)
                cses[prefix] = obj
            elif this_class == EX2GKRawDataContainer:
                obj = this_class.importFromPandas(val)
                if "RD" in self:
                    for name, oneDData in obj.items():
                        self["RD"].addRawDataObject(oneDData)
                else:
                    self["RD"] = obj
            else:
                obj = this_class.importFromPandas(val)
                self[key] = obj

        self["CD"] = EX2GKCoordinateContainer()
        self["CD"].append(cses.pop(""))
        for prefix, obj in cses.items():
            self["CD"].append(obj)
        return self

    def exportToPandas(self):
        pandas = {}
        for key, val in self.items():
            if key not in ["ED", "FSI", "FSO", "UPDATED"]:
                pandas[key] = val.exportToPandas()

        # Re-index Profile Data with used coordinate
#        p_ind = pandas["CD"][self["META"]["CSOP"]][self["META"]["CSOP"] + self["META"]["CSO"]]['V']  # Not the correct vector!
        p_ind = pandas["PD"]["X"]['']
        ind_name = self["META"]["CSOP"] + self["META"]["CSO"]
        pandas["PD"][ind_name] = p_ind
        pandas["PD"].set_index(ind_name, inplace=True)

        # Flatten Coordinate Data
        flat_cd = {}
        if "" in pandas["CD"]:
            flat_cd["CD"] = pandas["CD"].pop("")
        for prefix in list(pandas["CD"].keys()):
            flat_cd["CD/" + prefix] = pandas["CD"].pop(prefix)
        if pandas.pop("CD"):
            raise Exception
        pandas.update(flat_cd)

        # Flatten Raw Data
        if "RD" in pandas:
            for key in list(pandas["RD"].keys()):
                pandas["RD/" + key] = pandas["RD"].pop(key)
            if pandas.pop("RD"):
                raise Exception

        window = (self["META"]["T1"], self["META"]["T2"])
        shot = self["META"]["DEVICE"] + str(self["META"]["SHOT"])
        return pandas

    @staticmethod
    def json_converter(obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

    @classmethod
    def importFromJSONS(cls, jsons):
        dict_ = json.loads(jsons)
        self = cls()
        self.importFromDict(dict_)
        return self

    def exportToJSONS(self, pretty=False):
        if pretty:
            dump_kwargs = {"sort_keys": True,
                           "indent": 4}
        else:
            dump_kwargs = {}

        tw = self.exportToDict()
        jsons = json.dumps(tw, default=self.json_converter, **dump_kwargs)
        return jsons

    # This is primarily here because of legacy storage format, being default as a EX2GKTimeWindow object
    # This function should be called at EX2GKShot level instead to ensure all time slices are exported at once
    def exportToIMAS(self, user, dbname=None, run_number=1, path=".", backend="mdsplus", use_preset_impurities=False):
        imastool.export_to_imas(self, user=user, dbname=dbname, run_number=run_number, path=path, backend=backend, use_preset_impurities=use_preset_impurities)

    @staticmethod
    def convertCanonicalTimeWindow(t1, t2):
        t1_canon = int(np.floor(t1 * 10000))
        t2_canon = int(np.floor(t2 * 10000))
        return t1_canon, t2_canon

    def getCanonicalTimeWindow(self):
        return self.convertCanonicalTimeWindow(self["META"]["T1"], self["META"]["T2"])

    def getCanonicalTag(self, remove_shot_details=False):
        machine_canon = self["META"]["DEVICE"].upper()
        shot_canon = self["META"]["SHOT"]
        t1_canon, t2_canon = self.getCanonicalTimeWindow()
        tag = "%s_%d_%d_%d" % (machine_canon, shot_canon, t1_canon, t2_canon) if not remove_shot_details else "%d_%d" % (t1_canon, t2_canon)
        return tag

    def primeForMATLABConversion(self, include_coordinates=True, include_globals=True, include_profiles=True, include_equilibrium=True, include_raw_data=False, include_fit_settings=False, include_flags=False, include_descriptions=True,
                                       phase_filter=None, quality_filter=False, cx_filter=False, rot_filter=False, source_filter=False, verbose=0):
        outdict = {}
        passed = True
        if isinstance(phase_filter, int):
            passed = passed and self["META"]["WINPHASE"] == phase_filter
        if quality_filter:
            if "FLAG" in self and "HTEST" in self["FLAG"] and "QTEST" in self["FLAG"] and "ETEST" in self["FLAG"]:
                passed = passed and self["FLAG"]["HTEST"] and self["FLAG"]["QTEST"] and self["FLAG"]["ETEST"]
            else:
                passed = False
        if cx_filter:
            temp = False
            pf = self["RD"].getPresentFields('list') if "RD" in self else []
            for ii in range(len(pf)):
                if pf[ii].startswith("TI"):
                    temp = True
            passed = passed and temp
        if rot_filter:
            pf = self["RD"].getPresentFields('dict') if "RD" in self else {}
            passed = passed and "AF" in pf
        if source_filter:
            pf = self["RD"].getPresentFields('dict') if "RD" in self else {}
            # Checks for particle and momentum sources removed for simplicity
            passed = passed and "ST" in pf and "J" in pf # and "SN" in pf and "SP" in pf
        if passed:
            outdict["SAMPLE"] = {"SHOT": np.array([self["META"]["SHOT"]]), "TWIN": np.array([self["META"]["T1"], self["META"]["T2"]])}
            dellist = []
            for key in self:
                if key == "UPDATED":
                    outdict[key] = copy.deepcopy(self[key])
                elif key == "CD" and include_coordinates:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                elif key == "ZD" and include_globals:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                elif key == "PD" and include_profiles:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                elif key == "ED" and include_equilibrium:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                elif key == "RD" and include_raw_data:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                elif key in ["FSI", "FSO"] and include_fit_settings:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                elif key == "FLAG" and include_flags:
                    outdict[key] = self[key].stripFromClass(with_descriptions=include_descriptions)
                else:
                    dellist.append(key)
            if include_descriptions:
                descdict = self.describe()
                for key in dellist:
                    if key in descdict:
                        del descdict[key]
                outdict["DESC"] = descdict
                outdict["COMMENT"] = "Profiles produced using EX2GK-"+__version__+". Profile error bars are combined fit and measurement errors, quoted as 1 sigma."
        return outdict

    def primeForJETPEAKConversion(self, ordered=None, **kwargs):
        settings = {}
        settings.update(kwargs)
        settings["include_coordinates"] = True
        settings["include_globals"] = True
        settings["include_profiles"] = True
        settings["include_equilibrium"] = False
        settings["include_raw_data"] = False
        settings["include_fit_settings"] = False
        settings["include_flags"] = False
        settings["include_descriptions"] = True
        outdict = self.primeForMATLABConversion(**settings)
        if "ZD" in outdict:
            removelist = ["NESCALE", "TESEPTARGET", "NESEPTARGET", "PEDTOP", "EQLIMX", "EQLIMPIN", "EQLIMPEQ", "VAVG", "TAUE"]
            for key in outdict["ZD"]:
                if key.startswith("OUT"):
                    removelist.append(key)
            for key in removelist:
                if key in outdict["ZD"]:
                    del outdict["ZD"][key]
                    if "DESC" in outdict["ZD"] and key in outdict["ZD"]["DESC"]:
                        del outdict["ZD"]["DESC"][key]
        if "PD" in outdict:
            removelist = ["ZEFFP"]
            for key in outdict["PD"]:
                if key.startswith("OUT"):
                    removelist.append(key)
            for key in removelist:
                if key in outdict["PD"]:
                    del outdict["PD"][key]
                    if "DESC" in outdict["PD"] and key in outdict["PD"]["DESC"]:
                        del outdict["PD"]["DESC"][key]
        descdict = ptools.extract_key(outdict, "DESC")
        dcomment = outdict.pop("COMMENT", None)
        if dcomment is not None:
            descdict["COMMENT"] = dcomment
        return outdict, descdict

    def exportToMATLAB(self, matfilename, minimal=True, **kwargs):
        if not isinstance(matfilename, str):
            raise TypeError("%s .mat file name argument must be a string. MATLAB file creation aborted." % (type(self).exportToMATLAB.__name__))
        settings = {}
        settings.update(kwargs)
        if minimal:
            settings["include_coordinates"] = True
            settings["include_globals"] = True
            settings["include_profiles"] = True
            settings["include_equilibrium"] = False
            settings["include_raw_data"] = False
            settings["include_fit_settings"] = False
            settings["include_flags"] = False
            settings["include_descriptions"] = False
        matdict = self.primeForMATLABConversion(**settings)
        status = False
        if matdict:
            fmtconv.obj2mat(matdict, matfilename)
            status = True
        return status

    def exportToJETPEAK(self, matfilename, ordered=None, **kwargs):
        if not isinstance(matfilename, str):
            raise TypeError("%s .mat file name argument must be a string. MATLAB file creation for JETPEAK aborted." % (type(self).exportToJETPEAK.__name__))
        settings = {}
        settings.update(kwargs)
        matdict, descdict = self.primeForJETPEAKConversion(ordered=ordered, **settings)
        matdict = ptools.insert_key(matdict, descdict, "DESC")
        if "COMMENT" in descdict:
            matdict["COMMENT"] = descdict["COMMENT"]
        status = False
        if matdict:
            fmtconv.obj2mat(matdict, matfilename)
            status = True
        return status


class EX2GKShot(OrderedDict):

    def __init__(self, *args, **kwargs):
        super(EX2GKShot, self).__init__()
        self._machine = None
        self._idnum = None
        self.update(*args, **kwargs)
        self._debug = False

    def setDebugMode(self, state=True):
        self._debug = True if state else False
        mode = "" if self._debug else "de"
        print("%s - Debug mode %sactivated." % (type(self).__name__, mode))
        for key in self:
            self[key].setDebugMode(self._debug)

    def _update_class_variables(self):
        if len(self.values()) == 0:
            self._machine = None
            self._idnum = None
            warnings.warn("%s instance is empty!" % (type(self).__name__), UserWarning)

    def update(self, *args, **kwargs):
        if "_machine" not in self.__dict__:
            self._machine = None
        if "_idnum" not in self.__dict__:
            self._idnum = None
        if "_machine" in kwargs:
            if isinstance(kwargs["_machine"], str):
                self.device = kwargs["_machine"]
            else:
                warnings.warn("%s machine keyword argument must be a string, input not accepted" % (type(self).__name__), UserWarning)
            del kwargs["_machine"]
        if "_idnum" in kwargs:
            if isinstance(kwargs["_idnum"], number_types) and int(kwargs["_idnum"]) >= 0:
                self.shot = int(kwargs["_idnum"])
            else:
                warnings.warn("%s identification number keyword argument must be a positive integer, input not accepted" % (type(self).__name__), UserWarning)
            del kwargs["_idnum"]
        for key in kwargs:
            self.insertTimeWindow(kwargs[key], sort=False)
        for arg in args:
            self.insertTimeWindow(arg, sort=False)
        self.sort()

    def insertTimeWindow(self, tw, sort=True, force=True):
        if isinstance(tw, EX2GKTimeWindow):
            iflag = False
            c_tw = tw.getCanonicalTimeWindow()
            if c_tw not in self.keys():
                self[c_tw] = tw
                iflag = True
            elif force:
                warnings.warn("Timewindow with canonical times (%s, %s) already in shot. Overwriting.." % c_tw)
                self[c_tw] = tw
                iflag = True
            else:
                warnings.warn("Timewindow with canonical times (%s, %s) already in shot." % c_tw)

            if sort and iflag:
                self.sort()
        else:
            warnings.warn("%s could not insert object, not a EX2GKTimeWindow instance!" % (type(self).insertTimeWindow.__name__), UserWarning)

    def __setitem__(self, key, value):
        if not isinstance(key, tuple):
            raise TypeError("%s keys must be tuples, recommended to use %s for time window insertion" % (type(self).__name__, type(self).insertTimeWindow.__name__))
        if isinstance(value, EX2GKTimeWindow):
            if self._machine is None:
                self._machine = value.device
            if self._idnum is None:
                self._idnum = value.shot
            if value.device != self._machine:
                warnings.warn("Timewindow (%s, %s) does not belong to device %s" % (key, self.device), UserWarning)
            elif value.shot != self._idnum:
                warnings.warn("Timewindow (%s, %s) does not belong to shot number %s" % (key, self._idnum), UserWarning)
            else:
                super(EX2GKShot, self).__setitem__(key, copy.deepcopy(value))
        else:
            warnings.warn("%s is not a valid data type in %s, item not added" % (type(value).__name__, type(self).__name__), UserWarning)

    def __repr__(self):
        content_string = ''
        for tw in self.values():
            if content_string:
                content_string = content_string + ', '
            content_string = content_string + '{0}({1})'.format(type(tw).__name__, repr(tw.getCanonicalTimeWindow()))
        return '{0}({1}, {2} : [{3}])'.format(type(self).__name__, repr(self._machine), repr(self._idnum), content_string)

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if self._machine is None:
            match &= (self._machine is other._machine)
        else:
            match &= (self._machine == other._machine)
        if self._idnum is None:
            match &= (self._idnum is other._idnum)
        else:
            match &= (self._idnum == other._idnum)
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    def sort(self):
        sorted_keys = []
        t0s = [t[0] for t in self.keys()]
        ut0s = sorted(list(set(t0s))) # Sorted unique elements
        for ut0 in ut0s:
            t1s = [t[1] for t in self.keys() if t[0] == ut0]
            if len(t1s) == 1:
                sorted_keys.append((ut0, t1s[0]))
            else:
                sorted_t1s = sorted(t1s, reverse=True)
                sorted_keys.extend([(ut0, t1) for t1 in sorted_t1s])
        for key_ in sorted_keys[1:]:
            self.move_to_end(key_)

    def getByIndex(self, index):
        return list(self.values())[index]

    def getByCanonicalTimeWindow(self, ct1, ct2, tolerance=0):
        tol = int(tolerance) if isinstance(tolerance, number_types) else 0
        time_window_out = None
        for key, val in self.items():
            ctw = val.getCanonicalTimeWindow()
            if ctw[0] <= (ct1 + tol) and ctw[0] >= (ct1 - tol) and ctw[1] <= (ct2 + tol) and ctw[1] >= (ct2 - tol):
                time_window_out = copy.deepcopy(val)
        return time_window_out

    def getSubKeyUnion(self):
        key_union = set()
        for tw in self.values():
            key_union = key_union.union(tw.keys())
        return key_union

    def getSubSubKeyUnion(self):
        key_unions = {}
        for key in self.getSubKeyUnion():
            if key != 'UPDATED':
                key_unions[key] = set()
                for tw in self.values():
                    sub_key = tw.get(key, None)
                    if sub_key is not None:
                        key_unions[key] = key_unions[key].union(sub_key.keys())
            else:
                key_unions[key] = set(['UPDATED'])
        if 'CD' in key_unions:
            deep_cd_keys = {}
            for sub_key in key_unions['CD']:
                deep_cd_keys[sub_key] = set()
                for tw in self.values():
                    sub_sub_key = tw['CD'].get(sub_key, None)
                    if sub_sub_key is not None:
                        deep_cd_keys[sub_key] = deep_cd_keys[sub_key].union(sub_sub_key.keys())
            key_unions['CD'] = deep_cd_keys
        return key_unions

    @property
    def shot(self):
        self._update_class_variables()
        return self._idnum

    @shot.setter
    def shot(self, value):
        if len(self.values()) == 0:
            if not (isinstance(value, number_types) and int(value) >= 0):
                raise ValueError("%s identification number must be a positive integer" % (type(self).__name__))
            self._idnum = int(value)
        else:
            warnings.warn("Can only change %s identification number if no time windows are stored inside" % (type(self).__name__))

    @property
    def device(self):
        self._update_class_variables()
        return self._machine

    @device.setter
    def device(self, value):
        if len(self.values()) == 0:
            if not isinstance(value, str):
                raise ValueError("%s machine label must be a string" % (type(self).__name__))
            self._machine = value
        else:
            warnings.warn("Can only change %s machine label if no time windows are stored inside" % (type(self).__name__))

    @classmethod
    def importFromList(cls, data_list, suppress_warnings=False):
        self = cls()
        if not isinstance(data_list, array_types):
            raise ValueError('Could not import object, not an array-like object!')
        for ii in range(0, len(data_list)):
            try:
                twobj = EX2GKTimeWindow.importFromDict(data_list[ii], suppress_warnings=True)
                self.insertTimeWindow(twobj, sort=False, force=False)
            except:
                if not suppress_warnings:
                    warnings.warn("Element number %d in list object does not make a valid EX2GKTimeWindow object" % (ii), UserWarning)
        self.sort()
        return self

    def exportToList(self):
        output = []
        self.sort()
        for tw in self.values():
            output.append(tw.exportToDict())
        return output

    def exportToPandas(self):
        panda_dicts = {}
        for c_tw, tw in self.items():
            panda = tw.exportToPandas()
            for k, v in panda.items():
                if k not in panda_dicts:
                    panda_dicts[k] = OrderedDict()
                panda_dicts[k][c_tw] = v
        for k in list(panda_dicts.keys()):
            panda_dicts[k] = pd.concat(panda_dicts[k].values(), keys=range(len(panda_dicts)), names=['Timewindow'])
        return panda_dicts

    def exportToIMAS(self, user, dbname=None, run_number=1, path=".", backend="mdsplus", use_preset_impurities=False):
        imastool.export_to_imas(self, user=user, dbname=dbname, run_number=run_number, path=path, backend=backend, use_preset_impurities=use_preset_impurities)

    def getCanonicalIndex(self):
        return (self.device.upper(), self.shot)

    def getCanonicalTag(self):
        (machine_canon, shot_canon) = self.getCanonicalIndex()
        tag = "%s_%d" % (machine_canon, shot_canon)
        return tag

    def exportEachToMATLAB(self, matfilebasename, minimal=True, produce_eqdsk=True, \
                                 phase_filter=None, quality_filter=False, cx_filter=False, rot_filter=False, source_filter=False):
        if not isinstance(matfilebasename, str):
            raise TypeError("%s base name argument must be a string" % (type(self).exportEachToMATLAB.__name__))
        for tw in self.values():
            filetag = matfilebasename[:-4] if matfilebasename.endswith('.mat') else matfilebasename
            filetag = filetag+"_"+tw.getCanonicalTag(remove_shot_details=True)
            passed = tw.exportToMATLAB(filetag+'.mat', \
                                       minimal=minimal, \
                                       phase_filter=phase_filter, \
                                       quality_filter=quality_filter, \
                                       cx_filter=cx_filter, \
                                       rot_filter=rot_filter, \
                                       source_filter=source_filter
                                      )
            if passed and produce_eqdsk:
                try:
                    tw.generateEQDSKFile(filetag+'.eqdsk', use_fitted_pressures=False)
                except Exception:
                    subprocess.run(['rm', filetag+'.*'])

    def primeForMATLABConversion(self, minimal=True, phase_filter=None, quality_filter=False, cx_filter=False, rot_filter=False, source_filter=False, verbose=0):
        outdict = {}
        include_globals = True
        include_profiles = True
        include_equilibrium = True
        include_raw_data = True
        include_fit_settings = True
        include_flags = True
        include_descriptions = True
        if minimal:
            include_globals = True
            include_profiles = True
            include_equilibrium = False
            include_raw_data = False
            include_fit_settings = False
            include_flags = False
            include_descriptions = False
        for tw in self.values():
            if verbose > 0:
                times = tw.getCanonicalTimeWindow()
                print("   Converting time window [%.4f, %.4f]..." % (times[0] / 1.0e4, times[1] / 1.0e4))
            twtag = tw.getCanonicalTag(remove_shot_details=True)
            twdict = tw.primeForMATLABConversion(include_globals=include_globals, \
                                                 include_profiles=include_profiles, \
                                                 include_equilibrium=include_equilibrium, \
                                                 include_raw_data=include_raw_data, \
                                                 include_fit_settings=include_fit_settings, \
                                                 include_flags=include_flags, \
                                                 include_descriptions=include_descriptions, \
                                                 phase_filter=phase_filter, \
                                                 quality_filter=quality_filter, \
                                                 cx_filter=cx_filter, \
                                                 rot_filter=rot_filter, \
                                                 source_filter=source_filter, \
                                                 verbose=verbose-1
                                                )
            if twdict:
                outdict[twtag] = twdict
        return outdict

    def primeForJETPEAKConversion(self, ordered=None, tolerance=0, **kwargs):
        olist = []
        outdict = {}
        descdict = {}
        if isinstance(ordered, (list, tuple)):
            for ii in range(len(ordered)):
                if isinstance(ordered[ii], (list, tuple)) and len(ordered[ii]) >= 3 and \
                   isinstance(ordered[ii][0], number_types) and int(ordered[ii][0]) == self.shot and \
                   isinstance(ordered[ii][1], number_types) and int(ordered[ii][1]) >= 0 and \
                   isinstance(ordered[ii][2], number_types) and int(ordered[ii][2]) >= int(ordered[ii][1]):
                    tempitem = (ordered[ii][0], ordered[ii][1], ordered[ii][2])
                    olist.append(tempitem)
        if len(olist) > 0:
            settings = {}
            settings.update(kwargs)
#            settings["include_coordinates"] = True
#            settings["include_globals"] = True
#            settings["include_profiles"] = True
#            settings["include_equilibrium"] = False
#            settings["include_raw_data"] = False
#            settings["include_fit_settings"] = False
#            settings["include_flags"] = False
#            settings["include_descriptions"] = True
            for ii in range(len(olist)):
                ttw = self.getByCanonicalTimeWindow(olist[ii][1], olist[ii][2], tolerance=tolerance)
                if ttw is not None:
                    twdict, twddict = ttw.primeForJETPEAKConversion(**settings)
                    if twdict:
                        if outdict:
                            outdict = ptools.recursive_dict_append(outdict, twdict)
                        else:
                            outdict.update(twdict)
                    if twddict:
                        if descdict:
                            descdict = ptools.update_key(descdict, twddict, "DESC")
                            if "COMMENT" in twddict:
                                descdict["COMMENT"] = twddict["COMMENT"]
                        else:
                            descdict.update(twddict)
        else:
            settings = {}
            settings.update(kwargs)
            for tw in self.values():
                twdict, twddict = tw.primeForJETPEAKConversion(**settings)
                if twdict:
                    if outdict:
                        outdict = ptools.recursive_dict_append(outdict, twdict)
                    else:
                        outdict.update(twdict)
                    if twddict:
                        if descdict:
                            descdict = ptools.update_key(descdict, twddict, "DESC")
                            if "COMMENT" in twddict:
                                descdict["COMMENT"] = twddict["COMMENT"]
                        else:
                            descdict.update(twddict)
        return outdict, descdict

    def exportToMATLAB(self, matfilename, minimal=True, **kwargs):
        if not isinstance(matfilename, str):
            raise TypeError("%s .mat file name argument must be a string. MATLAB file creation aborted." % (type(self).exportToMATLAB.__name__))
        settings = {"minimal": minimal}
        settings.update(kwargs)
        matdict = self.primeForMATLABConversion(**settings)
        status = False
        if matdict:
            fmtconv.obj2mat(matdict, matfilename)
            status = True
        return status

    def exportToJETPEAK(self, matfilename, ordered=None, **kwargs):
        if not isinstance(matfilename, str):
            raise TypeError("%s .mat file name argument must be a string. MATLAB file creation for JETPEAK aborted." % (type(self).exportToJETPEAK.__name__))
        settings = {}
        settings.update(kwargs)
        matdict, descdict = self.primeForJETPEAKConversion(ordered=ordered, **settings)
        matdict = ptools.insert_key(matdict, descdict, "DESC")
        if "COMMENT" in descdict:
            matdict["COMMENT"] = descdict["COMMENT"]
        status = False
        if matdict:
            fmtconv.obj2mat(matdict, matfilename)
            status = True
        return status


class EX2GKShotCollection(OrderedDict):
    def __init__(self, *args, **kwargs):
        super(EX2GKShotCollection, self).__init__()
        self.update(*args, **kwargs)
        self._debug = False

    def setDebugMode(self, state=True):
        self._debug = True if state else False
        mode = "" if self._debug else "de"
        print("%s - Debug mode %sactivated." % (type(self).__name__, mode))
        for key in self:
            self[key].setDebugMode(self._debug)

    def insertShot(self, shot, sort=True, force=True):
        if isinstance(shot, EX2GKShot):
            iflag = False
            index = shot.getCanonicalIndex()
            if index not in self.keys():
                self[index] = shot
                iflag = True
            elif force:
                warnings.warn("Shot with device %s and number %s already in collection. Overwriting.." % (shot.device, shot.shot))
                self[index] = shot
                iflag = True
            else:
                warnings.warn("Shot with device %s and number %s already in collection." % (shot.device, shot.shot))

            if iflag and sort:
                self.sort()
        else:
            warnings.warn("%s could not insert object, not a EX2GKShot instance!" % (type(self).insertShot.__name__), UserWarning)

    def mergeTimeWindow(self, time_window, sort=True, force=False):
        if isinstance(time_window, EX2GKTimeWindow):
            shot = EX2GKShot()
            shot.insertTimeWindow(time_window)
            self.mergeShot(shot, sort=sort, force=force)
        else:
            warnings.warn("%s could not merge object, not a EX2GKTimeWindow instance!" % (type(self).mergeTimeWindow.__name__), UserWarning)

    def mergeShot(self, shot, sort=True, force=False):
        if isinstance(shot, EX2GKShot):
            index = shot.getCanonicalIndex()
            if index not in self.keys():
                self.insertShot(shot)
            else:
                for key, value in shot.items():
                    self[index].insertTimeWindow(value, sort=False, force=force)
                self[index].sort()

            if sort:
                self.sort()
        else:
            warnings.warn("%s could not merge object, not a EX2GKShot instance!" % (type(self).mergeShot.__name__), UserWarning)

    def mergeCollection(self, collection, sort=True, force=False):
        if isinstance(collection, EX2GKShotCollection):
            for index in collection:
                if index not in self.keys():
                    self.insertShot(collection[index], sort=sort, force=force)
                else:
                    self.mergeShot(collection[index], sort=sort, force=force)
        else:
            warnings.warn("%s could not merge object, not a EX2GKShotCollection instance!" % (type(self).mergeCollection.__name__), UserWarning)

    def update(self, *args, **kwargs):
        for key in kwargs:
            self.insertShot(kwargs[key], sort=False)
        for arg in args:
            self.insertShot(arg, sort=False)
        self.sort()

    def __setitem__(self, key, value):
        if not isinstance(key, tuple):
            raise TypeError("%s keys must be tuples, recommended to use %s for shot insertion" % (type(self).__name__, type(self).insertShot.__name__))
        if isinstance(value, EX2GKShot):
            super(EX2GKShotCollection, self).__setitem__(key, copy.deepcopy(value))
        else:
            warnings.warn("%s is not a valid data type in %s, item not added" % (type(value).__name__, type(self).__name__), UserWarning)

    def __repr__(self):
        content_string = ''
        for key, value in self.items():
            if content_string:
                content_string = content_string + ', '
            content_string = content_string + repr(value)
        return '{0}({1})'.format(type(self).__name__, content_string)

    def __copy__(self):
        return type(self)(self)

    def copy(self):
        return type(self)(self)

    def __eq__(self, other):
        match = isinstance(other, type(self)) and (len(self) == len(other))
        if match:
            for key in self:
                if key in other:
                    if self[key] is None:
                        match &= (self[key] is other[key])
                    elif isinstance(self[key], np.ndarray):
                        match &= np.all(np.isclose(self[key], other[key], equal_nan=True))
                    elif isinstance(self[key], float):
                        match &= np.isclose(self[key], other[key], equal_nan=True)
                    else:
                        match &= (self[key] == other[key])
                else:
                    match = False
                    print("%s %s mismatch" % (type(self).__name__, key))
                    break
        else:
            print("%s configuration mismatch" % (type(self).__name__))
        return match

    def sort(self):
        sorted_keys = sorted(self.keys())
        for key_ in sorted_keys[1:]:
            self.move_to_end(key_)

    def getByCanonicalIndex(self, device, shot):
        return self[(device, shot)]

    def getByIndex(self, index):
        return list(self.values())[index]

    def getByShotNumber(self, shot):
        tempout = None
        for key in self:
            if key[1] == shot:
                tempout = self[key]
        if tempout is None:
            raise KeyError(repr(shot))
        return tempout

    def getSubKeyUnion(self):
        sub_key_union = {}
        for shot in self.values():
            shot_key_union = shot.getSubSubKeyUnion()
            for key in shot_key_union:
                if key not in sub_key_union:
                    if key != 'CD':
                        sub_key_union[key] = set()
                    else:
                        sub_key_union[key] = {}
                if key != 'CD':
                    sub_key_union[key] = sub_key_union[key].union(shot_key_union[key])
                else:
                    for sub_key in shot_key_union[key]:
                        if sub_key not in sub_key_union[key]:
                            sub_key_union[key][sub_key] = set()
                        sub_key_union[key][sub_key] = sub_key_union[key][sub_key].union(shot_key_union[key][sub_key])
        return sub_key_union

    @classmethod
    def importFromDict(cls, shot_dict, suppress_warnings=False):
        self = cls()
        if not isinstance(shot_dict, (dict, OrderedDict)):
            raise ValueError("Could not import object, not a dict!")
        for key, value in shot_dict.items():
            try:
                sobj = EX2GKShot.importFromList(value, suppress_warnings)
                self.insertShot(sobj, sort=False, force=False)
            except:
                if not suppress_warnings:
                    warnings.warn("Item label %s in dict does not make a valid EX2GKShot object" % (key), UserWarning)
        self.sort()
        return self

    def exportToDict(self):
        output = dict()
        for key in self:
            if key[1] in output:
                warnings.warn("Shot identification number clash occurred during export of %s!" % (type(self).__name__))
            output[key[1]] = self[key].exportToList()
        return output

    @classmethod
    def importFromBrine(cls, directory_name, force=True, suppress_warnings=True):
        self = cls()
        if not isinstance(directory_name, str):
            raise ValueError("%s directory argument must be a string" % (cls.importFromBrine.__name__))
        inpath = Path(directory_name)
        if inpath.is_dir():
            for fn in inpath.glob("**/*.pkl"):
                try:
                    tw = EX2GKTimeWindow.importFromPickle(fn)
                    shot = EX2GKShot()
                    shot.insertTimeWindow(tw, sort=True, force=force)
                    self.mergeShot(shot, sort=True, force=force)
                except:
                    if not suppress_warnings:
                        warnings.warn("Pickle file %s does not make a valid EX2GKTimeWindow object" % (fn), UserWarning)
            self.sort()
        else:
            raise IOError("Brine directory %s not found. Aborting import into %s object!" % (inpath, cls.__name__))
        return self

    @classmethod
    def importFromPickle(cls, pickle_filename, pickle_filedir=None, force=True, suppress_warnings=True):
        self = cls()
        pdir = Path(pickle_filedir) if isinstance(pickle_filedir, str) else Path('./')
        if pdir.is_dir() and isinstance(pickle_filename, str):
            ppath = pdir / pickle_filename
            if ppath.is_file():
                data = ptools.unpickle_this(str(ppath.resolve()))
                if isinstance(data, dict):
                    if "META_SHOT" not in data:
                        try:
                            self = cls.importFromDict(data, suppress_warnings=False)
                        except:
                            if not suppress_warnings:
                                warnings.warn("Pickle file %s does not make a valid %s object" % (ppath, cls.__name__), UserWarning)
                    else:
                        try:
                            tw = EX2GKTimeWindow.importFromDict(data)
                            shot = EX2GKShot()
                            shot.insertTimeWindow(tw, sort=True, force=force)
                            self.insertShot(shot, sort=True, force=force)
                        except:
                            if not suppress_warnings:
                                warnings.warn("Pickle file %s does not make a valid %s object for insertion into %s object" % (ppath, EX2GKTimeWindow.__name__, cls.__name__), UserWarning)
            else:
                raise IOError("Pickle file %s not found. Aborting import!" % (ppath))
        else:
            raise IOError("Pickle file directory %s not found. Aborting import!" % (pdir))
        return self

    def exportToPandas(self):
        all_shots_dicts = {}
        for shotno, shot in self.items():
            panda_dict = shot.exportToPandas()
            for k in panda_dict.keys():
                if k not in all_shots_dicts:
                    all_shots_dicts[k] = OrderedDict()
                all_shots_dicts[k][shotno] = panda_dict[k]

        full_panda = {}
        for k, v in all_shots_dicts.items():
            full_panda[k] = pd.concat(all_shots_dicts[k], names=['Device', 'Shot'])
        return full_panda

    def exportToIMAS(self, user, path=".", backend="mdsplus", use_preset_impurities=False):
        if not isinstance(user, str):
            raise TypeError("%s user ID argument must be a string, aborting!" % (type(self).exportToIMAS.__name__))
        for shotno, shot in self.items():
            shot.exportToIMAS(user, path=path, backend=backend, use_preset_impurities=use_preset_impurities)

    def exportEachToMATLAB(self, matfilebasename=None, minimal=True, produce_eqdsk=True, \
                             phase_filter=None, quality_filter=False, cx_filter=False, rot_filter=False, source_filter=False):
        basename = matfilebasename if isinstance(matfilebasename, str) else ""
        for shot in self.values():
            filetag = basename[:-4] if basename.endswith('.mat') else basename
            if basename and not basename.endswith('/'):
                filetag = filetag+'_'
            filetag = filetag+shot.getCanonicalTag()
            shot.exportEachToMATLAB(filetag+'.mat', minimal=minimal, produce_eqdsk=produce_eqdsk, \
                                    phase_filter=phase_filter, quality_filter=quality_filter, \
                                    cx_filter=cx_filter, rot_filter=rot_filter, source_filter=source_filter)

    def primeForMATLABConversion(self, minimal=True, phase_filter=None, quality_filter=False, cx_filter=False, rot_filter=False, source_filter=False, verbose=0):
        outdict = {}
        ii = 0
        for shot in self.values():
            if verbose > 0:
                print("Converting %s..." % (str(shotno)))
            shottag = shot.getCanonicalTag()
            shotdict = shot.primeForMATLABConversion(minimal=minimal, \
                                                     phase_filter=phase_filter, \
                                                     quality_filter=quality_filter, \
                                                     cx_filter=cx_filter, \
                                                     rot_filter=rot_filter, \
                                                     source_filter=source_filter, \
                                                     verbose=verbose-1
                                                    )
            if shotdict:
                outdict[shottag] = shotdict
        return outdict

    def primeForJETPEAKConversion(self, ordered=None, tolerance=0, **kwargs):
        olist = []
        outdict = {}
        descdict = {}
        if isinstance(ordered, (list, tuple)):
            for ii in range(len(ordered)):
                if isinstance(ordered[ii], (list, tuple)) and len(ordered[ii]) >= 3 and \
                   isinstance(ordered[ii][0], number_types) and int(ordered[ii][0]) > 0 and \
                   isinstance(ordered[ii][1], number_types) and int(ordered[ii][1]) >= 0 and \
                   isinstance(ordered[ii][2], number_types) and int(ordered[ii][2]) >= int(ordered[ii][1]):
                    tempitem = (ordered[ii][0], ordered[ii][1], ordered[ii][2])
                    olist.append(tempitem)
        if len(olist) > 0:
            settings = {}
            settings.update(kwargs)
#            settings["include_coordinates"] = True
#            settings["include_globals"] = True
#            settings["include_profiles"] = True
#            settings["include_equilibrium"] = False
#            settings["include_raw_data"] = False
#            settings["include_fit_settings"] = False
#            settings["include_flags"] = False
            for ii in range(len(olist)):
                tshot = self.getByShotNumber(olist[ii][0])
                if tshot is not None:
                    ttw = tshot.getByCanonicalTimeWindow(olist[ii][1], olist[ii][2], tolerance=tolerance)
                    if ttw is not None:
                        twdict, twddict = ttw.primeForJETPEAKConversion(**settings)
                        if twdict:
                            if outdict:
                                outdict = ptools.recursive_dict_append(outdict, twdict)
                            else:
                                outdict.update(twdict)
                            if twddict:
                                if descdict:
                                    descdict = ptools.update_key(descdict, twddict, "DESC")
                                    if "COMMENT" in twddict:
                                        descdict["COMMENT"] = twddict["COMMENT"]
                                else:
                                    descdict.update(twddict)
        else:
            settings = {}
            settings.update(kwargs)
            for shot in self.values():
                shotdict, shotddict = shot.primeForJETPEAKConversion(**settings)
                if shotdict:
                    if outdict:
                        outdict = ptools.recursive_dict_append(outdict, shotdict)
                    else:
                        outdict.update(shotdict)
                    if shotddict:
                        if descdict:
                            descdict = ptools.update_key(descdict, shotddict, "DESC")
                            if "COMMENT" in shotddict:
                                descdict["COMMENT"] = shotddict["COMMENT"]
                        else:
                            descdict.update(shotddict)
        return outdict, descdict

    def exportToMATLAB(self, matfilename, minimal=True, **kwargs):
        if not isinstance(matfilename, str):
            raise TypeError("%s .mat file name argument must be a string. MATLAB file creation aborted." % (type(self).exportToMATLAB.__name__))
        settings = {"minimal": minimal}
        settings.update(kwargs)
        matdict = self.primeForMATLABConversion(**settings)
        status = False
        if matdict:
            fmtconv.obj2mat(matdict, matfilename)
            status = True
        return status

    def exportToJETPEAK(self, matfilename, ordered=None, **kwargs):
        if not isinstance(matfilename, str):
            raise TypeError("%s .mat file name argument must be a string. MATLAB file creation for JETPEAK aborted." % (type(self).exportToJETPEAK.__name__))
        settings = {}
        settings.update(kwargs)
        matdict, descdict = self.primeForJETPEAKConversion(ordered=ordered, **settings)
        matdict = ptools.insert_key(matdict, descdict, "DESC")
        if "COMMENT" in descdict:
            matdict["COMMENT"] = descdict["COMMENT"]
        status = False
        if matdict:
            fmtconv.obj2mat(matdict, matfilename)
            status = True
        return status
