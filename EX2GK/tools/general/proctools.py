# Script with functions to facilitate processing of any data
# Developer: Aaron Ho - 04/04/2017

# Required imports
import os
import sys
import copy
import numpy as np
import re
import pickle
import functools
from scipy.special import gamma
from scipy.interpolate import interp1d

# Custom imports
from EX2GK.GPR1D import GPR1D as gp

np_itypes = (np.int8, np.int16, np.int32, np.int64)
np_utypes = (np.uint8, np.uint16, np.uint32, np.uint64)
np_ftypes = (np.float16, np.float32, np.float64)

number_types = (float, int, np_itypes, np_utypes, np_ftypes)
array_types = (list, tuple, np.ndarray)
#string_types = (str, np.str_)


def pickle_this(obj, filename, filedir='./'):
    """
    Writes a Python object into a Python-style binary file, or pickle.
    Only truly useful for those not fluent in Python pickling.

    :arg obj: obj. Python object to be pickled.

    :arg filename: str. Name of the pickle binary file to be created.

    :kwarg filedir: str. Optional path to the directory which pickle file will be stored in. Default is pwd.

    :returns: int. Success flag. 0 = success, 1 otherwise
    """
    status = 1
    fdir = copy.deepcopy(filedir)
    if isinstance(fdir, str) and not fdir.endswith('/'):
        fdir = fdir + '/'
    if os.path.isdir(fdir):
        if not re.search(r'\.p(kl)?$', filename):
            filename = filename + '.pkl'
        with open(fdir + filename, 'wb') as bf:
            pickle.dump(obj, bf, protocol=3)
            status = 0
    return status


def unpickle_this(filepath, filedir=None):
    """
    Loads a Python-style binary file, or pickle, into memory.
    Only truly useful for those not fluent in Python pickling.

    :arg filepath: str. Name of the pickle binary file to be loaded.

    :kwarg filedir: str. Optional path to the directory which pickle file is stored in. Default is pwd.

    :returns: obj. The Python object stored in the specified pickle file.
    """
    obj = None
    fdir = copy.deepcopy(filedir) if isinstance(filedir, str) else ''
    if fdir and not fdir.endswith('/'):
        fdir = fdir + '/' 
    if os.path.isfile(fdir+filepath):
        with open(fdir+filepath, 'rb') as bf:
            obj = pickle.load(bf)
    return obj


def delete_fields_from_dict(datadict, fields=None):
    """
    Automates the deletion of specified fields in a Python
    dictionary object.

    :arg datadict: dict. Dictonary object from which fields are to be deleted.

    :kwarg fields: list. List of fields that will be deleted from the dictionary, provided they exist.

    :returns: dict. Identical dictionary object as input object except with specified fields removed.
    """
    obj = None
    keys = []
    if isinstance(datadict, dict):
        obj = datadict
    if isinstance(fields, (list, tuple)):
        keys.extend(fields)
    elif isinstance(fields, str):
        keys.append([fields])

    if obj is not None and obj and len(keys) > 0:
        for item in keys:
            if item in obj:
                del obj[item]

    return obj


def recursive_getattr(obj,attr,*args):
    """
    Applies getattr function recursively if given a nested attribute string.

    :returns: obj. Value of requested nested attribute.
    """
    def _getattr(obj,attr):
        return getattr(obj,attr,*args)
    return functools.reduce(_getattr,[obj]+attr.split('.'))


def recursive_setattr(obj,attr,val):
    """
    Applies setattr function recursively if given a nested attribute string.

    :returns: None.
    """
    (pre,dummy,post) = attr.rpartition('.')
    return setattr(recursive_getattr(obj,pre) if pre else obj,post,val)


def flatten_dict(obj, delimiter="_"):
    """
    Flatten a nested dictionary object.

    :returns: dict.
    """
    if not isinstance(obj, dict):
        raise TypeError("Cannot flatten a non-dictionary object. Aborting.")
    delim = delimiter if isinstance(delimiter, str) and len(delimiter) == 1 else "_"
    new_obj = dict()
    for key in obj:
        if isinstance(obj[key], dict):
            temp = flatten_dict(obj[key], delimiter=delimiter)
            for lkey in temp:
                new_obj[key+delim+lkey] = copy.deepcopy(temp[lkey])
        else:
            new_obj[key] = copy.deepcopy(obj[key])
    return new_obj

def find_flattened_key(obj, key, default=None, delimiter="_"):
    """
    Search for flattened key within a nested dictionary object.

    :returns: obj.
    """
    if not isinstance(obj, dict):
        raise TypeError("Cannot search a non-dictionary object. Aborting.")
    if not isinstance(key, str):
        raise TypeError("Cannot find a non-string key. Aborting.")
    delim = delimiter if isinstance(delimiter, str) and len(delimiter) == 1 else "_"
    skey = key.strip().split(delim)
    target_key = skey[0]
    result = default
    if target_key in obj:
        if len(skey) == 1:
            result = obj[target_key]
        elif isinstance(obj[target_key], dict):
            new_key = delim.join(skey[1:])
            result = find_flattened_key(obj[target_key], new_key, delimiter=delimiter)
    return result

def insert_flattened_key(obj, key, val, delimiter="_", parents=False):
    """
    Inserts flattened key into a nested dictionary object, option to create parent dictionaries.

    :returns: dict.
    """
    if not isinstance(obj, dict):
        raise TypeError("Cannot insert into a non-dictionary object. Aborting.")
    if not isinstance(key, str):
        raise TypeError("Cannot create a non-string key. Aborting.")
    delim = delimiter if isinstance(delimiter, str) and len(delimiter) == 1 else "_"
    skey = key.strip().split(delim)
    target_key = skey[0]
    if len(skey) == 1:
        obj[target_key] = copy.deepcopy(val)
    elif target_key in obj and isinstance(obj[target_key], dict):
        new_key = delim.join(skey[1:])
        obj[target_key] = insert_flattened_key(obj[target_key], new_key, val, delimiter=delimiter, parents=parents)
    elif parents:
        new_key = delim.join(skey[1:])
        obj[target_key] = {}
        obj[target_key] = insert_flattened_key(obj[target_key], new_key, val, delimiter=delimiter, parents=parents)
    else:
        raise KeyError("Cannot insert %s into dictionary as it does not traverse an existing path in the nested structure. Aborting." % (key))
    return obj

def insert_key(one, two, key):
    for rkey in two:
        if rkey == key:
            one[key] = two[key]
        elif rkey in one and isinstance(one[rkey], dict):
            one[rkey] = insert_key(one[rkey], two[rkey], key)
    return one

def update_key(one, two, key):
    for rkey in two:
        if rkey == key:
            if key in one:
                one[key].update(two[key])
            else:
                one[key] = two[key]
        elif isinstance(one, dict) and rkey in one:
            one[rkey] = update_key(one[rkey], two[rkey], key)
    return one

def extract_key(one, key):
    two = {}
    if key in one:
        two[key] = one[key]
        del one[key]
    for rkey in one:
        if isinstance(one[rkey], dict):
            rtwo = extract_key(one[rkey], key)
            if rtwo:
                two[rkey] = rtwo
    return two

def recursive_dict_print(one, nlayer=1):
    if not isinstance(one, dict):
        raise TypeError("Cannot apply %s to a non-dictionary object. Aborting." % (recursive_dict_print.__name__))
    nn = int(nlayer) if isinstance(nlayer, int) and int(nlayer) > 0 else 1
    for key, val in one.items():
        keystr = ""
        for ii in range(nn-1):
            keystr += "    "
        keystr += key
        print(keystr)
        if isinstance(val, dict):
            recursive_dict_print(val, nlayer=nn+1)

def recursive_dict_append(one, two, nfirst=1, verbose=0):
    """
    Recursively appends fields from a dictionary object into another dictionary object.

    :returns: dict.
    """
    if not isinstance(one, dict):
        raise TypeError("Cannot apply %s to a non-dictionary object. Aborting." % (recursive_dict_append.__name__))
    if not isinstance(two, dict):
        raise TypeError("Cannot apply %s to a non-dictionary object. Aborting." % (recursive_dict_append.__name__))
    nn = int(nfirst) if isinstance(nfirst, int) and int(nfirst) > 0 else 1
    out = copy.deepcopy(one)
    keylist = []
    keylist.extend([key for key in one if key not in keylist])
    keylist.extend([key for key in two if key not in keylist])
    for key in keylist:
        if key in one and key in two:
            if one[key] is not None and isinstance(two[key], type(one[key])):
                if isinstance(one[key], dict):
                    out[key] = recursive_dict_append(one[key], two[key], nfirst=nn, verbose=verbose-1)
                elif isinstance(one[key], str):
                    out[key] = np.append(np.array([one[key]]), np.array([two[key]]), axis=0).flatten()
                elif isinstance(one[key], number_types):
                    out[key] = np.stack((np.array([one[key]]), np.array([two[key]])), axis=0)
                elif isinstance(one[key], array_types):
                    if isinstance(one[key][0], str):
                        out[key] = np.stack((np.array(one[key]), np.array(two[key])), axis=0)
                    elif isinstance(one[key][0], number_types):
                        out[key] = np.stack((np.array(one[key]), np.array(two[key])), axis=0)
                    elif isinstance(one[key][0], array_types):
                        tempone = np.array(one[key])
                        if tempone.shape[0] == nn:
                            out[key] = np.append(tempone, np.array([two[key]]), axis=0)
                        else:
                            out[key] = np.stack((tempone, np.array(two[key])), axis=0)
                    elif verbose > 0:
                        print("%s field not transferred due to unsupported type." % (key))
                elif verbose > 0:
                    print("%s field not transferred due to unsupported type." % (key))
            elif isinstance(one[key], array_types) and two[key] is not None:
                if isinstance(one[key][0], str) and isinstance(two[key], str):
                    out[key] = np.append(np.array(one[key]), np.array([two[key]]), axis=0).flatten()
                elif isinstance(one[key][0], number_types) and isinstance(two[key], number_types):
                    out[key] = np.append(np.array(one[key]), np.array([two[key]]), axis=0)
                elif isinstance(one[key][0], array_types):
                    if isinstance(two[key], str):
                        out[key] = np.append(np.array(one[key]), np.array([two[key]]), axis=0).flatten()
                    if isinstance(two[key], number_types):
                        out[key] = np.append(np.array(one[key]), np.atleast_2d([two[key]]), axis=0)
                    elif isinstance(two[key], array_types):
                        out[key] = np.append(np.array(one[key]), np.array([two[key]]), axis=0)
                    elif verbose > 0:
                        print("%s field not transferred due to type mismatch or unsupported type." % (key))
                elif verbose > 0:
                    print("%s field not transferred due to type mismatch or unsupported type." % (key))
            elif one[key] is not None and two[key] is None:
                if isinstance(one[key], dict):
                    twoval = {}
                    for tkey in one[key]:
                        twoval[tkey] = None
                    out[key] = recursive_dict_append(one[key], twoval, nfirst=nn, verbose=verbose-1)
                elif isinstance(one[key], str):
                    twoval = np.array([""])
                    out[key] = np.append(np.array([one[key]]), twoval, axis=0).flatten()
                elif isinstance(one[key], number_types):
                    twoval = np.atleast_2d([np.NaN])
                    out[key] = np.append(np.atleast_2d([one[key]]), twoval, axis=0)
                elif isinstance(one[key], array_types) and len(one[key]) > 0:
                    tempone = np.array(one[key])
                    if isinstance(one[key][0], str):
                        twoval = np.array([""])
                        if tempone.shape[0] != nn:
                            tempone = np.array([one[key]])
                            twoval = np.full(tempone.shape, "")
                        out[key] = np.append(tempone, twoval, axis=0)
                    elif isinstance(one[key][0], number_types):
                        twoval = np.array([np.NaN])
                        if tempone.shape[0] != nn:
                            tempone = np.array([one[key]])
                            twoval = np.full(tempone.shape, np.NaN)
                        out[key] = np.append(tempone, twoval, axis=0)
                    elif isinstance(one[key][0], array_types):
                        temptwo = np.array([one[key][0]])
                        twoval = np.full(temptwo.shape, np.NaN)
                        if tempone.shape[0] != nn:
                            tempone = np.array([one[key]])
                            twoval = np.full(tempone.shape, np.NaN)
                        out[key] = np.append(tempone, twoval, axis=0)
                    elif verbose > 0:
                        print("%s field not transferred due to unsupported type." % (key))
                elif verbose > 0:
                    print("%s field not transferred due to unsupported type." % (key))
            elif one[key] is None and two[key] is not None:
                if isinstance(two[key], dict):
                    oneval = {}
                    for tkey in two[key]:
                        oneval[tkey] = None
                    out[key] = recursive_dict_append(oneval, two[key], nfirst=nn, verbose=verbose-1)
                elif isinstance(two[key], str):
                    oneval = np.full((nn, ), "")
                    out[key] = np.append(oneval, np.array([two[key]]), axis=0).flatten()
                elif isinstance(two[key], number_types):
                    oneval = np.full((nn, 1), np.NaN)
                    out[key] = np.append(oneval, np.atleast_2d([two[key]]), axis=0)
                elif isinstance(two[key], array_types) and len(two[key]) > 0:
                    temptwo = np.array([two[key]])
                    if isinstance(two[key][0], str):
                        oneval = np.array([np.full(temptwo[0].shape, "")])
                        for ii in range(nn-1):
                            oneval = np.append(oneval, np.array([np.full(temptwo[0].shape, "")]), axis=0)
                        out[key] = np.append(oneval, temptwo, axis=0)
                    elif isinstance(two[key][0], (number_types, array_types)):
                        oneval = np.array([np.full(temptwo[0].shape, "")])
                        for ii in range(nn-1):
                            oneval = np.append(oneval, np.array([np.full(temptwo[0].shape, "")]), axis=0)
                        out[key] = np.append(oneval, temptwo, axis=0)
                    elif verbose > 0:
                        print("%s field not transferred due to unsupported type." % (key))
                elif verbose > 0:
                    print("%s field not transferred due to unsupported type." % (key))
            elif one[key] is None and two[key] is None and verbose > 0:
                print("%s field not transferred since both entries are NoneType." % (key))
            elif verbose > 0:
                print("%s field not transferred due to type mismatch." % (key))
        elif key in one:
            if verbose > 1:
                print("%s field not present in second dictionary." % (key))
            if isinstance(one[key], dict):
                twoval = {}
                for tkey in one[key]:
                    twoval[tkey] = None
                out[key] = recursive_dict_append(one[key], twoval, nfirst=nn, verbose=verbose-1)
            elif isinstance(one[key], str):
                twoval = np.array([""])
                out[key] = np.append(np.array([one[key]]), twoval, axis=0).flatten()
            elif isinstance(one[key], number_types):
                twoval = np.atleast_2d([np.NaN])
                out[key] = np.append(np.atleast_2d([one[key]]), twoval, axis=0)
            elif isinstance(one[key], array_types) and len(one[key]) > 0:
                tempone = np.array(one[key])
                if isinstance(one[key][0], str):
                    twoval = np.array([""])
                    if tempone.shape[0] != nn:
                        tempone = np.array([one[key]])
                        twoval = np.full(tempone.shape, "")
                    out[key] = np.append(tempone, twoval, axis=0)
                elif isinstance(one[key][0], number_types):
                    twoval = np.array([np.NaN])
                    if tempone.shape[0] != nn:
                        tempone = np.array([one[key]])
                        twoval = np.full(tempone.shape, np.NaN)
                    out[key] = np.append(tempone, twoval, axis=0)
                elif isinstance(one[key][0], array_types):
                    temptwo = np.array([one[key][0]])
                    twoval = np.full(temptwo.shape, np.NaN)
                    if tempone.shape[0] != nn:
                        tempone = np.array([one[key]])
                        twoval = np.full(tempone.shape, np.NaN)
                    out[key] = np.append(tempone, twoval, axis=0)
                elif verbose > 0:
                    print("%s field not transferred due to unsupported type." % (key))
            elif verbose > 0:
                print("%s field not transferred due to unsupported type." % (key))
        elif key in two:
            if verbose > 1:
                print("%s field not present in first dictionary." % (key))
            if isinstance(two[key], dict):
                oneval = {}
                for tkey in two[key]:
                    oneval[tkey] = None
                out[key] = recursive_dict_append(oneval, two[key], nfirst=nn, verbose=verbose-1)
            elif isinstance(two[key], str):
                oneval = np.full((nn, ), "")
                out[key] = np.append(oneval, np.array([two[key]]), axis=0).flatten()
            elif isinstance(two[key], number_types):
                oneval = np.full((nn, 1), np.NaN)
                out[key] = np.append(oneval, np.atleast_2d([two[key]]), axis=0)
            elif isinstance(two[key], array_types) and len(two[key]) > 0:
                temptwo = np.array([two[key]])
                if isinstance(two[key][0], str):
                    oneval = np.array([np.full(temptwo[0].shape, "")])
                    for ii in range(nn-1):
                        oneval = np.append(oneval, np.array([np.full(temptwo[0].shape, "")]), axis=0)
                    out[key] = np.append(oneval, temptwo, axis=0)
                elif isinstance(two[key][0], (number_types, array_types)):
                    oneval = np.array([np.full(temptwo[0].shape, np.NaN)])
                    for ii in range(nn-1):
                        oneval = np.append(oneval, np.array([np.full(temptwo[0].shape, "")]), axis=0)
                    out[key] = np.append(oneval, temptwo, axis=0)
                elif verbose > 0:
                    print("%s field not transferred due to unsupported type." % (key))
            elif verbose > 0:
                print("%s field not transferred due to unsupported type." % (key))
    return out


def moving_average(data, navg=5):
    """
    Calculates moving average of input 1D vector.

    :arg data: array. Data vector over which moving average is to be applied.

    :kwarg navg: int. Number of points in averaging window.

    :returns: array. Averaged data vector with identical shape as input vector.
    """
    dvec = None
    nn = 5
    if isinstance(data, (list, tuple)):
        dvec = np.array(data)
    elif isinstance(data, np.ndarray):
        dvec = data.flatten()
    if isinstance(navg, number_types):
        nn = int(navg)

    mavg = None
    if dvec is not None and dvec.size > 0:
        mavg = np.zeros((dvec.size+nn-1,))
        grid = np.full((nn,), dvec[0])
        for ii in np.arange(0, mavg.size):
             grid[:-1] = grid[1:]
             newval = dvec[ii] if ii < dvec.size else 0.0
             grid[-1] = newval
             mavg[ii] = np.average(grid)
    return mavg


def create_range_mask(values, ranges, equality=True):
    """
    """
    vvec = None
    rlist = None
    if isinstance(values, (list, tuple)):
        vvec = np.array(values)
    elif isinstance(values, np.ndarray):
        vvec = values.flatten()
    if isinstance(ranges, (list, tuple)) and len(ranges) > 0:
        rlist = copy.deepcopy(ranges)

    rmask = None
    if vvec is not None and rlist is not None and len(vvec) > 0:
        ffilt = np.isfinite(vvec)
        imask = np.invert(ffilt)
        for ii in np.arange(0, len(rlist)):
            tmask = np.all([vvec[ffilt] >= rlist[ii][0], vvec[ffilt] <= rlist[ii][1]], axis=0) if equality else np.all([vvec[ffilt] > rlist[ii][0], vvec[ffilt] < rlist[ii][1]], axis=0)
            imask[ffilt] = np.any([imask[ffilt], tmask], axis=0)
        rmask = np.invert(imask)

    return rmask


def non_finite_filler(yvalues, fill_value=0.0, xvalues=None, debug=False):
    """
    Replaces all NaNs and infs within a vector with nearby values, to prevent errors in
    numerical integration schemes.
    
    :arg yvalues: array. Vector of y-values from which NaNs and infs should be replaced.

    :kwarg fill_value: float or str. If float, replaces all NaNs and infs with this value. If str, only accepts next, previous, or nearest.

    :kwarg xvalues: array. Vector of x-values for reference by relational replacement methods.

    :kwarg debug: bool. Writes to standard output if replacement method was chosen without providing x-value vector.

    :returns: (array, array). Correspondingly ordered and filled y-values, correspondingly ordered x-values
    """
    xvec = None
    yvec = None
    fval = 0.0
    ftype = 0
    if isinstance(yvalues, (list, tuple)):
        yvec = np.array(yvalues).flatten()
    elif isinstance(yvalues, np.ndarray):
        yvec = yvalues.flatten()
    if isinstance(fill_value, number_types):
        fval = float(fill_value)
    elif isinstance(fill_value, str):
        if re.match('next', str(fill_value), flags=re.IGNORECASE):
            ftype = 1
        elif re.match('previous', str(fill_value), flags=re.IGNORECASE):
            ftype = 2
        elif re.match('nearest', str(fill_value), flags=re.IGNORECASE):
            ftype = 3
    if isinstance(xvalues, (list, tuple)):
        xvec = np.array(xvalues).flatten()
    elif isinstance(xvalues, np.ndarray):
        xvec = xvalues.flatten()

    if xvec is not None and yvec is not None and xvec.shape != yvec.shape:
        xvec = None
    if xvec is None:
        if debug:
            print("   Assumed ordered vector was provided in non_finite_filler.")
        if ftype == 3:
            ftype = 1
            if debug:
                print("   Nearest option not possible without corresponding xvalues in non_finite_filler, switching to next option.")

    if yvec is not None:
        nfilt = np.any([np.isnan(yvec), np.invert(np.isfinite(yvec))], axis=0)
        nidxs = np.where(nfilt)[0]
        for ii in nidxs:
            if ftype == 0:
               yvec[ii] = fval
            elif xvec is not None:
                nxs = xvec[np.invert(nfilt)]
                nys = yvec[np.invert(nfilt)]
                mindiff = None
                idx = 0
                if ftype == 1:
                    for jj in np.arange(0, nxs.size):
                        diff = nxs[jj] - xvec[ii]
                        if diff >= 0.0 and (mindiff is None or diff < mindiff):
                            mindiff = diff
                            idx = jj
                    yvec[ii] = yvec[idx] if mindiff is not None else fval
                if ftype == 2:
                    for jj in np.arange(0, nxs.size):
                        diff = nxs[jj] - xvec[ii]
                        if diff <= 0.0 and (mindiff is None or diff > mindiff):
                            mindiff = diff
                            idx = jj
                    yvec[ii] = yvec[idx] if mindiff is not None else fval
                if ftype == 3:
                    for jj in np.arange(0, nxs.size):
                        diff = np.abs(nxs[jj] - xvec[ii])
                        if mindiff is None or diff < mindiff:
                            mindiff = diff
                            idx = jj
                    yvec[ii] = yvec[idx] if mindiff is not None else fval
            else:
                if ftype == 1:
                    jj = 0
                    while jj < (yvec.size - ii) and np.any([np.isnan(yvec[ii+jj]), np.invert(np.isfinite(yvec[ii+jj]))], axis=0):
                        jj = jj + 1
                    yvec[ii] = yvec[ii+jj] if jj < (yvec.size - ii) else fval
                if ftype == 2:
                    jj = 0
                    while jj <= ii and np.any([np.isnan(yvec[ii-jj]), np.invert(np.isfinite(yvec[ii-jj]))], axis=0):
                        jj = jj + 1
                    yvec[ii] = yvec[ii-jj] if jj < (yvec.size - ii) else fval
                if ftype == 3:
                    jja = 0
                    while jja < (yvec.size - ii) and np.any([np.isnan(yvec[ii+jja]), np.invert(np.isfinite(yvec[ii+jja]))], axis=0):
                        jja = jja + 1
                    jjb = 0
                    while jjb <= ii and np.any([np.isnan(yvec[ii-jjb]), np.invert(np.isfinite(yvec[ii-jjb]))], axis=0):
                        jjb = jjb + 1
                    jj = None
                    if jja >= (yvec.size - ii) and jjb <= ii:
                        jj = -jjb
                    elif jjb > ii and jja < (yvec.size - ii):
                        jj = jja
                    elif jja < (yvec.size - ii) and jjb <= ii:
                        jj = jja if jja <= jjb else -jjb
                    yvec[ii] = yvec[ii+jj] if jj is not None else fval

    return (yvec, xvec)


def anchored_polyfit(degree, xdata, ydata, xanchors=None, yanchors=None, omit_const=False, omit_linear=False, output_lm=False):
    """
    Performs least-squares polynomial fitting with specified polynomial degree and specified
    anchors / constrained points. The anchors are applied via the Lagrange multiplier method.
    If anchors are not required, this function operates the same as numpy.polyfit.

    :arg degree: int. Degree of polynomial, N, to be used in the fit.

    :arg xdata: array. Vector of x-values to be fit.

    :arg ydata: array. Vector of y-values to be fit.

    :kwarg xanchors: array. Optional vector of x-values for anchor points. Must match size of yanchors.

    :kwarg yanchors: array. Optional vector of y-values for anchor points. Must match size of xanchors.

    :kwarg omit_const: bool. Flag to remove zeroth-order component of polynomial from fitting routine.

    :kwarg omit_linear: bool. Flag to remove first-order component of polynomial from fitting routine.

    :kwarg output_lm: bool. Flag to include Lagrange multipliers for anchor points in output vectors.

    :returns: array. Coefficients of polynomial from lowest order to highest order, Lagrange multipliers follow coefficients if specified.
    """
    deg = 1
    xx = None
    yy = None
    xc = np.array([])
    yc = np.array([])
    if isinstance(degree, number_types) and int(degree) > 0:
        deg = int(degree)
    if isinstance(xdata, (list, tuple)) and len(xdata) > 0:
        xx = np.array(xdata).flatten()
    elif isinstance(xdata, np.ndarray) and xdata.size > 0:
        xx = xdata.flatten()
    if isinstance(ydata, (list, tuple)) and len(ydata) > 0:
        yy = np.array(ydata).flatten()
    elif isinstance(ydata, np.ndarray) and ydata.size > 0:
        yy = ydata.flatten()
    if isinstance(xanchors, (list, tuple)) and len(xanchors) > 0:
        xc = np.array(xanchors).flatten()
    elif isinstance(xanchors, np.ndarray) and xanchors.size > 0:
        xc = xanchors.flatten()
    if isinstance(yanchors, (list, tuple)) and len(yanchors) > 0:
        yc = np.array(yanchors).flatten()
    elif isinstance(yanchors, np.ndarray) and yanchors.size > 0:
        yc = yanchors.flatten()

    params = None
    if xx is not None and yy is not None:
        spow = 0
        AA = None
        CC = None
        for ii in np.arange(spow, deg+1):
            fadd = True
            if ii == 0 and omit_const:
                fadd = False
            if ii == 1 and omit_linear:
                fadd = False
            if fadd:
                AA = np.hstack((AA, np.atleast_2d(np.power(xx, ii)).T)) if AA is not None else np.atleast_2d(np.power(xx, ii)).T
                if xc.size > 0:
                    CC = np.hstack((CC, np.atleast_2d(np.power(xc, ii)).T)) if CC is not None else np.atleast_2d(np.power(xc, ii)).T
        MM = 2.0 * np.dot(AA.T, AA)
        if CC is not None:
            MM = np.vstack((MM, CC))
            MM = np.hstack((MM, np.vstack((CC.T, np.zeros((xc.size, xc.size))))))
        BB = np.vstack((2.0 * np.dot(AA.T, np.atleast_2d(yy).T), np.atleast_2d(yc).T))
        params = np.linalg.solve(MM, BB)
        if omit_const:
            params = np.insert(params, 0, 0.0)
        if omit_linear:
            params = np.insert(params, 1, 0.0)
        if CC is not None and not output_lm:
            params = params[:-xc.size]

    return params


def finite_filter_2d(data, indices=None, operation='and', filter_axis=1, finvert=False):
    """
    Finite-value filter accepting a two-dimensional data
    array and removing NaN and infinity values from it. It
    is possible to specify which axis and which indices on
    that axis the filter will be constructed with, but the
    resulting filter will be applied to the entire array.

    :arg data: array. The data which the filter will be constructed from and applied to.

    :kwarg indices: array. Vector of indices specifying the data which the filter will be constructed from.

    :kwarg operation: str. Specification of the operation to be used to combine multiple indices, Default is or.

    :kwarg filter_axis: int. Axis of array which the filter will be constructed from, applied to other axis uniformly.

    :kwarg finvert: bool. Flag to invert the generated filter before applying to the data.

    :returns: array. Identical to input data array except with non-finite values and associated data remvoed.
    """
    odata = None
    idxs = []
    op = 'and'
    ftranspose = False
    if isinstance(data, array_types):
        odata = np.atleast_2d(copy.deepcopy(data))
    if isinstance(indices, (list, tuple)):
        for ii in np.arange(0, len(indices)):
            idxs.append(indices[ii])
    elif isinstance(indices, np.ndarray):
        for ii in np.arange(0, indices.size):
            idxs.append(indices[ii])
    else:
        idxs = None
    if isinstance(operation, str) and operation.lower() in ['and', 'or']:
        op = operation.lower()
    if isinstance(filter_axis, number_types) and int(filter_axis) == 0:
        ftranspose = True

    fdata = None
    if odata is not None and odata.size > 0:
        if ftranspose:
            odata = odata.T
        if idxs is None:
            idxs = list(range(odata.shape[0]))
        nffilt = None
        if op == 'and':
            nffilt = np.full((odata.shape[1],), True)
            for idx in idxs:
                if idx >= 0 and idx < odata.shape[0]:
                    nffilt = np.logical_and(nffilt, np.isfinite(odata[idx, :]))
        elif op == 'or':
            nffilt = np.full((odata.shape[1],), False)
            for idx in idxs:
                if idx >= 0 and idx < odata.shape[0]:
                    nffilt = np.logical_or(nffilt, np.isfinite(odata[idx, :]))

        if nffilt is not None and finvert:
            nffilt = np.invert(nffilt)
        if nffilt is not None and np.any(nffilt):
            for ii in np.arange(0, odata.shape[0]):
                fdata = odata[ii, :][nffilt] if fdata is None else np.vstack((fdata, odata[ii, :][nffilt]))
        else:
            fdata = np.array([])
        if ftranspose:
            fdata = fdata.T

    return fdata


def bounded_filter_2d(data, lower_bound=None, upper_bound=None, lower_equality=True, upper_equality=True, indices=None, filter_axis=1, finvert=False):
    """
    Bounded value filter accepting a two-dimensional data
    array and removing values above and / or below the
    specified values from it. It is possible to specify
    which axis and which indices on that axis the filter
    will be constructed with, but the resulting filter
    will be applied to the entire array.

    :arg data: array. The data which the filter will be constructed from and applied to.

    :kwarg lower_bound: float. Lower bounding value, data values below this number will be removed.

    :kwarg upper_bound: float. Upper bounding value, data values above this number will be removed.

    :kwarg lower_equality: bool. Flag indicating that values equal to the lower bound should be kept.

    :kwarg upper_equality: bool. Flag indicating that values equal to the upper bound should be kept.

    :kwarg indices: array. Vector of indices specifying the data which the filter will be constructed from.

    :kwarg filter_axis: int. Axis of array which the filter will be constructed from, applied to other axis uniformly.

    :kwarg finvert: bool. Flag to invert the generated filter before applying to the data.

    :returns: array. Identical to input data array except with out-of-bounds values and associated data remvoed.
    """
    odata = None
    lb = None
    ub = None
    idxs = []
    ftranspose = False
    if isinstance(data, array_types):
        odata = np.atleast_2d(copy.deepcopy(data))
    if isinstance(lower_bound, number_types):
        lb = float(lower_bound)
    if isinstance(upper_bound, number_types):
        if lb is not None and float(upper_bound) < lb:
            ub = lb
            lb = float(upper_bound)
        else:
            ub = float(upper_bound)
    if isinstance(indices, (list, tuple)):
        for ii in np.arange(0, len(indices)):
            idxs.append(indices[ii])
    elif isinstance(indices, np.ndarray):
        for ii in np.arange(0, indices.size):
            idxs.append(indices[ii])
    else:
        idxs = None
    if isinstance(filter_axis, (int, float)) and int(filter_axis) == 0:
        ftranspose = True

    fdata = None
    if odata is not None and odata.size > 0:
        if ftranspose:
            odata = odata.T
        if idxs is None:
            idxs = list(range(odata.shape[0]))
        bandfilt = None
        lbfilt = np.full((odata.shape[1],), True)
        ubfilt = np.full((odata.shape[1],), True)
        for idx in idxs:
            if idx >= 0 and idx < odata.shape[0]:
                if lb is not None:
                    if lower_equality:
                        lbfilt = np.logical_and(lbfilt, odata[idx, :] >= lb)
                    else:
                        lbfilt = np.logical_and(lbfilt, odata[idx, :] > lb)
                if ub is not None:
                    if upper_equality:
                        ubfilt = np.logical_and(ubfilt, odata[idx, :] <= ub)
                    else:
                        ubfilt = np.logical_and(ubfilt, odata[idx, :] < ub)
            bandfilt = np.logical_and(lbfilt, ubfilt)

        if bandfilt is not None and finvert:
            bandfilt = np.invert(bandfilt)
        if bandfilt is not None and np.any(bandfilt):
            for ii in np.arange(0, odata.shape[0]):
                fdata = odata[ii, :][bandfilt] if fdata is None else np.vstack((fdata, odata[ii, :][bandfilt]))
        else:
            fdata = np.array([])
        if ftranspose:
            fdata = fdata.T

    return fdata


def custom_filter_2d(data, filters=None, operation='and', filter_axis=1, finvert=False):
    """
    Customized filter accepting a two-dimensional data
    array and removing user-specified values from it. It
    is possible to specify which axis the user-defined
    filters wer constructed with, and the combined
    result will be applied to the entire array.

    :arg data: array. The data which the filter will be constructed from and applied to.

    :kwarg filters: list. List of boolean arrays corresponding to pre-calculated user-defined filters.

    :kwarg operation: str. Specification of the operation to be used to combine multiple indices, Default is or.

    :kwarg filter_axis: int. Axis of array which the filter will be constructed from, applied to other axis uniformly.

    :kwarg finvert: bool. Flag to invert the generated filter before applying to the data.

    :returns: array. Identical to input data array except with user-defined values and associated data remvoed.
    """
    odata = None
    ffs = []
    op = 'and'
    ftranspose = False
    if isinstance(data, array_types):
        odata = np.atleast_2d(copy.deepcopy(data))
    if isinstance(filters, (list, tuple)):
        for ii in np.arange(0, len(filters)):
            ffs.append(filters[ii])
    if isinstance(operation, str) and operation.lower() in ['and', 'or']:
        op = operation.lower()
    if isinstance(filter_axis, number_types) and int(filter_axis) == 0:
        ftranspose = True

    fdata = None
    if odata is not None and odata.size > 0 and len(ffs) > 0:
        if ftranspose:
            odata = odata.T
        userfilt = None
        if op == 'and':
            userfilt = np.full((odata.shape[1],), True)
            for ff in ffs:
                filt = ff
                if isinstance(ff, (list, tuple)):
                    filt = np.array(ff).flatten()
                if filt.size == odata.shape[1]:
                    userfilt = np.logical_and(userfilt, filt)
        elif op == 'or':
            userfilt = np.full((odata.shape[1],), False)
            for ff in ffs:
                filt = ff
                if isinstance(ff, (list, tuple)):
                    filt = np.array(ff).flatten()
                if filt.size == odata.shape[1]:
                    userfilt = np.logical_or(userfilt, filt)

        if userfilt is not None and finvert:
            userfilt = np.invert(userfilt)
        if userfilt is not None and np.any(userfilt):
            for ii in np.arange(0, odata.shape[0]):
                fdata = odata[ii, :][userfilt] if fdata is None else np.vstack((fdata, odata[ii, :][userfilt]))
        else:
            fdata = np.array([])
        if ftranspose:
            fdata = fdata.T

    return fdata


def compute_total_variance(variances, values=None, use_n=True):
    """
    Calculates total variance from a given data set of values and error bars. Uses the Law of
    Total Variance, which accounts for the population variance (scatter) of the given values
    in addition to the input error bars.

    Note that the c4(n) correction is used to provide a better estimate of the population
    variance given a small finite set of data, since the sample variance usually underestimates
    the population variance for small samples. This term has no effective impact for n > 100.

    :arg variances: array. Vector of variances corresponding to input values.

    :kwarg values: array. Vector of values for calculation of population variance (optional but recommended)

    :kwarg use_n: bool. Flag to toggle reduction of input error bar contribution by square-root of N.

    :returns: (float, float, int). Total variance of the provided data, population variance of provided values, number of valid data points given.
    """
    varvec = None
    valvec = None
    if isinstance(variances, (list, tuple)) and len(variances) > 0:
        varvec = np.array(variances)
    elif isinstance(variances, np.ndarray) and variances.size > 0:
        varvec = variances.copy()
    if isinstance(values, (list, tuple)) and varvec is not None and len(values) == varvec.size:
        valvec = np.array(values)
    elif isinstance(values, np.ndarray) and varvec is not None and values.size == varvec.size:
        valvec = values.copy()
    else:
        valvec = np.zeros(varvec.shape)

    vartotal = np.NaN
    varpop = np.NaN
    n = 0
    if varvec is not None:
        n = valvec.size
        varpopfac = 0.1
        if n > 343:
            varpopfac = 1.0 - np.exp(1.0 - n / 45.0)   # Proxy continuation - SciPy function below gives NaNs for n > 343
        elif n > 1:
            varpopfac = (2.0 / (n - 1.0)) * np.power(gamma(n / 2) / gamma((n - 1) / 2), 2.0)   # c4(n) correction factor
        varpop = np.nanvar(valvec) / np.power(varpopfac, 2.0) if n > 1 else 0.0
        meanvar = np.nanmean(varvec) / float(n) if use_n else np.nanmean(varvec)
        vartotal = meanvar + varpop

    return (vartotal, varpop, n)


def calc_eb_error(values, value_errors, sigma=1.0, use_n=True):
    """
    Calculates average of N-D input matrix and square-root of its variance, given the error bars
    as symmetrical +/- values. Assumes that the average is to be done over axis 0 of the matrix.

    :arg values: array. Matrix of values to be averaged.

    :arg value_errors: array. Matrix of error bars corresponding to input values.

    :kwarg sigma: float. Specify nature of provided errors through distribution width, in units of sigma.

    :kwarg use_n: bool. Toggle scaling by number of data points in average input variance, does not affect population variance.

    :returns: (array, array, array). Averaged value(s), square-root value(s) of combined variance of the mean and mean of the errors, std. of the mean(s).
    """
    vals = None
    errs = None
    sig = 1.0
    nmin = 0
    if isinstance(values, (list, tuple)) and len(values) > 0:
        vals = np.array(values)
    elif isinstance(values, np.ndarray) and values.size > 0:
        vals = values.copy()
    if isinstance(value_errors, (list, tuple)) and len(value_errors) > 0:
        errs = np.array(value_errors)
    elif isinstance(value_errors, np.ndarray) and value_errors.size > 0:
        errs = value_errors.copy()
    elif value_errors is None and vals is not None:
        errs = vals.copy()
    if isinstance(sigma, number_types) and float(sigma) > 0.0:
        sig = float(sigma)

    avgdist = None
    stddist = None
    stdmean = None
    if vals is not None and errs is not None:
        if vals.shape != errs.shape:
            raise ValueError('Given data in eb error calculation do not have the same shape.')
        sc = np.nanmax(np.abs(vals))
        if not np.isfinite(sc) or sc == 0.0:
            sc = 1.0
        vals = vals / sc
        verrs = np.power(errs / sig / sc, 2.0)
        (vtotal, vpop, nvals) = compute_total_variance(verrs, vals, use_n=use_n)
        avgdist = np.nanmean(vals) * sc
        stddist = np.sqrt(vtotal) * sc
        stdmean = np.sqrt(vpop / nvals) * sc

    return (avgdist, stddist, stdmean)


def calc_hilo_error(values, lower_values, upper_values, sigma=1.0, use_n=True):
    """
    Calculates average of N-D input matrix and square-root of its variance, given the error bars
    as asymmetrical +/- values. Assumes that the average is to be done over axis 0 of the matrix.

    :arg values: array. Matrix of values to be averaged.

    :arg lower_values: array. Matrix of lower error bars corresponding to input values.

    :arg upper_values: array. Matrix of upper error bars corresponding to input values.

    :kwarg sigma: float. Specify nature of provided errors through distribution width, in units of sigma.

    :kwarg use_n: bool. Toggle scaling by number of data points in average input variance, does not affect population variance.

    :returns: (array, array, array). Averaged value(s), square-root value(s) of combined variance of the mean and mean of the errors, std. of the mean(s).
    """
    vals = None
    lvals = None
    uvals = None
    sig = 1.0
    if isinstance(values, (list, tuple)) and len(values) > 0:
        vals = np.array(values)
    elif isinstance(values, np.ndarray) and values.size > 0:
        vals = values.copy()
    if isinstance(lower_values, (list, tuple)) and len(lower_values) > 0:
        lvals = np.array(lower_values)
    elif isinstance(lower_values, np.ndarray) and lower_values.size > 0:
        lvals = lower_values.copy()
    elif lower_values is None and vals is not None:
        lvals = vals.copy()
    if isinstance(upper_values, (list, tuple)) and len(upper_values) > 0:
        uvals = np.array(upper_values)
    elif isinstance(upper_values, np.ndarray) and upper_values.size > 0:
        uvals = upper_values.copy()
    elif upper_values is None and vals is not None:
        uvals = vals.copy()
    if isinstance(sigma, number_types) and float(sigma) > 0.0:
        sig = float(sigma)

    avgdist = None
    stddist = None
    stdmean = None
    if vals is not None and lvals is not None and uvals is not None:
        if vals.shape != lvals.shape or vals.shape != uvals.shape:
            raise ValueError('Given data in hi-lo error calculation do not have the same shape.')
        sc = np.nanmax(np.abs(vals))
        if not np.isfinite(sc) or sc == 0.0:
            sc = 1.0
        lerr = np.abs(vals - lvals)
        uerr = np.abs(uvals - vals)
        vals = vals / sc
        errs = np.nanmax(np.stack((lerr, uerr), axis=0), axis=0)
        verrs = np.power(errs / sig / sc, 2.0)
        (vtotal, vpop, nvals) = compute_total_variance(verrs, vals, use_n=use_n)
        avgdist = np.nanmean(vals) * sc
        stddist = np.sqrt(vtotal) * sc
        stdmean = np.sqrt(vpop / nvals) * sc

    return (avgdist, stddist, stdmean)


def filtered_average(values, value_errors=None, lower_values=None, upper_values=None, percent_errors=None, sigma=1.0, lower_bounds=None, upper_bounds=None, mask_ranges=None, use_n=True):
    """
    Calculates average of input array and square-root of its variance, depending on the error bar arguments used
    in the call. Assumes that the average is to be done over axis 0 of the array. Essentially an abstracted wrapper
    for functions calc_eb_error or calc_hilo_error for 1- and 2-D, so as to avoid mis-specification errors in
    programming. Optionally applies broad acceptance filters based on upper and lower value bounds.

    :arg values: array. N-D array of values to be averaged.

    :kwarg value_errors: array. N-D array of error bars corresponding to input values. Specify for symmetric functions.

    :kwarg lower_values: array. N-D array of lower error bars corresponding to input values. Specify for asymmetric functions.

    :kwarg upper_values: array. N-D array of upper error bars corresponding to input values. Specify for asymmetric functions.

    :kwarg percent_errors: array. N-D array of percentage errors corresponding to input values. Specify for symmetric functions.

    :kwarg sigma: float. Specify nature of provided errors through distribution width, sigma.

    :kwarg lower_bounds: array. Lower bound of value in order to be included in averaging. Can contain a single value or array of values.

    :kwarg upper_bounds: array. Upper bound of value in order to be included in averaging. Can contain a single value or array of values.

    :kwarg mask_ranges: list. Optional list of tuples containing start and end values for ranges to exclude in averaging process.

    :kwarg use_n: bool. Flag to toggle reduction of input error bar contribution by square-root of N.

    :returns: (array, array, array, array). Averaged value(s), square-root value(s) of combined variance of the mean and errors, std. of the mean(s), number of points in average.
    """
    vals = None
    errs = None
    lvals = None
    uvals = None
    perrs = None
    sig = 1.0
    sc = 1.0
    lbnds = None
    ubnds = None

    if isinstance(values, (list, tuple)) and len(values) > 0:
        vals = np.array(values)
    elif isinstance(values, np.ndarray) and values.size > 0:
        vals = values.copy()
    if isinstance(value_errors, (list, tuple)) and len(value_errors) > 0:
        errs = np.array(value_errors)
    elif isinstance(value_errors, np.ndarray) and value_errors.size > 0:
        errs = value_errors.copy()
    if errs is None:
        if isinstance(lower_values, (list, tuple)) and len(lower_values) > 0:
            lvals = np.array(lower_values)
        elif isinstance(lower_values, np.ndarray) and lower_values.size > 0:
            lvals = lower_values.copy()
        if isinstance(upper_values, (list, tuple)) and len(upper_values) > 0:
            uvals = np.array(upper_values)
        elif isinstance(upper_values, np.ndarray) and upper_values.size > 0:
            uvals = upper_values.copy()
    if errs is None and (lvals is None or uvals is None):
        if isinstance(percent_errors, (list, tuple)) and len(percent_errors) > 0:
            perrs = np.array(percent_errors)
        elif isinstance(percent_errors, np.ndarray) and percent_errors.size > 0:
            perrs = percent_errors.copy()
        elif isinstance(percent_errors, (float, int)) and float(percent_errors) > 0.0 and vals is not None:
            perrs = np.full(vals.shape, float(percent_errors))
    if isinstance(sigma, number_types) and float(sigma) > 0.0:
        sig = float(sigma)
    if isinstance(lower_bounds, number_types):
        lbnds = np.array([lower_bounds])
    elif isinstance(lower_bounds, (list, tuple)) and len(lower_bounds) > 0:
        lbnds = np.array(lower_bounds)
    elif isinstance(lower_bounds, np.ndarray) and lower_bounds.size > 0:
        lbnds = lower_bounds.copy()
    if isinstance(upper_bounds, number_types):
        ubnds = np.array([upper_bounds])
    if isinstance(upper_bounds, (list, tuple)) and len(upper_bounds) > 0:
        ubnds = np.array(upper_bounds)
    elif isinstance(upper_bounds, np.ndarray) and upper_bounds.size > 0:
        ubnds = upper_bounds.copy()

    if vals is not None and errs is None and (lvals is None or uvals is None) and perrs is None:
        errs = np.zeros(vals.shape)

    avgdist = None
    stddist = None
    stdmean = None
    navgmap = None
    if vals is not None and errs is not None:
        if vals.shape != errs.shape:
            raise ValueError('Given data in eb error calculation do not have the same shape.')
        navgmap = np.full(vals.shape, True, dtype=bool)
        if vals.ndim == 1:

            # NaN and Inf filter - removal of entire index
            nfilt = np.all([np.isfinite(vals), np.isfinite(errs)], axis=0)
            fvals = vals[nfilt]
            ferrs = errs[nfilt]
            navgmap[np.invert(nfilt)] = False
            tavgmap = navgmap[nfilt]

            # Upper and lower bound filter - removal of single point
            lfilt = np.full(fvals.shape, True)
            ufilt = np.full(fvals.shape, True)
            if lbnds is not None and lbnds.size > 0:
                lbnd = lbnds.flatten()[0]
                lfilt = (fvals >= lbnd)
            if ubnds is not None and ubnds.size > 0:
                ubnd = ubnds.flatten()[0]
                ufilt = (fvals <= ubnd)
            rfilt = np.all([lfilt, ufilt], axis=0)
            fvals = fvals[rfilt].flatten() if np.any(rfilt) else None
            ferrs = ferrs[rfilt].flatten() if np.any(rfilt) else None
            tavgmap[np.invert(rfilt)] = False
            navgmap[nfilt] = tavgmap

            # Calculation call
            (avgdist, stddist, stdmean) = calc_eb_error(fvals, ferrs, sigma, use_n)
            if not isinstance(avgdist, np.ndarray) and avgdist is not None:
                avgdist = np.array([avgdist])
            if not isinstance(stddist, np.ndarray) and stddist is not None:
                stddist = np.array([stddist])
            if not isinstance(stdmean, np.ndarray) and stdmean is not None:
                stdmean = np.array([stdmean])

        elif vals.ndim == 2:
            avgdist = np.full((vals.shape[1],), np.NaN)
            stddist = avgdist.copy()
            stdmean = avgdist.copy()
            for jj in np.arange(0, vals.shape[1]):

                # NaN and Inf filter - removal of entire index
                nfilt = np.all([np.isfinite(vals[:, jj]), np.isfinite(errs[:, jj])], axis=0)
                fvals = vals[:, jj][nfilt]
                ferrs = errs[:, jj][nfilt]
                navgmap[:, jj][np.invert(nfilt)] = False
                tavgmap = navgmap[:, jj][nfilt]

                # Upper and lower bound filter - removal of single point
                lfilt = np.full(fvals.shape, True)
                ufilt = np.full(fvals.shape, True)
                if lbnds is not None and lbnds.size > 0:
                    lbnd = lbnds.flatten()[0]
                    if lbnds.ndim == 1 and lbnds.size == vals.shape[1]:
                        lbnd = lbnds[jj]
                    lfilt = (fvals >= lbnd)
                if ubnds is not None and ubnds.size > 0:
                    ubnd = ubnds.flatten()[0]
                    if ubnds.ndim == 1 and ubnds.size == vals.shape[1]:
                        ubnd = ubnds[jj]
                    ufilt = (fvals <= ubnd)
                rfilt = np.all([lfilt, ufilt], axis=0)
                fvals = fvals[rfilt].flatten() if np.any(rfilt) else None
                ferrs = ferrs[rfilt].flatten() if np.any(rfilt) else None
                tavgmap[np.invert(rfilt)] = False
                navgmap[:, jj][nfilt] = tavgmap

                # Calculation call and filling appropriate index in grand matrix
                (avg, std, sdm) = calc_eb_error(fvals, ferrs, sigma, use_n)
                avgdist[jj] = avg if avg is not None else np.NaN
                stddist[jj] = std if std is not None else np.NaN
                stdmean[jj] = sdm if sdm is not None else np.NaN

    elif vals is not None and lvals is not None and uvals is not None:
        if vals.shape != lvals.shape or vals.shape != uvals.shape:
            raise ValueError('Given data in hi-lo error calculation do not have the same shape.')
        navgmap = np.full(vals.shape, True, dtype=bool)
        if vals.ndim == 1:

            # NaN and Inf filter - removal of entire index
            nfilt = np.all([np.isfinite(vals), np.isfinite(lvals), np.isfinite(uvals)], axis=0)
            fvals = vals[nfilt]
            flvals = lvals[nfilt]
            fuvals = uvals[nfilt]
            navgmap[np.invert(nfilt)] = False
            tavgmap = navgmap[nfilt]

            # Upper and lower bound filter - removal of single point
            lfilt = np.full(fvals.shape, True)
            ufilt = np.full(fvals.shape, True)
            if lbnds is not None and lbnds.size > 0:
                lbnd = lbnds.flatten()[0]
                lfilt = (fvals >= lbnd)
            if ubnds is not None and ubnds.size > 0:
                ubnd = ubnds.flatten()[0]
                ufilt = (fvals <= ubnd)
            rfilt = np.all([lfilt, ufilt], axis=0)
            fvals = fvals[rfilt].flatten() if np.any(rfilt) else None
            flvals = flvals[rfilt].flatten() if np.any(rfilt) else None
            fuvals = fuvals[rfilt].flatten() if np.any(rfilt) else None
            tavgmap[np.invert(rfilt)] = False
            navgmap[nfilt] = tavgmap

            # Calculation call
            (avgdist, stddist, stdmean) = calc_hilo_error(fvals, flvals, fuvals, sigma, use_n)
            if not isinstance(avgdist, np.ndarray) and avgdist is not None:
                avgdist = np.array([avgdist])
            if not isinstance(stddist, np.ndarray) and stddist is not None:
                stddist = np.array([stddist])
            if not isinstance(stdmean, np.ndarray) and stdmean is not None:
                stdmean = np.array([stdmean])

        elif vals.ndim == 2:
            avgdist = np.full((vals.shape[1],), np.NaN)
            stddist = avgdist.copy()
            stdmean = avgdist.copy()
            for jj in np.arange(0, vals.shape[1]):

                # NaN and Inf filter - removal of entire index
                nfilt = np.all([np.isfinite(vals[:, jj]), np.isfinite(lvals[:, jj]), np.isfinite(uvals[:, jj])], axis=0)
                fvals = vals[:, jj][nfilt]
                flvals = lvals[:, jj][nfilt]
                fuvals = uvals[:, jj][nfilt]
                navgmap[:, jj][np.invert(nfilt)] = False
                tavgmap = navgmap[:, jj][nfilt]

                # Upper and lower bound filter - removal of single point
                lfilt = np.full(fvals.shape, True)
                ufilt = np.full(fvals.shape, True)
                if lbnds is not None and lbnds.size > 0:
                    lbnd = lbnds.flatten()[0]
                    if lbnds.ndim == 1 and lbnds.size == vals.shape[1]:
                        lbnd = lbnds[jj]
                    lfilt = (fvals >= lbnd)
                if ubnds is not None and ubnds.size > 0:
                    ubnd = ubnds.flatten()[0]
                    if ubnds.ndim == 1 and ubnds.size == vals.shape[1]:
                        ubnd = ubnds[jj]
                    ufilt = (fvals <= ubnd)
                rfilt = np.all([lfilt, ufilt], axis=0)
                fvals = fvals[rfilt].flatten() if np.any(rfilt) else None
                flvals = flvals[rfilt].flatten() if np.any(rfilt) else None
                fuvals = fuvals[rfilt].flatten() if np.any(rfilt) else None
                tavgmap[np.invert(rfilt)] = False
                navgmap[:, jj][nfilt] = tavgmap

                # Calculation call and filling appropriate index in grand matrix
                (avg, std, sdm) = calc_hilo_error(fvals, flvals, fuvals, sigma, use_n)
                avgdist[jj] = avg if avg is not None else np.NaN
                stddist[jj] = std if std is not None else np.NaN
                stdmean[jj] = sdm if sdm is not None else np.NaN

    elif vals is not None and perrs is not None:
        if vals.shape != perrs.shape:
            raise ValueError('Given data in pc error calculation do not have the same shape.')
        navgmap = np.full(vals.shape, True, dtype=bool)
        if vals.ndim == 1:

            # NaN and Inf filter - removal of entire index
            nfilt = np.all([np.isfinite(vals), np.isfinite(perrs)], axis=0)
            fvals = vals[nfilt]
            ferrs = perrs[nfilt] * fvals
            navgmap[np.invert(nfilt)] = False
            tavgmap = navgmap[nfilt]

            # Upper and lower bound filter - removal of single point
            lfilt = np.full(fvals.shape, True)
            ufilt = np.full(fvals.shape, True)
            if lbnds is not None and lbnds.size > 0:
                lbnd = lbnds.flatten()[0]
                lfilt = (fvals >= lbnd)
            if ubnds is not None and ubnds.size > 0:
                ubnd = ubnds.flatten()[0]
                ufilt = (fvals <= ubnd)
            rfilt = np.all([lfilt, ufilt], axis=0)
            fvals = fvals[rfilt].flatten() if np.any(rfilt) else None
            ferrs = ferrs[rfilt].flatten() if np.any(rfilt) else None
            tavgmap[np.invert(rfilt)] = False
            navgmap[nfilt] = tavgmap

            # Calculation call
            (avgdist, stddist, stdmean) = calc_eb_error(fvals, ferrs, sigma, use_n)
            if not isinstance(avgdist, np.ndarray) and avgdist is not None:
                avgdist = np.array([avgdist])
            if not isinstance(stddist, np.ndarray) and stddist is not None:
                stddist = np.array([stddist])
            if not isinstance(stdmean, np.ndarray) and stdmean is not None:
                stdmean = np.array([stdmean])

        elif vals.ndim == 2:
            avgdist = np.full((vals.shape[1],), np.NaN)
            stddist = avgdist.copy()
            stdmean = avgdist.copy()
            for jj in np.arange(0, vals.shape[1]):

                # NaN and Inf filter - removal of entire index
                nfilt = np.any([np.isfinite(vals[:, jj]), np.isfinite(perrs[:, jj])], axis=0)
                fvals = vals[:, jj][nfilt]
                ferrs = perrs[:, jj][nfilt] * fvals
                navgmap[:, jj][np.invert(nfilt)] = False
                tavgmap = navgmap[:, jj][nfilt]

                # Upper and lower bound filter - removal of single point
                lfilt = np.full(fvals.shape, True)
                ufilt = np.full(fvals.shape, True)
                if lbnds is not None and lbnds.size > 0:
                    lbnd = lbnds.flatten()[0]
                    if lbnds.ndim == 1 and lbnds.size == vals.shape[1]:
                        lbnd = lbnds[jj]
                    lfilt = (fvals >= lbnd)
                if ubnds is not None and ubnds.size > 0:
                    ubnd = ubnds.flatten()[0]
                    if ubnds.ndim == 1 and ubnds.size == vals.shape[1]:
                        ubnd = ubnds[jj]
                    ufilt = (fvals <= ubnd)
                rfilt = np.all([lfilt, ufilt], axis=0)
                fvals = fvals[rfilt].flatten() if np.any(rfilt) else None
                ferrs = ferrs[rfilt].flatten() if np.any(rfilt) else None
                tavgmap[np.invert(rfilt)] = False
                navgmap[:, jj][nfilt] = tavgmap

                # Calculation call and filling appropriate index in grand matrix
                (avg, std, sdm) = calc_eb_error(fvals, ferrs, sigma, use_n)
                avgdist[jj] = avg if avg is not None else np.NaN
                stddist[jj] = std if std is not None else np.NaN
                stdmean[jj] = sdm if sdm is not None else np.NaN

    if avgdist is None or stddist is None or stdmean is None:
        navgmap = None

    return (avgdist, stddist, stdmean, navgmap)


def calc_gaussian_figure_of_merit(value1, error1, value2, error2):
    """
    Computes containment figure-of-merit for comparing two Gaussian
    distributions. Derived from normalized area under curve defined
    by multiplying the two distributions together.

    :arg value1: array. Mean values of first distribution.

    :arg error1: array. Standard deviation values of first distribution.

    :arg value2: array. Mean values of second distribution.

    :arg error2: array. Standard deviation values of second distribution.

    :returns: (array, array, array)
        Figure-of-merit, normalized area under the multiplied curve, correction
        factor for distribution widths.
    """
    v1 = None
    e1 = None
    v2 = None
    e2 = None
    if isinstance(value1, number_types):
        v1 = np.array([value1])
    elif isinstance(value1, array_types):
        v1 = np.array(value1).flatten()
    if isinstance(error1, number_types):
        e1 = np.array([error1])
    elif isinstance(error1, array_types):
        e1 = np.array(error1).flatten()
    if isinstance(value2, number_types):
        v2 = np.array([value2])
    elif isinstance(value2, array_types):
        v2 = np.array(value2).flatten()
    if isinstance(error2, number_types):
        e2 = np.array([error2])
    elif isinstance(error2, array_types):
        e2 = np.array(error2).flatten()

    tm = None
    ts = None
    fom = None
    if v1 is not None and e1 is not None and v2 is not None and e2 is not None:
        tm = np.exp(-np.power(v1 - v2, 2.0) / (np.sqrt(2.0) * (np.power(e1, 2.0) + np.power(e2, 2.0))))
        ts = np.exp(-np.power(3.0 * e1 / v1, 2.0) - np.power(3.0 * e2 / v2, 2.0))
        fom = tm * ts

    return (fom, tm, ts)


def define_ion_species(z=None, a=None, short_name=None, long_name=None, user_mass=False):
    """
    Provides standardized names and values for the typical ion species within a tokamak.
    Used primarily to facilitate plotting of resulting fits. Not designed to accomodate
    partially charged states.

    :kwarg z: int. Atomic charge of particle, ie. fully ionized ion charge.

    :kwarg a: int. Atomic mass of particle, only needed for distinguishing H,D,T.

    :kwarg short_name: str. Shorthand name of element, returns average atomic mass and full atomic charge.

    :kwarg user_mass: bool. Toggles return of atomic mass argument as species mass, if provided.

    :returns: (str,float,float).
        Shorthand name of particle species, atomic charge of particle species, atomic
        mass of particle species.
    """
    specieslist = {  "e":  (  0.000544617, -1.0), "n":  (  1.000866492,  0.0), \
                     "H":  (  1.0,  1.0),  "D":  (  2.0,  1.0),  "T":  (  3.0,  1.0), "He":  (  4.0,  2.0), \
                    "Li":  (  7.0,  3.0), "Be":  (  9.0,  4.0),  "B":  ( 11.0,  5.0),  "C":  ( 12.0,  6.0), \
                     "N":  ( 14.0,  7.0),  "O":  ( 16.0,  8.0),  "F":  ( 19.0,  9.0), "Ne":  ( 20.0, 10.0), \
                    "Na":  ( 23.0, 11.0), "Mg":  ( 24.0, 12.0), "Al":  ( 27.0, 13.0), "Si":  ( 28.0, 14.0), \
                     "P":  ( 31.0, 15.0),  "S":  ( 32.0, 16.0), "Cl":  ( 35.0, 17.0), "Ar":  ( 40.0, 18.0), \
                     "K":  ( 39.0, 19.0), "Ca":  ( 40.0, 20.0), "Sc":  ( 45.0, 21.0), "Ti":  ( 48.0, 22.0), \
                     "V":  ( 51.0, 23.0), "Cr":  ( 52.0, 24.0), "Mn":  ( 55.0, 25.0), "Fe":  ( 56.0, 26.0), \
                    "Co":  ( 59.0, 27.0), "Ni":  ( 58.0, 28.0), "Cu":  ( 63.0, 29.0), "Zn":  ( 64.0, 30.0), \
                    "Ga":  ( 69.0, 31.0), "Ge":  ( 72.0, 32.0), "As":  ( 75.0, 33.0), "Se":  ( 80.0, 34.0), \
                    "Br":  ( 79.0, 35.0), "Kr":  ( 84.0, 36.0), "Rb":  ( 85.0, 37.0), "Sr":  ( 88.0, 38.0), \
                     "Y":  ( 89.0, 39.0), "Zr":  ( 90.0, 40.0), "Nb":  ( 93.0, 41.0), "Mo":  ( 96.0, 42.0), \
                    "Tc":  ( 99.0, 43.0), "Ru":  (102.0, 44.0), "Rh":  (103.0, 45.0), "Pd":  (106.0, 46.0), \
                    "Ag":  (107.0, 47.0), "Cd":  (114.0, 48.0), "In":  (115.0, 49.0), "Sn":  (120.0, 50.0), \
                    "Sb":  (121.0, 51.0), "Te":  (128.0, 52.0),  "I":  (127.0, 53.0), "Xe":  (131.0, 54.0), \
                    "Cs":  (133.0, 55.0), "Ba":  (138.0, 56.0), "La":  (139.0, 57.0),                       \
                    "Lu":  (175.0, 71.0), "Hf":  (178.0, 72.0), "Ta":  (181.0, 73.0),  "W":  (184.0, 74.0), \
                    "Re":  (186.0, 75.0), "Os":  (190.0, 76.0), "Ir":  (193.0, 77.0), "Pt":  (195.0, 78.0), \
                    "Au":  (197.0, 79.0), "Hg":  (200.0, 80.0), "Tl":  (205.0, 81.0), "Pb":  (208.0, 82.0), \
                    "Bi":  (209.0, 83.0), "Po":  (209.0, 84.0), "At":  (210.0, 85.0), "Rn":  (222.0, 86.0)    }

    tz = None
    ta = None
    sn = None
    ln = None
    if isinstance(z, number_types) and int(np.rint(z)) >= -1:
        tz = int(np.rint(z))
    if isinstance(a, number_types) and int(np.rint(a)) > 0:
        ta = int(np.rint(a))
    if isinstance(short_name, str) and short_name in specieslist:
        sn = short_name
    if isinstance(long_name, str):
        print("Long name species identifier not yet implemented")

    # Determines atomic charge number based on atomic mass number, if no charge number is given
    if isinstance(ta, int) and tz is None:
        for key, val in specieslist.items():
            if ta == int(np.rint(val[0])):
                ta = int(np.rint(val[0]))
                tz = int(np.rint(val[1]))
    if tz is None:
        ta = None

    # Enforces default return value as deuterium if no arguments or improper arguments are given
    if ta is None and sn is None and ln is None:
        if tz is None:
            tz = 1
        if tz == 1:
            ta = 2

    sz = None
    sa = None
    sname = None
#    lname = None

    periodic_table = [ "n", # The first 'element' should always be the neutron, for consistency with the numbering
                       "H",                                                                                "He", \
                      "Li","Be",                                                   "B", "C", "N", "O", "F","Ne", \
                      "Na","Mg",                                                  "Al","Si", "P", "S","Cl","Ar", \
                       "K","Ca","Sc","Ti", "V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr", \
                      "Rb","Sr", "Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te", "I","Xe", \
                      "Cs","Ba","La",     "Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu", \
                                     "Hf","Ta", "W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn", \
                      "Fr","Ra","Ac",     "Th","Pa", "U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr", \
                                     "Rf","Db","Sg","Bh","Hs",                                                   \
                      "e"]  # The last 'element' should always be the electron, for consistency with the numbering

    # Prioritize shorthand element name over atomic charge argument
    if sn is not None and sn in specieslist:
        sname = sn
        (sa, sz) = specieslist[sname]
    elif tz is not None:
        sname = periodic_table[tz] if tz < len(periodic_table) - 1 else periodic_table[-1]
        (sa, sz) = specieslist[sname]

    if sname is not None and sa is not None and sz is not None:
        # Allow user specification of mass according to broad heuristic isotopic limits
        if user_mass and isinstance(a, number_types) and float(a) >= sz and float(a) <= sz*3:
            sa = float(a)
        elif ta is not None and float(ta) != sa:
            sa = float(ta)
        for key, val in specieslist.items():
            if sa == int(np.rint(val[0])) and sz == int(np.rint(val[1])):
                sname = key

    return (sname, sa, sz)


def define_coordinate_system(cs=None):
    """
    Provides standardized names and labels for the typical radial coordination systems
    used in tokamak data analysis. Used primarily to facilitate plotting of resulting fits.

    :kwarg cs: str. Coordinate system specification.

    :returns: (str, str, str, str, str).
        Standardized label within the EX2GK custom data structure, full label for use in
        generic plots, SI unit of selected coordinate system, LaTeX-formatted shorthand
        label for use in presentation plots, LaTeX-formatted SI unit of selected coordinate
        system.
    """
    cstag = None
    cslbl = r'Unspecified'
    csunit = r''
    fcslbl = r'Unspecified'
    fcsunit = r''
    if cs is None:
        cstag = "RHOTORN"
        cslbl = r'Toroidal Rho'
        csunit = r''
        fcslbl = r'$\rho_{\text{tor}}$'
        fcsunit = r''
    elif re.match(r'^rhotorn?$', cs, flags=re.IGNORECASE):
        cstag = "RHOTORN"
        cslbl = r'Toroidal Rho'
        csunit = r''
        fcslbl = r'$\rho_{\text{tor}}$'
        fcsunit = r''
    elif re.match(r'^rhopoln?$', cs, flags=re.IGNORECASE):
        cstag = "RHOPOLN"
        cslbl = r'Poloidal Rho'
        csunit = r''
        fcslbl = r'$\rho_{\text{pol}}$'
        fcsunit = r''
    elif re.match(r'^psitor$', cs, flags=re.IGNORECASE):
        cstag = "PSITOR"
        cslbl = r'Toroidal Flux'
        csunit = r'Wb'
        fcslbl = r'$\psi_{\text{tor}}$'
        fcsunit = r'Wb'
    elif re.match(r'^psitorn$', cs, flags=re.IGNORECASE):
        cstag = "PSITORN"
        cslbl = r'Normalized Toroidal Flux'
        csunit = r''
        fcslbl = r'$\psi_{\text{tor},n}$'
        fcsunit = r''
    elif re.match(r'^psipol$', cs, flags=re.IGNORECASE):
        cstag = "PSIPOL"
        cslbl = r'Poloidal Flux'
        csunit = r'Wb'
        fcslbl = r'$\psi_{\text{pol}}$'
        fcsunit = r'Wb'
    elif re.match(r'^psipoln$', cs, flags=re.IGNORECASE):
        cstag = "PSIPOLN"
        cslbl = r'Normalized Poloidal Flux'
        csunit = r''
        fcslbl = r'$\psi_{\text{pol},n}$'
        fcsunit = r''
    elif re.match(r'^torflux$', cs, flags=re.IGNORECASE):
        cstag = "PSITOR"
        cslbl = r'Toroidal Flux'
        csunit = r'Wb'
        fcslbl = r'$\psi_{\text{tor}}$'
        fcsunit = r'Wb'
    elif re.match(r'^torfluxn$', cs, flags=re.IGNORECASE):
        cstag = "PSITORN"
        cslbl = r'Normalized Toroidal Flux'
        csunit = r''
        fcslbl = r'$\psi_{\text{tor},n}$'
        fcsunit = r''
    elif re.match(r'^polflux$', cs, flags=re.IGNORECASE):
        cstag = "PSIPOL"
        cslbl = r'Poloidal Flux'
        csunit = r'Wb'
        fcslbl = r'$\psi_{\text{pol}}$'
        fcsunit = r'Wb'
    elif re.match(r'^polfluxn$', cs, flags=re.IGNORECASE):
        cstag = "PSIPOLN"
        cslbl = r'Normalized Poloidal Flux'
        csunit = r''
        fcslbl = r'$\psi_{\text{pol},n}$'
        fcsunit = r''
    elif re.match(r'^rmajor$', cs, flags=re.IGNORECASE):
        cstag = "RMAJORO"
        cslbl = r'Major Radius'
        csunit = r'm'
        fcslbl = r'$R$'
        fcsunit = r'm'
    elif re.match(r'^rmajoro$', cs, flags=re.IGNORECASE):
        cstag = "RMAJORO"
        cslbl = r'Outer Major Radius'
        csunit = r'm'
        fcslbl = r'$R_o$'
        fcsunit = r'm'
    elif re.match(r'^rmajori$', cs, flags=re.IGNORECASE):
        cstag = "RMAJORI"
        cslbl = r'Inner Major Radius'
        csunit = r'm'
        fcslbl = r'$R_i$'
        fcsunit = r'm'
    elif re.match(r'^rminor$', cs, flags=re.IGNORECASE):
        cstag = "RMINORO"
        cslbl = r'Minor Radius'
        csunit = r'm'
        fcslbl = r'$r$'
        fcsunit = r'm'
    elif re.match(r'^rminoro$', cs, flags=re.IGNORECASE):
        cstag = "RMINORO"
        cslbl = r'Outer Minor Radius'
        csunit = r'm'
        fcslbl = r'$r_o$'
        fcsunit = r'm'
    elif re.match(r'^rminori$', cs, flags=re.IGNORECASE):
        cstag = "RMINORI"
        cslbl = r'Inner Minor Radius'
        csunit = r'm'
        fcslbl = r'$r_i$'
        fcsunit = r'm'
    elif re.match(r'^rmajora$', cs, flags=re.IGNORECASE):
        cstag = "RMAJORA"
        cslbl = r'Midplane-Averaged Major Radius'
        csunit = r'm'
        fcslbl = r'$R_{\text{ave}}$'
        fcsunit = r'm'
    elif re.match(r'^rminora$', cs, flags=re.IGNORECASE):
        cstag = "RMINORA"
        cslbl = r'Midplane-Averaged Minor Radius'
        csunit = r'm'
        fcslbl = r'$r_{\text{ave}}$'
        fcsunit = r'm'
    elif re.match(r'^rave$', cs, flags=re.IGNORECASE):
        cstag = "RMINORA"
        cslbl = r'Midplane-Averaged Minor Radius'
        csunit = r'm'
        fcslbl = r'$r_{\text{ave}}$'
        fcsunit = r'm'
    elif re.match(r'^fsvol(ume)?$', cs, flags=re.IGNORECASE):
        cstag = "FSVOL"
        cslbl = r'Flux Surface Volume'
        csunit = r'm**3'
        fcslbl = r'$V_{\text{fs}}$'
        fcsunit = r'm$^3$'
    elif re.match(r'^fsarea$', cs, flags=re.IGNORECASE):
        cstag = "FSAREA"
        cslbl = r'Flux Surface Surface Area'
        csunit = r'm**2'
        fcslbl = r'$A_{\text{fs}}$'
        fcsunit = r'm$^2$'
    elif re.match(r'^v(ol(ume)?)?$', cs, flags=re.IGNORECASE):
        cstag = "FSVOL"
        cslbl = r'Flux Surface Volume'
        csunit = r'm**3'
        fcslbl = r'$V_{\text{fs}}$'
        fcsunit = r'm$^3$'
    elif re.match(r'^a(rea)?$', cs, flags=re.IGNORECASE):
        cstag = "FSAREA"
        cslbl = r'Flux Surface Surface Area'
        csunit = r'm**2'
        fcslbl = r'$A_{\text{fs}}$'
        fcsunit = r'm$^2$'
    elif re.match(r'^.+_x$', cs, flags=re.IGNORECASE):
        cstag = "OUT"+cs.upper()
        cslbl = r'Adapter Coordinate'
        csunit = r''
        fcslbl = r'$x$'
        fcsunit = r''
    return (cstag, cslbl, csunit, fcslbl, fcsunit)


def convert_base_coords(datavec, csvecin, csvecout, cjvecin, cjvecout, cevecout):
    """
    Automated conversion between the unified coordinate systems defined
    in the standardised data object. Linearly interpolates between the
    provided vectors and computes the associated Jacobians as well,
    provided that the input Jacobians are with respect to the same
    reference coordinate system.

    :arg datadict: dict. Object containing unified coordinate system information.

    :arg datavec: array. Vector of radial points corresponding to input coordinate system.

    :arg csin: str. Standardized name of input coordinate system.

    :arg csout: str. Standardized name of output coordinate system.

    :kwarg prein: str. Standardized prefix for input coordinate system. I for interface, C for corrected.

    :kwarg preout: str. Standardized prefix for output coordinate system. I for interface, C for corrected.

    :returns: (array, array, array).
        Vector of radial points corresponding to the output coordinate system, vector
        of Jacobians corresponding to the conversion between input and output
        coordinate systems at the radial points, vector of radial errors corresponding
        to the conversion between input and output coordinate systems.
    """
    dvec = None
    civec = None        # Input coordinate system vector
    covec = None        # Output coordinate system vector
    joveci = None       # Input coordinate system Jacobian
    joveco = None       # Output coordinate system Jacobian
    eovec = None        # Output coordinate system error
    if isinstance(datavec, number_types):
        dvec = np.array([datavec]).flatten()
    elif isinstance(datavec, array_types):
        dvec = np.array(datavec).flatten()
    if isinstance(csvecin, array_types):
        civec = np.array(csvecin).flatten()
    if isinstance(csvecout, array_types):
        covec = np.array(csvecout).flatten()
    if isinstance(cjvecin, array_types):
        joveci = np.array(cjvecin).flatten()
    if isinstance(cjvecout, array_types):
        joveco = np.array(cjvecout).flatten()
    if isinstance(cevecout, array_types):
        eovec = np.array(cevecout).flatten()

    cvec = None       # Coordinate vector to be returned
    jvec = None       # Jacobian vector to be returned
    evec = None       # Error vector to be returned

    if dvec is not None and civec is not None and covec is not None and civec.shape == covec.shape:

        # Simplify process if input and output coordinate systems are identical
        if np.array_equal(civec, covec):
            cvec = dvec.copy()
            jvec = np.ones(cvec.shape)
            evec = np.zeros(cvec.shape)

        else:

            # Determine the converted coordinate vector
            cfunc = interp1d(civec, covec, bounds_error=False, fill_value='extrapolate')
            cvec = cfunc(dvec)

            # Determine the Jacobian vector - d (input CS) / d (output CS)
            jvec = np.ones(dvec.shape)
            if joveci is not None:
                if joveco is not None:
                    jifunc = interp1d(civec, joveci, bounds_error=False, fill_value='extrapolate')
                    jofunc = interp1d(civec, joveco, bounds_error=False, fill_value='extrapolate')
                    jnum = jofunc(dvec)
                    jtemp = jifunc(dvec)
                    nfilt = np.all([np.isfinite(jnum), np.isfinite(jtemp)], axis=0)
                    jnum2 = jnum[nfilt]
                    jtemp2 = jtemp[nfilt]
                    jinter = jvec[nfilt]
                    zfilt = np.invert(np.abs(jtemp2) < 1.0e-5)
                    jinter[zfilt] = jnum2[zfilt] / jtemp2[zfilt]
                    jvec[nfilt] = jinter.copy()
                else:
                    jfunc = interp1d(civec, joveci, bounds_error=False, fill_value='extrapolate')
                    jtemp = jfunc(dvec)
                    nfilt = np.isfinite(jtemp)
                    jtemp2 = jtemp[nfilt]
                    jinter = jvec[nfilt]
                    zfilt = np.invert(np.abs(jtemp2) < 1.0e-5)
                    jinter[zfilt] = 1.0 / jtemp2[zfilt]
                    jvec[nfilt] = jinter.copy()
            elif joveco is not None:
                jfunc = interp1d(civec, joveco, bounds_error=False, fill_value='extrapolate')
                jinter = jfunc(dvec)
                nfilt = np.isfinite(jinter)
                jvec[nfilt] = jinter[nfilt].copy()
            elif civec.size > 2:
                jcvec = civec[:-1] + np.diff(civec)
                jtvec = np.diff(civec) / np.diff(covec)
                jtfunc = interp1d(jcvec, jtvec, bounds_error=False, fill_value='extrapolate')
                jvec = jtfunc(dvec)

            # Determine the error vector
            #     Only provides the absolute error of the output coordinate system,
            #     must be manually combined with input error vector if required
            evec = np.zeros(dvec.shape)
            if eovec is not None:
                efunc = interp1d(civec, eovec, bounds_error=False, fill_value='extrapolate')
                evec = efunc(dvec)
                evec[evec < 0.0] = 0.0

    return (cvec, jvec, evec)


def define_quantity(name, exponent=None, derivative=None):
    """
    Provides standardized names and labels for the typical radial coordination systems
    used in tokamak data analysis. Used primarily to facilitate plotting of resulting fits.

    :arg name: str. Any typical generalized name for the desired quantity.

    :kwarg exponent: int. Order of magnitude of desired quantity, to be placed into plot labels.

    :kwarg derivative: str. Coordinate name for which derivative is taken with respect to, toggles output of derivative plot labels instead of regular ones.

    :returns: (str, str, str, str, str).
        Standardized label within the EX2GK custom data structure, full label for use in
        generic plots, SI unit of the selected quantity, LaTeX-formatted shorthand label
        for use in presentation plots, LaTeX-formatted SI unit of selected quantity.
    """
    qtag = None
    qlbl = r'Unspecified'
    qunit = r''
    fqlbl = r'Unspecified'
    fqunit = r''
    if not isinstance(name, str):
        raise TypeError("Name argument must be a string for quantity definition tool, aborting...")
    else:
        (qtag, qlbl, qunit, fqlbl, fqunit) = define_coordinate_system(name)
    if qtag is None:
        mm = re.match(r'^(CD_)?([A-Z]+)_([A-Z]?)([VE])$', name, flags=re.IGNORECASE)
        if mm:
            (qtag, qlbl, qunit, fqlbl, fqunit) = define_coordinate_system(mm.group(2))
            if qtag is not None:
                qtag = name
                if mm.group(4).upper() == "E":
                    qlbl = qlbl + r' Error'
#                if mm.group(3):
#                    qlbl = qlbl + r' Corrected'
    if qtag is None:
        mm = re.match(r'^(CD_)?([A-Z]+)_([A-Z]?)J_([A-Z]+)$', name, flags=re.IGNORECASE)
        if mm:
            (ntag, nlbl, nunit, fnlbl, fnunit) = define_coordinate_system(mm.group(4))
            (dtag, dlbl, dunit, fdlbl, fdunit) = define_coordinate_system(mm.group(2))
            if ntag is not None and dtag is not None:
                qtag = name
                qlbl = r'Jacobian ' + nlbl + r' wrt ' + dlbl
                fqlbl = r'$d$ ' + fnlbl + r' / $d$ ' + fdlbl
                fqunit = r''
                if not (nunit == dunit):
                    if nunit and not dunit:
                        qunit = nunit
                        fqunit = fnunit
                    elif dunit and not nunit:
                        mmm = re.search(r'^([A-Za-z]+)*\*\*([\-0-9]+)', dunit)
                        if mmm:
                            qunit = r'' + mmm.group(1) + r'**'
                            qunit = qunit + mmm.group(2) if re.match(r'^-', mmm.group(2)) else qunit + mmm.group(2)[1:]
                            fqunit = r'' + mmm.group(1) + r'$^{'
                            fqunit = fqunit + mmm.group(2) + r'}$' if re.match(r'^-', mmm.group(2)) else qunit + mmm.group(2)[1:] + r'}$'
                    else:
                        qunit = r'' + nunit + r' / ' + dunit
                        fqunit = r'' + fnunit + r' / ' + fdunit
    if qtag is None:
        explist = {-9: r'n', -6: r'u', -3: r'm', 0: r'', 3: r'k', 6: r'M', 9: 'G', 12: 'T'}
        fexplist = {-9: r'n', -6: r'$\mu$', -3: r'm', 0: r'', 3: r'k', 6: r'M', 9: 'G', 12: 'T'}
        elabel = r''
        felbl = r''
        etag = r''
        fetag = r''
        if isinstance(exponent, number_types):
            exp = int(exponent)
            scale = np.power(10.0, exp)
            if exp in explist and exp in fexplist:
                etag = explist[exp]
                fetag = fexplist[exp]
            else:
                etag = r'10^' + "%d" % (exp) + r' '
                fetag = r'$10^{' + "%d" % (exp) + r'}$ '
        if re.match(r'^t(ime)?$', name, flags=re.IGNORECASE):
            qtag = "TIME"
            qlbl = r'Time'
            qunit = etag + r's'
            fqlbl = r'$t$'
            fqunit = fetag + r's'
        elif re.match(r'^rmag$', name, flags=re.IGNORECASE):
            qtag = "RMAG"
            qlbl = r'Major Radius of Magnetic Axis'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{mag}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zmag$', name, flags=re.IGNORECASE):
            qtag = "ZMAG"
            qlbl = r'Height of Magnetic Axis'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{mag}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^bmag$', name, flags=re.IGNORECASE):
            qtag = "BMAG"
            qlbl = r'Magnetic Field at Magnetic Axis'
            qunit = etag + r'T'
            fqlbl = r'$B_{\text{mag}}$'
            fqunit = fetag + r'T'
        elif re.match(r'^bt$', name, flags=re.IGNORECASE):
            qtag = "BT"
            qlbl = r'Toroidal Magnetic Field'
            qunit = etag + r'T'
            fqlbl = r'$B_{T}$'
            fqunit = fetag + r'T'
        elif re.match(r'^bvac$', name, flags=re.IGNORECASE):
            qtag = "BT"
            qlbl = r'Toroidal Magnetic Field'
            qunit = etag + r'T'
            fqlbl = r'$B_{T}$'
            fqunit = fetag + r'T'
        elif re.match(r'^ipl?a?$', name, flags=re.IGNORECASE):
            qtag = "IPLA"
            qlbl = r'Total Plasma Current'
            qunit = etag + r'A'
            fqlbl = r'$I_{p}$'
            fqunit = fetag + r'A'
        elif re.match(r'^rgeo$', name, flags=re.IGNORECASE):
            qtag = "RGEO"
            qlbl = r'Major Radius of Geometric Axis'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{geo}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zgeo$', name, flags=re.IGNORECASE):
            qtag = "ZGEO"
            qlbl = r'Height of Geometric Axis'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{geo}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rxp(oint)?l$', name, flags=re.IGNORECASE):
            qtag = "RXPOINTL"
            qlbl = r'Major Radius of Lower X-Point'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{x,l}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zxp(oint)?l$', name, flags=re.IGNORECASE):
            qtag = "ZXPOINTL"
            qlbl = r'Height of Lower X-Point'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{x,l}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rxp(oint)?u$', name, flags=re.IGNORECASE):
            qtag = "RXPOINTU"
            qlbl = r'Major Radius of Upper X-Point'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{x,u}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zxp(oint)?u$', name, flags=re.IGNORECASE):
            qtag = "ZXPOINTU"
            qlbl = r'Height of Upper X-Point'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{x,u}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rs(trike)?lin?$', name, flags=re.IGNORECASE):
            qtag = "RSTRIKELIN"
            qlbl = r'Major Radius of Lower Inner Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{s,l,i}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zs(trike)?lin?$', name, flags=re.IGNORECASE):
            qtag = "ZSTRIKELIN"
            qlbl = r'Height of Lower Inner Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{s,l,i}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rs(trike)?lo(ut)?$', name, flags=re.IGNORECASE):
            qtag = "RSTRIKELOUT"
            qlbl = r'Major Radius of Lower Outer Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{s,l,o}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zs(trike)?lo(ut)?$', name, flags=re.IGNORECASE):
            qtag = "ZSTRIKELOUT"
            qlbl = r'Height of Lower Outer Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{s,l,o}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rs(trike)?uin?$', name, flags=re.IGNORECASE):
            qtag = "RSTRIKEUIN"
            qlbl = r'Major Radius of Upper Inner Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{s,u,i}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zs(trike)?uin?$', name, flags=re.IGNORECASE):
            qtag = "ZSTRIKEUIN"
            qlbl = r'Height of Upper Inner Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{s,u,i}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rs(trike)?uo(ut)?$', name, flags=re.IGNORECASE):
            qtag = "RSTRIKEUOUT"
            qlbl = r'Major Radius of Upper Outer Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{s,u,o}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^zs(trike)?uo(ut)?$', name, flags=re.IGNORECASE):
            qtag = "ZSTRIKEUOUT"
            qlbl = r'Height of Upper Outer Strike Point'
            qunit = etag + r'm'
            fqlbl = r'$Z_{\text{s,u,o}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^rvac$', name, flags=re.IGNORECASE):
            qtag = "RVAC"
            qlbl = r'Major Radius of Vacuum Reference'
            qunit = etag + r'm'
            fqlbl = r'$R_{\text{vac}}$'
            fqunit = fetag + r'm'
        elif re.match(r'^bvac$', name, flags=re.IGNORECASE):
            qtag = "BVAC"
            qlbl = r'Vacuum Magnetic Field at Vacuum Reference'
            qunit = etag + r'T'
            fqlbl = r'$B_{\text{vac}}$'
            fqunit = fetag + r'T'
        elif re.match(r'^ps?i?bnd$', name, flags=re.IGNORECASE):
            qtag = "PSIBND"
            qlbl = r'Poloidal Flux at LCFS'
            qunit = etag + r'Wb'
            fqlbl = r'$\psi_{\text{bnd}}$'
            fqunit = fetag + r'Wb'
        elif re.match(r'^ps?i?axs$', name, flags=re.IGNORECASE):
            qtag = "PSIAXS"
            qlbl = r'Poloidal Flux at Magnetic Axis'
            qunit = etag + r'Wb'
            fqlbl = r'$\psi_{\text{axs}}$'
            fqunit = fetag + r'Wb'
        elif re.match(r'^vloop$', name, flags=re.IGNORECASE):
            qtag = "VLOOP"
            qlbl = r'Loop Voltage'
            qunit = etag + r'V'
            fqlbl = r'$V_{\text{loop}}$'
            fqunit = fetag + r'V'
        elif re.match(r'^elong?$', name, flags=re.IGNORECASE):
            qtag = "ELONG"
            qlbl = r'Elongation'
            qunit = etag + r''
            fqlbl = r'$\kappa$'
            fqunit = fetag + r''
        elif re.match(r'^tri(ang)?lo?$', name, flags=re.IGNORECASE):
            qtag = "TRILO"
            qlbl = r'Lower Triangularity'
            qunit = etag + r''
            fqlbl = r'$\delta_{\text{lower}}$'
            fqunit = fetag + r''
        elif re.match(r'^tri(ang)?up?$', name, flags=re.IGNORECASE):
            qtag = "TRIUP"
            qlbl = r'Upper Triangularity'
            qunit = etag + r''
            fqlbl = r'$\delta_{\text{upper}}$'
            fqunit = fetag + r''
        elif re.match(r'^frac[hdt]$', name, flags=re.IGNORECASE):
            vv = re.search(r'([hdt])$', name, flags=re.IGNORECASE)
            itag = vv.group(1).upper()
            qtag = "FRAC" + itag
            qlbl = r'Isotopic Fraction of ' + itag
            qunit = etag + r''
            fqlbl = r'$f_{' + itag + r'}$'
            fqunit = fetag + r''
        elif re.match(r'^gasr[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            gtag = "GASR" + itag
            iitag = itag + " " if itag else itag
            qlbl = 'Injected Gas ' + iitag + 'Electron Flow Rate'
            qunit = etag + r's**-1'
            fqlbl = r'$R_{\text{gas}'+itag+r'}$'
            fqunit = fetag + r's$^{-1}$'
        elif re.match(r'^gasc[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            gtag = "GASC" + itag
            iitag = itag + " " if itag else itag
            qlbl = 'Injected Gas ' + iitag + 'Cumulative Electron Count'
            qunit = etag + r''
            fqlbl = r'$C_{\text{gas}'+itag+r'}$'
            fqunit = fetag + r''
        elif re.match(r'^ne$', name, flags=re.IGNORECASE):
            qtag = "NE"
            qlbl = r'Electron Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_e$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^te$', name, flags=re.IGNORECASE):
            qtag = "TE"
            qlbl = r'Electron Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$T_e$'
            fqunit = fetag + r'eV'
        elif re.match(r'^pe$', name, flags=re.IGNORECASE):
            qtag = "PE"
            qlbl = r'Electron Pressure'
            qunit = etag + r'Pa'
            fqlbl = r'$p_e$'
            fqunit = fetag + r'Pa'
        elif re.match(r'^neax$', name, flags=re.IGNORECASE):
            qtag = "NEAX"
            qlbl = r'Axial Electron Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_{e,0}$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^teax$', name, flags=re.IGNORECASE):
            qtag = "TEAX"
            qlbl = r'Axial Electron Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$T_{e,0}$'
            fqunit = fetag + r'eV'
        elif re.match(r'^neav$', name, flags=re.IGNORECASE):
            qtag = "NEBAR"
            qlbl = r'Volume-Averaged Electron Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$\left\langle n_e \right\rangle$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^teav$', name, flags=re.IGNORECASE):
            qtag = "TEBAR"
            qlbl = r'Volume-Averaged Electron Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$\left\langle T_e \right\rangle$'
            fqunit = fetag + r'eV'
        elif re.match(r'^nebar$', name, flags=re.IGNORECASE):
            qtag = "NEBAR"
            qlbl = r'Volume-Averaged Electron Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$\left\langle n_e \right\rangle$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^tebar$', name, flags=re.IGNORECASE):
            qtag = "TEBAR"
            qlbl = r'Volume-Averaged Electron Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$\left\langle T_e \right\rangle$'
            fqunit = fetag + r'eV'
        elif re.match(r'^nel(in)?$', name, flags=re.IGNORECASE):
            qtag = "NELIN"
            qlbl = r'Line-Averaged Electron Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$\bar{n_e}$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^tel(in)?$', name, flags=re.IGNORECASE):
            qtag = "TELIN"
            qlbl = r'Line-Averaged Electron Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$\bar{T_e}$'
            fqunit = fetag + r'eV'
        elif re.match(r'^bpt(t|h)(d|dia|exp)?$', name, flags=re.IGNORECASE):
            qetag = "BETAPEXP"
            qlbl = r'Beta Poloidal'
            qunit = etag + r''
            fqlbl = r'$\beta_{p}$'
            fqunit = fetag + r''
        elif re.match(r'^btt(t|h)(d|dia|exp)$', name, flags=re.IGNORECASE):
            qetag = "BETATEXP"
            qlbl = r'Beta Toroidal'
            qunit = etag + r''
            fqlbl = r'$\beta_{t}$'
            fqunit = fetag + r''
        elif re.match(r'^bnt(t|h)(d|dia|exp)$', name, flags=re.IGNORECASE):
            qetag = "BETANEXP"
            qlbl = r'Beta Normalized'
            qunit = etag + r''
            fqlbl = r'$\beta_{N}$'
            fqunit = fetag + r''
        elif re.match(r'^be?ta?t?p(ol)?(d|dia|exp)$', name, flags=re.IGNORECASE):
            qetag = "BETAPEXP"
            qlbl = r'Diamagnetic Beta Poloidal'
            qunit = etag + r''
            fqlbl = r'$\beta_{p,\text{dia}}$'
            fqunit = fetag + r''
        elif re.match(r'^be?ta?t?t(or)?(d|dia|exp)$', name, flags=re.IGNORECASE):
            qetag = "BETATEXP"
            qlbl = r'Diamagnetic Beta Toroidal'
            qunit = etag + r''
            fqlbl = r'$\beta_{t,\text{dia}}$'
            fqunit = fetag + r''
        elif re.match(r'^be?ta?t?n(orm)?(d|dia|exp)$', name, flags=re.IGNORECASE):
            qetag = "BETANEXP"
            qlbl = r'Diamagnetic Beta Normalized'
            qunit = etag + r''
            fqlbl = r'$\beta_{N,\text{dia}}$'
            fqunit = fetag + r''
        elif re.match(r'^be?ta?t?p(ol)?(m|mhd|eq)?$', name, flags=re.IGNORECASE):
            qetag = "BETAPMHD"
            qlbl = r'Equilibrium Beta Poloidal'
            qunit = etag + r''
            fqlbl = r'$\beta_{p,\text{mhd}}$'
            fqunit = fetag + r''
        elif re.match(r'^be?ta?t?t(or)?(m|mhd|eq)?$', name, flags=re.IGNORECASE):
            qetag = "BETATMHD"
            qlbl = r'Equilibrium Beta Toroidal'
            qunit = etag + r''
            fqlbl = r'$\beta_{t,\text{mhd}}$'
            fqunit = fetag + r''
        elif re.match(r'^be?ta?t?n(orm)?(m|mhd|eq)?$', name, flags=re.IGNORECASE):
            qetag = "BETANMHD"
            qlbl = r'Equilibrium Beta Normalized'
            qunit = etag + r''
            fqlbl = r'$\beta_{N,\text{mhd}}$'
            fqunit = fetag + r''
        elif re.match(r'^wexp$', name, flags=re.IGNORECASE):
            qetag = "WEXP"
            qlbl = r'Measured Stored Plasma Energy'
            qunit = etag + r'J'
            fqlbl = r'$W_{\text{exp}}$'
            fqunit = fetag + r'J'
        elif re.match(r'^wmhd$', name, flags=re.IGNORECASE):
            qetag = "WMHD"
            qlbl = r'Equilibrium Stored Plasma Energy'
            qunit = etag + r'J'
            fqlbl = r'$W_{\text{mhd}}$'
            fqunit = fetag + r'J'
        elif re.match(r'^betaei$', name, flags=re.IGNORECASE):
            qetag = "BETAEI"
            qlbl = r'Kinetic Beta Normalized (Electrons + Ions)'
            qunit = etag + r''
            fqlbl = r'$\beta_{ei}$'
            fqunit = fetag + r''
        elif re.match(r'^betath$', name, flags=re.IGNORECASE):
            qetag = "BETATH"
            qlbl = r'Kinetic Beta Normalized (Thermal)'
            qunit = etag + r''
            fqlbl = r'$\beta_{th}$'
            fqunit = fetag + r''
        elif re.match(r'^betatot$', name, flags=re.IGNORECASE):
            qetag = "BETATOT"
            qlbl = r'Kinetic Beta Normalized (Thermal + Fast)'
            qunit = etag + r''
            fqlbl = r'$\beta_{tot}$'
            fqunit = fetag + r''
        elif re.match(r'^wei$', name, flags=re.IGNORECASE):
            qetag = "WEI"
            qlbl = r'Stored Plasma Energy (Electrons + Ions)'
            qunit = etag + r'J'
            fqlbl = r'$W_{ei}$'
            fqunit = fetag + r'J'
        elif re.match(r'^wth$', name, flags=re.IGNORECASE):
            qetag = "WTH"
            qlbl = r'Stored Plasma Energy (Thermal)'
            qunit = etag + r'J'
            fqlbl = r'$W_{th}$'
            fqunit = fetag + r'J'
        elif re.match(r'^wtot$', name, flags=re.IGNORECASE):
            qetag = "WTOT"
            qlbl = r'Stored Plasma Energy (Thermal + Fast)'
            qunit = etag + r'J'
            fqlbl = r'$W_{tot}$'
            fqunit = fetag + r'J'
        elif re.match(r'^ni[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "NI" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Main Ion '+iitag+r'Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_i$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^ti[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "TI" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Main Ion '+iitag+r'Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$T_i$'
            fqunit = fetag + r'eV'
        elif re.match(r'^pi[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "PI" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Main Ion '+iitag+r'Pressure'
            qunit = etag + r'Pa'
            fqlbl = r'$p_i$'
            fqunit = fetag + r'Pa'
        elif re.match(r'^nimp?[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "NIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_{\text{imp}}$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^timp?[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "TIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$T_{\text{imp}}$'
            fqunit = fetag + r'eV'
        elif re.match(r'^fimp?[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "FIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Density Fraction'
            qunit = etag + r''
            fqlbl = r'$n_{\text{imp}} / n_e$'
            fqunit = fetag + r''
        elif re.match(r'^imp?c[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "FIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Density Fraction'
            qunit = etag + r''
            fqlbl = r'$n_{\text{imp}} / n_e$'
            fqunit = fetag + r''
        elif re.match(r'^pimp?[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "PIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Pressure'
            qunit = etag + r'Pa'
            fqlbl = r'$p_{\text{imp}}$'
            fqunit = fetag + r'Pa'
        elif re.match(r'^nz[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "NZ" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Assumed Impurity Ion '+iitag+r'Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_z$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.match(r'^tz[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "TZ" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Assumed Impurity Ion '+iitag+r'Temperature'
            qunit = etag + r'eV'
            fqlbl = r'$T_z$'
            fqunit = fetag + r'eV'
        elif re.match(r'^pz[0-9]*$', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "PZ" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Assumed Impurity Ion '+iitag+r'Pressure'
            qunit = etag + r'Pa'
            fqlbl = r'$p_z$'
            fqunit = fetag + r'Pa'
        elif re.match(r'^angf((t)|(tor))?$', name, flags=re.IGNORECASE):
            qtag = "AFTOR"
            qlbl = r'Toroidal Angular Frequency'
            qunit = etag + r'rad / s'
            fqlbl = r'$\Omega_{\text{tor}}$'
            fqunit = fetag + r'rad s$^{-1}$'
        elif re.match(r'^af((t)|(tor))?$', name, flags=re.IGNORECASE):
            qtag = "AFTOR"
            qlbl = r'Toroidal Angular Frequency'
            qunit = etag + r'rad / s'
            fqlbl = r'$\Omega_{\text{tor}}$'
            fqunit = fetag + r'rad s$^{-1}$'
        elif re.match(r'^omega((t)|(tor))?$', name, flags=re.IGNORECASE):
            qtag = "AFTOR"
            qlbl = r'Toroidal Angular Frequency'
            qunit = etag + r'rad / s'
            fqlbl = r'$\Omega_{\text{tor}}$'
            fqunit = fetag + r'rad s$^{-1}$'
        elif re.match(r'^v((t)|(tor))?$', name, flags=re.IGNORECASE):
            qtag = "UTOR"
            qlbl = r'Toroidal Flow Velocity'
            qunit = etag + r'm / s'
            fqlbl = r'$u_{\text{tor}}$'
            fqunit = fetag + r'm s$^{-1}$'
        elif re.match(r'^u((t)|(tor))?$', name, flags=re.IGNORECASE):
            qtag = "UTOR"
            qlbl = r'Toroidal Flow Velocity'
            qunit = etag + r'm / s'
            fqlbl = r'$u_{\text{tor}}$'
            fqunit = fetag + r'm s$^{-1}$'
        elif re.match(r'^q$', name, flags=re.IGNORECASE):
            qtag = "Q"
            qlbl = r'Safety Factor'
            qunit = etag + r''
            fqlbl = r'$q$'
            fqunit = fetag + r''
        elif re.match(r'^f?zeffp?$', name, flags=re.IGNORECASE):
            qtag = "ZEFF"
            qlbl = r'Effective Charge'
            qunit = etag + r'e'
            fqlbl = r'$Z_{\text{eff}}$'
            fqunit = fetag + r'e'
        elif re.match(r'^q0$', name, flags=re.IGNORECASE):
            qtag = "Q0"
            qlbl = r'Safety Factor at Axis'
            qunit = etag + r''
            fqlbl = r'$q_0$'
            fqunit = fetag + r''
        elif re.match(r'^q95$', name, flags=re.IGNORECASE):
            qtag = "Q95"
            qlbl = r'Safety Factor at Edge'
            qunit = etag + r''
            fqlbl = r'$q_{95}$'
            fqunit = fetag + r''
        elif re.match(r'^iota$', name, flags=re.IGNORECASE):
            qtag = "IOTA"
            qlbl = r'Rotational Transform'
            qunit = etag + r''
            fqlbl = r'$\iota$'
            fqunit = fetag + r''
        elif re.search(r'^p?rad$', name, flags=re.IGNORECASE):
            qtag = "PRAD"
            qlbl = r'Radiated Power'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{rad}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^qrad$', name, flags=re.IGNORECASE):
            qtag = "QRAD"
            qlbl = r'Radiated Power Density'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{\text{rad}}$'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^p?ohm?$', name, flags=re.IGNORECASE):
            qtag = "POHM"
            qlbl = r'Ohmic Heating Power'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{ohm}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^qohm?$', name, flags=re.IGNORECASE):
            qtag = "QOHM"
            qlbl = r'Ohmic Power Density'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{\text{ohm}}$'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^qthx$', name, flags=re.IGNORECASE):
            qtag = "QEX"
            qlbl = r'Exchange Power Density'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{\text{ex}}$'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^qex$', name, flags=re.IGNORECASE):
            qtag = "QEX"
            qlbl = r'Exchange Power Density'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{\text{ex}}$'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^li3?$', name, flags=re.IGNORECASE):
            qtag = "LI3"
            qlbl = r'Normalized Internal Inductance'
            qunit = etag + r''
            fqlbl = r'$l_{i3}$'
            fqunit = fetag + r''
        elif re.search(r'^p?fus$', name, flags=re.IGNORECASE):
            qtag = "PFUS"
            qlbl = r'Fusion Power'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{fus}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^qfus$', name, flags=re.IGNORECASE):
            qtag = "QFUS"
            qlbl = r'Fusion Power Density'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{\text{fus}}$'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^qfus$', name, flags=re.IGNORECASE):
            qtag = "QFUS"
            qlbl = r'Fusion Power Density'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{\text{fus}}$'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^r?nt$', name, flags=re.IGNORECASE):
            qtag = "NEUT"
            qlbl = r'Total Neutron Rate'
            qunit = etag + r's**-1'
            fqlbl = r'$R_{\text{neut}}$'
            fqunit = fetag + r's$^{-1}$'
        elif re.search(r'^ld(ebye)?$', name, flags=re.IGNORECASE):
            qtag = "LDEBYE"
            qlbl = r'Debye Length'
            qunit = etag + r'm'
            fqlbl = r'$\lambda_{D}$'
            fqunit = fetag + r'm'
        elif re.search(r'^powinj$', name, flags=re.IGNORECASE):
            qtag = "POWINJ"
            qlbl = r'Total Auxiliary Power Injected'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{inj}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^powlss$', name, flags=re.IGNORECASE):
            qtag = "POWLSS"
            qlbl = r'Total Auxiliary Power Lost'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{loss}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^powdep$', name, flags=re.IGNORECASE):
            qtag = "POWDEP"
            qlbl = r'Total Auxiliary Power Coupled'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{dep}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^p(ow)?rad', name, flags=re.IGNORECASE):
            qtag = "POWRAD"
            qlbl = r'Radiated Power'
            qunit = etag + r'W'
            fqlbl = r'$P_{\text{rad}}$'
            fqunit = fetag + r'W'
        elif re.search(r'^taue$', name, flags=re.IGNORECASE):
            qtag = "TAUE"
            qlbl = r'Energy Confinement Time'
            qunit = etag + r's'
            fqlbl = r'$\tau_{E}$'
            fqunit = fetag + r's'
            name = "xxx" + name
        fsrc = False
        if re.search(r'^qe[_\s]?', name, flags=re.IGNORECASE):
            qtag = "STE"
            qlbl = r'Electron Heat Source'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{e'
            fqunit = fetag + r'W m$^{-3}$'
            fsrc = True
        elif re.search(r'^stei$', name, flags=re.IGNORECASE):
            qtag = "STEI"
            qlbl = r'Electron-Ion Exchange Heat Source'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{ei}'
            fqunit = fetag + r'W m$^{-3}$'
        elif re.search(r'^ste[_\s]?', name, flags=re.IGNORECASE):
            qtag = "STE"
            qlbl = r'Electron Heat Source'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{e'
            fqunit = fetag + r'W m$^{-3}$'
            fsrc = True
        elif re.search(r'^qi[_\s]?', name, flags=re.IGNORECASE):
            qtag = "STI"
            qlbl = r'Ion Heat Source'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{i'
            fqunit = fetag + r'W m$^{-3}$'
            fsrc = True
        elif re.search(r'^sti[_\s]?', name, flags=re.IGNORECASE):
            qtag = "STI"
            qlbl = r'Ion Heat Source'
            qunit = etag + r'W m**-3'
            fqlbl = r'$Q_{i'
            fqunit = fetag + r'W m$^{-3}$'
            fsrc = True
        elif re.search(r'^sn?e[_\s]?', name, flags=re.IGNORECASE):
            qtag = "SNE"
            qlbl = r'Electron Particle Source'
            qunit = etag + r'm**-3 s**-1'
            fqlbl = r'$S_{e'
            fqunit = fetag + r'm$^{-3}$ s$^{-1}$'
            fsrc = True
        elif re.search(r'^tau[_\s]?', name, flags=re.IGNORECASE):
            qtag = "SP"
            qlbl = r'Angular Momentum Source'
            qunit = etag + r'N m**-2'
            fqlbl = r'$\tau_{'
            fqunit = fetag + r'N m$^{-2}$'
            fsrc = True
        elif re.search(r'^torq[_\s]?', name, flags=re.IGNORECASE):
            qtag = "SP"
            qlbl = r'Angular Momentum Source'
            qunit = etag + r'N m**-2'
            fqlbl = r'$\tau_{'
            fqunit = fetag + r'N m$^{-2}$'
            fsrc = True
        elif re.search(r'^s[pv][_\s]?', name, flags=re.IGNORECASE):
            qtag = "SP"
            qlbl = r'Angular Momentum Source'
            qunit = etag + r'N m**-2'
            fqlbl = r'$\tau_{'
            fqunit = fetag + r'N m$^{-2}$'
            fsrc = True
        elif re.search(r'^sn?i[0-9]*[_\s]?', name, flags=re.IGNORECASE):
            vv = re.search(r'i([0-9]*)', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "SNI" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Main Ion '+iitag+r'Particle Source'
            qunit = etag + r'm**-3 s**-1'
            fqlbl = r'$S_{i'
            fqunit = fetag + r'm$^{-3}$ s$^{-1}$'
            fsrc = True
        elif re.search(r'^s[_\s]?', name, flags=re.IGNORECASE):
            qtag = "SNI"
            qlbl = r'Ion Particle Source'
            qunit = etag + r'm**-3 s**-1'
            fqlbl = r'$S_{i'
            fqunit = fetag + r'm$^{-3}$ s$^{-1}$'
            fsrc = True
        elif re.search(r'^s?j[_\s]?', name, flags=re.IGNORECASE):
            qtag = "J"
            qlbl = r'Current Density'
            qunit = etag + r'A m**-2'
            fqlbl = r'$J_{'
            fqunit = fetag + r'A m$^{-2}$'
            fsrc = True
        elif re.search(r'^nfi?[_\s]?', name, flags=re.IGNORECASE):
            qtag = "NFI"
            qlbl = r'Fast Ion Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_{f'
            fqunit = fetag + r'm$^{-3}$'
            fsrc = True
        elif re.search(r'^wfi?[_\s]?', name, flags=re.IGNORECASE):
            qtag = "WFI"
            qlbl = r'Fast Ion Energy Density'
            qunit = etag + r'J m**-3'
            fqlbl = r'$W_{f'
            fqunit = fetag + r'J m$^{-3}$'
            fsrc = True
        elif re.search(r'^pfi?[_\s]?', name, flags=re.IGNORECASE):
            qtag = "PFI"
            qlbl = r'Fast Ion Pressure'
            qunit = etag + r'Pa'
            fqlbl = r'$p_{f'
            fqunit = fetag + r'Pa'
            fsrc = True
        elif re.search(r'^powi[_\s]?', name, flags=re.IGNORECASE):
            qtag = "POWI"
            qlbl = r'Power Injected'
            qunit = etag + r'W'
            fqlbl = r'$P_{i'
            fqunit = fetag + r'W'
            fsrc = True
        elif re.search(r'^powl[_\s]?', name, flags=re.IGNORECASE):
            qtag = "POWL"
            qlbl = r'Power Lost'
            qunit = etag + r'W'
            fqlbl = r'$P_{l'
            fqunit = fetag + r'W'
            fsrc = True
        elif re.search(r'^wnbd$', name, flags=re.IGNORECASE):
            qtag = "WFINBI"
            qlbl = r'NBI Fast Ion Energy Density'
            qunit = etag + r'J m**-3'
            fqlbl = r'$W_{f,\text{NBI}}$'
            fqunit = fetag + r'J m$^{-3}$'
        elif re.search(r'^wrfd$', name, flags=re.IGNORECASE):
            qtag = "WFIICRH"
            qlbl = r'ICRH Fast Ion Energy Density'
            qunit = etag + r'J m**-3'
            fqlbl = r'$W_{f,\text{ICRH}}$'
            fqunit = fetag + r'J m$^{-3}$'
        elif re.search(r'^dnbd$', name, flags=re.IGNORECASE):
            qtag = "NFINBI"
            qlbl = r'ICRH Fast Ion Density'
            qunit = etag + r'm**-3'
            fqlbl = r'$n_{f,\text{NBI}}$'
            fqunit = fetag + r'm$^{-3}$'
        elif re.search(r'^sbd$', name, flags=re.IGNORECASE):
            qtag = "SNI"
            qlbl = r'NBI Ion Particle Source'
            qunit = etag + r'm**-3 s**-1'
            fqlbl = r'$S_{i,\text{NBI}}$'
            fqunit = fetag + r'm$^{-3}$ s$^{-1}$'
        if fsrc:
            if re.search(r'nbi$', name, flags=re.IGNORECASE):
                qtag = qtag + "NBI"
                qlbl = r'NBI ' + qlbl
                fqlbl = fqlbl + r',\text{NBI}}$'
            elif re.search(r'ic(r[hf])?$', name, flags=re.IGNORECASE):
                qtag = qtag + "ICRH"
                qlbl = r'ICRH ' + qlbl
                fqlbl = fqlbl + r',\text{IC}}$'
            elif re.search(r'ec(r[hf])?$', name, flags=re.IGNORECASE):
                qtag = qtag + "ECRH"
                qlbl = r'ECRH ' + qlbl
                fqlbl = fqlbl + r',\text{EC}}$'
            elif re.search(r'lh(r[hf])?$', name, flags=re.IGNORECASE):
                qtag = qtag + "LH"
                qlbl = r'LH ' + qlbl
                fqlbl = fqlbl + r',\text{LH}}$'
            elif re.search(r'ohm(ic)?$', name, flags=re.IGNORECASE):
                qtag = qtag + "OHM"
                qlbl = r'Ohmic ' + qlbl
                fqlbl = fqlbl + r',\text{ohm}}$'
            else:
                fqlbl = fqlbl + r'}$'
        ftrans = False
        if re.search(r'^xe[_\s]?', name, flags=re.IGNORECASE):
            qtag = "XE"
            qlbl = r'Electron Heat Diffusivity'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$\chi_{e'
            fqunit = fetag + r'm$^{2}$ s$^{-1}$'
            ftrans = True
        elif re.search(r'^xi[_\s]?', name, flags=re.IGNORECASE):
            qtag = "XI"
            qlbl = r'Ion Heat Diffusivity'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$\chi_{i'
            fqunit = fetag + r'm$^{2}$ s$^{-1}$'
            ftrans = True
        elif re.search(r'^ximp?[0-9]*[_\s]?', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "XIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Heat Diffusivity'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$\chi_{\text{imp}'
            fqunit = fetag + r'm$^{2}$ s$^{-1}$'
            ftrans = True
        elif re.search(r'^de[_\s]?', name, flags=re.IGNORECASE):
            qtag = "DE"
            qlbl = r'Electron Particle Diffusivity'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$D_{e'
            fqunit = fetag + r'm$^{2}$ s$^{-1}$'
            ftrans = True
        elif re.search(r'^di[_\s]?', name, flags=re.IGNORECASE):
            qtag = "DI"
            qlbl = r'Ion Particle Diffusivity'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$D_{i'
            fqunit = fetag + r'm$^{2}$ s$^{-1}$'
            ftrans = True
        elif re.search(r'^dimp?[0-9]*[_\s]?', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "DIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Particle Diffusivity'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$D_{\text{imp}'
            fqunit = fetag + r'm$^{2}$ s$^{-1}$'
            ftrans = True
        elif re.search(r'^ve[_\s]?', name, flags=re.IGNORECASE):
            qtag = "DE"
            qlbl = r'Electron Particle Pinch'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$V_{e'
            fqunit = fetag + r'm s$^{-1}$'
            ftrans = True
        elif re.search(r'^vi[_\s]?', name, flags=re.IGNORECASE):
            qtag = "DI"
            qlbl = r'Ion Particle Pinch'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$V_{i'
            fqunit = fetag + r'm s$^{-1}$'
            ftrans = True
        elif re.search(r'^vimp?[0-9]*[_\s]?', name, flags=re.IGNORECASE):
            vv = re.search(r'([0-9]*)$', name, flags=re.IGNORECASE)
            itag = vv.group(1) if vv else ""
            qtag = "DIMP" + itag
            iitag = itag + " " if itag else itag
            qlbl = r'Impurity Ion '+iitag+r'Particle Pinch'
            qunit = etag + r'm**2 s**-1'
            fqlbl = r'$V_{\text{imp}'
            fqunit = fetag + r'm s$^{-1}$'
            ftrans = True
        if ftrans:
            if re.search(r'turb$', name, flags=re.IGNORECASE):
                qtag = qtag + "TURB"
                qlbl = r'Turbulent ' + qlbl
                fqlbl = fqlbl + r',\text{turb}}$'
            elif re.search(r'nc$', name, flags=re.IGNORECASE):
                qtag = qtag + "NC"
                qlbl = r'Neoclassical ' + qlbl
                fqlbl = fqlbl + r',\text{nc}}$'
            else:
                fqlbl = fqlbl + r'}$'
    if qtag is not None and isinstance(derivative, str):
        (rtag, rlbl, runit, frlbl, frunit) = define_quantity(derivative)
        qtag = qtag + "GRAD"
        qlbl = qlbl + r' Gradient'
        qunit = qunit + r' / ' + runit if runit else qunit
        fqlbl = r'$\nabla_{' + frlbl[1:-1] + r'}\,' + fqlbl[1:-1] + r'$'
        fqunit = fqunit + r'/' + frunit if frunit else fqunit
    return (qtag, qlbl, qunit, fqlbl, fqunit)


def old_coords_to_new(data_dict, top_level=True):
    """
    Function to convert the old-style coordinate labels to
    the new style determined by classes.py. Added for
    backwards compatability with previous versions.

    :arg data_dict: dict. Flattened export dictionary from previous EX2GK versions.

    :kwarg top_level: bool. Not yet implemented.

    :returns: dict. Flattened export dictionary from current EX2GK version.
    """
    if isinstance(data_dict, dict):
        clist = list(data_dict.keys())
        dlist = []
        for key in clist:
            if not re.search(r'BASE$', key):
                skey = key.split("_")
                mm = re.match(r'^([A-Z]*)([VJE])$', skey[-1])
                prefix = mm.group(1).upper() if mm.group(1) else ""
                idstart = 1 if skey[0] == "CD" else 0
                nkey = skey[0] + "_" if skey[0] == "CD" else ""
                for pp in range(idstart, len(skey)-1):
                    nkey = nkey + prefix + skey[pp] + "_"
                nkey = nkey + mm.group(2).upper()
                if nkey != key:
                    data_dict[nkey] = copy.deepcopy(data_dict[key])
                    dlist.append(key)
        for key in dlist:
            del data_dict[key]
    return data_dict


def sep_eq_line(line, float_width=16, floats_per_line=5, sep=' '):
    """ Split a eqdsk-style line and inserts seperator characters """
    splitted = [line[num*float_width:(num+1)*float_width]
                for num in range(floats_per_line)]
    separate = sep.join(splitted)
    return separate


def read_chunk(lines, length, floats_per_line=5):
    """ Read a single chunk (array/vector)

    Reads and pops for `lines` the amount of lines
    containing the to be read vector.

    Args:
        lines:  List of lines to be read. Destructive!
        length: Length of to be read vector

    Kwargs:
        floats_per_line: Amount of floats on a line [Default: 5]
    """
    num_lines = int(np.ceil(length / floats_per_line))
    vals = []
    for line in lines[:num_lines]:
        sep = sep_eq_line(line)
        vals.append(np.fromstring(sep, sep=' '))
    del lines[:num_lines]
    return vals


def read_eqdsk_file(fname):
    """ Read an eqdsk file """
    with open(fname, 'r') as ff:
        lines = ff.readlines()

    case = lines[0][:48].strip()
    header = lines.pop(0)[48:].split()
    eq = {}
    # Read sizes of arrays/vectors
    eq['idum'] = int(header[0])
    eq['nw'] = int(header[1])
    eq['nh'] = int(header[2])

    # Read singles
    eq['rdim'], eq['zdim'], eq['rcentr'], eq['rleft'], eq['zmid'] = \
        np.fromstring(sep_eq_line(lines.pop(0)), sep=' ')
    eq['rmaxis'], eq['zmaxis'], eq['simag'], eq['sibry'], eq['bcentr'] = \
        np.fromstring(sep_eq_line(lines.pop(0)), sep=' ')
    eq['current'], eq['simag2'], eq['dummy1'], eq['rmaxis2'], eq['dummy2'] = \
        np.fromstring(sep_eq_line(lines.pop(0)), sep=' ')
    eq['zmaxis2'], eq['dummy3'], eq['sibry2'], eq['dummy3'], eq['dummy4'] = \
        np.fromstring(sep_eq_line(lines.pop(0)), sep=' ')

    # Remove dummy fields
    for ii in range(1, 5):
        del eq['dummy' + str(ii)]

    # Check if duplicate fields are equal
    for base in ['simag', 'rmaxis', 'zmaxis', 'sibry']:
        if not eq[base] == eq.pop(base + '2'):
            raise Exception("Dual values for '{!s}' not equal!".format(base))

    # Read 1D array blocks
    for name in ['fpol', 'pres', 'ffprim', 'pprime']:
        eq[name] = np.hstack(read_chunk(lines, eq['nw']))

    # Read psi map
    eq['psirz'] = np.hstack(read_chunk(lines, eq['nw'] * eq['nh']))
    eq['psirz'] = eq['psirz'].reshape((eq['nw'], eq['nh']))

    # Read q-profile
    eq['qpsi'] = np.hstack(read_chunk(lines, eq['nw']))

    # Read sizes of boundary vector and limiter vector
    header = lines.pop(0)
    eq['nbbbs'] = int(header[:5])
    eq['limitr'] = int(header[5:])

    # Read boundary vector
    if eq['nbbbs'] > 0:
        bbbs = read_chunk(lines, eq['nbbbs'] * 2)
        bbbs = np.hstack(bbbs).reshape((eq['nbbbs'], 2))
        eq['rbbbs'] = bbbs[:, 0]
        eq['zbbbs'] = bbbs[:, 1]
    else:
        eq['rbbbs'] = None
        eq['zbbbs'] = None

    # Read limiter vector
    if eq['limitr'] > 0:
        lim = read_chunk(lines, eq['limitr'] * 2)
        lim = np.hstack(lim).reshape((eq['limitr'], 2))
        eq['rlim'] = lim[:, 0]
        eq['zlim'] = lim[:, 1]
    else:
        eq['rlim'] = None
        eq['zlim'] = None

    return eq


def write_eqdsk_file(fname, nw, nh, rdim, zdim, rcentr, rleft, zmid, rmaxis, zmaxis, simag, sibry, bcentr, \
                     current, fpol, pres, ffprim, pprime, psirz, qpsi, nbbbs, limitr, rbbbs, zbbbs, rlim, zlim, case='', idum=0):
    """
    Writes provided equilibrium data into the EQDSK format.

    :arg fname: str. Name of the EQDSK file to be generated.

    :arg nw: int. Number of radial points in 2D grid and in 1D profiles, assumed equal to each other.

    :arg nh: int. Number of vertical points in 2D grid.

    :arg rdim: float. Width of 2D grid box, in the radial direction.

    :arg zdim: float. Height of 2D grid box, in the vertical direction.

    :arg rcentr: float. Location of the geometric center of the machine in the radial direction, does not necessarily need to be mid-point in radial direction of 2D grid.

    :arg rleft: float. Location of the left-most point in radial direction (lowest radial value) of 2D grid, needed for grid reconstruction.

    :arg zmid: float. Location of the mid-point in vertical direction of 2D grid, needed for grid reconstruction.

    :arg rmaxis: float. Location of the magnetic center of the equilibrium in the radial direction.

    :arg zmaxis: float. Location of the magnetic center of the equilibrium in the vertical direction.

    :arg simag: float. Value of the poloidal flux at the magnetic center of the equilibrium.

    :arg sibry: float. Value of the poloidal flux at the boundary of the equilibrium, defined as the last closed flux surface and not necessarily corresponding to any edge of the 2D grid.

    :arg bcentr: float. Value of the toroidal magnetic field at the geometric center of the machine, typically provided as the value in vacuum.

    :arg current: float. Value of the total current in the plasma, typically provided as the total current in the toroidal direction.

    :arg fpol: array. Absolute unnormalized poloidal flux as a function of radius.

    :arg pres: array. Total plasma pressure as a function of radius.

    :arg ffprime: array. F * derivative of F with respect to normalized poloidal flux as a function of radius.

    :arg pprime: array. Derivative of plasma pressure with respect to normalized poloidal flux as a function of radius.

    :arg psirz: array. 2D poloidal flux map as a function of radial coordinate and vertical coordinate.

    :arg qpsi: array. Safety factor as a function of radius.

    :arg nbbbs: int. Number of points in description of plasma boundary contour, can be zero.

    :arg limitr: int. Number of points in description of plasma limiter contour, can be zero.

    :arg rbbbs: array. Ordered list of radial values corresponding to points in the plasma boundary contour description.

    :arg zbbbs: array. Ordered list of vertical values corresponding to points in the plasma boundary contour description.

    :arg rlim: array. Ordered list of radial values corresponding to points in the plasma boundary contour description.

    :arg zlim: array. Ordered list of vertical values corresponding to points in the plasma boundary contour description.

    :kwarg case: str. String to identify file, non-essential and written into the 48 character space at the start of the file.

    :kwarg idum: int. Dummy integer value to identify file origin, non-essential and is sometimes used to identify the FORTRAN output number.

    :returns: none.
    """
    if not isinstance(fname, str):
        raise TypeError("fname field must be a string. EQDSK file write aborted.")
    if not isinstance(nw, int):
        raise TypeError("nw field must be an integer. EQDSK file write aborted.")
    if not isinstance(nh, int):
        raise TypeError("nh field must be an integer. EQDSK file write aborted.")
    if not isinstance(rdim, float):
        raise TypeError("rdim field must be a real number. EQDSK file write aborted.")
    if not isinstance(zdim, float):
        raise TypeError("zdim field must be a real number. EQDSK file write aborted.")
    if not isinstance(rcentr, float):
        raise TypeError("rcentr field must be a real number. EQDSK file write aborted.")
    if not isinstance(rleft, float):
        raise TypeError("rleft field must be a real number. EQDSK file write aborted.")
    if not isinstance(zmid, float):
        raise TypeError("zmid field must be a real number. EQDSK file write aborted.")
    if not isinstance(rmaxis, float):
        raise TypeError("rmaxis field must be a real number. EQDSK file write aborted.")
    if not isinstance(zmaxis, float):
        raise TypeError("zmaxis field must be a real number. EQDSK file write aborted.")
    if not isinstance(simag, float):
        raise TypeError("simag field must be a real number. EQDSK file write aborted.")
    if not isinstance(sibry, float):
        raise TypeError("sibry field must be a real number. EQDSK file write aborted.")
    if not isinstance(bcentr, float):
        raise TypeError("bcentr field must be a real number. EQDSK file write aborted.")
    if not isinstance(current, float):
        raise TypeError("current field must be a real number. EQDSK file write aborted.")
    if not isinstance(fpol, array_types):
        raise TypeError("fpol field must be an integer. EQDSK file write aborted.")
    if not isinstance(pres, array_types):
        raise TypeError("pres field must be an integer. EQDSK file write aborted.")
    if not isinstance(ffprim, array_types):
        raise TypeError("ffprim field must be an integer. EQDSK file write aborted.")
    if not isinstance(pprime, array_types):
        raise TypeError("pprime field must be an integer. EQDSK file write aborted.")
    if not isinstance(psirz, array_types):
        raise TypeError("psirz field must be an integer. EQDSK file write aborted.")
    if not isinstance(qpsi, array_types):
        raise TypeError("qpsi field must be an integer. EQDSK file write aborted.")
    if nbbbs is not None and not isinstance(nbbbs, int):
        raise TypeError("nbbbs field must be an integer or set to None. EQDSK file write aborted.")
    if limitr is not None and not isinstance(limitr, int):
        raise TypeError("limitr field must be an integer or set to None. EQDSK file write aborted.")
    if rbbbs is not None and not isinstance(rbbbs, array_types):
        raise TypeError("rbbbs field must be an integer. EQDSK file write aborted.")
    if zbbbs is not None and not isinstance(zbbbs, array_types):
        raise TypeError("zbbbs field must be an integer. EQDSK file write aborted.")
    if rlim is not None and not isinstance(rlim, array_types):
        raise TypeError("rlim field must be an integer. EQDSK file write aborted.")
    if zlim is not None and not isinstance(zlim, array_types):
        raise TypeError("zlim field must be an integer. EQDSK file write aborted.")
    if not isinstance(case, str):
        raise TypeError("case field must be a string. EQDSK file write aborted.")
    if not isinstance(idum, int):
        raise TypeError("idum field must be an integer. EQDSK file write aborted.")
    if os.path.exists(fname):
        print("%s exists, overwriting file with EQDSK file!" % (fname))
    if nbbbs is None or rbbbs is None or zbbbs is None:
        nbbbs = 0
        rbbbs = []
        zbbbs = []
    if limitr is None or rlim is None or zlim is None:
        limitr = 0
        rlim = []
        zlim = []
    with open(fname, 'w') as ff:
        gcase = case[:48] if len(case) > 48 else case
        ff.write("%-48s%4d%4d%4d\n" % (gcase, idum, nw, nh))
        ff.write("%16.9E%16.9E%16.9E%16.9E%16.9E\n" % (rdim, zdim, rcentr, rleft, zmid))
        ff.write("%16.9E%16.9E%16.9E%16.9E%16.9E\n" % (rmaxis, zmaxis, simag, sibry, bcentr))
        ff.write("%16.9E%16.9E%16.9E%16.9E%16.9E\n" % (current, simag, 0.0, rmaxis, 0.0))
        ff.write("%16.9E%16.9E%16.9E%16.9E%16.9E\n" % (zmaxis, 0.0, sibry, 0.0, 0.0))
        for ii in range(0, len(fpol)):
            ff.write("%16.9E" % (fpol[ii]))
            if (ii + 1) % 5 == 0 and (ii + 1) != len(fpol):
                ff.write("\n")
        ff.write("\n")
        for ii in range(0, len(pres)):
            ff.write("%16.9E" % (pres[ii]))
            if (ii + 1) % 5 == 0 and (ii + 1) != len(pres):
                ff.write("\n")
        ff.write("\n")
        for ii in range(0, len(ffprim)):
            ff.write("%16.9E" % (ffprim[ii]))
            if (ii + 1) % 5 == 0 and (ii + 1) != len(ffprim):
                ff.write("\n")
        ff.write("\n")
        for ii in range(0, len(pprime)):
            ff.write("%16.9E" % (pprime[ii]))
            if (ii + 1) % 5 == 0 and (ii + 1) != len(pprime):
                ff.write("\n")
        ff.write("\n")
        kk = 0
        for ii in range(0, nh):
            for jj in range(0, nw):
                ff.write("%16.9E" % (psirz[ii, jj]))
                if (kk + 1) % 5 == 0 and (kk + 1) != nh * nw:
                    ff.write("\n")
                kk = kk + 1
        ff.write("\n")
        for ii in range(0, len(qpsi)):
            ff.write("%16.9E" % (qpsi[ii]))
            if (ii + 1) % 5 == 0 and (ii + 1) != len(qpsi):
                ff.write("\n")
        ff.write("\n")
        ff.write("%5d%5d\n" % (nbbbs, limitr))
        kk = 0
        for ii in range(0, nbbbs):
            ff.write("%16.9E" % (rbbbs[ii]))
            if (kk + 1) % 5 == 0 and (ii + 1) != nbbbs:
                ff.write("\n")
            kk = kk + 1
            ff.write("%16.9E" % (zbbbs[ii]))
            if (kk + 1) % 5 == 0 and (ii + 1) != nbbbs:
                ff.write("\n")
            kk = kk + 1
        ff.write("\n")
        kk = 0
        for ii in range(0, limitr):
            ff.write("%16.9E" % (rlim[ii]))
            if (kk + 1) % 5 == 0 and (kk + 1) != limitr:
                ff.write("\n")
            kk = kk + 1
            ff.write("%16.9E" % (zlim[ii]))
            if (kk + 1) % 5 == 0 and (kk + 1) != limitr:
                ff.write("\n")
            kk = kk + 1
        ff.write("\n")
    print('Output EQDSK file saved as %s.' % (fname))
