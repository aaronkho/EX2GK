# Functions to convert MATLAB mat structures to Python dictionaries and vice versa
# Developed by Aaron Ho - 22/02/2017

# Required imports
import os
import re
import numpy as np
import scipy.io as sio
import pickle


def pickle_this(obj, filename, filedir='./'):
    """
    Writes a Python object into a Python-style binary file, or pickle.
    Only truly useful for those not fluent in Python pickling.

    :arg obj: object. Python object to be pickled.

    :arg filename: str. Name of the pickle binary file to be created.

    :kwarg filedir: str. Optional path to the directory which pickle file will be stored in. Default is pwd.

    :returns: int. Success flag. 0 = success, 1 otherwise
    """
    status = 1
    fdir = copy.deepcopy(filedir)
    if isinstance(fdir, str) and not fdir.endswith('/'):
        fdir = fdir + '/'
    if os.path.isdir(fdir):
        if not re.search(r'\.p(kl)?$', filename):
            filename = filename + '.pkl'
        with open(fdir + filename, 'wb') as bf:
            pickle.dump(obj, bf, protocol=3)
            status = 0
    return status


def unpickle_this(filepath, filedir=None):
    """
    Loads a Python-style binary file, or pickle, into memory.
    Only truly useful for those not fluent in Python pickling.

    :arg filepath: str. Name of the pickle binary file to be loaded.

    :kwarg filedir: str. Optional path to the directory which pickle file is stored in. Default is pwd.

    :returns: obj. The Python object stored in the specified pickle file.
    """
    obj = None
    fdir = copy.deepcopy(filedir) if isinstance(filedir, str) else ''
    if fdir and not fdir.endswith('/'):
        fdir = fdir + '/' 
    if os.path.isfile(fdir+filepath):
        with open(fdir+filepath, 'rb') as bf:
            obj = pickle.load(bf)
    return obj


def _deconstruct(matobj):
    """
    Private function. Reassembles the direct conversion from .mat into a more easily
    navigatable structure, while conserving the hierarchy and nested structures in the
    original format.

    :arg matobj: obj. Python object generated from scipy.io.loadmat function.

    :returns: obj. Reassembled and cleaned version of input Python object.
    """
    pyobj = None
    mnames = matobj.dtype.names
    if mnames is not None:
        pyobj = dict()
        for name in mnames:
            pyobj[name] = _deconstruct(matobj[name][0][0])
    else:
        pyobj = matobj.copy()
    return pyobj


def mat2py(matfile, pyfname=None):
    """
    Loads data stored in a .mat file into memory as a Python object.
    
    :arg matfile: str. Name of .mat file to be loaded.

    :kwarg pyfname: str. Optional argument specifying the file name to save equivalent Python object.

    :returns: obj. Loaded data from .mat file as a Python object.
    """
    spy = None
    if os.path.isfile(matfile):
        spy = dict()
        smat = sio.loadmat(matfile)
        mkeys = [k for k in smat.keys() if not re.search(r'__$', k)]
        for key in mkeys:
            spy[key] = _deconstruct(smat[key])
        if pyfname is not None:
            newname = pyfname
            if not re.search(r'\.p(kl)?$', newname):
                newname = newname + '.pkl'
            pickle_this(spy, newname)
            print('Output Python object dumped into %s.' % (newname))
    else:
        print('File %s not found.' % (matfile))
    return spy


def obj2mat(pyobj, matfname):
    """
    Converts data stored in a Python object into a .mat file, retaining the hieracrhy and
    and nested structures.
    
    :arg pyobj: dict. Python object to be saved in .mat file, typically a dictionary.

    :arg matfname: str. Optional argument specifying the file name to save equivalent Python object.

    :returns: none.
    """
    if isinstance(pyobj, dict):
        pkeys = pyobj.keys()
        newname = matfname
        if not re.search(r'\.mat$', newname):
            newname = newname + '.mat'
        sio.savemat(newname, pyobj)
        print('Output MATLAB object saved in %s.' % (newname))
    else:
        print('Invalid Python object passed to converter.')


def py2mat(pyfile, matfname):
    """
    Converts data stored in a Python pickle file into a .mat file, retaining the hieracrhy and
    and nested structures.
    
    :arg pyfile: str. Name of pickle file to be loaded.

    :arg matfname: str. Optional argument specifying the file name to save equivalent Python object.

    :returns: none.
    """
    if os.path.isfile(pyfile):
        spy = unpickle_this(pyfile)
        obj2mat(spy, matfname)
    else:
        print('File %s not found.' % (pyfile))

def obj2xmls(pyobj):
    """
    Converts native Python objects into XML strings, only handles strings, floats, integers, lists,
    tuples, numpy arrays, and dicts.

    :arg pyobj: dict. Python object to be converted into XML string, typically a dictionary.

    :returns: str. XML string representation of input object.
    """
    xmls = ''
    if isinstance(pyobj, str):
        xmls += pyobj
    elif isinstance(pyobj, (float, int)):
        xmls += repr(pyobj)
    elif isinstance(pyobj, (list, tuple, np.ndarray)):
        for ii in range(len(pyobj)):
            istr = obj2xmls(pyobj[ii])
            xmls += '<element>' + istr + '</element>'
    elif isinstance(pyobj, dict):
        for key in pyobj:
            kstr = obj2xmls(pyobj[key])
            xmls += '<' + key.lower() + '>' + kstr + '</' + key.lower() + '>'
    return xmls
