# Functions for converting EX2GK class structure to IMAS IDS structure
# Developer: Aaron Ho - 29/10/2023

# Required imports
import os
import sys
import copy
from operator import itemgetter
from collections import OrderedDict
from pathlib import Path
import re
import json
import datetime
from collections.abc import MutableMapping
from packaging import version
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz

# Internal package imports
from EX2GK import __version__
from EX2GK.tools.general import classes, proctools as ptools, phystools as qtools, fmt_converter as fmtconv

number_types = (int, float,                                # Built-in types
                np.int8, np.int16, np.int32, np.int64,     # Signed integer types
                np.uint8, np.uint16, np.uint32, np.uint64, # Unsigned integer types
                np.float16, np.float32, np.float64         # Floating point decimal types
               )

array_types = (list, tuple,                                # Built-in types
               np.ndarray,                                 # numpy array
               pd.Series                                   # pandas array
              )

min_imas_version_str = "3.28.0"
min_imasal_version_str = "4.7.2"
min_imas_version = version.parse(min_imas_version_str)
min_imasal_version = version.parse(min_imasal_version_str)
imas_source_name_translation = {"NBI": "nbi",
                                "ICRH": "ic",
                                "ECRH": "ec",
                                "LH": "lh",
                                "OHM": "ohmic",
                                "RAD": "radiation"
                               }
imas_source_type_index = {"unspecified": (0, "Unspecified source type"),
                          "nbi": (2, "Source from Neutral Beam Injection"),
                          "ic": (5, "Sources from heating at the ion cyclotron range of frequencies"),
                          "ec": (3, "Sources from electron cyclotron heating and current drive"),
                          "lh": (4, "Sources from lower hybrid heating and current drive"),
                          "ohmic": (7, "Source from ohmic heating"),
                          "radiation": (200, "Total radiation source; radiation losses are negative sources")
                         }
imas_wall_type_index = {0: ("unspecified", "unspecified"),
                        1: ("C", "Carbon"),
                        2: ("W", "Tungsten"),
                        3: ("C_W_coating", "Carbon with tungsten coating"),
                        4: ("SS", "Stainless steel"),
                        5: ("SS_C_coating", "Stainless steel with carbon coating"),
                        6: ("IN", "Inconel"),
                        7: ("IN_C_coating", "Inconel with carbon coating"),
                        8: ("B_C", "Boron carbide"),
                        9: ("Ti_C_coating", "Titanium with carbon coating"),
                        10: ("Be", "Beryllium")
                       }
ids_provenance_max_length = 7    # Maximum determined by HDF5 backend inside UAL - but why?


def insert_into_generic_ids_structure(struct, data):
    if isinstance(data, dict):
        for key, val in data.items():
            container = getattr(struct, key, None)
            if container is not None:
                if isinstance(val, dict):
                    container = insert_into_generic_ids_structure(container, val)
                elif isinstance(val, list):
                    if "getAoSElement" in dir(container):
                        for item in val:
                            new_struct = container.getAoSElement()
                            item_struct = insert_into_generic_ids_structure(new_struct, item)
                            container.append(item_struct)
                    else:
                        for item in val:
                            container.append(item)
                else:
                    container = val
                setattr(struct, key, container)
    return struct


def add_ids_dict_value(struct, field, values=None, upper_errors=None, lower_errors=None, dd_version=min_imas_version):
    error_version_switch = "3.38.0"
    if isinstance(struct, dict) and values is not None:
        struct[field] = copy.deepcopy(values)
        if upper_errors is not None:
            upper_field = field + "_error_upper"
            uerrs = upper_errors if dd_version >= version.parse(error_version_switch) else values + upper_errors
            struct[upper_field] = copy.deepcopy(uerrs)
            if lower_errors is not None and np.all((upper_errors - lower_errors) == 0.0):
                lower_field = field + "_error_lower"
                lerrs = lower_errors if dd_version >= version.parse(error_version_switch) else values - lower_errors
                struct[lower_field] = copy.deepcopy(lerrs)
    return struct


def pass_pulse_schedule_ids_time_slice_values(cls, user=None, dd_version=min_imas_version):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        #machine = cls["META"]["DEVICE"]
        tm = cls["META"]["T2"]
        tw = cls["META"]["T2"] - cls["META"]["T1"]
        ids_time = float(tm) - 0.5 * float(tw)

        # Toroidal field control target, for use in JINTRAC dynamic simulations
        tfstruct = {}
        tf_prov = {}
        if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None and "RVAC" in cls["ZD"] and cls["ZD"]["RVAC"][""] is not None:

            btstruct = {}
            bt_prov = {}

            attr = "reference"
            valstruct = {}
            tag = "data"
            bvac = cls["ZD"]["BVAC"][""]
            bvac_err = cls["ZD"]["BVAC"]["EB"]
            rvac = cls["ZD"]["RVAC"][""]
            #rvac_err = cls["ZD"]["RVAC"]["EB"]
            val = np.array([bvac * rvac])
            err = np.array([bvac_err * rvac])
            lerr = None
            if float(np.abs(val) - 3.0 * err) <= 0.0:
                lerr = np.abs(val) / 3.0
            if float(np.abs(val)) >= 0.0:
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            else:
                if lerr is None:
                    lerr = copy.deepcopy(err)
                    err = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=lerr, lower_errors=err, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            btstruct[attr] = valstruct
            prov = None
            if "PROV" in cls["ZD"]["BVAC"]:
                prov = prov + "; " + cls["ZD"]["BVAC"]["PROV"] if prov is not None else cls["ZD"]["BVAC"]["PROV"]
            if "PROV" in cls["ZD"]["RVAC"]:
                prov = prov + "; " + cls["ZD"]["RVAC"]["PROV"] if prov is not None else cls["ZD"]["RVAC"]["PROV"]
            if prov is not None:
                bt_prov[attr + "/" + tag] = prov

            structkey = "b_field_tor_vacuum_r"
            tfstruct[structkey] = btstruct
            for key, val in bt_prov.items():
                tf_prov[structkey + "/" + key] = val

        if tfstruct:
            structkey = "tf"
            idsd[structkey] = tfstruct
            for key, val in tf_prov.items():
                idsp[structkey + "/" + key] = val

        # Plasma current control target, for use in JINTRAC dynamic simulations
        fcstruct = {}
        fc_prov = {}
        if "IPLA" in cls["ZD"] and cls["ZD"]["IPLA"][""] is not None:

            ipstruct = {}
            ip_prov = {}

            attr = "reference"
            valstruct = {}
            tag = "data"
            val = np.array([cls["ZD"]["IPLA"][""]])
            err = np.array([cls["ZD"]["IPLA"]["EB"]])
            lerr = None
            if float(np.abs(val) - 3.0 * err) <= 0.0:
                lerr = np.abs(val) / 3.0
                if float(np.abs(val)) < 0.0:
                    lerr = copy.deepcopy(err)
                    err = np.abs(val) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            ipstruct[attr] = valstruct
            if "PROV" in cls["ZD"]["IPLA"]:
                ip_prov[attr + "/" + tag] = cls["ZD"]["IPLA"]["PROV"]

            structkey = "i_plasma"
            fcstruct[structkey] = ipstruct
            for key, val in ip_prov.items():
                fc_prov[structkey + "/" + key] = val

        if fcstruct:
            structkey = "flux_control"
            idsd[structkey] = fcstruct
            for key, val in fc_prov.items():
                idsp[structkey + "/" + key] = val

        # Use nbi IDS for expansion into individual PINI descriptions, if necessary
        nbstruct = {}
        nb_prov = {}
        if "POWINBI" in cls["ZD"] and cls["ZD"]["POWINBI"][""] is not None:

            powstruct = {}
            pow_prov = {}

            attr = "reference"
            valstruct = {}
            tag = "data"
            val = np.array([cls["ZD"]["POWINBI"][""]])
            err = np.array([cls["ZD"]["POWINBI"]["EB"]])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            powstruct[attr] = valstruct
            if "PROV" in cls["ZD"]["POWINBI"]:
                pow_prov[attr + "/" + tag] = cls["ZD"]["POWINBI"]["PROV"]

            structkey = "power"
            nbstruct[structkey] = powstruct
            for key, val in pow_prov.items():
                nb_prov[structkey + "/" + key] = val

        if nbstruct:
            structkey = "nbi"
            idsd[structkey] = nbstruct
            for key, val in nb_prov.items():
                idsp[structkey + "/" + key] = val

        # Use ec.launcher for expansion into multiple antenna descriptions, if necessary
        ecstruct = {}
        ec_prov = {}
        if "POWIECRH" in cls["ZD"] and cls["ZD"]["POWIECRH"][""] is not None:

            powstruct = {}
            pow_prov = {}

            attr = "reference"
            valstruct = {}
            tag = "data"
            val = np.array([cls["ZD"]["POWIECRH"][""]])
            err = np.array([cls["ZD"]["POWIECRH"]["EB"]])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            powstruct[attr] = valstruct
            if "PROV" in cls["ZD"]["POWIECRH"]:
                pow_prov[attr + "/" + tag] = cls["ZD"]["POWIECRH"]["PROV"]

            structkey = "power"
            ecstruct[structkey] = powstruct
            for key, val in pow_prov.items():
                ec_prov[structkey + "/" + key] = val

        if ecstruct:
            structkey = "ec"
            idsd[structkey] = ecstruct
            for key, val in ec_prov.items():
                idsp[structkey + "/" + key] = val

        # Use lh.antenna for expansion into multiple antenna descriptions, if necessary
        lhstruct = {}
        lh_prov = {}
        if "POWILH" in cls["ZD"] and cls["ZD"]["POWILH"][""] is not None:

            powstruct = {}
            pow_prov = {}

            attr = "reference"
            valstruct = {}
            tag = "data"
            val = np.array([cls["ZD"]["POWILH"][""]])
            err = np.array([cls["ZD"]["POWILH"]["EB"]])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            powstruct[attr] = valstruct
            if "PROV" in cls["ZD"]["POWILH"]:
                pow_prov[attr + "/" + tag] = cls["ZD"]["POWILH"]["PROV"]

            structkey = "power"
            lhstruct[structkey] = powstruct
            for key, val in pow_prov.items():
                lh_prov[structkey + "/" + key] = val

        if lhstruct:
            structkey = "lh"
            idsd[structkey] = lhstruct
            for key, val in lh_prov.items():
                idsp[structkey + "/" + key] = val

        # Use ic.antenna for expansion into multiple antenna descriptions, if necessary
        icstruct = {}
        ic_prov = {}
        if "POWIICRH" in cls["ZD"] and cls["ZD"]["POWIICRH"][""] is not None:

            # General power field not present for ICRH - why?
            antstruct = {}
            ant_prov = {}

            powstruct = {}
            pow_prov = {}

            attr = "reference"
            valstruct = {}
            tag = "data"
            val = np.array([cls["ZD"]["POWIICRH"][""]])
            err = np.array([cls["ZD"]["POWIICRH"]["EB"]])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            powstruct[attr] = valstruct
            if "PROV" in cls["ZD"]["POWIICRH"]:
                pow_prov[attr + "/" + tag] = cls["ZD"]["POWIICRH"]["PROV"]

            structkey = "power"
            antstruct[structkey] = powstruct
            for key, val in pow_prov.items():
                ant_prov[structkey + "/" + key] = val

            structkey = "antenna"
            if structkey not in icstruct:
                icstruct[structkey] = []
            icstruct[structkey].append(antstruct)
            ii = len(icstruct[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in ant_prov.items():
                ic_prov[structkey + istr + "/" + key] = val

        if icstruct:
            structkey = "ic"
            idsd[structkey] = icstruct
            for key, val in ic_prov.items():
                idsp[structkey + "/" + key] = val

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            idsd["time"] = np.array([ids_time])

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd

def pass_summary_ids_time_slice_values(cls, user=None, dd_version=min_imas_version, use_preset_impurities=False):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        cs_prefix = cls["META"]["CSOP"]
        hmodeflag = 1 if cls["FLAG"]["HMODE"] else 0

        r_minor = None
        rprov = None
        if "RMINORA" in cls["CD"][cs_prefix]:
            rmin = itemgetter(0)(cls["CD"].convert(np.array([1.0]), "RHOTORN", "RMINORA", cs_prefix, cs_prefix))
            r_minor = float(rmin)
            rprov = "EX2GK: CD_"+cs_prefix+"RMINORA"
        elif "RMINORO" in cls["CD"][cs_prefix] and "RMINORI" in cls["CD"][cs_prefix]:
            rmini = itemgetter(0)(cls["CD"].convert(np.array([1.0]), "RHOTORN", "RMINORI", cs_prefix, cs_prefix))
            rmino = itemgetter(0)(cls["CD"].convert(np.array([1.0]), "RHOTORN", "RMINORO", cs_prefix, cs_prefix))
            r_minor = float(rmino + rmini) / 2.0
            rprov = "EX2GK: CD_"+cs_prefix+"RMINORO; EX2GK: CD_"+cs_prefix+"RMINORI"

        # Determine wall material, if present from experimental data
        wallidx = 0
        if cls["META"]["MATWALL"] == "Be":
            wallidx = 10
        elif cls["META"]["MATWALL"] == "W":
            wallidx = 2
        elif cls["META"]["MATWALL"] == "C":
            wallidx = 1
        idx = wallidx if wallidx in imas_wall_type_index else 0
        (short_name, long_name) = imas_wall_type_index[idx]
        idsd["wall"] = {"material": {"index": idx, "name": short_name, "description": long_name}}

        axis = {}
        axis_prov = {}

        posstruct = {}
        pos_prov = {}
        rhotorn_axis = np.array([0.0])
        if rhotorn_axis is not None:
            attr = "rho_tor_norm"
            posstruct[attr] = rhotorn_axis
            pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"RHOTORN"
        rho_axis = itemgetter(0)(cls["CD"].convert(rhotorn_axis, "RHOTORN", "PSITOR", cs_prefix, cs_prefix))
        if rho_axis is not None and "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"]["EB"] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "rho_tor"
            posstruct[attr] = np.sqrt(np.abs(2.0 * rho_axis / cls["ZD"]["BVAC"][""]))
            pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"PSITOR; EX2GK: ZD_BVAC"
        psi_axis = itemgetter(0)(cls["CD"].convert(rhotorn_axis, "RHOTORN", "PSIPOL", cs_prefix, cs_prefix))
        if psi_axis is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "psi"
            posstruct[attr] = 2.0 * np.pi * psi_axis
            pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"PSIPOL"
        if "RMAG" in cls["ZD"] and cls["ZD"]["RMAG"][""] is not None:
            attr = "r"
            posstruct[attr] = np.array([cls["ZD"]["RMAG"][""]])
            if "PROV" in cls["ZD"]["RMAG"] and cls["ZD"]["RMAG"]["PROV"] is not None:
                pos_prov[attr] = cls["ZD"]["RMAG"]["PROV"]
        if "ZMAG" in cls["ZD"] and cls["ZD"]["ZMAG"][""] is not None:
            attr = "z"
            posstruct[attr] = np.array([cls["ZD"]["ZMAG"][""]])
            if "PROV" in cls["ZD"]["ZMAG"] and cls["ZD"]["ZMAG"]["PROV"] is not None:
                pos_prov[attr] = cls["ZD"]["ZMAG"]["PROV"]
        if posstruct:
            structkey = "position"
            axis[structkey] = posstruct
            for key, val in pos_prov.items():
                axis_prov[structkey + "/" + key] = val

        x_axis = itemgetter(0)(cls["CD"].convert(rhotorn_axis, "RHOTORN", cls["META"]["CSO"], cs_prefix, cs_prefix))
        if "TE" in cls["PD"]:
            attr = "t_e"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "TE")
            err = cls["PD"].interpolate(x_axis, "X", "TE", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["TE"] and cls["PD"]["TE"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["TE"]["PROV"]
        if "TI1" in cls["PD"]:
            attr = "t_i_average"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "TI1")
            err = cls["PD"].interpolate(x_axis, "X", "TI1", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["TI1"] and cls["PD"]["TI1"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["TI1"]["PROV"]
        if "NE" in cls["PD"]:
            attr = "n_e"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "NE")
            err = cls["PD"].interpolate(x_axis, "X", "NE", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["NE"] and cls["PD"]["NE"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["NE"]["PROV"]
        if "NI1" in cls["PD"] and not use_preset_impurities:
            attr = "n_i_total"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "NI1")
            err = cls["PD"].interpolate(x_axis, "X", "NI1", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["NI1"] and cls["PD"]["NI1"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["NI1"]["PROV"]
        if "ZEFFP" in cls["PD"] and not cls["FLAG"].checkFlag("FLATZEFF"):
            attr = "zeff"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "ZEFFP")
            err = cls["PD"].interpolate(x_axis, "X", "ZEFFP", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) < 1.0:
                lerr = (val - 1.0) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["ZEFFP"] and cls["PD"]["ZEFFP"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["ZEFFP"]["PROV"]
        elif "ZEFF" in cls["PD"]:
            attr = "zeff"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "ZEFF")
            err = cls["PD"].interpolate(x_axis, "X", "ZEFF", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) < 1.0:
                lerr = (val - 1.0) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["ZEFF"] and cls["PD"]["ZEFF"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["ZEFF"]["PROV"]
        if "Q"+cs_prefix in cls["PD"]:
            attr = "q"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "Q"+cs_prefix)
            err = cls["PD"].interpolate(x_axis, "X", "Q"+cs_prefix, error_flag=True)
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["Q"+cs_prefix] and cls["PD"]["Q"+cs_prefix]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["Q"+cs_prefix]["PROV"]
        if "SH"+cs_prefix in cls["PD"]:
            attr = "magnetic_shear"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_axis, "X", "SH"+cs_prefix)
            err = cls["PD"].interpolate(x_axis, "X", "SH"+cs_prefix, error_flag=True)
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["PD"]["SH"+cs_prefix] and cls["PD"]["SH"+cs_prefix]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["PD"]["SH"+cs_prefix]["PROV"]
        if "BMAG" in cls["ZD"] and cls["ZD"]["BMAG"][""] is not None:
            attr = "b_field"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BMAG"][""]])
            err = np.array([cls["ZD"]["BMAG"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            axis[attr] = valstruct
            if "PROV" in cls["ZD"]["BMAG"] and cls["ZD"]["BMAG"]["PROV"] is not None:
                axis_prov[attr + "/" + tag] = cls["ZD"]["BMAG"]["PROV"]

        separatrix = {}
        separatrix_prov = {}

        posstruct = {}
        pos_prov = {}
        rhotorn_sep = np.array([1.0])
        if rhotorn_sep is not None:
            attr = "rho_tor_norm"
            posstruct[attr] = rhotorn_sep
            pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"RHOTORN"
        rho_sep = itemgetter(0)(cls["CD"].convert(rhotorn_sep, "RHOTORN", "PSITOR", cs_prefix, cs_prefix))
        if rho_sep is not None and "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"]["EB"] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "rho_tor"
            posstruct[attr] = np.sqrt(np.abs(2.0 * rho_sep / cls["ZD"]["BVAC"][""]))
            pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"PSITOR; EX2GK: ZD_BVAC"
        psi_sep = itemgetter(0)(cls["CD"].convert(rhotorn_sep, "RHOTORN", "PSIPOL", cs_prefix, cs_prefix))
        if psi_sep is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "psi"
            posstruct[attr] = 2.0 * np.pi * psi_sep
            pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"PSIPOL"
        if posstruct:
            structkey = "position"
            separatrix[structkey] = posstruct
            for key, val in pos_prov.items():
                separatrix_prov[structkey + "/" + key] = val

        x_sep = itemgetter(0)(cls["CD"].convert(rhotorn_sep, "RHOTORN", cls["META"]["CSO"], cs_prefix, cs_prefix))
        if "TE" in cls["PD"]:
            attr = "t_e"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "TE")
            err = cls["PD"].interpolate(x_sep, "X", "TE", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["TE"] and cls["PD"]["TE"]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["TE"]["PROV"]
        if "TI1" in cls["PD"]:
            attr = "t_i_average"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "TI1")
            err = cls["PD"].interpolate(x_sep, "X", "TI1", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["TI1"] and cls["PD"]["TI1"]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["TI1"]["PROV"]
        if "NE" in cls["PD"]:
            attr = "n_e"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "NE")
            err = cls["PD"].interpolate(x_sep, "X", "NE", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["NE"] and cls["PD"]["NE"]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["NE"]["PROV"]
        if "NI1" in cls["PD"] and not use_preset_impurities:
            attr = "n_i_total"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "NI1")
            err = cls["PD"].interpolate(x_sep, "X", "NI1", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["NI1"] and cls["PD"]["NI1"]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["NI1"]["PROV"]
        if "ZEFFP" in cls["PD"] and not cls["FLAG"].checkFlag("FLATZEFF"):
            attr = "zeff"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "ZEFFP")
            err = cls["PD"].interpolate(x_sep, "X", "ZEFFP", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 1.0:
                lerr = (val - 1.0) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["ZEFFP"] and cls["PD"]["ZEFFP"]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["ZEFFP"]["PROV"]
        elif "ZEFF" in cls["PD"]:
            attr = "zeff"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "ZEFF")
            err = cls["PD"].interpolate(x_sep, "X", "ZEFF", error_flag=True)
            lerr = None
            if float(val - 3.0 * err) <= 1.0:
                lerr = (val - 1.0) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["ZEFF"] and cls["PD"]["ZEFF"]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["ZEFF"]["PROV"]
        if "Q"+cs_prefix in cls["PD"]:
            attr = "q"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "Q"+cs_prefix)
            err = cls["PD"].interpolate(x_sep, "X", "Q"+cs_prefix, error_flag=True)
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["Q"+cs_prefix] and cls["PD"]["Q"+cs_prefix]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["Q"+cs_prefix]["PROV"]
        if "SH"+cs_prefix in cls["PD"]:
            attr = "magnetic_shear"
            valstruct = {}
            tag = "value"
            val = cls["PD"].interpolate(x_sep, "X", "SH"+cs_prefix)
            err = cls["PD"].interpolate(x_sep, "X", "SH"+cs_prefix, error_flag=True)
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            separatrix[attr] = valstruct
            if "PROV" in cls["PD"]["SH"+cs_prefix] and cls["PD"]["SH"+cs_prefix]["PROV"] is not None:
                separatrix_prov[attr + "/" + tag] = cls["PD"]["SH"+cs_prefix]["PROV"]

        pedestal = {}
        pedestal_prov = {}

        if hmodeflag != 0 and "RHOPEDTOP" in cls["ZD"] and cls["ZD"]["RHOPEDTOP"][""] is not None:
            posstruct = {}
            pos_prov = {}
            rhotorn_pedtop = np.array([cls["ZD"]["RHOPEDTOP"][""]])
            if rhotorn_pedtop is None:
                attr = "rho_tor_norm"
                posstruct[attr] = rhotorn_pedtop
                pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"RHOTORN"
            rho_pedtop = itemgetter(0)(cls["CD"].convert(rhotorn_pedtop, "RHOTORN", "PSITOR", cs_prefix, cs_prefix))
            if rho_pedtop is not None and "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"]["EB"] is not None:
                # Convert COCOS 1 -> COCOS 11
                attr = "rho_tor"
                posstruct[attr] = np.sqrt(np.abs(2.0 * rho_pedtop / cls["ZD"]["BVAC"][""]))
                pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"PSITOR; EX2GK: ZD_BVAC"
            psi_pedtop = itemgetter(0)(cls["CD"].convert(rhotorn_pedtop, "RHOTORN", "PSIPOL", cs_prefix, cs_prefix))
            if psi_pedtop is not None:
                # Convert COCOS 1 ->  COCOS 11
                attr = "psi"
                posstruct[attr] = 2.0 * np.pi * psi_pedtop
                pos_prov[attr] = "EX2GK: PD_X; EX2GK: CD_"+cs_prefix+"PSIPOL"
            if posstruct:
                structkey = "position"
                pedestal[structkey] = posstruct
                for key, val in pos_prov.items():
                    pedestal_prov[structkey + "/" + key] = val

            x_pedtop = itemgetter(0)(cls["CD"].convert(rhotorn_pedtop, "RHOTORN", cls["META"]["CSO"], cs_prefix, cs_prefix))
            if "TE" in cls["PD"]:
                attr = "t_e"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "TE")
                err = cls["PD"].interpolate(x_pedtop, "X", "TE", error_flag=True)
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["TE"] and cls["PD"]["TE"]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["TE"]["PROV"]
            if "TI1" in cls["PD"]:
                attr = "t_i_average"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "TI1")
                err = cls["PD"].interpolate(x_pedtop, "X", "TI1", error_flag=True)
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["TI1"] and cls["PD"]["TI1"]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["TI1"]["PROV"]
            if "NE" in cls["PD"]:
                attr = "n_e"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "NE")
                err = cls["PD"].interpolate(x_pedtop, "X", "NE", error_flag=True)
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["NE"] and cls["PD"]["NE"]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["NE"]["PROV"]
            if "NI1" in cls["PD"] and not use_preset_impurities:
                attr = "n_i_total"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "NI1")
                err = cls["PD"].interpolate(x_pedtop, "X", "NI1", error_flag=True)
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["NI1"] and cls["PD"]["NI1"]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["NI1"]["PROV"]
            if "ZEFFP" in cls["PD"] and not cls["FLAG"].checkFlag("FLATZEFF"):
                attr = "zeff"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "ZEFFP")
                err = cls["PD"].interpolate(x_pedtop, "X", "ZEFFP", error_flag=True)
                lerr = None
                if float(val - 3.0 * err) <= 1.0:
                    lerr = (val - 1.0) / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["ZEFFP"] and cls["PD"]["ZEFFP"]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["ZEFFP"]["PROV"]
            elif "ZEFF" in cls["PD"]:
                attr = "zeff"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "ZEFF")
                err = cls["PD"].interpolate(x_pedtop, "X", "ZEFF", error_flag=True)
                lerr = None
                if float(val - 3.0 * err) <= 1.0:
                    lerr = (val - 1.0) / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["ZEFF"] and cls["PD"]["ZEFF"]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["ZEFF"]["PROV"]
            if "Q"+cs_prefix in cls["PD"]:
                attr = "q"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "Q"+cs_prefix)
                err = cls["PD"].interpolate(x_pedtop, "X", "Q"+cs_prefix)
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["Q"+cs_prefix] and cls["PD"]["Q"+cs_prefix]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["Q"+cs_prefix]["PROV"]
            if "SH"+cs_prefix in cls["PD"]:
                attr = "magnetic_shear"
                valstruct = {}
                tag = "value"
                val = cls["PD"].interpolate(x_pedtop, "X", "SH"+cs_prefix)
                err = cls["PD"].interpolate(x_pedtop, "X", "SH"+cs_prefix)
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                pedestal[attr] = valstruct
                if "PROV" in cls["PD"]["SH"+cs_prefix] and cls["PD"]["SH"+cs_prefix]["PROV"] is not None:
                    pedestal_prov[attr + "/" + tag] = cls["PD"]["SH"+cs_prefix]["PROV"]

        # Populate local plasma paramter data
        if axis or separatrix or pedestal:
            localstruct = {}
            local_prov = {}
            if axis:
                structkey = "magnetic_axis"
                localstruct[structkey] = axis
                for key, val in axis_prov.items():
                    local_prov[structkey + "/" + key] = val
            if separatrix:
                structkey = "separatrix"
                localstruct[structkey] = separatrix
                for key, val in separatrix_prov.items():
                    local_prov[structkey + "/" + key] = val
            if pedestal:
                structkey = "pedestal"
                localstruct[structkey] = pedestal
                for key, val in pedestal_prov.items():
                    local_prov[structkey + "/" + key] = val
            if localstruct:
                structkey = "local"
                idsd[structkey] = localstruct
                for key, val in local_prov.items():
                    idsp[structkey + "/" + key] = val

        boundary = {}
        boundary_prov = {}
        if "CONFIG" in cls["META"]:
            btype = cls["META"]["CONFIG"]
            if btype > 1:
                btype = 1    # Set everything that is neither limiter or divertor to divertor (=1)
            attr = "type"
            valstruct = {}
            tag = "value"
            valstruct[tag] = np.array([btype])
            boundary[attr] = valstruct
        if "RGEO" in cls["ZD"] and cls["ZD"]["RGEO"][""] is not None:
            attr = "geometric_axis_r"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["RGEO"][""]])
            err = np.array([cls["ZD"]["RGEO"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["RGEO"] and cls["ZD"]["RGEO"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["RGEO"]["PROV"]
        if "ZGEO" in cls["ZD"] and cls["ZD"]["ZGEO"][""] is not None:
            attr = "geometric_axis_z"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["ZGEO"][""]])
            err = np.array([cls["ZD"]["ZGEO"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["ZGEO"] and cls["ZD"]["ZGEO"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["ZGEO"]["PROV"]
        if "RMAG" in cls["ZD"] and cls["ZD"]["RMAG"][""] is not None:
            attr = "magnetic_axis_r"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["RMAG"][""]])
            err = np.array([cls["ZD"]["RMAG"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["RMAG"] and cls["ZD"]["RMAG"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["RMAG"]["PROV"]
        if "ZMAG" in cls["ZD"] and cls["ZD"]["ZMAG"][""] is not None:
            attr = "magnetic_axis_z"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["ZMAG"][""]])
            err = np.array([cls["ZD"]["ZMAG"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["ZMAG"] and cls["ZD"]["ZMAG"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["ZMAG"]["PROV"]
        if r_minor is not None:
            attr = "minor_radius"
            valstruct = {}
            tag = "value"
            val = np.array([r_minor])
            err = None
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if rprov is not None:
                boundary_prov[attr + "/" + tag] = rprov
        if "ELONG" in cls["ZD"] and cls["ZD"]["ELONG"][""] is not None:
            attr = "elongation"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["ELONG"][""]])
            err = np.array([cls["ZD"]["ELONG"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["ELONG"] and cls["ZD"]["ELONG"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["ELONG"]["PROV"]
        if "TRIANGU" in cls["ZD"] and cls["ZD"]["TRIANGU"][""] is not None:
            attr = "triangularity_upper"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["TRIANGU"][""]])
            err = np.array([cls["ZD"]["TRIANGU"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["TRIANGU"] and cls["ZD"]["TRIANGU"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["TRIANGU"]["PROV"]
        if "TRIANGL" in cls["ZD"] and cls["ZD"]["TRIANGL"][""] is not None:
            attr = "triangularity_lower"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["TRIANGL"][""]])
            err = np.array([cls["ZD"]["TRIANGL"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["TRIANGL"] and cls["ZD"]["TRIANGL"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["TRIANGL"]["PROV"]
        # Only use lower strike point definition for now, IDS only allows one inner and one outer (no double null)
        if "RSTRIKELIN" in cls["ZD"] and cls["ZD"]["RSTRIKELIN"][""] is not None:
            attr = "strike_point_inner_r"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["RSTRIKELIN"][""]])
            err = np.array([cls["ZD"]["RSTRIKELIN"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["RSTRIKELIN"] and cls["ZD"]["RSTRIKELIN"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["RSTRIKELIN"]["PROV"]
        if "ZSTRIKELIN" in cls["ZD"] and cls["ZD"]["ZSTRIKELIN"][""] is not None:
            attr = "strike_point_inner_z"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["ZSTRIKELIN"][""]])
            err = np.array([cls["ZD"]["ZSTRIKELIN"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["ZSTRIKELIN"] and cls["ZD"]["ZSTRIKELIN"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["ZSTRIKELIN"]["PROV"]
        if "RSTRIKELOUT" in cls["ZD"] and cls["ZD"]["RSTRIKELOUT"][""] is not None:
            attr = "strike_point_outer_r"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["RSTRIKELOUT"][""]])
            err = np.array([cls["ZD"]["RSTRIKELOUT"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["RSTRIKELOUT"] and cls["ZD"]["RSTRIKELOUT"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["RSTRIKELOUT"]["PROV"]
        if "ZSTRIKELOUT" in cls["ZD"] and cls["ZD"]["ZSTRIKELOUT"][""] is not None:
            attr = "strike_point_outer_z"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["ZSTRIKELOUT"][""]])
            err = np.array([cls["ZD"]["ZSTRIKELOUT"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            boundary[attr] = valstruct
            if "PROV" in cls["ZD"]["ZSTRIKELOUT"] and cls["ZD"]["ZSTRIKELOUT"]["PROV"] is not None:
                boundary_prov[attr + "/" + tag] = cls["ZD"]["ZSTRIKELOUT"]["PROV"]

        if boundary:
            structkey = "boundary"
            idsd[structkey] = boundary
            for key, val in boundary_prov.items():
                idsp[structkey + "/" + key] = val

        # Assumes horizontal line-of-sight crossing magnetic axis
        lvec = None
        lprov = None
        if "RMAJORO" in cls["CD"][cs_prefix] and "RMAJORI" in cls["CD"][cs_prefix]:
            rmaji = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "RMAJORI", cs_prefix, cs_prefix))
            rmajo = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "RMAJORO", cs_prefix, cs_prefix))
            lvec = np.hstack((rmaji[1::-1], rmajo))
            lprov = "EX2GK: CD_"+cs_prefix+"RMAJORO; EX2GK: CD_"+cs_prefix+"RMAJORI"
        ne_line_average = None
        ne_line_average_error = None
        nprov = None
        if lvec is not None:

            line_average = {}
            line_average_prov = {}

            ltot = float(np.sum(np.diff(lvec)))
            if "TE" in cls["PD"] and cls["PD"]["TE"][""] is not None:
                attr = "t_e"
                valstruct = {}
                tag = "value"
                yvec = np.hstack((cls["PD"]["TE"][""][1::-1], cls["PD"]["TE"][""]))
                yebvec = np.hstack((cls["PD"]["TE"]["EB"][1::-1], cls["PD"]["TE"]["EB"]))
                val = np.array([np.trapz(yvec, lvec) / ltot])
                err = np.array([np.sqrt(np.trapz(np.power(yebvec, 2.0), lvec) / ltot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                line_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["TE"] and cls["PD"]["TE"]["PROV"] is not None:
                    prov = cls["PD"]["TE"]["PROV"]
                if lprov is not None:
                    prov = prov + "; " + lprov if prov is not None else "EX2GK: " + lprov
                if nprov is not None:
                    line_average_prov[attr + "/" + tag] = prov
            if "TI1" in cls["PD"] and cls["PD"]["TI1"][""] is not None:
                attr = "t_i_average"
                valstruct = {}
                tag = "value"
                yvec = np.hstack((cls["PD"]["TI1"][""][1::-1], cls["PD"]["TI1"][""]))
                yebvec = np.hstack((cls["PD"]["TI1"]["EB"][1::-1], cls["PD"]["TI1"]["EB"]))
                val = np.array([np.trapz(yvec, lvec) / ltot])
                err = np.array([np.sqrt(np.trapz(np.power(yebvec, 2.0), lvec) / ltot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                line_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["TI1"] and cls["PD"]["TI1"]["PROV"] is not None:
                    prov = cls["PD"]["TI1"]["PROV"]
                if lprov is not None:
                    prov = prov + "; " + lprov if prov is not None else "EX2GK: " + lprov
                if prov is not None:
                    line_average_prov[attr + "/" + tag] = prov
            if "NE" in cls["PD"] and cls["PD"]["NE"][""] is not None:
                attr = "n_e"
                valstruct = {}
                tag = "value"
                yvec = np.hstack((cls["PD"]["NE"][""][1::-1], cls["PD"]["NE"][""]))
                yebvec = np.hstack((cls["PD"]["NE"]["EB"][1::-1], cls["PD"]["NE"]["EB"]))
                ne_line_average = np.trapz(yvec, lvec) / ltot
                ne_line_average_error = np.sqrt(np.trapz(np.power(yebvec, 2.0), lvec) / ltot)
                val = np.array([ne_line_average])
                err = np.array([ne_line_average_error])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                line_average[attr] = valstruct
                if "PROV" in cls["PD"]["NE"] and cls["PD"]["NE"]["PROV"] is not None:
                    nprov = cls["PD"]["NE"]["PROV"]
                if lprov is not None:
                    nprov = nprov + "; " + lprov if nprov is not None else "EX2GK: " + lprov
                if prov is not None:
                    line_average_prov[attr + "/" + tag] = nprov
            if "NI1" in cls["PD"] and cls["PD"]["NI1"][""] is not None and not use_preset_impurities:
                attr = "n_i_total"
                valstruct = {}
                tag = "value"
                yvec = np.hstack((cls["PD"]["NI1"][""][1::-1], cls["PD"]["NI1"][""]))
                yebvec = np.hstack((cls["PD"]["NI1"]["EB"][1::-1], cls["PD"]["NI1"]["EB"]))
                val = np.array([np.trapz(yvec, lvec) / ltot])
                err = np.array([np.sqrt(np.trapz(np.power(yebvec, 2.0), lvec) / ltot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                line_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["NI1"] and cls["PD"]["NI1"]["PROV"] is not None:
                    prov = cls["PD"]["NI1"]["PROV"]
                if lprov is not None:
                    prov = prov + "; " + lprov if prov is not None else "EX2GK: " + lprov
                if prov is not None:
                    line_average_prov[attr + "/" + tag] = prov
            if "ZEFFP" in cls["PD"] and cls["PD"]["ZEFFP"][""] is not None and not cls["FLAG"].checkFlag("FLATZEFF"):
                attr = "zeff"
                valstruct = {}
                tag = "value"
                yvec = np.hstack((cls["PD"]["ZEFFP"][""][1::-1], cls["PD"]["ZEFFP"][""]))
                yebvec = np.hstack((cls["PD"]["ZEFFP"]["EB"][1::-1], cls["PD"]["ZEFFP"]["EB"]))
                val = np.array([np.trapz(yvec, lvec) / ltot])
                err = np.array([np.sqrt(np.trapz(np.power(yebvec, 2.0), lvec) / ltot)])
                lerr = None
                if float(val - 3.0 * err) <= 1.0:
                    lerr = (val - 1.0) / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                line_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["ZEFFP"] and cls["PD"]["ZEFFP"]["PROV"] is not None:
                    prov = cls["PD"]["ZEFFP"]["PROV"]
                if lprov is not None:
                    prov = prov + "; " + lprov if prov is not None else "EX2GK: " + lprov
                if prov is not None:
                    line_average_prov[attr + "/" + tag] = prov
            elif "ZEFF" in cls["PD"] and cls["PD"]["ZEFF"][""] is not None:
                attr = "zeff"
                valstruct = {}
                tag = "value"
                yvec = np.hstack((cls["PD"]["ZEFF"][""][1::-1], cls["PD"]["ZEFF"][""]))
                yebvec = np.hstack((cls["PD"]["ZEFF"]["EB"][1::-1], cls["PD"]["ZEFF"]["EB"]))
                val = np.array([np.trapz(yvec, lvec) / ltot])
                err = np.array([np.sqrt(np.trapz(np.power(yebvec, 2.0), lvec) / ltot)])
                lerr = None
                if float(val - 3.0 * err) <= 1.0:
                    lerr = (val - 1.0) / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                line_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["ZEFF"] and cls["PD"]["ZEFF"]["PROV"] is not None:
                    prov = cls["PD"]["ZEFF"]["PROV"]
                if lprov is not None:
                    prov = prov + "; " + lprov if prov is not None else "EX2GK: " + lprov
                if prov is not None:
                    line_average_prov[attr + "/" + tag] = prov

            if line_average:
                structkey = "line_average"
                idsd[structkey] = line_average
                for key, val in line_average_prov.items():
                    idsp[structkey + "/" + key] = val

        vvec = None
        vprov = None
        if "FSVOL" in cls["CD"][cs_prefix]:
            vvec = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "FSVOL", cls["META"]["CSOP"], cls["META"]["CSOP"]))
            vprov = "EX2GK: CD_"+cs_prefix+"FSVOL"
        if vvec is not None:

            volume_average = {}
            volume_average_prov = {}

            vtot = float(np.sum(np.diff(vvec)))
            if "TE" in cls["PD"] and cls["PD"]["TE"][""] is not None:
                attr = "t_e"
                valstruct = {}
                tag = "value"
                val = np.array([np.trapz(cls["PD"]["TE"][""], vvec) / vtot])
                err = np.array([np.sqrt(np.trapz(np.power(cls["PD"]["TE"]["EB"], 2.0), vvec) / vtot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                volume_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["TE"] and cls["PD"]["TE"]["PROV"] is not None:
                    prov = cls["PD"]["TE"]["PROV"]
                if vprov is not None:
                    prov = prov + "; " + vprov if prov is not None else "EX2GK: " + vprov
                if prov is not None:
                    volume_average_prov[attr + "/" + tag] = prov
            if "TI1" in cls["PD"] and cls["PD"]["TI1"][""] is not None:
                attr = "t_i_average"
                valstruct = {}
                tag = "value"
                val = np.array([np.trapz(cls["PD"]["TI1"][""], vvec) / vtot])
                err = np.array([np.sqrt(np.trapz(np.power(cls["PD"]["TI1"]["EB"], 2.0), vvec) / vtot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                volume_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["TI1"] and cls["PD"]["TI1"]["PROV"] is not None:
                    prov = cls["PD"]["TI1"]["PROV"]
                if vprov is not None:
                    prov = prov + "; " + vprov if prov is not None else "EX2GK: " + vprov
                if prov is not None:
                    volume_average_prov[attr + "/" + tag] = prov
            if "NE" in cls["PD"] and cls["PD"]["NE"][""] is not None:
                attr = "n_e"
                valstruct = {}
                tag = "value"
                val = np.array([np.trapz(cls["PD"]["NE"][""], vvec) / vtot])
                err = np.array([np.sqrt(np.trapz(np.power(cls["PD"]["NE"]["EB"], 2.0), vvec) / vtot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                volume_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["NE"] and cls["PD"]["NE"]["PROV"] is not None:
                    prov = cls["PD"]["NE"]["PROV"]
                if vprov is not None:
                    prov = prov + "; " + vprov if prov is not None else "EX2GK: " + vprov
                if prov is not None:
                    volume_average_prov[attr + "/" + tag] = prov
            if "NI1" in cls["PD"] and cls["PD"]["NI1"][""] is not None and not use_preset_impurities:
                attr = "n_i_total"
                valstruct = {}
                tag = "value"
                val = np.array([np.trapz(cls["PD"]["NI1"][""], vvec) / vtot])
                err = np.array([np.sqrt(np.trapz(np.power(cls["PD"]["NI1"]["EB"], 2.0), vvec) / vtot)])
                lerr = None
                if float(val - 3.0 * err) <= 0.0:
                    lerr = val / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                volume_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["NI1"] and cls["PD"]["NI1"]["PROV"] is not None:
                    prov = cls["PD"]["NI1"]["PROV"]
                if vprov is not None:
                    prov = prov + "; " + vprov if prov is not None else "EX2GK: " + vprov
                if prov is not None:
                    volume_average_prov[attr + "/" + tag] = prov
            if "ZEFFP" in cls["PD"] and not cls["FLAG"].checkFlag("FLATZEFF"):
                attr = "zeff"
                valstruct = {}
                tag = "value"
                val = np.array([np.trapz(cls["PD"]["ZEFFP"][""], vvec) / vtot])
                err = np.array([np.sqrt(np.trapz(np.power(cls["PD"]["ZEFFP"]["EB"], 2.0), vvec) / vtot)])
                lerr = None
                if float(val - 3.0 * err) <= 1.0:
                    lerr = (val - 1.0) / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                volume_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["ZEFFP"] and cls["PD"]["ZEFFP"]["PROV"] is not None:
                    prov = cls["PD"]["ZEFFP"]["PROV"]
                if vprov is not None:
                    prov = prov + "; " + vprov if prov is not None else "EX2GK: " + vprov
                if prov is not None:
                    volume_average_prov[attr + "/" + tag] = prov
            elif "ZEFF" in cls["PD"]:
                attr = "zeff"
                valstruct = {}
                tag = "value"
                val = np.array([np.trapz(cls["PD"]["ZEFF"][""], vvec) / vtot])
                err = np.array([np.sqrt(np.trapz(np.power(cls["PD"]["ZEFF"]["EB"], 2.0), vvec) / vtot)])
                lerr = None
                if float(val - 3.0 * err) <= 1.0:
                    lerr = (val - 1.0) / 3.0
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                volume_average[attr] = valstruct
                prov = None
                if "PROV" in cls["PD"]["ZEFF"] and cls["PD"]["ZEFF"]["PROV"] is not None:
                    prov = cls["PD"]["ZEFF"]["PROV"]
                if vprov is not None:
                    prov = prov + "; " + vprov if prov is not None else "EX2GK: " + vprov
                if prov is not None:
                    volume_average_prov[attr + "/" + tag] = prov

            if volume_average:
                structkey = "volume_average"
                idsd[structkey] = volume_average
                for key, val in volume_average_prov.items():
                    idsp[structkey + "/" + key] = val

        gas = {}
        gas_prov = {}

        gas_rate = 0.0
        gas_rate_error = 0.0
        if "NUMG" in cls["META"] and cls["META"]["NUMG"] is not None:
            attr = "total"
            valstruct = {}
            tag = "value"
            prov = None
            for ii in range(cls["META"]["NUMG"]):
                itag = "%d" % (ii)
                if "GASR"+itag in cls["ZD"] and cls["ZD"]["GASR"+itag][""] is not None:
                    gas_rate += cls["ZD"]["GASR"+itag][""]
                    gas_rate_error += cls["ZD"]["GASR"+itag]["EB"]
                    if "PROV" in cls["ZD"]["GASR"+itag] and cls["ZD"]["GASR"+itag]["PROV"] is not None:
                        prov = prov + "; " + cls["ZD"]["GASR"+itag]["PROV"] if prov is not None else cls["ZD"]["GASR"+itag]["PROV"]
            val = np.array([gas_rate])
            err = np.array([gas_rate_error])
            lerr = None
            if float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            gas[attr] = valstruct
            if prov is not None:
                gas_prov[attr + "/" + key] = prov
        if gas:
            structkey = "gas_injection_rates"
            idsd[structkey] = gas
            for key, val in gas_prov.items():
                idsp[structkey + "/" + key] = val

        heating = {}
        heating_prov = {}

        total_power = 0.0
        total_power_error = 0.0
        tpprov = None
        if "POWINBI" in cls["ZD"] and cls["ZD"]["POWINBI"][""] is not None:
            pprov = None
            attr = "power_launched_nbi"
            injstruct = {}
            tag = "value"
            power = cls["ZD"]["POWINBI"][""]
            power_error = cls["ZD"]["POWINBI"]["EB"]
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            injstruct = add_ids_dict_value(injstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = injstruct
            if "PROV" in cls["ZD"]["POWINBI"] and cls["ZD"]["POWINBI"]["PROV"] is not None:
                pprov = cls["ZD"]["POWINBI"]["PROV"]
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            attr = "power_nbi"
            coupstruct = {}
            tag = "value"
            if power is not None and power > 0.0 and "POWLNBI" in cls["ZD"] and cls["ZD"]["POWLNBI"][""] is not None:
                power -= cls["ZD"]["POWLNBI"][""]
                power_error = np.sqrt(np.power(power_error, 2.0) + np.power(cls["ZD"]["POWLNBI"]["EB"], 2.0))
                if "PROV" in cls["ZD"]["POWLNBI"] and cls["ZD"]["POWLNBI"]["PROV"] is not None:
                    pprov = pprov + "; " + cls["ZD"]["POWLNBI"]["PROV"] if pprov is not None else cls["ZD"]["POWLNBI"]["PROV"]
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            coupstruct = add_ids_dict_value(coupstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = coupstruct
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            total_power += float(power)
            total_power_error += float(np.power(power_error, 2.0))
            if pprov is not None:
                tpprov = tpprov + "; " + pprov if tpprov is not None else pprov
        if "POWIICRH" in cls["ZD"] and cls["ZD"]["POWIICRH"][""] is not None:
            pprov = None
            attr = "power_launched_ic"
            injstruct = {}
            tag = "value"
            power = cls["ZD"]["POWIICRH"][""]
            power_error = cls["ZD"]["POWIICRH"]["EB"]
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(power) > 0.0 and float(power - 3.0 * power_error) <= 0.0:
                lerr = power / 3.0
            injstruct = add_ids_dict_value(injstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = injstruct
            if "PROV" in cls["ZD"]["POWIICRH"] and cls["ZD"]["POWIICRH"]["PROV"] is not None:
                pprov = cls["ZD"]["POWIICRH"]["PROV"]
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            if power is not None and power > 0.0 and "POWLICRH" in cls["ZD"] and cls["ZD"]["POWLICRH"][""] is not None:
                power -= cls["ZD"]["POWLICRH"][""]
                power_error = np.sqrt(np.power(power_error, 2.0) + np.power(cls["ZD"]["POWLICRH"]["EB"], 2.0))
                if "PROV" in cls["ZD"]["POWLICRH"] and cls["ZD"]["POWLICRH"]["PROV"] is not None:
                    pprov = pprov + "; " + cls["ZD"]["POWLICRH"]["PROV"] if pprov is not None else cls["ZD"]["POWLICRH"]["PROV"]
            attr = "power_ic"
            coupstruct = {}
            tag = "value"
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            coupstruct = add_ids_dict_value(coupstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = coupstruct
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            total_power += float(power)
            total_power_error += float(np.power(power_error, 2.0))
            if pprov is not None:
                tpprov = tpprov + "; " + pprov if tpprov is not None else pprov
        if "POWIECRH" in cls["ZD"] and cls["ZD"]["POWIECRH"][""] is not None:
            pprov = None
            attr = "power_launched_ec"
            injstruct = {}
            tag = "value"
            power = cls["ZD"]["POWIECRH"][""]
            power_error = cls["ZD"]["POWIECRH"]["EB"]
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            injstruct = add_ids_dict_value(injstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = injstruct
            if "PROV" in cls["ZD"]["POWIECRH"] and cls["ZD"]["POWIECRH"]["PROV"] is not None:
                pprov = cls["ZD"]["POWIECRH"]["PROV"]
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            if power is not None and power > 0.0 and "POWLECRH" in cls["ZD"] and cls["ZD"]["POWLECRH"][""] is not None:
                power -= cls["ZD"]["POWLECRH"][""]
                power_error = np.sqrt(np.power(power_error, 2.0) + np.power(cls["ZD"]["POWLECRH"]["EB"], 2.0))
                if "PROV" in cls["ZD"]["POWLECRH"] and cls["ZD"]["POWLECRH"]["PROV"] is not None:
                    pprov = pprov + "; " + cls["ZD"]["POWLECRH"]["PROV"] if pprov is not None else cls["ZD"]["POWLECRH"]["PROV"]
            attr = "power_ec"
            coupstruct = {}
            tag = "value"
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            coupstruct = add_ids_dict_value(coupstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = coupstruct
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            total_power += float(power)
            total_power_error += float(np.power(power_error, 2.0))
            if pprov is not None:
                tpprov = tpprov + "; " + pprov if tpprov is not None else pprov
        if "POWILH" in cls["ZD"] and cls["ZD"]["POWILH"][""] is not None:
            pprov = None
            attr = "power_launched_lh"
            injstruct = {}
            tag = "value"
            power = cls["ZD"]["POWILH"][""]
            power_error = cls["ZD"]["POWILH"]["EB"]
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            injstruct = add_ids_dict_value(injstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = injstruct
            if "PROV" in cls["ZD"]["POWILH"] and cls["ZD"]["POWILH"]["PROV"] is not None:
                pprov = cls["ZD"]["POWILH"]["PROV"]
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            if "POWLLH" in cls["ZD"] and cls["ZD"]["POWLLH"][""] is not None:
                power -= cls["ZD"]["POWLLH"][""]
                power_error = np.sqrt(np.power(power_error, 2.0) + np.power(cls["ZD"]["POWLLH"]["EB"], 2.0))
                if "PROV" in cls["ZD"]["POWLLH"] and cls["ZD"]["POWLLH"]["PROV"] is not None:
                    pprov = pprov + "; " + cls["ZD"]["POWLLH"]["PROV"] if pprov is not None else cls["ZD"]["POWLLH"]["PROV"]
            attr = "power_lh"
            coupstruct = {}
            tag = "value"
            val = np.array([power])
            err = np.array([power_error])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) <= 0.0:
                lerr = val / 3.0
            coupstruct = add_ids_dict_value(coupstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            heating[attr] = coupstruct
            if pprov is not None:
                heating_prov[attr + "/" + tag] = pprov
            total_power += float(power)
            total_power_error += float(np.power(power_error, 2.0))
            if pprov is not None:
                tpprov = tpprov + "; " + pprov if tpprov is not None else pprov

        total_power_error = np.sqrt(total_power_error)
        if heating:
            structkey = "heating_current_drive"
            idsd[structkey] = heating
            for key, val in heating_prov.items():
                idsp[structkey + "/" + key] = val

        global_data = {}
        global_data_prov = {}

        if hmodeflag is not None:
            attr = "h_mode"
            valstruct= {}
            tag = "value"
            valstruct[tag] = np.array([hmodeflag])
            global_data[attr] = valstruct
            global_data_prov[attr + "/" + tag] = "EX2GK: Internal calculation"
        if "PSIPOLN" in cls["CD"][cs_prefix] and "Q"+cs_prefix in cls["PD"] and cls["PD"]["Q"+cs_prefix] is not None:
            attr = "q_95"
            valstruct = {}
            tag = "value"
            psi_95 = np.array([0.95])
            x_95 = itemgetter(0)(cls["CD"].convert(psi_95, "PSIPOLN", cls["META"]["CSO"], cs_prefix, cs_prefix))
            val = cls["PD"].interpolate(x_95, "X", "Q"+cs_prefix)
            err = cls["PD"].interpolate(x_95, "X", "Q"+cs_prefix, error_flag=True)
            lerr = None
            if float(np.abs(val) - 3.0 * err) <= 0.0:
                lerr = np.abs(val) / 3.0
                if float(val) < 0.0:
                    lerr = copy.deepcopy(err)
                    err = np.abs(val) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["PD"]["Q"+cs_prefix] and cls["PD"]["Q"+cs_prefix]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["PD"]["Q"+cs_prefix]["PROV"] + "; CD_"+cs_prefix+"PSIPOLN"
        taue_exp = None
        taueeb_exp = None
        tauprov = None
        if "WMHD" in cls["ZD"] and cls["ZD"]["WMHD"][""] is not None and total_power is not None and total_power > 0.0:
            attr = "tau_energy"
            valstruct = {}
            tag = "value"
            taue_exp = float(cls["ZD"]["WMHD"][""]) / total_power
            taueeb_exp = taue_exp * float(np.sqrt(np.power(cls["ZD"]["WMHD"]["EB"] / cls["ZD"]["WMHD"][""], 2.0) + np.power(total_power_error / total_power, 2.0)))
            val = np.array([taue_exp])
            err = np.array([taueeb_exp])
            lerr = None
            #if float(val - 3.0 * err) >= 0.0:
            #    lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            tauprov = tpprov
            if "PROV" in cls["ZD"]["WMHD"] and cls["ZD"]["WMHD"]["PROV"] is not None:
                tauprov = tauprov + "; " + cls["ZD"]["WMHD"]["PROV"] if tauprov is not None else cls["ZD"]["WMHD"]["PROV"]
            if tauprov is not None:
                global_data_prov[attr + "/" + tag] = tauprov
        plasma_current = None
        plasma_current_error = None
        iprov = None
        if "IPLA" in cls["ZD"] and cls["ZD"]["IPLA"][""] is not None:
            attr = "ip"
            valstruct = {}
            tag = "value"
            plasma_current = float(cls["ZD"]["IPLA"][""])
            plasma_current_error = float(cls["ZD"]["IPLA"]["EB"])
            val = np.array([plasma_current])
            err = np.array([plasma_current_error])
            lerr = None
            if float(np.abs(val) - 3.0 * err) >= 0.0:
                lerr = np.abs(val) / 3.0
                if float(val) > 0.0:
                    lerr = copy.deepcopy(err)
                    err = np.abs(val) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["IPLA"] and cls["ZD"]["IPLA"]["PROV"] is not None:
                iprov = cls["ZD"]["IPLA"]["PROV"]
            if iprov is not None:
                global_data_prov[attr + "/" + tag] = iprov
        if "VLOOP" in cls["ZD"] and cls["ZD"]["VLOOP"][""] is not None:
            attr = "v_loop"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["VLOOP"][""]])
            err = np.array([cls["ZD"]["VLOOP"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["VLOOP"] and cls["ZD"]["VLOOP"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["VLOOP"]["PROV"]
        if "LI3EXP" in cls["ZD"] and cls["ZD"]["LI3EXP"][""] is not None:
            attr = "li"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["LI3EXP"][""]])
            err = np.array([cls["ZD"]["LI3EXP"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["LI3EXP"] and cls["ZD"]["LI3EXP"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["LI3EXP"]["PROV"]
        if "LI3MHD" in cls["ZD"] and cls["ZD"]["LI3MHD"][""] is not None:
            attr = "li_mhd"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["LI3MHD"][""]])
            err = np.array([cls["ZD"]["LI3MHD"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["LI3MHD"] and cls["ZD"]["LI3MHD"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["LI3MHD"]["PROV"]
        if "BETANEXP" in cls["ZD"] and cls["ZD"]["BETANEXP"][""] is not None:
            attr = "beta_tor_norm"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BETANEXP"][""]])
            err = np.array([cls["ZD"]["BETANEXP"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BETANEXP"] and cls["ZD"]["BETANEXP"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BETANEXP"]["PROV"]
        if "BETAPEXP" in cls["ZD"] and cls["ZD"]["BETAPEXP"][""] is not None:
            attr = "beta_pol"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BETAPEXP"][""]])
            err = np.array([cls["ZD"]["BETAPEXP"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BETAPEXP"] and cls["ZD"]["BETAPEXP"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BETAPEXP"]["PROV"]
        if "BETATEXP" in cls["ZD"] and cls["ZD"]["BETATEXP"][""] is not None:
            attr = "beta_tor"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BETATEXP"][""]])
            err = np.array([cls["ZD"]["BETATEXP"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BETATEXP"] and cls["ZD"]["BETATEXP"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BETATEXP"]["PROV"]
        if "BETANMHD" in cls["ZD"] and cls["ZD"]["BETANMHD"][""] is not None:
            attr = "beta_tor_norm_mhd"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BETANMHD"][""]])
            err = np.array([cls["ZD"]["BETANMHD"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BETANMHD"] and cls["ZD"]["BETANMHD"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BETANMHD"]["PROV"]
        if "BETAPMHD" in cls["ZD"] and cls["ZD"]["BETAPMHD"][""] is not None:
            attr = "beta_pol_mhd"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BETAPMHD"][""]])
            err = np.array([cls["ZD"]["BETAPMHD"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BETAPMHD"] and cls["ZD"]["BETAPMHD"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BETAPMHD"]["PROV"]
        if "BETATMHD" in cls["ZD"] and cls["ZD"]["BETATMHD"][""] is not None:
            attr = "beta_tor_mhd"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BETATMHD"][""]])
            err = np.array([cls["ZD"]["BETATMHD"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BETATMHD"] and cls["ZD"]["BETATMHD"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BETATMHD"]["PROV"]
        if "WMHD" in cls["ZD"] and cls["ZD"]["WMHD"][""] is not None:
            attr = "energy_mhd"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["WMHD"][""]])
            err = np.array([cls["ZD"]["WMHD"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["WMHD"] and cls["ZD"]["WMHD"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["WMHD"]["PROV"]
        if "WTH" in cls["ZD"] and cls["ZD"]["WTH"][""] is not None:
            attr = "energy_thermal"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["WTH"][""]])
            err = np.array([cls["ZD"]["WTH"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["WTH"] and cls["ZD"]["WTH"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["WTH"]["PROV"]
        if "WTOT" in cls["ZD"] and cls["ZD"]["WTOT"][""] is not None:
            attr = "energy_total"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["WTOT"][""]])
            err = np.array([cls["ZD"]["WTOT"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["WTOT"] and cls["ZD"]["WTOT"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["WTOT"]["PROV"]
        if "VOLUME" in cls["ZD"] and cls["ZD"]["VOLUME"][""] is not None:
            attr = "volume"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["VOLUME"][""]])
            err = np.array([cls["ZD"]["VOLUME"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["VOLUME"] and cls["ZD"]["VOLUME"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["VOLUME"]["PROV"]
        if "RVAC" in cls["ZD"] and cls["ZD"]["RVAC"][""] is not None:
            attr = "r0"
            valstruct = {}
            tag = "value"
            val = float(cls["ZD"]["RVAC"][""])
            err = float(cls["ZD"]["RVAC"]["EB"])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["RVAC"] and cls["ZD"]["RVAC"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["RVAC"]["PROV"]
        if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
            attr = "b0"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["BVAC"][""]])
            err = np.array([cls["ZD"]["BVAC"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["BVAC"]["PROV"]
        if "POWIOHM" in cls["ZD"] and cls["ZD"]["POWIOHM"][""] is not None:
            attr = "power_ohm"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["POWIOHM"][""]])
            err = np.array([cls["ZD"]["POWIOHM"]["EB"]])
            lerr = None
            if float(val) > 0.0 and float(val - 3.0 * err) < 0.0:
                lerr = val / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["POWIOHM"] and cls["ZD"]["POWIOHM"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["POWIOHM"]["PROV"]
        prad_total = None
        prad_total_error = np.array([0.0])
        if "POWRAD" in cls["ZD"] and cls["ZD"]["POWRAD"][""] is not None:
            attr = "power_radiated"
            valstruct = {}
            tag = "value"
            prad_total = np.array([cls["ZD"]["POWRAD"][""]])
            prad_total_error = np.array([cls["ZD"]["POWRAD"]["EB"]])
            val = prad_total.copy()
            err = prad_total_error.copy()
            lerr = None
            if float(np.abs(val) - 3.0 * err) < 0.0:
                lerr = np.abs(val) / 3.0
                if float(np.abs(val)) > 0.0:
                    lerr = copy.deepcopy(err)
                    err = np.abs(val) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["POWRAD"] and cls["ZD"]["POWRAD"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["POWRAD"]["PROV"]
        prad_inside = None
        prad_inside_error = np.array([0.0])
        if "POWRADBULK" in cls["ZD"] and cls["ZD"]["POWRADBULK"][""] is not None:
            attr = "power_radiated_inside_lcfs"
            valstruct = {}
            tag = "value"
            prad_inside = np.array([cls["ZD"]["POWRADBULK"][""]])
            prad_inside_error = np.array([cls["ZD"]["POWRADBULK"]["EB"]])
            val = prad_inside.copy()
            err = prad_inside_error.copy()
            lerr = None
            if float(np.abs(val) - 3.0 * err) < 0.0:
                lerr = np.abs(val) / 3.0
                if float(np.abs(val)) > 0.0:
                    lerr = copy.deepcopy(err)
                    err = np.abs(val) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if "PROV" in cls["ZD"]["POWRADBULK"] and cls["ZD"]["POWRADBULK"]["PROV"] is not None:
                global_data_prov[attr + "/" + tag] = cls["ZD"]["POWRADBULK"]["PROV"]
        if prad_total is not None and prad_inside is not None:
            attr = "power_radiated_outside_lcfs"
            valstruct = {}
            tag = "value"
            val = prad_total - prad_inside
            err = np.sqrt(np.power(prad_total_error, 2.0) + np.power(prad_inside_error, 2.0))
            lerr = None
            if float(np.abs(val) - 3.0 * err) < 0.0:
                lerr = np.abs(val) / 3.0
                if float(np.abs(val)) > 0.0:
                    lerr = copy.deepcopy(err)
                    err = np.abs(val) / 3.0
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            global_data_prov[attr + "/" + tag] = 'global_quantities/power_radiated/value; global_quantities/power_radiated_inside_lcfs/value'

        scaling_law_data = {}
        slprov = None
        if plasma_current is not None:
            scaling_law_data["plasma_current"] = plasma_current
            scaling_law_data["plasma_current_error"] = plasma_current_error
            if iprov is not None:
                slprov = slprov + "; " + iprov if slprov is not None else iprov
        if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
            scaling_law_data["toroidal_field"] = cls["ZD"]["BVAC"][""]
            scaling_law_data["toroidal_field_error"] = cls["ZD"]["BVAC"]["EB"]
            if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"] is not None:
                slprov = slprov + "; " + cls["ZD"]["BVAC"]["PROV"] if slprov is not None else cls["ZD"]["BVAC"]["PROV"]
        if ne_line_average is not None:
            scaling_law_data["line_density"] = ne_line_average
            scaling_law_data["line_density_error"] = ne_line_average_error
            if nprov is not None:
                slprov = slprov + "; " + nprov if slprov is not None else nprov
        if total_power is not None:
            scaling_law_data["total_power"] = total_power
            scaling_law_data["total_power_error"] = total_power_error
            if tpprov is not None:
                slprov = slprov + "; " + tpprov if slprov is not None else tpprov
        if r_minor is not None:
            scaling_law_data["minor_radius"] = r_minor
            scaling_law_data["minor_radius_error"] = 0.0
            if rprov is not None:
                slprov = slprov + "; " + rprov if slprov is not None else rprov
        if "RMAG" in cls["ZD"] and cls["ZD"]["RMAG"][""] is not None:
            scaling_law_data["major_radius"] = cls["ZD"]["RMAG"][""]
            scaling_law_data["major_radius_error"] = cls["ZD"]["RMAG"]["EB"]
            if "PROV" in cls["ZD"]["RMAG"] and cls["ZD"]["RMAG"]["PROV"] is not None:
                slprov = slprov + "; " + cls["ZD"]["RMAG"]["PROV"] if slprov is not None else cls["ZD"]["RMAG"]["PROV"]
        if "ELONG" in cls["ZD"] and cls["ZD"]["ELONG"][""] is not None:
            scaling_law_data["elongation"] = cls["ZD"]["ELONG"][""]
            scaling_law_data["elongation_error"] = cls["ZD"]["ELONG"]["EB"]
            if "PROV" in cls["ZD"]["ELONG"] and cls["ZD"]["ELONG"]["PROV"] is not None:
                slprov = slprov + "; " + cls["ZD"]["ELONG"]["PROV"] if slprov is not None else cls["ZD"]["ELONG"]["PROV"]
        if "ZI1" in cls["META"]:
            scaling_law_data["isotope_mass"] = cls["META"]["ZI1"]
            scaling_law_data["isotope_mass_error"] = 0.0
            slprov = slprov + "; META_Z1" if slprov is not None else "META_Z1"
        (taue, taueeb) = qtools.calc_scaling_confinement_time(style=0, **scaling_law_data)
        if taue is not None:
            attr = "tau_energy_98"
            valstruct = {}
            tag = "value"
            val = np.array([taue])
            err = np.array([taueeb])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            global_data[attr] = valstruct
            if slprov is not None:
                global_data_prov[attr + "/" + tag] = slprov
            if taue_exp is not None:
                attr = "h_98"
                valstruct = {}
                tag = "value"
                val = np.array([taue_exp / taue])
                err = np.array([(taue_exp / taue) * np.sqrt(np.power(taueeb_exp / taue_exp, 2.0) + np.power(taueeb / taue, 2.0))])
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                global_data[attr] = valstruct
                if tauprov is not None:
                    global_data_prov[attr + "/" + tag] = tauprov

        if plasma_current is not None and r_minor is not None:
            r_minor_error = 0.0
            (gwlim, gwlimeb) = qtools.calc_greenwald_density_limit(plasma_current, r_minor, plasma_current_error, r_minor_error)
            if ne_line_average is not None and gwlim is not None:
                attr = "greenwald_fraction"
                valstruct = {}
                tag = "value"
                val = np.array([float(ne_line_average / gwlim)])
                err = np.array([float(ne_line_average * gwlimeb / np.power(gwlim, 2.0))])
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                global_data[attr] = valstruct
                prov = None
                if iprov is not None:
                    prov = prov + "; " + iprov if prov is not None else iprov
                if rprov is not None:
                    prov = prov + "; " + rprov if prov is not None else rprov
                if nprov is not None:
                    prov = prov + "; " + nprov if prov is not None else nprov
                if prov is not None:
                    global_data_prov[attr + "/" + tag] = prov

        if global_data:
            structkey = "global_quantities"
            idsd[structkey] = global_data
            for key, val in global_data_prov.items():
                idsp[structkey + "/" + key] = val

        fusstruct = {}
        fus_prov = {}

        ratestruct = {}
        rate_prov = {}

        if "NEUTRTOT" in cls["ZD"] and cls["ZD"]["NEUTRTOT"][""] is not None:
            attr = "total"
            valstruct = {}
            tag = "value"
            val = np.array([cls["ZD"]["NEUTRTOT"][""]])
            err = np.array([cls["ZD"]["NEUTRTOT"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            ratestruct[attr] = valstruct
            if "PROV" in cls["ZD"]["NEUTRTOT"] and cls["ZD"]["NEUTRTOT"]["PROV"] is not None:
                rate_prov[attr + "/" + tag] = cls["ZD"]["NEUTRTOT"]["PROV"]

        if ratestruct:
            structkey = "neutron_rates"
            fusstruct[structkey] = ratestruct
            for key, val in rate_prov.items():
                fus_prov[structkey + "/" + key] = val

        if fusstruct:
            structkey = "fusion"
            idsd[structkey] = fusstruct
            for key, val in fus_prov.items():
                idsp[structkey + "/" + key] = val

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            tm = cls["META"]["T2"]
            tw = cls["META"]["T2"] - cls["META"]["T1"]
            ids_time = float(tm) - 0.5 * float(tw)
            idsd["time"] = np.array([float(tm)])   # This definition of time (with time_width) will cause inconsistencies with other IDSs generated by this tool, use other one?
            #idsd["time"] = np.array([ids_time])
            idsd["time_width"] = np.array([float(tw)])
            phase = 1 if cls["META"]["WINPHASE"] == 0 else 0
            idsd["stationary_phase_flag"] = {"value": np.array([phase])}

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd

# Note that the equilibrium is taken directly from the EFIT solution (constrained or not) and values may vary from summary IDS
def pass_equilibrium_ids_time_slice_values(cls, user=None, dd_version=min_imas_version):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        cs_prefix = cls["META"]["CSOP"]
#        zddata = cls["ZD"].grabEquilibriumQuantities()
        eddata = cls["ED"].grabEquilibriumQuantities()

        r_minor = None
        rprov = None
        if "RMINORA" in cls["CD"][cs_prefix]:
            rmin = itemgetter(0)(cls["CD"].convert(np.array([1.0]), "RHOTORN", "RMINORA", cs_prefix, cs_prefix))
            r_minor = float(rmin)
            rprov = "EX2GK: CD_"+cs_prefix+"RMINORA"
        elif "RMINORO" in cls["CD"][cs_prefix] and "RMINORI" in cls["CD"][cs_prefix]:
            rmini = itemgetter(0)(cls["CD"].convert(np.array([1.0]), "RHOTORN", "RMINORI", cs_prefix, cs_prefix))
            rmino = itemgetter(0)(cls["CD"].convert(np.array([1.0]), "RHOTORN", "RMINORO", cs_prefix, cs_prefix))
            r_minor = float(rmino + rmini) / 2.0
            rprov = "EX2GK: CD_"+cs_prefix+"RMINORO; EX2GK: CD_"+cs_prefix+"RMINORI"

        tf = {}
        tf_prov = {}
        if "RVAC" in cls["ZD"] and cls["ZD"]["RVAC"][""] is not None:
            attr = "r0"
            val = float(cls["ZD"]["RVAC"][""])
            err = float(cls["ZD"]["RVAC"]["EB"])
            lerr = None
            tf = add_ids_dict_value(tf, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["RVAC"] and cls["ZD"]["RVAC"]["PROV"]:
                tf_prov[attr] = cls["ZD"]["RVAC"]["PROV"]
        if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
            attr = "b0"
            val = np.array([cls["ZD"]["BVAC"][""]])
            err = np.array([cls["ZD"]["BVAC"]["EB"]])
            lerr = None
            tf = add_ids_dict_value(tf, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"]:
                tf_prov[attr] = cls["ZD"]["BVAC"]["PROV"]
        if tf:
            structkey = "vacuum_toroidal_field"
            idsd[structkey] = tf
            for key, val in tf_prov.items():
                idsp[structkey + "/" + key] = val

        time_slice = {}
        time_slice_prov = {}

        separatrix = {}
        separatrix_prov = {}
        if "CONFIG" in cls["META"] and cls["META"]["CONFIG"] is not None:
            attr = "type"
            btype = cls["META"]["CONFIG"]
            if btype > 1:
                btype = 1    # Set everything that is neither limiter or divertor to divertor (=1)
            separatrix[attr] = btype
        if r_minor is not None:
            attr = "minor_radius"
            separatrix[attr] = r_minor
            if rprov is not None:
                separatrix_prov[attr] = rprov
        if "NLIM" in eddata and eddata["NLIM"] > 0:
            outstruct = {}
            out_prov = {}
            attr = "r"
            outstruct[attr] = eddata["RLIM"]
            if "LIM" in cls["ED"] and "PROV" in cls["ED"]["LIM"] and cls["ED"]["LIM"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["LIM"]["PROV"]
            attr = "z"
            outstruct[attr] = eddata["ZLIM"]
            if "LIM" in cls["ED"] and "PROV" in cls["ED"]["LIM"] and cls["ED"]["LIM"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["LIM"]["PROV"]

            structkey = "outline"
            separatrix[structkey] = outstruct
            for key, val in out_prov.items():
                separatrix_prov[structkey + "/" + key] = val
        elif "NBND" in eddata and eddata["NBND"] > 0:
            outstruct = {}
            out_prov = {}
            attr = "r"
            outstruct[attr] = eddata["RBND"]
            if "BND" in cls["ED"] and "PROV" in cls["ED"]["BND"] and cls["ED"]["BND"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["BND"]["PROV"]
            attr = "z"
            outstruct[attr] = eddata["ZBND"]
            if "BND" in cls["ED"] and "PROV" in cls["ED"]["BND"] and cls["ED"]["BND"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["BND"]["PROV"]

            structkey = "outline"
            separatrix[structkey] = outstruct
            for key, val in out_prov.items():
                separatrix_prov[structkey + "/" + key] = val
        if "PSIBND" in cls["ZD"] and cls["ZD"]["PSIBND"][""] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "psi"
            separatrix[attr] = 2.0 * np.pi * cls["ZD"]["PSIBND"][""]
            if "PROV" in cls["ZD"]["PSIBND"] and cls["ZD"]["PSIBND"]["PROV"]:
                separatrix_prov[attr] = cls["ZD"]["PSIBND"]["PROV"]
            attr = "psi_norm"
            separatrix[attr] = 1.0
        if "RGEO" in cls["ZD"] and cls["ZD"]["RGEO"][""] is not None and "ZGEO" in cls["ZD"] and cls["ZD"]["ZGEO"][""] is not None:
            geostruct = {}
            geo_prov = {}
            attr = "r"
            geostruct[attr] = cls["ZD"]["RGEO"][""]
            if "PROV" in cls["ZD"]["RGEO"] and cls["ZD"]["RGEO"]["PROV"]:
                geo_prov[attr] = cls["ZD"]["RGEO"]["PROV"]
            attr = "z"
            geostruct[attr] = cls["ZD"]["ZGEO"][""]
            if "PROV" in cls["ZD"]["ZGEO"] and cls["ZD"]["ZGEO"]["PROV"]:
                geo_prov[attr] = cls["ZD"]["ZGEO"]["PROV"]

            structkey = "geometric_axis"
            separatrix[structkey] = geostruct
            for key, val in geo_prov.items():
                separatrix_prov[structkey + "/" + key] = val
        if "ELONG" in cls["ZD"] and cls["ZD"]["ELONG"][""] is not None:
            attr = "elongation"
            separatrix[attr] = cls["ZD"]["ELONG"][""]
            if "PROV" in cls["ZD"]["ELONG"] and cls["ZD"]["ELONG"]["PROV"] is not None:
                separatrix_prov[attr] = cls["ZD"]["ELONG"]["PROV"]
        if "TRIANGU" in cls["ZD"] and cls["ZD"]["TRIANGU"][""] is not None:
            attr = "triangularity_upper"
            separatrix[attr] = cls["ZD"]["TRIANGU"][""]
            if "PROV" in cls["ZD"]["TRIANGU"] and cls["ZD"]["TRIANGU"]["PROV"] is not None:
                separatrix_prov[attr] = cls["ZD"]["TRIANGU"]["PROV"]
        if "TRIANGL" in cls["ZD"] and cls["ZD"]["TRIANGL"][""] is not None:
            attr = "triangularity_lower"
            separatrix[attr] = cls["ZD"]["TRIANGL"][""]
            if "PROV" in cls["ZD"]["TRIANGL"] and cls["ZD"]["TRIANGL"]["PROV"] is not None:
                separatrix_prov[attr] = cls["ZD"]["TRIANGL"]["PROV"]
        # X point lower
        if "RXPOINTL" in cls["ZD"] and cls["ZD"]["RXPOINTL"][""] is not None and "ZXPOINTL" in cls["ZD"] and cls["ZD"]["ZXPOINTL"][""] is not None:
            xpstruct = {}
            xp_prov = {}
            attr = "r"
            xpstruct[attr] = cls["ZD"]["RXPOINTL"][""]
            if "PROV" in cls["ZD"]["RXPOINTL"] and cls["ZD"]["RXPOINTL"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["RXPOINTL"]["PROV"]
            attr = "z"
            xpstruct[attr] = cls["ZD"]["ZXPOINTL"][""]
            if "PROV" in cls["ZD"]["ZXPOINTL"] and cls["ZD"]["ZXPOINTL"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["ZXPOINTL"]["PROV"]

            structkey = "x_point"
            if structkey not in separatrix:
                separatrix[structkey] = []
            separatrix[structkey].append(xpstruct)
            ii = len(separatrix["x_point"])
            istr = '(%d)' % (ii - 1)
            for key, val in xp_prov.items():
                separatrix_prov[structkey + istr + "/" + key] = val
        # X point upper
        if "RXPOINTU" in cls["ZD"] and cls["ZD"]["RXPOINTU"][""] is not None and "ZXPOINTU" in cls["ZD"] and cls["ZD"]["ZXPOINTU"][""] is not None:
            xpstruct = {}
            xp_prov = {}
            attr = "r"
            xpstruct[attr] = cls["ZD"]["RXPOINTU"][""]
            if "PROV" in cls["ZD"]["RXPOINTU"] and cls["ZD"]["RXPOINTU"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["RXPOINTU"]["PROV"]
            attr = "z"
            xpstruct[attr] = cls["ZD"]["ZXPOINTU"][""]
            if "PROV" in cls["ZD"]["ZXPOINTU"] and cls["ZD"]["ZXPOINTU"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["ZXPOINTU"]["PROV"]

            structkey = "x_point"
            if structkey not in separatrix:
                separatrix[structkey] = []
            separatrix[structkey].append(xpstruct)
            ii = len(separatrix[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in xp_prov.items():
                separatrix_prov[structkey + istr + "/" + key] = val
        # Strike point lower inner
        if "RSTRIKELIN" in cls["ZD"] and cls["ZD"]["RSTRIKELIN"][""] is not None and "ZSTRIKELIN" in cls["ZD"] and cls["ZD"]["ZSTRIKELIN"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["RSTRIKELIN"] and cls["ZD"]["RSTRIKELIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKELIN"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["ZSTRIKELIN"] and cls["ZD"]["ZSTRIKELIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKELIN"]["PROV"]

            structkey = "strike_point"
            if structkey not in separatrix:
                separatrix[structkey] = []
            separatrix[structkey].append(spstruct)
            ii = len(separatrix[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                separatrix_prov[structkey + istr + "/" + key] = val
        # Strike point lower outer
        if "RSTRIKELOUT" in cls["ZD"] and cls["ZD"]["RSTRIKELOUT"][""] is not None and "ZSTRIKELOUT" in cls["ZD"] and cls["ZD"]["ZSTRIKELOUT"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["RSTRIKELOUT"] and cls["ZD"]["RSTRIKELOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKELOUT"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["ZSTRIKELOUT"] and cls["ZD"]["ZSTRIKELOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKELOUT"]["PROV"]

            structkey = "strike_point"
            if structkey not in separatrix:
                separatrix[structkey] = []
            separatrix[structkey].append(spstruct)
            ii = len(separatrix[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                separatrix_prov[structkey + istr + "/" + key] = val
        # Strike point upper inner
        if "RSTRIKEUIN" in cls["ZD"] and cls["ZD"]["RSTRIKEUIN"][""] is not None and "ZSTRIKEUIN" in cls["ZD"] and cls["ZD"]["ZSTRIKEUIN"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKEUIN"][""]
            if "PROV" in cls["ZD"]["RSTRIKEUIN"] and cls["ZD"]["RSTRIKEUIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKEUIN"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["ZSTRIKEUIN"] and cls["ZD"]["ZSTRIKEUIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKEUIN"]["PROV"]

            structkey = "strike_point"
            if structkey not in separatrix:
                separatrix[structkey] = []
            separatrix[structkey].append(spstruct)
            ii = len(separatrix[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                separatrix_prov[structkey + istr + "/" + key] = val
        # Strike point upper outer
        if "RSTRIKEUOUT" in cls["ZD"] and cls["ZD"]["RSTRIKEUOUT"][""] is not None and "ZSTRIKEUOUT" in cls["ZD"] and cls["ZD"]["ZSTRIKEUOUT"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKEUOUT"][""]
            if "PROV" in cls["ZD"]["RSTRIKEUOUT"] and cls["ZD"]["RSTRIKEUOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKEUOUT"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["ZSTRIKEUOUT"] and cls["ZD"]["ZSTRIKEUOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKEUOUT"]["PROV"]

            structkey = "strike_point"
            if structkey not in separatrix:
                separatrix[structkey] = []
            separatrix[structkey].append(spstruct)
            ii = len(separatrix[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                separatrix_prov[structkey + istr + "/" + key] = val

        if separatrix:
            structkey = "boundary_separatrix"
            time_slice[structkey] = separatrix
            for key, val in separatrix_prov.items():
                time_slice_prov[structkey + "/" + key] = val

        boundary = {}
        boundary_prov = {}
        if "CONFIG" in cls["META"] and cls["META"]["CONFIG"] is not None:
            btype = cls["META"]["CONFIG"]
            if btype > 1:
                btype = 1    # Set everything that is neither limiter or divertor to divertor (=1)
            attr = "type"
            boundary[attr] = btype
        if r_minor is not None:
            attr = "minor_radius"
            boundary[attr] = r_minor
            if rprov is not None:
                boundary_prov[attr] = rprov
        if "NLIM" in eddata and eddata["NLIM"] > 0:
            outstruct = {}
            out_prov = {}
            attr = "r"
            outstruct[attr] = eddata["RLIM"]
            if "LIM" in cls["ED"] and "PROV" in cls["ED"]["LIM"] and cls["ED"]["LIM"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["LIM"]["PROV"]
            attr = "z"
            outstruct[attr] = eddata["ZLIM"]
            if "LIM" in cls["ED"] and "PROV" in cls["ED"]["LIM"] and cls["ED"]["LIM"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["LIM"]["PROV"]

            structkey = "outline"
            boundary[structkey] = outstruct
            for key, val in out_prov.items():
                boundary_prov[structkey + "/" + key] = val
        elif "NBND" in eddata and eddata["NBND"] > 0:
            outstruct = {}
            out_prov = {}
            attr = "r"
            outstruct[attr] = eddata["RBND"]
            if "BND" in cls["ED"] and "PROV" in cls["ED"]["BND"] and cls["ED"]["BND"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["BND"]["PROV"]
            attr = "z"
            outstruct[attr] = eddata["ZBND"]
            if "BND" in cls["ED"] and "PROV" in cls["ED"]["BND"] and cls["ED"]["BND"]["PROV"] is not None:
                out_prov[attr] = cls["ED"]["BND"]["PROV"]

            structkey = "outline"
            boundary[structkey] = outstruct
            for key, val in out_prov.items():
                boundary_prov[structkey + "/" + key] = val
        if "PSIBND" in cls["ZD"] and cls["ZD"]["PSIBND"][""] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "psi"
            boundary[attr] = 2.0 * np.pi * cls["ZD"]["PSIBND"][""]
            if "PROV" in cls["ZD"]["PSIBND"] and cls["ZD"]["PSIBND"]["PROV"]:
                boundary_prov[attr] = cls["ZD"]["PSIBND"]["PROV"]
            attr = "psi_norm"
            boundary[attr] = 1.0
        if "RGEO" in cls["ZD"] and cls["ZD"]["RGEO"][""] is not None and "ZGEO" in cls["ZD"] and cls["ZD"]["ZGEO"][""] is not None:
            geostruct = {}
            geo_prov = {}
            attr = "r"
            geostruct[attr] = cls["ZD"]["RGEO"][""]
            if "PROV" in cls["ZD"]["RGEO"] and cls["ZD"]["RGEO"]["PROV"]:
                geo_prov[attr] = cls["ZD"]["RGEO"]["PROV"]
            attr = "z"
            geostruct[attr] = cls["ZD"]["ZGEO"][""]
            if "PROV" in cls["ZD"]["ZGEO"] and cls["ZD"]["ZGEO"]["PROV"]:
                geo_prov[attr] = cls["ZD"]["ZGEO"]["PROV"]

            structkey = "geometric_axis"
            boundary[structkey] = geostruct
            for key, val in geo_prov.items():
                boundary_prov[structkey + "/" + key] = val
        if "ELONG" in cls["ZD"] and cls["ZD"]["ELONG"][""] is not None:
            attr = "elongation"
            boundary[attr] = cls["ZD"]["ELONG"][""]
            if "PROV" in cls["ZD"]["ELONG"] and cls["ZD"]["ELONG"]["PROV"] is not None:
                boundary_prov[attr] = cls["ZD"]["ELONG"]["PROV"]
        if "TRIANGU" in cls["ZD"] and cls["ZD"]["TRIANGU"][""] is not None:
            attr = "triangularity_upper"
            boundary[attr] = cls["ZD"]["TRIANGU"][""]
            if "PROV" in cls["ZD"]["TRIANGU"] and cls["ZD"]["TRIANGU"]["PROV"] is not None:
                boundary_prov[attr] = cls["ZD"]["TRIANGU"]["PROV"]
        if "TRIANGL" in cls["ZD"] and cls["ZD"]["TRIANGL"][""] is not None:
            attr = "triangularity_lower"
            boundary[attr] = cls["ZD"]["TRIANGL"][""]
            if "PROV" in cls["ZD"]["TRIANGL"] and cls["ZD"]["TRIANGL"]["PROV"] is not None:
                boundary_prov[attr] = cls["ZD"]["TRIANGL"]["PROV"]
        # X point lower
        if "RXPOINTL" in cls["ZD"] and cls["ZD"]["RXPOINTL"][""] is not None and "ZXPOINTL" in cls["ZD"] and cls["ZD"]["ZXPOINTL"][""] is not None:
            xpstruct = {}
            xp_prov = {}
            attr = "r"
            xpstruct[attr] = cls["ZD"]["RXPOINTL"][""]
            if "PROV" in cls["ZD"]["RXPOINTL"] and cls["ZD"]["RXPOINTL"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["RXPOINTL"]["PROV"]
            attr = "z"
            xpstruct[attr] = cls["ZD"]["ZXPOINTL"][""]
            if "PROV" in cls["ZD"]["ZXPOINTL"] and cls["ZD"]["ZXPOINTL"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["ZXPOINTL"]["PROV"]

            structkey = "x_point"
            if structkey not in boundary:
                boundary[structkey] = []
            boundary[structkey].append(xpstruct)
            ii = len(boundary[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in xp_prov.items():
                boundary_prov[structkey + istr + "/" + key] = val
        # X point upper
        if "RXPOINTU" in cls["ZD"] and cls["ZD"]["RXPOINTU"][""] is not None and "ZXPOINTU" in cls["ZD"] and cls["ZD"]["ZXPOINTU"][""] is not None:
            xpstruct = {}
            xp_prov = {}
            attr = "r"
            xpstruct[attr] = cls["ZD"]["RXPOINTU"][""]
            if "PROV" in cls["ZD"]["RXPOINTU"] and cls["ZD"]["RXPOINTU"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["RXPOINTU"]["PROV"]
            attr = "z"
            xpstruct[attr] = cls["ZD"]["ZXPOINTU"][""]
            if "PROV" in cls["ZD"]["ZXPOINTU"] and cls["ZD"]["ZXPOINTU"]["PROV"]:
                xp_prov[attr] = cls["ZD"]["ZXPOINTU"]["PROV"]

            structkey = "x_point"
            if structkey not in boundary:
                boundary[structkey] = []
            boundary[structkey].append(xpstruct)
            ii = len(boundary[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in xp_prov.items():
                boundary_prov[structkey + istr + "/" + key] = val
        # Strike point lower inner
        if "RSTRIKELIN" in cls["ZD"] and cls["ZD"]["RSTRIKELIN"][""] is not None and "ZSTRIKELIN" in cls["ZD"] and cls["ZD"]["ZSTRIKELIN"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["RSTRIKELIN"] and cls["ZD"]["RSTRIKELIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKELIN"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["ZSTRIKELIN"] and cls["ZD"]["ZSTRIKELIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKELIN"]["PROV"]

            structkey = "strike_point"
            if structkey not in boundary:
                boundary[structkey] = []
            boundary[structkey].append(spstruct)
            ii = len(boundary[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                boundary_prov[structkey + istr + "/" + key] = val
        # Strike point lower outer
        if "RSTRIKELOUT" in cls["ZD"] and cls["ZD"]["RSTRIKELOUT"][""] is not None and "ZSTRIKELOUT" in cls["ZD"] and cls["ZD"]["ZSTRIKELOUT"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["RSTRIKELOUT"] and cls["ZD"]["RSTRIKELOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKELOUT"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["ZSTRIKELOUT"] and cls["ZD"]["ZSTRIKELOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKELOUT"]["PROV"]

            structkey = "strike_point"
            if structkey not in boundary:
                boundary[structkey] = []
            boundary[structkey].append(spstruct)
            ii = len(boundary[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                boundary_prov[structkey + istr + "/" + key] = val
        # Strike point upper inner
        if "RSTRIKEUIN" in cls["ZD"] and cls["ZD"]["RSTRIKEUIN"][""] is not None and "ZSTRIKEUIN" in cls["ZD"] and cls["ZD"]["ZSTRIKEUIN"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["RSTRIKEUIN"] and cls["ZD"]["RSTRIKEUIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKEUIN"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELIN"][""]
            if "PROV" in cls["ZD"]["ZSTRIKEUIN"] and cls["ZD"]["ZSTRIKEUIN"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKEUIN"]["PROV"]

            structkey = "strike_point"
            if structkey not in boundary:
                boundary[structkey] = []
            boundary[structkey].append(spstruct)
            ii = len(boundary[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                boundary_prov[structkey + istr + "/" + key] = val
        # Strike point upper outer
        if "RSTRIKEUOUT" in cls["ZD"] and cls["ZD"]["RSTRIKEUOUT"][""] is not None and "ZSTRIKEUOUT" in cls["ZD"] and cls["ZD"]["ZSTRIKEUOUT"][""] is not None:
            spstruct = {}
            sp_prov = {}
            attr = "r"
            spstruct[attr] = cls["ZD"]["RSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["RSTRIKEUOUT"] and cls["ZD"]["RSTRIKEUOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["RSTRIKEUOUT"]["PROV"]
            attr = "z"
            spstruct[attr] = cls["ZD"]["ZSTRIKELOUT"][""]
            if "PROV" in cls["ZD"]["ZSTRIKEUOUT"] and cls["ZD"]["ZSTRIKEUOUT"]["PROV"]:
                sp_prov[attr] = cls["ZD"]["ZSTRIKEUOUT"]["PROV"]

            structkey = "strike_point"
            if structkey not in boundary:
                boundary[structkey] = []
            boundary[structkey].append(spstruct)
            ii = len(boundary[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in sp_prov.items():
                boundary_prov[structkey + istr + "/" + key] = val

        if boundary:
            structkey = "boundary"
            time_slice[structkey] = boundary
            for key, val in boundary_prov.items():
                time_slice_prov[structkey + "/" + key] = val

        global_data = {}
        global_data_prov = {}

        x_axis = itemgetter(0)(cls["CD"].convert(np.array([0.0]), "PSIPOLN", cls["META"]["CSO"], cs_prefix, cs_prefix))
        x_95 = itemgetter(0)(cls["CD"].convert(np.array([0.95]), "PSIPOLN", cls["META"]["CSO"], cs_prefix, cs_prefix))
        if "Q"+cs_prefix in cls["PD"] and cls["PD"]["Q"+cs_prefix][""] is not None:
            attr = "q_axis"
            val = float(cls["PD"].interpolate(x_axis, "X", "Q"+cs_prefix))
            err = float(cls["PD"].interpolate(x_axis, "X", "Q"+cs_prefix, error_flag=True))
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"]["Q"+cs_prefix] and cls["PD"]["Q"+cs_prefix]["PROV"] is not None:
                global_data_prov[attr] = cls["PD"]["Q"+cs_prefix]["PROV"]
            attr = "q_95"
            val = float(cls["PD"].interpolate(x_95, "X", "Q"+cs_prefix))
            err = float(cls["PD"].interpolate(x_95, "X", "Q"+cs_prefix, error_flag=True))
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"]["Q"+cs_prefix] and cls["PD"]["Q"+cs_prefix]["PROV"] is not None:
                global_data_prov[attr] = cls["PD"]["Q"+cs_prefix]["PROV"]
        if "BETATEXP" in cls["ZD"] and cls["ZD"]["BETATEXP"][""] is not None:
            attr = "beta_tor"
            val = float(cls["ZD"]["BETATEXP"][""])
            err = float(cls["ZD"]["BETATEXP"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["BETATEXP"] and cls["ZD"]["BETATEXP"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["BETATEXP"]["PROV"]
        if "BETAPEXP" in cls["ZD"] and cls["ZD"]["BETAPEXP"][""] is not None:
            attr = "beta_pol"
            val = float(cls["ZD"]["BETAPEXP"][""])
            err = float(cls["ZD"]["BETAPEXP"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["BETAPEXP"] and cls["ZD"]["BETAPEXP"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["BETAPEXP"]["PROV"]
        if "BETANEXP" in cls["ZD"] and cls["ZD"]["BETANEXP"][""] is not None:
            attr = "beta_normal"
            val = float(cls["ZD"]["BETANEXP"][""])
            err = float(cls["ZD"]["BETANEXP"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["BETANEXP"] and cls["ZD"]["BETANEXP"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["BETANEXP"]["PROV"]
        if "IPLA" in cls["ZD"] and cls["ZD"]["IPLA"][""] is not None:
            attr = "ip"
            val = float(cls["ZD"]["IPLA"][""])
            err = float(cls["ZD"]["IPLA"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["IPLA"] and cls["ZD"]["IPLA"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["IPLA"]["PROV"]
        if "LI3MHD" in cls["ZD"] and cls["ZD"]["LI3MHD"][""] is not None:
            attr = "li_3"
            val = float(cls["ZD"]["LI3MHD"][""])
            err = float(cls["ZD"]["LI3MHD"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["LI3MHD"] and cls["ZD"]["LI3MHD"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["LI3MHD"]["PROV"]
        if "WMHD" in cls["ZD"] and cls["ZD"]["WMHD"][""] is not None:
            attr = "energy_mhd"
            val = float(cls["ZD"]["WMHD"][""])
            err = float(cls["ZD"]["WMHD"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["WMHD"] and cls["ZD"]["WMHD"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["WMHD"]["PROV"]
        if "VOLUME" in cls["ZD"] and cls["ZD"]["VOLUME"][""] is not None:
            attr = "volume"
            val = float(cls["ZD"]["VOLUME"][""])
            err = float(cls["ZD"]["VOLUME"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["VOLUME"] and cls["ZD"]["VOLUME"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["VOLUME"]["PROV"]
        if "XSAREA" in cls["ZD"] and cls["ZD"]["XSAREA"][""] is not None:
            attr = "area"
            val = float(cls["ZD"]["XSAREA"][""])
            err = float(cls["ZD"]["XSAREA"]["EB"])
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["XSAREA"] and cls["ZD"]["XSAREA"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["XSAREA"]["PROV"]
        if "PSIAXS" in cls["ZD"] and cls["ZD"]["PSIAXS"][""] is not None and "PSIBND" in cls["ZD"] and cls["ZD"]["PSIBND"][""] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "psi_axis"
            val = 2.0 * np.pi * cls["ZD"]["PSIAXS"][""]
            err = 2.0 * np.pi * cls["ZD"]["PSIAXS"]["EB"]
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["PSIAXS"] and cls["ZD"]["PSIAXS"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["PSIAXS"]["PROV"]
            # Convert COCOS 1 -> COCOS 11
            attr = "psi_boundary"
            val = 2.0 * np.pi * cls["ZD"]["PSIBND"][""]
            err = 2.0 * np.pi * cls["ZD"]["PSIBND"]["EB"]
            lerr = None
            global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["PSIBND"] and cls["ZD"]["PSIBND"]["PROV"] is not None:
                global_data_prov[attr] = cls["ZD"]["PSIBND"]["PROV"]
        if "RMAG" in cls["ZD"] and cls["ZD"]["RMAG"][""] is not None and "ZMAG" in cls["ZD"] and cls["ZD"]["ZMAG"][""] is not None:
            magstruct = {}
            mag_prov = {}
            attr = "r"
            val = float(cls["ZD"]["RMAG"][""])
            err = float(cls["ZD"]["RMAG"]["EB"])
            lerr = None
            magstruct = add_ids_dict_value(magstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["RMAG"] and cls["ZD"]["RMAG"]["PROV"] is not None:
                mag_prov[attr] = cls["ZD"]["RMAG"]["PROV"]
            attr = "z"
            val = float(cls["ZD"]["ZMAG"][""])
            err = float(cls["ZD"]["ZMAG"]["EB"])
            lerr = None
            magstruct = add_ids_dict_value(magstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["ZD"]["ZMAG"] and cls["ZD"]["ZMAG"]["PROV"] is not None:
                mag_prov[attr] = cls["ZD"]["ZMAG"]["PROV"]

            structkey = "magnetic_axis"
            global_data[structkey] = magstruct
            for key, val in mag_prov.items():
                global_data_prov[structkey + "/" + key] = val

        if global_data:
            structkey = "global_quantities"
            time_slice[structkey] = global_data
            for key, val in global_data_prov.items():
                time_slice_prov[structkey + "/" + key] = val

        profiles = {}
        profiles_prov = {}

        pol_flux = itemgetter(0)(cls["CD"].convert(eddata["X"], "PSIPOLN", "PSIPOL", cs_prefix, cs_prefix))
        if pol_flux is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "psi"
            profiles[attr] = 2.0 * np.pi * pol_flux
            profiles_prov[attr] = "EX2GK: CD_"+cs_prefix+"PSIPOL"
        tor_flux = itemgetter(0)(cls["CD"].convert(eddata["X"], "PSIPOLN", "PSITOR", cs_prefix, cs_prefix))
        if tor_flux is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "phi"
            profiles[attr] = 2.0 * np.pi * tor_flux
            profiles_prov[attr] = "EX2GK: CD_"+cs_prefix+"PSITOR"
            if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
                # Convert COCOS 1 -> COCOS 11
                attr = "rho_tor"
                profiles[attr] = np.sqrt(np.abs(2.0 * tor_flux / cls["ZD"]["BVAC"][""]))
                if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"] is not None:
                    profiles_prov[attr] = cls["ZD"]["BVAC"]["PROV"] + "; EX2GK: CD_"+cs_prefix+"PSITOR"
        rho_tor_norm = itemgetter(0)(cls["CD"].convert(eddata["X"], "PSIPOLN", "RHOTORN", cs_prefix, cs_prefix))
        if rho_tor_norm is not None:
            attr = "rho_tor_norm"
            profiles[attr] = rho_tor_norm
            profiles_prov[attr] = "EX2GK: CD_"+cs_prefix+"RHOTORN"
        if "P" in eddata and eddata["P"] is not None:
            attr = "pressure"
            profiles[attr] = eddata["P"]
            if "P" in cls["ED"] and "PROV" in cls["ED"]["P"] and cls["ED"]["P"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["P"]["PROV"]
        if "F" in eddata and eddata["F"] is not None:
            attr = "f"
            profiles[attr] = eddata["F"]
            if "F" in cls["ED"] and "PROV" in cls["ED"]["F"] and cls["ED"]["F"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["F"]["PROV"]
        if "PP" in eddata and eddata["PP"] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "dpressure_dpsi"
            profiles[attr] = eddata["PP"] / (2.0 * np.pi)
            if "PP" in cls["ED"] and "PROV" in cls["ED"]["PP"] and cls["ED"]["PP"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["PP"]["PROV"]
        if "FFP" in eddata and eddata["FFP"] is not None:
            # Convert COCOS 1 -> COCOS 11
            attr = "f_df_dpsi"
            profiles[attr] = eddata["FFP"] / (2.0 * np.pi)
            if "FFP" in cls["ED"] and "PROV" in cls["ED"]["FFP"] and cls["ED"]["FFP"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["FFP"]["PROV"]
        if "Q" in eddata and eddata["Q"] is not None:
            attr = "q"
            profiles[attr] = eddata["Q"]
            if "Q" in cls["ED"] and "PROV" in cls["ED"]["Q"] and cls["ED"]["Q"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["Q"]["PROV"]
        if "AREA" in cls["ED"] and cls["ED"]["AREA"][""] is not None:
            attr = "area"
            profiles[attr] = cls["ED"]["AREA"][""]
            if "AREA" in cls["ED"] and "PROV" in cls["ED"]["AREA"] and cls["ED"]["AREA"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["AREA"]["PROV"]
        if "VOL" in cls["ED"] and cls["ED"]["VOL"][""] is not None:
            attr = "volume"
            profiles[attr] = cls["ED"]["VOL"][""]
            if "VOL" in cls["ED"] and "PROV" in cls["ED"]["VOL"] and cls["ED"]["VOL"]["PROV"] is not None:
                profiles_prov[attr] = cls["ED"]["VOL"]["PROV"]

        if profiles:
            structkey = "profiles_1d"
            time_slice[structkey] = profiles
            for key, val in profiles_prov.items():
                time_slice_prov[structkey + "/" + key] = val

        maps = {}
        maps_prov = {}

        rvec = None
        zvec = None
        if "RLEFT" in eddata and eddata["RLEFT"] is not None and "RRIGHT" in eddata and eddata["RRIGHT"] is not None:
            rvec = np.linspace(eddata["RLEFT"], eddata["RRIGHT"], eddata["NR"])
        if "ZBOTTOM" in eddata and eddata["ZBOTTOM"] is not None and "ZTOP" in eddata and eddata["ZTOP"] is not None:
            zvec = np.linspace(eddata["ZBOTTOM"], eddata["ZTOP"], eddata["NZ"])
        if rvec is not None and zvec is not None and "PSI" in eddata and eddata["PSI"] is not None:
            if "grid_type" not in maps:
                maps["grid_type"] = {}
            maps["grid_type"]["index"] = 1
            maps["grid_type"]["name"] = "rectangular"
            maps["grid_type"]["description"] = "Cylindrical R,Z ala eqdsk (R=dim1, Z=dim2). In this case the position arrays should not be filled since they are redundant with grid/dim1 and dim2."
            if "grid" not in maps:
                maps["grid"] = {}
            maps["grid"]["dim1"] = rvec
            maps["grid"]["dim2"] = zvec
            maps["psi"] = 2.0 * np.pi * eddata["PSI"].T   # Convert COCOS 1 -> COCOS 11
            if "PSI" in cls["ED"] and "PROV" in cls["ED"]["PSI"] and cls["ED"]["PSI"]["PROV"] is not None:
                maps_prov["grid/dim1"] = cls["ED"]["PSI"]["PROV"]
                maps_prov["grid/dim2"] = cls["ED"]["PSI"]["PROV"]
                maps_prov["psi"] = cls["ED"]["PSI"]["PROV"]

        if maps:
            structkey = "profiles_2d"
            if structkey not in time_slice:
                time_slice[structkey] = []
            time_slice[structkey].append(maps)
            ii = len(time_slice[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in maps_prov.items():
                nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                time_slice_prov[structkey + istr + "/" + key] = nval

        if time_slice:
            structkey = "time_slice"
            if structkey not in idsd:
                idsd[structkey] = []
            idsd[structkey].append(time_slice)
            ii = len(idsd[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in time_slice_prov.items():
                nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                idsp[structkey + istr + "/" + key] = nval

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            tm = cls["META"]["T2"]
            tw = cls["META"]["T2"] - cls["META"]["T1"]
            ids_time = float(tm) - 0.5 * float(tw)
            idsd["time"] = np.array([ids_time])

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd

def pass_core_profiles_ids_time_slice_values(cls, user=None, dd_version=min_imas_version, include_raw=False, use_preset_impurities=False):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        cs_prefix = cls["META"]["CSOP"]
        tm = cls["META"]["T2"]
        tw = cls["META"]["T2"] - cls["META"]["T1"]
        ids_time = float(tm) - 0.5 * float(tw)

        profiles = {}
        profiles_prov = {}

        electrons = {}
        electrons_prov = {}

        # Electron temperature
        rkey = "TE"
        pkey = "TE"
        if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
            fitstruct = {}
            fitstruct_prov = {}
            val = cls["RD"][rkey][""].copy()
            err = cls["RD"][rkey]["EB"].copy()
            fitstruct["measured"] = val
            fitstruct["measured_error_upper"] = err
            weight = np.ones(val.shape)
            fitstruct["weight"] = weight
            fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            fitstruct["time_measurement"] = np.full(val.shape, float(tm))
            fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
            if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                fitstruct["reconstructed"] = recon
                chi2 = np.full(val.shape, 1000.0)
                zfilt = (err > 0.0)
                if np.any(zfilt):
                    chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                fitstruct["chi_squared"] = chi2
                if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                    fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
            if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
            if fitstruct:
                structkey = "temperature_fit"
                electrons[structkey] = fitstruct
                for key, val in fitstruct_prov.items():
                    electrons_prov[structkey + "/" + key] = val
        if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
            attr = "temperature"
            val = cls["PD"][pkey][""].copy()
            err = cls["PD"][pkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 0.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            electrons[attr + "_validity"] = 0
            if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                electrons_prov[attr] = cls["PD"][pkey]["PROV"]

        # Electron density
        rkey = "NE"
        pkey = "NE"
        if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
            fitstruct = {}
            fitstruct_prov = {}
            val = cls["RD"][rkey][""].copy()
            err = cls["RD"][rkey]["EB"].copy()
            fitstruct["measured"] = val
            fitstruct["measured_error_upper"] = err
            weight = np.ones(val.shape)
            fitstruct["weight"] = weight
            fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            fitstruct["time_measurement"] = np.full(val.shape, float(tm))
            fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
            if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                fitstruct["reconstructed"] = recon
                chi2 = np.full(val.shape, 1000.0)
                zfilt = (err > 0.0)
                if np.any(zfilt):
                    chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                fitstruct["chi_squared"] = chi2
                if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                    fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
            if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
            if fitstruct:
                structkey = "density_fit"
                electrons[structkey] = fitstruct
                for key, val in fitstruct_prov.items():
                    electrons_prov[structkey + "/" + key] = val
        if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
            attr = "density_thermal"
            val = cls["PD"][pkey][""].copy()
            err = cls["PD"][pkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 0.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                electrons_prov[attr] = cls["PD"][pkey]["PROV"]

            # Fast component is not expected to be present for electrons
            refshape = cls["PD"]["X"][""].shape
            fattr = "density_fast"
            nfast_val = np.zeros(refshape)
            nfast_err = np.zeros(refshape)
            nfast_lerr = None
            prov = None
            non_physical_filter = (nfast_val - 3.0 * nfast_err) < 0.0
            if np.any(non_physical_filter):
                nfast_lerr = copy.deepcopy(err)
                nfast_lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, fattr, values=nfast_val, upper_errors=nfast_err, lower_errors=nfast_lerr, dd_version=dd_version)

            # Summing thermal and fast
            tattr = "density"
            ntot_val = val + nfast_val
            ntot_err = np.sqrt(np.power(err, 2.0) + np.power(nfast_err, 2.0))
            ntot_lerr = None
            non_physical_filter = (ntot_val - 3.0 * ntot_err) < 0.0
            if np.any(non_physical_filter):
                ntot_lerr = copy.deepcopy(ntot_err)
                ntot_lerr[non_physical_filter] = ntot_val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, tattr, values=ntot_val, upper_errors=ntot_err, lower_errors=ntot_lerr, dd_version=dd_version)
            electrons[tattr + "_validity"] = 0
            electrons_prov[tattr] = 'profiles_1d(*)/electrons/density_thermal; profiles_1d(*)/electrons/density_fast'

        # Electron pressure
        pkey = "PE"
        if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
            attr = "pressure_thermal"
            val = cls["PD"][pkey][""].copy()
            err = cls["PD"][pkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 0.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                electrons_prov[attr] = cls["PD"][pkey]["PROV"]

            # Further splitting into thermal and fast, currently assume all are thermal
            refshape = cls["PD"]["X"][""].shape
            pfast_val = np.zeros(refshape)
            pfast_err = np.zeros(refshape)
            pfast_lerr = None
            prov = None
            non_physical_filter = (pfast_val - 3.0 * pfast_err) < 0.0
            eattr = "pressure_fast_perpendicular"
            perfrac = 2.0 / 3.0
            pfast_per_val = perfrac * pfast_val
            pfast_per_err = perfrac * pfast_err
            pfast_per_lerr = perfrac * pfast_lerr if pfast_lerr is not None else None
            electrons = add_ids_dict_value(electrons, eattr, values=pfast_per_val, upper_errors=pfast_per_err, lower_errors=pfast_per_lerr, dd_version=dd_version)
            aattr = "pressure_fast_parallel"
            parfrac = 1.0 / 3.0
            pfast_par_val = parfrac * pfast_val
            pfast_par_err = parfrac * pfast_err
            pfast_par_lerr = parfrac * pfast_lerr if pfast_lerr is not None else None
            electrons = add_ids_dict_value(electrons, aattr, values=pfast_par_val, upper_errors=pfast_par_err, lower_errors=pfast_par_lerr, dd_version=dd_version)

            tattr = "pressure"
            ptot_val = val + pfast_val
            ptot_err = np.sqrt(np.power(err, 2.0) + np.power(pfast_err, 2.0))
            ptot_lerr = None
            non_physical_filter = (ptot_val - 3.0 * ptot_err) < 0.0
            if np.any(non_physical_filter):
                ptot_lerr = copy.deepcopy(ptot_err)
                ptot_lerr[non_physical_filter] = ptot_val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, tattr, values=ptot_val, upper_errors=ptot_err, lower_errors=ptot_lerr, dd_version=dd_version)
#            electrons[tattr + "_validity"] = 0
            electrons_prov[tattr] = 'profiles_1d(*)/electrons/pressure_thermal; profiles_1d(*)/electrons/pressure_fast_perpendicular; profiles_1d(*)/electrons/pressure_fast_parallel'

        # Collisionality (electron-ion) - why is this in the electrons class?
        pkey = "OMCOLLEINORM"
        if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
            attr = "collisionality_norm"
            val = cls["PD"][pkey][""].copy()
            err = cls["PD"][pkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 0.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            electrons = add_ids_dict_value(electrons, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#            electrons[attr + "_validity"] = 0
            if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                electrons_prov[attr] = cls["PD"][pkey]["PROV"]

        if electrons:
            structkey = "electrons"
            profiles[structkey] = electrons
            for key, val in electrons_prov.items():
                profiles_prov[structkey + "/" + key] = val

        # Average ion temperature
        rkey = "TIMP"
        pkey = "TI1"
        if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
            fitstruct = {}
            fitstruct_prov = {}
            val = cls["RD"][rkey][""].copy()
            err = cls["RD"][rkey]["EB"].copy()
            fitstruct["measured"] = val
            fitstruct["measured_error_upper"] = err
            weight = np.ones(val.shape)
            fitstruct["weight"] = weight
            fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            fitstruct["time_measurement"] = np.full(val.shape, float(tm))
            fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
            if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                fitstruct["reconstructed"] = recon
                chi2 = np.full(val.shape, 1000.0)
                zfilt = (err > 0.0)
                if np.any(zfilt):
                    chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                fitstruct["chi_squared"] = chi2
                if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                    fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
            if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
            if fitstruct:
                structkey = "t_i_average_fit"
                profiles[structkey] = fitstruct
                for key, val in fitstruct_prov.items():
                    profiles_prov[structkey + "/" + key] = val
        if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
            attr = "t_i_average"
            val = cls["PD"][pkey][""].copy()
            err = cls["PD"][pkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 0.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#            profiles[attr + "_validity"] = 0
            if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][pkey]["PROV"]

        # Ions
        if not use_preset_impurities:

            # Main ions
            for nn in range(cls["META"]["NUMI"]):

                ionstruct = {}
                ionstruct_prov = {}
                ntag = "I%d" % (nn + 1)

                # Ion temperature
                rkey = "T" + ntag
                pkey = "T" + ntag
                if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
                    fitstruct = {}
                    fitstruct_prov = {}
                    val = cls["RD"][rkey][""].copy()
                    err = cls["RD"][rkey]["EB"].copy()
                    fitstruct["measured"] = val
                    fitstruct["measured_error_upper"] = err
                    weight = np.ones(val.shape)
                    fitstruct["weight"] = weight
                    fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    fitstruct["time_measurement"] = np.full(val.shape, float(tm))
                    fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
                    if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                        recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                        fitstruct["reconstructed"] = recon
                        chi2 = np.full(val.shape, 1000.0)
                        zfilt = (err > 0.0)
                        if np.any(zfilt):
                            chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                        fitstruct["chi_squared"] = chi2
                        if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                            fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
                    if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                        fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
                    if fitstruct:
                        structkey = "temperature_fit"
                        ionstruct[structkey] = fitstruct
                        for key, val in fitstruct_prov.items():
                            ionstruct_prov[structkey + "/" + key] = val
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "temperature"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                # Ion density
                rkey = "N" + ntag
                pkey = "N" + ntag
                if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
                    fitstruct = {}
                    fitstruct_prov = {}
                    val = cls["RD"][rkey][""].copy()
                    err = cls["RD"][rkey]["EB"].copy()
                    fitstruct["measured"] = val
                    fitstruct["measured_error_upper"] = err
                    weight = np.ones(val.shape)
                    fitstruct["weight"] = weight
                    fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    fitstruct["time_measurement"] = np.full(val.shape, float(tm))
                    fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
                    if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                        recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                        fitstruct["reconstructed"] = recon
                        chi2 = np.full(val.shape, 1000.0)
                        zfilt = (err > 0.0)
                        if np.any(zfilt):
                            chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                        fitstruct["chi_squared"] = chi2
                        if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                            fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
                    if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                        fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
                    if fitstruct:
                        structkey = "density_fit"
                        ionstruct[structkey] = fitstruct
                        for key, val in fitstruct_prov.items():
                            ionstruct_prov[structkey + "/" + key] = val
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "density_thermal"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                    # Checking if fast component is present due to ingested source profiles
                    refshape = cls["PD"]["X"][""].shape
                    fattr = "density_fast"
                    nfast_val = np.zeros(refshape)
                    nfast_err = np.zeros(refshape)
                    nfast_lerr = None
                    prov = None
                    for ff in range(cls["META"]["NUMFI"]):
                        ftag = "%d" % (ff+1)
                        if "AFI"+ftag in cls["META"] and cls["META"]["AFI"+ftag] is not None and "ZFI"+ftag in cls["META"] and cls["META"]["ZFI"+ftag] is not None and np.isclose(cls["META"]["AFI"+ftag], cls["META"]["A"+ntag]) and np.isclose(cls["META"]["ZFI"+ftag], cls["META"]["Z"+ntag]) and "SRCFI"+ftag in cls["META"] and cls["META"]["SRCFI"+ftag] is not None:
                            src = None
                            if cls["ZD"].isSourcePresent(cls["META"]["SRCFI"+ftag]):
                                for key in imas_source_name_translation:
                                    if cls["META"]["SRCFI"+ftag] == key:
                                        src = key
                            if src is not None:
                                fkey = "NFI" + ftag + src
                                if fkey in cls["PD"] and cls["PD"][fkey] is not None:
                                    nfast_val = nfast_val + cls["PD"][fkey][""]
                                    nfast_err = np.sqrt(np.power(nfast_err, 2.0) + np.power(cls["PD"][fkey]["EB"], 2.0))
                                    if "PROV" in cls["PD"][fkey] and cls["PD"][fkey]["PROV"] is not None:
                                        prov = prov + "; " + cls["PD"][fkey]["PROV"] if prov is not None else cls["PD"][fkey]["PROV"]
                    non_physical_filter = (nfast_val - 3.0 * nfast_err) < 0.0
                    if np.any(non_physical_filter):
                        nfast_lerr = copy.deepcopy(nfast_err)
                        nfast_lerr[non_physical_filter] = nfast_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, fattr, values=nfast_val, upper_errors=nfast_err, lower_errors=nfast_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[fattr] = prov

                    # Summing thermal and fast
                    tattr = "density"
                    ntot_val = val + nfast_val
                    ntot_err = np.sqrt(np.power(err, 2.0) + np.power(nfast_err, 2.0))
                    ntot_lerr = None
                    non_physical_filter = (ntot_val - 3.0 * ntot_err) < 0.0
                    if np.any(non_physical_filter):
                        ntot_lerr = copy.deepcopy(ntot_err)
                        ntot_lerr[non_physical_filter] = ntot_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, tattr, values=ntot_val, upper_errors=ntot_err, lower_errors=ntot_lerr, dd_version=dd_version)
                    ionstruct[tattr + "_validity"] = 0
                    ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/density_thermal; profiles_1d(*)/ion(*)/density_fast'

                # Ion pressure
                pkey = "P" + ntag
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "pressure_thermal"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                    # Checking if fast component is present, needs to be split into perpendicular and parallel
                    refshape = cls["PD"]["X"][""].shape
                    pfast_val = np.zeros(refshape)
                    pfast_err = np.zeros(refshape)
                    pfast_lerr = None
                    prov = None
                    for ff in range(cls["META"]["NUMFI"]):
                        ftag = "%d" % (ff+1)
                        if "AFI"+ftag in cls["META"] and cls["META"]["AFI"+ftag] is not None and "ZFI"+ftag in cls["META"] and cls["META"]["ZFI"+ftag] is not None and np.isclose(cls["META"]["AFI"+ftag], cls["META"]["A"+ntag]) and np.isclose(cls["META"]["ZFI"+ftag], cls["META"]["Z"+ntag]) and "SRCFI"+ftag in cls["META"] and cls["META"]["SRCFI"+ftag] is not None:
                            src = None
                            if cls["ZD"].isSourcePresent(cls["META"]["SRCFI"+ftag]):
                                for key in imas_source_name_translation:
                                    if cls["META"]["SRCFI"+ftag] == key:
                                        src = key
                            if src is not None:
                                fkey = "WFI" + ftag + src
                                if fkey in cls["PD"] and cls["PD"][fkey] is not None:
                                    wfac = 2.0 / 3.0
                                    pfast_val = pfast_val + wfac * cls["PD"][fkey][""]
                                    pfast_err = np.sqrt(np.power(pfast_err, 2.0) + np.power(wfac * cls["PD"][fkey]["EB"], 2.0))
                                    if "PROV" in cls["PD"][fkey] and cls["PD"][fkey]["PROV"] is not None:
                                        prov = prov + "; " + cls["PD"][fkey]["PROV"] if prov is not None else cls["PD"][fkey]["PROV"]
                    non_physical_filter = (pfast_val - 3.0 * pfast_err) < 0.0
                    if np.any(non_physical_filter):
                        pfast_lerr = copy.deepcopy(pfast_err)
                        pfast_lerr[non_physical_filter] = pfast_val[non_physical_filter] / 3.0

                    # Assumes 2/3 and 1/3 split into perpendicular and parallel directions respectively (isotropic pressure assumption)
                    eattr = "pressure_fast_perpendicular"
                    perfrac = 2.0 / 3.0
                    pfast_per_val = perfrac * pfast_val
                    pfast_per_err = perfrac * pfast_err
                    pfast_per_lerr = perfrac * pfast_lerr if pfast_lerr is not None else None
                    ionstruct = add_ids_dict_value(ionstruct, eattr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[eattr] = prov

                    aattr = "pressure_fast_parallel"
                    parfrac = 1.0 / 3.0
                    pfast_par_val = parfrac * pfast_val
                    pfast_par_err = parfrac * pfast_err
                    pfast_par_lerr = parfrac * pfast_lerr if pfast_lerr is not None else None
                    ionstruct = add_ids_dict_value(ionstruct, aattr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[aattr] = prov

                    # Summing thermal and fast
                    tattr = "pressure"
                    ptot_val = val + pfast_val
                    ptot_err = np.sqrt(np.power(err, 2.0) + np.power(pfast_err, 2.0))
                    ptot_lerr = None
                    non_physical_filter = (ptot_val - 3.0 * ptot_err) < 0.0
                    if np.any(non_physical_filter):
                        ptot_lerr = copy.deepcopy(ptot_err)
                        ptot_lerr[non_physical_filter] = ptot_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, tattr, values=ptot_val, upper_errors=ptot_err, lower_errors=ptot_lerr, dd_version=dd_version)
#                    ionstruct[tattr + "_validity"] = 0
                    ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/pressure_thermal; profiles_1d(*)/ion(*)/pressure_fast_perpendicular; profiles_1d(*)/ion(*)/pressure_fast_parallel'

                # Ion toroidal rotation frequency
                pkey = "AFTOR" + ntag
                gkey = "AFTOR"
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "rotation_frequency_tor"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]
                elif cls.isFitted() and gkey in cls["PD"] and cls["PD"][gkey][""] is not None:
                    attr = "rotation_frequency_tor"
                    val = cls["PD"][gkey][""].copy()
                    err = cls["PD"][gkey]["EB"].copy()
                    lerr = None
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][gkey] and cls["PD"][gkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][gkey]["PROV"]

                # Ion species metadata
                akey = "A" + ntag
                zkey = "Z" + ntag
                mkey = "MAT" + ntag
                if ionstruct and akey in cls["META"] and cls["META"][akey] is not None and zkey in cls["META"] and cls["META"][zkey] is not None:
                    # These species only have one element
                    elemstruct = {}
                    elemstruct["a"] = float(cls["META"][akey])
                    elemstruct["z_n"] = float(cls["META"][zkey])
                    elemstruct["atoms_n"] = 1
                    if "element" not in ionstruct:
                        ionstruct["element"] = []
                    ionstruct["element"].append(elemstruct)
                    if mkey in cls["META"] and cls["META"][mkey] is not None:
                        ionstruct["label"] = cls["META"][mkey] + ("+%d" % (cls["META"][zkey]))
                    ionstruct["multiple_states_flag"] = 0
                    # The flat charge state assumption is not always correct - what can this be replaced with?
                    zion = float(cls["META"][zkey]) if float(cls["META"][zkey]) < 46.0 else 46.0
                    ionstruct["z_ion"] = zion
                    ionstruct["z_ion_1d"] = np.full(cls["PD"]["X"][""].shape, zion)
                    ionstruct["z_ion_square_1d"] = np.power(ionstruct["z_ion_1d"], 2.0)

                if ionstruct:
                    structkey = "ion"
                    if structkey not in profiles:
                        profiles[structkey] = []
                    profiles[structkey].append(ionstruct)
                    ii = len(profiles[structkey])
                    istr = '(%d)' % (ii - 1)
                    for key, val in ionstruct_prov.items():
                        nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                        profiles_prov[structkey + istr + "/" + key] = nval

            # Known impurity data
            for nn in range(cls["META"]["NUMIMP"]):

                ionstruct = {}
                ionstruct_prov = {}
                ntag = "IMP%d" % (nn + 1)

                # Ion temperature
                rkey = "T" + ntag
                pkey = "T" + ntag
                okey = "TIMP"
                if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
                    fitstruct = {}
                    fitstruct_prov = {}
                    val = cls["RD"][rkey][""].copy()
                    err = cls["RD"][rkey]["EB"].copy()
                    fitstruct["measured"] = val
                    fitstruct["measured_error_upper"] = err
                    weight = np.ones(val.shape)
                    fitstruct["weight"] = weight
                    fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    fitstruct["time_measurement"] = np.full(val.shape, float(tm))
                    fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
                    if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                        recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                        fitstruct["reconstructed"] = recon
                        chi2 = np.full(val.shape, 1000.0)
                        zfilt = (err > 0.0)
                        if np.any(zfilt):
                            chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                        fitstruct["chi_squared"] = chi2
                        if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                            fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
                    if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                        fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
                    if fitstruct:
                        structkey = "temperature_fit"
                        ionstruct[structkey] = fitstruct
                        for key, val in fitstruct_prov.items():
                            ionstruct_prov[structkey + "/" + key] = val
                elif include_raw and okey in cls["RD"] and cls["RD"][okey][""] is not None:
                    fitstruct = {}
                    fitstruct_prov = {}
                    val = cls["RD"][okey][""].copy()
                    err = cls["RD"][okey]["EB"].copy()
                    fitstruct["measured"] = val
                    fitstruct["measured_error_upper"] = err
                    weight = np.ones(val.shape)
                    fitstruct["weight"] = weight
                    fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][okey]["X"], cls["RD"][okey].coordinate, "RHOTORN", cls["RD"][okey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][okey]["X"] + cls["RD"][okey]["XEB"], cls["RD"][okey].coordinate, "RHOTORN", cls["RD"][okey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][okey]["X"] - cls["RD"][okey]["XEB"], cls["RD"][okey].coordinate, "RHOTORN", cls["RD"][okey].coord_prefix, cs_prefix))
                    fitstruct["time_measurement"] = np.full(val.shape, float(tm))
                    fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
                    if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                        recon = cls["PD"].interpolate(cls["RD"][okey]["X"], "X", pkey)
                        fitstruct["reconstructed"] = recon
                        chi2 = np.full(val.shape, 1000.0)
                        zfilt = (err > 0.0)
                        if np.any(zfilt):
                            chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                        fitstruct["chi_squared"] = chi2
                        if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                            fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
                    if "PROV" in cls["RD"][okey] and cls["RD"][okey]["PROV"] is not None:
                        fitstruct_prov["measured"] = cls["RD"][okey]["PROV"]
                    if fitstruct:
                        structkey = "temperature_fit"
                        ionstruct[structkey] = fitstruct
                        for key, val in fitstruct_prov.items():
                            ionstruct_prov[structkey + "/" + key] = val
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "temperature"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                # Ion density
                rkey = "N" + ntag
                pkey = "N" + ntag
                if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
                    fitstruct = {}
                    fitstruct_prov = {}
                    val = cls["RD"][rkey][""].copy()
                    err = cls["RD"][rkey]["EB"].copy()
                    fitstruct["measured"] = val
                    fitstruct["measured_error_upper"] = err
                    weight = np.ones(val.shape)
                    fitstruct["weight"] = weight
                    fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                    fitstruct["time_measurement"] = np.full(val.shape, float(tm))
                    fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
                    if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                        recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                        fitstruct["reconstructed"] = recon
                        chi2 = np.full(val.shape, 1000.0)
                        zfilt = (err > 0.0)
                        if np.any(zfilt):
                            chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                        fitstruct["chi_squared"] = chi2
                        if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                            fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
                    if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                        fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
                    if fitstruct:
                        structkey = "density_fit"
                        ionstruct[structkey] = fitstruct
                        if fitstruct_prov:
                            ionstruct_prov[structkey + "/" + key] = val
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "density_thermal"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                    # Fast component is not expected to be present for impurities
                    refshape = cls["PD"]["X"][""].shape
                    fattr = "density_fast"
                    nfast_val = np.zeros(refshape)
                    nfast_err = np.zeros(refshape)
                    nfast_lerr = None
                    prov = None
                    non_physical_filter = (nfast_val - 3.0 * nfast_err) < 0.0
                    if np.any(non_physical_filter):
                        nfast_lerr = copy.deepcopy(nfast_err)
                        nfast_lerr[non_physical_filter] = nfast_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, fattr, values=nfast_val, upper_errors=nfast_err, lower_errors=nfast_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[fattr] = prov

                    # Summing thermal and fast
                    tattr = "density"
                    ntot_val = val + nfast_val
                    ntot_err = np.sqrt(np.power(err, 2.0) + np.power(nfast_err, 2.0))
                    non_physical_filter = (ntot_val - 3.0 * ntot_err) < 0.0
                    if np.any(non_physical_filter):
                        ntot_lerr = copy.deepcopy(ntot_err)
                        ntot_lerr[non_physical_filter] = ntot_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, tattr, values=ntot_val, upper_errors=ntot_err, lower_errors=ntot_lerr, dd_version=dd_version)
                    ionstruct[tattr + "_validity"] = 0
                    ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/density_thermal; profiles_1d(*)/ion(*)/density_fast'

                # Ion pressure
                pkey = "P" + ntag
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "pressure_thermal"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                    # Fast component is not expected to be present for impurities
                    refshape = cls["PD"]["X"][""].shape
                    pfast_val = np.zeros(refshape)
                    pfast_err = np.zeros(refshape)
                    pfast_lerr = None
                    prov = None
                    non_physical_filter = (pfast_val - 3.0 * pfast_err) < 0.0
                    if np.any(non_physical_filter):
                        pfast_lerr = copy.deepcopy(pfast_err)
                        pfast_lerr[non_physical_filter] = pfast_val[non_physical_filter] / 3.0

                    # Assumes 2/3 and 1/3 split into perpendicular and parallel directions respectively (isotropic pressure assumption)
                    eattr = "pressure_fast_perpendicular"
                    perfrac = 2.0 / 3.0
                    pfast_per_val = perfrac * pfast_val
                    pfast_per_err = perfrac * pfast_err
                    pfast_per_lerr = perfrac * pfast_lerr if pfast_lerr is not None else None
                    ionstruct = add_ids_dict_value(ionstruct, eattr, values=pfast_per_val, upper_errors=pfast_per_err, lower_errors=pfast_per_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[eattr] = prov

                    aattr = "pressure_fast_parallel"
                    parfrac = 1.0 / 3.0
                    pfast_par_val = parfrac * pfast_val
                    pfast_par_err = parfrac * pfast_err
                    pfast_par_lerr = parfrac * pfast_lerr if pfast_lerr is not None else None
                    ionstruct = add_ids_dict_value(ionstruct, aattr, values=pfast_par_val, upper_errors=pfast_par_err, lower_errors=pfast_par_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[aattr] = prov

                    # Summing thermal and fast
                    tattr = "pressure"
                    ptot_val = val + pfast_val
                    ptot_err = np.sqrt(np.power(err, 2.0) + np.power(pfast_err, 2.0))
                    ptot_lerr = None
                    non_physical_filter = (ptot_val - 3.0 * ptot_err) < 0.0
                    if np.any(non_physical_filter):
                        ptot_lerr = copy.deepcopy(ptot_err)
                        ptot_lerr[non_physical_filter] = ptot_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, tattr, values=ptot_val, upper_errors=ptot_err, lower_errors=ptot_lerr, dd_version=dd_version)
#                    ionstruct[tattr + "_validity"] = 0
                    ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/pressure_thermal; profiles_1d(*)/ion(*)/pressure_fast_perpendicular; profiles_1d(*)/ion(*)/pressure_fast_parallel'

                # Ion toroidal rotation frequency
                pkey = "AFTOR" + ntag
                gkey = "AFTOR"
                if pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "rotation_frequency_tor"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]
                elif gkey in cls["PD"] and cls["PD"][gkey][""] is not None:
                    attr = "rotation_frequency_tor"
                    val = cls["PD"][gkey][""].copy()
                    err = cls["PD"][gkey]["EB"].copy()
                    lerr = None
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][gkey] and cls["PD"][gkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][gkey]["PROV"]

                # Ion species metadata
                akey = "A" + ntag
                zkey = "Z" + ntag
                mkey = "MAT" + ntag
                if ionstruct and akey in cls["META"] and cls["META"][akey] is not None and zkey in cls["META"] and cls["META"][zkey] is not None:
                    # These species only have one element
                    elemstruct = {}
                    elemstruct["a"] = float(cls["META"][akey])
                    elemstruct["z_n"] = float(cls["META"][zkey])
                    elemstruct["atoms_n"] = 1
                    if "element" not in ionstruct:
                        ionstruct["element"] = []
                    ionstruct["element"].append(elemstruct)
                    if mkey in cls["META"] and cls["META"][mkey] is not None:
                        ionstruct["label"] = cls["META"][mkey] + ("+%d" % (cls["META"][zkey]))
                    ionstruct["multiple_states_flag"] = 1
                    ionstruct["state"] = []
                    # The flat charge state assumption is not always correct - what can this be replaced with?
                    zion = float(cls["META"][zkey]) if float(cls["META"][zkey]) < 46.0 else 46.0
                    ionstruct["z_ion"] = zion
                    ionstruct["z_ion_1d"] = np.full(cls["PD"]["X"][""].shape, zion)
                    ionstruct["z_ion_square_1d"] = np.power(ionstruct["z_ion_1d"], 2.0)

                if ionstruct:
                    structkey = "ion"
                    if structkey not in profiles:
                        profiles[structkey] = []
                    profiles[structkey].append(ionstruct)
                    ii = len(profiles[structkey])
                    istr = '(%d)' % (ii - 1)
                    for key, val in ionstruct_prov.items():
                        nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                        profiles_prov[structkey + istr + "/" + key] = nval

            # Estimate impurity data
            for nn in range(cls["META"]["NUMZ"]):

                ionstruct = {}
                ionstruct_prov = {}
                ntag = "Z%d" % (nn + 1)

                # Ion temperature
                pkey = "T" + ntag
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "temperature"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                # Ion density
                pkey = "N" + ntag
                if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "density_thermal"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                    # Fast component is not expected to be present for impurities
                    refshape = cls["PD"]["X"][""].shape
                    fattr = "density_fast"
                    nfast_val = np.zeros(refshape)
                    nfast_err = np.zeros(refshape)
                    nfast_lerr = None
                    prov = None
                    non_physical_filter = (nfast_val - 3.0 * nfast_err) < 0.0
                    if np.any(non_physical_filter):
                        nfast_lerr = copy.deepcopy(nfast_err)
                        nfast_lerr[non_physical_filter] = nfast_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, fattr, values=nfast_val, upper_errors=nfast_err, lower_errors=nfast_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[fattr] = prov

                    # Summing thermal and fast
                    tattr = "density"
                    ntot_val = val + nfast_val
                    ntot_err = np.sqrt(np.power(err, 2.0) + np.power(nfast_err, 2.0))
                    ntot_lerr = None
                    non_physical_filter = (ntot_val - 3.0 * ntot_err) < 0.0
                    if np.any(non_physical_filter):
                        ntot_lerr = copy.deepcopy(ntot_err)
                        ntot_lerr[non_physical_filter] = ntot_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, tattr, values=ntot_val, upper_errors=ntot_err, lower_errors=ntot_lerr, dd_version=dd_version)
                    ionstruct[tattr + "_validity"] = 0
                    ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/density_thermal; profiles_1d(*)/ion(*)/density_fast'

                # Ion pressure
                pkey = "P" + ntag
                if pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "pressure_thermal"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    non_physical_filter = (val - 3.0 * err) < 0.0
                    if np.any(non_physical_filter):
                        lerr = copy.deepcopy(err)
                        lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]

                    # Fast component is not expected to be present for impurities
                    refshape = cls["PD"]["X"][""].shape
                    pfast_val = np.zeros(refshape)
                    pfast_err = np.zeros(refshape)
                    pfast_lerr = None
                    prov = None
                    non_physical_filter = (pfast_val - 3.0 * pfast_err) < 0.0
                    if np.any(non_physical_filter):
                        pfast_lerr = copy.deepcopy(pfast_err)
                        pfast_lerr[non_physical_filter] = pfast_val[non_physical_filter] / 3.0

                    # Assumes 2/3 and 1/3 split into perpendicular and parallel directions respectively (isotropic pressure assumption)
                    eattr = "pressure_fast_perpendicular"
                    perfrac = 2.0 / 3.0
                    pfast_per_val = perfrac * pfast_val
                    pfast_per_err = perfrac * pfast_err
                    pfast_per_lerr = perfrac * pfast_lerr if pfast_lerr is not None else None
                    ionstruct = add_ids_dict_value(ionstruct, eattr, values=pfast_per_val, upper_errors=pfast_per_err, lower_errors=pfast_per_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[eattr] = prov

                    aattr = "pressure_fast_parallel"
                    parfrac = 1.0 / 3.0
                    pfast_par_val = parfrac * pfast_val
                    pfast_par_err = parfrac * pfast_err
                    pfast_par_lerr = parfrac * pfast_lerr if pfast_lerr is not None else None
                    ionstruct = add_ids_dict_value(ionstruct, aattr, values=pfast_par_val, upper_errors=pfast_par_err, lower_errors=pfast_par_lerr, dd_version=dd_version)
                    if prov is not None:
                        ionstruct_prov[aattr] = prov

                    # Summing thermal and fast
                    tattr = "pressure"
                    ptot_val = val + pfast_val
                    ptot_err = np.sqrt(np.power(err, 2.0) + np.power(pfast_err, 2.0))
                    ptot_lerr = None
                    non_physical_filter = (ptot_val - 3.0 * ptot_err) < 0.0
                    if np.any(non_physical_filter):
                        ptot_lerr = copy.deepcopy(ptot_err)
                        ptot_lerr[non_physical_filter] = ptot_val[non_physical_filter] / 3.0
                    ionstruct = add_ids_dict_value(ionstruct, tattr, values=ptot_val, upper_errors=ptot_err, lower_errors=ptot_lerr, dd_version=dd_version)
#                    ionstruct[tattr + "_validity"] = 0
                    ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/pressure_thermal; profiles_1d(*)/ion(*)/pressure_fast_perpendicular; profiles_1d(*)/ion(*)/pressure_fast_parallel'

                # Ion toroidal rotation frequency
                pkey = "AFTOR" + ntag
                gkey = "AFTOR"
                if pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                    attr = "rotation_frequency_tor"
                    val = cls["PD"][pkey][""].copy()
                    err = cls["PD"][pkey]["EB"].copy()
                    lerr = None
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]
                elif gkey in cls["PD"] and cls["PD"][gkey][""] is not None:
                    attr = "rotation_frequency_tor"
                    val = cls["PD"][gkey][""].copy()
                    err = cls["PD"][gkey]["EB"].copy()
                    lerr = None
                    ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                    ionstruct[attr + "_validity"] = 0
                    if "PROV" in cls["PD"][gkey] and cls["PD"][gkey]["PROV"] is not None:
                        ionstruct_prov[attr] = cls["PD"][gkey]["PROV"]

                # Ion species metadata
                akey = "A" + ntag
                zkey = "Z" + ntag
                mkey = "MAT" + ntag
                if ionstruct and akey in cls["META"] and cls["META"][akey] is not None and zkey in cls["META"] and cls["META"][zkey] is not None:
                    # These species only have one element
                    elemstruct = {}
                    elemstruct["a"] = float(cls["META"][akey])
                    elemstruct["z_n"] = float(cls["META"][zkey])
                    elemstruct["atoms_n"] = 1
                    if "element" not in ionstruct:
                        ionstruct["element"] = []
                    ionstruct["element"].append(elemstruct)
                    if mkey in cls["META"] and cls["META"][mkey] is not None:
                        ionstruct["label"] = cls["META"][mkey] + ("+%d" % (cls["META"][zkey]))
                    ionstruct["multiple_states_flag"] = 1
                    ionstruct["state"] = []
                    # The flat charge state assumption is not always correct - what can this be replaced with?
                    zion = float(cls["META"][zkey]) if float(cls["META"][zkey]) < 46.0 else 46.0
                    ionstruct["z_ion"] = zion
                    ionstruct["z_ion_1d"] = np.full(cls["PD"]["X"][""].shape, zion)
                    ionstruct["z_ion_square_1d"] = np.power(ionstruct["z_ion_1d"], 2.0)

                if ionstruct:
                    structkey = "ion"
                    if structkey not in profiles:
                        profiles[structkey] = []
                    profiles[structkey].append(ionstruct)
                    ii = len(profiles[structkey])
                    istr = '(%d)' % (ii - 1)
                    for key, val in ionstruct_prov.items():
                        nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                        profiles_prov[structkey + istr + "/" + key] = nval

        else:

            if cls.isFitted() and "ZEFF" in cls["PD"] and cls["PD"]["ZEFF"][""] is not None and "TE" in cls["PD"] and cls["PD"]["TE"][""] is not None and "NE" in cls["PD"] and cls["PD"]["NE"][""] is not None and "V" in cls["PD"] and cls["PD"]["V"][""] is not None and "POWRAD" in cls["ZD"] and cls["ZD"]["POWRAD"][""] is not None:

                ee = 1.60217662e-19
                prad = np.abs(cls["ZD"]["POWRADBULK"][""]) if "POWRADBULK" in cls["ZD"] and cls["ZD"]["POWRADBULK"][""] is not None else np.abs(cls["ZD"]["POWRAD"][""])
                imp_type = 1
                if cls["META"]["MATWALL"] == 'C':
                    imp_type = 1
                elif cls["META"]["MATWALL"] == 'Be':
                    imp_type = 3
                impurity_data = qtools.optimize_density_fractions(cls["PD"]["NE"][""], cls["PD"]["TE"][""], cls["PD"]["V"][""], cls["PD"]["ZEFF"][""], prad, style=imp_type, itag=cls["META"]["MATI1"])
                fneg = False
                for ii in range(len(impurity_data)):
                    if np.any(impurity_data[ii][1] <= 0.0):
                        fneg = True
                if fneg:
                    imp_type = imp_type - 1
                    impurity_data = qtools.optimize_density_fractions(cls["PD"]["NE"][""], cls["PD"]["TE"][""], cls["PD"]["V"][""], cls["PD"]["ZEFF"][""], prad, style=imp_type, itag=cls["META"]["MATI1"])
                    

                temp_iondata = {"TAGS": []}
                neprov = "; " + cls["PD"]["NE"]["PROV"] if "PROV" in cls["PD"]["NE"] and cls["PD"]["NE"]["PROV"] is not None else ""
                tiprov = cls["PD"]["TI1"]["PROV"] if "PROV" in cls["PD"]["TI1"] and cls["PD"]["TI1"]["PROV"] is not None else "EX2GK: Internal estimation"

                for ii in range(len(impurity_data)):

                    itag = "I1" if ii == 0 else "Z%d" % (ii)
                    temp_iondata["TAGS"].append(itag)
                    temp_iondata["MAT"+itag] = impurity_data[ii][0]
                    temp_iondata["N"+itag] = {"": impurity_data[ii][1] * cls["PD"]["NE"][""], "EB": impurity_data[ii][1] * cls["PD"]["NE"]["EB"], "PROV": "EX2GK: Internal calculation" + neprov}
                    temp_iondata["T"+itag] = {"": cls["PD"]["TI1"][""].copy(), "EB": cls["PD"]["TI1"]["EB"], "PROV": tiprov}
                    temp_iondata["Z"+itag] = impurity_data[ii][2]
                    if itag.startswith("Z") and "TZ1" in cls["PD"] and cls["PD"]["TZ1"][""] is not None:
                        temp_iondata["T"+itag][""] = cls["PD"]["TZ1"][""].copy()
                        temp_iondata["T"+itag]["EB"] = cls["PD"]["TZ1"]["EB"].copy()
                        temp_iondata["T"+itag]["PROV"] = cls["PD"]["TZ1"]["PROV"] if "PROV" in cls["PD"]["TZ1"] and cls["PD"]["TZ1"]["PROV"] is not None else "EX2GK: Internal estimation"
                    if itag.startswith("Z") and "TIMP1" in cls["PD"] and cls["PD"]["TIMP1"][""] is not None:
                        temp_iondata["T"+itag][""] = cls["PD"]["TIMP1"][""].copy()
                        temp_iondata["T"+itag]["EB"] = cls["PD"]["TIMP1"]["EB"].copy()
                        temp_iondata["T"+itag]["PROV"] = cls["PD"]["TIMP1"]["PROV"] if "PROV" in cls["PD"]["TIMP1"] and cls["PD"]["TIMP1"]["PROV"] is not None else "EX2GK: Internal estimation"
                    temp_iondata["P"+itag] = {
                        "": ee * temp_iondata["N"+itag][""] * temp_iondata["T"+itag][""],
                        "EB": ee * np.sqrt(np.power(temp_iondata["N"+itag]["EB"] * temp_iondata["T"+itag][""], 2.0) + np.power(temp_iondata["N"+itag][""] * temp_iondata["T"+itag]["EB"], 2.0))
                    }
                    if itag.startswith("I") and cls["META"]["NUMI"] > 1 and cls["META"]["WGTI1"] < 0.95:
                        for ii in range(1, cls["META"]["NUMI"]):
                            ntag = "I%d" % (ii + 1)
                            temp_iondata["TAGS"].append(ntag)
                            temp_iondata["MAT"+ntag] = cls["META"]["MAT"+ntag]
                            temp_iondata["N"+ntag] = {"": cls["META"]["WGT"+ntag] * temp_iondata["N"+itag][""], "EB": cls["META"]["WGT"+ntag] * temp_iondata["N"+itag]["EB"], "PROV": temp_iondata["N"+itag]["PROV"]}
                            temp_iondata["T"+ntag] = {"": temp_iondata["T"+itag][""].copy(), "EB": temp_iondata["T"+itag]["EB"].copy(), "PROV": temp_iondata["T"+itag]["PROV"]}
                            temp_iondata["Z"+ntag] = temp_iondata["Z"+itag].copy()
                            temp_iondata["P"+ntag] = {
                                "": ee * temp_iondata["N"+ntag][""] * temp_iondata["T"+ntag][""],
                                "EB": ee * np.sqrt(np.power(temp_iondata["N"+ntag]["EB"] * temp_iondata["T"+ntag][""], 2.0) + np.power(temp_iondata["N"+ntag][""] * temp_iondata["T"+ntag]["EB"], 2.0))
                            }
                        temp_iondata["N"+itag][""] = cls["META"]["WGT"+itag] * temp_iondata["N"+itag][""]
                        temp_iondata["N"+itag]["EB"] = cls["META"]["WGT"+itag] * temp_iondata["N"+itag]["EB"]
                        temp_iondata["P"+itag] = {
                            "": ee * temp_iondata["N"+itag][""] * temp_iondata["T"+itag][""],
                            "EB": ee * np.sqrt(np.power(temp_iondata["N"+itag]["EB"] * temp_iondata["T"+itag][""], 2.0) + np.power(temp_iondata["N"+itag][""] * temp_iondata["T"+itag]["EB"], 2.0))
                        }

                for ntag in temp_iondata["TAGS"]:

                    ionstruct = {}
                    ionstruct_prov = {}

                    # Ion temperature
                    pkey = "T" + ntag
                    rkey = "T" + ntag
                    if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
                        fitstruct = {}
                        fitstruct_prov = {}
                        val = cls["RD"][rkey][""].copy()
                        err = cls["RD"][rkey]["EB"].copy()
                        fitstruct["measured"] = val
                        fitstruct["measured_error_upper"] = err
                        weight = np.ones(val.shape)
                        fitstruct["weight"] = weight
                        fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                        #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                        #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
                        fitstruct["time_measurement"] = np.full(val.shape, float(tm))
                        fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
                        if cls.isFitted() and pkey in temp_iondata and temp_iondata[pkey][""] is not None:
                            rfunc = interp1d(cls["PD"]["X"][""], temp_iondata[pkey][""], kind='linear', bounds_error=False, fill_value='extrapolate')
                            recon = rfunc(cls["RD"][rkey]["X"])
                            fitstruct["reconstructed"] = recon
                            chi2 = np.full(val.shape, 1000.0)
                            zfilt = (err > 0.0)
                            if np.any(zfilt):
                                chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                            fitstruct["chi_squared"] = chi2
                            if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                                fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
                        if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                            fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
                        if fitstruct:
                            structkey = "temperature_fit"
                            ionstruct[structkey] = fitstruct
                            for key, val in fitstruct_prov.items():
                                ionstruct_prov[structkey + "/" + key] = val
                    if pkey in temp_iondata and temp_iondata[pkey][""] is not None:
                        attr = "temperature"
                        val = temp_iondata[pkey][""].copy()
                        err = temp_iondata[pkey]["EB"].copy()
                        lerr = None
                        non_physical_filter = (val - 3.0 * err) < 0.0
                        if np.any(non_physical_filter):
                            lerr = copy.deepcopy(err)
                            lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                        ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        ionstruct[attr + "_validity"] = 0
                        if "PROV" in temp_iondata[pkey] and temp_iondata[pkey]["PROV"] is not None:
                            ionstruct_prov[attr] = temp_iondata[pkey]["PROV"]

                    # Ion density
                    pkey = "N" + ntag
                    if pkey in temp_iondata and temp_iondata[pkey][""] is not None:
                        attr = "density_thermal"
                        val = temp_iondata[pkey][""].copy()
                        err = temp_iondata[pkey]["EB"].copy()
                        lerr = None
                        non_physical_filter = (val - 3.0 * err) < 0.0
                        if np.any(non_physical_filter):
                            lerr = copy.deepcopy(err)
                            lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                        ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in temp_iondata[pkey] and temp_iondata[pkey]["PROV"] is not None:
                            ionstruct_prov[attr] = temp_iondata[pkey]["PROV"]

                        # Fast component is not expected to be present for impurities
                        refshape = cls["PD"]["X"][""].shape
                        fattr = "density_fast"
                        nfast_val = np.zeros(refshape)
                        nfast_err = np.zeros(refshape)
                        nfast_lerr = None
                        prov = None
                        non_physical_filter = (nfast_val - 3.0 * nfast_err) < 0.0
                        if np.any(non_physical_filter):
                            nfast_lerr = copy.deepcopy(nfast_err)
                            nfast_lerr[non_physical_filter] = nfast_val[non_physical_filter] / 3.0
                        ionstruct = add_ids_dict_value(ionstruct, fattr, values=nfast_val, upper_errors=nfast_err, lower_errors=nfast_lerr, dd_version=dd_version)
                        if prov is not None:
                            ionstruct_prov[fattr] = prov

                        # Summing thermal and fast
                        tattr = "density"
                        ntot_val = val + nfast_val
                        ntot_err = np.sqrt(np.power(err, 2.0) + np.power(nfast_err, 2.0))
                        ntot_lerr = None
                        non_physical_filter = (ntot_val - 3.0 * ntot_err) < 0.0
                        if np.any(non_physical_filter):
                            ntot_lerr = copy.deepcopy(ntot_err)
                            ntot_lerr[non_physical_filter] = ntot_val[non_physical_filter] / 3.0
                        ionstruct = add_ids_dict_value(ionstruct, tattr, values=ntot_val, upper_errors=ntot_err, lower_errors=ntot_lerr, dd_version=dd_version)
                        ionstruct[tattr + "_validity"] = 0
                        ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/density_thermal; profiles_1d(*)/ion(*)/density_fast'

                    # Ion pressure
                    pkey = "P" + ntag
                    if pkey in temp_iondata and temp_iondata[pkey][""] is not None:
                        attr = "pressure_thermal"
                        val = temp_iondata[pkey][""].copy()
                        err = temp_iondata[pkey]["EB"].copy()
                        lerr = None
                        non_physical_filter = (val - 3.0 * err) < 0.0
                        if np.any(non_physical_filter):
                            lerr = copy.deepcopy(err)
                            lerr[non_physical_filter] = val[non_physical_filter] / 3.0
                        ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in temp_iondata[pkey] and temp_iondata[pkey]["PROV"] is not None:
                            ionstruct_prov[attr] = temp_iondata[pkey]["PROV"]

                        # Fast component is not expected to be present for impurities
                        refshape = cls["PD"]["X"][""].shape
                        pfast_val = np.zeros(refshape)
                        pfast_err = np.zeros(refshape)
                        pfast_lerr = None
                        prov = None
                        non_physical_filter = (pfast_val - 3.0 * pfast_err) < 0.0
                        if np.any(non_physical_filter):
                            pfast_lerr = copy.deepcopy(pfast_err)
                            pfast_lerr[non_physical_filter] = pfast_val[non_physical_filter] / 3.0

                        # Assumes 2/3 and 1/3 split into perpendicular and parallel directions respectively (isotropic pressure assumption)
                        eattr = "pressure_fast_perpendicular"
                        perfrac = 2.0 / 3.0
                        pfast_per_val = perfrac * pfast_val
                        pfast_per_err = perfrac * pfast_err
                        pfast_per_lerr = perfrac * pfast_lerr if pfast_lerr is not None else None
                        ionstruct = add_ids_dict_value(ionstruct, eattr, values=pfast_per_val, upper_errors=pfast_per_err, lower_errors=pfast_per_lerr, dd_version=dd_version)
                        if prov is not None:
                            ionstruct_prov[eattr] = prov

                        aattr = "pressure_fast_parallel"
                        parfrac = 1.0 / 3.0
                        pfast_par_val = parfrac * pfast_val
                        pfast_par_err = parfrac * pfast_err
                        pfast_per_lerr = perfrac * pfast_lerr if pfast_lerr is not None else None
                        ionstruct = add_ids_dict_value(ionstruct, aattr, values=pfast_par_val, upper_errors=pfast_par_err, lower_errors=pfast_par_lerr, dd_version=dd_version)
                        if prov is not None:
                            ionstruct_prov[aattr] = prov

                        # Summing thermal and fast
                        tattr = "pressure"
                        ptot_val = val + pfast_val
                        ptot_err = np.sqrt(np.power(err, 2.0) + np.power(pfast_err, 2.0))
                        ptot_lerr = None
                        non_physical_filter = (ptot_val - 3.0 * ptot_err) < 0.0
                        if np.any(non_physical_filter):
                            ptot_lerr = copy.deepcopy(ptot_err)
                            ptot_lerr[non_physical_filter] = ptot_val[non_physical_filter] / 3.0
                        ionstruct = add_ids_dict_value(ionstruct, tattr, values=ptot_val, upper_errors=ptot_err, lower_errors=ptot_lerr, dd_version=dd_version)
#                        ionstruct[tattr + "_validity"] = 0
                        ionstruct_prov[tattr] = 'profiles_1d(*)/ion(*)/pressure_thermal; profiles_1d(*)/ion(*)/pressure_fast_perpendicular; profiles_1d(*)/ion(*)/pressure_fast_parallel'

                    # Ion toroidal rotation frequency
                    pkey = "AFTOR" + ntag
                    gkey = "AFTOR"
                    if pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                        attr = "rotation_frequency_tor"
                        val = cls["PD"][pkey][""].copy()
                        err = cls["PD"][pkey]["EB"].copy()
                        lerr = None
                        ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                        ionstruct[attr + "_validity"] = 0
                        if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                            ionstruct_prov[attr] = cls["PD"][pkey]["PROV"]
                    if gkey in cls["PD"] and cls["PD"][gkey][""] is not None:
                        attr = "rotation_frequency_tor"
                        val = cls["PD"][gkey][""].copy()
                        err = cls["PD"][gkey]["EB"].copy()
                        lerr = None
                        ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#                        ionstruct[attr + "_validity"] = 0
                        if "PROV" in cls["PD"][gkey] and cls["PD"][gkey]["PROV"] is not None:
                            ionstruct_prov[attr] = cls["PD"][gkey]["PROV"]

                    # Ion species metadata
                    mkey = "MAT" + ntag
                    if ionstruct and mkey in temp_iondata and temp_iondata[mkey] is not None:
                        # These species only have one element
                        (sn, sa, sz) = ptools.define_ion_species(short_name=temp_iondata[mkey])
                        elemstruct = {}
                        elemstruct["a"] = float(sa)
                        elemstruct["z_n"] = float(sz)
                        elemstruct["atoms_n"] = 1
                        if "element" not in ionstruct:
                            ionstruct["element"] = []
                        ionstruct["element"].append(elemstruct)
                        ionstruct["label"] = sn + ("+%d" % (sz))
                        ionstruct["multiple_states_flag"] = 1
                        ionstruct["state"] = []
                        # The flat charge state assumption is not always correct - what can this be replaced with?
                        ionstruct["z_ion"] = sz
                        ionstruct["z_ion_1d"] = temp_iondata["Z"+ntag]
                        ionstruct["z_ion_square_1d"] = np.power(ionstruct["z_ion_1d"], 2.0)

                    if ionstruct:
                        structkey = "ion"
                        if structkey not in profiles:
                            profiles[structkey] = []
                        profiles[structkey].append(ionstruct)
                        ii = len(profiles[structkey])
                        istr = '(%d)' % (ii - 1)
                        for key, val in ionstruct_prov.items():
                            nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                            profiles_prov[structkey + istr + "/" + key] = nval

        # Bulk toroidal rotation frequency
#        key = "AFTOR"
#        if cls.isFitted() and key in cls["PD"] and cls["PD"][key][""] is not None:
#            attr = "rotation_frequency_tor"
#            val = cls["PD"][key][""].copy()
#            err = cls["PD"][key]["EB"].copy()
#            lerr = None
#            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
##            profiles[attr + "_validity"] = 0
#            if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
#                profiles_prov[attr] = cls["PD"][key]["PROV"]
        key = "AFTOR"
        if cls.isFitted() and key in cls["PD"] and cls["PD"][key][""] is not None:
            attr = "rotation_frequency_tor_sonic"
            val = cls["PD"][key][""].copy()
            err = cls["PD"][key]["EB"].copy()
            lerr = None
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#            profiles[attr + "_validity"] = 0
            if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][key]["PROV"]

        # Effective charge
        pkey = "ZEFFP"
        rkey = "ZEFF"
        if include_raw and rkey in cls["RD"] and cls["RD"][rkey][""] is not None:
            fitstruct = {}
            fitstruct_prov = {}
            val = cls["RD"][rkey][""].copy()
            err = cls["RD"][rkey]["EB"].copy()
            fitstruct["measured"] = val
            fitstruct["measured_error_upper"] = err
            weight = np.ones(val.shape)
            fitstruct["weight"] = weight
            fitstruct["rho_tor_norm"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_upper"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] + cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            #fitstruct["rho_tor_norm_error_lower"] = itemgetter(0)(cls["CD"].convert(cls["RD"][rkey]["X"] - cls["RD"][rkey]["XEB"], cls["RD"][rkey].coordinate, "RHOTORN", cls["RD"][rkey].coord_prefix, cs_prefix))
            fitstruct["time_measurement"] = np.full(val.shape, float(tm))
            fitstruct["time_measurement_width"] = np.full(val.shape, float(tw))
            if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None:
                recon = cls["PD"].interpolate(cls["RD"][rkey]["X"], "X", pkey)
                fitstruct["reconstructed"] = recon
                chi2 = np.full(val.shape, 1000.0)
                zfilt = (err > 0.0)
                if np.any(zfilt):
                    chi2[zfilt] = np.power(weight[zfilt] * (recon[zfilt] - val[zfilt]) / err[zfilt], 2.0)
                fitstruct["chi_squared"] = chi2
                if "FSO" in cls and rkey in cls["FSO"] and len(cls["FSO"][rkey]) > 0:
                    fitstruct["parameters"] = cls["FSO"][rkey][0].exportToXMLString()
            if "PROV" in cls["RD"][rkey] and cls["RD"][rkey]["PROV"] is not None:
                fitstruct_prov["measured"] = cls["RD"][rkey]["PROV"]
            if fitstruct:
                structkey = "zeff_fit"
                profiles[structkey] = fitstruct
                for key, val in fitstruct_prov.items():
                    profiles_prov[structkey + "/" + key] = val
        if cls.isFitted() and pkey in cls["PD"] and cls["PD"][pkey][""] is not None and not cls["FLAG"].checkFlag("FLATZEFF"):
            attr = "zeff"
            val = cls["PD"][pkey][""].copy()
            err = cls["PD"][pkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 1.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = (val[non_physical_filter] - 1.0) / 3.0
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#            profiles[attr + "_validity"] = 0
            if "PROV" in cls["PD"][pkey] and cls["PD"][pkey]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][pkey]["PROV"]
        elif cls.isFitted() and rkey in cls["PD"] and cls["PD"][rkey][""] is not None:
            attr = "zeff"
            val = cls["PD"][rkey][""].copy()
            err = cls["PD"][rkey]["EB"].copy()
            lerr = None
            non_physical_filter = (val - 3.0 * err) < 1.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = (val[non_physical_filter] - 1.0) / 3.0
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#            profiles[attr + "_validity"] = 0
            if "PROV" in cls["PD"][rkey] and cls["PD"][rkey]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][rkey]["PROV"]

        # Safety factor
        key = "Q" + cs_prefix
        if cls.isFitted() and key in cls["PD"] and cls["PD"][key][""] is not None:
            attr = "q"
            val = cls["PD"][key][""].copy()
            err = cls["PD"][key]["EB"].copy()
            lerr = None
            non_physical_filter = (np.abs(val) - 3.0 * err) < 0.0
            if np.any(non_physical_filter):
                lerr = copy.deepcopy(err)
                lerr[non_physical_filter] = val[non_physical_filter] / 3.0
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
#            profiles[attr + "_validity"] = 0
            if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][key]["PROV"]

        # Ohmic plasma current
        key = "JOHM"
        if cls.isFitted() and key in cls["PD"] and cls["PD"][key][""] is not None:
            attr = "j_ohmic"
            val = cls["PD"][key][""].copy()
            err = cls["PD"][key]["EB"].copy()
            lerr = None
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][key]["PROV"]

        # Total plasma current (assume only ohmic component)
        key = "JOHM"
        if cls.isFitted() and key in cls["PD"] and cls["PD"][key][""] is not None:
            attr = "j_total"
            val = cls["PD"][key][""].copy()
            err = cls["PD"][key]["EB"].copy()
            lerr = None
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][key]["PROV"]
        elif cls.isFitted():
            refshape = cls["PD"]["X"][""].shape
            attr = "j_total"
            val = np.zeros(refshape)
            err = np.zeros(refshape)
            lerr = None
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)

        # Plasma toroidal current profile (assume equal to ohmic current)
        key = "JOHM"
        if cls.isFitted() and key in cls["PD"] and cls["PD"][key][""] is not None:
            attr = "j_tor"
            val = cls["PD"][key][""].copy()
            err = cls["PD"][key]["EB"].copy()
            lerr = None
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                profiles_prov[attr] = cls["PD"][key]["PROV"]
        elif cls.isFitted():
            refshape = cls["PD"]["X"][""].shape
            attr = "j_tor"
            val = np.zeros(refshape)
            err = np.zeros(refshape)
            lerr = None
            profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)

        # General radial grid for all core profiles
        grid = {}
        grid_prov = {}
        if profiles and cls.isFitted():
            rngrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "RHOTORN", cs_prefix, cs_prefix))
            if rngrid is not None:
                attr = "rho_tor_norm"
                grid[attr] = rngrid
                if cls["META"]["CSO"] == "RHOTORN" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                    grid_prov[attr] = cls["PD"]["X"]["PROV"]
                else:
                    grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"RHOTORN"
            rtgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "PSITOR", cs_prefix, cs_prefix))
            if rtgrid is not None and "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
                # Convert COCOS 1 -> COCOS 11
                attr = "rho_tor"
                grid[attr] = np.sqrt(np.abs(2.0 * rtgrid / cls["ZD"]["BVAC"][""]))
                bprov = cls["ZD"]["BVAC"]["PROV"] if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"] is not None else "ZD_BVAC"
                if cls["META"]["CSO"] == "PSITOR" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                    grid_prov[attr] = cls["PD"]["X"]["PROV"] + "; " + bprov
                else:
                    grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"PSITOR; " + bprov
            rpgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "RHOPOLN", cs_prefix, cs_prefix))
            if rpgrid is not None:
                attr = "rho_pol_norm"
                grid[attr] = rpgrid
                if cls["META"]["CSO"] == "RHOPOLN" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                    grid_prov[attr] = cls["PD"]["X"]["PROV"]
                else:
                    grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"RHOPOLN"
            fpgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "PSIPOL", cs_prefix, cs_prefix))
            if fpgrid is not None:
                # Convert COCOS 1 -> COCOS 11
                attr = "psi"
                grid[attr] = 2.0 * np.pi * fpgrid
                if cls["META"]["CSO"] == "PSIPOL" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                    grid_prov[attr] = cls["PD"]["X"]["PROV"]
                else:
                    grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"PSIPOL"
            xagrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "FSAREA", cs_prefix, cs_prefix))
            if xagrid is not None:
                attr = "area"
                grid[attr] = xagrid
                if cls["META"]["CSO"] == "FSAREA" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                    grid_prov[attr] = cls["PD"]["X"]["PROV"]
                else:
                    grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"FSAREA"
            xvgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "FSVOL", cs_prefix, cs_prefix))
            if xvgrid is not None:
                attr = "volume"
                grid[attr] = xvgrid
                if cls["META"]["CSO"] == "FSVOL" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                    grid_prov[attr] = cls["PD"]["X"]["PROV"]
                else:
                    grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"FSVOL"
            if "PSIAXS" in cls["ZD"] and cls["ZD"]["PSIAXS"][""] is not None and "PSIBND" in cls["ZD"] and cls["ZD"]["PSIBND"][""] is not None:
                attr = "psi_magnetic_axis"
                grid[attr] = float(2.0 * np.pi * cls["ZD"]["PSIAXS"][""])
                if "PROV" in cls["ZD"]["PSIAXS"] and cls["ZD"]["PSIAXS"]["PROV"] is not None:
                    grid_prov[attr] = cls["ZD"]["PSIAXS"]["PROV"]
                attr = "psi_boundary"
                grid[attr] = float(2.0 * np.pi * cls["ZD"]["PSIBND"][""])
                if "PROV" in cls["ZD"]["PSIBND"] and cls["ZD"]["PSIBND"]["PROV"] is not None:
                    grid_prov[attr] = cls["ZD"]["PSIBND"]["PROV"]
        if grid:
            structkey = "grid"
            profiles[structkey] = grid
            for key, val in grid_prov.items():
                profiles_prov[structkey + "/" + key] = val

        if profiles:
            profiles["time"] = ids_time
            structkey = "profiles_1d"
            if structkey not in idsd:
                idsd[structkey] = []
            idsd[structkey].append(profiles)
            ii = len(idsd[structkey])
            istr = '(%d)' % (ii - 1)
            for key, val in profiles_prov.items():
                nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                idsp[structkey + istr + "/" + key] = nval

        if idsd:
            tf = {}
            tf_prov = {}
            if "RVAC" in cls["ZD"] and cls["ZD"]["RVAC"][""] is not None:
                attr = "r0"
                val = float(cls["ZD"]["RVAC"][""])
                err = float(cls["ZD"]["RVAC"]["EB"])
                lerr = None
                tf = add_ids_dict_value(tf, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                if "PROV" in cls["ZD"]["RVAC"] and cls["ZD"]["RVAC"]["PROV"]:
                    tf_prov[attr] = cls["ZD"]["RVAC"]["PROV"]
            if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
                attr = "b0"
                val = np.array([cls["ZD"]["BVAC"][""]])
                err = np.array([cls["ZD"]["BVAC"]["EB"]])
                lerr = None
                tf = add_ids_dict_value(tf, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"]:
                    tf_prov[attr] = cls["ZD"]["BVAC"]["PROV"]
            if tf:
                structkey = "vacuum_toroidal_field"
                idsd[structkey] = tf
                for key, val in tf_prov.items():
                    idsp[structkey + "/" + key] = val

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            idsd["time"] = np.array([ids_time])

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd

def pass_core_sources_ids_time_slice_values(cls, user=None, dd_version=min_imas_version, include_raw=False, use_raw=False):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        cs_prefix = cls["META"]["CSOP"]
        tm = cls["META"]["T2"]
        tw = cls["META"]["T2"] - cls["META"]["T1"]
        ids_time = float(tm) - 0.5 * float(tw)

        for source_name in cls.allowed_sources:

            source = {}
            source_prov = {}

            src = None
            if cls["ZD"].isSourcePresent(source_name):
                for key in imas_source_name_translation:
                    if source_name == key:
                        src = key

            if src is not None:

                global_data = {}
                global_data_prov = {}

                # Total deposited power, already subtracts identified losses
                ikey = "POW" + src if src == "RAD" else "POWI" + src
                lkey = "POWL" + src
                if ikey in cls["ZD"] and cls["ZD"][ikey][""] is not None:
                    attr = "power"
                    val = float(cls["ZD"][ikey][""])
                    err = float(cls["ZD"][ikey]["EB"])
                    lerr = None
                    prov = None
                    if "PROV" in cls["ZD"][ikey] and cls["ZD"][ikey]["PROV"] is not None:
                        prov = prov + "; " + cls["ZD"][ikey]["PROV"] if prov is not None else cls["ZD"][ikey]["PROV"]
                    if lkey in cls["ZD"] and cls["ZD"][lkey][""] is not None:
                        val = val - float(cls["ZD"][lkey][""])
                        err = err + float(cls["ZD"][lkey]["EB"])
                        if "PROV" in cls["ZD"][lkey] and cls["ZD"][lkey]["PROV"] is not None:
                            prov = prov + "; " + cls["ZD"][lkey]["PROV"] if prov is not None else cls["ZD"][lkey]["PROV"]
                    global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if prov is not None:
                        global_data_prov[attr] = prov

                elecstruct = {}
                elecstruct_prov = {}

                # Electron particle source
                key = "SNE" + src
                if key in cls["ZD"] and cls["ZD"][key][""] is not None:
                    attr = "particles"
                    val = float(cls["ZD"][key][""])
                    err = float(cls["ZD"][key]["EB"])
                    lerr = None
                    elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][key] and cls["ZD"][key]["PROV"] is not None:
                        elecstruct_prov[attr] = cls["ZD"][key]["PROV"]

                # Electron energy source
                key = "STE" + src
                radkey = "ST" + src
                if key in cls["ZD"] and cls["ZD"][key][""] is not None:
                    attr = "power"
                    val = float(cls["ZD"][key][""])
                    err = float(cls["ZD"][key]["EB"])
                    lerr = None
                    elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][key] and cls["ZD"][key]["PROV"] is not None:
                        elecstruct_prov[attr] = cls["ZD"][key]["PROV"]
                if src == "RAD" and radkey in cls["ZD"] and cls["ZD"][radkey][""] is not None:
                    # This branch handles the radiation sink term
                    attr = "power"
                    val = float(cls["ZD"][radkey][""])
                    err = float(cls["ZD"][radkey]["EB"])
                    lerr = None
                    elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][radkey] and cls["ZD"][radkey]["PROV"] is not None:
                        elecstruct_prov[attr] = cls["ZD"][radkey]["PROV"]

                if elecstruct:
                    structkey = "electrons"
                    global_data[structkey] = elecstruct
                    for key, val in elecstruct_prov.items():
                         global_data_prov[structkey + "/" + key] = val

                # Ion particle source
                key = "SNI" + src
                if key in cls["ZD"] and cls["ZD"][key][""] is not None:
                    attr = "total_ion_particles"
                    val = float(cls["ZD"][key][""])
                    err = float(cls["ZD"][key]["EB"])
                    lerr = None
                    global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][key] and cls["ZD"][key]["PROV"] is not None:
                        global_data_prov[attr] = cls["ZD"][key]["PROV"]

                # Ion energy source
                key = "STI" + src
                if key in cls["ZD"] and cls["ZD"][key][""] is not None:
                    attr = "total_ion_power"
                    val = float(cls["ZD"][key][""])
                    err = float(cls["ZD"][key]["EB"])
                    lerr = None
                    global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][key] and cls["ZD"][key]["PROV"] is not None:
                        global_data_prov[attr] = cls["ZD"][key]["PROV"]

                # Bulk momentum source
                key = "SP" + src
                if key in cls["ZD"] and cls["ZD"][key][""] is not None:
                    attr = "torque_tor"
                    val = float(cls["ZD"][key][""])
                    err = float(cls["ZD"][key]["EB"])
                    lerr = None
                    global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][key] and cls["ZD"][key]["PROV"] is not None:
                        global_data_prov[attr] = cls["ZD"][key]["PROV"]

                # Current source
                key = "J" + src
                if key in cls["ZD"] and cls["ZD"][key][""] is not None:
                    attr = "current_parallel"
                    val = float(cls["ZD"][key][""])
                    err = float(cls["ZD"][key]["EB"])
                    lerr = None
                    global_data = add_ids_dict_value(global_data, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                    if "PROV" in cls["ZD"][key] and cls["ZD"][key]["PROV"] is not None:
                        global_data_prov[attr] = cls["ZD"][key]["PROV"]

                if global_data:
                    global_data["time"] = ids_time
                    structkey = "global_quantities"
                    if structkey not in source:
                        source[structkey] = []
                    source[structkey].append(global_data)
                    if global_data_prov:
                        ii = len(source[structkey])
                        istr = '(%d)' % (ii - 1)
                        for key, val in global_data_prov.items():
                             nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                             source_prov[structkey + istr + "/" + key] = nval

                profiles = {}
                profiles_prov = {}

                if use_raw:
                    pass

                elif cls.isFitted():

                    refshape = cls["PD"]["X"][""].shape
                    xvgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "FSVOL", cs_prefix, cs_prefix))
                    vprov = None
                    if cls["META"]["CSO"] == "FSVOL" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                        vprov = cls["PD"]["X"]["PROV"]
                    else:
                        vprov = "EX2GK: CD_"+cs_prefix+"FSVOL"

                    elecstruct = {}
                    elecstruct_prov = {}

                    # Electron particle source
                    key = "SNE" + src
                    if key in cls["PD"] and cls["PD"][key][""] is not None:
                        attr = "particles"
                        val = cls["PD"][key][""].copy()
                        err = cls["PD"][key]["EB"].copy()
                        lerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            elecstruct_prov[attr] = cls["PD"][key]["PROV"]
                        attr = "particles_inside"
                        cval = cumtrapz(val, xvgrid, initial=0.0)
                        cerr = cumtrapz(err, xvgrid, initial=0.0)
                        clerr = cumtrapz(lerr, xvgrid, initial=0.0) if lerr is not None else None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            elecstruct_prov[attr] = cls["PD"][key]["PROV"] + "; " + vprov if vprov is not None else cls["PD"][key]["PROV"]
                    else:
                        attr = "particles"
                        val = np.zeros(refshape)
                        err = np.zeros(refshape)
                        lerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        elecstruct_prov[attr] = "EX2GK: Internal assumption"
                        attr = "particles_inside"
                        cval = np.zeros(refshape)
                        cerr = np.zeros(refshape)
                        clerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        elecstruct_prov[attr] = "EX2GK: Internal assumption"

                    # Electron heat source
                    key = "STE" + src
                    radkey = "ST" + src
                    if key in cls["PD"] and cls["PD"][key][""] is not None:
                        attr = "energy"
                        val = cls["PD"][key][""].copy()
                        err = cls["PD"][key]["EB"].copy()
                        lerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            elecstruct_prov[attr] = cls["PD"][key]["PROV"]
                        attr = "power_inside"
                        cval = cumtrapz(val, xvgrid, initial=0.0)
                        cerr = cumtrapz(err, xvgrid, initial=0.0)
                        clerr = cumtrapz(lerr, xvgrid, initial=0.0) if lerr is not None else None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            elecstruct_prov[attr] = cls["PD"][key]["PROV"] + "; " + vprov if vprov is not None else cls["PD"][key]["PROV"]
                    elif src == "RAD" and radkey in cls["PD"] and cls["PD"][radkey][""] is not None:
                        # This branch handles the radiation sink term
                        attr = "energy"
                        val = cls["PD"][radkey][""].copy()
                        err = cls["PD"][radkey]["EB"].copy()
                        lerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][radkey] and cls["PD"][radkey]["PROV"] is not None:
                            elecstruct_prov[attr] = cls["PD"][radkey]["PROV"]
                        attr = "power_inside"
                        cval = cumtrapz(val, xvgrid, initial=0.0)
                        cerr = cumtrapz(err, xvgrid, initial=0.0)
                        clerr = cumtrapz(lerr, xvgrid, initial=0.0) if lerr is not None else None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][radkey] and cls["PD"][radkey]["PROV"] is not None:
                            elecstruct_prov[attr] = cls["PD"][radkey]["PROV"] + "; " + vprov if vprov is not None else cls["PD"][radkey]["PROV"]
                    else:
                        attr = "energy"
                        val = np.zeros(refshape)
                        err = np.zeros(refshape)
                        lerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        elecstruct_prov[attr] = "EX2GK: Internal assumption"
                        attr = "power_inside"
                        cval = np.zeros(refshape)
                        cerr = np.zeros(refshape)
                        clerr = None
                        elecstruct = add_ids_dict_value(elecstruct, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        elecstruct_prov[attr] = "EX2GK: Internal assumption"

                    if elecstruct:
                        structkey = "electrons"
                        profiles[structkey] = elecstruct
                        for key, val in elecstruct_prov.items():
                             profiles_prov[structkey + "/" + key] = val

                    # Total ion heat source
                    key = "STI" + src
                    if key in cls["PD"] and cls["PD"][key][""] is not None:
                        attr = "total_ion_energy"
                        val = cls["PD"][key][""].copy()
                        err = cls["PD"][key]["EB"].copy()
                        lerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            profiles_prov[attr] = cls["PD"][key]["PROV"]
                        attr = "total_ion_power_inside"
                        cval = cumtrapz(val, xvgrid, initial=0.0)
                        cerr = cumtrapz(err, xvgrid, initial=0.0)
                        clerr = cumtrapz(lerr, xvgrid, initial=0.0) if lerr is not None else None
                        profiles = add_ids_dict_value(profiles, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            profiles_prov[attr] = cls["PD"][key]["PROV"] + "; " + vprov if vprov is not None else cls["PD"][key]["PROV"]
                    else:
                        attr = "total_ion_energy"
                        val = np.zeros(refshape)
                        err = np.zeros(refshape)
                        lerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        profiles_prov[attr] = "EX2GK: Internal assumption"
                        attr = "total_ion_power_inside"
                        cval = np.zeros(refshape)
                        cerr = np.zeros(refshape)
                        clerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        profiles_prov[attr] = "EX2GK: Internal assumption"

                    # Bulk momentum source
                    key = "SP" + src
                    if key in cls["PD"] and cls["PD"][key][""] is not None:
                        attr = "momentum_tor"
                        val = cls["PD"][key][""].copy()
                        err = cls["PD"][key]["EB"].copy()
                        lerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            profiles_prov[attr] = cls["PD"][key]["PROV"]
                        attr = "torque_tor_inside"
                        cval = cumtrapz(val, xvgrid, initial=0.0)
                        cerr = cumtrapz(err, xvgrid, initial=0.0)
                        clerr = cumtrapz(lerr, xvgrid, initial=0.0) if lerr is not None else None
                        profiles = add_ids_dict_value(profiles, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            profiles_prov[attr] = cls["PD"][key]["PROV"] + "; " + vprov if vprov is not None else cls["PD"][key]["PROV"]
                    else:
                        attr = "momentum_tor"
                        val = np.zeros(refshape)
                        err = np.zeros(refshape)
                        lerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        profiles_prov[attr] = "EX2GK: Internal assumption"
                        attr = "torque_tor_inside"
                        cval = np.zeros(refshape)
                        cerr = np.zeros(refshape)
                        clerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        profiles_prov[attr] = "EX2GK: Internal assumption"

                    # Current source
                    key = "J" + src
                    if key in cls["PD"] and cls["PD"][key][""] is not None:
                        attr = "j_parallel"
                        val = cls["PD"][key][""].copy()
                        err = cls["PD"][key]["EB"].copy()
                        lerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            profiles_prov[attr] = cls["PD"][key]["PROV"]
                        attr = "current_parallel_inside"
                        cval = cumtrapz(val, xvgrid, initial=0.0)
                        cerr = cumtrapz(err, xvgrid, initial=0.0)
                        clerr = cumtrapz(lerr, xvgrid, initial=0.0) if lerr is not None else None
                        profiles = add_ids_dict_value(profiles, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        if "PROV" in cls["PD"][key] and cls["PD"][key]["PROV"] is not None:
                            profiles_prov[attr] = cls["PD"][key]["PROV"] + "; " + vprov if vprov is not None else cls["PD"][key]["PROV"]
                    else:
                        attr = "j_parallel"
                        val = np.zeros(refshape)
                        err = np.zeros(refshape)
                        lerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                        profiles_prov[attr] = "EX2GK: Internal assumption"
                        attr = "current_parallel_inside"
                        cval = np.zeros(refshape)
                        cerr = np.zeros(refshape)
                        clerr = None
                        profiles = add_ids_dict_value(profiles, attr, values=cval, upper_errors=cerr, lower_errors=clerr, dd_version=dd_version)
                        profiles_prov[attr] = "EX2GK: Internal assumption"

                    # Ion sources - required for ion metadata, but profiles typically come from total
                    #     Fast ion populations specified in core profiles IDS
                    for nn in range(0, cls["META"]["NUMI"]):

                        ionstruct = {}
                        ionstruct_prov = {}
                        ntag = "%d" % (nn + 1)

                        # Ion particle source
                        skey = "SNI" + ntag + src
                        key = "SNI" + src
                        if skey in cls["PD"] and cls["PD"][skey][""] is not None:
                            attr = "particles"
                            val = cls["PD"][skey][""].copy()
                            err = cls["PD"][skey]["EB"].copy()
                            lerr = None
                            ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            if "PROV" in cls["PD"] and cls["PD"][skey]["PROV"] is not None:
                                ionstruct_prov[attr] = cls["PD"][skey]["PROV"]
                        elif key in cls["PD"] and cls["PD"][key][""] is not None:
                            attr = "particles"
                            val = cls["PD"][key][""].copy()
                            err = cls["PD"][key]["EB"].copy()
                            lerr = None
                            ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            if "PROV" in cls["PD"] and cls["PD"][key]["PROV"] is not None:
                                ionstruct_prov[attr] = cls["PD"][key]["PROV"]
                        else:
                            attr = "particles"
                            val = np.zeros(refshape)
                            err = np.zeros(refshape)
                            lerr = None
                            ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            ionstruct_prov[attr] = "EX2GK: Internal assumption"

                        skey = "STI" + ntag + src
                        key = "STI" + src
                        if skey in cls["PD"] and cls["PD"][skey][""] is not None:
                            attr = "energy"
                            val = cls["PD"][skey][""].copy()
                            err = cls["PD"][skey]["EB"].copy()
                            lerr = None
                            ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            if "PROV" in cls["PD"] and cls["PD"][skey]["PROV"] is not None:
                                ionstruct_prov[attr] = cls["PD"][skey]["PROV"]
                        if key in cls["PD"] and cls["PD"][key][""] is not None:
                            attr = "energy"
                            val = cls["PD"][key][""].copy()
                            err = cls["PD"][key]["EB"].copy()
                            lerr = None
                            ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            if "PROV" in cls["PD"] and cls["PD"][key]["PROV"] is not None:
                                ionstruct_prov[attr] = cls["PD"][key]["PROV"]
                        else:
                            attr = "energy"
                            val = np.zeros(refshape)
                            err = np.zeros(refshape)
                            lerr = None
                            ionstruct = add_ids_dict_value(ionstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            ionstruct_prov[attr] = "EX2GK: Internal assumption"

                        momstruct = {}
                        momstruct_prov = {}

                        key = "SP" + src
                        if key in cls["PD"] and cls["PD"][key][""] is not None:
                            attr = "toroidal"
                            val = cls["PD"][key][""].copy()
                            err = cls["PD"][key]["EB"].copy()
                            lerr = None
                            momstruct = add_ids_dict_value(momstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            if "PROV" in cls["PD"] and cls["PD"][key]["PROV"] is not None:
                                momstruct_prov[attr] = cls["PD"][key]["PROV"]
                        else:
                            attr = "toroidal"
                            val = np.zeros(refshape)
                            err = np.zeros(refshape)
                            lerr = None
                            momstruct = add_ids_dict_value(momstruct, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                            momstruct_prov[attr] = "EX2GK: Internal assumption"

                        if momstruct:
                            structkey = "momentum"
                            ionstruct[structkey] = momstruct
                            for key, val in momstruct_prov.items():
                                 ionstruct_prov[structkey + "/" + key] = val

#                        attr = "element"
                        akey = "AI" + ntag
                        zkey = "ZI" + ntag
                        if ionstruct and akey in cls["META"] and cls["META"][akey] is not None and zkey in cls["META"] and cls["META"][zkey] is not None:
                            elemstruct = {}
                            elemstruct["a"] = float(cls["META"][akey])
                            elemstruct["z_n"] = float(cls["META"][zkey])
                            elemstruct["atoms_n"] = 1
                            if "element" not in ionstruct:
                                ionstruct["element"] = []
                            ionstruct["element"].append(elemstruct)

                        if ionstruct:
                            structkey = "ion"
                            if structkey not in profiles:
                                profiles[structkey] = []
                            profiles[structkey].append(ionstruct)
                            ii = len(profiles[structkey])
                            istr = '(%d)' % (ii - 1)
                            for key, val in ionstruct_prov.items():
                                nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                                profiles_prov[structkey + istr + "/" + key] = nval

                if profiles:

                    # General radial grid for core sources of this type
                    grid = {}
                    grid_prov = {}

                    if use_raw:
                        pass

                    elif cls.isFitted():
                        rngrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "RHOTORN", cs_prefix, cs_prefix))
                        if rngrid is not None:
                            attr = "rho_tor_norm"
                            grid[attr] = rngrid
                            if cls["META"]["CSO"] == "RHOTORN" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                                grid_prov[attr] = cls["PD"]["X"]["PROV"]
                            else:
                                grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"RHOTORN"
                        rtgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "PSITOR", cs_prefix, cs_prefix))
                        if rtgrid is not None and "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
                            # Convert COCOS 1 -> COCOS 11
                            attr = "rho_tor"
                            grid[attr] = np.sqrt(np.abs(2.0 * rtgrid / cls["ZD"]["BVAC"][""]))
                            bprov = cls["ZD"]["BVAC"]["PROV"] if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"] is not None else "ZD_BVAC"
                            if cls["META"]["CSO"] == "PSITOR" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                                grid_prov[attr] = cls["PD"]["X"]["PROV"] + "; " + bprov
                            else:
                                grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"PSITOR; " + bprov
                        rpgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "RHOPOLN", cs_prefix, cs_prefix))
                        if rpgrid is not None:
                            attr = "rho_pol_norm"
                            grid[attr] = rpgrid
                            if cls["META"]["CSO"] == "RHOPOLN" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                                grid_prov[attr] = cls["PD"]["X"]["PROV"]
                            else:
                                grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"RHOPOLN"
                        fpgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "PSIPOL", cs_prefix, cs_prefix))
                        if fpgrid is not None:
                            # Convert COCOS 1 -> COCOS 11
                            attr = "psi"
                            grid[attr] = 2.0 * np.pi * fpgrid
                            if cls["META"]["CSO"] == "PSIPOL" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                                grid_prov[attr] = cls["PD"]["X"]["PROV"]
                            else:
                                grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"PSIPOL"
                        xagrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "FSAREA", cs_prefix, cs_prefix))
                        if xagrid is not None:
                            attr = "area"
                            grid[attr] = xagrid
                            if cls["META"]["CSO"] == "FSAREA" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                                grid_prov[attr] = cls["PD"]["X"]["PROV"]
                            else:
                                grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"FSAREA"
                        xvgrid = itemgetter(0)(cls["CD"].convert(cls["PD"]["X"][""], cls["META"]["CSO"], "FSVOL", cs_prefix, cs_prefix))
                        if xvgrid is not None:
                            attr = "volume"
                            grid[attr] = xvgrid
                            if cls["META"]["CSO"] == "FSVOL" and "PROV" in cls["PD"]["X"] and cls["PD"]["X"]["PROV"] is not None:
                                grid_prov[attr] = cls["PD"]["X"]["PROV"]
                            else:
                                grid_prov[attr] = "EX2GK: CD_"+cs_prefix+"FSVOL"
                        if "PSIAXS" in cls["ZD"] and cls["ZD"]["PSIAXS"][""] is not None and "PSIBND" in cls["ZD"] and cls["ZD"]["PSIBND"][""] is not None:
                            # Convert COCOS 1 -> COCOS 11
                            attr = "psi_magnetic_axis"
                            grid[attr] = float(2.0 * np.pi * cls["ZD"]["PSIAXS"][""])
                            if "PROV" in cls["ZD"]["PSIAXS"] and cls["ZD"]["PSIAXS"]["PROV"] is not None:
                                grid_prov[attr] = cls["ZD"]["PSIAXS"]["PROV"]
                            # Convert COCOS 1 -> COCOS 11
                            battr = "psi_boundary"
                            grid[battr] = float(2.0 * np.pi * cls["ZD"]["PSIBND"][""])
                            if "PROV" in cls["ZD"]["PSIBND"] and cls["ZD"]["PSIBND"]["PROV"] is not None:
                                grid_prov[battr] = cls["ZD"]["PSIBND"]["PROV"]

                    if grid:
                        structkey = "grid"
                        profiles[structkey] = grid
                        for key, val in grid_prov.items():
                            profiles_prov[structkey + "/" + key] = val

                if profiles:
                    profiles["time"] = ids_time
                    structkey = "profiles_1d"
                    if structkey not in source:
                        source[structkey] = []
                    source[structkey].append(profiles)
                    ii = len(source[structkey])
                    istr = '(%d)' % (ii - 1)
                    for key, val in profiles_prov.items():
                        nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                        source_prov[structkey + istr + "/" + key] = nval

            if source:
                if "identifier" not in source:
                    source["identifier"] = {}
                source_name = imas_source_name_translation[src]
                (source_index, source_description) = imas_source_type_index[source_name]
                source["identifier"]["name"] = source_name
                source["identifier"]["index"] = source_index
                source["identifier"]["description"] = source_description

                structkey = "source"
                if structkey not in idsd:
                    idsd[structkey] = []
                idsd[structkey].append(source)
                ii = len(idsd[structkey])
                istr = '(%d)' % (ii - 1)
                for key, val in source_prov.items():
                    nval = val.replace(structkey + '(*)/', structkey + istr + '/')
                    idsp[structkey + istr + "/" + key] = nval

        if idsd:
            tf = {}
            tf_prov = {}
            if "RVAC" in cls["ZD"] and cls["ZD"]["RVAC"][""] is not None:
                attr = "r0"
                val = float(cls["ZD"]["RVAC"][""])
                err = float(cls["ZD"]["RVAC"]["EB"])
                lerr = None
                tf = add_ids_dict_value(tf, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                if "PROV" in cls["ZD"]["RVAC"] and cls["ZD"]["RVAC"]["PROV"]:
                    tf_prov[attr] = cls["ZD"]["RVAC"]["PROV"]
            if "BVAC" in cls["ZD"] and cls["ZD"]["BVAC"][""] is not None:
                attr = "b0"
                val = np.array([cls["ZD"]["BVAC"][""]])
                err = np.array([cls["ZD"]["BVAC"]["EB"]])
                lerr = None
                tf = add_ids_dict_value(tf, attr, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                if "PROV" in cls["ZD"]["BVAC"] and cls["ZD"]["BVAC"]["PROV"]:
                    tf_prov[attr] = cls["ZD"]["BVAC"]["PROV"]
            if tf:
                structkey = "vacuum_toroidal_field"
                idsd[structkey] = tf
                for key, val in tf_prov.items():
                    idsp[structkey + "/" + key] = val

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            idsd["time"] = np.array([ids_time])

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd

def pass_nbi_ids_time_slice_values(cls, user=None, dd_version=min_imas_version):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        tm = cls["META"]["T2"]
        tw = cls["META"]["T2"] - cls["META"]["T1"]
        ids_time = float(tm) - 0.5 * float(tw)

        nbi_unit_list = cls["META"]["CFGNBI"] if "CFGNBI" in cls["META"] else []
        for utag in nbi_unit_list:

            unit = {}
            unit_prov = {}

            name = ""
            if utag == "4":
                name = "Octant 4"
            if utag == "8":
                name = "Octant 8"
            unit["name"] = name
            unit["identifier"] = utag

            anbi = cls["ZD"]["ANBI"+utag][""] if "ANBI"+utag in cls["ZD"] else None
            znbi = cls["ZD"]["ZNBI"+utag][""] if "ZNBI"+utag in cls["ZD"] else None
            if anbi is not None and znbi is not None:
                species = {}
                species_prov = {}
                (species["label"], species["a"], species["z_n"]) = ptools.define_ion_species(z=znbi, a=anbi, user_mass=True)
                if "PROV" in cls["ZD"]["ANBI"+utag] and cls["ZD"]["ANBI"+utag]["PROV"]:
                    species_prov["a"] = cls["ZD"]["ANBI"+utag]["PROV"]
                if "PROV" in cls["ZD"]["ZNBI"+utag] and cls["ZD"]["ZNBI"+utag]["PROV"]:
                    species_prov["z_n"] = cls["ZD"]["ZNBI"+utag]["PROV"]
                if species:
                    structkey = "species"
                    unit[structkey] = species
                    for key, val in species_prov.items():
                        unit_prov[structkey + "/" + key] = val

            if "PNBI"+utag in cls["ZD"] and cls["ZD"]["PNBI"+utag][""] is not None:
                attr = "power_launched"
                valstruct = {}
                tag = "data"
                val = np.array([cls["ZD"]["PNBI"+utag][""]])
                err = np.array([cls["ZD"]["PNBI"+utag]["EB"]])
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                valstruct["time"] = np.array([ids_time])
                unit[attr] = valstruct
                if "PROV" in cls["ZD"]["PNBI"+utag] and cls["ZD"]["PNBI"+utag]["PROV"]:
                    unit_prov[attr + "/" + tag] = cls["ZD"]["PNBI"+utag]["PROV"]

            if "ENBI"+utag in cls["ZD"] and cls["ZD"]["ENBI"+utag][""] is not None:
                attr = "energy"
                valstruct = {}
                tag = "data"
                val = np.array([cls["ZD"]["ENBI"+utag][""]])
                err = np.array([cls["ZD"]["ENBI"+utag]["EB"]])
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                valstruct["time"] = np.array([ids_time])
                unit[attr] = valstruct
                if "PROV" in cls["ZD"]["ENBI"+utag] and cls["ZD"]["ENBI"+utag]["PROV"]:
                    unit_prov[attr + "/" + tag] = cls["ZD"]["ENBI"+utag]["PROV"]

            if "EFRAC1NBI"+utag in cls["ZD"] and cls["ZD"]["EFRAC1NBI"+utag][""] is not None and "EFRAC2NBI"+utag in cls["ZD"] and cls["ZD"]["EFRAC2NBI"+utag] is not None and "EFRAC3NBI"+utag in cls["ZD"]["EFRAC3NBI"+utag] is not None:
                attr = "beam_power_fraction"
                valstruct = {}
                tag = "data"
                frac1 = np.array([cls["ZD"]["EFRAC1NBI"+utag]])
                frac2 = np.array([cls["ZD"]["EFRAC2NBI"+utag]])
                frac3 = np.array([cls["ZD"]["EFRAC3NBI"+utag]])
                val = np.vstack((frac1, frac2, frac3))
                err = None
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                valstruct["time"] = np.array([ids_time])
                unit[attr] = valstruct
                if "PROV" in cls["ZD"]["EFRAC1NBI"+utag] and cls["ZD"]["EFRAC1NBI"+utag]["PROV"]:
                    unit_prov[attr + "/" + tag] = cls["ZD"]["EFRAC1NBI"+utag]["PROV"]

            if unit:
                structkey = "unit"
                if structkey not in idsd:
                    idsd[structkey] = []
                idsd[structkey].append(unit)
                ii = len(idsd[structkey])
                istr = '(%d)' % (ii - 1)
                for key, val in unit_prov.items():
                    idsp[structkey + istr + "/" + key] = val

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            idsd["time"] = np.array([ids_time])

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd

def pass_ic_antennas_ids_time_slice_values(cls, user=None, dd_version=min_imas_version):

    # Data containers
    idsd = {}
    idsp = {}

    if isinstance(cls, classes.EX2GKTimeWindow):

        # Grab generic data
        tm = cls["META"]["T2"]
        tw = cls["META"]["T2"] - cls["META"]["T1"]
        ids_time = float(tm) - 0.5 * float(tw)

        icrh_antenna_list = cls["META"]["CFGICRH"] if "CFGICRH" in cls["META"] else []
        for atag in icrh_antenna_list:

            antenna = {}
            antenna_prov = {}

            name = atag
            if atag == "E":
                name = "ILA"
            antenna["name"] = name
            antenna["identifier"] = atag

            if "PICRH"+atag in cls["ZD"] and cls["ZD"]["PICRH"+atag][""] is not None:
                attr = "power_launched"
                valstruct = {}
                tag = "data"
                val = np.array([cls["ZD"]["PICRH"+atag][""]])
                err = np.array([cls["ZD"]["PICRH"+atag]["EB"]])
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                valstruct["time"] = np.array([ids_time])
                antenna[attr] = valstruct
                if "PROV" in cls["ZD"]["PICRH"+atag] and cls["ZD"]["PICRH"+atag]["PROV"]:
                    antenna_prov[attr + "/" + tag] = cls["ZD"]["PICRH"+atag]["PROV"]

            if "FREQICRH"+atag in cls["ZD"] and cls["ZD"]["FREQICRH"+atag][""] is not None:
                attr = "frequency"
                valstruct = {}
                tag = "data"
                val = np.array([cls["ZD"]["FREQICRH"+atag][""]])
                err = None
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                valstruct["time"] = np.array([ids_time])
                antenna[attr] = valstruct
                if "PROV" in cls["ZD"]["FREQICRH"+atag] and cls["ZD"]["FREQICRH"+atag]["PROV"]:
                    antenna_prov[attr + "/" + tag] = cls["ZD"]["FREQICRH"+atag]["PROV"]

            module = {}
            module_prov = {}

            # Code currently designed only for one module - needs to be more modular if multiple modules are required
            if "PHASEICRH"+atag in cls["ZD"] and cls["ZD"]["PHASEICRH"+atag][""] is not None:
                attr = "phase_forward"
                valstruct = {}
                tag = "data"
                val = np.array([cls["ZD"]["PHASEICRH"+atag][""]])
                err = None
                lerr = None
                valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
                valstruct["time"] = np.array([ids_time])
                module[attr] = valstruct
                if "PROV" in cls["ZD"]["PHASEICRH"+atag] and cls["ZD"]["PHASEICRH"+atag]["PROV"]:
                    module_prov[attr + "/" + tag] = cls["ZD"]["PHASEICRH"+atag]["PROV"]

            if module:
                structkey = "module"
                if structkey not in antenna:
                    antenna[structkey] = []
                antenna[structkey].append(module)
                ii = len(antenna[structkey])
                istr = '(%d)' % (ii - 1)
                for key, val in module_prov.items():
                    antenna_prov[structkey + istr + "/" + key] = val

            if antenna:
                structkey = "antenna"
                if structkey not in idsd:
                    idsd[structkey] = []
                idsd[structkey].append(antenna)
                ii = len(idsd[structkey])
                istr = '(%d)' % (ii - 1)
                for key, val in antenna_prov.items():
                    idsp[structkey + istr + "/" + key] = val

        if "POWIICRH" in cls["ZD"] and cls["ZD"]["POWIICRH"][""] is not None:
            attr = "power_launched"
            valstruct = {}
            tag = "data"
            val = np.array([cls["ZD"]["POWIICRH"][""]])
            err = np.array([cls["ZD"]["POWIICRH"]["EB"]])
            lerr = None
            valstruct = add_ids_dict_value(valstruct, tag, values=val, upper_errors=err, lower_errors=lerr, dd_version=dd_version)
            valstruct["time"] = np.array([ids_time])
            idsd[attr] = valstruct
            if "PROV" in cls["ZD"]["POWIICRH"] and cls["ZD"]["POWIICRH"]["PROV"]:
                idsp[attr + "/" + tag] = cls["ZD"]["POWIICRH"]["PROV"]

        # Fill provenance data
        if idsd and idsp:
            provstruct = {}
            attr = "node"
            if attr not in provstruct:
                provstruct[attr] = []
            for key, val in idsp.items():
                provenance_provided = False
                for item in provstruct[attr]:
                    if item["path"] == key:
                        provenance_provided = True
                if not provenance_provided:
                    provdict = {"path": key, "sources": []}
                    field_list = val.split(';') if val is not None else []
                    for field in field_list:
                        provdict["sources"].append(field.strip())
                        # Apparently having too large of an array causes segfaults
                        if len(provdict["sources"]) >= ids_provenance_max_length:
                            provdict["sources"][-1] = "List truncated by UAL constraints"
                            break
                    provstruct[attr].append(provdict)
            structkey = "ids_properties"
            idsd[structkey] = {"provenance": provstruct}

        # Fill IDS metadata
        if idsd:
            idsd["time"] = np.array([ids_time])

            if "ids_properties" not in idsd:
                idsd["ids_properties"] = {}
            idsd["ids_properties"]["homogeneous_time"] = 1
            idsd["ids_properties"]["creation_date"] = cls["UPDATED"]
            if user is not None:
                idsd["ids_properties"]["provider"] = user

            if "code" not in idsd:
                idsd["code"] = {}
            idsd["code"]["commit"] = "unknown"
            idsd["code"]["name"] = "EX2GK"
            idsd["code"]["output_flag"] = np.array([])
            idsd["code"]["repository"] = r'https://gitlab.com/aaronkho/EX2GK.git'
            idsd["code"]["version"] = __version__

    return idsd


def pass_all_ids_values(cls, user, dd_version=min_imas_version, include_raw=False, use_preset_impurities=False):
    ids_dict = {}
    if isinstance(cls, classes.EX2GKTimeWindow):
        ids_dict = {
            "pulse_schedule": pass_pulse_schedule_ids_time_slice_values(cls, user, dd_version=dd_version),
            "summary": pass_summary_ids_time_slice_values(cls, user, dd_version=dd_version, use_preset_impurities=use_preset_impurities),
            "equilibrium": pass_equilibrium_ids_time_slice_values(cls, user, dd_version=dd_version),
            "core_profiles": pass_core_profiles_ids_time_slice_values(cls, user, dd_version=dd_version, include_raw=include_raw, use_preset_impurities=use_preset_impurities),
            "core_sources": pass_core_sources_ids_time_slice_values(cls, user, dd_version=dd_version, include_raw=include_raw),
            "nbi": pass_nbi_ids_time_slice_values(cls, user, dd_version=dd_version),
            "ic_antennas": pass_ic_antennas_ids_time_slice_values(cls, user, dd_version=dd_version)
        }
    return ids_dict


def pass_standardized_ids_values(cls, user, dd_version=None, include_raw=False, use_preset_impurities=False, fill_style="interpolate", empty_int=-999999999, empty_float=-9e+40):

    multi_tw_ids_dict = None
    itw = 0

    if isinstance(cls, classes.EX2GKTimeWindow):

        if multi_tw_ids_dict is None:
            multi_tw_ids_dict = {}
        ids_dict = pass_all_ids_values(cls, user, dd_version=dd_version, include_raw=include_raw, use_preset_impurities=use_preset_impurities)
        for ids_key in ids_dict:
            if ids_key not in multi_tw_ids_dict:
                multi_tw_ids_dict[ids_key] = [{}] * itw if itw > 0 else []
            multi_tw_ids_dict[ids_key].append(ids_dict[ids_key])
        itw = 1

    if isinstance(cls, classes.EX2GKShot):

        cls.sort()

        # Generate data with IDS field names
        for tw in cls.values():
            if multi_tw_ids_dict is None:
                multi_tw_ids_dict = {}
            ids_dict = pass_all_ids_values(tw, user, dd_version=dd_version, include_raw=include_raw, use_preset_impurities=use_preset_impurities)
            for ids_key in ids_dict:
                if ids_key not in multi_tw_ids_dict:
                    multi_tw_ids_dict[ids_key] = [{}] * itw if itw > 0 else []
                multi_tw_ids_dict[ids_key].append(ids_dict[ids_key])
            itw += 1

    if itw > 1:

        # Check field names which exist
        delimiter = "."
        ids_fields = {}
        if multi_tw_ids_dict is not None:
            for ids_key, ids_list in multi_tw_ids_dict.items():
                for ii in range(len(ids_list)):
                    keyblock = ptools.flatten_dict(ids_list[ii], delimiter=delimiter)
                    if ids_key not in ids_fields:
                        ids_fields[ids_key] = list(keyblock.keys())
                    else:
                        for key in keyblock:
                            if key not in ids_fields[ids_key]:
                                ids_fields[ids_key].append(key)

        # Ensure uniformity of data availability
        for ids_key in ids_fields:
            for key in ids_fields[ids_key]:
                for ii in range(itw):
                    ids = copy.deepcopy(multi_tw_ids_dict[ids_key][ii])
                    repval = ptools.find_flattened_key(ids, key, delimiter=delimiter)
                    if repval is None:
                        if key.endswith("_error_upper") and key[:-12] in ids_fields[ids_key]:
                            repval = ptools.find_flattened_key(ids, key[:-12])
                            only_interp = True
                        if key.endswith("_error_lower") and key[:-5] + "upper" in ids_fields[ids_key] and key[:-12] in ids_fields[ids_key]:
                            err = ptools.find_flattened_key(ids, key[:-5] + "upper", delimiter=delimiter)
                            val = ptools.find_flattened_key(ids, key[:-12], delimiter=delimiter)
                            repval = val + val - err if val is not None and err is not None else None
                            only_interp = True
                        reptime = ptools.find_flattened_key(ids, "time", delimiter=delimiter)
                        val_below = None
                        time_below = None
                        jj = ii - 1
                        while val_below is None and jj >= 0:
                            temp = ptools.find_flattened_key(multi_tw_ids_dict[ids_key][jj], key, delimiter=delimiter)
                            fgood = (temp is not None)
                            fgood &= not (isinstance(temp, (int, np.int8, np.int16, np.int32)) and temp == empty_int)
                            fgood &= not (isinstance(temp, (float, np.float16, np.float32, np.float64)) and temp == empty_float)
                            fgood &= not (isinstance(temp, (list, tuple, np.ndarray)) and len(temp) == 0)
                            if fgood:
                                val_below = copy.deepcopy(temp)
                                time_below = ptools.find_flattened_key(multi_tw_ids_dict[ids_key][jj], "time", delimiter=delimiter)
                            jj -= 1
                        val_above = None
                        time_above = None
                        jj = ii + 1
                        while val_above is None and jj < itw:
                            temp = ptools.find_flattened_key(multi_tw_ids_dict[ids_key][jj], key, delimiter=delimiter)
                            fgood = (temp is not None)
                            fgood &= not (isinstance(temp, (int, np.int8, np.int16, np.int32)) and temp == empty_int)
                            fgood &= not (isinstance(temp, (float, np.float16, np.float32, np.float64)) and temp == empty_float)
                            fgood &= not (isinstance(temp, (list, tuple, np.ndarray)) and len(temp) == 0)
                            if fgood:
                                val_above = copy.deepcopy(temp)
                                time_above = ptools.find_flattened_key(multi_tw_ids_dict[ids_key][jj], "time", delimiter=delimiter)
                            jj += 1
                        if val_below is not None:
                            if val_above is not None and time_above is not None and time_below is not None:
                                if fill_style == "interpolate":
                                    repval = reptime * (val_above - val_below) / (time_above - time_below)
                                else:   # "nearest"
                                    repval = val_below if np.abs(time_above - reptime) >= np.abs(time_below - reptime) else val_above
                            else:
                                repval = val_below
                        elif val_above is not None:
                            repval = val_above
                    if repval is not None:
                        ids = ptools.insert_flattened_key(ids, key, repval, delimiter=delimiter, parents=True)
                    multi_tw_ids_dict[ids_key][ii] = copy.deepcopy(ids)

    return multi_tw_ids_dict


def extract_metadata_from_class(cls):

    idnum = 0
    machine = 'test'
    if isinstance(cls, classes.EX2GKTimeWindow):
        idnum = cls["META"]["SHOT"]
        machine = cls["META"]["DEVICE"].lower()
    elif isinstance(cls, classes.EX2GKShot):
        idnum = cls.shot
        machine = cls.device.lower()

    return machine, idnum


def export_to_imas(cls, user, dbname=None, run_number=1, path=".", backend="mdsplus", use_preset_impurities=False):

    if isinstance(cls, (classes.EX2GKTimeWindow, classes.EX2GKShot)):

        imas = None
        try:
            import imas
        except ImportError:
            warnings.warn("IMAS Python module not found or not configured properly, skipping export command!", UserWarning)

        if imas is not None:

            backend_options = {
                "mdsplus": imas.imasdef.MDSPLUS_BACKEND,
                "ascii": imas.imasdef.ASCII_BACKEND,
                "hdf5": imas.imasdef.HDF5_BACKEND
            }
            bopt = backend_options[backend] if backend in backend_options else imasdef.MDSPLUS_BACKEND
            vsplit = imas.names[0].split("_")
            imas_version = version.parse(".".join(vsplit[1:4]))
            ual_version = version.parse(".".join(vsplit[5:]))
            db = "ids" if dbname is None else dbname
            if imas_version < min_imas_version:
                raise ImportError("IMAS version must be >= %s! Aborting!" % (min_imas_version_str))
            if ual_version < min_imasal_version:
                raise ImportError("IMAS AL version must be >= %s! Aborting!" % (min_imasal_version_str))
            if not isinstance(user, str):
                raise TypeError("%s user ID argument must be a string, aborting!" % (type(self).exportToIMAS.__name__))
            if not isinstance(db, str):
                raise TypeError("%s database name argument must be a string, aborting!" % (type(self).exportToIMAS.__name__))
            if not isinstance(run_number, number_types):
                raise TypeError("%s run number argument must be a positive integer, aborting!" % (type(self).exportToIMAS.__name__))

            print("Using IMAS version: %s" % (imas_version))
            print("Using UAL version: %s" % (ual_version))

            opath = Path(path) if isinstance(path, str) else Path(".")
            oname = opath.resolve()
            fullname = str(oname.stem)
            if bopt == imas.imasdef.ASCII_BACKEND:
                opath = opath / "3" / "0"
                if opath.exists() and not opath.is_dir():
                    raise IOError("%s path argument, %s, already exists and is not a directory, aborting!" % (type(self).exportToIMAS.__name__, str(opath)))
                elif not opath.exists():
                    opath.mkdir(parents=True)
                oname = opath.resolve()
                fullname = str(oname) + "/" + db
            machine, idnum = extract_metadata_from_class(cls)
            runnum = run_number

            db_entry = imas.DBEntry(bopt, fullname, idnum, runnum, user_name=user)
            status, ctx = db_entry.open()

            # Checks if run number already exists in the chosen database region
            while status == 0:
                db_entry.close()
                runnum += 1
                db_entry = imas.DBEntry(bopt, fullname, idnum, runnum, user_name=user)
                status, ctx = db_entry.open()
            if runnum != run_number:
                print("Requested occurrance number already exists in database! Updated to next empty number, %d." % (runnum))
            #TODO: Find a robust way to switch 3/0 to 3/1 when run number >= 1000, etc.

            status, ctx = db_entry.create()     # This is necessary because open command fails if run number does not exist!
            if status == 0:
                ids_dict = pass_standardized_ids_values(cls, user, dd_version=imas_version, include_raw=True, use_preset_impurities=use_preset_impurities, fill_style="interpolate", empty_int=imas.imasdef.EMPTY_INT, empty_float=imas.imasdef.EMPTY_FLOAT)
                #for ids_key in ids_dict:
                    #print(ids_key, "  Length:", len(ids_dict[ids_key]))
                    #print("----------------")
                    #for ii in range(len(ids_dict[ids_key])):
                    #    ptools.recursive_dict_print(ids_dict[ids_key][ii])
                    #    print("----------------")
                if "pulse_schedule" in ids_dict:
                    for ii in range(len(ids_dict["pulse_schedule"])):
                        ps_ids = imas.pulse_schedule()
                        ps_ids = insert_into_generic_ids_structure(ps_ids, ids_dict["pulse_schedule"][ii])
                        db_entry.put_slice(ps_ids)
                    print("Pulse schedule IDS generated with %d time slices!" % (ii+1))
                if "summary" in ids_dict:
                    for ii in range(len(ids_dict["summary"])):
                        sum_ids = imas.summary()
                        sum_ids = insert_into_generic_ids_structure(sum_ids, ids_dict["summary"][ii])
                        db_entry.put_slice(sum_ids)
                    print("Summary IDS generated with %d time slices!" % (ii+1))
                if "equilibrium" in ids_dict:
                    for ii in range(len(ids_dict["equilibrium"])):
                        eq_ids = imas.equilibrium()
                        eq_ids = insert_into_generic_ids_structure(eq_ids, ids_dict["equilibrium"][ii])
                        db_entry.put_slice(eq_ids)
                    print("Equilibrium IDS generated with %d time slices!" % (ii+1))
                if "core_profiles" in ids_dict:
                    for ii in range(len(ids_dict["core_profiles"])):
                        cp_ids = imas.core_profiles()
                        cp_ids = insert_into_generic_ids_structure(cp_ids, ids_dict["core_profiles"][ii])
                        db_entry.put_slice(cp_ids)
                    print("Core profiles IDS generated with %d time slices!" % (ii+1))
                if "core_sources" in ids_dict:
                    for ii in range(len(ids_dict["core_sources"])):
                        cs_ids = imas.core_sources()
                        cs_ids = insert_into_generic_ids_structure(cs_ids, ids_dict["core_sources"][ii])
                        db_entry.put_slice(cs_ids)
                    print("Core sources IDS generated with %d time slices!" % (ii+1))
                if "nbi" in ids_dict:
                    for ii in range(len(ids_dict["nbi"])):
                        nb_ids = imas.nbi()
                        nb_ids = insert_into_generic_ids_structure(nb_ids, ids_dict["nbi"][ii])
                        db_entry.put_slice(nb_ids)
                        print("NBI IDS generated with %d time slices!" % (ii+1))
                if "ic_antennas" in ids_dict:
                    for ii in range(len(ids_dict["ic_antennas"])):
                        ic_ids = imas.ic_antennas()
                        ic_ids = insert_into_generic_ids_structure(ic_ids, ids_dict["ic_antennas"][ii])
                        db_entry.put_slice(ic_ids)
                        print("IC antennas IDS generated with %d time slices!" % (ii+1))
            else:
                backend_strings = {
                    imas.imasdef.ASCII_BACKEND: "ASCII backend",
                    imas.imasdef.MDSPLUS_BACKEND: "MDS+ backend",
                    imas.imasdef.HDF5_BACKEND: "HDF5 backend"
                }
                bstr = backend_strings[bopt] if bopt in backend_strings else "unknown backend"
                print("Error creating IDS files with %s! Aborting!" % (bstr))
            db_entry.close()
            if status == 0:
                print("Data successfully stored in %s, with runid %d." % (oname.resolve(), runnum))

    else:
        print("Improper class argument given to IMAS export function, aborting!")
