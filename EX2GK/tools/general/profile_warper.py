# Script with functions to modify fitted profiles using GPR fit uncertainties
# Developer: Aaron Ho - 20/04/2018

# Required imports
import os
import sys
import re
import pwd
import copy
import argparse
import shutil
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import collections

array_types = (list, tuple, np.ndarray)

class _WarpFunction():

    def __init__(self, name="None", func=None, pars=None):
        self._wname = name
        self._wfunc = func
        self._params = np.array(copy.deepcopy(pars)).flatten() if isinstance(pars, array_types) else None

    def __call__(self, x):
        fout = None
        if callable(self._wfunc):
            fout = self._wfunc(x)
        else:
            raise NotImplementedError('Warp function not yet defined.')
        return fout

    def get_name(self):
        return self._wname

    def get_parameters(self):
        return self._params

    def set_parameters(self, pars):
        upars = np.array(copy.deepcopy(pars)).flatten() if isinstance(pars, array_types) else None
        if upars is None:
            raise TypeError('Argument must be an array-like object.')
        if self._params is not None:
            if upars.size >= self._params.size:
                self._params = upars[:self._params.size]
            else:
                raise ValueError('Parameter list must contain at least %d elements.' % (self._params.size))
        else:
            raise AttributeError('WarpFunction object has no parameters.')


class _WarpOperator(_WarpFunction):

    def __init__(self, name="None", func=None, wlist=None):
        super(_WarpOperator, self).__init__(name, func)
        self._warp_list = wlist if isinstance(wlist, list) else []

    def get_parameters(self):
        val = np.array([])
        for ww in self._warp_list:
            val = np.append(val, ww.get_parameters())
        return val

    def set_parameters(self, pars):
        upars = np.array(copy.deepcopy(pars)).flatten() if isinstance(pars, array_types) else None
        if upars is None:
            raise TypeError('Argument must be an array-like object.')
        npars = self.get_parameters().size
        if npars > 0:
            if upars.size >= npars:
                ndone = 0
                for ww in self._warp_list:
                    nhere = ndone + ww.get_parameters().size
                    if nhere != ndone:
                        if nhere == npars:
                            ww.set_parameters(pars[ndone:])
                        else:
                            ww.set_parameters(pars[ndone:nhere])
                        ndone = nhere
            else:
                raise ValueError('Parameter list must contain at least %d elements.' % (npars))
        else:
            raise AttributeError('WarpFunction object has no parameters.')


class WarpSum(_WarpOperator):

    def _warp(self, x):
        fout = np.NaN if self._warp_list is None else np.zeros(x.shape)
        for ww in self._warp_list:
            fout = fout + ww(x)
        return fout

    def __init__(self, *args, wlist=None):
        uwlist = []
        name = "None"
        if len(args) >= 2 and isinstance(args[0], _WarpFunction) and isinstance(args[1], _WarpFunction):
            name = ""
            for ww in args:
                if isinstance(ww, _WarpFunction):
                    uwlist.append(ww)
                    name = name + "-" + ww.get_name() if name else ww.get_name()
        elif isinstance(wlist, list) and len(wlist) >= 2 and isinstance(wlist[0], _WarpFunction) and isinstance(wlist[1], _WarpFunction):
            name = ""
            for ww in wlist:
                if isinstance(ww, _WarpFunction):
                    uwlist.append(ww)
                    name = name + "-" + ww.get_name() if name else ww.get_name()
        else:
            raise TypeError('Arguments to WarpSum must be WarpFunction objects.')
        super(WarpSum, self).__init__("Sum("+name+")", self._warp, uwlist)

    def __copy__(self):
        wcopy_list = []
        for ww in self._warp_list:
            wcopy_list.append(copy.copy(ww))
        wcopy = WarpSum(wlist=wcopy_list)
        return wcopy


class WarpProduct(_WarpOperator):

    def _warp(self, x):
        fout = np.NaN if self._warp_list is None else np.ones(x.shape)
        for ww in self._warp_list:
            fout = fout * ww(x)
        return fout

    def __init__(self, *args, wlist=None):
        uwlist = []
        name = "None"
        if len(args) >= 2 and isinstance(args[0], _WarpFunction) and isinstance(args[1], _WarpFunction):
            name = ""
            for ww in args:
                if isinstance(ww, _WarpFunction):
                    uwlist.append(ww)
                    name = name + "-" + ww.get_name() if name else ww.get_name()
        elif isinstance(wlist, list) and len(wlist) >= 2 and isinstance(wlist[0], _WarpFunction) and isinstance(wlist[1], _WarpFunction):
            name = ""
            for ww in wlist:
                if isinstance(ww, _WarpFunction):
                    uwlist.append(ww)
                    name = name + "-" + ww.get_name() if name else ww.get_name()
        else:
            raise TypeError('Arguments to WarpProduct must be WarpFunction objects.')
        super(WarpProduct, self).__init__("Prod("+name+")", self._warp, uwlist)

    def __copy__(self):
        wcopy_list = []
        for ww in self._warp_list:
            wcopy_list.append(copy.copy(ww))
        wcopy = WarpProduct(wlist=wcopy_list)
        return wcopy


class WarpConstant(_WarpFunction):

    def _warp(self, x):
        cc = self._params[0]
        fout = cc * np.ones(x.shape)
        return fout

    def __init__(self, const=1.0):
        pars = np.zeros((1,))
        if isinstance(const, (int, float)):
            pars[0] = float(const)
        else:
            raise TypeError('Constant parameter must be a real number.')
        super(WarpConstant, self).__init__("const", self._warp, pars)

    def __copy__(self):
        const = float(self._params[0])
        wcopy = WarpConstant(const)
        return wcopy


class WarpLinear(_WarpFunction):

    def _warp(self, x):
        mm = self._params[0]
        x0 = self._params[1]
        fout = mm * (x - x0)
        return fout

    def __init__(self, slope=1.0, zero=0.0):
        pars = np.zeros((2,))
        if isinstance(slope, (int, float)):
            pars[0] = float(slope)
        else:
            raise TypeError('Slope parameter must be a real number.')
        if isinstance(zero, (int, float)):
            pars[1] = float(zero)
        else:
            raise TypeError('Zero parameter must be a real number.')
        super(WarpLinear, self).__init__("lin", self._warp, pars)

    def __copy__(self):
        slope = float(self._params[0])
        zero = float(self._params[1])
        wcopy = WarpLinear(slope, zero)
        return wcopy


class WarpTanh(_WarpFunction):

    def _warp(self, x):
        y0 = self._params[0]
        x0 = self._params[1]
        hp = self._params[2]
        wp = self._params[3]
        fout = 0.5 * hp * np.tanh((x - x0) / wp) + y0
        return fout

    def __init__(self, vpos=0.0, hpos=0.0, height=1.0, width=1.0):
        pars = np.zeros((4,))
        if isinstance(vpos, (int, float)):
            pars[0] = float(vpos)
        else:
            raise TypeError('Vertical position parameter must be a real number.')
        if isinstance(hpos, (int, float)):
            pars[1] = float(hpos)
        else:
            raise TypeError('Horizontal position parameter must be a real number.')
        if isinstance(height, (int, float)) and float(height) != 0.0:
            pars[2] = float(height)
        else:
            raise TypeError('Height parameter must be a non-zero real number.')
        if isinstance(width, (int, float)) and float(width) != 0.0:
            pars[3] = float(width)
        else:
            raise TypeError('Width parameter must be a non-zero real number.')
        super(WarpTanh, self).__init__("tanh", self._warp, pars)

    def __copy__(self):
        vpos = float(self._params[0])
        hpos = float(self._params[1])
        height = float(self._params[2])
        width = float(self._params[3])
        wcopy = WarpTanh(vpos, hpos, height, width)
        return wcopy


def WarpFunctionConstructor(name):
    wfunc = None
    if isinstance(name, str):
        m = re.search(r'^(.*?)\((.*)\).*$', name)
        if m:
            links = m.group(2).split('-')
            names = []
            bflag = False
            cname = ''
            for jj in np.arange(0, len(links)):
                cname = links[jj] if not bflag else cname + '-' + links[jj]
                if re.search('\(', links[jj]):
                    bflag = True
                if re.search('\)', links[jj]):
                    bflag = False
                if not bflag:
                    names.append(cname)
            wwlist = []
            for ii in np.arange(0, len(names)):
                wwlist.append(WarpFunctionConstructor(names[ii]))
            if re.search('^Sum$', m.group(1)):
                wfunc = WarpSum(wlist=wwlist)
            elif re.search('^Prod$', m.group(1)):
                wfunc = WarpProduct(wlist=wwlist)
        else:
            if re.match('^const$', name):
                wfunc = WarpConstant()
            elif re.match('^lin$', name):
                wfunc = WarpLinear()
            elif re.match('^tanh$', name):
                wfunc = WarpTanh()
    return wfunc


def WarpFunctionReconstructor(name, pars=None):
    wfunc = WarpFunctionConstructor(name)
    pvec = np.array(copy.deepcopy(pars)).flatten() if isinstance(pars, array_types) else None
    if isinstance(wfunc, _WarpFunction) and pvec is not None:
        npar = wfunc.get_parameters().size
        if pvec.size >= npar:
            upars = pvec[:npar] if pvec.size > npar else pvec.copy()
            wfunc.set_parameters(upars)
    return wfunc


def modify_profile(xinput=None, yinput=None, sinput=None, warpname=None, parlist=None, minval=None, interpvec=None, outfile=None, modtype=None, plotflag=False):

    moptions = {'sigma':0, 'shift':1, 'scale':2}
    mreverse = ['sigma', 'shift', 'scale']

    ix = None
    iy = None
    ie = None
    qq = None
    mname = 'const'
    upars = None
    lb = None
    ivec = None
    oname = None
    mode = 2
    exname = None
    if isinstance(xinput, array_types):
        ix = np.array(xinput).flatten()
    if isinstance(yinput, array_types):
        iy = np.array(yinput).flatten()
    if isinstance(sinput, array_types):
        ie = np.array(sinput).flatten()
    if isinstance(warpname, str):
        mname = warpname
    if isinstance(parlist, (list, tuple)) and len(parlist) > 0:
        upars = []
        for ii in np.arange(0, len(parlist)):
            upars.append(parlist[ii])
    elif isinstance(parlist, np.ndarray) and parlist.size > 0:
        upars = []
        for ii in np.arange(0, parlist.size):
            upars.append(parlist[ii])
    if isinstance(minval, (int, float)):
        lb = float(minval)
    if isinstance(interpvec, (list, tuple)) and len(interpvec) > 0:
        ivec = np.array(interpvec)
    elif isinstance(interpvec, np.ndarray) and interpvec.size > 0:
        ivec = interpvec.flatten()
    if isinstance(outfile, str):
        oname = outfile
    if isinstance(modtype, str) and modtype in moptions:
        mode = moptions[modtype]
    elif isinstance(modtype, (float, int)) and int(modtype) >= 0 and int(modtype) <= 2:
        mode = int(modtype)

    if ie is None and mode == 0:
        raise IOError("Error data must be provided for sigma mode modifications.")

    output = None
    if ix is not None and iy is not None:
        wfunc = WarpFunctionReconstructor(mname, pars=upars)
        if wfunc is None:
            raise TypeError("Unrecognized modification specification, %s." % (mname))

        px = copy.deepcopy(ix)
        py = copy.deepcopy(iy)
        if ie is None:
            ie = np.zeros(ix.shape)
        errs = copy.deepcopy(ie)

        if ivec is not None:
            pyfunc = interp1d(px, py, bounds_error=False, fill_value="extrapolate")
            py = pyfunc(ivec.flatten())
            if mode == 0:
                errfunc = interp1d(px, errs, bounds_error=False, fill_value="extrapolate")
                errs = np.abs(errfunc(ivec.flatten()))
            else:
                errs = np.zeros(ivec.shape)
            px = ivec.flatten()

        pmod = wfunc(px)
        if mode == 1:
            py = py + pmod
        elif mode == 2:
            py = py * pmod
        else:
            py = py + pmod * errs
        if lb is not None:
            py[py < lb] = lb

        if oname is not None:
            if not oname.endswith('.txt'):
                oname = oname + '.txt'
            with open(oname, 'w') as ff:
                for ii in np.arange(0, px.size):
                    ff.write("%15.4f%15.6e\n" % (px[ii], py[ii]))
#        ndata = np.vstack((px, py, errs)).T

        if plotflag:
            pxmod = copy.deepcopy(ix)
            pmodo = wfunc(pxmod)
            pymod = np.zeros(pxmod.shape)
            if mode == 1:
                pymod = iy + pmodo
            elif mode == 2:
                pymod = iy * pmodo
            else:
                pymod = iy + pmodo * ie
            fig = plt.figure(figsize=(8, 4))
            ax1 = fig.add_subplot(121)
            ax1.plot(ix, iy, color='g', ls='--', label='Original')
            if mode == 0:
                yl = iy - ie
                yu = iy + ie
                ax1.fill_between(ix, yl, yu, facecolor='g', edgecolor='None', alpha=0.2)
            ax1.plot(pxmod, pymod, color='r', label='Modified')
            ax1.legend(loc='best')
            ax2 = fig.add_subplot(122)
            ax2.plot(pxmod, pmodo, color='b', label='Mod Function'+' ('+mreverse[mode].lower()+')')
            ax2.legend(loc='best')
            fig.tight_layout()
            plotfile = 'modded_profile.png'
            if oname is not None:
                plotfile = oname + '.png'
                mm = re.search('^(.+)\.txt$', oname, flags=re.IGNORECASE)
                if mm:
                    plotfile = mm.group(1) + '.png'
            fig.savefig(plotfile, dpi=300)
            plt.close(fig)

        output = copy.deepcopy(py)

    else:
        raise TypeError("Input data not valid for profile modification script.")

    return output
