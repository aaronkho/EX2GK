# Script with functions to perform sanity and consistency checks to identify high quality data
# Developer: Aaron Ho - 07/03/2018

# Required imports
import os
import sys
import copy
import numpy as np
import re
import pickle
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz
from scipy.optimize import minimize
try:
    from scipy.ndimage import map_coordinates
except ImportError:
    from scipy.ndimage.interpolation import map_coordinates

# Custom imports, must be in PYTHONPATH or in working directory
from EX2GK.tools.general import proctools as ptools

np_itypes = (np.int8, np.int16, np.int32, np.int64)
np_utypes = (np.uint8, np.uint16, np.uint32, np.uint64)
np_ftypes = (np.float16, np.float32, np.float64)

number_types = (float, int, np_itypes, np_utypes, np_ftypes)
array_types = (list, tuple, np.ndarray)


def calc_equipartition_power(ts1raw, ns1raw, ts2raw=None, ns2raw=None, as1=None, as2=None, zs1=None, zs2=None, debye=None, ts1raweb=None, ns1raweb=None, ts2raweb=None, ns2raweb=None, debyeeb=None, momentum=False, verbose=0):
    """
    Calculates the equipartition power transfer from species 1 to 2 using the
    provided temperature and density profiles. Assumes the species 1 temperature
    profile is similar, but not equal, to the species 2 temperature profile if it
    is not provided. If not provided, a difference of 10% is assumed and the
    resulting power can be used to estimate the sustainable temperature
    difference between the two species.

    See Wesson and NRL Plasma Formulary for more details on the equation.

    :arg ts1raw: array. Vector of temperatures for particle species 1 corresponding to radial points.

    :arg ns1raw: array. Vector of densities for particle species 2 corresponding to radial points.

    :kwarg ts2raw: array. Vector of temperatures for particle species 2 corresponding to radial points.

    :kwarg ns2raw: array. Vector of densities for particle species 2 corresponding to radial points.

    :kwarg as1: float. Mass of particle species 1 in atomic mass units, u.

    :kwarg as2: float. Mass of particle species 2 in atomic mass units, u.

    :kwarg zs1: float. Charge of particle species 1 in elementary charge units, e.

    :kwarg zs2: float. Charge of particle species 2 in elementary charge units, e.

    :kwarg debye: array. Vector of plasma Debye lengths corresponding to radial points.

    :kwarg momentum: bool. Flag to calculate equipartition flux using momentum collision rate instead of energy collision rate.

    :kwarg verbose: int. Set level of verbosity on function, higher value shows more information.

    :returns: array. Vector of equipartition power transfer corresponding to radial points.
    """
    n1 = None
    t1 = None
    n2 = None
    t2 = None
    n1eb = None
    t1eb = None
    n2eb = None
    t2eb = None
    a1 = 1.0 / 1822.888    # Default assumption - species 1 is electrons
    a2 = 2.0141            # Default assumption - species 2 is deuterons
    z1 = -1.0
    z2 = 1.0
    bmax = None
    bmaxeb = None
    eflag = True
    if isinstance(ts1raw, number_types):
        t1 = np.array([ts1raw])
    elif isinstance(ts1raw, array_types):
        t1 = np.array(ts1raw).flatten()
    if isinstance(ts1raweb, number_types):
        t1eb = np.array([ts1raweb])
    elif isinstance(ts1raweb, array_types):
        t1eb = np.array(ts1raweb).flatten()
    elif t1 is not None:
        t1eb = np.zeros(t1.shape)
    if isinstance(ns1raw, number_types):
        n1 = np.array([ns1raw])
    elif isinstance(ns1raw, array_types):
        n1 = np.array(ns1raw).flatten()
    if isinstance(ns1raweb, number_types):
        n1eb = np.array([ns1raweb])
    elif isinstance(ns1raweb, array_types):
        n1eb = np.array(ns1raweb).flatten()
    elif n1 is not None:
        n1eb = np.zeros(n1.shape)
    if isinstance(ts2raw, number_types):
        t2 = np.array([ts2raw])
    elif isinstance(ts2raw, array_types):
        t2 = np.array(ts2raw).flatten()
    elif t1 is not None:
        t2 = t1.copy()
    if isinstance(ts2raweb, number_types):
        t2eb = np.array([ts2raweb])
    elif isinstance(ts2raweb, array_types):
        t2eb = np.array(ts2raweb).flatten()
    elif t2 is not None:
        t2eb = np.zeros(t2.shape)
    if isinstance(ns2raw, number_types):
        n2 = np.array([ns2raw])
    elif isinstance(ns2raw, array_types):
        n2 = np.array(ns2raw).flatten()
    elif n1 is not None:
        n2 = 0.9 * n1
    if isinstance(ns2raweb, number_types):
        n2eb = np.array([ns2raweb])
    elif isinstance(ns2raweb, array_types):
        n2eb = np.array(ns2raweb).flatten()
    elif n2 is not None:
        n2eb = np.zeros(n2.shape)
    if isinstance(as1, number_types) and float(as1) >= 1.0:
        a1 = float(as1)
    if isinstance(as2, number_types) and float(as2) >= 1.0:
        a2 = float(as2)
    if isinstance(zs1, number_types) and float(zs1) >= 1.0:
        z1 = float(zs1)
    if isinstance(zs2, number_types) and float(zs2) >= 1.0:
        z2 = float(zs2)
    if isinstance(debye, number_types):
        bmax = np.array([debye])
    elif isinstance(debye, array_types):
        bmax = np.array(debye).flatten()
    if isinstance(debyeeb, number_types):
        bmaxeb = np.array([debyeeb])
    elif isinstance(debyeeb, array_types):
        bmaxeb = np.array(debyeeb).flatten()
    elif bmax is not None:
        bmaxeb = np.zeros(bmax.shape)
#    if isinstance(teraw, number_types):
#        te = np.array([teraw])
#    elif isinstance(teraw, array_types):
#        te = np.array(teraw)
#    elif as1 is None and zs1 is None:
#        te = t1.copy()
#        eflag = False
#    if isinstance(neraw, number_types):
#        ne = np.array([neraw])
#    elif isinstance(neraw, array_types):
#        ne = np.array(neraw)
#    elif as1 is None and zs1 is None:
#        ne = n1.copy()
#        eflag = False

    equipower = None
    equipowereb = None
    if n1 is not None and t1 is not None and n1.shape == t1.shape and n1.shape == n2.shape and n2.shape == t2.shape:

#        p1 = n1 * t1
#        p2 = n2 * t2
#        pe = ne * te
#        zfilt = np.all([p1 > 0.0, p2 > 0.0, pe > 0.0], axis=0)

        # Value filters - set really low on purposes
        n1[n1 <= 0.0] = 1.0e5
        t1[t1 <= 0.0] = 1.0e-1
        n2[n2 <= 0.0] = 1.0e5
        t2[t2 <= 0.0] = 1.0e-1

        ee = 1.60217662e-19
        hbar = 6.3507799e-8  # hbar
        eoe = 5.5263494e7    # eps_0 / e
        eou = 9.64853329e7   # e / u
        mu = a1 * a2 / (a1 + a2)
        u2 = eou * (t1 / a1 + t2 / a2)                                       # [m^2/s^2]
        qmb = hbar / (2.0 * mu * np.sqrt(u2))                                # [m]
        ldb = eou / (4.0 * np.pi * eoe * mu * u2)                            # [m]
        qm_mask = (ldb < qmb)
        bmin = copy.deepcopy(ldb)
        if np.any(qm_mask):
            bmin[qm_mask] = qmb[qm_mask]

        u2eb = eou * np.sqrt(np.power(t1eb / a1, 2.0) + np.power(t2eb / a2, 2.0))
        qmbeb = np.abs(qmb * 0.5 * u2eb / u2)
        ldbeb = np.abs(ldb * u2eb / u2)
        bmineb = copy.deepcopy(ldbeb)
        if np.any(qm_mask):
            bmineb[qm_mask] = qmb[qm_mask]

        # If Debye length not given, calculate assuming plasma consists only of the two provided species
        if bmax is None:
            bmax = np.sqrt(eoe / (z1**2.0 * n1 / t1 + z2**2.0 * n2 / t2))    # [m]
            b1eb = (z1**2.0 * n1 / t1) * np.sqrt(np.power(n1eb / n1, 2.0) + np.power(t1eb / t1, 2.0))
            b2eb = (z2**2.0 * n2 / t2) * np.sqrt(np.power(n2eb / n2, 2.0) + np.power(t2eb / t2, 2.0))
            bmaxeb = bmax * 0.5 * np.sqrt(np.power(b1eb, 2.0) + np.power(b2eb, 2.0)) / (z1**2.0 * n1 / t1 + z2**2.0 * n2 / t2)
#        if eflag:
#            ibmax2 = ibmax2 + ne / (te * eoe)

        # Calculate Coulomb logarithm
        zfilt = np.all([np.isfinite(bmax), bmin > 0.0], axis=0)
        logcoul = np.zeros(n1.shape)
        logcoul[zfilt] = np.log(bmax[zfilt] / bmin[zfilt])                   # [1]
        logcouleb = np.zeros(logcoul.shape)
        logcouleb[zfilt] = np.sqrt(np.power(bmaxeb[zfilt] / bmax[zfilt], 2.0) + np.power(bmineb[zfilt] / bmin[zfilt], 2.0))

        # Coulomb logarithm from Wesson
#        logcoul = 15.2 - 0.5 * np.log(n1 / 1.0e19) + np.log(t1 / 1.0e3)
#        logcouleb = (t1 / np.sqrt(n1)) * np.sqrt(np.power(t1eb / t1, 2.0) + 0.5 * np.power(n1eb / n1, 2.0))

        # Calculate collisionality
        #    Note that these strange parameter combinations are simply to avoid overflow / underflow errors in SI units
#        cc = np.sqrt(np.pi / 8.0) / 16.0                                     # [1]
        cc = np.sqrt(2.0 / np.pi) / (6.0 * np.pi)                            # [1]
        pc = np.power(z1 * z2, 2.0) / (a1 * a2)                              # [1]
        dens = n2 * ee * np.power(eou / eoe, 2.0)                            # [1/s^2] * e
#        dens = n1 * ee * np.power(eou / eoe, 2.0)
        relv = np.power(u2, -1.5) / ee                                       # [s^3/m^3] / e
#        relv = np.power(eou * t1 / a1, -1.5) / ee
#        collrate = np.full(n1.shape, np.NaN)
        collrate = cc * pc * dens * relv * logcoul                           # [1/s]
        if momentum:
            momc = (a1 + a2) / (2.0 * a1)                                    # Conversion factor to momentum transfer collisionality from 
            collrate = collrate * momc

        denseb = np.abs(n2eb * ee * np.power(eou / eoe, 2.0))
#        denseb = np.abs(n1eb * ee * np.power(eou / eoe, 2.0))
        relveb = np.abs(relv * 1.5 * u2eb / u2)
#        relveb = np.abs(relv * 1.5 * t1eb / t1)
        collrateeb = cc * pc * np.sqrt(np.power(denseb * relv * logcoul, 2.0) + np.power(dens * relveb * logcoul, 2.0) + np.power(dens * relv * logcouleb, 2.0))
        if momentum:
            momc = (a1 + a2) / (2.0 * a1)
            collrateeb = collrateeb * momc

        # Calculate equipartition flux based on collisionality
        tdiff = ee * (t1 - t2)                                               # [J]
        ediff = 1.5 * n1 * tdiff                                             # [J/m^3]
#        equipower = np.zeros(n1.shape)
        equipower = ediff * collrate                                         # [W/m^3]

        tdiffeb = ee * np.sqrt(np.power(t1eb, 2.0) + np.power(t2eb, 2.0))
        ediffeb = 1.5 * np.sqrt(np.power(n1eb * tdiff, 2.0) + np.power(n1 * tdiffeb, 2.0))
        equipowereb = np.sqrt(np.power(ediffeb * collrate, 2.0) + np.power(ediff * collrateeb, 2.0))

        if verbose >= 3:
            print(cc)
            print(pc)
            print(dens)
            print(relv)
        if verbose >= 2:
            print(ec)
            print(temp)
            print(collrate)
        if verbose >= 1:
            print(equipower)

    return (equipower, equipowereb)


def equipartition_test(volume, equiflux, srceraw, srciraw, equifluxeb=None, srceebraw=None, srciebraw=None, srcrad=None, srcradeb=None, passwidth=None):
    """
    Checks validity of temperature profiles via comparison of integrated equipartition
    flux against integrated power deposition profiles.

    :arg volume: array. Vector of flux surface volumes corresponding to ordered radial points.

    :arg equiflux: array. Vector of equipartition fluxes corresponding to ordered radial points.

    :arg srceraw: array. Vector of heat deposition to electrons corresponding to ordered radial points.

    :arg srciraw: array. Vector of heat deposition to ions corresponding to ordered radial points.

    :kwarg srceebraw: array. Vector of 1 sigma errors for heat deposition to electrons corresponding to ordered radial points.

    :kwarg srciebraw: array. Vector of 1 sigma errors for heat deposition to ions corresponding to ordered radial points.

    :kwarg srcrad: array. Vector of radiated power corresponding to ordered radial points. Not implemented yet.

    :kwarg srcradeb: array. Vector of 1 sigma errors for radiated power corresponding to ordered radial points. Not implemented yet.

    :kwarg passwidth: float. Tolerance with respect to error bar within which the test passes.

    :returns: (bool, array, array, array, bool).
        Result of equipartition power feasibility, vector of cumulative integration of equipartition flux,
        vector of cumulative integration of electron heat source, vector cumulative integration of ion
        heat source, flag indicating equipartition flux primarily flows from ions to electrons.
    """
    vol = None
    eqf = None
    eqfeb = None
    hse = None
    hseeb = None
    hsi = None
    hsieb = None
    hsr = None    # Radiated power vectors not used in this scheme
    hsreb = None
    eps = 1.96    # Default is within 95% confidence interval
    if isinstance(volume, array_types):
        vol = np.array(volume).flatten()
    if isinstance(equiflux, array_types):
        eqf = np.array(equiflux).flatten()
    if isinstance(equifluxeb, array_types):
        eqfeb = np.array(equifluxeb).flatten()
    if isinstance(srceraw, array_types):
        hse = np.array(srceraw).flatten()
    if isinstance(srciraw, array_types):
        hsi = np.array(srciraw).flatten()
    if isinstance(srceebraw, array_types):
        hseeb = np.array(srceebraw).flatten()
    if isinstance(srciebraw, array_types):
        hsieb = np.array(srciebraw).flatten()
    if isinstance(srcrad, array_types):
        hsr = np.array(srcrad).flatten()
    if isinstance(srcradeb, array_types):
        hsreb = np.array(srcradeb).flatten()
    if isinstance(passwidth, number_types) and float(passwidth) >= 0.0:
        eps = float(passwidth)

    idxmax = None
    eqclose = None
    srcclose = None
    srccloseeb = None
    result = None
    if vol is not None and eqf is not None and hse is not None and hsi is not None and vol.size == eqf.size and vol.size == hse.size and vol.size == hsi.size:
        equipow = cumtrapz(eqf, vol, initial=0.0)
        equipoweb = np.sqrt(cumtrapz(np.power(eqfeb, 2.0), vol, initial=0.0)) if eqfeb is not None else 0.05 * equipow
        dfilt = np.invert(np.isnan(eqf))
        srcepow = cumtrapz(hse, vol, initial=0.0)
        srcepoweb = np.sqrt(cumtrapz(np.power(hseeb, 2.0), vol, initial=0.0)) if hseeb is not None else 0.05 * srcepow
        srcipow = cumtrapz(hsi, vol, initial=0.0)
        srcipoweb = np.sqrt(cumtrapz(np.power(hsieb, 2.0), vol, initial=0.0)) if hsieb is not None else 0.05 * srcipow
#        srcrpow = cumtrapz(hsr, vol, initial=0.0) if hsr is not None else np.zeros(equipow.shape)
#        srcrpoweb = np.sqrt(cumtrapz(np.power(hsreb, 2.0), vol, initial=0.0)) if hsreb is not None else 0.05 * srcrpow
        ffilt = np.all([np.isfinite(equipow), np.isfinite(srcepow), np.isfinite(srcipow)], axis=0)

        # Split equipartition power based on sign to determine whether to compare to ion heating power or electron heating power
        srcvec = np.zeros(eqf.shape)
        srcebvec = np.zeros(eqf.shape)
        srctvec = srcvec[ffilt]
        srcebtvec = srcebvec[ffilt]
        efilt = (eqf[ffilt] > 0.0)
        ifilt = np.invert(efilt)
        srctvec[efilt] = srcepow[ffilt][efilt]
        srcebtvec[efilt] = srcepoweb[ffilt][efilt]
        srctvec[ifilt] = srcipow[ffilt][ifilt]
        srcebtvec[ifilt] = srcipoweb[ffilt][ifilt]
        srcvec[ffilt] = srctvec
        srcebvec[ffilt] = srcebtvec
        testvec = np.zeros(eqf.shape) + 1.0e-10
        gfilt = (srcvec != 0.0)
        testvec[gfilt] = (srcvec[gfilt] - np.abs(equipow[gfilt])) / np.abs(srcvec[gfilt])
        idxmax = np.where(testvec == np.nanmin(testvec[1:]))[0][0]
        eqclose = equipow[idxmax]
        eqcloseeb = equipoweb[idxmax]
        srcclose = srcvec[idxmax]
        srccloseeb = srcebvec[idxmax]
        testvec[gfilt] = testvec[gfilt] + eps * (srcebvec[gfilt] + equipoweb[gfilt]) / np.abs(srcvec[gfilt])
        result = True if np.all(testvec > 0.0) else False

    return (result, idxmax, eqclose, eqcloseeb, srcclose, srccloseeb)


def heating_power_test(volume, srceraw, srciraw, srceraweb=None, srciraweb=None, totalpow=None, totalpoweb=None, passwidth=None):
    """
    Checks validity of energy deposition profiles via comparison of integrated
    deposition profile against total injected heating power.

    :arg volume: array. Vector of flux surface volumes corresponding to ordered radial points.

    :arg srceraw: array. Vector of heat deposition to electrons corresponding to ordered radial points.

    :arg srciraw: array. Vector of heat deposition to ions corresponding to ordered radial points.

    :kwarg srceraweb: array. Vector of errors associated with heat deposition to electrons, assumed to be 1 sigma.

    :kwarg srciraweb: array. Vector of errors associated with heat deposition to ions, assumed to be 1 sigma.

    :kwarg totalpow: float. Total injected power taken from power injection devices.

    :kwarg totalpoweb: float. Error bar of total injected power, assumed to be 1 sigma error.

    :kwarg passwidth: float. Tolerance with respect to error bar within which the test passes.

    :returns: (bool, float).
        Result of energy deposition profile feasibility, total power from integrated deposition profiles.
    """
    vol = None
    srce = None
    srci = None
    srceeb = None
    srcieb = None
    tpow = 0.0
    tpoweb = 0.0
    eps = 1.96    # Default is within 95% confidence interval
    if isinstance(volume, array_types):
        vol = np.array(volume).flatten()
    if isinstance(srceraw, array_types):
        srce = np.array(srceraw).flatten()
    if isinstance(srciraw, array_types):
        srci = np.array(srciraw).flatten()
    if isinstance(srceraweb, array_types):
        srceeb = np.array(srceraweb).flatten()
    if isinstance(srciraweb, array_types):
        srcieb = np.array(srciraweb).flatten()
    if isinstance(totalpow, number_types) and float(totalpow) > 0.0:
        tpow = float(totalpow)
    if isinstance(totalpoweb, number_types) and float(totalpoweb) > 0.0:
        tpoweb = float(totalpoweb)
    if isinstance(passwidth, number_types) and float(passwidth) >= 0.0:
        eps = float(passwidth)

    result = None
    srcpow = None
    srcpoweb = None
    if vol is not None and srce is not None and srci is not None and vol.size == srce.size and vol.size == srci.size:
        nfilt = np.all([np.isfinite(srce), np.isfinite(srci)], axis=0)
        hse = srce[nfilt]
        hse[hse < 0.0] = 0.0
        hsi = srci[nfilt]
        hsi[hsi < 0.0] = 0.0
        srcpow = float(np.trapz(hse + hsi, vol[nfilt]))

        if srceeb is None:
            srceeb = np.zeros(srce.shape)
        if srcieb is None:
            srcieb = np.zeros(srci.shape)

        enfilt = np.all([nfilt, np.isfinite(srceeb), np.isfinite(srcieb)], axis=0)
        srceeb[np.invert(enfilt)] = 0.0
        srcieb[np.invert(enfilt)] = 0.0
        hseeb = np.power(srceeb, 2.0)
        hsieb = np.power(srcieb, 2.0)
        srcpoweb = float(np.trapz(np.sqrt(hseeb + hsieb), vol))

        # Assumes error given as 1 sigma
        result = True if np.abs(tpow - srcpow) < np.sqrt(np.power(tpoweb, 2.0) + np.power(srcpoweb, 2.0)) * eps else False

    return (result, srcpow)


def energy_content_test(volume, peraw, peebraw=None, piraw=None, piebraw=None, pimpraw=None, pimpebraw=None, pfiraw=None, pfiebraw=None, wtot=None, wtoteb=None, passwidth=None):
    """
    Checks validity of density and temperature profiles via comparison of
    integrated pressure profiles times 1.5e against total stored energy in
    the plasma, W_diamagnetic.

    :arg volume: array. Vector of flux surface volumes corresponding to ordered radial points.

    :arg peraw: array. Vector of electron pressures corresponding to ordered radial points.

    :kwarg peebraw: array. Vector of 1 sigma errors for electron pressures corresponding to ordered radial points.

    :kwarg piraw: array. Vector of ion pressures corresponding to ordered radial points.

    :kwarg piebraw: array. Vector of 1 sigma errors for ion pressures corresponding to ordered radial points.

    :kwarg pimpraw: array. Vector of impurity ion pressures corresponding to ordered radial points.

    :kwarg pimpebraw: array. Vector of 1 sigma errors for impurity ion pressures corresponding to ordered radial points.

    :kwarg pfiraw: array. Vector of fast ion pressures corresponding to ordered radial points.

    :kwarg pfiebraw: array. Vector of 1 sigma errors for fast ion pressures corresponding to ordered radial points.

    :kwarg wtot: float. Total stored energy in the plasma, recommended to use W_diamagnetic based on magnetic measurements.

    :kwarg wtoteb: float. 1 sigma error for the total stored energy in the plasma.

    :kwarg passwidth: float. Tolerance with respect to error bar within which the test passes.

    :returns: (bool, float, float, float, float, float, float).
        Result of kinetic profile feasibility, integrated energy stored in electrons, 1 sigma error of 
        integrated energy stored in electrons, integrated energy stored in ions, 1 sigma error of
        integrated energy stored in ions, total integrated plasma energy, 1 sigma error of total
        integrated plasma energy.
    """
    vol = None
    pe = None
    pi = None
    peeb = None
    pieb = None
    pimp = None
    pimpeb = None
    pfi = None
    pfieb = None
    tnrg = 0.0
    tnrgeb = 0.0
    eps = 1.96
    if isinstance(volume, array_types):
        vol = np.array(volume).flatten()
    if isinstance(peraw, array_types):
        pe = np.array(peraw).flatten()
    if isinstance(peebraw, array_types):
        peeb = np.array(peebraw).flatten()
    elif isinstance(peebraw, number_types):
        if pe is not None:
            peeb = np.full(pe.shape, float(peebraw))
    if isinstance(piraw, array_types):
        pi = np.array(piraw).flatten()
    if isinstance(piebraw, array_types):
        pieb = np.array(piebraw).flatten()
    elif isinstance(piebraw, number_types):
        if pi is not None:
            pieb = np.full(pi.shape, float(piebraw))
    if isinstance(pimpraw, array_types):
        pimp = np.array(pimpraw).flatten()
    if isinstance(pimpebraw, array_types):
        pimpeb = np.array(pimpebraw).flatten()
    elif isinstance(pimpebraw, number_types):
        if pimp is not None:
            pimpeb = np.full(pimp.shape, float(pimpebraw))
    if isinstance(pfiraw, array_types):
        pfi = np.array(pfiraw).flatten()
    if isinstance(pfiebraw, array_types):
        pfieb = np.array(pfiebraw).flatten()
    elif isinstance(pfiebraw, number_types):
        if pfi is not None:
            pfieb = np.full(pfi.shape, float(pfiebraw))
    if isinstance(wtot, number_types) and float(wtot) > 0.0:
        tnrg = float(wtot)
    if isinstance(wtoteb, number_types) and float(wtoteb) > 0.0:
        tnrgeb = float(wtoteb)
    if isinstance(passwidth, number_types) and float(passwidth) > 0.0:
        eps = float(passwidth)

    result = None
    wei = None
    weieb = None
    wth = None
    wtheb = None
    wtot = None
    wtoteb = None
    if vol is not None and pe is not None and vol.size == pe.size:
        zfilt = (pe <= 0.0)
        pe[zfilt] = 0.0
        we = float(np.trapz(1.5 * pe, vol))
        weeb = 0.0
        if peeb is not None and vol.size == peeb.size:
            weeb = float(np.trapz(1.5 * peeb, vol))
        wi = 0.0
        wieb = 0.0
        if pi is not None and vol.size == pi.size:
            zfilt = (pi <= 0.0)
            pi[zfilt] = 0.0
            wi = float(np.trapz(1.5 * pi, vol))
            if pieb is not None and vol.size == pieb.size:
                wieb = float(np.trapz(1.5 * pieb, vol))
        else:
            wi = we
            wieb = 0.05 * wi + weeb
        wimp = 0.0
        wimpeb = 0.0
        if pimp is not None and vol.size == pimp.size:
            zfilt = (pimp <= 0.0)
            pimp[zfilt] = 0.0
            wimp = float(np.trapz(1.5 * pimp, vol))
            if pimpeb is not None and vol.size == pimpeb.size:
                wimpeb = float(np.trapz(1.5 * pimpeb, vol))
        wfi = 0.0
        wfieb = 0.0
        if pfi is not None and vol.size == pfi.size:
            zfilt = (pfi <= 0.0)
            pfi[zfilt] = 0.0
            wfi = float(np.trapz(1.5 * pfi, vol))
            if pfieb is not None and vol.size == pfieb.size:
                wfieb = float(np.trapz(1.5 * pfieb, vol))

        # Assumes error given as 1 sigma
        wei = we + wi
        weieb = weeb + wieb
        wth = wei + wimp
        wtheb = weieb + wimpeb
        wtot = wth + wfi
        wtoteb = wtheb + wfieb     # Does not add in quadrature - should it?
        result = True if np.abs(tnrg - wtot) < (tnrgeb + wtoteb) * eps else False

    return (result, wei, weieb, wth, wtheb, wtot, wtoteb)


def elm_detector(time, signal, ratio_threshold=1.5, end_criterion=0.8, delta_threshold=None, n_smooth=None):
    """
    """
    tvec = None
    svec = None
    rthr = 1.5
    ecrit = 0.8
    dthr = None
    navg = None
    if isinstance(time, array_types):
        tvec = np.array(time).flatten()
    if isinstance(signal, array_types):
        svec = np.array(signal).flatten()
    if isinstance(ratio_threshold, number_types) and float(ratio_threshold) > 1.0:
        rthr = float(ratio_threshold)
    if isinstance(end_criterion, number_types) and float(end_criterion) > 0.0 and float(end_criterion) < 1.0:
        ecrit = float(end_criterion)
    if isinstance(delta_threshold, number_types) and float(delta_threshold) > 0.0:
        dthr = float(delta_threshold)
    if isinstance(n_smooth, number_types) and int(n_smooth) > 1 and int(n_smooth) < svec.size - 1:
        navg = int(n_smooth)

    bvec = []
    dvec = []
    if tvec is not None and svec is not None and tvec.size == svec.size:
        if navg is not None:
            temp = ptools.moving_average(svec, navg=navg)
            svec = temp[:tvec.size]
        felm = True if svec[0] > np.nanmean(svec) else False
        bval = float(0.9 * np.nanmean(svec)) if felm else float(svec[0])
        mval = float(svec[0])
        if felm:
            bvec.append(float(tvec[0] - 0.01))
        for ii in np.arange(0, svec.size):
            if felm:
                if svec[ii] > mval:
                    mval = float(svec[ii])
                if len(bvec) > len(dvec):
                    dvec.append(float(tvec[ii] - bvec[-1]))
                else:
                    dvec[-1] = float(tvec[ii] - bvec[-1])
                if float((svec[ii] - bval) / (mval - bval + 1.0e-10)) < (1.0 - ecrit):
                    felm = False
                    bval = float(svec[ii])
            else:
                if dthr is not None and float(svec[ii] - bval) > dthr:
                    felm = True
                    bvec.append(float(tvec[ii-1]))
                    mval = float(svec[ii])
                elif float(svec[ii] / bval) > rthr:
                    felm = True
                    bvec.append(float(tvec[ii-1]))
                    mval = float(svec[ii])
                else:
                    bval = float(svec[ii])
        if felm and len(bvec) > len(dvec):
            dvec.append(float(tvec[-1] + 0.01 - bvec[-1]))

    return (bvec, dvec, svec)


def blip_detector(time, signal, ratio_threshold=1.5, dead_time=None, delta_threshold=None, n_smooth=None):
    """
    """
    tvec = None
    svec = None
    rthr = 1.5
    dead = 0.0
    dthr = None
    navg = None
    if isinstance(time, array_types):
        tvec = np.array(time).flatten()
    if isinstance(signal, array_types):
        svec = np.array(signal).flatten()
    if isinstance(ratio_threshold, number_types) and float(ratio_threshold) > 1.0:
        rthr = float(ratio_threshold)
    if isinstance(delta_threshold, number_types) and float(delta_threshold) > 0.0:
        dthr = float(delta_threshold)
    if isinstance(smoothing_factor, number_types) and int(smoothing_factor) > 1 and int(smoothing_factor) < svec.size - 1:
        navg = int(smoothing_factor)

    bvec = []
    if tvec is not None and svec is not None and tvec.size == svec.size:
        if navg is not None:
            temp = ptools.moving_average(svec, navg=navg)
            svec = temp[:tvec.size]
        ftrigger = False
        dtrigger = 0.0
        bval = float(0.9 * np.nanmean(svec)) if felm else float(svec[0])
        mval = float(svec[0])
        for ii in np.arange(0, svec.size):
            if not ftrigger:
                if dthr is not None and float(svec[ii] - bval) > dthr:
                    ftrigger = True
                    bvec.append(float(tvec[ii-1]))
                elif float(svec[ii] / bval) > rthr:
                    ftrigger = True
                    bvec.append(float(tvec[ii-1]))
                bval = float(svec[ii])
            else:
                dtrigger = dtrigger + (tvec[ii] - bvec[-1])
            if dtrigger >= dead:
                ftrigger = False
                dtrigger = 0.0

    return (bvec, dvec, svec)


def rz2pf(psi, rr, zz, ri, zi, axsval=None, bndval=None, style=0, fabs=False, order=2, prefilter=True):
    """
    Equilibrium mapping routine from R,Z -> psi_pol

    :arg psi: array. 2D poloidal flux array defining the conversion mapping.

    :arg rr: array. 2D R coordinate array corresponding to poloidal flux map.

    :arg zz: array. 2D Z coordinate array corresponding to poloidal flux map.

    :arg ri: array. R coordinate vector to be converted.

    :arg zi: array. Z coordinate vector to be converted.

    :kwarg axsval: float. Optional poloidal flux value corresponding to magnetic axis, for normalization.

    :kwarg bndval: float. Optional poloidal flux value corresponding to boundary, for normalization.

    :returns: array. Input array converted to poloidal flux coordinates.
    """
    psimap = None
    rmap = None
    zmap = None
    rvec = None
    zvec = None
    psivec = None
    vaxs = None
    vbnd = None
    rtype = 0
    if isinstance(psi, np.ndarray) and psi.ndim >= 2:
        psimap = psi.copy()
    if isinstance(rr, np.ndarray) and rr.ndim >= 2:
        rmap = rr.copy()
    if isinstance(zz, np.ndarray) and zz.ndim >= 2:
        zmap = zz.copy()
    if isinstance(ri, number_types):
        rvec = np.array([ri])
    elif isinstance(ri, (list, tuple)):
        rvec = np.array(ri)
    elif isinstance(ri, np.ndarray):
        rvec = ri.copy()
    if isinstance(zi, number_types):
        zvec = np.array([zi])
    elif isinstance(zi, (list, tuple)):
        zvec = np.array(zi)
    elif isinstance(zi, np.ndarray):
        zvec = zi.copy()
    if isinstance(axsval, number_types):
        vaxs = float(axsval)
    if isinstance(bndval, number_types):
        vbnd = float(bndval)
    if isinstance(style, (int, np_itypes)):
        rtype = int(style)

    if psimap is not None and rmap is not None and zmap is not None and rvec is not None and zvec is not None:
        if fabs:
            psimap = np.abs(psimap)
        if psimap.shape == rmap.shape and psimap.shape == zmap.shape:
            if rvec.size == 1 and zvec.size > 1:
                rvec = np.full(zvec.shape, rvec[0])
            elif zvec.size == 1 and rvec.size > 1:
                zvec = np.full(rvec.shape, zvec[0])
            if rvec.shape == zvec.shape:
                dr = (np.amax(rmap) - np.amin(rmap)) / (psimap.shape[1] - 1)
                dz = (np.amax(zmap) - np.amin(zmap)) / (psimap.shape[0] - 1)        
                scaling = np.array([dz, dr])
                offset  = np.array([np.amin(zmap), np.amin(rmap)])
                coords = np.array((zvec, rvec))
                index = ((coords.T - offset) / scaling).T
                psivec = map_coordinates(psimap, index, mode='nearest', order=order, prefilter=prefilter)

    if psivec is not None and vaxs is not None and vbnd is not None:
        # Default: Input and output in the same system (do nothing)
        if rtype == 1:    # Input unnormalized and output normalized
            psivec = (psivec - vaxs) / (vbnd - vaxs)
        elif rtype == 2:  # Input normalized and output unnormalized
            psivec = psivec * (vbnd - vaxs) + vaxs

    return psivec


def rz2b(psi, rr, zz, ri, zi, ff, pf, deltar=0.01, deltaz=0.01, axsval=None, bndval=None, fnorm=False, fabs=False, order=2, prefilter=True):
    """
    """
    rvec = None
    zvec = None
    fvec = None
    pvec = None
    dr = 0.01
    dz = 0.01
    vaxs = None
    vbnd = None
    bvec = None
    if isinstance(ri, number_types):
        rvec = np.array([ri])
    elif isinstance(ri, (list, tuple)):
        rvec = np.array(ri)
    elif isinstance(ri, np.ndarray):
        rvec = ri.copy()
    if isinstance(zi, number_types):
        zvec = np.array([zi])
    elif isinstance(zi, (list, tuple)):
        zvec = np.array(zi)
    elif isinstance(zi, np.ndarray):
        zvec = zi.copy()
    if isinstance(ff, number_types):
        fvec = np.array([ff])
    elif isinstance(ff, (list, tuple)):
        fvec = np.array(ff)
    elif isinstance(ff, np.ndarray):
        fvec = ff.copy()
    if isinstance(pf, number_types):
        pvec = np.array([pf])
    elif isinstance(pf, (list, tuple)):
        pvec = np.array(pf)
    elif isinstance(pf, np.ndarray):
        pvec = pf.copy()
    if isinstance(deltar, number_types) and float(deltar) > 0.0:
        dr = float(deltar)
    if isinstance(deltaz, number_types) and float(deltaz) > 0.0:
        dz = float(deltaz)
    if isinstance(axsval, number_types):
        vaxs = float(axsval)
    if isinstance(bndval, number_types):
        vbnd = float(bndval)

    if rvec is not None and zvec is not None and fvec is not None and pvec is not None and pvec.size == fvec.size:

        # Compute four points needed for to calculate 2D derivative via central difference scheme
        rl = rvec - (dr / 2.0)
        ru = rvec + (dr / 2.0)
        zl = zvec - (dz / 2.0)
        zu = zvec + (dz / 2.0)

        style = 2 if fnorm else 0
        pxrz = rz2pf(psi, rr, zz, rvec, zvec, axsval=vaxs, bndval=vbnd, style=style, fabs=fabs, order=order, prefilter=prefilter)
        pfrl = rz2pf(psi, rr, zz, rl, zvec, axsval=vaxs, bndval=vbnd, style=style, fabs=fabs, order=order, prefilter=prefilter)
        pfru = rz2pf(psi, rr, zz, ru, zvec, axsval=vaxs, bndval=vbnd, style=style, fabs=fabs, order=order, prefilter=prefilter)
        pfzl = rz2pf(psi, rr, zz, rvec, zl, axsval=vaxs, bndval=vbnd, style=style, fabs=fabs, order=order, prefilter=prefilter)
        pfzu = rz2pf(psi, rr, zz, rvec, zu, axsval=vaxs, bndval=vbnd, style=style, fabs=fabs, order=order, prefilter=prefilter)

        if pxrz is not None and pfrl is not None and pfru is not None and pfzl is not None and pfzu is not None:
            # |grad_psi| = Vector addition of orthogonal directional derivatives of psi in r and z
            dprz = np.sqrt(np.power((pfru - pfrl) / dr, 2.0) + np.power((pfzu - pfzl) / dz, 2.0))

            # F = Interpolation of F(psi) = R * B_tor at magnetic axis to psi_norm of (r,z) points
            if fnorm and vaxs is not None and vbnd is not None:
                pvec = pvec * (vbnd - vaxs) + vaxs
            ffunc = interp1d(pvec, fvec, kind='linear', bounds_error=False, fill_value='extrapolate')
            ffrz = ffunc(pxrz)

            # |B_pol| = |grad_psi| / R
            bpol = dprz / rvec

            # |B_tor| = F / R
            btor = ffrz / rvec

            # |B| = Vector addition of toroidal magnetic field (given by F) and poloidal magnetic field (given by grad_psi)
            bvec = np.sqrt(np.power(bpol, 2.0) + np.power(btor, 2.0))

    return (bvec, bpol, btor)


def calc_midplane_averaged_coordinates_from_scratch(rmajo, rmaji, rlcfso, rlcfsi, rmajoj=None, rmajij=None, rmajoe=None, rmajie=None, rlcfsoe=None, rlcfsie=None):
    """
    """
    # The original radial coordinates are implicitly defined by rmajoj and rmajij arguments
    rmajl = None                    # midplane-averaged major radius of last-closed-flux-surface (separatrix)
    rmajleb = 0.0
    rminl = None                    # midplane-averaged minor radius of last-closed-flux-surface (separatrix)
    rminleb = 0.0
    rmaja = None                    # midplane-averaged major radius of each labelled flux surface
    drmaja = np.ones(rmajo.shape)   # derivative of midplane-averaged major radius with respect to the original radial coordinate
    rmajaeb = np.zeros(rmajo.shape)
    jrmaja = np.ones(rmajo.shape)   # derivative of the original radial coordinate with respect to midplane-averaged major radius (used as Jacobian for gradient conversion)
    rmina = None                    # midplane-average minor radius of each labelled flux surface
    drmina = np.ones(rmajo.shape)   # derivative of midplane-average minor radius with respect to the original radial corodinate
    rminaeb = np.zeros(rmajo.shape)
    jrmina = np.ones(rmajo.shape)   # derivative of the original radial coordinate with respect to midplane-average minor radius (used as Jacobian for gradient conversion)

    # Filter on original to prevent divide by zero errors, however unlikely they are
    jfilt = np.all([np.abs(rmajoj) > 1.0e-6, np.abs(rmajij) > 1.0e-6, rmajoj != rmajij], axis=0)

    rmajl = float((rlcfso + rlcfsi) / 2.0)
    rminl = float((rlcfso - rlcfsi) / 2.0)
    if rlcfsoe is not None and rlcfsie is not None:
        rmajleb = float(np.sqrt(np.power(rlcfsoe, 2.0) + np.power(rlcfsie, 2.0)) / 2.0)
        rminleb = float(np.sqrt(np.power(rlcfsoe, 2.0) + np.power(rlcfsie, 2.0)) / 2.0)

    rmaja = (rmajo + rmaji) / 2.0
    rmina = (rmajo - rmaji) / 2.0
    if rmajoe is not None and rmajie is not None:
        rmajaeb = np.sqrt(np.power(rmajoe, 2.0) + np.power(rmajie, 2.0)) / 2.0
        rminaeb = np.sqrt(np.power(rmajoe, 2.0) + np.power(rmajie, 2.0)) / 2.0
    if rmajoj is not None and rmajij is not None:
        if np.any(jfilt):
            jrmaja[jfilt] = 2.0 * rmajoj[jfilt] * rmajij[jfilt] / (rmajij[jfilt] + rmajoj[jfilt])
            drmaja[jfilt] = (rmajij[jfilt] + rmajoj[jfilt]) / (2.0 * rmajoj[jfilt] * rmajij[jfilt])
            jrmina[jfilt] = 2.0 * rmajoj[jfilt] * rmajij[jfilt] / (rmajij[jfilt] - rmajoj[jfilt])
            drmina[jfilt] = (rmajij[jfilt] - rmajoj[jfilt]) / (2.0 * rmajoj[jfilt] * rmajij[jfilt])
        ifilt = np.isfinite(jrmina)
        if np.any(np.invert(ifilt)):
            ifunc = interp1d(rmina[ifilt], jrmina[ifilt], bounds_error=False, fill_value='extrapolate')
            jrmina[np.invert(ifilt)] = ifunc(rmina[np.invert(ifilt)])

    return (rmajl, rmajleb, rminl, rminleb, rmaja, drmaja, rmajaeb, jrmaja, rmina, drmina, rminaeb, jrmina)


def calc_generic_normalized_gradient(val_in, scale=1.0, derivative=None, jacobian=None, normalization=None, val_err=None, der_err=None, enforce_positive=False, default_der_err=0.01):
    """
    """
    vvec = None
    dvec = None
    def_derr = 0.01
    if isinstance(val_in, number_types):
        vvec = np.array([val_in])
    elif isinstance(val_in, array_types):
        vvec = np.array(val_in)
    if isinstance(default_der_err, number_types) and float(default_der_err) >= 0.0:
        def_derr = float(default_der_err)

    vvec_out = None
    verr_out = None
    avec_out = None
    aerr_out = None
    if vvec is not None and len(vvec) > 0:
        sc = np.ones(vvec.shape)
        dvec = np.zeros(vvec.shape)
        jvec = np.ones(vvec.shape)
        nvec = np.ones(vvec.shape)
        verr = np.zeros(vvec.shape)
        derr = np.zeros(vvec.shape)
        if isinstance(scale, number_types):
            sc = np.full(vvec.shape, float(scale))
        elif isinstance(scale, array_types) and len(scale) == len(vvec):
            sc = np.array(scale)
        if isinstance(derivative, number_types):
            dvec = np.full(vvec.shape, derivative)
        elif isinstance(derivative, array_types) and len(derivative) == len(vvec):
            dvec = np.array(derivative)
        if isinstance(jacobian, number_types):
            jvec = np.full(vvec.shape, jacobian)
        elif isinstance(jacobian, array_types) and len(jacobian) == len(vvec):
            jvec = np.array(jacobian)
        if isinstance(normalization, number_types):
            nvec = np.full(vvec.shape, normalization)
        elif isinstance(normalization, array_types) and len(normalization) == len(vvec):
            nvec = np.array(normalization)
        if isinstance(val_err, number_types):
            verr = np.full(vvec.shape, val_err)
        elif isinstance(val_err, array_types) and len(val_err) == len(vvec):
            verr = np.array(val_err)
        if isinstance(der_err, number_types):
            derr = np.full(vvec.shape, der_err)
        elif isinstance(der_err, array_types) and len(der_err) == len(vvec):
            derr = np.array(der_err)

        zfilt = (vvec > 0.0) if enforce_positive else (np.abs(vvec) > 0.0)
        dfilt = np.all([np.abs(dvec) > 0.0, zfilt], axis=0)
        vvec_out = vvec / sc
        if np.any(np.invert(zfilt)):
            vvec_out[np.invert(zfilt)] = 1.0e-3 * np.nanmax(vvec_out)
        verr_out = verr / sc
        avec_out = np.zeros(vvec.shape)
        aerr_out = np.full(vvec.shape, def_derr)     # Given value should just be a placeholder
        if np.any(zfilt):
            avec_out[zfilt] = nvec[zfilt] * jvec[zfilt] * dvec[zfilt] / vvec[zfilt]
        if np.any(dfilt):
            aerr_out[dfilt] = np.abs(avec_out[dfilt]) * np.sqrt(np.power(derr[dfilt] / dvec[dfilt], 2.0) + np.power(verr[dfilt] / vvec[dfilt], 2.0))

    return (vvec_out, verr_out, avec_out, aerr_out)


def calc_iter_h98y2_confinement_time(plasma_current, toroidal_field, line_density, total_power, major_radius, elongation, inverse_aspect_ratio, isotope_mass,
                                     plasma_current_error=None, toroidal_field_error=None, line_density_error=None, total_power_error=None,
                                     major_radius_error=None, elongation_error=None, inverse_aspect_ratio_error=None, isotope_mass_error=None):
    """
    Calculate ITERH-98P(y,2) scaled confinement time, requires inputs to be already scaled appropriately.
    """
    ip = None
    ipeb = 0.0
    bt = None
    bteb = 0.0
    nela = None
    nelaeb = 0.0
    tpow = None
    tpoweb = 0.0
    rmaj = None
    rmajeb = 0.0
    elong = None
    elongeb = 0.0
    iaspect = None
    iaspecteb = 0.0
    afuel = None
    afueleb = 0.0
    if isinstance(plasma_current, number_types):
        ip = float(np.abs(plasma_current))
    if isinstance(plasma_current_error, number_types):
        ipeb = float(plasma_current_error)
    if isinstance(toroidal_field, number_types):
        bt = float(np.abs(toroidal_field))
    if isinstance(toroidal_field_error, number_types):
        bteb = float(toroidal_field_error)
    if isinstance(line_density, number_types):
        nela = float(line_density)
    if isinstance(line_density_error, number_types):
        nelaeb = float(line_density_error)
    if isinstance(total_power, number_types):
        tpow = float(total_power)
    if isinstance(total_power_error, number_types):
        tpoweb = float(total_power_error)
    if isinstance(major_radius, number_types):
        rmaj = float(major_radius)
    if isinstance(major_radius_error, number_types):
        rmajeb = float(major_radius_error)
    if isinstance(elongation, number_types):
        elong = float(elongation)
    if isinstance(elongation_error, number_types):
        elongeb = float(elongation_error)
    if isinstance(inverse_aspect_ratio, number_types):
        iaspect = float(inverse_aspect_ratio)
    if isinstance(inverse_aspect_ratio_error, number_types):
        iaspecteb = float(inverse_aspect_ratio_error)
    if isinstance(isotope_mass, number_types):
        afuel = float(isotope_mass)
    if isinstance(isotope_mass_error, number_types):
        afueleb = float(isotope_mass_error)

    taue = None
    taueeb = None
    if ip is not None and bt is not None and nela is not None and tpow is not None and rmaj is not None and elong is not None and iaspect is not None and afuel is not None:
        cfac = 0.0562
        exponents = [0.93, 0.15, 0.41, -0.69, 1.97, 0.78, 0.58, 0.19]
        values = [ip, bt, nela, tpow, rmaj, elong, iaspect, afuel]
        errors = [ipeb, bteb, nelaeb, tpoweb, rmajeb, elongeb, iaspecteb, afueleb]
        taue = cfac
        taueeb = 0.0
        for ii in np.arange(0, len(exponents)):
            val = np.power(values[ii], exponents[ii]) if values[ii] != 0.0 else 0.0
            err = exponents[ii] * np.power(values[ii], exponents[ii] - 2.0) * errors[ii] if values[ii] != 0.0 else 0.0
            taue *= val
            taueeb += err

    return (taue, taueeb)


def calc_scaling_confinement_time(style=0, **kwargs):

    scaling_law_dict = {0: "ITERH-98P(y,2)"}

    taue = None
    taueeb = None
    if style in scaling_law_dict:
        law = scaling_law_dict[style]
        if law == "ITERH-98P(y,2)":
            ip = kwargs.get("plasma_current", None)
            bt = kwargs.get("toroidal_field", None)
            nl = kwargs.get("line_density", None)
            pi = kwargs.get("total_power", None)
            rm = kwargs.get("major_radius", None)
            am = kwargs.get("minor_radius", None)
            el = kwargs.get("elongation", None)
            ai = kwargs.get("isotope_mass", None)
            ipe = kwargs.get("plasma_current_error", None)
            bte = kwargs.get("toroidal_field_error", None)
            nle = kwargs.get("line_density_error", None)
            pie = kwargs.get("total_power_error", None)
            rme = kwargs.get("major_radius_error", None)
            ame = kwargs.get("minor_radius_error", None)
            ele = kwargs.get("elongation_error", None)
            aie = kwargs.get("isotope_mass_error", None)
            ia = am / rm if rm is not None and am is not None else None
            iae = ia * np.sqrt(np.power(ame / am, 2.0) + np.power(rme / rm, 2.0)) if ia is not None and rme is not None and ame is not None else None
            if ip is not None:
                ip = ip * 1.0e-6   # Convert to MA
                if ipe is not None:
                    ipe = ipe * 1.0e-6
            if nl is not None:
                nl = nl * 1.0e-19  # Convert to 10^19 m^-3
                if nle is not None:
                    nle = nle * 1.0e-19
            if pi is not None:
                pi = pi * 1.0e-6   # Convert to MW
                if pie is not None:
                    pie = pie * 1.0e-6
            (taue, taueeb) = calc_iter_h98y2_confinement_time(ip, bt, nl, pi, rm, el, ia, ai, ipe, bte, nle, pie, rme, ele, iae, aie)

    return (taue, taueeb)


def calc_greenwald_density_limit(plasma_current, minor_radius, plasma_current_error=None, minor_radius_error=None):
    """
    """
    ip = None
    ipeb = 0.0
    rmin = None
    rmineb = 0.0
    if isinstance(plasma_current, number_types):
        ip = float(np.abs(plasma_current))
    if isinstance(plasma_current_error, number_types):
        ipeb = float(plasma_current_error)
    if isinstance(minor_radius, number_types):
        rmin = float(minor_radius)
    if isinstance(minor_radius_error, number_types):
        rmineb = float(minor_radius_error)

    denslim = None
    denslimeb = None
    if ip is not None and rmin is not None:
        cfac = 1.0e14   # Conversion from 10^20 m^-3 / MA -> m^-3 / A
        denslim = cfac * ip / (np.pi * np.power(rmin, 2.0))
        denslimeb = denslim * np.sqrt(np.power(ipeb / ip, 2.0) + np.power(2.0 * rmineb / rmin, 2.0))

    return (denslim, denslimeb)


def calc_beryllium_charge(te):
    tvec = np.array([    0.0,    10.0,    20.0,    40.0,    60.0,    80.0,
                       100.0,   120.0,   140.0,   160.0,   180.0,   200.0,
                      1000.0])
    zvec = np.array([ 0.0, 1.3, 2.2, 3.1, 3.5, 3.7, 3.8, 3.9, 3.95, 3.99, 3.998, 3.999, 4.0])
    zfunc = interp1d(tvec, zvec, kind='linear', bounds_error=False, fill_value=(zvec[0], zvec[-1]))
    z_out = zfunc(te)
    #upper_mask = (z_out > 4.0)
    #if np.any(upper_mask):
    #    z_out[upper_mask] = 4.0
    return z_out


def calc_carbon_charge(te):
    tvec = np.array([    0.0,   20.0,   40.0,   60.0,   80.0,  100.0,  120.0,  140.0,  160.0,
                       180.0,  200.0,  220.0,  240.0,  260.0,  280.0,  300.0,  320.0,  340.0,
                       360.0,  380.0,  400.0,  550.0,  650.0,  825.0, 1250.0, 2500.0])
    zvec = np.array([ 0.000, 1.300, 2.500, 3.600, 4.500, 5.250, 5.600, 5.780, 5.870, 5.915,
                      5.945, 5.958, 5.968, 5.975, 5.980, 5.982, 5.985, 5.988, 5.990, 5.991,
                      5.992, 5.996, 5.997, 5.998, 5.999, 6.000])
    zfunc = interp1d(tvec, zvec, kind='linear', bounds_error=False, fill_value=(zvec[0], zvec[-1]))
    z_out = zfunc(te)
    #upper_mask = (z_out > 6.0)
    #if np.any(upper_mask):
    #    z_out[upper_mask] = 6.0
    return z_out


def calc_nickel_charge(te):
    tvec = np.append(np.linspace(0.0, 3500.0, 36), [14000.0])
    zvec = np.array([  0.00, 11.00, 15.00, 17.00, 17.65, 18.20, 18.75, 19.50, 20.25, 21.00,
                      21.70, 22.30, 22.90, 23.50, 23.90, 24.25, 24.50, 24.80, 24.90, 25.10,
                      25.20, 25.30, 25.35, 25.40, 25.45, 25.50, 25.55, 25.60, 25.65, 25.70,
                      25.75, 25.80, 25.85, 25.90, 25.95, 26.00, 27.00])
    zfunc = interp1d(tvec, zvec, kind='linear', bounds_error=False, fill_value=(zvec[0], zvec[-1]))
    z_out = zfunc(te)
    #upper_mask = (z_out > 26.0)  # Assumed upper limit for practical scenarios
    #if np.any(upper_mask):
    #    z_out[upper_mask] = 26.0
    return z_out


def calc_tungsten_charge(te):
    tvec = np.append(np.linspace(0.0, 8000.0, 81), [9000.0])
    zvec = np.array([  0.00, 13.50, 17.50, 19.70, 21.40, 22.50, 23.50, 24.20, 24.90, 25.60,
                      26.30, 27.00, 27.70, 28.40, 29.10, 29.80, 30.50, 31.30, 32.10, 32.80,
                      33.40, 33.90, 34.50, 35.30, 36.20, 37.10, 38.20, 39.40, 40.50, 41.50,
                      42.30, 42.90, 43.40, 43.80, 44.10, 44.30, 44.50, 44.80, 45.00, 45.10,
                      45.30, 45.50, 45.65, 45.80, 45.95, 46.10, 46.25, 46.40, 46.55, 46.70,
                      46.85, 47.00, 47.15, 47.30, 47.45, 47.60, 47.75, 47.90, 48.05, 48.20,
                      48.35, 48.50, 48.65, 48.80, 48.95, 49.10, 49.25, 49.40, 49.55, 49.70,
                      49.85, 50.00, 50.20, 50.40, 50.60, 50.80, 51.00, 51.20, 51.40, 51.60,
                      51.80, 52.00])
    zfunc = interp1d(tvec, zvec, kind='linear', bounds_error=False, fill_value=(zvec[0], zvec[-1]))
    z_out = zfunc(te)
    #upper_mask = (z_out > 52.0)  # Assumed upper limit for practical scenarios
    #if np.any(upper_mask):
    #    z_out[upper_mask] = 52.0
    return z_out


def calc_beryllium_cooling_factor(te):
    tvec = np.linspace(0.0, 5.0, 51)
    lz_be = np.array([ -32.15490, -31.92082, -31.76955, -31.69897, -31.82391, -32.22185, -32.82391,
                       -33.52288, -34.04576, -34.34679, -34.52288, -34.25964, -33.69897, -33.39794,
                       -33.22185, -33.09691, -33.04576, -33.12494, -33.30103, -33.60206, -33.92082,
                       -34.09691, -34.22185, -34.30103, -34.37675, -34.42022, -34.45593, -34.48149,
                       -34.50864, -34.52288, -34.52288, -34.50864, -34.48149, -34.45593, -34.42022,
                       -34.39794, -34.37675, -34.35655, -34.33724, -34.31876, -34.30103, -34.28400,
                       -34.26761, -34.25181, -34.23657, -34.22185, -34.20761, -34.19382, -34.18046,
                       -34.16749, -34.15490])
    lzfunc = interp1d(tvec, lz_be, kind='cubic', bounds_error=False, fill_value=(lz_be[0], lz_be[-1]))
    lz_out = np.power(10.0, lzfunc(np.log10(te)))
    return lz_out


def calc_carbon_cooling_factor(te):
    tvec = np.linspace(0.0, 5.0, 51)
    lz_c = np.array([ -34.39794, -33.15490, -32.00000, -31.52288, -31.30103, -31.22185, -31.27572,
                      -31.39794, -31.69897, -32.15490, -32.69897, -33.30103, -33.69897, -33.95861,
                      -34.00000, -33.69897, -33.39794, -33.22185, -33.15490, -33.15490, -33.30103,
                      -33.52288, -33.69897, -33.82391, -33.92082, -34.00000, -34.04576, -34.09691,
                      -34.13668, -34.15490, -34.16749, -34.16115, -34.15490, -34.12494, -34.08092,
                      -34.03152, -33.97881, -33.92082, -33.85387, -33.76955, -33.69897, -33.65758,
                      -33.61979, -33.58503, -33.55284, -33.52288, -33.49485, -33.46852, -33.44370,
                      -33.42022, -33.39794])
    lzfunc = interp1d(tvec, lz_c, kind='cubic', bounds_error=False, fill_value=(lz_c[0], lz_c[-1]))
    lz_out = np.power(10.0, lzfunc(np.log10(te)))
    return lz_out


def calc_nickel_cooling_factor(te):
    tvec = np.linspace(0.0, 5.0, 51)
    lz_ni = np.array([ -31.69897, -31.75696, -31.83863, -31.92082, -31.97881, -32.02228, -32.04576,
                       -32.06048, -32.05552, -32.02228, -31.92082, -31.69897, -31.45593, -31.25964,
                       -31.09691, -30.95861, -30.83863, -30.75696, -30.69897, -30.67366, -30.67778,
                       -30.69897, -30.74473, -30.88606, -31.14267, -31.37675, -31.52288, -31.58503,
                       -31.62893, -31.63827, -31.64782, -31.72125, -31.90309, -32.09691, -32.22185,
                       -32.25964, -32.23657, -32.20066, -32.19382, -32.22185, -32.27572, -32.30103,
                       -32.29243, -32.27572, -32.25964, -32.24413, -32.24413, -32.22915, -32.21467,
                       -32.20066, -32.18709])
    lzfunc = interp1d(tvec, lz_ni, kind='cubic', bounds_error=False, fill_value=(lz_ni[0], lz_ni[-1]))
    lz_out = np.power(10.0, lzfunc(np.log10(te)))
    return lz_out


def calc_tungsten_cooling_factor(te):
    tvec = np.linspace(0.0, 5.0, 51)
    lz_w = np.array([ -40.00000, -40.00000, -40.00000, -40.00000, -40.00000, -40.00000, -40.00000,
                      -40.00000, -40.00000, -40.00000, -40.00000, -40.00000, -40.00000, -40.00000,
                      -39.69897, -39.00000, -37.00000, -35.00000, -33.00000, -32.30103, -31.52288,
                      -30.92082, -30.45593, -30.34679, -30.37675, -30.38722, -30.36653, -30.33255,
                      -30.31426, -30.32331, -30.34679, -30.36653, -30.37675, -30.38195, -30.36653,
                      -30.32790, -30.44370, -30.69897, -31.00000, -31.15490, -31.22185, -31.27572,
                      -31.30103, -31.30980, -31.29243, -31.27572, -31.25964, -31.24413, -31.22915,
                      -31.21467, -31.20066])
    lzfunc = interp1d(tvec, lz_w, kind='cubic', bounds_error=False, fill_value=(lz_w[0], lz_w[-1]))
    lz_out = np.power(10.0, lzfunc(np.log10(te)))
    return lz_out


def requires_z3(zeff, prad):  # Is this necessary?
    zval = np.mean(zeff)
    result = pval > (2.06e6 * (zval - 1.0))
    return result


def calc_radiated_power_with_carbon_nickel(k, ne, te, zeff, vol, verbose=0):
    z1 = calc_carbon_charge(te)
    z2 = calc_nickel_charge(te)
    lz1 = calc_carbon_cooling_factor(te)
    lz2 = calc_nickel_cooling_factor(te)
    qz1 = lz1 * ne * ne * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0))
    qz2 = lz2 * ne * ne * k[0] * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0))
    pz1 = np.trapz(qz1, vol)
    pz2 = np.trapz(qz2, vol)
    if verbose > 1:
        print(pz1)
        print(pz2)
    psol = pz1 + pz2
    if verbose > 0:
        print(psol)
    return psol


def calc_radiated_power_with_carbon_nickel_tungsten(k, ne, te, zeff, vol, verbose=0):
    z1 = calc_carbon_charge(te)
    z2 = calc_nickel_charge(te)
    z3 = calc_tungsten_charge(te)
    lz1 = calc_carbon_cooling_factor(te)
    lz2 = calc_nickel_cooling_factor(te)
    lz3 = calc_tungsten_cooling_factor(te)
    qz1 = lz1 * ne * ne * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0) + k[0] * k[1] * z3 * (z3 - 1.0))
    qz2 = lz2 * ne * ne * k[0] * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0) + k[0] * k[1] * z3 * (z3 - 1.0))
    qz3 = lz3 * ne * ne * k[0] * k[1] * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0) + k[0] * k[1] * z3 * (z3 - 1.0))
    pz1 = np.trapz(qz1, vol)
    pz2 = np.trapz(qz2, vol)
    pz3 = np.trapz(qz3, vol)
    if verbose > 1:
        print(pz1)
        print(pz2)
        print(pz3)
    psol = pz1 + pz2 + pz3
    if verbose > 0:
        print(psol)
    return psol


def calc_radiated_power_with_beryllium_nickel(k, ne, te, zeff, vol, verbose=0):
    z1 = calc_beryllium_charge(te)
    z2 = calc_nickel_charge(te)
    lz1 = calc_beryllium_cooling_factor(te)
    lz2 = calc_nickel_cooling_factor(te)
    qz1 = lz1 * ne * ne * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0))
    qz2 = lz2 * ne * ne * k[0] * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0))
    pz1 = np.trapz(qz1, vol)
    pz2 = np.trapz(qz2, vol)
    if verbose > 1:
        print(pz1)
        print(pz2)
    psol = pz1 + pz2
    if verbose > 0:
        print(psol)
    return psol


def calc_radiated_power_with_beryllium_nickel_tungsten(k, ne, te, zeff, vol, verbose=0):
    z1 = calc_beryllium_charge(te)
    z2 = calc_nickel_charge(te)
    z3 = calc_tungsten_charge(te)
    lz1 = calc_beryllium_cooling_factor(te)
    lz2 = calc_nickel_cooling_factor(te)
    lz3 = calc_tungsten_cooling_factor(te)
    qz1 = lz1 * ne * ne * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0) + k[0] * k[1] * z3 * (z3 - 1.0))
    qz2 = lz2 * ne * ne * k[0] * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0) + k[0] * k[1] * z3 * (z3 - 1.0))
    qz3 = lz3 * ne * ne * k[0] * k[1] * (zeff - 1.0) / (z1 * (z1 - 1.0) + k[0] * z2 * (z2 - 1.0) + k[0] * k[1] * z3 * (z3 - 1.0))
    pz1 = np.trapz(qz1, vol)
    pz2 = np.trapz(qz2, vol)
    pz3 = np.trapz(qz3, vol)
    if verbose > 1:
        print(pz1)
        print(pz2)
        print(pz3)
    psol = pz1 + pz2 + pz3
    if verbose > 0:
        print(psol)
    return psol


def calc_radiated_power(k, ne, te, zeff, vol, style=0, verbose=0):
    psol = np.NaN
    kk = None
    if isinstance(k, (list, tuple, np.ndarray)):
        kk = np.array(k).flatten()
    if kk is not None:
        if style == 0 and len(kk) == 1:
            psol = calc_radiated_power_with_carbon_nickel(kk, ne, te, zeff, vol, verbose=verbose)
        elif style == 1 and len(kk) == 2:
            psol = calc_radiated_power_with_carbon_nickel_tungsten(kk, ne, te, zeff, vol, verbose=verbose)
        elif style == 2 and len(kk) == 1:
            psol = calc_radiated_power_with_beryllium_nickel(kk, ne, te, zeff, vol, verbose=verbose)
        elif style == 3 and len(kk) == 2:
            psol = calc_radiated_power_with_beryllium_nickel_tungsten(kk, ne, te, zeff, vol, verbose=verbose)
    return psol


def calc_origin_distance(k, style=0):
    metric = np.NaN
    if isinstance(k, (list, tuple, np.ndarray)):
        kk = np.array(k).flatten()
        if len(kk) > 1:
            kmult = np.zeros(k.shape)
            kmult[0] = 10.0
            kmult[1] = 100.0
            dist = np.sqrt(np.sum(np.power(kk * kmult, 2.0)))
            metric = 1.0e3 * dist
        elif len(kk) > 0:
            metric = 0.0
    return metric


def calc_bulk_impurity_metric(k, te, zeff, target, style=0):
    metric = np.NaN
    if isinstance(k, (list, tuple, np.ndarray)):
        kk = np.array(k).flatten()
        if style == 0:
            #z1 = calc_carbon_charge(te)
            #z2 = calc_nickel_charge(te)
            #frac = (zeff - 1.0) / (z1 * (z1 - 1.0) + kk[0] * z2 * (z2 - 1.0))
            metric = 0.0
        elif style == 1 and len(kk) >= 2:
            z1 = calc_carbon_charge(te)
            z2 = calc_nickel_charge(te)
            z3 = calc_tungsten_charge(te)
            frac = (zeff - 1.0) / (z1 * (z1 - 1.0) + kk[0] * z2 * (z2 - 1.0) + kk[0] * kk[1] * z3 * (z3 - 1.0))
            metric = 1.0e6 * np.abs(frac.flatten()[0] - target)
        elif style == 2:
            #z1 = calc_beryllium_charge(te)
            #z2 = calc_nickel_charge(te)
            #frac = (zeff - 1.0) / (z1 * (z1 - 1.0) + kk[0] * z2 * (z2 - 1.0))
            metric = 0.0
        elif style == 3 and len(kk) >= 2:
            z1 = calc_beryllium_charge(te)
            z2 = calc_nickel_charge(te)
            z3 = calc_tungsten_charge(te)
            frac = (zeff - 1.0) / (z1 * (z1 - 1.0) + kk[0] * z2 * (z2 - 1.0) + kk[0] * kk[1] * z3 * (z3 - 1.0))
            metric = 1.0e6 * np.abs(frac.flatten()[0] - target)
    return metric


def optimize_ratios(ne, te, vol, zeff, prad, style=0, verbose=0):
    res = None
    target = 0.01 if style in [2, 3] else 0.02
    #loss = lambda x: np.abs(calc_radiated_power(x, ne, te, zeff, vol, style=style) - prad) + calc_origin_distance(x, style=style)
    loss = lambda x: np.abs(calc_radiated_power(x, ne, te, zeff, vol, style=style) - prad) + calc_bulk_impurity_metric(x, te, zeff, target, style=style)
    x0 = np.array([0.01])
    if style in [1, 3]:
        x0 = np.array([0.1, 0.05])
    xtol = 1.0e-6 if np.mean(zeff) > 1.1 else 1.0e-4
    ftol = 1.0e-3 if np.mean(zeff) > 1.1 else 1.0e-2
    res = minimize(loss, x0, method='nelder-mead', options={'maxiter': 10000, 'xatol': xtol, 'fatol': ftol})
    out = res.x
    if verbose > 0:
        temp = calc_radiated_power(out, ne, te, zeff, vol, style=style, verbose=verbose)
        print(temp - prad)
    return out


def calc_density_fractions_with_2_impurities(k1, z1, z2, zeff):
    f1 = (zeff - 1.0) / (z1 * (z1 - 1.0) + k1 * z2 * (z2 - 1.0))
    f2 = k1 * f1
    f0 = 1.0 - f1 * z1 - f2 * z2
    return f0, f1, f2


def calc_density_fractions_with_3_impurities(k1, k2, z1, z2, z3, zeff):
    f1 = (zeff - 1.0) / (z1 * (z1 - 1.0) + k1 * z2 * (z2 - 1.0) + k1 * k2 * z3 * (z3 - 1.0))
    f2 = k1 * f1
    f3 = k2 * f2
    f0 = 1.0 - f1 * z1 - f2 * z2 - f3 * z3
    return f0, f1, f2, f3


def calc_density_fractions_with_carbon_nickel(k, te, zeff, itag="D"):
    z0 = np.ones(te.shape)
    z1 = calc_carbon_charge(te)
    z2 = calc_nickel_charge(te)
    f0, f1, f2 = calc_density_fractions_with_2_impurities(k[0], z1, z2, zeff)
    out = [
        (itag, f0, z0),
        ("C", f1, z1),
        ("Ni", f2, z2)
    ]
    return out


def calc_density_fractions_with_carbon_nickel_tungsten(k, te, zeff, itag="D"):
    z0 = np.ones(te.shape)
    z1 = calc_carbon_charge(te)
    z2 = calc_nickel_charge(te)
    z3 = calc_tungsten_charge(te)
    f0, f1, f2, f3 = calc_density_fractions_with_3_impurities(k[0], k[1], z1, z2, z3, zeff)
    out = [
        (itag, f0, z0),
        ("C", f1, z1),
        ("Ni", f2, z2),
        ("W", f3, z3)
    ]
    return out


def calc_density_fractions_with_beryllium_nickel(k, te, zeff, itag="D"):
    z0 = np.ones(te.shape)
    z1 = calc_beryllium_charge(te)
    z2 = calc_nickel_charge(te)
    f0, f1, f2 = calc_density_fractions_with_2_impurities(k[0], z1, z2, zeff)
    out = [
        (itag, f0, z0),
        ("Be", f1, z1),
        ("Ni", f2, z2)
    ]
    return out


def calc_density_fractions_with_beryllium_nickel_tungsten(k, te, zeff, itag="D"):
    z0 = np.ones(te.shape)
    z1 = calc_beryllium_charge(te)
    z2 = calc_nickel_charge(te)
    z3 = calc_tungsten_charge(te)
    f0, f1, f2, f3 = calc_density_fractions_with_3_impurities(k[0], k[1], z1, z2, z3, zeff)
    out = [
        (itag, f0, z0),
        ("Be", f1, z1),
        ("Ni", f2, z2),
        ("W", f3, z3)
    ]
    return out


def calc_density_fractions(k, te, zeff, style=0, itag="D", verbose=0):
    out = []
    if isinstance(k, (list, tuple, np.ndarray)):
        kk = np.array(k).flatten()
        if style == 0 and len(kk) == 1:
            out = calc_density_fractions_with_carbon_nickel(kk, te, zeff, itag=itag)
        elif style == 1 and len(kk) == 2:
            out = calc_density_fractions_with_carbon_nickel_tungsten(kk, te, zeff, itag=itag)
        elif style == 2 and len(kk) == 1:
            out = calc_density_fractions_with_beryllium_nickel(kk, te, zeff, itag=itag)
        elif style == 3 and len(kk) == 2:
            out = calc_density_fractions_with_beryllium_nickel_tungsten(kk, te, zeff, itag=itag)
    return out


def optimize_density_fractions(ne, te, vol, zeff, prad, style=0, itag="D", verbose=0):
    rel = optimize_ratios(ne, te, vol, zeff, prad, style=style, verbose=verbose)
    return calc_density_fractions(rel, te, zeff, style=style, itag=itag, verbose=verbose)

