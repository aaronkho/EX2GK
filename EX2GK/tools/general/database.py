import os
import getpass
import urllib.parse
import json
import copy
import warnings

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, OperationFailure
import numpy as np
from IPython import embed

from EX2GK.tools.general.classes import EX2GKTimeWindow, EX2GKShot, EX2GKShotCollection

def connect_to_database(username=None, userpass=None, server_ip=None):
    user = os.getenv('MONGODB_USER') if not isinstance(username,str) else username
    password = os.getenv('MONGODB_PASSWORD') if not isinstance(userpass,str) else userpass
    server_ip = os.getenv('MONGODB_IP') if not isinstance(server_ip,str) else server_ip
    if user is None:
        user = urllib.parse.quote_plus(input("1D profile DB user:"))
    if password is None:
        password = urllib.parse.quote_plus(getpass.getpass("1D profile DB password:"))
    if server_ip is None:
        server_ip = '134.209.193.21'
    client = MongoClient(server_ip, 27017, username=user, password=password, serverSelectionTimeoutMS=2000, authSource='onedpdb')
    try:
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        print("Could not connect to database at '{!s}'.".format(server_ip))
    except OperationFailure:
        print("Failed to authenticate to database, check user name and password")
        exit()

    db = client['onedpdb']
    return db

def generate_uid(dev, shot, t1, t2):
    uid = ''
    uid += dev
    uid += '_' + str(shot)
    uid += '_' + str(int(np.floor(t1 * 10000)))
    uid += '_' + str(int(np.floor(t2 * 10000)))
    return uid

def submit_one_to_db(db_connection, timewindow):
    flat_dict = None
    if isinstance(timewindow, EX2GKTimeWindow):
        flat_dict = timewindow.exportToDict()
    elif isinstance(timewindow, dict) and "META_SHOT" in timewindow:
        flat_dict = copy.deepcopy(timewindow)
    if flat_dict is not None:
        db_dict = {}
        for k, v in flat_dict.items():
            if k == 'UPDATED':
                db_dict[k] = v
            else:
                group, name = k.split('_', 1)
                if group not in db_dict:
                    db_dict[group] = {}
                db_dict[group][name] = v
        db_dict['_id'] = generate_uid(db_dict['META']['DEVICE'],
                                      db_dict['META']['SHOT'],
                                      db_dict['META']['T1'],
                                      db_dict['META']['T2'])
        jsons = json.dumps(db_dict, default=EX2GKTimeWindow.json_converter)
        timewindow_collection = db_connection['timewindows']
        timewindow_collection.insert_one(json.loads(jsons))
    else:
        warnings.warn('Time window given does not fit the standard form, ignoring submission',UserWarning)

def extract_shot_from_db(db_connection, dev, shot):
    timewindow_collection = db_connection['timewindows']
    query = {"META.DEVICE" : dev, "META.SHOT" : shot}
    timewindow_document_list = timewindow_collection.find(query)
    shot_object = EX2GKShot()
    for key,var in timewindow_document_list.items():
        shot_object.insertTimeWindow(convert_doc_to_class(timewindow_document))
    shot_object.sort()
    return shot_object

def extract_one_from_db(db_connection, dev, shot, t1, t2):
    timewindow_collection = db_connection['timewindows']
    shot_object = extract_shot_from_db(db_connection, dev, shot)
    timewindow_object = None
    if shot_object.shot is not None:
        ctw = shot_object.convertCanonicalTimeWindow(t1, t2)
        timewindow_object = shot_object.getByCanonicalTimeWindow(ctw[0], ctw[1])
    if timewindow_object is None:
        print("Time window not found!")
    return timewindow_object

def convert_doc_to_class(timewindow_document):
    flat_dict = {}
    for group, subdict in timewindow_document.items():
        if group == '_id':
            pass
        elif group == 'UPDATED':
            flat_dict[group] = subdict
        else:
            for name, v in subdict.items():
                flat_dict[group + '_' + name] = v
    tw_cls = EX2GKTimeWindow.importFromDict(flat_dict)
    return tw_cls
