# Script with functions to perform profile fitting on extracted JET PPF data
# Developer: Aaron Ho - 14/02/2017

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import matplotlib
import matplotlib.pyplot as plt
import collections

# Internal package imports
from EX2GK.tools.general import proctools as ptools, classes as ctools, read_EX2GK_output as xread

np_itypes = (np.int8, np.int16, np.int32, np.int64)
np_utypes = (np.uint8, np.uint16, np.uint32, np.uint64)
np_ftypes = (np.float16, np.float32, np.float64)

number_types = (float, int, np_itypes, np_utypes, np_ftypes)
array_types = (list, tuple, np.ndarray)


def plot_coord_systems(procdata, plottag=None, plotdir=None):
    """
    Plots the relations between the various radial coordinate systems and
    associated Jacobians, as calculated and interpolated by the
    calc_coords_with_gp() function.

    :arg coorddata: dict. Implementation-specific object containing unified coordinate system data.

    :kwarg plottag: str. Optional tag to append to the filename, ideally used to make the plot name unique.

    :kwarg plotdir: str. Optional path to directory where generated plots will be saved to, making the directory tree if necessary.

    :returns: none.
    """
    DAT = None
    tag = ''
    if isinstance(procdata, dict):
        DAT = procdata
    if isinstance(plottag, str) and plottag:
        tag = '_' + plottag

    if DAT is not None:

        odir = './'
        if isinstance(plotdir, str):
            odir = plotdir
        if not odir.endswith('/'):
            odir = odir + '/'
        if not os.path.exists(odir):
            os.makedirs(odir)

        snstr = "%06d" % (int(DAT["SHOT"]))
        colors = ['r', 'b', 'g', 'c', 'm', 'y']
        eq = []
        ll = []
        if "EQLIST" in DAT and DAT["EQLIST"] and len(DAT["EQLIST"]) > 1:
            for ii in np.arange(0, len(DAT["EQLIST"])):
                if DAT["EQLIST"][ii].upper() in DAT and DAT[DAT["EQLIST"][ii].upper()] is not None:
                    eq.append(DAT[DAT["EQLIST"][ii].upper()])
                    ll.append(DAT["EQLIST"][ii].upper())
        else:
            eq.append(DAT)
            label = DAT["EQLIST"][0] if "EQLIST" in DAT and DAT["EQLIST"] and DAT["EQLIST"] else 'Unspecified'
            ll.append(label)
        bcs = DAT["CJ_BASE"] if "CJ_BASE" in DAT and DAT["CJ_BASE"] is not None else "POLFLUXN"
        blabel = itemgetter(1)(ptools.define_coordinate_system(bcs))

        # Toroidal rho vs. Poloidal rho
        (xcs, xlabel) = ptools.define_coordinate_system("RHOPOLN")
        (ycs, ylabel) = ptools.define_coordinate_system("RHOTORN")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CS_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CS_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'rhotn_vs_rhopn_'+snstr+tag+'.png')
            plt.close(fig)

        # Jacobian of Toroidal rho vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("RHOTORN")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None and DAT["CS_"+bcs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CJ_"+bcs+"_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CJ_"+bcs+"_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CJ_C"+bcs+"_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(r'd '+blabel+r' / d '+ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
#            fig.savefig(odir+'dpsipndrhotn_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Toroidal magnetic flux norm vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("TORFLUXN")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:        
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CS_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CS_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'psitn_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Jacobian of Toroidal magnetic flux norm vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("TORFLUXN")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None and DAT["CS_"+bcs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CJ_"+bcs+"_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CJ_"+bcs+"_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CJ_C"+bcs+"_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(r'd '+blabel+r' / d '+ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
#            fig.savefig(odir+'dpsipndpsitn_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Toroidal magnetic flux vs. Poloidal magnetic flux
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUX")
        (ycs, ylabel) = ptools.define_coordinate_system("TORFLUX")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CS_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CS_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'psit_vs_psip_'+snstr+tag+'.png')
            plt.close(fig)

        # Jacobian of toroidal magnetic flux vs. Poloidal magnetic flux
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUX")
        (ycs, ylabel) = ptools.define_coordinate_system("TORFLUX")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None and DAT["CS_"+bcs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CJ_"+bcs+"_"+xcs] is not None and eq[ii]["CJ_"+bcs+"_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CJ_"+bcs+"_"+ycs] / (eq[ii]["CJ_"+bcs+"_"+xcs] + 1.0e-6), color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CJ_C"+bcs+"_C"+ycs] / (DAT["CJ_C"+bcs+"_C"+xcs] + 1.0e-6), color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(r'd '+xlabel+r' / d '+ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
#            fig.savefig(odir+'dpsipndpsit_vs_psip_'+snstr+tag+'.png')
            plt.close(fig)

        # Major radius vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("RMAJOR")
        ycs = "RMAJOR"
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs+"O"] is not None and DAT["CS_"+ycs+"I"] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CS_"+ycs+"O"] is not None and eq[ii]["CS_"+ycs+"I"] is not None:
                    ax.plot(np.hstack((eq[ii]["CS_"+xcs][::-1], eq[ii]["CS_"+xcs])), np.hstack((eq[ii]["CS_"+ycs+"I"][::-1], eq[ii]["CS_"+ycs+"O"])), \
                            color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(np.hstack((DAT["CS_C"+xcs][::-1], DAT["CS_C"+xcs])), np.hstack((DAT["CS_C"+ycs+"I"][::-1], DAT["CS_C"+ycs+"O"])), color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
#            fig.savefig(odir+'Rmaj_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Poloidal magnetic flux norm vs. Major radius
        (xcs, xlabel) = ptools.define_coordinate_system("RMAJOR")
        (ycs, ylabel) = ptools.define_coordinate_system("POLFLUXN")
        xcs = "RMAJOR"
        if DAT["CS_"+xcs+"O"] is not None and DAT["CS_"+xcs+"I"] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs+"I"] is not None and eq[ii]["CS_"+xcs+"O"] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(np.hstack((eq[ii]["CS_"+xcs+"I"][::-1], eq[ii]["CS_"+xcs+"O"])), np.hstack((eq[ii]["CS_"+ycs][::-1], eq[ii]["CS_"+ycs])), \
                            color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(np.hstack((DAT["CS_C"+xcs+"I"][::-1], DAT["CS_C"+xcs+"O"])), np.hstack((DAT["CS_C"+ycs][::-1], DAT["CS_C"+ycs])), color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'psipn_vs_Rmaj_'+snstr+tag+'.png')
            plt.close(fig)

        # Jacobian of Poloidal magnetic flux norm vs. Major radius
        (xcs, xlabel) = ptools.define_coordinate_system("RMAJOR")
        (ycs, ylabel) = ptools.define_coordinate_system("RMAJOR")
        xcs = "RMAJOR"
        ycs = "RMAJOR"
        if DAT["CS_"+xcs+"O"] is not None and DAT["CS_"+xcs+"I"] is not None and DAT["CS_"+bcs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs+"I"] is not None and eq[ii]["CS_"+xcs+"O"] is not None and eq[ii]["CJ_"+bcs+"_"+ycs+"I"] is not None and eq[ii]["CJ_"+bcs+"_"+ycs+"O"] is not None:
                    ax.plot(np.hstack((eq[ii]["CS_"+xcs+"I"][::-1], eq[ii]["CS_"+xcs+"O"])), np.hstack((eq[ii]["CJ_"+bcs+"_"+ycs+"I"][::-1], eq[ii]["CJ_"+bcs+"_"+ycs+"O"])), \
                            color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(np.hstack((DAT["CS_C"+xcs+"I"][::-1], DAT["CS_C"+xcs+"O"])), np.hstack((DAT["CJ_C"+bcs+"_C"+ycs+"I"][::-1], DAT["CJ_C"+bcs+"_C"+ycs+"O"])), color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(r'd '+blabel+r' / d '+ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'dpsipndRmaj_vs_Rmaj_'+snstr+tag+'.png')
            plt.close(fig)

        # Toroidal magnetic flux norm vs. Major radius
        (xcs, xlabel) = ptools.define_coordinate_system("RMAJOR")
        (ycs, ylabel) = ptools.define_coordinate_system("TORFLUXN")
        xcs = "RMAJOR"
        if DAT["CS_"+xcs+"O"] is not None and DAT["CS_"+xcs+"I"] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs+"I"] is not None and eq[ii]["CS_"+xcs+"O"] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(np.hstack((eq[ii]["CS_"+xcs+"I"][::-1], eq[ii]["CS_"+xcs+"O"])), np.hstack((eq[ii]["CS_"+bcs][::-1], eq[ii]["CS_"+bcs])), \
                            color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(np.hstack((DAT["CS_C"+xcs+"I"][::-1], DAT["CS_C"+xcs+"O"])), np.hstack((DAT["CS_C"+ycs][::-1], DAT["CS_C"+ycs])), color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'psitn_vs_Rmaj_'+snstr+tag+'.png')
            plt.close(fig)

        # Flux surface volume vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("FSVOL")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CS_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CS_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'vol_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Jacobian of flux surface volume vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("FSVOL")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CJ_"+bcs+"_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CJ_"+bcs+"_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CJ_C"+bcs+"_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(r'd '+blabel+r' / d '+ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
#            fig.savefig(odir+'dpsipndvol_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Flux tube surface area vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("FSAREA")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CS_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CS_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CS_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
            fig.savefig(odir+'area_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)

        # Jacobian of flux tube surface area vs. Poloidal magnetic flux norm
        (xcs, xlabel) = ptools.define_coordinate_system("POLFLUXN")
        (ycs, ylabel) = ptools.define_coordinate_system("FSAREA")
        if DAT["CS_"+xcs] is not None and DAT["CS_"+ycs] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for ii in np.arange(0, len(eq)):
                if eq[ii]["CS_"+xcs] is not None and eq[ii]["CJ_"+bcs+"_"+ycs] is not None:
                    ax.plot(eq[ii]["CS_"+xcs], eq[ii]["CJ_"+bcs+"_"+ycs], color=colors[ii], label=ll[ii])
            if DAT["CC_FLAG"]:
                ax.plot(DAT["CS_C"+xcs], DAT["CJ_C"+xcs+"_C"+ycs], color='k', label=r'Corr. Map')
            ax.set_xlabel(xlabel)
            ax.set_ylabel(r'd '+blabel+r' / d '+ylabel)
            ax.legend(loc='best')
            fig.tight_layout()
#            fig.savefig(odir+'dpsipndarea_vs_psipn_'+snstr+tag+'.png')
            plt.close(fig)


def plot_profile_data(datafile, shot, tw=None, plotsig=None, plotname=None, plotdir=None, coord='RHOTORN', rawflag=False, \
                      xlim=None, fontsize=None, legendsize=None, plot_format='png', plot_dpi=300, hmarkers=None, vmarkers=None, \
                      extrafile=None, extradict=None):
    """
    Generates a plot of the most pertinent fitted profiles,
    as well as the extracted energy deposition profiles,
    and saves it into a pre-defined plot folder with a
    self-generated name. The radial coordinate of the plot
    will always be square-root normalized toroidal flux.

    :arg datafile: str. Path to Python binary file containing standardized profile fits.

    :arg shot: int. Discharge identification number of data to be plotted.

    :kwarg tw: int. Time window index of data to be plotted.

    :kwarg plotsig: float. Specify the width of the plotted error bars, in units of standard deviations (sigma).

    :kwarg plotname: str. Optional input to specify the name of the generated plot.

    :kwarg plotdir: str. Optional input to specify the folder into which the generated plots are saved.

    :kwarg rawflag: bool. Flag to toggle plotting of measurement data only.

    :kwarg xlim: float. Upper bound for x-axis to be shown in plot. Not implemented yet!

    :kwarg fontsize: int. Font size to be used in the plot, in points.

    :kwarg legendsize: int. Font size to be used in the legend of the plot, in points.

    :kwarg plot_format: str. Specification of the format which generated plot is to be saved.

    :kwarg plot_dpi: int. Specification of the resolution which generated plot is to be saved.

    :kwarg hmarkers: array. List of x-axis locations on which a vertical dashed line is to be plotted, for marking purposes.

    :kwarg vmarkers: array. List of y-axis locations on which a horizontal dashed line is to be plotted, for marking purposes.

    :kwarg extrafile: str. Path to additional Python binary file containing standardized profile fits, for comparison purposes.

    :kwarg extradict: dict. Additional input for standardized profile fits to be plotted, for comparison purposes.

    :returns: none.
    """
    snum = None
    tidx = 0
    if isinstance(shot, (int, float)) and int(shot) > 0:
         snum = int(shot)
    if isinstance(tw, (int, float)) and int(tw) >= 0:
         tidx = int(tw)

    fsize = int(fontsize) if isinstance(fontsize, (int, float)) and int(fontsize) > 6 else 14
    matplotlib.rc('font', family='serif')
    matplotlib.rc('font', serif='cm10')
    matplotlib.rc('font', size=fsize)
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
    lsize = int(legendsize) if isinstance(legendsize, (int, float)) and int(legendsize) > 6 else fsize - 6

    if snum is not None and isinstance(datafile, str) and os.path.isfile(datafile) and datafile.endswith('.pkl'):

        idata = ptools.unpickle_this(datafile)
        if "META_SHOT" not in idata and snum in idata and len(idata[snum]) > tidx:
            idata = idata[snum][tidx]
        elif "META_SHOT" not in idata:
            idata = None

        if idata is not None:
            sdata = ctools.EX2GKTimeWindow.importFromDict(idata)
            pdir = plotdir if isinstance(plotdir, str) else './'
            if pdir.endswith('/'):
                pdir = pdir + '/'
            if not os.path.exists(pdir):
                os.makedirs(pdir)
            pname = plotname if isinstance(plotname, str) else 'ex2gk_out'
            mm = re.match(r'^(.*)\.([a-z0-9]+)$', pname)
            if mm:
                pname = mm.group(1)
            pfmt = plot_format if plot_format in ['bmp', 'jpg', 'png', 'ps', 'eps', 'svg', 'pdf'] else 'png'
            pdpi = int(plot_dpi) if isinstance(plot_dpi, (int, float)) and int(plot_dpi) > 50 else 300

            csout = sdata["META"]["CSO"]
            ctag = sdata["META"]["CSOP"]
            qdda = sdata["META"]["EQSRC"]
            xerrflag = sdata["FLAG"].checkFlag("NIGP")
            rdlist = sdata["RD"].getPresentFields('list')
            pdlist = sdata["PD"].getPresentFields('list')
            ocode = sdata["META"]["CODETAG"]
            csplot = coord if isinstance(coord, str) and coord in sdata["CD"].allowed_names else csout
            (plotx, plotxj, plotxe) = sdata["CD"].convert(sdata["PD"]["X"][""], csout, csplot, prefix_in=ctag, prefix_out=ctag)

            nsigma = float(plotsig) if isinstance(plotsig, (int, float)) and float(plotsig) > 0.0 else 2.0
            rsigma = 1.0
            abound = float(xlim) if isinstance(xlim, (int, float)) and float(xlim) > 0.0 else 1.0
            estyles = ['+', 'x', '*', '<', '>']
            istyles = ['o', '^', 'v', 's', 'D', 'h', 'p', 'd']
            ms = 4
            colors = ['b', 'r', 'c', 'm', 'y']
            (qx, xl, xu, fxl, fxu) = ptools.define_quantity(csplot, exponent=None, derivative=None)
            dfxl = r'$\nabla_{' + fxl[1:-1] + r'}$'

            ssize = (5, 4)
            xlabel = fxl+r' ['+fxu+']' if fxu else fxl
            snstr = ("%d" % (int(snum)))
            rftag = 'raw_' if rawflag else ''
            xomin = np.nanmin(plotx)
            xomax = np.nanmax(plotx)
            xticks = np.linspace(xomin, xomax, 5)
            if isinstance(xlim, (list, tuple)) and len(xlim) >= 2:
                xomin = float(xlim[0])
                xomax = float(xlim[1])
            pfilt = np.all([plotx >= xomin, plotx <= xomax], axis=0)

            hlines = None
            vlines = None
            if isinstance(hmarkers, number_types):
                hlines = np.array([float(hmarkers)])
            elif isinstance(hmarkers, array_types):
                hlines = np.array(hmarkers)
            if isinstance(vmarkers, number_types):
                vlines = np.array([float(vmarkers)])
            elif isinstance(vmarkers, array_types):
                vlines = np.array(vmarkers)

            edata = None
            edlist = []
            if isinstance(extrafile, str) and os.path.isfile(extrafile) and extrafile.endswith('.txt'):
                etemp = xread.read_data_file(extrafile)
                for key in sdata["META"]:
                    if key not in ["CSO", "CSOP"]:
                        etemp["META_"+key] = copy.deepcopy(sdata["META"][key])
                edata = ctools.EX2GKTimeWindow.importFromDict(etemp)
                edlist = edata["PD"].getPresentFields('list')

            ddata = collections.OrderedDict()
            if isinstance(extradict, dict) and "META_SHOT" not in extradict:
                for key in extradict:
                    ddata[key] = copy.deepcopy(extradict[key])

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 19
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("NE", exponent=exp, derivative=None)
            ylabel = r'$n$ ['+fyu+']' if fyu else r'$n$'
            if "NE" in pdlist:
                sc = np.power(10.0, -exp) if isinstance(exp, int) and exp != 0 else 1.0
                bndval = float(sdata["PD"]["NE"][""][-1] * sc) if np.isfinite(sdata["PD"]["NE"][""][-1]) else float(sdata["PD"]["NE"][""][-2] * sc)
                yle = sdata["PD"]["NE"][""] - nsigma * sdata["PD"]["NE"]["EB"]
                yue = sdata["PD"]["NE"][""] + nsigma * sdata["PD"]["NE"]["EB"]
#                if "NI1" in pdlist:
#                    yli = sdata["PD"]["NI1"][""] - nsigma * sdata["PD"]["NI1"]["EB"]
#                    yui = sdata["PD"]["NI1"][""] + nsigma * sdata["PD"]["NI1"]["EB"]
                flegend = False
                if not rawflag:
                    ellabel = fyl+r' Fit'
                    ax.plot(plotx[pfilt], sdata["PD"]["NE"][""][pfilt]*sc, color='g', label=ellabel)
                    if pfmt == 'eps':
                        ax.plot(plotx[pfilt], yle[pfilt]*sc, color='g', ls='-.')
                        ax.plot(plotx[pfilt], yue[pfilt]*sc, color='g', ls='-.')
                    else:
                        ax.fill_between(plotx[pfilt], yle[pfilt]*sc, yue[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                    if "NE" in edlist and csplot == "RHOTORN":
                        eellabel = fyl+r' Sim'
                        ele = edata["PD"]["NE"][""] - nsigma * edata["PD"]["NE"]["EB"]
                        eue = edata["PD"]["NE"][""] + nsigma * edata["PD"]["NE"]["EB"]
                        efilt = np.all([edata["PD"]["NE"]["X"] >= xomin, edata["PD"]["NE"]["X"] <= xomax], axis=0)
                        ax.plot(edata["PD"]["NE"]["X"][efilt], edata["PD"]["NE"][""][efilt]*sc, color='r', label=eellabel)
                        if pfmt == 'eps':
                            ax.plot(edata["PD"]["NE"]["X"][efilt], ele[efilt]*sc, color='r', ls='-.')
                            ax.plot(edata["PD"]["NE"]["X"][efilt], eue[efilt]*sc, color='r', ls='-.')
                        else:
                            ax.fill_between(edata["PD"]["NE"]["X"][efilt], ele[efilt]*sc, eue[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                    ci = 0
                    for key, value in ddata.items():
                        if "NE" in value and csplot == "RHOTORN":
                            dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
                            ax.plot(value["XRHO"][-1, :][dfilt], value["NE"][-1, :][dfilt]*sc, color=colors[ci], label=key)
                        ci = ci + 1
#                    if "NI1" in pdlist:
#                        flabel = r'Fit' if "NI1" in rdlist else r'Est.'
#                        ioncolor = 'g'
#                        ionlabel = r'$n_i$ '+flabel
#                        ax.plot(plotx[pfilt], sdata["PD"]["NI1"][""][pfilt]*sc, color=ioncolor, ls='--', label=ionlabel)
#                        if pfmt == 'eps':
#                            ax.plot(plotx[pfilt], yli[pfilt]*sc, color=ioncolor, ls='-.')
#                            ax.plot(plotx[pfilt], yui[pfilt]*sc, color=ioncolor, ls='-.')
#                        else:
#                            ax.fill_between(plotx[pfilt], yli[pfilt]*sc, yui[pfilt]*sc, facecolor=ioncolor, edgecolor='None', alpha=0.2)
#                        if "NI1" in edlist and csplot == "RHOTORN":
#                            eionlabel = r'$n_i$ Sim'
#                            eli = edata["PD"]["NI1"][""] - nsigma * edata["PD"]["NI1"]["EB"]
#                            eui = edata["PD"]["NI1"][""] + nsigma * edata["PD"]["NI1"]["EB"]
#                            efilt = np.all([edata["PD"]["NI1"]["X"] >= xomin, edata["PD"]["NI1"]["X"] <= xomax], axis=0)
#                            ax.plot(edata["PD"]["NI1"]["X"][efilt], edata["PD"]["NI1"][""][efilt]*sc, color='r', ls='--', label=eionlabel)
#                            if pfmt == 'eps':
#                                ax.plot(edata["PD"]["NI1"]["X"][efilt], eli[efilt]*sc, color='r', ls='-.')
#                                ax.plot(edata["PD"]["NI1"]["X"][efilt], eui[efilt]*sc, color='r', ls='-.')
#                            else:
#                                ax.fill_between(edata["PD"]["NI1"]["X"][efilt], eli[efilt]*sc, eui[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
#                        ci = 0
#                        for key, value in ddata.items():
#                            if "NI" in value and csplot == "RHOTORN":
#                                dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
#                                ax.plot(value["XRHO"][-1, :][dfilt], value["NI"][-1, :][dfilt]*sc, color=colors[ci], label=key)
#                            ci = ci + 1
                    flegend = True
                if edata is None and "NE" in rdlist:
                    csraw = sdata["RD"]["NE"].coordinate
                    rtag = sdata["RD"]["NE"].coord_prefix
                    jj = 0
                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["NE"]["X"], csraw, csplot, rtag, ctag)
                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                    yraw = sdata["RD"]["NE"][""]
                    xebraw = rsigma * np.sqrt(np.power(sdata["RD"]["NE"]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                    yebraw = rsigma * sdata["RD"]["NE"]["EB"]
                    for ii in np.arange(0, len(sdata["RD"]["NE"]["SRC"])):
                        filt = np.all([rfilt, sdata["RD"]["NE"]["MAP"] == ii], axis=0)
                        if np.any(filt) and not re.search(r'BC$', sdata["RD"]["NE"]["SRC"][ii], flags=re.IGNORECASE):
                            diaglabel = fyl+r' Exp. ('+sdata["RD"]["NE"]["SRC"][ii]+r')' if rawflag else None
                            ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=estyles[jj], markersize=ms, label=diaglabel)
                            jj = jj + 1
                            flegend = True
#                if edata is None and "NI1" in rdlist:
#                    csraw = sdata["RD"]["NI1"].coordinate
#                    rtag = sdata["RD"]["NI1"].coord_prefix
#                    jj = 0
#                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["NI1"]["X"], csraw, csplot, rtag, ctag)
#                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
#                    yraw = sdata["RD"]["NI1"][""]
#                    xebraw = rsigma * np.sqrt(np.power(sdata["RD"]["NI1"]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
#                    yebraw = rsigma * sdata["RD"]["NI1"]["EB"]
#                    for ii in np.arange(0, len(sdata["RD"]["NI1"]["SRC"])):
#                        filt = np.all([rfilt, sdata["RD"]["NI1"]["MAP"] == ii], axis=0)
#                        if np.any(filt) and not re.search(r'BC$', sdata["RD"]["NI1"]["SRC"][ii], flags=re.IGNORECASE):
#                            diaglabel = r'$n_i$ Exp. ('+sdata["RD"]["NI1"]["SRC"][ii]+r')' if rawflag else None
#                            ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=estyles[jj], markersize=ms, label=diaglabel)
#                            jj = jj + 1
#                            flegend = True
                (ymin, ymax) = ax.get_ylim()
#                ymax = 1.05 * np.nanmax([np.nanmax(yue[pfilt]), np.nanmax(yui[pfilt])], axis=0) * sc
                ymax = 1.05 * np.nanmax(yue[pfilt]) * sc
                ymin = -0.05 * ymax
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                else:
                    ax.plot([xomin, xomax], [bndval, bndval], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                if flegend:
                    ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'n_' + pname + '_' + rftag + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 3
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("TE", exponent=exp, derivative=None)
            ylabel = r'$T$ ['+fyu+']' if fyu else r'$T$'
            if "TE" in pdlist and "TI1" in pdlist:
                sc = np.power(10.0, -exp) if isinstance(exp, int) and exp != 0 else 1.0
                bndval = float(sdata["PD"]["TE"][""][-1] * sc) if np.isfinite(sdata["PD"]["TE"][""][-1]) else float(sdata["PD"]["TE"][""][-2] * sc)
                yle = sdata["PD"]["TE"][""] - nsigma * sdata["PD"]["TE"]["EB"]
                yue = sdata["PD"]["TE"][""] + nsigma * sdata["PD"]["TE"]["EB"]
                yli = sdata["PD"]["TI1"][""] - nsigma * sdata["PD"]["TI1"]["EB"]
                yui = sdata["PD"]["TI1"][""] + nsigma * sdata["PD"]["TI1"]["EB"]
                flegend = False
                if not rawflag:
                    ellabel = fyl+r' Fit'
                    ax.plot(plotx[pfilt], sdata["PD"]["TE"][""][pfilt]*sc, color='g', label=ellabel)
                    if pfmt == 'eps':
                        ax.plot(plotx[pfilt], yle[pfilt]*sc, color='g', ls='-.')
                        ax.plot(plotx[pfilt], yue[pfilt]*sc, color='g', ls='-.')
                    else:
                        ax.fill_between(plotx[pfilt], yle[pfilt]*sc, yue[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                    if "TE" in edlist and csplot == "RHOTORN":
                        eellabel = fyl+r' Sim'
                        ele = edata["PD"]["TE"][""] - nsigma * edata["PD"]["TE"]["EB"]
                        eue = edata["PD"]["TE"][""] + nsigma * edata["PD"]["TE"]["EB"]
                        efilt = np.all([edata["PD"]["TE"]["X"] >= xomin, edata["PD"]["TE"]["X"] <= xomax], axis=0)
                        ax.plot(edata["PD"]["TE"]["X"][efilt], edata["PD"]["TE"][""][efilt]*sc, color='r', label=eellabel)
                        if pfmt == 'eps':
                            ax.plot(edata["PD"]["TE"]["X"][efilt], ele[efilt]*sc, color='r', ls='-.')
                            ax.plot(edata["PD"]["TE"]["X"][efilt], eue[efilt]*sc, color='r', ls='-.')
                        else:
                            ax.fill_between(edata["PD"]["TE"]["X"][efilt], ele[efilt]*sc, eue[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                    ci = 0
                    for key, value in ddata.items():
                        if "TE" in value and csplot == "RHOTORN":
                            dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
                            ax.plot(value["XRHO"][-1, :][dfilt], value["TE"][-1, :][dfilt]*sc, color=colors[ci], label=r'$T_e$ '+key)
                        ci = ci + 1
                    flabel = r'Fit' if "TI1" in rdlist else r'Est.'
                    ioncolor = 'g'
                    ionlabel = r'$T_i$ '+flabel
#                    if "TIMP" in rdlist:
#                        ionlabel = ionlabel + r' = $T_{imp}$ Fit'
                    ax.plot(plotx[pfilt], sdata["PD"]["TI1"][""][pfilt]*sc, color=ioncolor, ls='--', label=ionlabel)
                    if pfmt == 'eps':
                        ax.plot(plotx[pfilt], yli[pfilt]*sc, color=ioncolor, ls='-.')
                        ax.plot(plotx[pfilt], yui[pfilt]*sc, color=ioncolor, ls='-.')
                    else:
                        ax.fill_between(plotx[pfilt], yli[pfilt]*sc, yui[pfilt]*sc, facecolor=ioncolor, edgecolor='None', alpha=0.2)
                    if "TI1" in edlist and csplot == "RHOTORN":
                        eionlabel = r'$T_i$ Sim'
                        eli = edata["PD"]["TI1"][""] - nsigma * edata["PD"]["TI1"]["EB"]
                        eui = edata["PD"]["TI1"][""] + nsigma * edata["PD"]["TI1"]["EB"]
                        efilt = np.all([edata["PD"]["TI1"]["X"] >= xomin, edata["PD"]["TI1"]["X"] <= xomax], axis=0)
                        ax.plot(edata["PD"]["TI1"]["X"][efilt], edata["PD"]["TI1"][""][efilt]*sc, color='r', ls='--', label=eionlabel)
                        if pfmt == 'eps':
                            ax.plot(edata["PD"]["TI1"]["X"][efilt], eli[efilt]*sc, color='r', ls='-.')
                            ax.plot(edata["PD"]["TI1"]["X"][efilt], eui[efilt]*sc, color='r', ls='-.')
                        else:
                            ax.fill_between(edata["PD"]["TI1"]["X"][efilt], eli[efilt]*sc, eui[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                    ci = 0
                    for key, value in ddata.items():
                        if "TI" in value and csplot == "RHOTORN":
                            dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
                            ax.plot(value["XRHO"][-1, :][dfilt], value["TI"][-1, :][dfilt]*sc, color=colors[ci], ls='--', label=r'$T_i$ '+key)
                        ci = ci + 1
                    flegend = True
                if edata is None and "TE" in rdlist:
                    csraw = sdata["RD"]["TE"].coordinate
                    rtag = sdata["RD"]["TE"].coord_prefix
                    jj = 0
                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["TE"]["X"], csraw, csplot, rtag, ctag)
                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                    yraw = sdata["RD"]["TE"][""]
                    xebraw = rsigma * np.sqrt(np.power(sdata["RD"]["TE"]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                    yebraw = rsigma * sdata["RD"]["TE"]["EB"]
                    for ii in np.arange(0, len(sdata["RD"]["TE"]["SRC"])):
                        filt = np.all([rfilt, sdata["RD"]["TE"]["MAP"] == ii], axis=0)
                        if np.any(filt) and not re.search(r'BC$', sdata["RD"]["TE"]["SRC"][ii], flags=re.IGNORECASE):
                            diaglabel = fyl+r' Exp. ('+sdata["RD"]["TE"]["SRC"][ii]+r')' if rawflag else None
                            ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=estyles[jj], markersize=ms, label=diaglabel)
                            jj = jj + 1
                            flegend = True
                if edata is None and "TI1" in rdlist:
                    csraw = sdata["RD"]["TI1"].coordinate
                    rtag = sdata["RD"]["TI1"].coord_prefix
                    jj = 0
                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["TI1"]["X"], csraw, csplot, rtag, ctag)
                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                    yraw = sdata["RD"]["TI1"][""]
                    xebraw = rsigma * np.sqrt(np.power(sdata["RD"]["TI1"]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                    yebraw = rsigma * sdata["RD"]["TI1"]["EB"]
                    for ii in np.arange(0, len(sdata["RD"]["TI1"]["SRC"])):
                        filt = np.all([rfilt, sdata["RD"]["TI1"]["MAP"] == ii], axis=0)
                        if np.any(filt) and not re.search(r'BC$', sdata["RD"]["TI1"]["SRC"][ii], flags=re.IGNORECASE):
                            diaglabel = r'$T_i$ Exp. ('+sdata["RD"]["TI1"]["SRC"][ii]+r')' if rawflag else None
                            ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=istyles[jj], markersize=ms, label=diaglabel)
                            jj = jj + 1
                            flegend = True
                elif edata is None and "TIMP" in rdlist:
                    csraw = sdata["RD"]["TIMP"].coordinate
                    rtag = sdata["RD"]["TIMP"].coord_prefix
                    jj = 0
                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["TIMP"]["X"], csraw, csplot, rtag, ctag)
                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                    yraw = sdata["RD"]["TIMP"][""]
                    xebraw = rsigma * np.sqrt(np.power(sdata["RD"]["TIMP"]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                    yebraw = rsigma * sdata["RD"]["TIMP"]["EB"]
                    for ii in np.arange(0, len(sdata["RD"]["TIMP"]["SRC"])):
                        filt = np.all([rfilt, sdata["RD"]["TIMP"]["MAP"] == ii], axis=0)
                        if np.any(filt) and not re.search(r'BC$', sdata["RD"]["TIMP"]["SRC"][ii], flags=re.IGNORECASE):
                            diaglabel = r'$T_{\text{imp}}$ Exp. ('+sdata["RD"]["TIMP"]["SRC"][ii]+r')' if rawflag else None
                            ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=istyles[jj], markersize=ms, label=diaglabel)
                            jj = jj + 1
                            flegend = True
                (ymin, ymax) = ax.get_ylim()
                ymax = 1.05 * np.nanmax([np.nanmax(yue[pfilt]), np.nanmax(yui[pfilt])], axis=0) * sc
                ymin = -0.05 * ymax
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                else:
                    ax.plot([xomin, xomax], [bndval, bndval], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                if flegend:
                    ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 't_' + pname + '_' + rftag + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 17
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("NIMP", exponent=exp, derivative=None)
            ylabel = r'$n$ ['+fyu+']'
            ymin = -1.0
            ymax = -0.5
            flegend = False
            nimpflag = False
            for nn in np.arange(0, sdata["META"]["NUMIMP"]):
                ntag = "%d" % (nn+1)
                if "NIMP"+ntag in pdlist:
                    sc = np.power(10.0, -exp) if isinstance(exp, int) and exp != 0 else 1.0
                    istag = r'imp'
                    zimp = 1.0
                    if "ZIMP"+ntag in sdata["META"] and sdata["META"]["ZIMP"+ntag] is not None:
                        (istag, aimp, zimp) = ptools.define_ion_species(int(sdata["META"]["ZIMP"+ntag]))
                    if istag is None:
                        istag = r'imp'
                    yl = sdata["PD"]["NIMP"+ntag][""] - nsigma * sdata["PD"]["NIMP"+ntag]["EB"]
                    yu = sdata["PD"]["NIMP"+ntag][""] + nsigma * sdata["PD"]["NIMP"+ntag]["EB"]
                    if not rawflag:
                        implabel = r'$n_{\text{'+istag+r'}}$ Fit'
                        ax.plot(plotx[pfilt], sdata["PD"]["NIMP"+ntag][""][pfilt]*sc, color='g', label=implabel)
                        if pfmt == 'eps':
                            ax.plot(plotx[pfilt], yl[pfilt]*sc, color='g', ls='-.')
                            ax.plot(plotx[pfilt], yu[pfilt]*sc, color='g', ls='-.')
                        else:
                            ax.fill_between(plotx[pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                        if "NIMP"+ntag in edlist and csplot == "RHOTORN":
                            eimplabel = r'$n_{\text{'+istag+r'}}$ Sim'
                            el = edata["PD"]["NIMP"+ntag][""] - nsigma * edata["PD"]["NIMP"+ntag]["EB"]
                            eu = edata["PD"]["NIMP"+ntag][""] + nsigma * edata["PD"]["NIMP"+ntag]["EB"]
                            efilt = np.all([edata["PD"]["NIMP"+ntag]["X"] >= xomin, edata["PD"]["NIMP"+ntag]["X"] <= xomax], axis=0)
                            ax.plot(edata["PD"]["NIMP"+ntag]["X"][efilt], edata["PD"]["NIMP"+ntag][""][efilt]*sc, color='r', label=eimplabel)
                            if pfmt == 'eps':
                                ax.plot(edata["PD"]["NIMP"+ntag]["X"][efilt], el[efilt]*sc, color='r', ls='-.')
                                ax.plot(edata["PD"]["NIMP"+ntag]["X"][efilt], eu[efilt]*sc, color='r', ls='-.')
                            else:
                                ax.fill_between(edata["PD"]["NIMP"+ntag]["X"][efilt], el[efilt]*sc, eu[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                        flegend = True
#                        ci = 0
#                        for key, value in ddata.items():
#                            for impidx in np.arange(0, 10):
#                                imptag = "%d" % (impidx)
#                                if "NIM"+imptag in value and "ZIA"+imptag in value and np.abs(np.nanmean(value["ZIA"+imptag][-1, :]) - zimp) < 1.0e-1 and csplot == "RHOTORN":
#                                    dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
#                                    ax.plot(value["XRHO"][-1, :][dfilt], value["NIM"+imptag][-1, :][dfilt]*sc, color=colors[ci], label=key)
#                            ci = ci + 1
                    if edata is None and "NIMP"+ntag in rdlist:
                        csraw = sdata["RD"]["NIMP"+ntag].coordinate
                        rtag = sdata["RD"]["NIMP"+ntag].coord_prefix
                        jj = 0
                        (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["NIMP"+ntag]["X"], csraw, csplot, rtag, ctag)
                        rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                        yraw = sdata["RD"]["NIMP"+ntag][""]
                        xebraw = rsigma * np.sqrt(np.power(sdata["RD"]["NIMP"+ntag]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                        yebraw = rsigma * sdata["RD"]["NIMP"+ntag]["EB"]
                        for ii in np.arange(0, len(sdata["RD"]["NIMP"+ntag]["SRC"])):
                            filt = np.all([rfilt, sdata["RD"]["NIMP"+ntag]["MAP"] == ii], axis=0)
                            if np.any(filt) and not re.search(r'BC$', sdata["RD"]["NIMP"+ntag]["SRC"][ii], flags=re.IGNORECASE):
                                diaglabel = r'$n_{\text{'+istag+r'}}$ Exp. ('+sdata["RD"]["NIMP"+ntag]["SRC"][ii]+r')' if rawflag else None
                                ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=istyles[jj], markersize=ms, label=diaglabel)
                                jj = jj + 1
                                flegend = True
                    (ymin, ymax) = ax.get_ylim()
                    ymax = 1.05 * np.nanmax(np.hstack((yu[pfilt], ymax))) * sc
                    ymin = -0.05 * ymax
                    nimpflag = True
            if ymax > 0.0:
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                flegend = False
                if flegend:
                    ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            if nimpflag:
                fig.savefig(pdir + 'nimp_' + pname + '_' + rftag + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            qtag = "Q"+ctag if "Q"+ctag in pdlist and "Q"+ctag in rdlist else "Q"
            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("Q", exponent=0, derivative=None)
            ylabel = r'$q$'
            if qtag in pdlist:
                sc = 1.0
                yl = sdata["PD"][qtag][""] - nsigma * sdata["PD"][qtag]["EB"]
                yu = sdata["PD"][qtag][""] + nsigma * sdata["PD"][qtag]["EB"]
                flegend = False
                if not rawflag:
                    qlabel = fyl+r' Fit'
                    ax.plot(plotx[pfilt], sdata["PD"][qtag][""][pfilt]*sc, color='g', label=qlabel)
                    if pfmt == 'eps':
                        ax.plot(plotx[pfilt], yl[pfilt]*sc, color='g', ls='-.')
                        ax.plot(plotx[pfilt], yu[pfilt]*sc, color='g', ls='-.')
                    else:
                        ax.fill_between(plotx[pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                    if qtag in edlist and csplot == "RHOTORN":
                        eqlabel = fyl+r' Sim'
                        el = edata["PD"][qtag][""] - nsigma * edata["PD"][qtag]["EB"]
                        eu = edata["PD"][qtag][""] + nsigma * edata["PD"][qtag]["EB"]
                        efilt = np.all([edata["PD"][qtag]["X"] >= xomin, edata["PD"][qtag]["X"] <= xomax], axis=0)
                        ax.plot(edata["PD"][qtag]["X"][efilt], edata["PD"][qtag][""][efilt]*sc, color='r', label=eqlabel)
                        if pfmt == 'eps':
                            ax.plot(edata["PD"][qtag]["X"][efilt], el[efilt]*sc, color='r', ls='-.')
                            ax.plot(edata["PD"][qtag]["X"][efilt], eu[efilt]*sc, color='r', ls='-.')
                        else:
                            ax.fill_between(edata["PD"][qtag]["X"][efilt], el[efilt]*sc, eu[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                    ci = 0
                    for key, value in ddata.items():
                        if "Q" in value and csplot == "RHOTORN":
                            dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
                            ax.plot(value["XRHO"][-1, :][dfilt], value["Q"][-1, :][dfilt]*sc, color=colors[ci], label=key)
                        ci = ci + 1
                    flegend = True
                if edata is None and qtag in rdlist:
                    csraw = sdata["RD"][qtag].coordinate
                    rtag = sdata["RD"][qtag].coord_prefix
                    jj = 0
                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"][qtag]["X"], csraw, csplot, rtag, ctag)
                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                    yraw = sdata["RD"][qtag][""]
                    xebraw = rsigma * np.sqrt(np.power(sdata["RD"][qtag]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                    yebraw = rsigma * sdata["RD"][qtag]["EB"]
                    qlabel = qdda if not ctag else None
                    ax.errorbar(xraw[rfilt], yraw[rfilt]*sc, xerr=xebraw[rfilt], yerr=yebraw[rfilt]*sc, color='k', ls='', marker='+', markersize=ms, label=qlabel)
                    flegend = True
                (ymin, ymax) = ax.get_ylim()
                ymin = 0.5
                ymax = 1.05 * np.nanmax(yu[pfilt]) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                if flegend:
                    ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'q_' + pname + '_' + rftag + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 4
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("AFTOR", exponent=exp, derivative=None)
            ylabel = r'$\Omega_{\text{tor}}$ ['+fyu+r']'
            if "AFTOR" in pdlist:
                sc = np.power(10.0, -exp)  if isinstance(exp, int) and exp != 0 else 1.0
                yl = sdata["PD"]["AFTOR"][""] - nsigma * sdata["PD"]["AFTOR"]["EB"]
                yu = sdata["PD"]["AFTOR"][""] + nsigma * sdata["PD"]["AFTOR"]["EB"]
                flegend = False
                if not rawflag:
                    aflabel = fyl+r' Fit'
                    ax.plot(plotx[pfilt], sdata["PD"]["AFTOR"][""][pfilt]*sc, color='g', label=aflabel)
                    if pfmt == 'eps':
                        ax.plot(plotx[pfilt], yl[pfilt]*sc, color='g', ls='-.')
                        ax.plot(plotx[pfilt], yu[pfilt]*sc, color='g', ls='-.')
                    else:
                        ax.fill_between(plotx[pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                    if "AFTOR" in edlist and csplot == "RHOTORN":
                        eaflabel = fyl+r' Sim'
                        el = edata["PD"]["AFTOR"][""] - nsigma * edata["PD"]["AFTOR"]["EB"]
                        eu = edata["PD"]["AFTOR"][""] + nsigma * edata["PD"]["AFTOR"]["EB"]
                        efilt = np.all([edata["PD"]["AFTOR"]["X"] >= xomin, edata["PD"]["AFTOR"]["X"] <= xomax], axis=0)
                        ax.plot(edata["PD"]["AFTOR"]["X"][efilt], edata["PD"]["AFTOR"][""][efilt]*sc, color='r', label=eaflabel)
                        if pfmt == 'eps':
                            ax.plot(edata["PD"]["AFTOR"]["X"][efilt], el[efilt]*sc, color='r', ls='-.')
                            ax.plot(edata["PD"]["AFTOR"]["X"][efilt], eu[efilt]*sc, color='r', ls='-.')
                        else:
                            ax.fill_between(edata["PD"]["AFTOR"]["X"][efilt], el[efilt]*sc, eu[efilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                    ci = 0
                    for key, value in ddata.items():
                        if "ANGF" in value and csplot == "RHOTORN":
                            dfilt = np.all([value["XRHO"][-1, :] >= xomin, value["XRHO"][-1, :] <= xomax], axis=0)
                            ax.plot(value["XRHO"][-1, :][dfilt], value["ANGF"][-1, :][dfilt]*sc, color=colors[ci], label=key)
                        ci = ci + 1
                    flegend = True
                if edata is None and "AFTOR" in rdlist:
                    csraw = sdata["RD"]["AFTOR"].coordinate
                    rtag = sdata["RD"]["AFTOR"].coord_prefix
                    jj = 0
                    (xraw, jraw, xouteb) = sdata["CD"].convert(sdata["RD"]["AFTOR"]["X"], csraw, csplot, rtag, ctag)
                    rfilt = np.all([np.isfinite(xraw), xraw >= xomin, xraw <= xomax], axis=0)
                    yraw = sdata["RD"]["AFTOR"][""]
                    xebraw = nsigma * np.sqrt(np.power(sdata["RD"]["AFTOR"]["XEB"], 2.0) + np.power(xouteb, 2.0)) if xerrflag else np.zeros(xraw.shape)
                    yebraw = nsigma * sdata["RD"]["AFTOR"]["EB"]
                    for ii in np.arange(0, len(sdata["RD"]["AFTOR"]["SRC"])):
                        filt = np.all([rfilt, sdata["RD"]["AFTOR"]["MAP"] == ii], axis=0)
                        if np.any(filt) and not re.search(r'BC$', sdata["RD"]["AFTOR"]["SRC"][ii], flags=re.IGNORECASE):
                            diaglabel = fyl+r' Exp. ('+sdata["RD"]["AFTOR"]["SRC"][ii]+r')' if rawflag else None
                            ax.errorbar(xraw[filt], yraw[filt]*sc, xerr=xebraw[filt], yerr=yebraw[filt]*sc, color='k', ls='', marker=istyles[jj], markersize=ms, label=diaglabel)
                            jj = jj + 1
                            flegend = True
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin(yl[pfilt]) * sc
                ymax = 1.05 * np.nanmax(yu[pfilt]) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                if flegend:
                    ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'af_' + pname + '_' + rftag + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)


def plot_profile_gradients(datafile, shot, tw=None, plotsig=None, plotname=None, plotdir=None, xlim=None, fontsize=None, plot_format='png', plot_dpi=300, hmarkers=None, vmarkers=None):
    """
    Generates a plot of the gradients of the most pertinent
    fitted profiles, as well as the extracted energy
    deposition profiles, and saves it into a pre-defined
    plot folder with a self-generated name. The radial
    coordinate of the plot will always be square-root
    normalized toroidal flux.

    :arg datafile: str. Path to Python binary file containing standardized profile fits.

    :arg shot: int. Discharge identification number of data to be plotted.

    :kwarg tw: int. Time window index of data to be plotted.

    :kwarg plotsig: float. Specify the width of the plotted error bars, in units of standard deviations (sigma).

    :kwarg plot_xerr: bool. Flag to toggle the inclusion of radial coordinate errors in plot (not used in calculation).

    :kwarg plotname: str. Optional input to specify the name of the generated plot.

    :kwarg plotdir: str. Optional input to specify the folder into which the generated plots are saved.

    :kwarg rawflag: bool. Flag to toggle plotting of measurement data only.

    :kwarg xlim: float. Upper bound for x-axis to be shown in plot. Not implemented yet!

    :kwarg fontsize: int. Font size to be used in the plot, in points.

    :kwarg legendsize: int. Font size to be used in the legend of the plot, in points.

    :kwarg plot_format: str. Specification of the format which generated plot is to be saved.

    :kwarg plot_dpi: int. Specification of the resolution which generated plot is to be saved.

    :kwarg hmarkers: array. List of x-axis locations on which a vertical dashed line is to be plotted, for marking purposes.

    :kwarg vmarkers: array. List of y-axis locations on which a horizontal dashed line is to be plotted, for marking purposes.

    :kwarg extrafile: str. Path to additional Python binary file containing standardized profile fits, for comparison purposes.

    :kwarg extradict: dict. Additional input for standardized profile fits to be plotted, for comparison purposes.

    :returns: none.
    """
    snum = None
    tidx = 0
    if isinstance(shot, (int, float)) and int(shot) > 0:
         snum = int(shot)
    if isinstance(tw, (int, float)) and int(tw) >= 0:
         tidx = int(tw)

    fsize = int(fontsize) if isinstance(fontsize, (int, float)) and int(fontsize) > 6 else 14
    matplotlib.rc('font', family='serif')
    matplotlib.rc('font', serif='cm10')
    matplotlib.rc('font', size=fsize)
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']
    lsize = fsize - 4

    if snum is not None and isinstance(datafile, str) and os.path.isfile(datafile) and datafile.endswith('.pkl'):

        idata = ptools.unpickle_this(datafile)
        if "META_SHOT" not in idata and snum in idata and len(idata[snum]) > tidx:
            idata = idata[snum][tidx]
        else:
            idata = None

        if idata is not None:
            sdata = ctools.EX2GKTimeWindow.importFromDict(idata)
            pdir = plotdir if isinstance(plotdir, str) else './'
            if pdir.endswith('/'):
                pdir = pdir + '/'
            if not os.path.exists(pdir):
                os.makedirs(pdir)
            pname = plotname if isinstance(plotname, str) else 'ex2gk_out'
            mm = re.match(r'^(.*)\.([a-z0-9]+)$', pname)
            if mm:
                pname = mm.group(1)
            pfmt = plot_format if plot_format in ['bmp', 'jpg', 'png', 'ps', 'eps', 'svg', 'pdf'] else 'png'
            pdpi = int(plot_dpi) if isinstance(plot_dpi, (int, float)) and int(plot_dpi) > 50 else 300

            csout = sdata["META"]["CSO"]
            ctag = sdata["META"]["CSOP"]
            qdda = sdata["META"]["EQSRC"]
            xerrflag = sdata["FLAG"].checkFlag("NIGP")
            rdlist = sdata["RD"].getPresentFields('list')
            pdlist = sdata["PD"].getPresentFields('list')
            ocode = sdata["META"]["CODETAG"]

            nsigma = float(plotsig) if isinstance(plotsig, (int, float)) and float(plotsig) > 0.0 else 2.0
            abound = float(xlim) if isinstance(xlim, (int, float)) and float(xlim) > 0.0 else 1.0
            estyles = ['+', 'x', '*', '<', '>']
            istyles = ['o', '^', 'v', 's', 'D', 'h', 'p', 'd']
            (qx, xl, xu, fxl, fxu) = ptools.define_quantity(csout, exponent=None, derivative=None)
            dfxl = r'$\nabla_{' + fxl[1:-1] + r'}$'

            bsize = (8, 6)
            ssize = (5, 4)

            bfig = plt.figure(figsize=bsize)
            xlabel = fxl+r' ['+fxu+']' if fxu else fxl
            snstr = ("%d" % (int(snum)))
            xomin = np.nanmin(sdata["PD"]["X"][""])
            xomax = np.nanmax(sdata["PD"]["X"][""])
            xticks = [0.0, 0.25, 0.5, 0.75, 1.0]
            if isinstance(xlim, (list, tuple)) and len(xlim) >= 2:
                xomin = float(xlim[0])
                xomax = float(xlim[1])
            pfilt = np.all([sdata["PD"]["X"][""] >= xomin, sdata["PD"]["X"][""] <= xomax], axis=0)

            hlines = None
            vlines = None
            if isinstance(hmarkers, number_types):
                hlines = np.array([float(hmarkers)])
            elif isinstance(hmarkers, array_types):
                hlines = np.array(hmarkers)
            if isinstance(vmarkers, number_types):
                vlines = np.array([float(vmarkers)])
            elif isinstance(vmarkers, array_types):
                vlines = np.array(vmarkers)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 19
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("NE", exponent=exp, derivative=None)
            ylabel = dfxl+r'$\, n$ ['+fyu+']' if fyu else dfxl+r'$\, n$'
            if "NE" in pdlist and "GRAD" in sdata["RD"]["NE"]:
                sc = np.power(10.0, -exp) if isinstance(exp, int) and exp != 0 else 1.0
                ellabel = dfxl+r'$\, '+fyl[1:-1]+r'$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["NE"]["GRAD"][pfilt]*sc, color='g', label=ellabel)
                yle = sdata["PD"]["NE"]["GRAD"] - nsigma * sdata["PD"]["NE"]["GRADEB"]
                yue = sdata["PD"]["NE"]["GRAD"] + nsigma * sdata["PD"]["NE"]["GRADEB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yue[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, yue[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
#                if "NI1" in pdlist and "GRAD" in sdata["RD"]["NI1"]:
#                    flabel = r'Fit' if "NI1" in rdlist else r'Est.'
#                    ionlabel = dfxl+r'$\, n_i$ '+flabel
#                    ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["NI1"]["GRAD"][pfilt]*sc, color='r', label=ionlabel)
#                    yli = sdata["PD"]["NI1"]["GRAD"] - nsigma * sdata["PD"]["NI1"]["GRADEB"]
#                    yui = sdata["PD"]["NI1"]["GRAD"] + nsigma * sdata["PD"]["NI1"]["GRADEB"]
#                    if pfmt == 'eps':
#                        ax.plot(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, color='r', ls='--')
#                        ax.plot(sdata["PD"]["X"][""][pfilt], yui[pfilt]*sc, color='r', ls='--')
#                    else:
#                        ax.fill_between(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, yui[pfilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
#                ymin = 1.05 * np.nanmin([np.nanmin(yle[pfilt]), np.nanmin(yli[pfilt])], axis=0) * sc
                ymin = 1.05 * np.nanmin(yle[pfilt]) * sc
                ymax = 1.05 * np.nanmax(yue[pfilt]) * sc
                if ymax < 0.0:
                    ymax = 0.0
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'dn_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("NE", exponent=0, derivative=None)
            ylabel = r'$R/L_n$'
            if ocode == "QLK" and "OUT"+ocode+"_ANE" in pdlist:
                sc = 1.0
                ellabel = r'$R/L_{'+fyl[1:-1]+r'}$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["OUT"+ocode+"_ANE"][""][pfilt]*sc, color='g', label=ellabel)
                yle = sdata["PD"]["OUT"+ocode+"_ANE"][""] - nsigma * sdata["PD"]["OUT"+ocode+"_ANE"]["EB"]
                yue = sdata["PD"]["OUT"+ocode+"_ANE"][""] + nsigma * sdata["PD"]["OUT"+ocode+"_ANE"]["EB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yue[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, yue[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
#                if "OUT"+ocode+"_ANI1" in pdlist:
#                    flabel = r'Fit' if "NI1" in pdlist else r'Est.'
#                    ionlabel = r'$R/L_{n_i}$ '+flabel
#                    ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["OUT"+ocode+"_ANI1"][""][pfilt]*sc, color='r', label=ionlabel)
#                    yli = sdata["PD"]["OUT"+ocode+"_ANI1"][""] - nsigma * sdata["PD"]["OUT"+ocode+"_ANI1"]["EB"]
#                    yui = sdata["PD"]["OUT"+ocode+"_ANI1"][""] + nsigma * sdata["PD"]["OUT"+ocode+"_ANI1"]["EB"]
#                    if pfmt == 'eps':
#                        ax.plot(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, color='r', ls='--')
#                        ax.plot(sdata["PD"]["X"][""][pfilt], yui[pfilt]*sc, color='r', ls='--')
#                    else:
#                        ax.fill_between(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, yui[pfilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
#                ymin = 1.05 * np.nanmin([np.nanmin(yle[pfilt]), np.nanmin(yli[pfilt])], axis=0) * sc
#                ymax = 1.05 * np.nanmax([np.nanmax(yue[pfilt]), np.nanmax(yui[pfilt])], axis=0) * sc
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin(yle[pfilt]) * sc
                ymax = 1.05 * np.nanmax(yue[pfilt]) * sc
                if ymin > 0.0:
                    ymin = 0.0
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'rln_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 3
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("TE", exponent=exp, derivative=None)
            ylabel = dfxl+r'$\, T$ ['+fyu+']' if fyu else dfxl+r'$\, T$'
            if "TE" in pdlist and "GRAD" in sdata["PD"]["TE"] and "TI1" in pdlist and "GRAD" in sdata["PD"]["TI1"]:
                sc = np.power(10.0, -exp) if isinstance(exp, int) and exp != 0 else 1.0
                ellabel = dfxl+r'$\,  '+fyl[1:-1]+r'$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["TE"]["GRAD"][pfilt]*sc, color='g', label=ellabel)
                yle = sdata["PD"]["TE"]["GRAD"] - nsigma * sdata["PD"]["TE"]["GRADEB"]
                yue = sdata["PD"]["TE"]["GRAD"] + nsigma * sdata["PD"]["TE"]["GRADEB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yue[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, yue[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                flabel = r'Fit' if "TI1" in rdlist or "TIMP" in rdlist else r'Est.'
                ionlabel = dfxl+r'$\, T_i$ '+flabel
#                if "TIMP" in rdlist:
#                    ionlabel = ionlabel + r' = '+dfxl+'$\, T_{imp}$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["TI1"]["GRAD"][pfilt]*sc, color='r', label=ionlabel)
                yli = sdata["PD"]["TI1"]["GRAD"] - nsigma * sdata["PD"]["TI1"]["GRADEB"]
                yui = sdata["PD"]["TI1"]["GRAD"] + nsigma * sdata["PD"]["TI1"]["GRADEB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, color='r', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yui[pfilt]*sc, color='r', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, yui[pfilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin([np.nanmin(yle[pfilt]), np.nanmin(yli[pfilt])], axis=0) * sc
                ymax = 1.05 * np.nanmax([np.nanmax(yue[pfilt]), np.nanmax(yui[pfilt])], axis=0) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'dt_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("TE", exponent=0, derivative=None)
            ylabel = r'$R/L_T$'
            if ocode == "QLK" and "OUT"+ocode+"_ATE" in pdlist and "OUT"+ocode+"_ATI1" in pdlist:
                sc = 1.0
                ellabel = r'$R/L_{'+fyl[1:-1]+r'}$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["OUT"+ocode+"_ATE"][""][pfilt]*sc, color='g', label=ellabel)
                yle = sdata["PD"]["OUT"+ocode+"_ATE"][""] - nsigma * sdata["PD"]["OUT"+ocode+"_ATE"]["EB"]
                yue = sdata["PD"]["OUT"+ocode+"_ATE"][""] + nsigma * sdata["PD"]["OUT"+ocode+"_ATE"]["EB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yue[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yle[pfilt]*sc, yue[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                flabel = r'Fit' if "TI1" in rdlist or "TIMP" in rdlist else r'Est.'
                ionlabel = r'$R/L_{T_i}$ '+flabel
#                if "TIMP" in rdlist:
#                    ionlabel = ionlabel + r' = $R/L_{T_{imp}}$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["OUT"+ocode+"_ATI1"][""][pfilt]*sc, color='r', label=ionlabel)
                yli = sdata["PD"]["OUT"+ocode+"_ATI1"][""] - nsigma * sdata["PD"]["OUT"+ocode+"_ATI1"]["EB"]
                yui = sdata["PD"]["OUT"+ocode+"_ATI1"][""] + nsigma * sdata["PD"]["OUT"+ocode+"_ATI1"]["EB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, color='r', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yui[pfilt]*sc, color='r', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yli[pfilt]*sc, yui[pfilt]*sc, facecolor='r', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin([np.nanmin(yle[pfilt]), np.nanmin(yli[pfilt])], axis=0) * sc
                ymax = 1.05 * np.nanmax([np.nanmax(yue[pfilt]), np.nanmax(yui[pfilt])], axis=0) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'rlt_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            qtag = "Q"+ctag if "Q"+ctag in pdlist and "Q"+ctag in rdlist else "Q"
            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            ylabel = dfxl+r'$\, q$'
            if qtag in pdlist and "GRAD" in sdata["PD"][qtag]:
                sc = 1.0
                qlabel = dfxl+r'$\, q$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"][qtag]["GRAD"][pfilt]*sc, color='g', label=qlabel)
                yl = sdata["PD"][qtag]["GRAD"] - nsigma * sdata["PD"][qtag]["EB"]
                yu = sdata["PD"][qtag]["GRAD"] + nsigma * sdata["PD"][qtag]["EB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yu[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
                ymin = -2.0
                ymax = 1.05 * np.nanmax(yu[pfilt]) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(-2.0, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'dq_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            ylabel = r'$\hat{s}$'
            if ocode == "QLK" and "OUT"+ocode+"_SMAG" in pdlist:
                sc = 1.0
                qlabel = r'$\hat{s}$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["OUT"+ocode+"_SMAG"][""][pfilt]*sc, color='g', label=qlabel)
                yl = sdata["PD"]["OUT"+ocode+"_SMAG"][""] - nsigma * sdata["PD"]["OUT"+ocode+"_SMAG"]["EB"]
                yu = sdata["PD"]["OUT"+ocode+"_SMAG"][""] + nsigma * sdata["PD"]["OUT"+ocode+"_SMAG"]["EB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yu[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin(yl[pfilt]) * sc
                ymax = 1.05 * np.nanmax(yu[pfilt]) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'smag_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            exp = 4
            (qy, yl, yu, fyl, fyu) = ptools.define_quantity("AFTOR", exponent=exp, derivative=None)
            ylabel = dfxl+r'$\, \Omega_{\text{tor}}$ ['+fyl+r']'
            if "AFTOR" in pdlist and "GRAD" in sdata["PD"]["AFTOR"] is not None:
                sc = np.power(10.0, -exp) if isinstance(exp, int) and exp != 0 else 1.0
                aflabel = dfxl+r'$\, '+fyl[1:-1]+r'$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["AFTOR"]["GRAD"][pfilt]*sc, color='g', label=aflabel)
                yl = sdata["PD"]["AFTOR"]["GRAD"] - nsigma * sdata["PD"]["AFTOR"]["GRADEB"]
                yu = sdata["PD"]["AFTOR"]["GRAD"] + nsigma * sdata["PD"]["AFTOR"]["GRADEB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yu[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin(np.hstack((yl[pfilt], 0.0))) * sc
                ymax = 1.05 * np.nanmax(np.hstack((yu[pfilt], 0.0))) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'daf_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)

            fig = plt.figure(figsize=ssize)
            ax = fig.add_subplot(111)
            ylabel = r'$R/L_u$'
            if ocode == "QLK" and "OUT"+ocode+"_AUTOR" in pdlist:
                sc = 1.0
                qlabel = r'$R/L_u$ Fit'
                ax.plot(sdata["PD"]["X"][""][pfilt], sdata["PD"]["OUT"+ocode+"_AUTOR"][""][pfilt]*sc, color='g', label=qlabel)
                yl = sdata["PD"]["OUT"+ocode+"_AUTOR"][""] - nsigma * sdata["PD"]["OUT"+ocode+"_AUTOR"]["EB"]
                yu = sdata["PD"]["OUT"+ocode+"_AUTOR"][""] + nsigma * sdata["PD"]["OUT"+ocode+"_AUTOR"]["EB"]
                if pfmt == 'eps':
                    ax.plot(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, color='g', ls='--')
                    ax.plot(sdata["PD"]["X"][""][pfilt], yu[pfilt]*sc, color='g', ls='--')
                else:
                    ax.fill_between(sdata["PD"]["X"][""][pfilt], yl[pfilt]*sc, yu[pfilt]*sc, facecolor='g', edgecolor='None', alpha=0.2)
                (ymin, ymax) = ax.get_ylim()
                ymin = 1.05 * np.nanmin(yl[pfilt]) * sc
                ymax = 1.05 * np.nanmax(yu[pfilt]) * sc
                if hlines is not None:
                    for nn in np.arange(0, hlines.size):
                        if hlines[nn] > ymin and hlines[nn] < ymax:
                            ax.plot([xomin, xomax], [hlines[nn], hlines[nn]], color='k', ls='--')
                if vlines is not None:
                    for nn in np.arange(0, vlines.size):
                        if vlines[nn] > xomin and vlines[nn] < xomax:
                            ax.plot([vlines[nn], vlines[nn]], [ymin, ymax], color='k', ls='--')
                ax.set_ylim(ymin, ymax)
                ax.legend(loc='best', prop={'size':lsize})
            ax.set_xticks(xticks)
            ax.set_xlim(xomin, xomax)
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            fig.savefig(pdir + 'rlaf_' + pname + '_' + snstr + '.' + pfmt, bbox_inches='tight', format=pfmt, dpi=pdpi)
            plt.close(fig)



def plot_individual_data(datafile, shot, yquantity, time_window=None, rawflag=False, xquantity=None, xplotsig=None, yplotsig=None, \
                         csout=None, plotname=None, plotdir=None, plot_format='png', plot_dpi=300, fancyflag=False, pdebug=False):
    """
    Generates a plot of an individual quantity plotted against
    any other given quantity. Not guaranteed to work on all
    types of data but coded to be robust enough for general
    purposes.

    :arg procdata: dict. Object containing standardized profile fits and dimensionless gyrokinetic parameters.

    :arg xquantity: str. Entry in standardized object to act as the x-dimension of the plot.

    :arg yquantity: str. Entry in standardized object to act as the y-dimension of the plot.

    :kwarg xplotsig: float. Specify the width of the plotted x-error bars, in units of standard deviations (sigma), if any.

    :kwarg yplotsig: float. Specify the width of the plotted y-error bars, in units of standard deviations (sigma), if any.

    :kwarg plotname: str. Optional input to specify the name of the generated plot.

    :kwarg plotdir: str. Optional input to specify the folder into which the generated plots are saved.

    :kwarg fancyflag: bool. Optional flag to toggle plotting using LaTeX-like fonts and symbols.

    :returns: none.
    """
    DAT = None
    sn = None
    twi = None
    xsigma = None
    ysigma = None
    if os.path.isfile(datafile):
        DAT = ptools.unpickle_this(datafile)
    else:
        raise OSError("Datafile %s not found." % (datafile))
    if not isinstance(DAT, dict):
        raise TypeError("Invalid data object passed to plot function.")
    if isinstance(shot, (int, float)):
        if int(shot) in DAT:
            sn = int(shot)
        else:
            print("Requested shot number was not found inside data object. Aborting plot function.")
    else:
        raise TypeError("Invalid shot number passed to plot function.")
    if sn is not None and isinstance(time_window, (int, float)):
        if int(time_window) < len(DAT[sn]):
            twi = [int(time_window)]
        else:
            print("Requested time window index not found inside requested shot number. Defaulting to 'all'")
    if sn is not None and twi is None:
        twi = np.arange(0, len(DAT[sn])).tolist()
    if isinstance(xplotsig, (float, int)) and float(xplotsig) > 0.0:
        xsigma = float(xplotsig)
    if isinstance(yplotsig, (float, int)) and float(yplotsig) > 0.0:
        ysigma = float(yplotsig)

    if DAT is not None and sn is not None:
        odir = plotdir if isinstance(plotdir, str) else './'
        if not odir.endswith('/'):
            odir = odir + '/'
        if not os.path.exists(odir):
            os.makedirs(odir)

        if fancyflag:
            matplotlib.rc('font', family='serif')
            matplotlib.rc('font', serif='cm10')
            matplotlib.rc('font', size=14)
            matplotlib.rc('text', usetex=True)
            matplotlib.rcParams['text.latex.preamble']=[r'\usepackage{amsmath}']

        for tw in twi:
            sdata = ctools.EX2GKTimeWindow.importFromDict(DAT[sn][tw])
            #sdata = copy.deepcopy(DAT[sn][tw])
            if pdebug:
                print(sn, tw, repr(sdata.getCanonicalTimeWindow()))

            pfmt = plot_format if plot_format in ['bmp', 'jpg', 'png', 'ps', 'eps', 'svg', 'pdf'] else 'png'
            pdpi = int(plot_dpi) if isinstance(plot_dpi, (int, float)) and int(plot_dpi) > 50 else 300

            ocp = sdata["META"]["CSOP"] if not isinstance(csout, str) else ""
            ocs = sdata["META"]["CSO"] if not isinstance(csout, str) else csout.upper()
            xxtag = None
            yytag = None
            category = "PD" if not rawflag else "RD"
            (ytag, ylabel, yunit, fylabel, fyunit) = ptools.define_quantity(yquantity)
            if ytag in sdata[category]:
                yytag = ytag
            elif yquantity in sdata[category]:
                yytag = yquantity
            else:
                print("Requested y-value not found in data object. Aborting plot function for time window index %d." % (tw))
            (xtag, xlabel, xunit, fxlabel, fxunit) = ptools.define_quantity(xquantity)
            if not rawflag:
                if xtag in sdata[category]:
                    xxtag = xtag
                elif xquantity in sdata[category]:
                    xxtag = xquantity
            if xxtag is None and yytag is not None:
                xxtag = "X"
            else:
                print("Requested x-value not found in data object. Aborting plot function for time window index %d." % (tw))

            if pdebug:
                print(repr(xquantity), repr(xxtag))
                print(repr(yquantity), repr(yytag))
                print(csout, ocp, ocs)

            if yytag is not None and yytag in sdata[category]:

                sc = 1.0     # Perhaps can be used later
                ycs = sdata[category][yytag].coordinate if rawflag else ocs
                ycp = sdata[category][yytag].coord_prefix if rawflag else ocp
                xomin = np.around(np.nanmin(sdata["CD"][ocp][ocs]["V"]), decimals=4)
                xomax = np.around(np.nanmax(sdata["CD"][ocp][ocs]["V"]), decimals=4)
                pstyles = ['+', 'x', '*', '<', '>', 'o', '^', 'v', 's', 'D', 'h', 'p', 'd']
                #dfxl = r'$\nabla_{' + fxl[1:-1] + r'}$'

                fig = plt.figure(figsize=(5, 4))
                ax = fig.add_subplot(111)
                if ylabel is None or ylabel == 'Unspecified':
                    ylabel = yytag
                    fylabel = r''+ylabel+r''
                if xlabel is None or xlabel == 'Unspecified':
                    xlabel = xxtag
                    fxlabel = r''+xlabel+r''

                if pdebug:
                    print(rawflag, ycp, ycs)
                    print(xomin, xomax)
                    print(plotname, pfmt, pdpi)

                xxvec = sdata[category][yytag][xxtag] if rawflag else sdata[category][xxtag][""]
                (xvec, jvec, xevec) = sdata["CD"].convert(xxvec, ycs, ocs, ycp, ocp)
                rfilt = np.all([np.isfinite(xvec), xvec >= xomin, xvec <= xomax], axis=0)
                yvec = sdata[category][yytag][""] * sc
                yevec = sdata[category][yytag]["EB"] * sc
                yomin = 1.2 * np.nanmin(yvec - yevec) - 0.01 * np.nanmean(yvec)
                yomax = 1.2 * np.nanmax(yvec + yevec) + 0.01 * np.nanmean(yvec)
                if (yomax - yomin) > 50.0 * (np.nanmax(yvec) - np.nanmin(yvec)):
                    yomin = 1.5 * np.nanmin(yvec) - 0.1 * np.nanmean(yvec)
                    yomax = 1.5 * np.nanmax(yvec) + 0.1 * np.nanmean(yvec)
                if np.isclose(yomin, yomax):
                    yomin = yomin - 0.1
                    yomax = yomax + 0.1
                if "XEB" in sdata[category][yytag]:
                    xevec = np.sqrt(np.power(sdata[category][yytag]["XEB"], 2.0) + np.power(xevec, 2.0))
                if "SRC" in sdata[category][yytag] and "MAP" in sdata[category][yytag]:
                    jj = 0
                    for ii in np.arange(0, len(sdata[category][yytag]["SRC"])):
                        filt = np.all([rfilt, sdata[category][yytag]["MAP"] == ii], axis=0)
                        if np.any(filt) and not re.search(r'BC$', sdata[category][yytag]["SRC"][ii], flags=re.IGNORECASE):
                            diaglabel = fylabel+r' Exp. ('+sdata[category][yytag]["SRC"][ii]+r')'
                            ax.errorbar(xvec[filt], yvec[filt], xerr=xsigma*xevec[filt], yerr=ysigma*yevec[filt], color='k', ls='', marker=estyles[jj], markersize=ms, label=diaglabel)
                            jj = jj + 1
                            flegend = True
                else:
                    ax.plot(xvec[rfilt], yvec[rfilt], color='g', ls='-')
                    yl = yvec - ysigma * yevec
                    yu = yvec + ysigma * yevec
                    if pfmt == 'eps':
                        ax.plot(xvec[rfilt], yl[rfilt], color='g', ls='--')
                        ax.plot(xvec[rfilt], yu[rfilt], color='g', ls='--')
                    else:
                        ax.fill_between(xvec[rfilt], yl[rfilt], yu[rfilt], facecolor='g', edgecolor='None', alpha=0.2)
                ax.set_xlim(xomin, xomax)
                ax.set_ylim(yomin, yomax)
                if fancyflag:
                    ax.set_ylabel(fylabel)
                    ax.set_xlabel(fxlabel)
                else:
                    ax.set_ylabel(ylabel)
                    ax.set_xlabel(xlabel)

                oname = yytag.lower() if not isinstance(plotname, str) else plotname
                oname = oname + "_%d_%d" % (sn, tw)
                fig.savefig(odir+oname+'.'+pfmt, format=pfmt, dpi=pdpi, bbox_inches='tight')
                plt.close(fig)
