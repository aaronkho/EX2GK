# Script with functions to facilitate comparison of nested dictionaries without custom objects
# Developer: Aaron Ho - 16/08/2023

# Required imports
import os
import sys
import copy
import re
import numpy as np

np_itypes = (np.int8, np.int16, np.int32, np.int64)
np_utypes = (np.uint8, np.uint16, np.uint32, np.uint64)
np_ftypes = (np.float16, np.float32, np.float64)

number_types = (float, int, np_itypes, np_utypes, np_ftypes)
array_types = (list, tuple, np.ndarray)
#string_types = (str, np.str_)


def compare(one, two, level=0, skip_equality=False, ignore_keys=None, fdebug=False):
    """ Checks nested dictionary structure for equality of various native Python data types """
    ignore = ignore_keys if isinstance(ignore_keys, (list, tuple)) else []
    result = False
    if isinstance(one, dict) and isinstance(two, dict):
        result = True
        for key in one:
            if key in ignore:
                continue
            local_result = (key in two)
            local_one = None
            local_two = None
            if local_result:
                if isinstance(one[key], (dict, list)):
                    local_result &= compare(one[key], two[key], level=level+1, skip_equality=skip_equality, fdebug=fdebug)
                elif one[key] is None:
                    check = (one[key] is two[key])
                    local_result &= check
                    if not check:
                        local_one = repr(one[key])
                        local_two = repr(two[key])
                elif isinstance(one[key], np.ndarray):
                    check = len(one[key]) == len(two[key])
                    local_result &= check
                    if check:
                        good = np.isclose(one[key], two[key], equal_nan=True)
                        local_result &= np.all(good)
                        if not np.all(good):
                            local_one = repr(one[key][~good])
                            local_two = repr(two[key][~good])
                    else:
                        local_one = "Shape " + repr(one[key].shape)
                        local_two = "Shape " + repr(two[key].shape)
                else:
                    check = (one[key] == two[key]) if not skip_equality else (one[key] == one[key])
                    local_result &= check
                    if not check:
                        local_one = repr(one[key])
                        local_two = repr(two[key])
            else:
                local_one = "Present"
                local_two = "Missing"
            if fdebug:
                pstr = ""
                tl = level if isinstance(level, int) else 0
                for ii in range(tl):
                    pstr += "  "
                pstr += "%-10s %s" % (key, local_result)
                if isinstance(local_one, str) and isinstance(local_two, str):
                    pstr += "   :   Test: %s -> Ref: %s" % (local_one, local_two)
                print(pstr)
            result &= local_result
    elif isinstance(one, list) and isinstance(two, list):
        result = True
        for ii in range(len(one)):
            local_result = (ii < len(two)) and isinstance(two[ii], type(one[ii]))
            local_one = None
            local_two = None
            if local_result:
                if isinstance(one[ii], (dict, list)):
                    local_result &= compare(one[ii], two[ii], level=level+2, skip_equality=skip_equality, fdebug=fdebug)
                elif one[ii] is None:
                    check = (one[ii] is two[ii])
                    local_result &= check
                    if not check:
                        local_one = repr(one[ii])
                        local_two = repr(two[ii])
                elif isinstance(one[ii], np.ndarray):
                    check = len(one[ii]) == len(two[ii])
                    local_result &= check
                    if check:
                        good = np.isclose(one[ii], two[ii], equal_nan=True)
                        local_result &= np.all(good)
                        if not np.all(good):
                            local_one = repr(one[ii][~good])
                            local_two = repr(two[ii][~good])
                else:
                    check = (one[ii] == two[ii]) if not skip_equality else (one[ii] == one[ii])
                    local_result &= check
                    if not check:
                        local_one = repr(one[ii])
                        local_two = repr(two[ii])
            else:
                local_one = "Present"
                local_two = "Missing"
            if fdebug:
                idxstr = "Index %d" % (ii)
                pstr = ""
                tl = level if isinstance(level, int) else 0
                for ii in range(tl):
                    pstr += "  "
                pstr += "%-10s %s" % (idxstr, local_result)
                if isinstance(local_one, str) and isinstance(local_two, str):
                    pstr += "   :   Test: %s -> Ref: %s" % (local_one, local_two)
                print(pstr)
            result &= local_result
    if fdebug and ((isinstance(level, int) and level == 0) or not isinstance(level, int)):
        pstr = "%-10s %s" % ("TOTAL", local_result)
        print(pstr)
    return result

