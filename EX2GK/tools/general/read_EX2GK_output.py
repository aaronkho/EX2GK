# Script with functions to read ASCII text output from EX2GK GUI
# Developer: Aaron Ho - 11/02/2018

# Required imports
import os.path
import re
import struct
import copy
import numpy as np

# Read data from EX2GK GUI save data file
def read_data_file(input_file):
    """
    Reads data from ASCII file output of the EX2GK GUI, and places
    it back into the standardized format for data manipulation.

    :arg input_file: str. Path to ASCII file containing data to be read.

    :returns: dict. Parseable contents from ASCII file, placed into standardized format for EX2GK.
    """
    if not isinstance(input_file, str):
        raise IOError('Received non-string input. Abort.')
    if not os.path.isfile(input_file):
        raise IOError("File '" + input_file + "' not found. Abort.")

    data = dict()
    with open(input_file, 'r') as ff:
        hblock = False
        fblock = False
        tblock = ''
        sblock = None
        for line in ff:
            sline = line.strip()
            if sline and not re.search('^#', sline):
                if re.search('START OF HEADER', sline):
                    fblock = False
                    hblock = True
                    tblock = ''
                    sblock = None
                if not hblock:
                    if fblock:
                        ilist = sline.split()
                        if re.match(r'^[0-9+\-.eE]+$', ilist[0]) and re.match(r'^[0-9+\-.eE]+$', ilist[1]) and re.match(r'^[0-9+\-.eE]+$', ilist[2]):
                            xx = np.array([float(ilist[0])])
                            yy = np.array([float(ilist[1])])
                            ye = np.array([float(ilist[2])])
                            dyy = np.array([float(ilist[3])]) if len(ilist) > 3 and re.match('^[0-9+\-.eE]+$', ilist[3]) else np.array([np.NaN])
                            dye = np.array([float(ilist[4])]) if len(ilist) > 4 and re.match('^[0-9+\-.eE]+$', ilist[4]) else np.array([np.NaN])
                            idx = 5
                            extras = np.array([])
                            while idx < len(ilist) and re.match('^[0-9+\-.eE]+$', ilist[idx]):
                                extras = np.hstack((extras, float(ilist[idx])))
                                idx = idx + 1
                            if sblock+'_'+tblock not in data:
                                data[sblock+'_'+tblock+'X'] = copy.deepcopy(xx)
                                data[sblock+'_'+tblock] = copy.deepcopy(yy)
                                data[sblock+'_'+tblock+'EB'] = copy.deepcopy(ye)
                                data[sblock+'_'+tblock+'GRAD'] = copy.deepcopy(dyy)
                                data[sblock+'_'+tblock+'GRADEB'] = copy.deepcopy(dye)
                                data[sblock+'_'+tblock+'_EXTRAS'] = copy.deepcopy(extras)
                            else:
                                data[sblock+'_'+tblock+'X'] = np.hstack((data[sblock+'_'+tblock+'X'], xx))
                                data[sblock+'_'+tblock] = np.hstack((data[sblock+'_'+tblock], yy))
                                data[sblock+'_'+tblock+'EB'] = np.hstack((data[sblock+'_'+tblock+'EB'], ye))
                                data[sblock+'_'+tblock+'GRAD'] = np.hstack((data[sblock+'_'+tblock+'GRAD'], dyy))
                                data[sblock+'_'+tblock+'GRADEB'] = np.hstack((data[sblock+'_'+tblock+'GRADEB'], dye))
                                if data[sblock+'_'+tblock+'_EXTRAS'].ndim == 1:
                                    while data[sblock+'_'+tblock+'_EXTRAS'].size > extras.size:
                                        extras = np.hstack((extras, np.NaN))
                                    while extras.size > data[sblock+'_'+tblock+'_EXTRAS'].size:
                                        data[sblock+'_'+tblock+'_EXTRAS'] = np.hstack((data[sblock+'_'+tblock+'_EXTRAS'], np.NaN))
                                else:
                                    while data[sblock+'_'+tblock+'_EXTRAS'].shape[1] > extras.size:
                                        extras = np.hstack((extras, np.NaN))
                                    while extras.size > data[sblock+'_'+tblock+'_EXTRAS'].shape[1]:
                                        avec = np.full((data[sblock+'_'+tblock+'_EXTRAS'].shape[0], 1), np.NaN)
                                        data[sblock+'_'+tblock+'_EXTRAS'] = np.hstack((data[sblock+'_'+tblock+'_EXTRAS'], avec))
                                data[sblock+'_'+tblock+'_EXTRAS'] = np.vstack((data[sblock+'_'+tblock+'_EXTRAS'], extras))
                        else:
                            fblock = False
                    if not fblock:
                        mm = re.match(r'^[A-Za-z]+\s*([A-Za-z0-9]+)\s+([A-Za-z]+)\s', sline)
                        if mm:
                            fblock = True
                            tblock = mm.group(1)
                            sblock = 'RD' if re.match(r'^Raw$', mm.group(2)) else 'PD'
                elif re.search('END OF HEADER', sline):
                    hblock = False
                else:
                    mm = re.search(r'Shot Number:\s+([0-9]+)', sline)
                    if mm:
                        data["META_SHOT"] = int(mm.group(1))
                    mm = re.search(r'Radial Coordinate:\s+([A-Za-z]+)', sline)
                    if mm:
                        data["META_CSO"] = mm.group(1)
                    mm = re.search(r'Time Window Start:\s+([0-9.eE+\-]+)\s', sline)
                    if mm:
                        data["META_T1"] = float(mm.group(1))
                    mm = re.search(r'Time Window End:\s+([0-9.eE+\-]+)\s', sline)
                    if mm:
                        data["META_T2"] = float(mm.group(1))
                    mm = re.search(r'Time:\s+([0-9.eE+\-]+)\s', sline)
                    if mm:
                        data["META_T1"] = float(mm.group(1))
                        data["META_T2"] = float(mm.group(1))
        if data and "META_CSO" not in data:
            data["META_CSO"] = "RHOTORN"

    return data


def read_multi_file(multi_file):
    """
    Reads data from ASCII file output of the EX2GK GUI, and places
    it back into the standardized format for data manipulation.

    Seems to be a deprecated function based on field names being used!

    :arg input_file: str. Path to ASCII file containing data to be read.

    :returns: dict. Parseable contents from ASCII file, placed into standardized format for EX2GK.
    """
    if not isinstance(multi_file, str):
        raise IOError('Received non-string input. Abort.')
    if not os.path.isfile(multi_file):
        raise IOError("File '" + multi_file + "' not found. Abort.")

    data = dict()
    with open(multi_file, 'r') as ff:
        hblock = False
        fblock = False
        tblock = ''
        sblock = None
        for line in ff:
            sline = line.strip()
            if sline and not re.search('^#', sline):
                if re.search('START OF HEADER', sline):
                    fblock = False
                    hblock = True
                    tblock = ''
                    sblock = None
                if not hblock:
                    if fblock:
                        ilist = sline.split()
                        if re.match(r'^[0-9+\-.eE]+$', ilist[0]) and re.match(r'^[0-9+\-.eE]+$', ilist[1]) and re.match(r'^[0-9+\-.eE]+$', ilist[2]):
                            items = np.array([float(ilist[0]), float(ilist[1]), float(ilist[2])])
                            idx = 3
                            while idx < len(ilist) and re.match('^[0-9+\-.eE]+$', ilist[idx]):
                                items = np.hstack((items, float(ilist[idx])))
                                idx = idx + 1
                            if sblock+'_'+tblock not in data:
                                data[sblock+'_'+tblock] = items
                            else:
                                if data[sblock+'_'+tblock].ndim == 1:
                                    while data[sblock+'_'+tblock].size >items.size:
                                        items = np.hstack((items, np.NaN))
                                    while items.size > data[sblock+'_'+tblock].size:
                                        data[sblock+'_'+tblock] = np.hstack((data[sblock+'_'+tblock], np.NaN))
                                else:
                                    while data[sblock+'_'+tblock].shape[1] > items.size:
                                        items = np.hstack((items, np.NaN))
                                    while items.size > data[sblock+'_'+tblock].shape[1]:
                                        avec = np.full((data[sblock+'_'+tblock].shape[0], 1), np.NaN)
                                        data[sblock+'_'+tblock] = np.hstack((data[sblock+'_'+tblock], avec))
                                data[sblock+'_'+tblock] = np.vstack((data[sblock+'_'+tblock], items))
                        else:
                            fblock = False
                    if not fblock:
                        mm = re.match(r'^[A-Za-z]+\s*([A-Za-z0-9]+)\s', sline)
                        if mm:
                            fblock = True
                            tblock = mm.group(1)
                            sblock = 'HIST'
                elif re.search('END OF HEADER', sline):
                    hblock = False
                else:
                    mm = re.search(r'Shot Number:\s+([0-9]+)', sline)
                    if mm:
                        data["META_SHOT"] = int(mm.group(1))
                    mm = re.search(r'Radial Coordinate:\s+([A-Za-z]+)', sline)
                    if mm:
                        data["META_CSO"] = mm.group(1)
                    mm = re.search(r'Time Window Start:\s+([0-9.eE+\-]+)\s', sline)
                    if mm:
                        data["META_T1"] = float(mm.group(1))
                    mm = re.search(r'Time Window End:\s+([0-9.eE+\-]+)\s', sline)
                    if mm:
                        data["META_T2"] = float(mm.group(1))
                    mm = re.search(r'Time:\s+([0-9.eE+\-]+)\s', sline)
                    if mm:
                        data["META_T1"] = float(mm.group(1))
                        data["META_T2"] = float(mm.group(1))
        if data and "META_CSO" not in data:
            data["META_CSO"] = "RHOTORN"

    return data
