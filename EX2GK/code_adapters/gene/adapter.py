# Script with functions to converted standardized fitted profiles into GENE input parameters
# Developer: Aaron Ho - 28/02/2018

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
import warnings
from scipy.interpolate import interp1d

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools, phystools as ytools

number_types = (int,float,                                # Built-in types
                np.int8,np.int16,np.int32,np.int64,       # Signed integer types
                np.uint8,np.uint16,np.uint32,np.uint64,   # Unsigned integer types
                np.float16,np.float32,np.float64          # Floating point decimal types
               )

array_types = (list,tuple,                                # Built-in types
               np.ndarray                                 # numpy array
              )

# Provides indication of which geometries have been implemented in the adapter
geotags = {'s_alpha': "R"}


def calc_GENE_reference_values(twobj,newstruct=None,fdebug=False):

    # Structure used to collect relevant GENE data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    xvec_in = twobj["PD"]["X"][""].copy()
    nscale = 1.0e19
    tscale = 1.0e3
    outdict["ZD_NSCALE"] = {"VAL": nscale, "ERR": 0.0}
    outdict["ZD_TSCALE"] = {"VAL": tscale, "ERR": 0.0}
    outdict["PD_RHO"] = {"VAL": xvec_in, "ERR": np.zeros(xvec_in.shape)}

    # Reference values
    nref = None
    pddict = twobj["PD"].getPresentFields()
    if "N" in pddict and "E" in pddict["N"]:
        zfilt = (twobj["PD"]["NE"][""] > 0.0)
        nref = twobj["PD"]["NE"][""] / nscale
        if np.any(np.invert(zfilt)):
            nref[np.invert(zfilt)] = 1.0e-3 * np.amax(nref)
    if nref is None:
        raise ValueError("Electron density profile not found in data, aborting conversion to GENE values!")

    tref = None
    pddict = twobj["PD"].getPresentFields()
    if "T" in pddict and "E" in pddict["T"]:
        zfilt = (twobj["PD"]["TE"][""] > 0.0)
        tref = twobj["PD"]["TE"][""] / tscale
        if np.any(np.invert(zfilt)):
            tref[np.invert(zfilt)] = 1.0e-3 * np.amax(tref)
    if tref is None:
        raise ValueError("Electron temperature profile not found in data, aborting conversion to GENE values!")

    cref = 3.0949691e5                            # = sqrt(1 keV / m_p) in m/s
    outdict["PD_NREF"] = {"VAL": nref, "ERR": np.zeros(nref.shape)}
    outdict["PD_TREF"] = {"VAL": tref, "ERR": np.zeros(tref.shape)}
    outdict["ZD_CREF"] = {"VAL": cref, "ERR": 0.0}

    if fdebug:
        print("gene - adapter.py: %s, %s and %s populated." % ("NREF","TREF","CREF"))

    return outdict


def print_debug_GENE_reference(datadict):

    # Copy structure containing relevant GENE data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    if "PD_RHO" in outdict:
        with open(fdir+'rho.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_RHO"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_RHO"]["VAL"][ii]))
    if "PD_NREF" in outdict:
        with open(fdir+'nref.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_NREF"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_NREF"]["VAL"][ii]))
    if "PD_TREF" in outdict:
        with open(fdir+'Tref.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_TREF"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_TREF"]["VAL"][ii]))
    if "ZD_CREF" in outdict:
        with open(fdir+'cref.dat','w') as ff:
            ff.write("%20.8e" % (outdict["ZD_CREF"]["VAL"]))


def calc_GENE_radial_coordinates(twobj,newstruct=None,geometry_tag=None,use_bias=False,use_def=None,fdebug=False):

    # Structure used to collect relevant GENE data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Standardized radial coordinate for geometry specification
    if geometry_tag not in geotags:
        raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    gcs = twobj["META"]["CSO"]
    gcp = twobj["META"]["CSOP"] if use_bias else ""
    xzero = itemgetter(0)(twobj["CD"].convert([0.0],"PSIPOLN",gcs,gcp,gcp))
    bmag = twobj["ZD"][gcp+"BMAG"][""] if gcp+"BMAG" in twobj["ZD"] else twobj["ZD"]["BMAG"][""]
    bmageb = twobj["ZD"][gcp+"BMAG"]["EB"] if gcp+"BMAG" in twobj["ZD"] else twobj["ZD"]["BMAG"]["EB"]

    outdict["ZD_BO"] = {"VAL": bmag, "ERR": bmageb}

    if fdebug:
        print("gene - adapter.py: %s geometry - %s populated." % (geometry_tag,"BO"))

    if geometry_tag == "s_alpha":
        # Primary coordinate systems
        #    - Ro and Rzero are the midplane-averaged major radii of the flux surfaces
        #    - r and rmin are the midplane-averaged minor radii of the flux surfaces
        rzero = None
        rzeroeb = 0.0
        rmin = None
        rmineb = 0.0
        ro = None
        roeb = np.zeros(xvec_in.shape)
        dro = np.ones(xvec_in.shape)
        jro = np.ones(xvec_in.shape)
        rr = None
        rreb = np.zeros(xvec_in.shape)
        drr = np.ones(xvec_in.shape)
        jrr = np.ones(xvec_in.shape)
        if "RMAJORA" in twobj["CD"][gcp]:
            # Built-in class function from EX2GK/tools/general/classes.py: computes R_major_midplane_average at z_midplane, d gcs / d R_major_midplane_average, error from bias correction
            (ro,jro,roeb) = twobj["CD"].convert(xvec_in,gcs,"RMAJORA",gcp,gcp,fdebug=fdebug)
            jfilt = (np.abs(jro) > 1.0e-6)    # Filter to prevent divide by zero errors, however unlikely they are
            dro[jfilt] = 1.0 / jro[jfilt]
            (dv,dj,de) = twobj["CD"].convert([1.0],"PSIPOLN","RMAJORA",gcp,gcp,fdebug=fdebug)
            rzero = float(dv)
            rzeroeb = float(de)
        if "RMINORA" in twobj["CD"][gcp]:
            # Built-in class function from EX2GK/tools/general/classes.py: computes r_minor_midplane_average at z_midplane, d gcs / d r_minor_midplane_average, error from bias correction
            (rr,jrr,rreb) = twobj["CD"].convert(xvec_in,gcs,"RMINORA",gcp,gcp,fdebug=fdebug)
            jfilt = (np.abs(jrr) > 1.0e-6)    # Filter to prevent divide by zero errors, however unlikely they are
            drr[jfilt] = 1.0 / jrr[jfilt]
            (dv,dj,de) = twobj["CD"].convert([1.0],"PSIPOLN","RMINORA",gcp,gcp,fdebug=fdebug)
            rmin = float(dv)
            rmineb = float(de)

        # Alternative computation of midplane-averaged coordinate systems, used only if the straightforward definitions are not present inside data
        (rmajo,rmajoj,rmajoe) = twobj["CD"].convert(xvec_in,gcs,"RMAJORO",gcp,gcp,fdebug=fdebug)
        (rmaji,rmajij,rmajie) = twobj["CD"].convert(xvec_in,gcs,"RMAJORI",gcp,gcp,fdebug=fdebug)
        if ro is None or rr is None or rzero is None or rmin is None:
            (rlcfso,rlcfsoj,rlcfsoe) = twobj["CD"].convert([1.0],"PSIPOLN","RMAJORO",gcp,gcp,fdebug=fdebug)
            #(raxiso,raxisoj,raxisoe) = twobj["CD"].convert([0.0],"PSIPOLN","RMAJORO",gcp,gcp,fdebug=fdebug)
            (rlcfsi,rlcfsij,rlcfsie) = twobj["CD"].convert([1.0],"PSIPOLN","RMAJORI",gcp,gcp,fdebug=fdebug)
            #(raxisi,raxisij,raxisie) = twobj["CD"].convert([0.0],"PSIPOLN","RMAJORI",gcp,gcp,fdebug=fdebug)
            (rzero,rzeroeb,rmin,rmineb,ro,dro,roeb,jro,rr,drr,rreb,jrr) = ytools.calc_midplane_averaged_coordinates_from_scratch(rmajo,rmaji,rlcfso,rlcfsi,rmajoj,rmajij,rmajoe,rmajie,rlcfsoe,rlcfsie)

        # These coordinate vectors are needed to implement the geometrical approximations made in rotation-related calculations
        outdict["PD_RMAJORO"] = {"VAL": rmajo, "ERR": rmajoe, "DINV": rmajoj}
        outdict["PD_RMAJORI"] = {"VAL": rmaji, "ERR": rmajie, "DINV": rmajij}

        if isinstance(use_def,str) and use_def.lower() == 'jetto':
            # JETTO has a different standard definition of R0... but why?
            if "RZERO" in twobj["ZD"]:
                rzero = float(twobj["ZD"]["RZERO"][""])
                rzeroeb = float(twobj["ZD"]["RZERO"]["EB"])

        outdict["ZD_RZERO"] = {"VAL": rzero, "ERR": rzeroeb}
        outdict["ZD_RMIN"] = {"VAL": rmin, "ERR": rmineb}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,"RZERO","RMIN"))

        outdict["PD_RO"] = {"VAL": ro, "ERR": roeb, "DVAL": dro, "DERR": np.zeros(ro.shape), "DINV": jro}
        outdict["PD_R"] = {"VAL": rr, "ERR": rreb, "DVAL": drr, "DERR": np.zeros(rr.shape), "DINV": jrr}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,"RO","R"))

        # Derived coordinate systems
        #    - trpeps is the aspect ratio, r / Ro, of the flux surfaces
        tr = rr / ro
        treb = np.sqrt(np.power(rreb,2.0) + np.power(rr * roeb / ro,2.0)) / ro
        dtr = (drr * ro - rr * dro) / np.power(ro,2.0)
        jtr = jrr * jro * np.power(ro,2.0) / (ro * jro - rr * jrr)
        outdict["PD_TRPEPS"] = {"VAL": tr, "ERR": treb, "DVAL": dtr, "DERR": np.zeros(tr.shape), "DINV": jtr}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s populated." % (geometry_tag,"TRPEPS"))

    return outdict


def print_debug_GENE_radial_coordinates(datadict):

    # Copy structure containing relevant GENE data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    if "ZD_BO" in outdict:
        with open(fdir+'Bo.dat','w') as ff:
            ff.write("%20.8f" % (outdict["ZD_BO"]["VAL"]))
    if "ZD_RZERO" in outdict:
        with open(fdir+'R0.dat','w') as ff:
            ff.write("%20.8f" % (outdict["ZD_RZERO"]["VAL"]))
    if "ZD_RMIN" in outdict:
        with open(fdir+'Rmin.dat','w') as ff:
            ff.write("%20.8f" % (outdict["ZD_RMIN"]["VAL"]))
    if "PD_RO" in outdict:
        with open(fdir+'Ro.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_RO"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_RO"]["VAL"][ii]))
    if "PD_R" in outdict:
        with open(fdir+'r.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_R"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_R"]["VAL"][ii]))
    if "PD_TRPEPS" in outdict:
        with open(fdir+'trpeps.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_TRPEPS"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_TRPEPS"]["VAL"][ii]))


def calc_QuaLiKiz_magnetic_fields(twobj,newstruct=None,fdebug=False):

    # Structure used to collect relevant QuaLiKiz data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()

    # Total magnetic field at the magnetic axis, assumed to be purely toroidal
    #    There is some confusion whether this should actually be midplane-average toroidal (or total) magnetic field, which is a vector
    bzero = None
    bzeroeb = None
    dbzero = None     # Questionable if these are needed, is B_zero a vector?
    dbzeroeb = None
    # Toroidal component of magnetic field
    btor = None
    btoreb = None
    dbtor = None
    dbtoreb = None
    # Poloidal component of magnetic field
    bpol = None
    bpoleb = None
    dbpol = None
    dbpoleb = None

    # Calculate toroidal field and B_zero (different options for definitions also provided)
    if "FB" in pddict and "TOR" in pddict["FB"] and "GRAD" in twobj["PD"]["FBTOR"]:

#        if "PD_RO" in outdict:
#            # Assumed standard definition of B_tor using midplane averaged Ro based on QuaLiKiz geometric definitions - seems not to be used
#            btor = twobj["PD"]["FBTOR"][""] / outdict["PD_RO"]["VAL"]
#            btoreb = np.sqrt(np.power(twobj["PD"]["FBTOR"]["EB"],2.0) + np.power(btor * outdict["PD_RO"]["ERR"],2.0)) / outdict["PD_RO"]["VAL"]
#            dbtor = (twobj["PD"]["FBTOR"]["GRAD"] - btor * outdict["PD_RO"]["DVAL"]) / outdict["PD_RO"]["VAL"]
#            dbtoreb = np.sqrt(np.power(twobj["PD"]["FBTOR"]["GRADEB"],2.0) + np.power(btoreb * outdict["PD_RO"]["DVAL"],2.0) + \
#                              np.power(btor * outdict["PD_RO"]["DERR"],2.0) + np.power(dbtor * outdict["PD_RO"]["ERR"],2.0)) / outdict["PD_RO"]["VAL"]

#            # Assumed B_zero = F_tor / midplane-averaged R0, giving midplane-averaged toroidal magnetic field - unsure if this is the right definition
#            bzero = twobj["PD"]["FBTOR"][""] / outdict["PD_RO"]["VAL"]
#            bzeroeb = np.sqrt(np.power(twobj["PD"]["FBTOR"]["EB"],2.0) + np.power(bzero * outdict["PD_RO"]["ERR"],2.0))  / outdict["PD_RO"]["VAL"]
#            dbzero = (twobj["PD"]["FBTOR"]["GRAD"] - bzero *outdict["PD_RO"]["DVAL"]) / outdict["PD_RO"]["VAL"]
#            dbzeroeb = np.sqrt(np.power(twobj["PD"]["FBTOR"]["GRADEB"],2.0) + np.power(bzeroeb * outdict["PD_RO"]["DVAL"],2.0) + \
#                               np.power(bzero * outdict["PD_RO"]["DERR"],2.0) + np.power(dbzero * outdict["PD_RO"]["ERR"],2.0) / outdict["PD_RO"]["VAL"]

        # QuaLiKiz implementation in JETTO uses B_tor_outer instead of midplane averaged B_tor in parallel flow velocity calculation
        if "PD_RMAJORO" in outdict:
            torsign = np.sign(np.sum(twobj["PD"]["FBTOR"][""]))    # F_B_tor should either be all positive or all negative
            btor = torsign * twobj["PD"]["FBTOR"][""] / outdict["PD_RMAJORO"]["VAL"]
            btoreb = np.sqrt(np.power(twobj["PD"]["FBTOR"]["EB"],2.0) + np.power(btor * outdict["PD_RMAJORO"]["ERR"],2.0)) / outdict["PD_RMAJORO"]["VAL"]
            dbtor = (torsign * twobj["PD"]["FBTOR"]["GRAD"] - btor / outdict["PD_RMAJORO"]["DINV"]) / outdict["PD_RMAJORO"]["VAL"]
            dbtoreb = np.sqrt(np.power(twobj["PD"]["FBTOR"]["GRADEB"],2.0) + np.power(btoreb / outdict["PD_RMAJORO"]["DINV"],2.0) + \
                              np.power(dbtor * outdict["PD_RMAJORO"]["ERR"],2.0)) / outdict["PD_RMAJORO"]["VAL"]

        # QuaLiKiz implementation in JETTO uses B_zero instead of midplane averaged B_tor in perpendicular flow velocity calculation
        if "ZD_RZERO" in outdict:
            # Assumed B_zero = F_tor @ magnetic axis / R_zero is a scalar
            bzero = float(np.abs(twobj["PD"]["FBTOR"][""][0]) / outdict["ZD_RZERO"]["VAL"])
            bzeroeb = float(np.abs(twobj["PD"]["FBTOR"]["EB"][0]) / outdict["ZD_RZERO"]["VAL"])
            dbzero = 0.0
            dbzeroeb = 0.0

    if (btor is None or bzero is None) and "B" in pddict and "TOR" in pddict["B"] and "GRAD" in twobj["PD"]["BTOR"]:
        torsign = np.sign(np.sum(twobj["PD"]["BTOR"][""]))
        btor = torsign * twobj["PD"]["BTOR"][""]
        btoreb = np.abs(twobj["PD"]["BTOR"]["EB"])
        dbtor = torsign * twobj["PD"]["BTOR"]["GRAD"]
        dbtoreb = np.abs(twobj["PD"]["BTOR"]["GRADEB"])
        bzero = float(np.abs(twobj["PD"]["BTOR"][""][0]))
        bzeroeb = float(np.abs(twobj["PD"]["BTOR"]["EB"][0]))
        dbzero = 0.0
        dbzeroeb = 0.0

    if (btor is None or bzero is None) and "ZD_BO" in outdict and "PD_RMAJORO" in outdict and "ZD_RZERO" in outdict:
        btor = outdict["ZD_RZERO"]["VAL"] * np.abs(outdict["ZD_BO"]["VAL"]) / outdict["PD_RMAJORO"]["VAL"]
        btoreb = outdict["ZD_RZERO"]["VAL"] * np.abs(outdict["ZD_BO"]["ERR"]) / outdict["PD_RMAJORO"]["VAL"]
        dbtor = -outdict["ZD_RZERO"]["VAL"] * np.abs(outdict["ZD_BO"]["VAL"]) / (outdict["PD_RMAJORO"]["DINV"] * np.power(outdict["PD_RMAJORO"]["VAL"],2.0))
        dbtoreb = np.abs(outdict["ZD_RZERO"]["VAL"] * outdict["ZD_BO"]["ERR"] / (outdict["PD_RMAJORO"]["DINV"] * np.power(outdict["PD_RMAJORO"]["VAL"],2.0)))
        bzero = float(np.abs(outdict["ZD_BO"]["VAL"]))
        bzeroeb = float(np.abs(outdict["ZD_BO"]["ERR"]))
        dbzero = 0.0
        dbzeroeb = 0.0

    # Set with zeros if no data is available
    if bzero is None:
        bzero = 0.0
        bzeroeb = 0.0
        dbzero = 0.0
        dbzeroeb = 0.0
    if btor is None:
        btor = np.zeros(xvec_in.shape)
        btoreb = np.zeros(xvec_in.shape)
        dbtor = np.zeros(xvec_in.shape)
        dbtoreb = np.zeros(xvec_in.shape)

    if "B" in pddict and "POL" in pddict["B"] and "GRAD" in twobj["PD"]["BPOL"]:
        bpol = copy.deepcopy(twobj["PD"]["BPOL"][""])
        bpoleb = copy.deepcopy(twobj["PD"]["BPOL"]["EB"])
        dbpol = copy.deepcopy(twobj["PD"]["BPOL"]["GRAD"])
        dbpoleb = copy.deepcopy(twobj["PD"]["BPOL"]["GRADEB"])

    if bpol is None and "Q" in pddict and "" in pddict["Q"] and "GRAD" in twobj["PD"]["Q"] and "PD_R" in outdict and "PD_RO" in outdict:
        bpol = outdict["PD_R"]["VAL"] * btor / (outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"][""])
        bpoleb = np.sqrt(np.power(outdict["PD_R"]["ERR"] * btor,2.0) + np.power(outdict["PD_R"]["ERR"] * btoreb,2.0) + \
                         np.power(bpol * outdict["PD_RO"]["ERR"] * twobj["PD"]["Q"][""],2.0) + \
                         np.power(bpol * outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"]["EB"],2.0)) / (outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"][""])
        dbpol = (outdict["PD_R"]["DVAL"] * btor + outdict["PD_R"]["VAL"] * dbtor - \
                 bpol * outdict["PD_RO"]["DVAL"] * twobj["PD"]["Q"][""] - \
                 bpol * outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"]["GRAD"]) / (outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"][""])
        dbpoleb = np.sqrt(np.power(outdict["PD_R"]["DERR"] * btor,2.0) + np.power(outdict["PD_R"]["DVAL"] * btoreb,2.0) + \
                          np.power(outdict["PD_R"]["ERR"] * dbtor,2.0) + np.power(outdict["PD_R"]["VAL"] * dbtoreb,2.0) + \
                          np.power(bpoleb * outdict["PD_RO"]["DVAL"] * twobj["PD"]["Q"][""],2.0) + \
                          np.power(bpol * outdict["PD_RO"]["DERR"] * twobj["PD"]["Q"][""],2.0) + \
                          np.power(bpol * outdict["PD_RO"]["DVAL"] * twobj["PD"]["Q"]["EB"],2.0) + \
                          np.power(bpoleb * outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"]["GRAD"],2.0) + \
                          np.power(bpol * outdict["PD_RO"]["ERR"] * twobj["PD"]["Q"]["GRAD"],2.0) + \
                          np.power(bpol * outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"]["GRADEB"],2.0) + \
                          np.power(dbpol * outdict["PD_RO"]["ERR"] * twobj["PD"]["Q"][""],2.0) + \
                          np.power(dbpol * outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"]["EB"],2.0)) / (outdict["PD_RO"]["VAL"] * twobj["PD"]["Q"][""])

    # Set with zeros if no data is available
    if bpol is None:
        bpol = np.zeros(xvec_in.shape)
        bpoleb = np.zeros(xvec_in.shape)
        dbpol = np.zeros(xvec_in.shape)
        dbpoleb = np.zeros(xvec_in.shape)

    # Total magnetic field on outboard side, used for normalisation
    bout = np.sqrt(np.power(btor,2.0) + np.power(bpol,2.0))
    bouteb = np.sqrt(np.power(btoreb * btor,2.0) + np.power(bpoleb * bpol,2.0)) / bout
    dbout = (btor * dbtor + bpol * dbpol) / bout
    dbouteb = np.sqrt(np.power(btoreb * dbtor,2.0) + np.power(btor * dbtoreb,2.0) + \
                      np.power(bpoleb * dbpol,2.0) + np.power(bpol * dbpoleb,2.0) + np.power(bouteb * dbout,2.0)) / bout

    # Total flux-surface-averaged magnetic field, used for normalisation
    bfsa = np.sqrt(np.power(bzero,2.0) + np.power(bpol,2.0))
    bfsaeb = np.sqrt(np.power(bzeroeb * bzero,2.0) + np.power(bpoleb * bpol,2.0)) / bfsa
    dbfsa = (bzero * dbzero + bpol * dbpol) / bfsa
    dbfsaeb = np.sqrt(np.power(bzeroeb * dbzero,2.0) + np.power(bzero * dbzeroeb,2.0) + \
                      np.power(bpoleb * dbpol,2.0) + np.power(bpol * dbpoleb,2.0) + np.power(bfsaeb * dbfsa,2.0)) / bfsa

    outdict["ZD_BZERO"] = {"VAL": bzero, "ERR": bzeroeb}
    outdict["PD_BTOR"] = {"VAL": btor, "ERR": btoreb, "DVAL": dbtor, "DERR": dbtoreb}
    outdict["PD_BPOL"] = {"VAL": bpol, "ERR": bpoleb, "DVAL": dbpol, "DERR": dbpoleb}
    outdict["PD_BTOTO"] = {"VAL": bout, "ERR": bouteb, "DVAL": dbout, "DERR": dbouteb}
    outdict["PD_BTOT"] = {"VAL": bfsa, "ERR": bfsaeb, "DVAL": dbfsa, "DERR": dbfsaeb}

    if fdebug:
        print("qualikiz - adapter.py: magnetic fields calculated.")

    return outdict


def print_debug_GENE_magnetic_fields(datadict):

    # Copy structure containing relevant GENE data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    if "ZD_BZERO" in outdict:
        with open(fdir+'Bzero.dat','w') as ff:
            ff.write("%20.8e" % (outdict["ZD_BZERO"]["VAL"]))
    if "PD_BTOR" in outdict:
        with open(fdir+'Btor.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BTOR"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_BTOR"]["VAL"][ii]))
    if "PD_BPOL" in outdict:
        with open(fdir+'Bpol.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BPOL"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_BPOL"]["VAL"][ii]))
    if "PD_BTOT" in outdict:
        with open(fdir+'Btot.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BTOT"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_BTOT"]["VAL"][ii]))
    if "PD_BTOTO" in outdict:
        with open(fdir+'Btot_outer.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BTOTO"]["VAL"].size):
                ff.write("%20.8e\n" % (outdict["PD_BTOTO"]["VAL"][ii]))


def calc_GENE_species_density(twobj,newstruct=None,species_tag=None,geometry_tag=None,fdebug=False):

    # Structure used to collect GENE relevant data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Standardized radial coordinate for geometry specification
    if geometry_tag not in geotags:
        raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")
    rtag = geotags[geometry_tag]

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()
    stag = species_tag.upper() if isinstance(species_tag,str) else None

    if stag is not None and "N" in pddict and stag in pddict["N"]:
        qtag = "N" + stag
        nscale = float(outdict["ZD_NSCALE"]["VAL"]) if "ZD_NSCALE" in outdict else 1.0e19
        if stag != "E" and "PD_NREF" in outdict:
            nscale = nscale * outdict["PD_NREF"]["VAL"]
        val = copy.deepcopy(twobj["PD"][qtag][""])
        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
        jrr = copy.deepcopy(outdict["PD_"+rtag]["DINV"]) if "PD_"+rtag in outdict else np.ones(xvec_in.shape)
        norm = float(-outdict["ZD_RZERO"]["VAL"]) if "ZD_RZERO" in outdict else -1.0
        (nn,nneb,ann,anneb) = ytools.calc_generic_normalized_gradient(val,nscale,grad,jrr,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
        outdict["PD_"+qtag] = {"VAL": nn, "ERR": nneb}
        outdict["PD_A"+qtag] = {"VAL": ann, "ERR": anneb}
        outdict["PD_O"+qtag] = {"VAL": val, "ERR": valeb, "DVAL": grad, "DERR": gradeb}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,qtag,"A"+qtag))

    return outdict


def calc_GENE_species_temperature(twobj,newstruct=None,species_tag=None,geometry_tag=None,fdebug=False):

    # Structure used to collect GENE relevant data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Standardized radial coordinate for geometry specification
    if geometry_tag not in geotags:
        raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")
    rtag = geotags[geometry_tag]

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()
    stag = species_tag.upper() if isinstance(species_tag,str) else None

    if stag is not None and "T" in pddict and stag in pddict["T"]:
        qtag = "T" + stag
        tscale = float(outdict["ZD_TSCALE"]["VAL"]) if "ZD_TSCALE" in outdict else 1.0e3
        if stag != "E" and "PD_TREF" in outdict:
            tscale = tscale * outdict["PD_TREF"]["VAL"]
        val = copy.deepcopy(twobj["PD"][qtag][""])
        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
        jrr = copy.deepcopy(outdict["PD_"+rtag]["DINV"]) if "PD_"+rtag in outdict else np.ones(xvec_in.shape)
        norm = float(-outdict["ZD_RZERO"]["VAL"]) if "ZD_RZERO" in outdict else -1.0
        (tt,tteb,att,atteb) = ytools.calc_generic_normalized_gradient(val,tscale,grad,jrr,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.1)
        outdict["PD_"+qtag] = {"VAL": tt, "ERR": tteb}
        outdict["PD_A"+qtag] = {"VAL": att, "ERR": atteb}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,qtag,"A"+qtag))

    return outdict


def calc_GENE_species_pressure(twobj,newstruct=None,species_tag=None,geometry_tag=None,fdebug=False):

    # Structure used to collect GENE relevant data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Standardized radial coordinate for geometry specification
    if geometry_tag not in geotags:
        raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")
    rtag = geotags[geometry_tag]

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()
    stag = species_tag.upper() if isinstance(species_tag,str) else None

    pp = None
    ppeb = None
    dpp = np.zeros(xvec_in.shape)
    dppeb = np.zeros(xvec_in.shape)
    if stag is not None and "P" in pddict and stag in pddict["P"]:
        pp = copy.deepcopy(twobj["PD"]["P"+stag][""])
        ppeb = copy.deepcopy(twobj["PD"]["P"+stag]["EB"])
        if "GRAD" in twobj["PD"]["P"+stag]:
            dpp = copy.deepcopy(twobj["PD"]["P"+stag]["GRAD"])
            dppeb = copy.deepcopy(twobj["PD"]["P"+stag]["GRADEB"])
    elif stag is not None and "N" in pddict and stag in pddict["N"] and "T" in pddict and stag in pddict["T"]:
        pp = ee * twobj["PD"]["N"+stag][""] * twobj["PD"]["T"+stag][""]
        ppeb = ee * np.sqrt(np.power(twobj["PD"]["N"+stag][""] * twobj["PD"]["T"+stag]["EB"],2.0) + \
                            np.power(twobj["PD"]["N"+stag]["EB"] * twobj["PD"]["T"+stag][""],2.0))
        if "GRAD" in twobj["PD"]["N"+stag] and "GRAD" in twobj["PD"]["T"+stag]:
            dpp = ee * (twobj["PD"]["N"+stag]["GRAD"] * twobj["PD"]["T"+stag][""] + twobj["N"+stag][""] * twobj["T"+stag]["GRAD"])
            dppeb = ee * np.sqrt(np.power(twobj["PD"]["N"+stag]["GRADEB"] * twobj["PD"]["T"+stag][""],2.0) + \
                                  np.power(twobj["PD"]["N"+stag]["GRAD"] * twobj["PD"]["T"+stag]["EB"],2.0) + \
                                  np.power(twobj["PD"]["N"+stag]["EB"] * twobj["PD"]["T"+stag]["GRAD"],2.0) + \
                                  np.power(twobj["PD"]["N"+stag][""] * twobj["PD"]["T"+stag]["GRADEB"],2.0))
    elif "W" in pddict and stag in pddict["W"] and "GRAD" in twobj["PD"]["W"+stag]:
        w2p = 2.0 / 3.0                          # Factor 2/3 comes from the fact that W is energy density, not pressure
        pp = w2p * twobj["PD"]["W"+stag][""]
        ppeb = w2p * twobj["PD"]["W"+stag]["EB"]
        if "GRAD" in twobj["PD"]["W"+stag]:
            dpp = w2p * twobj["PD"]["W"+stag]["GRAD"]
            dppeb = w2p * twobj["PD"]["W"+stag]["GRADEB"]

    if pp is not None and "ZD_BO" in outdict:
        bref = float(outdict["ZD_BO"]["VAL"])
        brefeb = float(outdict["ZD_BO"]["ERR"])
        val = np.power(bref,2.0)
        valeb = np.sqrt(8.0) * np.abs(bref * brefeb)
        jrr = outdict["PD_"+rtag]["DINV"] if "PD_"+rtag in outdict else np.ones(xvec_in.shape)

        betac = 2.51327412e-6                    # (2 mu_0)  -- since p is already given in N m^-2
        # Note: JETTO-QLK interface uses 8 mu_0... but why? (from Suydam criterion?)

        norm = float(-outdict["ZD_RZERO"]["VAL"] * betac) if "ZD_RZERO" in outdict else float(-betac)
        (dum,dume,dbeta,dbetaeb) = ytools.calc_generic_normalized_gradient(val,1.0,dpp,jrr,norm,valeb,dppeb,enforce_positive=True,default_der_err=0.05)
        # Calculate second derivative of the pressure profile - used in calculation of ExB shearing rate
        ddnum = np.zeros(xvec_in.shape)
        ddnumeb = np.zeros(xvec_in.shape)
        ddden = np.zeros(xvec_in.shape)
        ddnum[:-1] = ddnum[:-1] + np.diff(dpp)
        ddnum[1:] = ddnum[1:] + np.diff(dpp)
        ddnumeb[:-1] = ddnumeb[:-1] + np.diff(dppeb)
        ddnumeb[1:] = ddnumeb[1:] + np.diff(dppeb)
        ddden[:-1] = ddden[:-1] + np.diff(xvec_in)
        ddden[1:] = ddden[1:] + np.diff(xvec_in)
        ddpp = ddnum / ddden
        ddppeb = ddnumeb / ddden
        beta = betac * pp / val
        betaeb = betac * np.sqrt(np.power(ppeb,2.0) + np.power(pp * valeb / val,2.0)) / val
        outdict["PD_P"+stag] = {"VAL": pp, "ERR": ppeb, "DVAL": dpp, "DERR": dppeb, "DDVAL": ddpp, "DDERR": ddppeb}
        outdict["PD_BETA"+stag] = {"VAL": beta, "ERR": betaeb}
        outdict["PD_ABETA"+stag] = {"VAL": dbeta, "ERR": dbetaeb}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,"P"+stag,"BETA"+stag))

    return outdict


def calc_GENE_species_charge(twobj,newstruct=None,species_tag=None,geometry_tag=None,fdebug=False):

    # Structure used to collect GENE relevant data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Standardized radial coordinate for geometry specification
    if geometry_tag not in geotags:
        raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")
    rtag = geotags[geometry_tag]

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()
    stag = species_tag.upper() if isinstance(species_tag,str) else None

    zz = None
    zzeb = None
    dzz = None
    dzzeb = None
    azz = None
    azzeb = None
    if stag is not None and "Z" in pddict and stag in pddict["Z"]:
        qtag = "Z" + stag
        zfilt = (twobj["PD"][qtag][""] >= 1.0)
        zztemp = copy.deepcopy(twobj["PD"][qtag][""])
        if np.any(np.invert(zfilt)):
            zztemp[np.invert(zfilt)] = 1.0
        dzz = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else np.zeros(xvec_in.shape)
        dzzeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else np.zeros(xvec_in.shape)
        jrr = copy.deepcopy(outdict["PD_"+rtag]["DINV"]) if "PD_"+rtag in outdict else np.ones(xvec_in.shape)
        norm = float(-outdict["ZD_RZERO"]["VAL"]) if "ZD_RZERO" in outdict else -1.0
        (zz,zzeb,azz,azzeb) = ytools.calc_generic_normalized_gradient(zztemp,1.0,dzz,jrr,norm,twobj["PD"][qtag]["EB"],dzzeb,enforce_positive=True,default_der_err=0.01)
    elif stag is not None and "META" in twobj and "Z"+stag in twobj["META"]:
        zz = np.full(xvec_in.shape,twobj["META"]["Z"+stag])
        zzeb = np.zeros(xvec_in.shape)
        dzz = np.zeros(xvec_in.shape)
        dzzeb = np.zeros(xvec_in.shape)
        azz = np.zeros(xvec_in.shape)
        azzeb = np.zeros(xvec_in.shape)

    if zz is not None:

        outdict["PD_Z"+stag] = {"VAL": zz, "ERR": zzeb, "DVAL": dzz, "DERR": dzzeb}
        outdict["PD_AZ"+stag] = {"VAL": azz, "ERR": azzeb}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,"Z"+stag,"AZ"+stag))

    return outdict


def print_debug_GENE_species(datadict,numions=0,numimps=0,numextras=0):

    # Copy structure containing relevant GENE data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    nion = int(numions) if isinstance(numions,number_types) and int(numions) > 0 else 0
    nimp = int(numimps) if isinstance(numimps,number_types) and int(numimps) > 0 else 0
    nz = int(numextras) if isinstance(numextras,number_types) and int(numextras) > 0 else 0
    niall = None
    aniall = None
    tiall = None
    atiall = None
    ziall = None

    stag = "E"
    if "PD_N"+stag in outdict:
        with open(fdir+'ne.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_N"+stag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_N"+stag]["VAL"][ii]))
        with open(fdir+'Ane.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_AN"+stag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_AN"+stag]["VAL"][ii]))
    if "PD_T"+stag in outdict:
        with open(fdir+'Te.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_T"+stag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_T"+stag]["VAL"][ii]))
        with open(fdir+'Ate.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_AT"+stag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_AT"+stag]["VAL"][ii]))

    for idx in np.arange(0,nion):
        stag = "I%d" % (idx+1)
        if "PD_N"+stag in outdict:
            niall = np.vstack((niall,outdict["PD_N"+stag]["VAL"])) if niall is not None else np.atleast_2d(outdict["PD_N"+stag]["VAL"])
            aniall = np.vstack((aniall,outdict["PD_AN"+stag]["VAL"])) if aniall is not None else np.atleast_2d(outdict["PD_AN"+stag]["VAL"])
        if "PD_T"+stag in outdict:
            tiall = np.vstack((tiall,outdict["PD_T"+stag]["VAL"])) if tiall is not None else np.atleast_2d(outdict["PD_T"+stag]["VAL"])
            atiall = np.vstack((atiall,outdict["PD_AT"+stag]["VAL"])) if atiall is not None else np.atleast_2d(outdict["PD_AT"+stag]["VAL"])
        if "PD_Z"+stag in outdict:
            ziall = np.vstack((ziall,outdict["PD_Z"+stag]["VAL"])) if ziall is not None else np.atleast_2d(outdict["PD_Z"+stag]["VAL"])

    for idx in np.arange(0,nimp):
        itag = "IMP%d" % (idx+1)
        if "PD_N"+stag in outdict:
            niall = np.vstack((niall,outdict["PD_N"+stag]["VAL"])) if niall is not None else np.atleast_2d(outdict["PD_N"+stag]["VAL"])
            aniall = np.vstack((aniall,outdict["PD_AN"+stag]["VAL"])) if aniall is not None else np.atleast_2d(outdict["PD_AN"+stag]["VAL"])
        if "PD_T"+stag in outdict:
            tiall = np.vstack((tiall,outdict["PD_T"+stag]["VAL"])) if tiall is not None else np.atleast_2d(outdict["PD_T"+stag]["VAL"])
            atiall = np.vstack((atiall,outdict["PD_AT"+stag]["VAL"])) if atiall is not None else np.atleast_2d(outdict["PD_AT"+stag]["VAL"])
        if "PD_Z"+stag in outdict:
            ziall = np.vstack((ziall,outdict["PD_Z"+stag]["VAL"])) if ziall is not None else np.atleast_2d(outdict["PD_Z"+stag]["VAL"])

    for idx in np.arange(0,nz):
        stag = "Z%d" % (idx+1)
        if "PD_N"+stag in outdict:
            niall = np.vstack((niall,outdict["PD_N"+stag]["VAL"])) if niall is not None else np.atleast_2d(outdict["PD_N"+stag]["VAL"])
            aniall = np.vstack((aniall,outdict["PD_AN"+stag]["VAL"])) if aniall is not None else np.atleast_2d(outdict["PD_AN"+stag]["VAL"])
        if "PD_T"+stag in outdict:
            tiall = np.vstack((tiall,outdict["PD_T"+stag]["VAL"])) if tiall is not None else np.atleast_2d(outdict["PD_T"+stag]["VAL"])
            atiall = np.vstack((atiall,outdict["PD_AT"+stag]["VAL"])) if atiall is not None else np.atleast_2d(outdict["PD_AT"+stag]["VAL"])
        if "PD_Z"+stag in outdict:
            ziall = np.vstack((ziall,outdict["PD_Z"+stag]["VAL"])) if ziall is not None else np.atleast_2d(outdict["PD_Z"+stag]["VAL"])

    if niall is not None and aniall is not None:
        with open(fdir+'ni.dat','w') as ff:
            for ii in np.arange(0,niall.shape[1]):
                for jj in np.arange(0,niall.shape[0]):
                    ff.write("%20.8f" % (niall[jj,ii]))
                ff.write("\n")
        with open(fdir+'Ani.dat','w') as ff:
            for ii in np.arange(0,aniall.shape[1]):
                for jj in np.arange(0,aniall.shape[0]):
                    ff.write("%20.8f" % (aniall[jj,ii]))
                ff.write("\n")
    if tiall is not None and atiall is not None:
        with open(fdir+'Ti.dat','w') as ff:
            for ii in np.arange(0,tiall.shape[1]):
                for jj in np.arange(0,tiall.shape[0]):
                    ff.write("%20.8f" % (tiall[jj,ii]))
                ff.write("\n")
        with open(fdir+'Ati.dat','w') as ff:
            for ii in np.arange(0,atiall.shape[1]):
                for jj in np.arange(0,atiall.shape[0]):
                    ff.write("%20.8f" % (atiall[jj,ii]))
                ff.write("\n")
    if ziall is not None:
        with open(fdir+'Zi.dat','w') as ff:
            for ii in np.arange(0,ziall.shape[1]):
                for jj in np.arange(0,ziall.shape[0]):
                    ff.write("%20.8f" % (ziall[jj,ii]))
                ff.write("\n")


def calc_GENE_equilibrium(twobj,newstruct=None,geometry_tag=None,use_bias=False,fdebug=False):

    # Structure used to collect GENE relevant data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Standardized radial coordinate for geometry specification
    if geometry_tag not in geotags:
        raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")
    rtag = geotags[geometry_tag]

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()
    gcs = twobj["META"]["CSO"]
    gcp = twobj["META"]["CSOP"] if use_bias else ""     # It is NOT recommended to use ad-hoc corrected equilibria here
    numion = twobj["META"]["NUMI"]
    numimp = twobj["META"]["NUMIMP"]
    numz = twobj["META"]["NUMZ"]

    # Safety factor and normalized gradient (s_mag)
    if "Q" in pddict and gcp in pddict["Q"]:
        qtag = "Q" + gcp
        zfilt = (twobj["PD"][qtag][""] != 0.0)
        scale = 1.0
        val = copy.deepcopy(twobj["PD"][qtag][""])
        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else np.zeros(xvec_in.shape)
        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else np.zeros(xvec_in.shape)
        jrr = copy.deepcopy(outdict["PD_"+rtag]["DINV"]) if "PD_"+rtag in outdict else np.ones(xvec_in.shape)
        norm = copy.deepcopy(outdict["PD_"+rtag]["VAL"]) if "PD_"+rtag in outdict else np.ones(xvec_in.shape)
        (qq,qqeb,smag,smageb) = ytools.calc_generic_normalized_gradient(val,scale,grad,jrr,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.2)
        if np.any(np.invert(zfilt)):
            smag[np.invert(zfilt)] = np.sign(smag[np.invert(zfilt)]) * np.Inf
            smageb[np.invert(zfilt)] = np.Inf
        outdict["PD_Q"] = {"VAL": qq, "ERR": qqeb, "DVAL": grad, "DERR": gradeb}
        outdict["PD_SMAG"] = {"VAL": smag, "ERR": smageb}

        if fdebug:
            print("gene - adapter.py: %s geometry - %s and %s populated." % (geometry_tag,"Q","SMAG"))

    # Plasma normalized pressure, beta
    if "PD_BETAE" in outdict:

        # Containers for beta values of specific categories of species in the plasma
        betae = copy.deepcopy(outdict["PD_BETAE"]["VAL"])
        betaeeb = copy.deepcopy(outdict["PD_BETAE"]["ERR"])
        betai = np.zeros(xvec_in.shape)
        betaieb = np.zeros(xvec_in.shape)
        betaz = np.zeros(xvec_in.shape)
        betazeb = np.zeros(xvec_in.shape)
        betaf = np.zeros(xvec_in.shape)
        betafeb = np.zeros(xvec_in.shape)

        # Combined errors not added in quadrature, difficult to justify as being completely independent from each other
        for idx in np.arange(0,numion):
            btag = "PD_BETAI%d" % (idx+1)
            if btag in outdict:
                betai = betai + outdict[btag]["VAL"]
                betaieb = betaieb + outdict[btag]["ERR"]
        for idx in np.arange(0,numimp):
            btag = "PD_BETAIMP%d" % (idx+1)
            if btag in outdict:
                betaz = betaz + outdict[btag]["VAL"]
                betazeb = betazeb + outdict[btag]["ERR"]
        for idx in np.arange(0,numz):
            btag = "PD_BETAZ%d" % (idx+1)
            if btag in outdict:
                betaz = betaz + outdict[btag]["VAL"]
                betazeb = betazeb + outdict[btag]["ERR"]
        sources = ["NBI","ICRH","ECRH","LH"]
        for src in sources:
            btag = "PD_BETAFI_"+src
            if btag in outdict:
                betaf = betaf + outdict[btag]["VAL"]
                betafeb = betafeb + outdict[btag]["ERR"]

        betaei = betae + betai
        betaeieb = betaeeb + betaieb
        betath = betae + betai + betaz
        betatheb = betaeeb + betaieb + betazeb
        betatot = betae + betai + betaz + betaf
        betatoteb = betaeeb + betaieb + betazeb + betafeb
        outdict["PD_BETAEI"] = {"VAL": betaei, "ERR": betaeieb}
        outdict["PD_BETATH"] = {"VAL": betath, "ERR": betatheb}
        outdict["PD_BETATOT"] = {"VAL": betatot, "ERR": betatoteb}

        if fdebug:
            print("gene - adapter.py: %s, %s and %s populated." % ("BETAEI","BETATH","BETATOT"))

    if geometry_tag == "s_alpha":
        # Plasma normalized pressure gradient, alpha
        if "PD_ABETAE" in outdict and "PD_Q" in outdict:

            # Containers for beta derivatives of specific categories of species in the plasma
            rdbetae = copy.deepcopy(outdict["PD_ABETAE"]["VAL"])
            rdbetaeeb = copy.deepcopy(outdict["PD_ABETAE"]["ERR"])
            rdbetai = np.zeros(xvec_in.shape)
            rdbetaieb = np.zeros(xvec_in.shape)
            rdbetaz = np.zeros(xvec_in.shape)
            rdbetazeb = np.zeros(xvec_in.shape)
            rdbetaf = np.zeros(xvec_in.shape)
            rdbetafeb = np.zeros(xvec_in.shape)

            # Combined errors not added in quadrature, difficult to justify as being completely independent from each other
            for idx in np.arange(0,numion):
                atag = "PD_ABETAI%d" % (idx+1)
                if atag in outdict:
                    rdbetai = rdbetai + outdict[atag]["VAL"]
                    rdbetaieb = rdbetaieb + outdict[atag]["ERR"]
            for idx in np.arange(0,numimp):
                atag = "PD_ABETAIMP%d" % (idx+1)
                if atag in outdict:
                    rdbetaz = rdbetaz + outdict[atag]["VAL"]
                    rdbetazeb = rdbetazeb + outdict[atag]["ERR"]
            for idx in np.arange(0,numz):
                atag = "PD_ABETAZ%d" % (idx+1)
                if atag in outdict:
                    rdbetaz = rdbetaz + outdict[atag]["VAL"]
                    rdbetazeb = rdbetazeb + outdict[atag]["ERR"]
            sources = ["NBI","ICRH","ECRH","LH"]
            for src in sources:
                atag = "PD_ABETAFI_"+src
                if atag in outdict:
                    rdbetaf = rdbetaf + outdict[atag]["VAL"]
                    rdbetafeb = rdbetafeb + outdict[atag]["ERR"]

            alphaei = np.power(outdict["PD_Q"]["VAL"],2.0) * (rdbetae + rdbetai)
            alphaeieb = np.sqrt(8.0 * np.power(outdict["PD_Q"]["ERR"] * outdict["PD_Q"]["VAL"] * (rdbetae + rdbetai),2.0) + \
                                      np.power(outdict["PD_Q"]["VAL"] * outdict["PD_Q"]["VAL"] * (rdbetaeeb + rdbetaieb),2.0))
            alphath = np.power(outdict["PD_Q"]["VAL"],2.0) * (rdbetae + rdbetai + rdbetaz)
            alphatheb = np.sqrt(8.0 * np.power(outdict["PD_Q"]["ERR"] * outdict["PD_Q"]["VAL"] * (rdbetae + rdbetai + rdbetaz),2.0) + \
                                      np.power(outdict["PD_Q"]["VAL"] * outdict["PD_Q"]["VAL"] * (rdbetaeeb + rdbetaieb + rdbetazeb),2.0))
            alphatot = np.power(outdict["PD_Q"]["VAL"],2.0) * (rdbetae + rdbetai + rdbetaz + rdbetaf)
            alphatoteb = np.sqrt(8.0 * np.power(outdict["PD_Q"]["ERR"] * outdict["PD_Q"]["VAL"] * (rdbetae + rdbetai + rdbetaz + rdbetaf),2.0) + \
                                       np.power(outdict["PD_Q"]["VAL"] * outdict["PD_Q"]["VAL"] * (rdbetaeeb + rdbetaieb + rdbetazeb + rdbetafeb),2.0))
            outdict["PD_ALPHAEI"] = {"VAL": alphaei, "ERR": alphaeieb}
            outdict["PD_ALPHATH"] = {"VAL": alphath, "ERR": alphatheb}
            outdict["PD_ALPHATOT"] = {"VAL": alphatot, "ERR": alphatoteb}

            if fdebug:
                print("gene - adapter.py: %s geometry - %s, %s and %s populated." % (geometry_tag,"ALPHAEI","ALPHATH","ALPHATOT"))

    return outdict


def print_debug_GENE_equilibrium(datadict):

    # Copy structure containing relevant GENE data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    if "PD_Q" in outdict:
        with open(fdir+'q.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_Q"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_Q"]["VAL"][ii]))
    if "PD_SMAG" in outdict:
        with open(fdir+'smag.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_SMAG"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_SMAG"]["VAL"][ii]))
    if "PD_BETAEI" in outdict:
        with open(fdir+'betaei.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BETAEI"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_BETAEI"]["VAL"][ii]))
    if "PD_BETATH" in outdict:
        with open(fdir+'betath.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BETATH"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_BETATH"]["VAL"][ii]))
    if "PD_BETATOT" in outdict:
        with open(fdir+'betatot.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_BETATOT"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_BETATOT"]["VAL"][ii]))
    if "PD_ALPHAEI" in outdict:
        with open(fdir+'alphaei.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_ALPHAEI"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_ALPHAEI"]["VAL"][ii]))
    if "PD_ALPHATH" in outdict:
        with open(fdir+'alphath.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_ALPHATH"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_ALPHATH"]["VAL"][ii]))
    if "PD_ALPHATOT" in outdict:
        with open(fdir+'alphatot.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_ALPHATOT"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_ALPHATOT"]["VAL"][ii]))


def calc_QuaLiKiz_geometric_rotation(twobj,newstruct=None,fdebug=False):

    # Structure used to collect relevant QuaLiKiz data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()

    # Toroidal and poloidal flow angular frequencies and gradients - intermediate quantity to be used in further calculations, not required by QuaLiKiz
    tags = ["TOR", "POL"]
    for dtag in tags:
        af = np.zeros(xvec_in.shape)
        afeb = np.zeros(xvec_in.shape)
        daf = np.zeros(xvec_in.shape)
        dafeb = np.zeros(xvec_in.shape)
        if "AF" in pddict and dtag in pddict["AF"]:
            qtag = "AF"+dtag
            af = copy.deepcopy(twobj["PD"][qtag][""])
            afeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
            if "GRAD" in twobj["PD"][qtag]:
                daf = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                dafeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
        outdict["PD_AF"+dtag] = {"VAL": af, "ERR": afeb, "DVAL": daf, "DERR": dafeb}

    # Toroidal velocity for R_major_midplane_average
    dtag = "TOR"
    if "PD_AF"+dtag in outdict and "PD_RO" in outdict:
        ro = copy.deepcopy(outdict["PD_RO"]["VAL"])
        roeb = copy.deepcopy(outdict["PD_RO"]["ERR"])
        dro = copy.deepcopy(outdict["PD_RO"]["DVAL"])
        droeb = copy.deepcopy(outdict["PD_RO"]["DERR"])
        utor = ro * outdict["PD_AF"+dtag]["VAL"]
        utoreb = np.sqrt(np.power(roeb * outdict["PD_AF"+dtag]["VAL"],2.0) + np.power(ro * outdict["PD_AF"+dtag]["ERR"],2.0))
        dutor = dro * outdict["PD_AF"+dtag]["VAL"] + ro * outdict["PD_AF"+dtag]["DVAL"]
        dutoreb = np.sqrt(np.power(droeb * outdict["PD_AF"+dtag]["VAL"],2.0) + np.power(dro * outdict["PD_AF"+dtag]["ERR"],2.0) + \
                          np.power(roeb * outdict["PD_AF"+dtag]["DVAL"],2.0) + np.power(ro * outdict["PD_AF"+dtag]["DERR"],2.0))

        # Alternate version using R_major_outer - not conventional!
#        (rmajo,rmajoj,rmajoe) = twobj["CD"].convert(xvec_in,gcs,"RMAJORO",gcp,gcp,fdebug=fdebug)
#        utor = rmajo * outdict["PD_AF"+dtag]["VAL"]
#        utoreb = np.sqrt(np.power(rmajoe * outdict["PD_AF"+dtag]["VAL"],2.0) + np.power(rmajo * outdict["PD_AF"+dtag]["ERR"],2.0))
#        dutor = outdict["PD_AF"+dtag]["VAL"] / rmajoj + rmajo * outdict["PD_AF"+dtag]["DVAL"]
#        dutoreb = np.sqrt(np.power(outdict["PD_AF"+dtag]["DVAL"] / rmajoj,2.0) + \
#                          np.power(rmajoe * outdict["PD_AF"+dtag]["DVAL"],2.0) + np.power(rmajo * outdict["PD_AF"+dtag]["DERR"],2.0))

        outdict["PD_U"+dtag] = {"VAL": utor, "ERR": utoreb, "DVAL": dutor, "DERR": dutoreb}
        if fdebug:
            print("qualikiz - adapter.py: %s populated." % ("U"+dtag))

    # Poloidal velocity for r_minor_midplane_average
    dtag = "POL"
    if "PD_AF"+dtag in outdict and "PD_R" in outdict:
        rr = copy.deepcopy(outdict["PD_R"]["VAL"])
        rreb = copy.deepcopy(outdict["PD_R"]["ERR"])
        drr = copy.deepcopy(outdict["PD_R"]["DVAL"])
        drreb = copy.deepcopy(outdict["PD_R"]["DERR"])
        upol = rr * outdict["PD_AF"+dtag]["VAL"]
        upoleb = np.sqrt(np.power(rreb * outdict["PD_AF"+dtag]["VAL"],2.0) + np.power(rr * outdict["PD_AF"+dtag]["ERR"],2.0))
        dupol = drr * outdict["PD_AF"+dtag]["VAL"] + rr * outdict["PD_AF"+dtag]["DVAL"]
        dupoleb = np.sqrt(np.power(drreb * outdict["PD_AF"+dtag]["VAL"],2.0) + np.power(drr * outdict["PD_AF"+dtag]["ERR"],2.0) + \
                          np.power(rreb * outdict["PD_AF"+dtag]["DVAL"],2.0) + np.power(rr * outdict["PD_AF"+dtag]["DERR"],2.0))

        outdict["PD_U"+dtag] = {"VAL": upol, "ERR": upoleb, "DVAL": dupol, "DERR": dupoleb}
        if fdebug:
            print("qualikiz - adapter.py: %s populated." % ("U"+dtag))

    # Toroidal flow velocity and normalized gradient
    dtag = "TOR"
    if "PD_U"+dtag in outdict:
        qtag = "PD_U"+dtag
        grad = copy.deepcopy(outdict[qtag]["DVAL"])
        gradeb = copy.deepcopy(outdict[qtag]["DERR"])
        jrr = copy.deepcopy(outdict["PD_R"]["DINV"]) if "PD_R" in outdict else np.ones(xvec_in.shape)
        cref = float(outdict["ZD_CREF"]["VAL"]) if "ZD_CREF" in outdict else 3.0949691e5     # = sqrt(1 keV / m_p) in m/s
        norm = float(-outdict["ZD_RZERO"]["VAL"] / cref) if "ZD_RZERO" in outdict else -1.0 / cref
        (dum,dume,au,aueb) = ytools.calc_generic_normalized_gradient(np.ones(xvec_in.shape),1.0,grad,jrr,norm,np.zeros(xvec_in.shape),gradeb,enforce_positive=False,default_der_err=0.0)
        mach = outdict["PD_U"+dtag]["VAL"] / cref
        macheb = outdict["PD_U"+dtag]["ERR"] / cref

        outdict["PD_MACH"+dtag] = {"VAL": mach, "ERR": macheb}
        outdict["PD_AU"+dtag] = {"VAL": au, "ERR": aueb}

        if fdebug:
            print("qualikiz - adapter.py: %s and %s populated." % ("MACHTOR","AUTOR"))

    # Poloidal flow velocity and normalized gradient - not calculated as it is not an input to QuaLiKiz

    return outdict


def calc_QuaLiKiz_magnetic_rotation(twobj,newstruct=None,use_def=None,fdebug=False):

    # Structure used to collect relevant QuaLiKiz data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()

    # Parallel flow velocity and normalized gradient - toroidal angular frequency multiplied by R_major_outer, using B_tor_outer for consistency
    if "PD_UTOR" in outdict and "PD_UPOL" in outdict and "PD_BTOR" in outdict and "PD_BPOL" in outdict and "PD_BTOTO" in outdict:

        untor = outdict["PD_UTOR"]["VAL"] / outdict["PD_BTOTO"]["VAL"]
        untoreb = np.sqrt(np.power(outdict["PD_UTOR"]["ERR"],2.0) + np.power(untor * outdict["PD_BTOTO"]["ERR"],2.0)) / outdict["PD_BTOTO"]["VAL"]
        duntor = (outdict["PD_UTOR"]["DVAL"] - untor * outdict["PD_BTOTO"]["DVAL"]) / outdict["PD_BTOTO"]["VAL"]
        duntoreb = np.sqrt(np.power(outdict["PD_UTOR"]["DERR"],2.0) + np.power(untoreb * outdict["PD_BTOTO"]["DVAL"],2.0) + \
                           np.power(untor * outdict["PD_BTOTO"]["DERR"],2.0) + np.power(duntor * outdict["PD_BTOTO"]["ERR"],2.0)) / outdict["PD_BTOTO"]["VAL"]

        unpol = outdict["PD_UPOL"]["VAL"] / outdict["PD_BTOTO"]["VAL"]
        unpoleb = np.sqrt(np.power(outdict["PD_UPOL"]["ERR"],2.0) + np.power(unpol * outdict["PD_BTOTO"]["ERR"],2.0)) / outdict["PD_BTOTO"]["VAL"]
        dunpol = (outdict["PD_UPOL"]["DVAL"] - unpol * outdict["PD_BTOTO"]["DVAL"]) / outdict["PD_BTOTO"]["VAL"]
        dunpoleb = np.sqrt(np.power(outdict["PD_UPOL"]["DERR"],2.0) + np.power(unpoleb * outdict["PD_BTOTO"]["DVAL"],2.0) + \
                           np.power(unpol * outdict["PD_BTOTO"]["DERR"],2.0) + np.power(dunpol * outdict["PD_BTOTO"]["ERR"],2.0)) / outdict["PD_BTOTO"]["VAL"]

        upar = untor * outdict["PD_BTOR"]["VAL"] + unpol * outdict["PD_BPOL"]["VAL"]
        upareb = np.sqrt(np.power(untoreb * outdict["PD_BTOR"]["VAL"],2.0) + np.power(untor * outdict["PD_BTOR"]["ERR"],2.0) + \
                         np.power(unpoleb * outdict["PD_BPOL"]["VAL"],2.0) + np.power(unpol * outdict["PD_BTOR"]["ERR"],2.0))
        dupar = duntor * outdict["PD_BTOR"]["VAL"] + untor * outdict["PD_BTOR"]["DVAL"] + \
                dunpol * outdict["PD_BPOL"]["VAL"] + unpol * outdict["PD_BPOL"]["DVAL"]
        dupareb = np.sqrt(np.power(duntoreb * outdict["PD_BTOR"]["VAL"],2.0) + np.power(duntor * outdict["PD_BTOR"]["ERR"],2.0) + \
                          np.power(untoreb * outdict["PD_BTOR"]["DVAL"],2.0) + np.power(untor * outdict["PD_BTOR"]["DERR"],2.0) + \
                          np.power(dunpoleb * outdict["PD_BPOL"]["VAL"],2.0) + np.power(dunpol * outdict["PD_BPOL"]["ERR"],2.0) + \
                          np.power(unpoleb * outdict["PD_BPOL"]["DVAL"],2.0) + np.power(unpol * outdict["PD_BPOL"]["DERR"],2.0))

        outdict["PD_UPAR"] = {"VAL": upar, "ERR": upareb, "DVAL": dupar, "DERR": dupareb}
        if fdebug:
            print("qualikiz - adapter.py: %s populated." % ("UPAR"))

    dtag = "PAR"
    if "PD_U"+dtag in outdict:
        qtag = "PD_U"+dtag
        grad = copy.deepcopy(outdict[qtag]["DVAL"])
        gradeb = copy.deepcopy(outdict[qtag]["DERR"])
        jrr = copy.deepcopy(outdict["PD_R"]["DINV"]) if "PD_R" in outdict else np.ones(xvec_in.shape)
        cref = float(outdict["ZD_CREF"]["VAL"]) if "ZD_CREF" in outdict else 3.0949691e5     # = sqrt(1 keV / m_p) in m/s
        norm = float(-outdict["ZD_RZERO"]["VAL"] / cref) if "ZD_RZERO" in outdict else -1.0 / cref
        (dum,dume,au,aueb) = ytools.calc_generic_normalized_gradient(np.ones(xvec_in.shape),1.0,grad,jrr,norm,np.zeros(xvec_in.shape),gradeb,enforce_positive=False,default_der_err=0.0)
        mach = outdict[qtag]["VAL"] / cref
        macheb = outdict[qtag]["ERR"] / cref

        outdict["PD_MACH"+dtag] = {"VAL": mach, "ERR": macheb}
        outdict["PD_AU"+dtag] = {"VAL": au, "ERR": aueb}

        if fdebug:
            print("qualikiz - adapter.py: %s and %s populated." % ("MACHPAR","AUPAR"))

    # Perpendicular flow velocity and gradient - using radial electric field and total flux-surface-averaged magnetic field (ExB drift velocity)
    dtag = "PER"
    uper = None
    upereb = None
    duper = None
    dupereb = None
    upi = None
    upieb = None
    dupi = None
    dupieb = None

    if "U" in pddict and dtag in pddict["U"]:
        qtag = "U"+dtag
        uper = copy.deepcopy(twobj["PD"][qtag][""])
        upereb = copy.deepcopy(twobj["PD"][qtag]["EB"])
        duper = np.zeros(xvec_in.shape)
        dupereb = np.zeros(xvec_in.shape)
        if "GRAD" in twobj["PD"][qtag]:
            duper = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
            dupereb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
        upi = np.zeros(xvec_in.shape)
        upieb = np.zeros(xvec_in.shape)
        dupi = np.zeros(xvec_in.shape)
        dupieb = np.zeros(xvec_in.shape)

    if uper is None and "E" in pddict and "TOR" in pddict["E"] and "PD_BTOT" in outdict and "PD_BZERO" in outdict and "PD_BPOL" in outdict:

        # Calculate ExB drift velocity from given radial electric field contributions
        exbtor = twobj["PD"]["ETOR"][""] / outdict["PD_BTOT"]["VAL"]
        exbtoreb = np.sqrt(np.power(twobj["PD"]["ETOR"]["EB"],2.0) + np.power(exbtor * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]
        dexbtor = np.zeros(xvec_in.shape)
        dexbtoreb = np.zeros(xvec_in.shape)
        if "GRAD" in twobj["PD"]["ETOR"]:
            dexbtor = (twobj["PD"]["ETOR"]["GRAD"] - exbtor * outdict["PD_BTOT"]["DVAL"]) / outdict["PD_BTOT"]["VAL"]
            dexbtoreb = np.sqrt(np.power(twobj["PD"]["ETOR"]["GRADEB"],2.0) + \
                                np.power(exbtoreb * outdict["PD_BTOT"]["DVAL"],2.0) + \
                                np.power(exbtor * outdict["PD_BTOT"]["DERR"],2.0) + \
                                np.power(dexbtor * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]

        exbpol = np.zeros(xvec_in.shape)
        exbpoleb = np.zeros(xvec_in.shape)
        dexbpol = np.zeros(xvec_in.shape)
        dexbpoleb = np.zeros(xvec_in.shape)
        if "POL" in pddict["E"]:
            exbpol = twobj["PD"]["EPOL"][""] / outdict["PD_BTOT"]["VAL"]
            expoleb = np.sqrt(np.power(twobj["PD"]["EPOL"]["EB"],2.0) + np.power(exbtor * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]
            if "GRAD" in twobj["PD"]["EPOL"]:
                dexbpol = (twobj["PD"]["EPOL"]["GRAD"] - exbpol * outdict["PD_BTOT"]["DVAL"]) / outdict["PD_BTOT"]["VAL"]
                dexbpoleb = np.sqrt(np.power(twobj["PD"]["ETOR"]["GRADEB"],2.0) + \
                                    np.power(exbpoleb * outdict["PD_BTOT"]["DVAL"],2.0) + \
                                    np.power(exbpol * outdict["PD_BTOT"]["DERR"],2.0) + \
                                    np.power(dexbpol * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]

        exbdpi = np.zeros(xvec_in.shape)
        exbdpieb = np.zeros(xvec_in.shape)
        dexbdpi = np.zeros(xvec_in.shape)
        dexbdpieb = np.zeros(xvec_in.shape)
        if "DPI" in pddict["E"]:
            exbdpi = twobj["PD"]["EDPI"][""] / outdict["PD_BTOT"]["VAL"]
            exbdpieb = np.sqrt(np.power(twobj["PD"]["EDPI"]["EB"],2.0) + np.power(exbdpi * otudict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]
            if "GRAD" in twobj["PD"]["EDPI"]:
                dexbdpi = (twobj["PD"]["EDPI"]["GRAD"] - exbdpi * outdict["PD_BTOT"]["DVAL"]) / outdict["PD_BTOT"]["VAL"]
                dexbdpieb = np.sqrt(np.power(twobj["PD"]["EDPI"]["GRADEB"],2.0) + \
                                    np.power(exbdpieb * outdict["PD_BTOT"]["DVAL"],2.0) + \
                                    np.power(exbdpi * outdict["PD_BTOT"]["DERR"],2.0) + \
                                    np.power(dexbdpi * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]

        # Assumed that all electric fields are given with the correct sign convention for their contribution
        uper = exbtor + exbpol + exbdpi
        upereb = np.sqrt(np.power(exbtoreb,2.0) + np.power(exbpoleb,2.0) + np.power(exbdpieb,2.0))
        duper = dexbtor + dexbpol + dexbdpi
        dupereb = np.sqrt(np.power(dexbtoreb,2.0) + np.power(dexbpoleb,2.0) + np.power(dexbdpieb,2.0))

    if uper is None and "PD_UTOR" in outdict and "PD_UPOL" in outdict and "PD_BTOT" in outdict and "PD_BPOL" in outdict and "ZD_BZERO" in outdict:

        untor = outdict["PD_UTOR"]["VAL"] / outdict["PD_BTOT"]["VAL"]
        untoreb = np.sqrt(np.power(outdict["PD_UTOR"]["ERR"],2.0) + np.power(untor * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]
        duntor = (outdict["PD_UTOR"]["DVAL"] - untor * outdict["PD_BTOT"]["DVAL"]) / outdict["PD_BTOT"]["VAL"]
        duntoreb = np.sqrt(np.power(outdict["PD_UTOR"]["DERR"],2.0) + np.power(untoreb * outdict["PD_BTOT"]["DVAL"],2.0) + \
                           np.power(untor * outdict["PD_BTOT"]["DERR"],2.0) + np.power(duntor * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]

        unpol = outdict["PD_UPOL"]["VAL"] / outdict["PD_BTOT"]["VAL"]
        unpoleb = np.sqrt(np.power(outdict["PD_UPOL"]["ERR"],2.0) + np.power(unpol * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]
        dunpol = (outdict["PD_UPOL"]["DVAL"] - unpol * outdict["PD_BTOT"]["DVAL"]) / outdict["PD_BTOT"]["VAL"]
        dunpoleb = np.sqrt(np.power(outdict["PD_UPOL"]["DERR"],2.0) + np.power(unpoleb * outdict["PD_BTOT"]["DVAL"],2.0) + \
                           np.power(unpol * outdict["PD_BTOT"]["DERR"],2.0) + np.power(dunpol * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]

        # Calculate radial electric field component due to ion pressure gradient
        ee = 1.60217662e-19
        numion = 1 if isinstance(use_def,str) and use_def.lower() == 'jetto' else twobj["META"]["NUMI"]
        numimp = 0 if isinstance(use_def,str) and use_def.lower() == 'jetto' else twobj["META"]["NUMIMP"]
        numz = 0 if isinstance(use_def,str) and use_def.lower() == 'jetto' else twobj["META"]["NUMZ"]
        ionloop = {"I": numion, "IMP": numimp, "Z": numz}
        dnpi = np.zeros(xvec_in.shape)
        dnpieb = np.zeros(xvec_in.shape)
        ddnpi = np.zeros(xvec_in.shape)
        ddnpieb = np.zeros(xvec_in.shape)
        for key in ionloop:
            for idx in range(0,ionloop[key]):
                itag = key+"%d" % (idx+1)
                if "PD_P"+itag in outdict and "PD_ON"+itag in outdict and "PD_Z"+itag in outdict:
                    niz = ee * outdict["PD_ON"+itag]["VAL"] * outdict["PD_Z"+itag]["VAL"] + 1.0e-10    # Avoid division by zero, p = 0 if n = 0
                    nizeb = np.sqrt(np.power(ee * outdict["PD_ON"+itag]["ERR"] * outdict["PD_Z"+itag]["VAL"],2.0) + \
                                    np.power(ee * outdict["PD_ON"+itag]["VAL"] * outdict["PD_Z"+itag]["ERR"],2.0))
                    dniz = ee * (outdict["PD_ON"+itag]["DVAL"] * outdict["PD_Z"+itag]["VAL"] + outdict["PD_ON"+itag]["VAL"] * outdict["PD_Z"+itag]["DVAL"])
                    dnizeb = np.sqrt(np.power(ee * outdict["PD_ON"+itag]["DERR"] * outdict["PD_Z"+itag]["VAL"],2.0) + \
                                     np.power(ee * outdict["PD_ON"+itag]["DVAL"] * outdict["PD_Z"+itag]["ERR"],2.0) + \
                                     np.power(ee * outdict["PD_ON"+itag]["ERR"] * outdict["PD_Z"+itag]["DVAL"],2.0) + \
                                     np.power(ee * outdict["PD_ON"+itag]["VAL"] * outdict["PD_Z"+itag]["DERR"],2.0))

                    current_dnpi = outdict["PD_P"+itag]["DVAL"] / niz
                    current_dnpieb = np.sqrt(np.power(outdict["PD_P"+itag]["DERR"],2.0) + np.power(current_dnpi * nizeb,2.0)) / niz
                    current_ddnpi = (outdict["PD_P"+itag]["DDVAL"] - current_dnpi * dniz) / niz
                    current_ddnpieb = np.sqrt(np.power(outdict["PD_P"+itag]["DDERR"],2.0) + np.power(current_dnpieb * dniz,2.0) + \
                                              np.power(current_dnpi * dnizeb,2.0) + np.power(current_ddnpi * nizeb,2.0)) / niz
                    dnpi = dnpi + current_dnpi
                    dnpieb = np.sqrt(np.power(dnpieb,2.0) + np.power(current_dnpieb,2.0))
                    ddnpi = ddnpi + current_ddnpi
                    ddnpieb = np.sqrt(np.power(ddnpieb,2.0) + np.power(current_ddnpieb,2.0))

        upi = dnpi / outdict["PD_BTOT"]["VAL"]
        upieb = np.sqrt(np.power(dnpieb,2.0) + np.power(upi * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]
        dupi = (ddnpi - upi * outdict["PD_BTOT"]["DVAL"]) / outdict["PD_BTOT"]["VAL"]
        dupieb = np.sqrt(np.power(ddnpieb,2.0) + np.power(upieb * outdict["PD_BTOT"]["DVAL"],2.0) + np.power(upi * outdict["PD_BTOT"]["DERR"],2.0) + \
                         np.power(dupi * outdict["PD_BTOT"]["ERR"],2.0)) / outdict["PD_BTOT"]["VAL"]

        # Calculate perpendicular flow via ExB drift velocity - includes ion pressure gradient and polodial flow velocity components is present
        uper = untor * outdict["PD_BPOL"]["VAL"] + upi - unpol * outdict["ZD_BZERO"]["VAL"]
        upereb = np.sqrt(np.power(untoreb * outdict["PD_BPOL"]["VAL"],2.0) + np.power(untor * outdict["PD_BPOL"]["ERR"],2.0) + np.power(upieb,2.0) + \
                         np.power(unpoleb * outdict["ZD_BZERO"]["VAL"],2.0) + np.power(unpol * outdict["ZD_BZERO"]["ERR"],2.0))
        duper = duntor * outdict["PD_BPOL"]["VAL"] + untor * outdict["PD_BPOL"]["DVAL"] + dupi - dunpol * outdict["ZD_BZERO"]["VAL"]
        dupereb = np.sqrt(np.power(duntoreb * outdict["PD_BPOL"]["VAL"],2.0) + np.power(duntor * outdict["PD_BPOL"]["ERR"],2.0) + \
                          np.power(untoreb * outdict["PD_BPOL"]["DVAL"],2.0) + np.power(untor * outdict["PD_BPOL"]["DERR"],2.0) + \
                          np.power(dupieb,2.0) + np.power(dunpoleb * outdict["ZD_BZERO"]["VAL"],2.0) + np.power(dunpol * outdict["ZD_BZERO"]["ERR"],2.0))

    if uper is not None:
        outdict["PD_U"+dtag] = {"VAL": uper, "ERR": upereb, "DVAL": duper, "DERR": dupereb}
        outdict["PD_UDPI"] = {"VAL": upi, "ERR": upieb, "DVAL": dupi, "DERR": dupieb}
        if fdebug:
            print("qualikiz - adapter.py: perpendicular flow velocity calculated.")

    if "PD_U"+dtag in outdict:
        qtag = "PD_U"+dtag
        grad = copy.deepcopy(outdict[qtag]["DVAL"])
        gradeb = copy.deepcopy(outdict[qtag]["DERR"])
        jrr = copy.deepcopy(outdict["PD_R"]["DINV"]) if "PD_R" in outdict else np.ones(xvec_in.shape)
        cref = float(outdict["ZD_CREF"]["VAL"]) if "ZD_CREF" in outdict else 3.0949691e5     # = sqrt(1 keV / m_p) in m/s
        norm = float(-outdict["ZD_RZERO"]["VAL"] / cref) if "ZD_RZERO" in outdict else -1.0 / cref
        (dum,dume,au,aueb) = ytools.calc_generic_normalized_gradient(np.ones(xvec_in.shape),1.0,grad,jrr,norm,np.zeros(xvec_in.shape),gradeb,enforce_positive=False,default_der_err=0.0)
        mach = outdict[qtag]["VAL"] / cref
        macheb = outdict[qtag]["ERR"] / cref

        outdict["PD_MACH"+dtag] = {"VAL": mach, "ERR": macheb}
        outdict["PD_AU"+dtag] = {"VAL": au, "ERR": aueb}

    return outdict


def calc_QuaLiKiz_ExB_shearing_rate(twobj,newstruct=None,fdebug=False):

    # Structure used to collect relevant QuaLiKiz data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()

    # Geometric factor (fgeo = q / r) for calculation of ExB shearing rate
    fgeo = None
    fgeoeb = None
    dfgeo = None
    dfgeoeb = None
    if "PD_R" in outdict and "PD_Q" in outdict:
        rr = copy.deepcopy(outdict["PD_R"]["VAL"])
        rr[rr < 1.0e-5] = 1.0e-5
        fgeo = outdict["PD_Q"]["VAL"] / rr
        fgeoeb = np.sqrt(np.power(outdict["PD_Q"]["ERR"],2.0) + np.power(fgeo * outdict["PD_R"]["ERR"],2.0)) / rr
        dfgeo = (outdict["PD_Q"]["DVAL"] - fgeo * outdict["PD_R"]["DVAL"]) / rr
        dfgeoeb = np.sqrt(np.power(outdict["PD_Q"]["DERR"],2.0) + np.power(fgeo * outdict["PD_R"]["DERR"],2.0) + \
                          np.power(fgeoeb * outdict["PD_R"]["DVAL"],2.0) + np.power(dfgeo * outdict["PD_R"]["ERR"],2.0)) / rr

    # Perpendicular flow velocity and gradient multiplied by the geometric factor
    #    This is the recommended data insertion point from JETTO JSP for TCI replication, q / r is too sensitive to numerical scheme
    dtag = "GEO"
    if "U" in pddict and "TOR"+dtag in pddict["U"]:
        ugeo = copy.deepcopy(twobj["PD"]["UTOR"+dtag][""])
        ugeoeb = copy.deepcopy(twobj["PD"]["UTOR"+dtag]["EB"])
        if "GRAD" in twobj["PD"]["UTOR"+dtag]:
            dugeo = copy.deepcopy(twobj["PD"]["UTOR"+dtag]["GRAD"])
            dugeoeb = copy.deepcopy(twobj["PD"]["UTOR"+dtag]["GRADEB"])

        # Adjustment of perpendicular flow to include polodial flow and ion pressure gradient components if also provided
        if "POL"+dtag in pddict["U"]:
            ugeo = ugeo + twobj["PD"]["UPOL"+dtag][""]
            ugeoeb = np.sqrt(np.power(ugeoeb,2.0) + np.power(twobj["PD"]["UPOL"+dtag]["EB"],2.0))
            if "GRAD" in twobj["PD"]["UPOL"+dtag]:
                dugeo = dugeo + twobj["PD"]["UPOL"+dtag]["GRAD"]
                dugeoeb = np.sqrt(np.power(dugeoeb,2.0) + np.power(twobj["PD"]["UPOL"+dtag]["GRADEB"],2.0))
        if "DPI"+dtag in pddict["U"]:
            ugeo = ugeo + twobj["PD"]["UDPI"+dtag][""]
            ugeoeb = np.sqrt(np.power(ugeoeb,2.0) + np.power(twobj["PD"]["UDPI"+dtag]["EB"],2.0))
            if "GRAD" in twobj["PD"]["UDPI"+dtag]:
                dugeo = dugeo + twobj["PD"]["UDPI"+dtag]["GRAD"]
                dugeoeb = np.sqrt(np.power(dugeoeb,2.0) + np.power(twobj["PD"]["UDPI"+dtag]["GRADEB"],2.0))

        outdict["PD_U"+dtag] = {"VAL": ugeo, "ERR": ugeoeb, "DVAL": dugeo, "DERR": dugeoeb}

    elif fgeo is not None and "PD_UPER" in outdict:
        ugeo = outdict["PD_UPER"]["VAL"] * fgeo
        ugeoeb = np.sqrt(np.power(outdict["PD_UPER"]["ERR"] * fgeo,2.0) + np.power(outdict["PD_UPER"]["VAL"] * fgeoeb,2.0))
        dugeo = outdict["PD_UPER"]["DVAL"] * fgeo - outdict["PD_UPER"]["VAL"] * dfgeo
        dugeoeb = np.sqrt(np.power(outdict["PD_UPER"]["DERR"] * fgeo,2.0) + np.power(outdict["PD_UPER"]["DVAL"] * fgeoeb,2.0) + \
                          np.power(outdict["PD_UPER"]["ERR"] * dfgeo,2.0) + np.power(outdict["PD_UPER"]["VAL"] * dfgeoeb,2.0))

        outdict["PD_U"+dtag] = {"VAL": ugeo, "ERR": ugeoeb, "DVAL": dugeo, "DERR": dugeoeb}

    # ExB shearing rate, gamma_E - related to perpendicular flow velocity gradient
    if fgeo is not None and "PD_U"+dtag in outdict:

        qtag = "PD_U"+dtag
        grad = copy.deepcopy(outdict[qtag]["DVAL"])
        gradeb = copy.deepcopy(outdict[qtag]["DERR"])
        jrr = copy.deepcopy(outdict["PD_R"]["DINV"]) if "PD_R" in outdict else np.ones(xvec_in.shape)
        cref = float(outdict["ZD_CREF"]["VAL"]) if "ZD_CREF" in outdict else 3.0949691e5     # = sqrt(1 keV / m_p) in m/s
        norm = float(outdict["ZD_RZERO"]["VAL"] / cref) if "ZD_RZERO" in outdict else 1.0 / cref
        (dum,dume,gammae,gammaeeb) = ytools.calc_generic_normalized_gradient(fgeo,1.0,grad,jrr,norm,fgeoeb,gradeb,enforce_positive=False,default_der_err=0.0)

        outdict["PD_GAMMAE"] = {"VAL": gammae, "ERR": gammaeeb}

        if fdebug:
            print("qualikiz - adapter.py: %s populated." % ("GAMMAE"))

    # ExB shearing rate, gamma_E - separate component due to ion pressure gradient for comparison purposes
    dtag = "DPI"
    if fgeo is not None and "PD_U"+dtag in outdict:

        qtag = "PD_U"+dtag
        grad = outdict[qtag]["DVAL"] * fgeo - outdict["PD_UPER"]["VAL"] * dfgeo
        gradeb = np.sqrt(np.power(outdict[qtag]["DERR"] * fgeo,2.0) + np.power(outdict[qtag]["DVAL"] * fgeoeb,2.0) + \
                         np.power(outdict[qtag]["ERR"] * dfgeo,2.0) + np.power(outdict[qtag]["VAL"] * dfgeoeb,2.0))
        jrr = copy.deepcopy(outdict["PD_R"]["DINV"]) if "PD_R" in outdict else np.ones(xvec_in.shape)
        cref = float(outdict["ZD_CREF"]["VAL"]) if "ZD_CREF" in outdict else 3.0949691e5     # = sqrt(1 keV / m_p) in m/s
        norm = float(outdict["ZD_RZERO"]["VAL"] / cref) if "ZD_RZERO" in outdict else 1.0 / cref
        (dum,dume,gammaedpi,gammaedpieb) = ytools.calc_generic_normalized_gradient(fgeo,1.0,grad,jrr,norm,fgeoeb,gradeb,enforce_positive=False,default_der_err=0.0)

        outdict["PD_GAMMAEDPI"] = {"VAL": gammaedpi, "ERR": gammaedpieb}

    return outdict


def print_debug_QuaLiKiz_rotation(datadict):

    # Copy structure containing relevant QuaLiKiz data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    dtag = "TOR"
    if "PD_MACH"+dtag in outdict:
        with open(fdir+'Mach'+dtag.lower()+'.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_MACH"+dtag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_MACH"+dtag]["VAL"][ii]))
    if "PD_AU"+dtag in outdict:
        with open(fdir+'Au'+dtag.lower()+'.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_AU"+dtag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_AU"+dtag]["VAL"][ii]))
    dtag = "PAR"
    if "PD_MACH"+dtag in outdict:
        with open(fdir+'Mach'+dtag.lower()+'.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_MACH"+dtag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_MACH"+dtag]["VAL"][ii]))
    if "PD_AU"+dtag in outdict:
        with open(fdir+'Au'+dtag.lower()+'.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_AU"+dtag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_AU"+dtag]["VAL"][ii]))
    dtag = "PER"
    if "PD_MACH"+dtag in outdict:
        with open(fdir+'Mach'+dtag.lower()+'.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_MACH"+dtag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_MACH"+dtag]["VAL"][ii]))
    if "PD_AU"+dtag in outdict:
        with open(fdir+'Au'+dtag.lower()+'.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_AU"+dtag]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_AU"+dtag]["VAL"][ii]))
    if "PD_UPER" in outdict:
        with open(fdir+'vper.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_UPER"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_UPER"]["VAL"][ii]))
    if "PD_UDPI" in outdict:
        with open(fdir+'vgradpi.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_UDPI"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_UDPI"]["VAL"][ii]))
    if "PD_GAMMAE" in outdict:
        with open(fdir+'gammaE.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_GAMMAE"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_GAMMAE"]["VAL"][ii]))
    if "PD_GAMMAEDPI" in outdict:
        with open(fdir+'gammaE_gradpi.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_GAMMAEDPI"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_GAMMAEDPI"]["VAL"][ii]))


def calc_GENE_collisionality(twobj,newstruct=None,fdebug=False):

    # Structure used to collect relevant GENE data
    outdict = copy.deepcopy(newstruct) if isinstance(newstruct,dict) else dict()

    # Internal debugging options
    fdir = './debug/'
    if fdebug and not os.path.isdir(fdir):
        os.makedirs(fdir)

    xvec_in = twobj["PD"]["X"][""].copy()
    pddict = twobj["PD"].getPresentFields()

    # Collisionality, nu_star - not strictly needed as a GENE input but useful for pre-analysis
    if "N" in pddict and "E" in pddict["N"] and "T" in pddict and "E" in pddict["T"] and "Z" in pddict and "EFFP" in pddict["Z"] and "PD_Q" in outdict and "ZD_RZERO" in outdict and "ZD_RMIN" in outdict and "PD_X" in outdict:
        ee = 1.60217662e-19
        eom = 1.75882e11          # e / me
        epsoe = 5.5263494e7       # eps0 / e
        e2oh = 3.8768976e-5       # e^2 / h
        mk2 = 1.16196e-8          # me / eps0^2
        telim = 24.2235           # in eV, temperature boundary below which the quantum mechanical formulation should take over

        xr = copy.deepcopy(outdict["PD_X"]["VAL"])
        xr[xr < 1.0e-3] = 1.0e-2
        zfilt = np.all([twobj["PD"]["NE"][""] > 0.0,twobj["PD"]["TE"][""] > 0.0],axis=0)
        logcoul = np.zeros(xvec_in.shape)
        collei = np.zeros(xvec_in.shape)
        tbounce = np.zeros(xvec_in.shape)
        nustar = np.zeros(xvec_in.shape)
        nustareb = np.zeros(xvec_in.shape)
        if np.any(zfilt):
            logcoul[zfilt] = 15.2 - 0.5 * np.log(twobj["PD"]["NE"][""][zfilt] * 1.0e-20) + np.log(twobj["PD"]["TE"][""][zfilt] * 1.0e-3)
#            logcoul[zfilt] = 24.0 - np.log(np.sqrt(twobj["PD"]["NE"][""][zfilt] * 1.0e-6)) + np.log(twobj["PD"]["TE"][""][zfilt])         # equivalent to constant of 14.8
#            logcoul[zfilt] = 31.3 - np.log(np.sqrt(twobj["PD"]["NE"][""][zfilt])) + np.log(twobj["PD"]["TE"][""][zfilt])                  # equivalent but deals with larger value in ln(ne)
            collei[zfilt] = 914.7 * twobj["PD"]["ZEFF"][""][zfilt] * logcoul[zfilt] * twobj["PD"]["NE"][""][zfilt] * 1.0e-19 / np.power(twobj["PD"]["TE"][""][zfilt] * 1.0e-3,1.5)
            tbounce[zfilt] = outdict["PD_Q"]["VAL"][zfilt] * outdict["ZD_RZERO"]["VAL"] / (np.sqrt(eom * twobj["PD"]["TE"][""][zfilt]) * np.power(xr[zfilt] / outdict["ZD_RMIN"]["VAL"],1.5))
            nustar[zfilt] = collei[zfilt] * tbounce[zfilt]
#            nustar[zfilt] = 2.3031e-18 * logcoul[zfilt] * rzero * twobj["PD"]["NE"][""][zfilt] / np.power(twobj["PD"]["TE"][""][zfilt],2.0)

        outdict["PD_NUSTAR"] = {"VAL": nustar, "ERR": nustareb}

        if fdebug:
            print("gene - adapter.py: %s populated." % ("NUSTAR"))

    return outdict


def print_debug_GENE_collisionality(datadict):

    # Copy structure containing relevant GENE data
    outdict = copy.deepcopy(datadict) if isinstance(datadict,dict) else dict()

    # Create debug directory if not already existing
    fdir = './debug/'
    if not os.path.isdir(fdir):
        os.makedirs(fdir)

    if "PD_NUSTAR" in outdict:
        with open(fdir+'nustar.dat','w') as ff:
            for ii in np.arange(0,outdict["PD_NUSTAR"]["VAL"].size):
                ff.write("%20.8f\n" % (outdict["PD_NUSTAR"]["VAL"][ii]))


def calc_GENE_inputs(basedata,geometry_tag=None,use_bias=False,use_def=None,fdebug=False):
    """
    GENE-SPECIFIC FUNCTION
    Calculates the dimensionless parameters specifically
    needed by the GENE gyrokinetic code. This function
    will convert everything to the associated radial
    coordinate system of the requested GENE geometry,
    provided that the conversion vectors are available.

    :arg basedata: dict. Object containing standardized profile fits and dimensionless gyrokinetic parameters.

    :kwarg geometry_tag: str. Specifies the GENE magnetic geometry option for which to compute gyrokinetic parameters.

    :kwarg use_bias: bool. Flag to toggle calculation of GENE inputs using the correction coordinate systems, if present.

    :kwarg use_def: str. Optional string input which specifies which algorithms to use for certain calculations.

    :kwarg fdebug: bool. Optional flag to print progress statements to terminal for use in debugging.

    :returns: dict. Object with identical structure as input object, except with GENE inputs inserted in a standardized format.
    """
    twobj = None
    if isinstance(basedata,classes.EX2GKTimeWindow):
        twobj = copy.deepcopy(basedata)
    if not isinstance(use_def,str):
        use_def = None

    if isinstance(twobj,classes.EX2GKTimeWindow) and twobj.isFitted():

        # Housekeeping
        pddict = twobj["PD"].getPresentFields()
        numion = twobj["META"]["NUMI"]
        numimp = twobj["META"]["NUMIMP"]
        numz = twobj["META"]["NUMZ"]
        outdict = dict()
        dtags = []

        # Standardized radial coordinate for geometry specification
        if geometry_tag not in geotags:
            raise ValueError("Requested GENE geometry not yet implemented within adapter, aborting conversion to GENE values!")
        outdict["META_CODEGEOM"] = geometry_tag
        dtags.append("META_CODEGEOM")

        # Calculates nref, Tref, and cref, and sets scaling factors for density and temperature (MANDATORY!)
        outdict = calc_GENE_reference_values(twobj,newstruct=outdict,fdebug=fdebug)
        dtags.extend(["ZD_NSCALE","ZD_TSCALE","PD_NREF","PD_TREF","ZD_CREF"])
        if fdebug:
            print_debug_GENE_reference(outdict)

        # Calculates all coordinate representations used by GENE and associated Jacobians for derivative transformation (MANDATORY!)
        outdict = calc_GENE_radial_coordinates(twobj,newstruct=outdict,geometry_tag=geometry_tag,use_bias=use_bias,use_def=use_def,fdebug=fdebug)
        dtags.extend(["ZD_BO","ZD_RZERO","ZD_RMIN","PD_RO","PD_R","PD_TRPEPS"])
        if fdebug:
            print_debug_GENE_radial_coordinates(outdict)

        # Calculates B_zer, B_tor, and B_pol - required for calculation of magnetic rotation quantities
#        outdict = calc_QuaLiKiz_magnetic_fields(twobj,newstruct=outdict,fdebug=fdebug)
#        dtags.extend(["ZD_BZERO","PD_BTOR","PD_BPOL"])
#        if fdebug:
#            print_debug_QuaLiKiz_magnetic_fields(outdict)

        stag = "E"
        # Electron density and normalized gradient
        outdict = calc_GENE_species_density(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
        # Electron temperature and normalized gradient
        outdict = calc_GENE_species_temperature(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
        # Electron pressure and normalized gradient (alpha)
        outdict = calc_GENE_species_pressure(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
        dtags.extend(["PD_N"+stag,"PD_AN"+stag,"PD_T"+stag,"PD_AT"+stag,"PD_BETA"+stag,"PD_Z"+stag,"PD_AZ"+stag])

        for idx in np.arange(0,numion):
            stag = "I%d" % (idx+1)
            # Main ion density and normalized gradient - calculated in main program using Z_eff and impurity ion data if not directly measured
            outdict = calc_GENE_species_density(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Main ion temperature and normalized gradient - set equal to impurity ion temperature data in main program if not directly measured
            outdict = calc_GENE_species_temperature(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Main ion pressure and normalized gradient (alpha)
            outdict = calc_GENE_species_pressure(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Main ion charge and normalized gradient
            outdict = calc_GENE_species_charge(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            dtags.extend(["PD_N"+stag,"PD_AN"+stag,"PD_T"+stag,"PD_AT"+stag,"PD_BETA"+stag,"PD_Z"+stag,"PD_AZ"+stag])

        for idx in np.arange(0,numimp):
            stag = "IMP%d" % (idx+1)
            # Impurity ion density and normalized gradient
            outdict = calc_GENE_species_density(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Impurity ion temperature and normalized gradient
            outdict = calc_GENE_species_temperature(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Impurity ion pressure and normalized gradient (alpha)
            outdict = calc_GENE_species_pressure(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Impurity ion charge and normalized gradient - does not account for partially ionized states
            outdict = calc_GENE_species_charge(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            dtags.extend(["PD_N"+stag,"PD_AN"+stag,"PD_T"+stag,"PD_AT"+stag,"PD_BETA"+stag,"PD_Z"+stag,"PD_AZ"+stag])

        ### The EX2GK main program allows up to two assumed impurities
        ###     These are generally used to match experimental Z_eff if measured impurities are minority or trace species
        for idx in np.arange(0,numz):
            stag = "Z%d" % (idx+1)
            # Assumed impurity ion density and normalized gradient
            outdict = calc_GENE_species_density(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Assumed impurity ion temperature and normalized gradient
            outdict = calc_GENE_species_temperature(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Assumed ion pressure and normalized gradient (alpha)
            outdict = calc_GENE_species_pressure(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            # Assumed ion charge and normalized gradient - does not account for partially ionized states
            outdict = calc_GENE_species_charge(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            dtags.extend(["PD_N"+stag,"PD_AN"+stag,"PD_T"+stag,"PD_AT"+stag,"PD_BETA"+stag,"PD_Z"+stag,"PD_AZ"+stag])

        # Fast ion contributions - density typically negligible and temperature not included due to inherent non-Maxwellian distribution function
        sources = ["NBI","ICRH","ECRH","LH"]
        for src in sources:
            stag = "FI_"+src
            # Fast ion pressure and normalized gradient (alpha) - not sure if necessary but provided for convenience
            outdict = calc_GENE_species_pressure(twobj,newstruct=outdict,species_tag=stag,geometry_tag=geometry_tag,fdebug=fdebug)
            dtags.extend(["PD_BETA"+stag])

        if fdebug:
            print_debug_GENE_species(outdict,numion,numimp,numz)

        # Calculates q, smag, and alpha_MHD - requires pressure contributions from individual species calculations
        outdict = calc_GENE_equilibrium(twobj,newstruct=outdict,geometry_tag=geometry_tag,use_bias=False,fdebug=fdebug)
        dtags.extend(["PD_Q","PD_SMAG","PD_BETAEI","PD_BETATH","PD_BETATOT","PD_ALPHAEI","PD_ALPHATH","PD_ALPHATOT"])
        if fdebug:
            print_debug_GENE_equilibrium(outdict)

        # Calculates Machtor and Autor - also calculates utor and upol for magnetic rotation calculation
#        outdict = calc_QuaLiKiz_geometric_rotation(twobj,newstruct=outdict,fdebug=fdebug)
#        dtags.extend(["PD_UTOR","PD_UPOL","PD_MACHTOR","PD_AUTOR"])

        # Calculates Machpar and Aupar - also calculates uper for ExB shearing rate calculation
#        outdict = calc_QuaLiKiz_magnetic_rotation(twobj,newstruct=outdict,use_def=use_def,fdebug=fdebug)
#        dtags.extend(["PD_UPAR","PD_UPER","PD_UDPI","PD_MACHPAR","PD_AUPAR","PD_MACHPER","PD_AUPER"])

        # Calculates gammaE
#        outdict = calc_QuaLiKiz_ExB_shearing_rate(twobj,newstruct=outdict,fdebug=fdebug)
#        dtags.extend(["PD_UGEO","PD_GAMMAE","PD_GAMMAEDPI"])

#        if fdebug:
#            print_debug_QuaLiKiz_rotation(outdict)

        # Calculates nustar
        outdict = calc_GENE_collisionality(twobj,newstruct=outdict,fdebug=fdebug)
        dtags.extend(["PD_NUSTAR"])
        if fdebug:
            print_debug_GENE_collisionality(outdict)

        # Fill code-specific fields in EX2GKTimeWindow object
        ctag = "GENE"
        twobj["META"].setPostProcessingCodeMetaData("GENE",ctag,ctag+"_TRPEPS")
        taglist = [item for item in dtags if item in outdict]
        for dtag in taglist:
            if dtag in outdict:
                mm = re.match(r'^(.+?)_(.+)$',dtag)
                if mm and mm.group(1) == "ZD":
                    if "VAL" in outdict[dtag]:
                        twobj["ZD"].addData("OUT"+ctag+"_"+mm.group(2),outdict[dtag]["VAL"],outdict[dtag]["ERR"])
                elif mm and mm.group(1) == "PD":
                    if "VAL" in outdict[dtag]:
                        twobj["PD"].addProfileData("OUT"+ctag+"_"+mm.group(2),outdict[dtag]["VAL"],outdict[dtag]["ERR"])
                        if "DVAL" in outdict[dtag]:
                            twobj["PD"].addProfileDerivativeData("OUT"+ctag+"_"+mm.group(2),outdict[dtag]["DVAL"],outdict[dtag]["DERR"])
                elif mm and mm.group(1) == "META":
                    with warnings.catch_warnings():
                        warnings.simplefilter("ignore")
                        twobj["META"].addValue(mm.group(2),outdict[dtag])

    return twobj


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument

def clean_code_inputs(basedata):
    """
    GENE-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides a standard method to delete code-specific
    data generated by the output adapter. This allows for minimization
    of required storage space and reduces chance of clashes when
    re-processing data with the same or another adapter.

    :arg basedata: dict. Standardized object containing processed and fitted data, possibly post-processed by this adapter.

    :returns: dict. Object identical in structure to input object, except with all code-specific fields from this adapter removed.
    """
    outdict = None
    if isinstance(basedata,classes.EX2GKTimeWindow):
        outdict = copy.deepcopy(basedata)

    if isinstance(outdict,classes.EX2GKTimeWindow) and outdict.isProcessed():
        code = "GENE"
        ctag = "GENE"
        pdlist = outdict["PD"].getPresentFields('list')
        for item in pdlist:
            if item.endswith(ctag):
                del twobj["PD"][item]
        if re.match(r'^'+code+r'$',outdict["META"]["CODE"],flags=re.IGNORECASE):
            outdict["META"].resetPostProcessingCodeMetaData()

    return outdict


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def calc_code_inputs(basedata,sysopt=None,forceflag=False,biasflag=False,fdebug=False):
    """
    GENE-SPECIFIC FUNCTION (STANDARDIZED NAME)
    This function provides a single entry point into the output adapter
    scripts for various target codes. For ease of use, only this file
    should be imported into any user-defined script and only this
    function should be used to process the desired standardized data.

    :arg basedata: dict. Standardized object containing processed and fitted data from EX2GK main program.

    :kwarg sysopt: str. Name of radial specification to be used within the adapter, not always applicable.

    :kwarg forceflag: bool. Flag to force the calculation of code-specific quantities, even if other code-specific quantities are present.

    :kwarg biasflag: bool. Flag to toggle calculation of code inputs using the correction coordinate systems, if applicable and data is present.

    :kwarg fdebug: bool. Optional flag to print progress statements to terminal for use in debugging.

    :returns: dict. Object identical in structure to input object, except containing code-specific quantities if appropriate data is present.
    """
    stddict = None
    if isinstance(basedata,classes.EX2GKTimeWindow):
        stddict = copy.deepcopy(basedata)

    outdata = None
    if stddict is not None:
        goflag = True
        if stddict["META"]["CODE"] is not None:
            if forceflag:
                print("This file has already been processed for use in %s. Deleting older values and re-processing." % (stddict["META"]["CODE"]))
                stddict = clean_code_inputs(stddict)
                goflag = True
            else:
                print("This file has already been processed for use in %s. Aborting..." % (stddict["META"]["CODE"]))
                goflag = False
        vardef = None
        if stddict["FLAG"].checkFlag("JETTO"):
            vardef = 'jetto'
        gtag = sysopt.lower() if isinstance(sysopt,str) else None
        outdata = calc_GENE_inputs(stddict,geometry_tag=gtag,use_bias=biasflag,use_def=vardef,fdebug=fdebug) if goflag else copy.deepcopy(stddict)

    return outdata
