#!/usr/bin/env python3
# Basic GENE input file generator - written by Garud Snoep, copied 04/11/2019

import sys
import os
import inspect
import re
import copy
import warnings
from operator import itemgetter
from collections import OrderedDict

import numpy as np
from scipy.interpolate import interp1d
import pickle
import subprocess

from EX2GK.tools.general import proctools as ptools
from EX2GK.code_adapters.gene import adapter

np_types = (np.int8,np.int16,np.int32,np.int64,np.uint8,np.uint16,np.uint32,np.uint64,np.float16,np.float32,np.float64)

def extract_value(rvec,dvec,rval,pflag=False):
    """
    Linear interpolates a given radial profile to extract values at requested radii.

    :arg rvec: array. Radial points of profile.

    :arg dvec: array. Value of profile.

    :arg rval: array. Radial points from which to extract profile values.

    :kwarg pflag: bool. Set True to strictly enforce positive return values.

    :returns: array. Profile values evaluated at requested radial points.
    """
    ival = None
    if isinstance(rval,(int,float,np_types)):
        ival = np.array([rval])
    elif isinstance(rval,(list,tuple,np.ndarray)):
        ival = np.array(rval).flatten()
    dval = None
    if isinstance(rvec,(tuple,list,np.ndarray)) and isinstance(dvec,(tuple,list,np.ndarray)) and isinstance(ival,np.ndarray):
        rrvec = np.array(rvec).squeeze()
        ddvec = np.array(dvec).squeeze()
        if rrvec.size == ddvec.size:
            nfilt = np.any([np.isfinite(rrvec),np.isfinite(ddvec)],axis=0)
            if np.count_nonzero(nfilt) > 1:
                dfunc = interp1d(rrvec[nfilt],ddvec[nfilt],bounds_error=False,fill_value='extrapolate')
                dval = dfunc(ival)
                dfilt = (dval < 0.0)
                if np.any(dfilt) and pflag:
                    dval[dfilt] = 0.0
    return dval


def extract_species_data(shotdata,stype,dval,alt=None):
    """
    Extracts required data to define a particle species within GENE, provided
    that the necessary data is present. Linearly interpolates any radial profiles.

    .. note:: Requires GENE output code adapter to be run on EX2GK database entry!

    :arg shotdata: dict. EX2GK database entry for a given time window.

    :arg stype: str. EX2GK identification string indicating which species to extract from database entry.

    :arg dval: array. Radial points from which to extract profile values.

    :kwarg alt: str. EX2GK identification string indicating desired back-up species to use if primary is unavailable.

    :returns: dict. Extracted data with standardized field names for use within function in this file.
    """
    SD = None
    ststr = None
    ipt = None
    atstr = None
    if isinstance(shotdata,dict):
        SD = shotdata
    if isinstance(dval,(float,int,np_types)):
        ipt = np.array([dval])
    elif isinstance(dval,(list,tuple,np.ndarray)):
        ipt = np.array(dval).flatten()
    if isinstance(stype,str):
        ststr = stype.upper()
    if isinstance(alt,str):
        atstr = alt.upper()

    PD = None
    if SD is not None and ststr is not None and ipt is not None:
        PD = dict()
        if "META_CODETAG" in SD and SD["META_CODETAG"] == "GENE":
            rvec = SD["PD_X"]
            ctag = "PD_OUT" + SD["META_CODETAG"] + "_"
            if "META_Z"+ststr in SD and isinstance(SD["META_Z"+ststr],(float,int,np_types)):
                aa = SD["META_A"+ststr] if "META_A"+ststr in SD and isinstance(SD["META_A"+ststr],(float,int,np_types)) else None
                (PD["NAME"],PD["A"],PD["Z"]) = ptools.define_ion_species(z=SD["META_Z"+ststr],a=aa)
            elif ststr == "E":
                (PD["NAME"],PD["A"],PD["Z"]) = ptools.define_ion_species(z=-1)
            elif ststr == "I":
                ststr = "I1"
                (PD["NAME"],PD["A"],PD["Z"]) = ptools.define_ion_species(z=1,a=2)
            else:
                PD = None

            if isinstance(PD,dict):
                PD["N"] = np.zeros(ipt.shape)
                PD["T"] = np.zeros(ipt.shape)
                PD["AN"] = np.full(ipt.shape,np.NaN)
                PD["AT"] = np.full(ipt.shape,np.NaN)
                PD["NEB"] = np.zeros(ipt.shape)
                PD["TEB"] = np.zeros(ipt.shape)
                PD["ANEB"] = np.full(ipt.shape,np.NaN)
                PD["ATEB"] = np.full(ipt.shape,np.NaN)

                if ctag+"N"+ststr in SD:
                    PD["N"] = extract_value(rvec,SD[ctag+"N"+ststr],ipt,pflag=True)
                elif atstr is not None and ctag+"N"+atstr in SD:
                    PD["N"] = extract_value(rvec,SD[ctag+"N"+atstr],ipt,pflag=True)
                if ctag+"T"+ststr in SD:
                    PD["T"] = extract_value(rvec,SD[ctag+"T"+ststr],ipt,pflag=True)
                elif atstr is not None and ctag+"T"+atstr in SD:
                    PD["T"] = extract_value(rvec,SD[ctag+"T"+atstr],ipt,pflag=True)
                if ctag+"AN"+ststr in SD:
                    PD["AN"] = extract_value(rvec,SD[ctag+"AN"+ststr],ipt)
                elif atstr is not None and ctag+"AN"+atstr in SD:
                    PD["AN"] = extract_value(rvec,SD[ctag+"AN"+atstr],ipt)
                if ctag+"AT"+ststr in SD:
                    PD["AT"] = extract_value(rvec,SD[ctag+"AT"+ststr],ipt)
                elif atstr is not None and ctag+"AT"+atstr in SD:
                    PD["AT"] = extract_value(rvec,SD[ctag+"AT"+atstr],ipt)
                if ctag+"N"+ststr+"EB" in SD:
                    PD["NEB"] = extract_value(rvec,SD[ctag+"N"+ststr+"EB"],ipt)
                elif atstr is not None and ctag+"N"+atstr+"EB" in SD:
                    PD["NEB"] = extract_value(rvec,SD[ctag+"N"+atstr+"EB"],ipt)
                if ctag+"T"+ststr+"EB" in SD:
                    PD["TEB"] = extract_value(rvec,SD[ctag+"T"+ststr+"EB"],ipt)
                elif atstr is not None and ctag+"T"+atstr+"EB" in SD:
                    PD["TEB"] = extract_value(rvec,SD[ctag+"T"+atstr+"EB"],ipt)
                if ctag+"AN"+ststr+"EB" in SD:
                    PD["ANEB"] = extract_value(rvec,SD[ctag+"AN"+ststr+"EB"],ipt)
                elif atstr is not None and ctag+"AN"+atstr+"EB" in SD:
                    PD["ANEB"] = extract_value(rvec,SD[ctag+"AN"+atstr+"EB"],ipt)
                if ctag+"AT"+ststr+"EB" in SD:
                    PD["ATEB"] = extract_value(rvec,SD[ctag+"AT"+ststr+"EB"],ipt)
                elif atstr is not None and ctag+"AT"+atstr+"EB" in SD:
                    PD["ATEB"] = extract_value(rvec,SD[ctag+"AT"+atstr+"EB"],ipt)

                if ctag+"Z"+ststr in SD:
                    PD["ZZ"] = extract_value(rvec,SD[ctag+"Z"+ststr],ipt)
                elif atstr is not None and ctag+"Z"+atstr in SD:
                    PD["ZZ"] = extract_value(rvec,SD[ctag+"Z"+atstr],ipt)
                if ctag+"Z"+ststr+"EB" in SD:
                    PD["ZZEB"] = extract_value(rvec,SD[ctag+"Z"+ststr+"EB"],ipt)
                elif atstr is not None and ctag+"Z"+atstr+"EB" in SD:
                    PD["ZZEB"] = extract_value(rvec,SD[ctag+"Z"+atstr+"EB"],ipt)
                if ctag+"AZ"+ststr in SD:
                    PD["AZZ"] = extract_value(rvec,SD[ctag+"AZ"+ststr],ipt)
                elif atstr is not None and ctag+"AZ"+atstr in SD:
                    PD["AZZ"] = extract_value(rvec,SD[ctag+"AZ"+atstr],ipt)
                if ctag+"AZ"+ststr+"EB" in SD:
                    PD["AZZEB"] = extract_value(rvec,SD[ctag+"AZ"+ststr+"EB"],ipt)
                elif atstr is not None and ctag+"AZ"+atstr+"EB" in SD:
                    PD["AZZEB"] = extract_value(rvec,SD[ctag+"AZ"+atstr+"EB"],ipt)

            else:
                PD = dict()
                PD["N"] = np.zeros(ipt.shape)
                PD["T"] = np.zeros(ipt.shape)
                PD["AN"] = np.full(ipt.shape,np.NaN)
                PD["AT"] = np.full(ipt.shape,np.NaN)
                PD["NEB"] = np.zeros(ipt.shape)
                PD["TEB"] = np.zeros(ipt.shape)
                PD["ANEB"] = np.full(ipt.shape,np.NaN)
                PD["ATEB"] = np.full(ipt.shape,np.NaN)

    return PD


def extract_data_point(shotdata,rvalue,rcoord):
    """
    Extracts required data to define a GENE run in its entirety, defining defaults
    if the necessary data is not present. Linearly interpolates any radial profiles.

    .. note:: Requires GENE output code adapter to be run on EX2GK database entry!

    :arg shotdata: dict. EX2GK database entry for a given time window.

    :arg rvalue: array. Radial point from which to extract profile values.

    :arg rcoord: str. Radial coordinate corresponding to input radial points, used to convert if necessary.

    :returns: dict. Extracted data with standardized field names for use within function in this file.
    """
    SD = None
    urval = None
    upt = None
    ucst = None
    if isinstance(shotdata,dict):
        SD = shotdata
    if isinstance(rvalue,(float,int,np_types)):
        upt = np.array([rvalue])
    elif isinstance(rvalue,(list,tuple,np.ndarray)):
        upt = np.array(rvalue).flatten()
    if isinstance(rcoord,str):
        ucst = rcoord

    PD = None
    if isinstance(SD,dict) and upt is not None and ucst is not None:

        PD = dict()

        # Obtain fit coordinate system
        (ucs,xlabel,xunit,dc,dl) = ptools.define_coordinate_system(ucst)
        rvec = SD["PD_X"].copy()
        rtag = SD["META_CSOP"] + SD["META_CSO"]
        utag = SD["META_CSOP"] + ucs
        ctag = "OUTGENE_"
        PD["GEOMETRY"] = SD["META_CODEGEOM"] if "META_CODEGEOM" in SD else None
        if PD["GEOMETRY"] is None:
            raise ValueError("Unexpected error in post-processed data, aborting GENE input generation!")
        PD["META"] = "%s_%d_%d_%d" % (SD["META_DEVICE"],SD["META_SHOT"],int(SD["META_T1"]*10000),int(SD["META_T2"]*10000))

        # Geometry
        ipt = itemgetter(0)(ptools.convert_base_coords(upt,SD["CD_"+utag+"_V"],SD["CD_"+rtag+"_V"],None,None,None))
        PD["RMAJOR_LCFS"] = SD["ZD_"+ctag+"RZERO"] if "ZD_"+ctag+"RZERO" in SD and SD["ZD_"+ctag+"RZERO"] is not None else 2.96
        PD["RMINOR_LCFS"] = SD["ZD_"+ctag+"RMIN"] if "ZD_"+ctag+"RMIN" in SD and SD["ZD_"+ctag+"RMIN"] is not None else 1.0
        PD["RMAJOR_POINT"] = extract_value(rvec,SD["PD_"+ctag+"RO"],ipt) if "PD_"+ctag+"RO" in SD and SD["PD_"+ctag+"RO"] is not None else np.full(ipt.shape,PD["RMAJOR_LCFS"])
        PD["RMINORN_POINT"] = extract_value(rvec,SD["PD_"+ctag+"TRPEPS"],ipt) if "PD_"+ctag+"TRPEPS" in SD and SD["PD_"+ctag+"TRPEPS"] is not None else np.full(ipt.shape,0.5)

        # Reference values
        PD["TREF"] = extract_value(rvec,SD["PD_"+ctag+"TREF"],ipt,pflag=True)
        PD["NREF"] = extract_value(rvec,SD["PD_"+ctag+"NREF"],ipt,pflag=True)
        sqemp = np.sqrt(1.60217662e-19 / 1.6726219e-27)
        PD["OMEGAREF"] = np.zeros(rvec.shape)

        # Species data
        (PD["AZ1"],PD["ZZ1"]) = itemgetter(1,2)(ptools.define_ion_species(short_name=SD["META_MATWALL"]))
        if PD["AZ1"] is None or PD["ZZ1"] is None:
            PD["AZ1"] = 12.0
            PD["ZZ1"] = 6.0
        (PD["AZ2"],PD["ZZ2"]) = itemgetter(1,2)(ptools.define_ion_species(short_name=SD["META_MATSTRIKE"]))
        if PD["AZ2"] is None or PD["ZZ2"] is None or PD["ZZ2"] <= 18.0 or PD["ZZ2"] >= 54.0:
            PD["AZ2"] = 58.0
            PD["ZZ2"] = 28.0
        pcnlim = 0.02
        zlimest = (1.0 + pcnlim * PD["ZZ1"] * PD["ZZ1"]) / (1.0 + pcnlim * PD["ZZ1"])
        PD["FZEFF"] = SD["ZD_FZEFF"] if "ZD_FZEFF" in SD and SD["ZD_FZEFF"] is not None else 1.5
        PD["FZEFFEB"] = SD["ZD_FZEFFEB"] if "ZD_FZEFFEB" in SD and SD["ZD_FZEFFEB"] is not None else 0.2
        PD["ZLIM1"] = SD["META_ZLIM1"] if "META_ZLIM1" in SD and SD["META_ZLIM1"] is not None else np.full(ipt.shape,zlimest)
        PD["ZEFF"] = extract_value(rvec,SD["PD_ZEFF"],ipt) if "PD_ZEFF" in SD and SD["PD_ZEFF"] is not None else np.full(ipt.shape,PD["FZEFF"])
        PD["ZEFFEB"] = extract_value(rvec,SD["PD_ZEFFEB"],ipt) if "PD_ZEFFEB" in SD and SD["PD_ZEFFEB"] is not None else np.full(ipt.shape,PD["FZEFFEB"])
        PD["DZEFF"] = extract_value(rvec,SD["PD_ZEFFGRAD"],ipt) if "PD_ZEFFGRAD" in SD and SD["PD_ZEFFGRAD"] is not None else np.zeros(ipt.shape)
        PD["DZEFFEB"] = extract_value(rvec,SD["PD_ZEFFGRADEB"],ipt) if "PD_ZEFFGRADEB" in SD and SD["PD_ZEFFGRADEB"] is not None else np.zeros(ipt.shape)
        PD["ELEC"] = extract_species_data(SD,"e",ipt)                  # Electrons
        PD["NMION"] = int(SD["META_NUMI"]) if "META_NUMI" in SD and isinstance(SD["META_NUMI"],(int,float)) else 1
        for ii in np.arange(0,PD["NMION"]):
            itag = "%d" % (ii+1)
            PD["MION"+itag] = extract_species_data(SD,"i"+itag,ipt)    # Main ions
        PD["NIION"] = int(SD["META_NUMIMP"]) if "META_NUMIMP" in SD and isinstance(SD["META_NUMIMP"],(int,float)) else 0
        for ii in np.arange(0,PD["NIION"]):
            itag = "%d" % (ii+1)
            PD["IION"+itag] = extract_species_data(SD,"imp"+itag,ipt)
        PD["TION1"] = extract_species_data(SD,"z1",ipt)                # Primary trace impurity ions for quasineutrality
        PD["TION2"] = extract_species_data(SD,"z2",ipt)                # Secondary trace impurity ions for quasineutrality

        fdebug = False
        if fdebug:
            ti1z = np.power(PD["TION1"]["Z"],2.0) * PD["TION1"]["N"]
            ti2z = np.power(PD["TION2"]["Z"],2.0) * PD["TION2"]["N"]
            print(PD["TION1"]["Z"],ti1z)
            print(PD["TION2"]["Z"],ti2z)
            uzeff = ti1z + ti2z
            for ii in np.arange(0,PD["NMION"]):
                itag = "%d" % (ii+1)
                miz = np.power(PD["MION"+itag]["Z"],2.0) * PD["MION"+itag]["N"]
                print(PD["MION"+itag]["Z"],miz)
                uzeff = uzeff + miz
            for ii in np.arange(0,PD["NIION"]):
                itag = "%d" % (ii+1)
                iiz = np.power(PD["IION"+itag]["Z"],2.0) * PD["IION"+itag]["N"]
                print(PD["IION"+itag]["Z"],iiz)
                uzeff = uzeff + iiz

        # Magnetic quantities
        PD["BREF"] = float(np.abs(SD["ZD_"+ctag+"BO"])) if "ZD_"+ctag+"BO" in SD and SD["ZD_"+ctag+"BO"] is not None else 2.5
        PD["Q"] = extract_value(rvec,SD["PD_"+ctag+"Q"],ipt) if "PD_"+ctag+"Q" in SD and SD["PD_"+ctag+"Q"] is not None else np.full(ipt.shape,1.5)
        PD["QEB"] = extract_value(rvec,SD["PD_"+ctag+"QEB"],ipt) if "PD_"+ctag+"QEB" in SD and SD["PD_"+ctag+"QEB"] is not None else np.full(ipt.shape,0.5)
        PD["SMAG"] = extract_value(rvec,SD["PD_"+ctag+"SMAG"],ipt) if "PD_"+ctag+"SMAG" in SD and SD["PD_"+ctag+"SMAG"] is not None else np.zeros(ipt.shape)
        PD["SMAGEB"] = extract_value(rvec,SD["PD_"+ctag+"SMAGEB"],ipt) if "PD_"+ctag+"SMAGEB" in SD and SD["PD_"+ctag+"SMAGEB"] is not None else np.zeros(ipt.shape)

        # Rotation quantities
#        PD["MTOR"] = extract_value(rvec,SD["PD_"+ctag+"MACHTOR"],ipt) if "PD_"+ctag+"MACHTOR" in SD and SD["PD_"+ctag+"MACHTOR"] is not None else np.zeros(ipt.shape)
#        PD["MTOREB"] = extract_value(rvec,SD["PD_"+ctag+"MACHTOREB"],ipt) if "PD_"+ctag+"MACHTOREB" in SD and SD["PD_"+ctag+"MACHTOREB"] is not None else np.zeros(ipt.shape)
#        PD["AUTOR"] = extract_value(rvec,SD["PD_"+ctag+"AUTOR"],ipt) if "PD_"+ctag+"AUTOR" in SD and SD["PD_"+ctag+"AUTOR"] is not None else np.zeros(ipt.shape)
#        PD["AUTOREB"] = extract_value(rvec,SD["PD_"+ctag+"AUTOREB"],ipt) if "PD_"+ctag+"AUTOREB" in SD and SD["PD_"+ctag+"AUTOREB"] is not None else np.zeros(ipt.shape)
#        PD["MPAR"] = extract_value(rvec,SD["PD_"+ctag+"MACHPAR"],ipt) if "PD_"+ctag+"MACHPAR" in SD and SD["PD_"+ctag+"MACHPAR"] is not None else None
#        PD["MPAREB"] = extract_value(rvec,SD["PD_"+ctag+"MACHPAREB"],ipt) if "PD_"+ctag+"MACHPAREB" in SD and SD["PD_"+ctag+"MACHPAREB"] is not None else np.zeros(ipt.shape)
#        PD["AUPAR"] = extract_value(rvec,SD["PD_"+ctag+"AUPAR"],ipt) if "PD_"+ctag+"AUPAR" in SD and SD["PD_"+ctag+"AUPAR"] is not None else None
#        PD["AUPAREB"] = extract_value(rvec,SD["PD_"+ctag+"AUPAREB"],ipt) if "PD_"+ctag+"AUPAREB" in SD and SD["PD_"+ctag+"AUPAREB"] is not None else np.zeros(ipt.shape)
        # This gammaE_rot term is normalized by Rmin, not Rzero - should it still be used?
#        PD["GAMMAEROT"] = -PD["AUTOR"] * PD["RMINORN_POINT"] * PD["RMINOR_LCFS"] / (PD["RMAJOR_POINT"] * PD["Q"])  # Based on circular geometry, pure toroidal rotation, neglects grad p term
#        PD["GAMMAEROTEB"] = (PD["RMINORN_POINT"] * PD["RMINOR_LCFS"] / (PD["RMAJOR_POINT"] * PD["Q"])) * np.sqrt(np.power(PD["AUTOREB"],2.0) + np.power(PD["AUTOR"] * PD["QEB"] / PD["Q"],2.0))
#        PD["GAMMAE"] = extract_value(rvec,SD["PD_"+ctag+"GAMMAE"],ipt) if "PD_"+ctag+"GAMMAE" in SD and SD["PD_"+ctag+"GAMMAE"] is not None else np.zeros(ipt.shape)
#        PD["GAMMAEEB"] = extract_value(rvec,SD["PD_"+ctag+"GAMMAEEB"],ipt) if "PD_"+ctag+"GAMMAEEB" in SD and SD["PD_"+ctag+"GAMMAEEB"] is not None else np.zeros(ipt.shape)

        # Kinetic quantities
        if PD["GEOMETRY"] == "s_alpha":
            PD["ALPHA"] = extract_value(rvec,SD["PD_"+ctag+"ALPHATH"],ipt) if "PD_"+ctag+"ALPHATH" in SD and SD["PD_"+ctag+"ALPHATH"] is not None else np.zeros(ipt.shape)
            PD["ALPHAEB"] = extract_value(rvec,SD["PD_"+ctag+"ALPHATHEB"],ipt) if "PD_"+ctag+"ALPHATHEB" in SD and SD["PD_"+ctag+"ALPHATHEB"] is not None else np.zeros(ipt.shape)

    return PD


def get_GENE_input_data(twdata,rho,make_input=False,use_imp_ions=False,solver_type=None,add_meta=False,run_directory=None,debug_flag=False):
    """
    Generates a rudimentary GENE inputs from a given EX2GK database
    entry. Linearly interpolates any radial profiles and assumes that
    requested radial points are provided in normalized rho toroidal.

    :arg twdata: dict. EX2GK database entry for a given time window.

    :arg rho: float. Radial point on which to generate GENE input parameters.

    :kwarg make_input: bool. Specify generation of GENE input file, not generated by default to allow stitching of multiple EX2GK entries.

    :kwarg use_imp_ions: bool. Specify use of impurity ions exactly as specified in EX2GK database entry.

    :kwarg solver_type: str. Specify solver to place inside generated GENE input file, if requested.

    :kwarg add_meta: bool. Automatically write metadata tag based on metadata in database entry. (Not yet implemented)

    :kwarg run_directory: str. Optional target run directory to place within the GENE input file, if generated.

    :kwarg debug_flag: bool. Enables console printing of common debug statements.

    :returns: (dict,dict). Dictionary containing all relevant GENE input parameters of all requested radial slices / first radial slice.
    """
    data = None
    here = None
    rot_flag = 1
    if isinstance(twdata,dict):
        data = twdata
    if isinstance(rho,(float,int,np_types)):
        here = np.array([rho])
    elif isinstance(rho,(list,tuple,np.ndarray)):
        here = np.array(rho).flatten()
    if here is not None:
        vfilt = np.all([here >= 0.0,here <= 1.0],axis=0)
        here = here[vfilt] if np.any(vfilt) else None
    sol = "IV"
    if isinstance(solver_type,str) and sol in ["IV","EV","EV_mfn","EV_harmonic"]:
        sol = solver_type
        if sol == "EV":
            sol = "EV_harmonic"

    outdict = None
    namelists = None
    ionorder = []
    if data is not None and here is not None:

        XD = extract_data_point(data,here,"RHOTORN")

        if XD is not None:

            outdict = OrderedDict()

            # Reference values
            outdict["Tref"] = XD["TREF"].copy()
            outdict["nref"] = XD["NREF"].copy()
            outdict["Bref"] = np.full(here.shape,XD["BREF"])
            outdict["Lref"] = np.full(here.shape,XD["RMAJOR_LCFS"])
            outdict["mref"] = np.full(here.shape,XD["MION1"]["A"]) if "MION1" in XD else np.ones(here.shape)
            outdict["omegatorref"] = XD["OMEGAREF"].copy()

            # Geometric quantities
            outdict["rho"] = here.copy()
            outdict["trpeps"] = XD["RMINORN_POINT"].copy()
            outdict["Ro"] = XD["RMAJOR_POINT"].copy()
            outdict["Rmin"] = np.full(here.shape,XD["RMINOR_LCFS"])
            outdict["R0"] = np.full(here.shape,XD["RMAJOR_LCFS"])

            # Magnetic quantities
            outdict["magn_geometry"] = XD["GEOMETRY"]
            outdict["q"] = XD["Q"].copy()
            outdict["smag"] = XD["SMAG"].copy()
            outdict["error_q"] = XD["QEB"].copy()
            outdict["error_smag"] = XD["SMAGEB"].copy()

            # Kinetic quantities
            if outdict["magn_geometry"] == "s_alpha":
                outdict["alpha"] = XD["ALPHA"].copy()
                outdict["error_alpha"] = XD["ALPHAEB"].copy()

            # Electron
            outdict["Ae"] = np.full(here.shape,XD["ELEC"]["A"])
            outdict["Te"] = XD["ELEC"]["T"].copy()
            outdict["ne"] = XD["ELEC"]["N"].copy()
            outdict["Ate"] = XD["ELEC"]["AT"].copy()
            outdict["Ane"] = XD["ELEC"]["AN"].copy()
            outdict["error_Te"] = XD["ELEC"]["TEB"].copy()
            outdict["error_ne"] = XD["ELEC"]["NEB"].copy()
            outdict["error_Ate"] = XD["ELEC"]["ATEB"].copy()
            outdict["error_Ane"] = XD["ELEC"]["ANEB"].copy()

            # Discern from impurity ion species
            impdata_present = 0
            numlabel = 1
            ltag = "%d" % (numlabel)
            while "IION"+ltag in XD and XD["IION"+ltag] is not None:
                itag = "IION"+ltag
                zvec = np.full(here.shape,XD[itag]["Z"]) if "ZZ" not in XD[itag] else XD[itag]["ZZ"].copy()
                zcomp = XD["ZEFF"] - (1.0 + XD[itag]["N"] * zvec * zvec) / (1.0 + XD[itag]["N"] * zvec)
                if np.nanmean(zvec) <= 10.1 and np.nanmean(XD[itag]["N"] * zvec) > 4.0e-3 and not np.any(zcomp < 0.0):
                    impdata_present = numlabel
                numlabel = numlabel + 1
                ltag = "%d" % (numlabel)

            numions = 0
            if use_imp_ions:

                numlabel = 1
                ntag = "%d" % (numions)
                ltag = "%d" % (numlabel)
                while "MION"+ltag in XD and XD["MION"+ltag] is not None:
                    itag = "MION"+ltag
                    outdict["Ai"+ntag] = np.full(here.shape,XD[itag]["A"])
                    outdict["Zi"+ntag] = np.full(here.shape,XD[itag]["Z"]) if "ZZ" not in XD[itag] else XD[itag]["ZZ"].copy()
                    outdict["Ti"+ntag] = XD[itag]["T"].copy()
                    outdict["ni"+ntag] = XD[itag]["N"].copy()
                    outdict["Ati"+ntag] = XD[itag]["AT"].copy()
                    outdict["Ani"+ntag] = XD[itag]["AN"].copy()
                    outdict["error_Ti"+ntag] = XD[itag]["TEB"].copy()
                    outdict["error_ni"+ntag] = XD[itag]["NEB"].copy()
                    outdict["error_Ati"+ntag] = XD[itag]["ATEB"].copy()
                    outdict["error_Ani"+ntag] = XD[itag]["ANEB"].copy()
                    numions = numions + 1
                    numlabel = numlabel + 1
                    ntag = "%d" % (numions)
                    ltag = "%d" % (numlabel)
                    ionorder.append(itag)

                numlabel = 1
                ntag = "%d" % (numions)
                ltag = "%d" % (numlabel)
                while "IION"+ltag in XD and XD["IION"+ltag] is not None:
                    itag = "IION"+ltag
                    outdict["Ai"+ntag] = np.full(here.shape,XD[itag]["A"])
                    outdict["Zi"+ntag] = np.full(here.shape,XD[itag]["Z"]) if "ZZ" not in XD[itag] else XD[itag]["ZZ"].copy()
                    outdict["Ti"+ntag] = XD[itag]["T"].copy()
                    outdict["ni"+ntag] = XD[itag]["N"].copy()
                    outdict["Ati"+ntag] = XD[itag]["AT"].copy()
                    outdict["Ani"+ntag] = XD[itag]["AN"].copy()
                    outdict["error_Ti"+ntag] = XD[itag]["TEB"].copy()
                    outdict["error_ni"+ntag] = XD[itag]["NEB"].copy()
                    outdict["error_Ati"+ntag] = XD[itag]["ATEB"].copy()
                    outdict["error_Ani"+ntag] = XD[itag]["ANEB"].copy()
                    numions = numions + 1
                    numlabel = numlabel + 1
                    ntag = "%d" % (numions)
                    ltag = "%d" % (numlabel)
                    ionorder.append(itag)

            elif impdata_present > 0:
                numions = 3
                itag = "%d" % (impdata_present)
                outdict["Ai0"] = np.full(here.shape,XD["MION1"]["A"])
                outdict["Zi0"] = np.full(here.shape,XD["MION1"]["Z"]) if "ZZ" not in XD["MION1"] else XD["MION1"]["ZZ"].copy()
                outdict["Ai1"] = np.full(here.shape,XD["IION"+itag]["A"])
                outdict["Zi1"] = np.full(here.shape,XD["IION"+itag]["Z"])
                outdict["Ai2"] = np.full(here.shape,XD["AZ2"])
                outdict["Zi2"] = np.full(here.shape,XD["ZZ2"])
                reft = XD["MION1"]["T"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["T"].copy()
                refat = XD["MION1"]["AT"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["AT"].copy()

                # Redefine to flat effective charge
                zeff = np.full(here.shape,XD["FZEFF"]) if "FZEFF" in XD and XD["FZEFF"] is not None else np.full(here.shape,np.nanmean(XD["ZEFF"]))
                zeffeb = np.full(here.shape,XD["FZEFFEB"]) if "FZEFFEB" in XD and XD["FZEFFEB"] is not None else np.full(here.shape,np.nanmean(XD["FZEFFEB"]))
                dzeff = np.zeros(here.shape)
                dzeffeb = np.zeros(here.shape)

                # Light impurity ion - Z <= 10
                ztag = "IION"+itag
                outdict["Ti1"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni1"] = XD[ztag]["N"].copy()
                outdict["Ati1"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani1"] = XD[ztag]["AN"].copy()
                outdict["error_Ti1"] = XD[ztag]["TEB"].copy()
                outdict["error_ni1"] = XD[ztag]["NEB"].copy()
                outdict["error_Ati1"] = XD[ztag]["ATEB"].copy()
                outdict["error_Ani1"] = XD[ztag]["ANEB"].copy()

                # Main plasma ion - hydrogenic
                ztag = "MION1"
                outdict["Ti0"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni0"] = ((outdict["Zi2"] - zeff) - outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["Zi2"] - 1.0)
                outdict["Ati0"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani0"] = (outdict["Ane"] * (outdict["Zi2"] - zeff) - outdict["R0"] * dzeff - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["ni0"] * (outdict["Zi2"] - 1.0))
                outdict["error_Ti0"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni0"] = np.sqrt(np.power((outdict["Zi2"] - zeff) * outdict["error_ne"] / outdict["ne"],2.0) + np.power(zeffeb,2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_ni0"] = np.sqrt(np.power(zeffeb,2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_Ati0"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ane"] * (outdict["Zi2"] - XD["ZEFF"]),2.0) + np.power(outdict["Ane"] * zeffeb,2.0) + np.power(outdict["R0"] * dzeffeb,2.0) + np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"],2.0)) / outdict["ni0"]
                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"],2.0)) / outdict["ni0"]

                # Heavy impurity ion - Z >= 26
                ztag = "TION2"
                outdict["Ti2"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni2"] = (1.0 - outdict["ni0"] * outdict["Zi0"] - outdict["ni1"] * outdict["Zi1"]) / outdict["Zi2"]
                outdict["Ati2"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["AT"]) > 0.0 else refat.copy()
                outdict["Ani2"] = (outdict["Ane"] - outdict["Ani0"] * outdict["ni0"] * outdict["Zi0"] - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"]) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ti2"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ne"] / outdict["ne"],2.0) + np.power(outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"],2.0)) / outdict["Zi2"]
                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"],2.0)) / outdict["Zi2"]
                outdict["error_Ati2"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ane"],2.0) + np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"],2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"],2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"],2.0) + np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"],2.0)) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"],2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"],2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"],2.0) + np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"],2.0)) / (outdict["ni2"] * outdict["Zi2"])

                kevc = 1.60217662e3           # (e * 10^19 * 10^3)  -- places p in N m^-2
                betac = 2.51327412e-6         # (2 mu_0)            -- since p is already given in N m^-2
                dpres = kevc * outdict["ne"] * (outdict["Te"] * (outdict["Ane"] + outdict["Ate"]) + \
                                                outdict["ni0"] * outdict["Ti0"] * (outdict["Ani0"] + outdict["Ati0"]) + \
                                                outdict["ni1"] * outdict["Ti1"] * (outdict["Ani1"] + outdict["Ati1"]) + \
                                                outdict["ni2"] * outdict["Ti2"] * (outdict["Ani2"] + outdict["Ati2"]))
                dpreseb = kevc * outdict["ne"] * np.sqrt(np.power(outdict["Te"],2.0) * (np.power(outdict["error_Ane"],2.0) + np.power(outdict["error_Ate"],2.0)) + \
                                                         np.power(outdict["Ti0"],2.0) * (np.power(outdict["error_Ani0"],2.0) + np.power(outdict["error_Ati0"],2.0)) + \
                                                         np.power(outdict["Ti1"],2.0) * (np.power(outdict["error_Ani1"],2.0) + np.power(outdict["error_Ati1"],2.0)) + \
                                                         np.power(outdict["Ti2"],2.0) * (np.power(outdict["error_Ani2"],2.0) + np.power(outdict["error_Ati2"],2.0)))

                if outdict["magn_geometry"] == "s_alpha":
                    outdict["alpha"] = np.power(outdict["q"] / outdict["Bo"],2.0) * dpres * betac
                    outdict["error_alpha"] = np.sqrt(np.power(2.0 * outdict["error_q"] * outdict["q"] * dpres * betac,2.0) + np.power(np.power(outdict["q"],2.0) * dpreseb * betac,2.0)) / np.power(outdict["Bo"],2.0)

#                ionorder = ["MION1","TION1","TION2"]

            else:
                zlimv = np.nanmin(np.hstack((XD["ZLIM1"],XD["ZEFF"] - 1.0e-3)))
                zlim = np.full(here.shape,float(zlimv))

                numions = 3
                outdict["Ai0"] = np.full(here.shape,XD["MION1"]["A"])
                outdict["Zi0"] = np.full(here.shape,XD["MION1"]["Z"]) if "ZZ" not in XD["MION1"] else XD["MION1"]["ZZ"].copy()
                outdict["Ai1"] = np.full(here.shape,XD["AZ1"])
                outdict["Zi1"] = np.full(here.shape,XD["ZZ1"])
                outdict["Ai2"] = np.full(here.shape,XD["AZ2"])
                outdict["Zi2"] = np.full(here.shape,XD["ZZ2"])
                reft = XD["MION1"]["T"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["T"].copy()
                refat = XD["MION1"]["AT"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["AT"].copy()

                # Light impurity ion - Z <= 10
                ztag = "TION1"
                outdict["Ti1"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni1"] = (outdict["Zi2"] - XD["ZEFF"]) / (outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"] + (outdict["Zi1"] - zlim) * (outdict["Zi2"] - 1.0) / (zlim - 1.0)))
                outdict["Ati1"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani1"] = outdict["Ane"] + outdict["R0"] * XD["DZEFF"] / (outdict["Zi2"] - XD["ZEFF"])
                outdict["error_Ti1"] = XD[ztag]["TEB"].copy()
                outdict["error_ni1"] = np.sqrt(np.power(outdict["error_ne"] / outdict["ne"],2.0) + np.power(XD["ZEFFEB"] / (outdict["Zi2"] - XD["ZEFF"]),2.0)) * outdict["ni1"]
                outdict["error_Ati1"] = XD[ztag]["ATEB"].copy()
                outdict["error_Ani1"] = np.sqrt(np.power(outdict["error_Ane"],2.0) + np.power(outdict["R0"] * XD["DZEFFEB"] / outdict["ni1"],2.0) + np.power(outdict["R0"] * XD["DZEFF"] * XD["ZEFFEB"] / (outdict["ni1"] * XD["ZEFF"]),2.0))

                # Main plasma ion - hydrogenic
                ztag = "MION1"
                outdict["Ti0"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni0"] = ((outdict["Zi2"] - XD["ZEFF"]) - outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["Zi2"] - 1.0)
                outdict["Ati0"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani0"] = (outdict["Ane"] * (outdict["Zi2"] - XD["ZEFF"]) - outdict["R0"] * XD["DZEFF"] - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["ni0"] * (outdict["Zi2"] - 1.0))
                outdict["error_Ti0"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni0"] = np.sqrt(np.power((outdict["Zi2"] - XD["ZEFF"]) * outdict["error_ne"] / outdict["ne"],2.0) + np.power(XD["ZEFFEB"],2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_ni0"] = np.sqrt(np.power(XD["ZEFFEB"],2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_Ati0"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ane"] * (outdict["Zi2"] - XD["ZEFF"]),2.0) + np.power(outdict["Ane"] * XD["ZEFFEB"],2.0) + np.power(outdict["R0"] * XD["DZEFFEB"],2.0) + np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"],2.0)) / outdict["ni0"]
                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]),2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"],2.0)) / outdict["ni0"]

                # Heavy impurity ion - Z >= 26
                ztag = "TION2"
                outdict["Ti2"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni2"] = (1.0 - outdict["ni0"] * outdict["Zi0"] - outdict["ni1"] * outdict["Zi1"]) / outdict["Zi2"]
                outdict["Ati2"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["AT"]) > 0.0 else refat.copy()
                outdict["Ani2"] = (outdict["Ane"] - outdict["Ani0"] * outdict["ni0"] * outdict["Zi0"] - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"]) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ti2"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ne"] / outdict["ne"],2.0) + np.power(outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"],2.0)) / outdict["Zi2"]
                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"],2.0)) / outdict["Zi2"]
                outdict["error_Ati2"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ane"],2.0) + np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"],2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"],2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"],2.0) + np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"],2.0)) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"],2.0) + np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"],2.0) + np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"],2.0) + np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"],2.0) + np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"],2.0)) / (outdict["ni2"] * outdict["Zi2"])

                kevc = 1.60217662e3           # (e * 10^19 * 10^3)  -- places p in N m^-2
                betac = 2.51327412e-6         # (2 mu_0)            -- since p is already given in N m^-2
                dpres = kevc * outdict["ne"] * (outdict["Te"] * (outdict["Ane"] + outdict["Ate"]) + \
                                                outdict["ni0"] * outdict["Ti0"] * (outdict["Ani0"] + outdict["Ati0"]) + \
                                                outdict["ni1"] * outdict["Ti1"] * (outdict["Ani1"] + outdict["Ati1"]) + \
                                                outdict["ni2"] * outdict["Ti2"] * (outdict["Ani2"] + outdict["Ati2"]))
                dpreseb = kevc * outdict["ne"] * np.sqrt(np.power(outdict["Te"],2.0) * (np.power(outdict["error_Ane"],2.0) + np.power(outdict["error_Ate"],2.0)) + \
                                                         np.power(outdict["Ti0"],2.0) * (np.power(outdict["error_Ani0"],2.0) + np.power(outdict["error_Ati0"],2.0)) + \
                                                         np.power(outdict["Ti1"],2.0) * (np.power(outdict["error_Ani1"],2.0) + np.power(outdict["error_Ati1"],2.0)) + \
                                                         np.power(outdict["Ti2"],2.0) * (np.power(outdict["error_Ani2"],2.0) + np.power(outdict["error_Ati2"],2.0)))

                if outdict["magn_geometry"] == "s_alpha":
                    outdict["alpha"] = np.power(outdict["q"] / outdict["Bo"],2.0) * dpres * betac
                    outdict["error_alpha"] = np.sqrt(np.power(2.0 * outdict["error_q"] * outdict["q"] * dpres * betac,2.0) + np.power(np.power(outdict["q"],2.0) * dpreseb * betac,2.0)) / np.power(outdict["Bo"],2.0)

#                ionorder = ["MION1","TION1","TION2"]

            # This state is occasionally found due to precision of input data and geometric reconstruction, lower bound prevents blanket acceptance
            if outdict["trpeps"][0] < 0.0 and outdict["trpeps"][0] > -1.0e-3:
                outdict["trpeps"][0] = -outdict["trpeps"][0]

            if debug_flag:
                zeff = outdict["ni0"] * np.power(outdict["Zi0"],2.0)
                for ii in np.arange(1,numions):
                    stag = "%d" % (ii)
                    zeff = zeff + outdict["ni"+stag] * np.power(outdict["Zi"+stag],2.0)
                print(XD["ZEFF"])
                print(zeff)

            if add_meta:
                outdict["twlabel"] = np.full(here.shape,XD["META"]) if "META" in XD else np.full(here.shape,None)

            datanames = ["q", "smag", "Te", "ne", "Ate", "Ane"]
            for zz in np.arange(0,numions):
                ltag = "%d" % (zz)
                datanames.extend(["Ti"+ltag, "ni"+ltag, "Ati"+ltag, "Ani"+ltag])
            if outdict["magn_geometry"] == "s_alpha":
                datanames.extend(["alpha"])
            baderrors = []
            for item in datanames:
                if "error_"+item in outdict and (np.all(outdict["error_"+item] == 0.0) or not np.any(np.isfinite(outdict["error_"+item]))):
                    outdict["error_"+item] = np.full(here.shape,None)
                elif "error_"+item in outdict and np.any(outdict["error_"+item] > 1.0e10):
                    baderrors.append(item)
            if len(baderrors) > 0:
                estr = " ".join(baderrors)
                warnings.warn("Abnormally large errors: "+estr,UserWarning)

            rgood = True
            if not np.all(outdict["Ro"] > 0.0) or not np.all(outdict["R0"] > 0.0):
                rgood = False
            if not np.all(outdict["Rmin"] >= 0.0) or not np.all(outdict["trpeps"] >= 0.0):
                rgood = False
            ingood = True
            engood = True
            itgood = True
            etgood = True
            for ii in range(0,numions):
                itag = "%d" % ii
                if np.any(outdict["ni"+itag] < 0.0):
                    ingood = False
                if np.any(outdict["Ti"+itag] <= 0.0):
                    itgood = False
            nfilt = (outdict["ne"] == 0.0)
            if np.any(nfilt):
                outdict["ne"][nfilt] = 1.0e-3
            if np.all(nfilt) or np.any(outdict["ne"] <= 0.0):
                engood = False
            if np.any(outdict["Te"] <= 0.0):
                etgood = False
            ggood = True
            gvals = ["Ane","Ate","smag"]
            if outdict["magn_geometry"] == "s_alpha":
                gvals.extend(["alpha"])
            for gval in gvals:
                if gval in outdict and not np.all(np.isfinite(outdict[gval])):
                    ggood = False
            for ii in range(0,numions):
                itag = "%d" % ii
                if not np.all(np.isfinite(outdict["Ani"+itag])):
                    ggood = False
                if not np.all(np.isfinite(outdict["Ati"+itag])):
                    ggood = False
            qgood = True
            if np.all(outdict["q"] == 1.5):
                qgood = False
            if np.any(outdict["q"] <= 0.0):
                qgood = False
            ebgood = True

            if rgood and ingood and engood and itgood and etgood and ggood and qgood and make_input:

                namelists = OrderedDict()

                ## GENE Printer - taken from script of Garud Snoep, modified to fit template
                ## Parallelization namelist
                namelists["parallelization"] = OrderedDict()
                namelists["parallelization"]["n_parallel_sims"] = 2
                
                ## Box namelist
                namelists["box"] = OrderedDict([
                    ("n_spec", 3),
                    ("nx0", "17 !check convergence by changing this"),
                    ("nky0", 1),
                    ("nz0", "16 !check convergence by changing this"),
                    ("nv0", "32 !check convergence by changing this"),
                    ("nw0", "8 !check convergence by changing this"),
                    ("kymin", "0.1 !scanrange: 0.1,0.1,0.8"),
                    ("lv", 3.0),
                    ("lw", 9.0),
                    ("mu_grid_type", "'gau_lag'"),
                    ("n0_global", -1111)
                ])
                
                ## I/O namelist
                diagdir = "null" if run_directory is None else repr(run_directory)
                namelists["in_out"] = OrderedDict([
                    ("diagdir", diagdir),
                    ("read_checkpoint", '.F.'),
                    ("istep_nrg", 10),
                    ("istep_field", 200),
                    ("istep_mom", 400),
                    ("istep_energy", 500),
                    ("istep_vsp", 500),
                    ("istep_schpt", 0)
                ])
                
                ## General namelist
                namelists["general"] = OrderedDict([
                    ("nonlinear", '.F.')
                ])
                if sol == "IV":
                    namelists["general"]["comp_type"] = "'IV'"
                    namelists["general"]["calc_dt"] = '.T.'
                    namelists["general"]["simtimelim"] = 10000.0
                    namelists["general"]["timelim"] = 43200
                elif sol == "EV_mfn":
                    namelists["general"]["comp_type"] = "'EV'"
                    namelists["general"]["which_ev"] = "'mfn'"
                    namelists["general"]["n_ev"] = 2
                    namelists["general"]["taumfn"] = 0.3
                elif sol == "EV_harmonic":
                    namelists["general"]["comp_type"] = "'EV'"
                    namelists["general"]["which_ev"] = "'harmonic'"
                namelists["general"].update([
                    ("collision_op", "'landau'"),
                    ("coll_on_h", '.T.'),
                    ("coll_f_fm_on", '.T.'),
                    ("coll_cons_model", "'self_adj'"),
                    ("coll", -1),
                    ("bpar", '.F.'),
                    ("debye2", -1),
                    ("hyp_z", 5),
                    ("init_cond", "'alm'")
                ])
                
                ## External contribution namelist
                namelists["external_contr"] = OrderedDict()
                
                ## Geometry namelist
                namelists["geometry"] = OrderedDict([
                    ("magn_geometry", "'"+outdict["magn_geometry"]+"'"),
                    ("trpeps", outdict["trpeps"][0]),
                    ("q0", outdict["q"][0]),
                    ("shat", outdict["smag"][0]),
                    ("major_R", outdict["Ro"][0] / outdict["R0"][0]),
                    ("amhd", outdict["alpha"][0]),
                    ("norm_flux_projection", '.F.'),
                    ("rhostar", -1),
                    ("dpdx_term", "'full_drift'"),
                    ("dpdx_pm", -1)
                ])
                
                ## Species namelist
                namelists["species:e"] = OrderedDict([
                    ("name", "'electrons'"),
                    ("mass", outdict["Ae"][0] / outdict["mref"][0]),
                    ("charge", -1),
                    ("temp", 1.0),
                    ("dens", 1.0),
                    ("omt", outdict["Ate"][0]),
                    ("omn", outdict["Ane"][0])
                ])
                for ii in range(0,numions):
                    itag = "%d" % ii
                    imass = outdict["Ai"+itag][0] / outdict["mref"][0] if ii != 0 else 1.0
                    namelists["species:i"+itag] = OrderedDict([
                        ("name", "'ions'"),
                        ("mass", imass),
                        ("charge", outdict["Zi"+itag][0]),
                        ("temp", outdict["Ti"+itag][0]),
                        ("dens", outdict["ni"+itag][0]),
                        ("omt", outdict["Ati"+itag][0]),
                        ("omn", outdict["Ani"+itag][0])
                    ])
                
                ## Info namelist
                namelists["info"] = OrderedDict()
                
                ## Units namelist
                namelists["units"] = OrderedDict([
                    ("Tref", outdict["Tref"][0]),
                    ("nref", outdict["nref"][0]),
                    ("Bref", outdict["Bref"][0]),
                    ("Lref", outdict["Lref"][0]),
                    ("mref", outdict["mref"][0]),
                    ("omegatorref", outdict["omegatorref"][0])
                ])

            if not rgood:
                outdict = None
                xobj = None
                raise ValueError("Negative or zero major radii or negative minor radii detected!")
            if not (ingood and engood):
                outdict = None
                xobj = None
                raise ValueError("Negative densities detected!")
            if not (itgood and etgood):
                outdict = None
                xobj = None
                raise ValueError("Negative temperatures detected!")
            if not ggood:
                outdict = None
                xobj = None
                raise ValueError("NaN values detected in gradient quantities!")
            if not qgood:
                outdict = None
                xobj = None
                raise ValueError("Negative or no safety factor profile detected!")

    return (outdict,namelists)


