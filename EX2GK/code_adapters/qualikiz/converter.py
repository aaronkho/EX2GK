# Script with functions to convert QuaLiKiz code-specific fields into more manageable data structures
# Developer: Aaron Ho - 19/05/2023

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import pandas as pd

# Internal package imports
from EX2GK.tools.general import classes

number_types = (
    int, float,                                  # Built-in types
    np.int8, np.int16, np.int32, np.int64,       # Signed integer types
    np.uint8, np.uint16, np.uint32, np.uint64,   # Unsigned integer types
    np.float16, np.float32, np.float64           # Floating point decimal types
)

array_types = (
    list, tuple,                                 # Built-in types
    np.ndarray,                                  # numpy array
    pd.Series                                    # Pandas array
)

def convert_time_window_to_minimal(basedata, rho=None):
    """
    """
    inclass = None
    rvec = None
    if isinstance(basedata, classes.EX2GKTimeWindow):
        inclass = copy.deepcopy(basedata)
    if isinstance(rho, number_types):
        rvec = np.array([rho]),flatten()
    if isinstance(rho, array_types):
        rvec = np.array(rho).flatten()

    zdmap = {
        "OUTQLK_BO": "Bo",
        "OUTQLK_RZERO": "R0",
        "OUTQLK_RMIN": "Rmin",
        "OUTQLK_BZERO": "B0",
        #"OUTQLK_NSCALE": "nscale",
        #"OUTQLK_TSCALE": "Tscale",
        "OUTQLK_CREF": "cs"
    }
    pdmap = {
        #"OUTQLK_NREF": "nref",
        #"OUTQLK_TREF": "Tref",
        "OUTQLK_RO": "Ro",
        "OUTQLK_R": "r",
        "OUTQLK_X": "x",
        "OUTQLK_EPS": "epsilon",
        "OUTQLK_BTOR": "Btor",
        "OUTQLK_BPOL": "Bpol",
        #"OUTQLK_BTOT": "B",
        "OUTQLK_Q": "q",
        "OUTQLK_SMAG": "smag",
        #"OUTQLK_BETAEI": "betaei",
        "OUTQLK_BETATH": "beta",
        "OUTQLK_BETATOT": "betatot",
        #"OUTQLK_ALPHAEI": "alphaei",
        "OUTQLK_ALPHATH": "alpha",
        #"OUTQLK_ALPHATOT": "alphatot",
        "OUTQLK_UTOR": "utor",
        "OUTQLK_UPOL": "upol",
        "OUTQLK_MACHTOR": "Machtor",
        "OUTQLK_AUTOR": "Autor",
        "OUTQLK_UPAR": "upar",
        "OUTQLK_UPER": "uper",
        "OUTQLK_UDPI": "ugradpi",
        "OUTQLK_MACHPAR": "Machpar",
        "OUTQLK_AUPAR": "Aupar",
        "OUTQLK_MACHPER": "Machper",
        "OUTQLK_AUPER": "Auper",
        #"OUTQLK_UGEO": "ugeo",
        "OUTQLK_GAMMAE": "gammaE",
        #"OUTQLK_GAMMAEDPI": "gammaEgradpi"
        "OUTQLK_NUSTAR": "Nustar",
        #"OUTQLK_NUSTARTOT": "Nustartot"
    }

    outdata = None
    if inclass is not None and inclass.isFitted() and inclass.isProcessed():

        species = {"E": "e"}
        numion = 0
        for ii in range(inclass["META"]["NUMI"]):
            ctag = "I%d" % (ii+1)
            if "OUTQLK_Z"+ctag in inclass["PD"] and "OUTQLK_N"+ctag in inclass["PD"] and "OUTQLK_T"+ctag in inclass["PD"]:
                species[ctag] = "i%d" % (numion)
                numion += 1
        for ii in range(inclass["META"]["NUMIMP"]):
            ctag = "IMP%d" % (ii+1)
            if "OUTQLK_Z"+ctag in inclass["PD"] and "OUTQLK_N"+ctag in inclass["PD"] and "OUTQLK_T"+ctag in inclass["PD"]:
                species[ctag] = "i%d" % (numion)
                numion += 1
        for ii in range(inclass["META"]["NUMZ"]):
            ctag = "Z%d" % (ii+1)
            if "OUTQLK_Z"+ctag in inclass["PD"] and "OUTQLK_N"+ctag in inclass["PD"] and "OUTQLK_T"+ctag in inclass["PD"]:
                species[ctag] = "i%d" % (numion)
                numion += 1
        for skey, stag in species.items():
            pdmap["OUTQLK_Z"+skey] = "Z"+stag
            pdmap["OUTQLK_AZ"+skey] = "Az"+stag
            pdmap["OUTQLK_N"+skey] = "n"+stag if stag == "e" else "normn"+stag
            pdmap["OUTQLK_AN"+skey] = "An"+stag
            pdmap["OUTQLK_T"+skey] = "T"+stag
            pdmap["OUTQLK_AT"+skey] = "At"+stag
            #pdmap["OUTQLK_BETA"+skey] = "beta"+stag

        tempdata = {}
        rcoord = inclass["PD"]["X"].coordinate
        if rvec is None:
            rvec = copy.deepcopy(inclass["PD"]["X"][""])
        xvec = itemgetter(0)(inclass["CD"].convert(rvec, "RHOTORN", rcoord))

        for key, tag in zdmap.items():
            if "ZD" in inclass and key in inclass["ZD"]:
                tempdata[tag] = np.full(xvec.shape, inclass["ZD"][key][""])

        for key, tag in pdmap.items():
            if "PD" in inclass and key in inclass["PD"]:
                tempdata[tag] = inclass["PD"].interpolate(xvec, "X", key)

        if tempdata:
            zeff = np.zeros(rvec.shape)
            for ii in range(numion):
                itag = "i%d" % (ii)
                zeff += tempdata["normn"+itag] * tempdata["Z"+itag] * tempdata["Z"+itag]
            tempdata["Zeff"] = copy.deepcopy(zeff)
            tempdata["logNustar"] = np.log10(tempdata["Nustar"])
            tempdata["rho"] = rvec
            tempdata["rho_idx"] = [x for x in range(len(rvec))]
            outdata = pd.DataFrame(tempdata, index=pd.RangeIndex(len(rvec)))

    else:
        print("Invalid EX2GKTimeWindow class object provided to converter. Skipping!")

    return outdata

