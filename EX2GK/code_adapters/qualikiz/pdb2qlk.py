#!/usr/bin/env python3

import sys
import os
import inspect
import re
import copy
import warnings
from operator import itemgetter
from collections import OrderedDict

import numpy as np, pandas as pd
from scipy.interpolate import interp1d
import pickle
import subprocess

from EX2GK.tools.general import proctools as ptools
from EX2GK.code_adapters.qualikiz import adapter
from qualikiz_tools.qualikiz_io.inputfiles import Electron, Ion, IonList, QuaLiKizXpoint, QuaLiKizPlan
#from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun, QuaLiKizBatch
from qualikiz_tools.machine_specific.bash import Run as QuaLiKizRun, Batch as QuaLiKizBatch

number_types = (
    int, float,                                # Built-in types
    np.int8, np.int16, np.int32, np.int64,     # Signed integer types
    np.uint8, np.uint16, np.uint32, np.uint64, # Unsigned integer types
    np.float16, np.float32, np.float64         # Floating point decimal types
)

array_types = (
    list, tuple,                                # Built-in types
    np.ndarray,                                 # numpy array
    pd.Series                                   # pandas array
)

def extract_value(rvec, dvec, rval, pflag=False):
    """
    Linear interpolates a given radial profile to extract values at requested radii.

    :arg rvec: array. Radial points of profile.

    :arg dvec: array. Value of profile.

    :arg rval: array. Radial points from which to extract profile values.

    :kwarg pflag: bool. Set True to strictly enforce positive return values.

    :returns: array. Profile values evaluated at requested radial points.
    """
    ival = None
    if isinstance(rval, number_types):
        ival = np.array([rval])
    elif isinstance(rval, array_types):
        ival = np.array(rval).flatten()
    dval = None
    if isinstance(rvec, array_types) and isinstance(dvec, array_types) and ival is not None:
        rrvec = np.array(rvec).squeeze()
        ddvec = np.array(dvec).squeeze()
        if rrvec.size == ddvec.size:
            nfilt = np.any([np.isfinite(rrvec), np.isfinite(ddvec)], axis=0)
            if np.count_nonzero(nfilt) > 1:
                dfunc = interp1d(rrvec[nfilt], ddvec[nfilt], bounds_error=False, fill_value='extrapolate')
                dval = dfunc(ival)
                dfilt = (dval < 0.0)
                if np.any(dfilt) and pflag:
                    dval[dfilt] = 0.0
    return dval


def extract_species_data(shotdata, stype, dval, alt=None):
    """
    Extracts required data to define a particle species within QuaLiKiz, provided
    that the necessary data is present. Linearly interpolates any radial profiles.

    .. note:: Requires QuaLiKiz output code adapter to be run on EX2GK database entry!

    :arg shotdata: dict. EX2GK database entry for a given time window.

    :arg stype: str. EX2GK identification string indicating which species to extract from database entry.

    :arg dval: array. Radial points from which to extract profile values.

    :kwarg alt: str. EX2GK identification string indicating desired back-up species to use if primary is unavailable.

    :returns: dict. Extracted data with standardized field names for use within function in this file.
    """
    SD = None
    ststr = None
    ipt = None
    atstr = None
    if isinstance(shotdata, dict):
        SD = shotdata
    if isinstance(dval, number_types):
        ipt = np.array([dval])
    elif isinstance(dval, array_types):
        ipt = np.array(dval).flatten()
    if isinstance(stype, str):
        ststr = stype.upper()
    if isinstance(alt, str):
        atstr = alt.upper()

    PD = None
    if SD is not None and ststr is not None and ipt is not None:
        PD = dict()
        if "META_CODETAG" in SD and SD["META_CODETAG"] == "QLK":
            rvec = SD["PD_X"]
            ctag = "PD_OUT" + SD["META_CODETAG"] + "_"
            if "META_Z"+ststr in SD and isinstance(SD["META_Z"+ststr], number_types):
                aa = SD["META_A"+ststr] if "META_A"+ststr in SD and isinstance(SD["META_A"+ststr], number_types) else None
                (PD["NAME"], PD["A"], PD["Z"]) = ptools.define_ion_species(z=SD["META_Z"+ststr], a=aa)
            elif ststr == "E":
                (PD["NAME"], PD["A"], PD["Z"]) = ptools.define_ion_species(z=-1)
            elif ststr == "I":
                ststr = "I1"
                (PD["NAME"], PD["A"], PD["Z"]) = ptools.define_ion_species(z=1, a=2)
            else:
                PD = None

            if isinstance(PD,dict):
                PD["N"] = np.zeros(ipt.shape)
                PD["T"] = np.zeros(ipt.shape)
                PD["AN"] = np.full(ipt.shape, np.NaN)
                PD["AT"] = np.full(ipt.shape, np.NaN)
                PD["NEB"] = np.zeros(ipt.shape)
                PD["TEB"] = np.zeros(ipt.shape)
                PD["ANEB"] = np.full(ipt.shape, np.NaN)
                PD["ATEB"] = np.full(ipt.shape, np.NaN)

                if ctag+"N"+ststr in SD:
                    PD["N"] = extract_value(rvec, SD[ctag+"N"+ststr], ipt, pflag=True)
                elif atstr is not None and ctag+"N"+atstr in SD:
                    PD["N"] = extract_value(rvec, SD[ctag+"N"+atstr], ipt, pflag=True)
                if ctag+"T"+ststr in SD:
                    PD["T"] = extract_value(rvec, SD[ctag+"T"+ststr], ipt, pflag=True)
                elif atstr is not None and ctag+"T"+atstr in SD:
                    PD["T"] = extract_value(rvec, SD[ctag+"T"+atstr], ipt, pflag=True)
                if ctag+"AN"+ststr in SD:
                    PD["AN"] = extract_value(rvec, SD[ctag+"AN"+ststr], ipt)
                elif atstr is not None and ctag+"AN"+atstr in SD:
                    PD["AN"] = extract_value(rvec, SD[ctag+"AN"+atstr], ipt)
                if ctag+"AT"+ststr in SD:
                    PD["AT"] = extract_value(rvec, SD[ctag+"AT"+ststr], ipt)
                elif atstr is not None and ctag+"AT"+atstr in SD:
                    PD["AT"] = extract_value(rvec, SD[ctag+"AT"+atstr], ipt)
                if ctag+"N"+ststr+"EB" in SD:
                    PD["NEB"] = extract_value(rvec, SD[ctag+"N"+ststr+"EB"], ipt)
                elif atstr is not None and ctag+"N"+atstr+"EB" in SD:
                    PD["NEB"] = extract_value(rvec, SD[ctag+"N"+atstr+"EB"], ipt)
                if ctag+"T"+ststr+"EB" in SD:
                    PD["TEB"] = extract_value(rvec, SD[ctag+"T"+ststr+"EB"], ipt)
                elif atstr is not None and ctag+"T"+atstr+"EB" in SD:
                    PD["TEB"] = extract_value(rvec, SD[ctag+"T"+atstr+"EB"], ipt)
                if ctag+"AN"+ststr+"EB" in SD:
                    PD["ANEB"] = extract_value(rvec, SD[ctag+"AN"+ststr+"EB"], ipt)
                elif atstr is not None and ctag+"AN"+atstr+"EB" in SD:
                    PD["ANEB"] = extract_value(rvec, SD[ctag+"AN"+atstr+"EB"], ipt)
                if ctag+"AT"+ststr+"EB" in SD:
                    PD["ATEB"] = extract_value(rvec, SD[ctag+"AT"+ststr+"EB"], ipt)
                elif atstr is not None and ctag+"AT"+atstr+"EB" in SD:
                    PD["ATEB"] = extract_value(rvec, SD[ctag+"AT"+atstr+"EB"], ipt)

                if ctag+"Z"+ststr in SD:
                    PD["ZZ"] = extract_value(rvec, SD[ctag+"Z"+ststr], ipt)
                elif atstr is not None and ctag+"Z"+atstr in SD:
                    PD["ZZ"] = extract_value(rvec, SD[ctag+"Z"+atstr], ipt)
                if ctag+"Z"+ststr+"EB" in SD:
                    PD["ZZEB"] = extract_value(rvec, SD[ctag+"Z"+ststr+"EB"], ipt)
                elif atstr is not None and ctag+"Z"+atstr+"EB" in SD:
                    PD["ZZEB"] = extract_value(rvec, SD[ctag+"Z"+atstr+"EB"], ipt)
                if ctag+"AZ"+ststr in SD:
                    PD["AZZ"] = extract_value(rvec, SD[ctag+"AZ"+ststr], ipt)
                elif atstr is not None and ctag+"AZ"+atstr in SD:
                    PD["AZZ"] = extract_value(rvec, SD[ctag+"AZ"+atstr], ipt)
                if ctag+"AZ"+ststr+"EB" in SD:
                    PD["AZZEB"] = extract_value(rvec, SD[ctag+"AZ"+ststr+"EB"], ipt)
                elif atstr is not None and ctag+"AZ"+atstr+"EB" in SD:
                    PD["AZZEB"] = extract_value(rvec, SD[ctag+"AZ"+atstr+"EB"], ipt)

            else:
                PD = dict()
                PD["N"] = np.zeros(ipt.shape)
                PD["T"] = np.zeros(ipt.shape)
                PD["AN"] = np.full(ipt.shape, np.NaN)
                PD["AT"] = np.full(ipt.shape, np.NaN)
                PD["NEB"] = np.zeros(ipt.shape)
                PD["TEB"] = np.zeros(ipt.shape)
                PD["ANEB"] = np.full(ipt.shape, np.NaN)
                PD["ATEB"] = np.full(ipt.shape, np.NaN)

    return PD


# QLK Input Guide:
#   R0 = (Rout + Rin) / 2          at LCFS
#   Ro = (Rout + Rin) / 2          at flux surface to be evaluated
#   rmin = (Rout - Rin) / 2        at LCFS
#   x = (Rout - Rin) / 2 / rmin    at flux surface to be evaluated, NORMALIZED!!!

def extract_data_point(shotdata, rvalue, rcoord):
    """
    Extracts required data to define a QuaLiKiz run in its entirety, defining defaults
    if the necessary data is not present. Linearly interpolates any radial profiles.

    .. note:: Requires QuaLiKiz output code adapter to be run on EX2GK database entry!

    :arg shotdata: dict. EX2GK database entry for a given time window.

    :arg rvalue: array. Radial points from which to extract profile values.

    :arg rcoord: str. Radial coordinate corresponding to input radial points, used to convert if necessary.

    :returns: dict. Extracted data with standardized field names for use within function in this file.
    """
    SD = None
    urval = None
    upt = None
    ucst = None
    if isinstance(shotdata, dict):
        SD = shotdata
    if isinstance(rvalue, number_types):
        upt = np.array([rvalue])
    elif isinstance(rvalue, array_types):
        upt = np.array(rvalue).flatten()
    if isinstance(rcoord, str):
        ucst = rcoord

    PD = None
    if isinstance(SD, dict) and upt is not None and ucst is not None:

        PD = dict()

        # Obtain fit coordinate system
        (ucs, xlabel, xunit, dc, dl) = ptools.define_coordinate_system(ucst)
        rvec = SD["PD_X"].copy()
        rtag = SD["META_CSOP"] + SD["META_CSO"]
        utag = SD["META_CSOP"] + ucs
        ctag = "PD_OUTQLK_"
        PD["META"] = "%s_%d_%d_%d" % (SD["META_DEVICE"], SD["META_SHOT"], int(SD["META_T1"] * 10000), int(SD["META_T2"] * 10000))

        # Geometry
        ipt = itemgetter(0)(ptools.convert_base_coords(upt, SD["CD_"+utag+"_V"], SD["CD_"+rtag+"_V"], None, None, None))
        PD["RMAJOR_LCFS"] = SD["ZD_OUTQLK_RZERO"] if "ZD_OUTQLK_RZERO" in SD and SD["ZD_OUTQLK_RZERO"] is not None else 2.96
        PD["RMINOR_LCFS"] = SD["ZD_OUTQLK_RMIN"] if "ZD_OUTQLK_RMIN" in SD and SD["ZD_OUTQLK_RMIN"] is not None else 1.0
        PD["RMAJOR_POINT"] = extract_value(rvec, SD[ctag+"RO"], ipt) if ctag+"RO" in SD and SD[ctag+"RO"] is not None else np.full(ipt.shape, PD["RMAJOR_LCFS"])
        PD["RMINORN_POINT"] = extract_value(rvec, SD[ctag+"X"], ipt) if ctag+"X" in SD and SD[ctag+"X"] is not None else np.full(ipt.shape, 0.5)

        # Reference values
        PD["TREF"] = extract_value(rvec, SD[ctag+"TREF"], ipt, pflag=True)
        PD["NREF"] = extract_value(rvec, SD[ctag+"NREF"], ipt, pflag=True)
        sqemp = np.sqrt(1.60217662e-19 / 1.6726219e-27)
        PD["CREF"] = np.sqrt(PD["TREF"]) * sqemp                       # cref = sqrt(e * Tref / mp)
        PD["LREF"] = np.full(ipt.shape, PD["RMAJOR_LCFS"])
        if lref_style == 1:
            PD["LREF"] = PD["RMAJOR_POINT"].copy()

        # Species data
        (PD["AZ1"],PD["ZZ1"]) = itemgetter(1,2)(ptools.define_ion_species(short_name=SD["META_MATWALL"]))
        if PD["AZ1"] is None or PD["ZZ1"] is None:
            PD["AZ1"] = 12.0
            PD["ZZ1"] = 6.0
        (PD["AZ2"],PD["ZZ2"]) = itemgetter(1,2)(ptools.define_ion_species(short_name=SD["META_MATSTRIKE"]))
        if PD["AZ2"] is None or PD["ZZ2"] is None or PD["ZZ2"] <= 18.0 or PD["ZZ2"] >= 54.0:
            PD["AZ2"] = 58.0
            PD["ZZ2"] = 28.0
        pcnlim = 0.02
        zlimest = (1.0 + pcnlim * PD["ZZ1"] * PD["ZZ1"]) / (1.0 + pcnlim * PD["ZZ1"])
        PD["FZEFF"] = SD["ZD_FZEFF"] if "ZD_FZEFF" in SD and SD["ZD_FZEFF"] is not None else 1.5
        PD["FZEFFEB"] = SD["ZD_FZEFFEB"] if "ZD_FZEFFEB" in SD and SD["ZD_FZEFFEB"] is not None else 0.2
        PD["ZLIM1"] = SD["META_ZLIM1"] if "META_ZLIM1" in SD and SD["META_ZLIM1"] is not None else np.full(ipt.shape,zlimest)
        PD["ZEFF"] = extract_value(rvec, SD["PD_ZEFF"], ipt) if "PD_ZEFF" in SD and SD["PD_ZEFF"] is not None else np.full(ipt.shape, PD["FZEFF"])
        PD["ZEFFEB"] = extract_value(rvec, SD["PD_ZEFFEB"], ipt) if "PD_ZEFFEB" in SD and SD["PD_ZEFFEB"] is not None else np.full(ipt.shape, PD["FZEFFEB"])
        PD["DZEFF"] = extract_value(rvec, SD["PD_ZEFFGRAD"], ipt) if "PD_ZEFFGRAD" in SD and SD["PD_ZEFFGRAD"] is not None else np.zeros(ipt.shape)
        PD["DZEFFEB"] = extract_value(rvec, SD["PD_ZEFFGRADEB"], ipt) if "PD_ZEFFGRADEB" in SD and SD["PD_ZEFFGRADEB"] is not None else np.zeros(ipt.shape)
        PD["ELEC"] = extract_species_data(SD, "e", ipt)                # Electrons
        PD["NMION"] = int(SD["META_NUMI"]) if "META_NUMI" in SD and SD["META_NUMI"] is not None else 1
        for ii in range(PD["NMION"]):
            itag = "%d" % (ii+1)
            PD["MION"+itag] = extract_species_data(SD, "i"+itag, ipt)  # Main ions
        PD["NIION"] = int(SD["META_NUMIMP"]) if "META_NUMIMP" in SD and SD["META_NUMIMP"] is not None else 0
        for ii in range(PD["NIION"]):
            itag = "%d" % (ii+1)
            PD["IION"+itag] = extract_species_data(SD, "imp"+itag, ipt)
        PD["TION1"] = extract_species_data(SD, "z1", ipt)              # Primary trace impurity ions for quasineutrality
        PD["TION2"] = extract_species_data(SD, "z2", ipt)              # Secondary trace impurity ions for quasineutrality

        fdebug = False
        if fdebug:
            ti1z = np.power(PD["TION1"]["Z"], 2.0) * PD["TION1"]["N"]
            ti2z = np.power(PD["TION2"]["Z"], 2.0) * PD["TION2"]["N"]
            print(PD["TION1"]["Z"], ti1z)
            print(PD["TION2"]["Z"], ti2z)
            uzeff = ti1z + ti2z
            for ii in range(PD["NMION"]):
                itag = "%d" % (ii+1)
                miz = np.power(PD["MION"+itag]["Z"], 2.0) * PD["MION"+itag]["N"]
                print(PD["MION"+itag]["Z"], miz)
                uzeff = uzeff + miz
            for ii in range(PD["NIION"]):
                itag = "%d" % (ii+1)
                iiz = np.power(PD["IION"+itag]["Z"], 2.0) * PD["IION"+itag]["N"]
                print(PD["IION"+itag]["Z"], iiz)
                uzeff = uzeff + iiz

        # Magnetic quantities
        PD["BAXS"] = float(np.abs(SD["ZD_OUTQLK_BO"])) if "ZD_OUTQLK_BO" in SD and SD["ZD_OUTQLK_BO"] is not None else 2.5
        PD["Q"] = extract_value(rvec, SD[ctag+"Q"], ipt) if ctag+"Q" in SD and SD[ctag+"Q"] is not None else np.full(ipt.shape, 1.5)
        PD["QEB"] = extract_value(rvec, SD[ctag+"QEB"], ipt) if ctag+"QEB" in SD and SD[ctag+"QEB"] is not None else np.full(ipt.shape, 0.5)
        PD["SMAG"] = extract_value(rvec, SD[ctag+"SMAG"], ipt) if ctag+"SMAG" in SD and SD[ctag+"SMAG"] is not None else np.zeros(ipt.shape)
        PD["SMAGEB"] = extract_value(rvec, SD[ctag+"SMAGEB"], ipt) if ctag+"SMAGEB" in SD and SD[ctag+"SMAGEB"] is not None else np.zeros(ipt.shape)

        # Rotation quantities
        PD["MTOR"] = extract_value(rvec, SD[ctag+"MACHTOR"], ipt) if ctag+"MACHTOR" in SD and SD[ctag+"MACHTOR"] is not None else np.zeros(ipt.shape)
        PD["MTOREB"] = extract_value(rvec, SD[ctag+"MACHTOREB"], ipt) if ctag+"MACHTOREB" in SD and SD[ctag+"MACHTOREB"] is not None else np.zeros(ipt.shape)
        PD["AUTOR"] = extract_value(rvec, SD[ctag+"AUTOR"], ipt) if ctag+"AUTOR" in SD and SD[ctag+"AUTOR"] is not None else np.zeros(ipt.shape)
        PD["AUTOREB"] = extract_value(rvec, SD[ctag+"AUTOREB"], ipt) if ctag+"AUTOREB" in SD and SD[ctag+"AUTOREB"] is not None else np.zeros(ipt.shape)
        PD["MPAR"] = extract_value(rvec, SD[ctag+"MACHPAR"], ipt) if ctag+"MACHPAR" in SD and SD[ctag+"MACHPAR"] is not None else None
        PD["MPAREB"] = extract_value(rvec, SD[ctag+"MACHPAREB"], ipt) if ctag+"MACHPAREB" in SD and SD[ctag+"MACHPAREB"] is not None else np.zeros(ipt.shape)
        PD["AUPAR"] = extract_value(rvec, SD[ctag+"AUPAR"], ipt) if ctag+"AUPAR" in SD and SD[ctag+"AUPAR"] is not None else None
        PD["AUPAREB"] = extract_value(rvec, SD[ctag+"AUPAREB"], ipt) if ctag+"AUPAREB" in SD and SD[ctag+"AUPAREB"] is not None else np.zeros(ipt.shape)
        # This gammaE_rot term is normalized by Rmin, not Rzero - should it still be used?
        PD["GAMMAEROT"] = -PD["AUTOR"] * PD["RMINORN_POINT"] * PD["RMINOR_LCFS"] / (PD["RMAJOR_POINT"] * PD["Q"])  # Based on circular geometry, pure toroidal rotation, neglects grad p term
        PD["GAMMAEROTEB"] = (PD["RMINORN_POINT"] * PD["RMINOR_LCFS"] / (PD["RMAJOR_POINT"] * PD["Q"])) * np.sqrt(np.power(PD["AUTOREB"], 2.0) + np.power(PD["AUTOR"] * PD["QEB"] / PD["Q"], 2.0))
        PD["GAMMAE"] = extract_value(rvec, SD[ctag+"GAMMAE"], ipt) if ctag+"GAMMAE" in SD and SD[ctag+"GAMMAE"] is not None else np.zeros(ipt.shape)
        PD["GAMMAEEB"] = extract_value(rvec, SD[ctag+"GAMMAEEB"], ipt) if ctag+"GAMMAEEB" in SD and SD[ctag+"GAMMAEEB"] is not None else np.zeros(ipt.shape)

        # Kinetic quantities
        PD["ALPHA"] = extract_value(rvec, SD[ctag+"ALPHATH"], ipt) if ctag+"ALPHATH" in SD and SD[ctag+"ALPHATH"] is not None else np.zeros(ipt.shape)
        PD["ALPHAEB"] = extract_value(rvec, SD[ctag+"ALPHATHEB"], ipt) if ctag+"ALPHATHEB" in SD and SD[ctag+"ALPHATHEB"] is not None else np.zeros(ipt.shape)

        # Pressures to emulate ad-hoc electromagnetic stabilization factor
        ptherm = extract_value(rvec, SD["PD_PE"], ipt) if "PD_PE" in SD and SD["PD_PE"] is not None else np.zeros(ipt.shape)
        for ii in range(PD["NMION"]):
            itag = "%d" % (ii+1)
            ptherm += extract_value(rvec, SD["PD_PI"+itag], ipt) if "PD_PI"+itag in SD and SD["PD_PI"+itag] is not None else np.zeros(ipt.shape)
        for ii in range(PD["NIION"]):
            itag = "%d" % (ii+1)
            ptherm += extract_value(rvec, SD["PD_PIMP"+itag], ipt) if "PD_PIMP"+itag in SD and SD["PD_PIMP"+itag] is not None else np.zeros(ipt.shape)
        ptherm += extract_value(rvec, SD["PD_PZ1"], ipt) if "PD_PZ1" in SD and SD["PD_PZ1"] is not None else np.zeros(ipt.shape)
        ptherm += extract_value(rvec, SD["PD_PZ2"], ipt) if "PD_PZ2" in SD and SD["PD_PZ2"] is not None else np.zeros(ipt.shape)
        PD["PTHERM"] = copy.deepcopy(ptherm)
        pfast = np.zeros(ipt.shape)
        sources = ["NBI", "ICRH", "ECRH", "LH"]
        for src in sources:
            pfast += extract_value(rvec, SD["PD_PFI"+src], ipt) if "PD_PFI"+src in SD and SD["PD_PFI"+src] is not None else np.zeros(ipt.shape)
        PD["PFAST"] = copy.deepcopy(pfast)

    return PD


def generate_QLK_input(scan_dict, kthetarhos=None, set_num_ions=1, set_rot_flag=1, set_tor_rot=True, set_coll_flag=1, set_coll_mult=1.0, perform_sanity_checks=True):
    """
    Converts a scan dictionary produced by this module into a
    QuaLiKizXpoint object. If the scan dictionary contains
    vectors of values, the first element of each vector is
    used to initalize the QuaLiKizXpoint object.

    :arg scan_dict: dict. Scan dictionary containing all necessary input parameters, as specified by this module.

    :kwarg kthetarhos: array. Vector of kthetarhos for which QuaLiKiz will calculate linear growth rates, uses recommended vector if not specified.

    :kwarg set_num_ions: int. Specify number of ions to include in generated QuaLiKiz run, must be equal to or less than number in scan dictionary.

    :kwarg set_rot_flag: int. Specify QuaLiKiz rotation flag - 0 = no rotation, 1 = rotation, 2 = rotation only applied for rho >= 0.5.

    :kwarg set_tor_rot: bool. Specify dropping of rotation term due to ion pressure gradient and poloidal plasma rotation.

    :kwarg set_coll_flag: int. Specify QuaLiKiz collision flag - 0 = without collisions, 1 = with collisions.

    :kwarg set_coll_mult: float. Specify multiplier for collisionality term within QuaLiKiz, defaults to 1.0.

    :kwarg perform_sanity_checks: bool. Flag to toggle rudimentary checking of input parameter values.

    :returns: object. QuaLiKizXpoint object, with input parameter values initialized with scan dictionary.
    """
    outdict = None
    ktrs = [0.1, 0.175, 0.25, 0.325, 0.4, 0.5, 0.6, 0.8, 1.0, 2.0, 3.5, 6.0, 10.5, 15.0, 19.5, 24.0, 30.0, 36.0]         # Standard vector of kthetarhos (18 values)
    numions = 1
    rot_flag = 1
    assume_tor_rot = True if set_tor_rot else False
    coll_flag = True if set_coll_flag else False
    collmult = 1.0
    if isinstance(scan_dict, dict):
        outdict = copy.deepcopy(scan_dict)
    if isinstance(kthetarhos, (tuple, list)) and len(kthetarhos) > 0:
        ktrs = list(kthetarhos)
    elif isinstance(kthetarhos, np.ndarray) and kthetarhos.size > 0:
        ktrs = list(kthetarhos.flatten())
    if isinstance(set_num_ions, int):
        numions = int(set_num_ions)
    if isinstance(set_rot_flag, int):
        rot_flag = int(set_rot_flag)
    elif isinstance(set_rot_flag, bool):
        rot_flag = 1 if set_rot_flag else 0
    if isinstance(set_coll_mult, number_types):
        collmult = float(set_coll_mult)
        coll_flag = True

    xobj = None
    if outdict is not None:
        rgood = True
        ingood = True
        engood = True
        itgood = True
        etgood = True
        ggood = True
        qgood = True
        rotgood = True
        titegood = True

        if perform_sanity_checks:

            # Geometric input sanity checks
            if not np.all(outdict["Ro"] > 0.0) or not np.all(outdict["R0"] > 0.0):
                rgood = False
            if not np.all(outdict["Rmin"] >= 0.0) or not np.all(outdict["x"] >= 0.0):
                rgood = False

            # Plasma kinetic input sanity checks
            for ii in range(0, numions):
                itag = "%d" % ii
                if np.any(outdict["ni"+itag] < 0.0):
                    ingood = False
                if np.any(outdict["Ti"+itag] <= 0.0):
                    itgood = False
            nfilt = (outdict["ne"] == 0.0)
            if np.any(nfilt):
                outdict["ne"][nfilt] = 1.0e-3
            if np.all(nfilt) or np.any(outdict["ne"] <= 0.0):
                engood = False
            if np.any(outdict["Te"] <= 0.0):
                etgood = False

            # Plasma kinetic gradient input sanity checks
            gvals = ["Ane", "Ate", "alpha", "smag", "Autor", "Aupar", "gammaE"]
            for gval in gvals:
                if gval in outdict and not np.all(np.isfinite(outdict[gval])):
                    ggood = False
            for ii in range(0, numions):
                itag = "%d" % ii
                if not np.all(np.isfinite(outdict["Ani"+itag])):
                    ggood = False
                if not np.all(np.isfinite(outdict["Ati"+itag])):
                    ggood = False

            # Magnetic input sanity checks
            if np.all(outdict["q"] == 1.5):
                qgood = False
            if np.any(outdict["q"] <= 0.0):
                qgood = False

            # Rotation input sanity checks
            if np.sum(np.abs(outdict["Machtor"])) == 0.0 and np.sum(np.abs(outdict["gammaE"])) == 0.0 and rot_flag != 0:
                rotgood = False

            # Temperature ratio sanity checks - necessary due to specifics of QuaLiKiz internal fast ion identifier
            tfilt = (outdict["Te"] > 0.0)
            for ii in range(0, numions):
                itag = "%d" % ii
                tfilt = np.all([outdict["Ti"+itag] > 5.0 * outdict["Te"], tfilt], axis=0)
            titegood = False if np.any(tfilt) else True

        if not rgood:
            outdict = None
            xobj = None
            raise ValueError("Negative or zero major radii or negative minor radii detected!")
        if not (ingood and engood):
            outdict = None
            xobj = None
            raise ValueError("Negative densities detected!")
        if not (itgood and etgood):
            outdict = None
            xobj = None
            raise ValueError("Negative temperatures detected!")
        if not ggood:
            outdict = None
            xobj = None
            raise ValueError("NaN values detected in gradient quantities!")
        if not qgood:
            outdict = None
            xobj = None
            raise ValueError("Negative or no safety factor profile detected!")
        if not rotgood:
            warnings.warn("Rotation option specified but rotation quantities are zero, recommended to use rot_flag=0!", UserWarning)
        if not titegood:
            for ii in range(0, numions):
                itag = "%d" % ii
                outdict["Ti"+itag][tfilt] = 4.0 * outdict["Te"][tfilt]
                outdict["mod_Ti"+itag] = copy.deepcopy(outdict["Ti"+itag])
            warnings.warn("Temperature ratio Ti/Te > 5 for all ions, adjusting ion temperatures such that Ti/Te = 4!", UserWarning)

        if rgood and ingood and engood and itgood and etgood and ggood and qgood:

            mtor_def = 0.0
            ator_def = None if assume_tor_rot else 0.0
            mpar_def = None if assume_tor_rot else 0.0
            apar_def = None if assume_tor_rot else 0.0
            game_def = 0.0

            eobj = Electron(
                T=outdict["Te"][0],
                n=outdict["ne"][0],
                At=outdict["Ate"][0],
                An=outdict["Ane"][0],
                type=1,
                anis=1.0,
                danisdr=0.0
            )
            ilobj = IonList()
            for jj in np.arange(0, numions):
                jtag = "%d" % (jj)
                iobj = Ion(
                    T=outdict["Ti"+jtag][0],
                    n=outdict["ni"+jtag][0],
                    At=outdict["Ati"+jtag][0],
                    An=outdict["Ani"+jtag][0],
                    type=1,
                    anis=1.0,
                    danisdr=0.0,
                    A=outdict["Ai"+jtag][0],
                    Z=outdict["Zi"+jtag][0]
                )
                ilobj.append(copy.deepcopy(iobj))
            mtor = outdict["Machtor"][0] if "Machtor" in outdict else mtor_def
            ator = outdict["Autor"][0] if "Autor" in outdict else ator_def
            mpar = outdict["Machpar"][0] if "Machpar" in outdict else mpar_def
            apar = outdict["Aupar"][0] if "Aupar" in outdict else apar_def
            game = outdict["gammaE"][0] if "gammaE" in outdict else game_def
            xobj = QuaLiKizXpoint(
                kthetarhos=ktrs,
                electrons=eobj,
                ions=ilobj,
                x_eq_rho=False,
                rho=outdict["rho"][0],
                rot_flag=rot_flag,
                assume_tor_rot=assume_tor_rot,
                coll_flag=coll_flag,
                collmult=collmult,
                set_qn_normni=True,
                set_qn_normni_ion=0,
                set_qn_An=True,
                set_qn_An_ion=0,
                phys_meth=2,
                numsols=2,
                separateflux=True,
                x=outdict["x"][0],
                Ro=outdict["Ro"][0],
                Rmin=outdict["Rmin"][0],
                R0=outdict["R0"][0],
                q=outdict["q"][0],
                smag=outdict["smag"][0],
                alpha=outdict["alpha"][0],
                Bo=outdict["Bo"][0],
                Machtor=mtor,
                gammaE=game,
                Autor=ator,
                Machpar=mpar,
                Aupar=apar
            )

    return xobj


def get_QLK_input_data(twdata, rho, kthetarhos=None, make_xpoint=False, use_imp_ions=False, set_rzero=None, set_rot_flag=1, pure_tor_rot=False, set_coll_flag=1, coll_mult=None, set_em_stab_flag=0, add_meta=False, debug_flag=False):
    """
    Generates a QuaLiKizXpoint object from a given EX2GK database entry and
    its associated radial scan dictionary, if multiple radial points are
    specified. Linearly interpolates any radial profiles and assumes that
    requested radial points are provided in normalized rho toroidal.

    :arg twdata: dict. EX2GK database entry for a given time window.

    :arg rho: array. Radial points on which to generate QuaLiKiz input parameters.

    :kwarg kthetarhos: array. Vector of kthetarhos for which QuaLiKiz will calculate linear growth rates, uses recommended vector if not specified.

    :kwarg make_xpoint: bool. Specify generation of QuaLiKizXpoint object, not generated by default to allow stitching of multiple EX2GK entries.

    :kwarg use_imp_ions: bool. Specify use of impurity ions exactly as specified in EX2GK database entry.

    :kwarg set_rzero: float. Specify reference length for QuaLiKiz, uses computed value in EX2GK database entry if not provided.

    :kwarg set_rot_flag: int. Specify QuaLiKiz rotation flag - 0 = no rotation, 1 = rotation, 2 = rotation only applied for rho >= 0.5.

    :kwarg pure_tor_rot: bool. Specify dropping of rotation term due to ion pressure gradient and poloidal plasma rotation.

    :kwarg set_coll_flag: int. Specify QuaLiKiz collision flag - 0 = without collisions, 1 = with collisions.

    :kwarg coll_mult: float. Specify multiplier for collisionality term within QuaLiKiz, defaults to 1.0.

    :kwarg set_em_stab_flag: int. Specify ad-hoc EM-stabilization option used in integrated modelling - 0 = none, 1 = scale Ati, 2 = scale 1/Ti, scale Ati and Ate.

    :kwarg add_meta: bool. Automatically write metadata tag based on metadata in database entry. (Not yet implemented)

    :kwarg debug_flag: bool. Enables console printing of common debug statements.

    :returns: (dict, obj). Radial snake-like scan dictionary, QuaLiKizXpoint object of first point in scan dictionary.
    """
    data = None
    here = None
    rzero = None
    rot_flag = 1
    tor_flag = pure_tor_rot
    if isinstance(twdata,dict):
        data = twdata
    if isinstance(rho, number_types):
        here = np.array([rho])
    elif isinstance(rho, array_types):
        here = np.array(rho).flatten()
    if here is not None:
        vfilt = np.all([here >= 0.0, here <= 1.0], axis=0)
        here = here[vfilt] if np.any(vfilt) else None
    if isinstance(set_rzero, number_types):
        rzero = float(set_rzero)
    if isinstance(set_rot_flag, int):
        rot_flag = int(set_rot_flag)
    elif isinstance(set_rot_flag, bool):
        rot_flag = 1 if set_rot_flag else 0

    xobj = None
    xsig = None
    outdict = None
    ionorder = []
    if data is not None and here is not None:

        XD = extract_data_point(data, here, "RHOTORN")

        if XD is not None:

            outdict = OrderedDict()

            # Geometric quantities
            outdict["rho"] = here.copy()
            outdict["x"] = XD["RMINORN_POINT"].copy()
            outdict["Ro"] = XD["RMAJOR_POINT"].copy()
            outdict["Rmin"] = np.full(here.shape, XD["RMINOR_LCFS"])
            outdict["R0"] = np.full(here.shape, XD["RMAJOR_LCFS"])

            # Magnetic quantities
            outdict["Bo"] = np.full(here.shape, XD["BAXS"])
            outdict["q"] = XD["Q"].copy()
            outdict["smag"] = XD["SMAG"].copy()
            outdict["error_q"] = XD["QEB"].copy()
            outdict["error_smag"] = XD["SMAGEB"].copy()

            # Kinetic quantities
            outdict["alpha"] = XD["ALPHA"].copy()
            outdict["error_alpha"] = XD["ALPHAEB"].copy()

            # Electron
            outdict["Te"] = XD["ELEC"]["T"].copy()
            outdict["ne"] = XD["ELEC"]["N"].copy()
            outdict["Ate"] = XD["ELEC"]["AT"].copy()
            outdict["Ane"] = XD["ELEC"]["AN"].copy()
            outdict["error_Te"] = XD["ELEC"]["TEB"].copy()
            outdict["error_ne"] = XD["ELEC"]["NEB"].copy()
            outdict["error_Ate"] = XD["ELEC"]["ATEB"].copy()
            outdict["error_Ane"] = XD["ELEC"]["ANEB"].copy()

            # Discern from impurity ion species
            impdata_present = 0
            numlabel = 1
            ltag = "%d" % (numlabel)
            while "IION"+ltag in XD and XD["IION"+ltag] is not None:
                itag = "IION"+ltag
                zvec = np.full(here.shape, XD[itag]["Z"]) if "ZZ" not in XD[itag] else XD[itag]["ZZ"].copy()
                zcomp = XD["ZEFF"] - (1.0 + XD[itag]["N"] * zvec * zvec) / (1.0 + XD[itag]["N"] * zvec)
                if np.nanmean(zvec) <= 10.1 and np.nanmean(XD[itag]["N"] * zvec) > 4.0e-3 and not np.any(zcomp < 0.0):
                    impdata_present = numlabel
                numlabel = numlabel + 1
                ltag = "%d" % (numlabel)

            numions = 0
            if use_imp_ions:

                numlabel = 1
                ntag = "%d" % (numions)
                ltag = "%d" % (numlabel)
                while "MION"+ltag in XD and XD["MION"+ltag] is not None:
                    itag = "MION"+ltag
                    outdict["Ai"+ntag] = np.full(here.shape, XD[itag]["A"])
                    outdict["Zi"+ntag] = np.full(here.shape, XD[itag]["Z"]) if "ZZ" not in XD[itag] else XD[itag]["ZZ"].copy()
                    outdict["Ti"+ntag] = XD[itag]["T"].copy()
                    outdict["ni"+ntag] = XD[itag]["N"].copy()
                    outdict["Ati"+ntag] = XD[itag]["AT"].copy()
                    outdict["Ani"+ntag] = XD[itag]["AN"].copy()
                    outdict["error_Ti"+ntag] = XD[itag]["TEB"].copy()
                    outdict["error_ni"+ntag] = XD[itag]["NEB"].copy()
                    outdict["error_Ati"+ntag] = XD[itag]["ATEB"].copy()
                    outdict["error_Ani"+ntag] = XD[itag]["ANEB"].copy()
                    numions = numions + 1
                    numlabel = numlabel + 1
                    ntag = "%d" % (numions)
                    ltag = "%d" % (numlabel)
                    ionorder.append(itag)

                numlabel = 1
                ntag = "%d" % (numions)
                ltag = "%d" % (numlabel)
                while "IION"+ltag in XD and XD["IION"+ltag] is not None:
                    itag = "IION"+ltag
                    outdict["Ai"+ntag] = np.full(here.shape, XD[itag]["A"])
                    outdict["Zi"+ntag] = np.full(here.shape, XD[itag]["Z"]) if "ZZ" not in XD[itag] else XD[itag]["ZZ"].copy()
                    outdict["Ti"+ntag] = XD[itag]["T"].copy()
                    outdict["ni"+ntag] = XD[itag]["N"].copy()
                    outdict["Ati"+ntag] = XD[itag]["AT"].copy()
                    outdict["Ani"+ntag] = XD[itag]["AN"].copy()
                    outdict["error_Ti"+ntag] = XD[itag]["TEB"].copy()
                    outdict["error_ni"+ntag] = XD[itag]["NEB"].copy()
                    outdict["error_Ati"+ntag] = XD[itag]["ATEB"].copy()
                    outdict["error_Ani"+ntag] = XD[itag]["ANEB"].copy()
                    numions = numions + 1
                    numlabel = numlabel + 1
                    ntag = "%d" % (numions)
                    ltag = "%d" % (numlabel)
                    ionorder.append(itag)

            elif impdata_present > 0:
                numions = 3
                itag = "%d" % (impdata_present)
                outdict["Ai0"] = np.full(here.shape, XD["MION1"]["A"])
                outdict["Zi0"] = np.full(here.shape, XD["MION1"]["Z"]) if "ZZ" not in XD["MION1"] else XD["MION1"]["ZZ"].copy()
                outdict["Ai1"] = np.full(here.shape, XD["IION"+itag]["A"])
                outdict["Zi1"] = np.full(here.shape, XD["IION"+itag]["Z"])
                outdict["Ai2"] = np.full(here.shape, XD["AZ2"])
                outdict["Zi2"] = np.full(here.shape, XD["ZZ2"])
                reft = XD["MION1"]["T"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["T"].copy()
                refat = XD["MION1"]["AT"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["AT"].copy()

                # Redefine to flat effective charge
                zeff = np.full(here.shape, XD["FZEFF"]) if "FZEFF" in XD and XD["FZEFF"] is not None else np.full(here.shape, np.nanmean(XD["ZEFF"]))
                zeffeb = np.full(here.shape, XD["FZEFFEB"]) if "FZEFFEB" in XD and XD["FZEFFEB"] is not None else np.full(here.shape, np.nanmean(XD["FZEFFEB"]))
                dzeff = np.zeros(here.shape)
                dzeffeb = np.zeros(here.shape)

                # Light impurity ion - Z <= 10
                ztag = "IION"+itag
                outdict["Ti1"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni1"] = XD[ztag]["N"].copy()
                outdict["Ati1"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani1"] = XD[ztag]["AN"].copy()
                outdict["error_Ti1"] = XD[ztag]["TEB"].copy()
                outdict["error_ni1"] = XD[ztag]["NEB"].copy()
                outdict["error_Ati1"] = XD[ztag]["ATEB"].copy()
                outdict["error_Ani1"] = XD[ztag]["ANEB"].copy()

                # Main plasma ion - hydrogenic
                ztag = "MION1"
                outdict["Ti0"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni0"] = ((outdict["Zi2"] - zeff) - outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["Zi2"] - 1.0)
                outdict["Ati0"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani0"] = (outdict["Ane"] * (outdict["Zi2"] - zeff) - outdict["R0"] * dzeff - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["ni0"] * (outdict["Zi2"] - 1.0))
                outdict["error_Ti0"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni0"] = np.sqrt(np.power((outdict["Zi2"] - zeff) * outdict["error_ne"] / outdict["ne"], 2.0) +
#                                               np.power(zeffeb, 2.0) +
#                                               np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_ni0"] = np.sqrt(np.power(zeffeb, 2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_Ati0"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ane"] * (outdict["Zi2"] - XD["ZEFF"]), 2.0) +
#                                                np.power(outdict["Ane"] * zeffeb, 2.0) +
#                                                np.power(outdict["R0"] * dzeffeb, 2.0) +
#                                                np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
#                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
#                                                np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"], 2.0)) / outdict["ni0"]
                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
                                                np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"], 2.0)) / outdict["ni0"]

                # Heavy impurity ion - Z >= 26
                ztag = "TION2"
                outdict["Ti2"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni2"] = (1.0 - outdict["ni0"] * outdict["Zi0"] - outdict["ni1"] * outdict["Zi1"]) / outdict["Zi2"]
                outdict["Ati2"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["AT"]) > 0.0 else refat.copy()
                outdict["Ani2"] = (outdict["Ane"] - outdict["Ani0"] * outdict["ni0"] * outdict["Zi0"] - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"]) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ti2"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ne"] / outdict["ne"], 2.0) + np.power(outdict["error_ni0"] * outdict["Zi0"], 2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"],2.0)) / outdict["Zi2"]
                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ni0"] * outdict["Zi0"], 2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"], 2.0)) / outdict["Zi2"]
                outdict["error_Ati2"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ane"], 2.0) +
#                                                np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"], 2.0) +
#                                                np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"], 2.0) +
#                                                np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"], 2.0) +
#                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"], 2.0) +
#                                                np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"], 2.0)) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"], 2.0) +
                                                np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"], 2.0) +
                                                np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"], 2.0) +
                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"], 2.0) +
                                                np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"], 2.0)) / (outdict["ni2"] * outdict["Zi2"])

                kevc = 1.60217662e3           # (e * 10^19 * 10^3)  -- places p in N m^-2
                betac = 2.51327412e-6         # (2 mu_0)            -- since p is already given in N m^-2
                dpres = kevc * outdict["ne"] * (outdict["Te"] * (outdict["Ane"] + outdict["Ate"]) +
                                                outdict["ni0"] * outdict["Ti0"] * (outdict["Ani0"] + outdict["Ati0"]) +
                                                outdict["ni1"] * outdict["Ti1"] * (outdict["Ani1"] + outdict["Ati1"]) +
                                                outdict["ni2"] * outdict["Ti2"] * (outdict["Ani2"] + outdict["Ati2"]))
                dpreseb = kevc * outdict["ne"] * np.sqrt(np.power(outdict["Te"], 2.0) * (np.power(outdict["error_Ane"], 2.0) + np.power(outdict["error_Ate"], 2.0)) +
                                                         np.power(outdict["Ti0"], 2.0) * (np.power(outdict["error_Ani0"], 2.0) + np.power(outdict["error_Ati0"], 2.0)) +
                                                         np.power(outdict["Ti1"], 2.0) * (np.power(outdict["error_Ani1"], 2.0) + np.power(outdict["error_Ati1"], 2.0)) +
                                                         np.power(outdict["Ti2"], 2.0) * (np.power(outdict["error_Ani2"], 2.0) + np.power(outdict["error_Ati2"], 2.0)))
                outdict["alpha"] = np.power(outdict["q"] / outdict["Bo"], 2.0) * dpres * betac
                outdict["error_alpha"] = np.sqrt(np.power(2.0 * outdict["error_q"] * outdict["q"] * dpres * betac, 2.0) + np.power(np.power(outdict["q"], 2.0) * dpreseb * betac, 2.0)) / np.power(outdict["Bo"], 2.0)

#                ionorder = ["MION1", "TION1", "TION2"]

            else:
                zlimv = np.nanmin(np.hstack((XD["ZLIM1"], XD["ZEFF"] - 1.0e-3)))
                zlim = np.full(here.shape, float(zlimv))

                numions = 3
                outdict["Ai0"] = np.full(here.shape, XD["MION1"]["A"])
                outdict["Zi0"] = np.full(here.shape, XD["MION1"]["Z"]) if "ZZ" not in XD["MION1"] else XD["MION1"]["ZZ"].copy()
                outdict["Ai1"] = np.full(here.shape, XD["AZ1"])
                outdict["Zi1"] = np.full(here.shape, XD["ZZ1"])
                outdict["Ai2"] = np.full(here.shape, XD["AZ2"])
                outdict["Zi2"] = np.full(here.shape, XD["ZZ2"])
                reft = XD["MION1"]["T"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["T"].copy()
                refat = XD["MION1"]["AT"].copy() if np.sum(XD["MION1"]["T"]) > 0.0 else XD["ELEC"]["AT"].copy()

                # Light impurity ion - Z <= 10
                ztag = "TION1"
                outdict["Ti1"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni1"] = (outdict["Zi2"] - XD["ZEFF"]) / (outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"] + (outdict["Zi1"] - zlim) * (outdict["Zi2"] - 1.0) / (zlim - 1.0)))
                outdict["Ati1"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani1"] = outdict["Ane"] + outdict["R0"] * XD["DZEFF"] / (outdict["Zi2"] - XD["ZEFF"])
                outdict["error_Ti1"] = XD[ztag]["TEB"].copy()
                outdict["error_ni1"] = np.sqrt(np.power(outdict["error_ne"] / outdict["ne"], 2.0) + np.power(XD["ZEFFEB"] / (outdict["Zi2"] - XD["ZEFF"]), 2.0)) * outdict["ni1"]
                outdict["error_Ati1"] = XD[ztag]["ATEB"].copy()
                outdict["error_Ani1"] = np.sqrt(np.power(outdict["error_Ane"], 2.0) +
                                                np.power(outdict["R0"] * XD["DZEFFEB"] / outdict["ni1"], 2.0) +
                                                np.power(outdict["R0"] * XD["DZEFF"] * XD["ZEFFEB"] / (outdict["ni1"] * XD["ZEFF"]), 2.0))

                # Main plasma ion - hydrogenic
                ztag = "MION1"
                outdict["Ti0"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni0"] = ((outdict["Zi2"] - XD["ZEFF"]) - outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["Zi2"] - 1.0)
                outdict["Ati0"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else refat.copy()
                outdict["Ani0"] = (outdict["Ane"] * (outdict["Zi2"] - XD["ZEFF"]) - outdict["R0"] * XD["DZEFF"] - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"])) / (outdict["ni0"] * (outdict["Zi2"] - 1.0))
                outdict["error_Ti0"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni0"] = np.sqrt(np.power((outdict["Zi2"] - XD["ZEFF"]) * outdict["error_ne"] / outdict["ne"], 2.0) +
#                                               np.power(XD["ZEFFEB"], 2.0) +
#                                               np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_ni0"] = np.sqrt(np.power(XD["ZEFFEB"], 2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0)) / (outdict["Zi2"] - 1.0)
                outdict["error_Ati0"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ane"] * (outdict["Zi2"] - XD["ZEFF"]), 2.0) +
#                                                np.power(outdict["Ane"] * XD["ZEFFEB"], 2.0) +
#                                                np.power(outdict["R0"] * XD["DZEFFEB"], 2.0) +
#                                                np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
#                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
#                                                np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"], 2.0)) / outdict["ni0"]
                outdict["error_Ani0"] = np.sqrt(np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"] * (outdict["Zi2"] - outdict["Zi1"]), 2.0) +
                                                np.power(outdict["Ani0"] * outdict["error_ni0"] / outdict["ni0"], 2.0)) / outdict["ni0"]

                # Heavy impurity ion - Z >= 26
                ztag = "TION2"
                outdict["Ti2"] = XD[ztag]["T"].copy() if np.sum(XD[ztag]["T"]) > 0.0 else reft.copy()
                outdict["ni2"] = (1.0 - outdict["ni0"] * outdict["Zi0"] - outdict["ni1"] * outdict["Zi1"]) / outdict["Zi2"]
                outdict["Ati2"] = XD[ztag]["AT"].copy() if np.sum(XD[ztag]["AT"]) > 0.0 else refat.copy()
                outdict["Ani2"] = (outdict["Ane"] - outdict["Ani0"] * outdict["ni0"] * outdict["Zi0"] - outdict["Ani1"] * outdict["ni1"] * outdict["Zi1"]) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ti2"] = XD[ztag]["TEB"].copy()
#                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ne"] / outdict["ne"], 2.0) + 
#                                               np.power(outdict["error_ni0"] * outdict["Zi0"], 2.0) + 
#                                               np.power(outdict["error_ni1"] * outdict["Zi1"], 2.0)) / outdict["Zi2"]
                outdict["error_ni2"] = np.sqrt(np.power(outdict["error_ni0"] * outdict["Zi0"], 2.0) + np.power(outdict["error_ni1"] * outdict["Zi1"], 2.0)) / outdict["Zi2"]
                outdict["error_Ati2"] = XD[ztag]["ATEB"].copy()
#                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ane"], 2.0) +
#                                                np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"], 2.0) +
#                                                np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"], 2.0) +
#                                                np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"], 2.0) +
#                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"], 2.0) +
#                                                np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"], 2.0)) / (outdict["ni2"] * outdict["Zi2"])
                outdict["error_Ani2"] = np.sqrt(np.power(outdict["error_Ani0"] * outdict["ni0"] * outdict["Zi0"], 2.0) +
                                                np.power(outdict["Ani0"] * outdict["error_ni0"] * outdict["Zi0"], 2.0) +
                                                np.power(outdict["error_Ani1"] * outdict["ni1"] * outdict["Zi1"], 2.0) +
                                                np.power(outdict["Ani1"] * outdict["error_ni1"] * outdict["Zi1"], 2.0) +
                                                np.power(outdict["error_ni2"] * outdict["Zi2"] * outdict["Ani2"], 2.0)) / (outdict["ni2"] * outdict["Zi2"])

                kevc = 1.60217662e3           # (e * 10^19 * 10^3)  -- places p in N m^-2
                betac = 2.51327412e-6         # (2 mu_0)            -- since p is already given in N m^-2
                dpres = kevc * outdict["ne"] * (outdict["Te"] * (outdict["Ane"] + outdict["Ate"]) + \
                                                outdict["ni0"] * outdict["Ti0"] * (outdict["Ani0"] + outdict["Ati0"]) + \
                                                outdict["ni1"] * outdict["Ti1"] * (outdict["Ani1"] + outdict["Ati1"]) + \
                                                outdict["ni2"] * outdict["Ti2"] * (outdict["Ani2"] + outdict["Ati2"]))
                dpreseb = kevc * outdict["ne"] * np.sqrt(np.power(outdict["Te"], 2.0) * (np.power(outdict["error_Ane"], 2.0) + np.power(outdict["error_Ate"], 2.0)) +
                                                         np.power(outdict["Ti0"], 2.0) * (np.power(outdict["error_Ani0"], 2.0) + np.power(outdict["error_Ati0"], 2.0)) +
                                                         np.power(outdict["Ti1"], 2.0) * (np.power(outdict["error_Ani1"], 2.0) + np.power(outdict["error_Ati1"], 2.0)) +
                                                         np.power(outdict["Ti2"], 2.0) * (np.power(outdict["error_Ani2"], 2.0) + np.power(outdict["error_Ati2"], 2.0)))
                outdict["alpha"] = np.power(outdict["q"] / outdict["Bo"], 2.0) * dpres * betac
                outdict["error_alpha"] = np.sqrt(np.power(2.0 * outdict["error_q"] * outdict["q"] * dpres * betac, 2.0) + np.power(np.power(outdict["q"], 2.0) * dpreseb * betac, 2.0)) / np.power(outdict["Bo"], 2.0)

#                ionorder = ["MION1", "TION1", "TION2"]

            if debug_flag:
                zeff = np.zeros(here.shape)
                for ii in range(numions):
                    stag = "%d" % (ii)
                    zeff = zeff + outdict["ni"+stag] * np.power(outdict["Zi"+stag], 2.0)
                print(XD["ZEFF"])
                print(zeff)

            # Rotation quantities
            outdict["Machtor"] = XD["MTOR"].copy()
            outdict["error_Machtor"] = XD["MTOREB"].copy()
            if "MPAR" in XD and XD["MPAR"] is not None and "AUPAR" in XD and XD["AUPAR"] is not None:
                tor_flag = False
                outdict["Autor"] = XD["AUTOR"].copy()
                outdict["Machpar"] = XD["MPAR"].copy()
                outdict["Aupar"] = XD["AUPAR"].copy()
                outdict["error_Autor"] = XD["AUTOREB"].copy()
                outdict["error_Machpar"] = XD["MPAREB"].copy()
                outdict["error_Aupar"] = XD["AUPAREB"].copy()
            outdict["gammaE"] = XD["GAMMAEROT"].copy() if tor_flag else XD["GAMMAE"].copy()
            outdict["error_gammaE"] = XD["GAMMAEROTEB"].copy() if tor_flag else XD["GAMMAEEB"].copy()

            if set_em_stab_flag == 1:
                em_stab_fac = XD["PTHERM"] / (XD["PTHERM"] + XD["PFAST"])
                for ii in range(numions):
                    stag = "%d" % (ii)
                    outdict["Ati"+stag] = outdict["Ati"+stag] * em_stab_fac
                    outdict["error_Ati"+stag] = outdict["error_Ati"+stag] * em_stab_fac
            if set_em_stab_flag == 2:
                em_stab_fac = XD["PTHERM"] / (XD["PTHERM"] + XD["PFAST"])
                for ii in range(numions):
                    stag = "%d" % (ii)
                    outdict["Ti"+stag] = outdict["Ti"+stag] / em_stab_fac
                    outdict["error_Ti"+stag] = outdict["error_Ti"+stag] / em_stab_fac
            if set_em_stab_flag == 3:
                em_stab_fac = XD["PTHERM"] / (XD["PTHERM"] + XD["PFAST"])
                outdict["Ate"] = outdict["Ate"] * em_stab_fac
                outdict["error_Ate"] = outdict["error_Ate"] * em_stab_fac
                for ii in range(numions):
                    stag = "%d" % (ii)
                    outdict["Ati"+stag] = outdict["Ati"+stag] * em_stab_fac
                    outdict["error_Ati"+stag] = outdict["error_Ati"+stag] * em_stab_fac

            if rzero is not None:
                outdict["R0"] = np.full(outdict["R0"].shape, rzero)

            # This state is occasionally found due to precision of input data and geometric reconstruction, lower bound prevents blanket acceptance
            if outdict["x"][0] < 0.0 and outdict["x"][0] > -1.0e-3:
                outdict["x"][0] = -outdict["x"][0]

            if add_meta:
                outdict["twlabel"] = np.full(here.shape, XD["META"]) if "META" in XD else np.full(here.shape, None)
#                for ii in range(len(outdict["twlabel"])):
#                    if outdict["twlabel"][ii] is not None:
#                        outdict["twlabel"][ii] = outdict["twlabel"][ii] + "_r%03d" % (np.rint(outdict["rho"][ii] * 100))

            datanames = ["q", "smag", "alpha", "Te", "ne", "Ate", "Ane"]
            for zz in range(numions):
                ltag = "%d" % (zz)
                datanames.extend(["Ti"+ltag, "ni"+ltag, "Ati"+ltag, "Ani"+ltag])
            rot_datanames = ["Machtor", "gammaE", "Autor", "Machpar", "Aupar"]
            baderrors = []
            for item in datanames:
                if "error_"+item in outdict and (np.all(outdict["error_"+item] == 0.0) or not np.any(np.isfinite(outdict["error_"+item]))):
                    outdict["error_"+item] = np.full(here.shape, None)
                elif "error_"+item in outdict and np.any(outdict["error_"+item] > 1.0e10):
                    baderrors.append(item)
            if rot_flag != 0:
                for item in rot_datanames:
                    if "error_"+item in outdict and (np.all(outdict["error_"+item] == 0.0) or not np.any(np.isfinite(outdict["error_"+item]))):
                        outdict["error_"+item] = np.full(here.shape, None)
                    elif "error_"+item in outdict and np.any(outdict["error_"+item] > 1.0e10):
                        baderrors.append(item)
            if len(baderrors) > 0:
                estr = " ".join(baderrors)
                warnings.warn("Abnormally large errors: "+estr,UserWarning)

            if make_xpoint:
                xobj = generate_QLK_input(
                    outdict,
                    kthetarhos,
                    set_num_ions=numions,
                    set_rot_flag=rot_flag,
                    set_tor_rot=tor_flag,
                    set_coll_flag=set_coll_flag,
                    set_coll_mult=coll_mult,
                    perform_sanity_checks=True
                )

    return (outdict, xobj)


def get_QLK_snake_data(twlist, rho, kthetarhos=None, make_xpoint=False, use_imp_ions=False, set_rot_flag=1, pure_tor_flag=False, enforce_error=None):
    """
    Extracts QuaLiKiz input parameters from desired radial points over a list
    of EX2GK database entries. Generates a scan dictionary populated with all
    the extracted points and a template QuaLiKizXpoint object.

    :arg twlist: list. List of EX2GK database entries from which QuaLiKiz parameters will be extracted.

    :arg rho: array. Radial points on which to generate QuaLiKiz input parameters.

    :kwarg kthetarhos: array. Vector of kthetarhos for which QuaLiKiz will calculate linear growth rates, uses recommended vector if not specified.

    :kwarg make_xpoint: bool. Specify generation of QuaLiKizXpoint object, not generated by default to allow stitching of multiple EX2GK entries.

    :kwarg use_imp_ions: bool. Specify use of impurity ions exactly as specified in EX2GK database entry.

    :kwarg set_rot_flag: int. Specify QuaLiKiz rotation flag - 0 = no rotation, 1 = rotation, 2 = rotation only applied for rho >= 0.5.

    :kwarg enforce_error: list. Returns none when time window data does not contain error data for the listed variables.

    :returns: (dict, obj). Radial snake-like scan dictionary, QuaLiKizXpoint object of first point in scan dictionary.
    """
    dlist = None
    here = None
    xfilt = [True]
    if isinstance(twlist, dict):
        dlist = [twlist]
    elif isinstance(twlist, (tuple, list)):
        dlist = twlist
    if isinstance(rho, number_types):
        here = np.array([rho])
    elif isinstance(rho, array_types):
        here = np.array(rho).flatten()
    if here is not None:
        vfilt = np.all([here >= 0.0, here <= 1.0], axis=0)
        here = here[vfilt] if np.any(vfilt) else None
    if isinstance(enforce_error, (list, tuple)) and len(enforce_error) > 0:
        xfilt = [True] * len(enforce_error)

    xobj = None
    xdict = None
    if dlist is not None and here is not None:
        for ii in range(len(dlist)):
            fgenx = False if isinstance(xobj, QuaLiKizXpoint) else True
            (txdict, txobj) = get_QLK_input_data(
                dlist[ii], here, kthetarhos,
                make_xpoint=fgenx,
                use_imp_ions=use_imp_ions,
                set_rot_flag=set_rot_flag,
                pure_tor_rot=pure_tor_flag,
                add_meta=True
            )
            if fgenx and isinstance(txobj, QuaLiKizXpoint):
                xobj = copy.deepcopy(txobj)
            if isinstance(txdict, dict):
                rmnames = ["Zeff"]
                for key in txdict:
                    mm = re.match(r'^error_(.+)$', key)
                    if mm:
                        if isinstance(enforce_error, (list, tuple)) and mm.group(1) in enforce_error:
                            idx = enforce_error.index(mm.group(1))
                            if np.all(txdict[key] == None) and not np.isclose(np.sum(txdict[mm.group(1)]), 0.0):
                                xfilt[idx] = False
                        rmnames.append(key)
                    if re.match(r'^mod_(.+)$', key):
                        rmnames.append(key)
                    if re.match(r'^R0$', key):
                        rmnames.append(key)
                ptools.delete_fields_from_dict(txdict, fields=rmnames)
                if not all(xfilt):
                    estr = ""
                    for ii in range(0,len(xfilt)):
                        if not xfilt[ii]:
                            estr = estr + " " + enforce_error[ii]
                    warnings.warn("Enforced errors not present:"+estr, UserWarning)
                    txdict = None
                    txobj = None
            if isinstance(txdict, dict):
                if xdict is None:
                    xdict = OrderedDict()
                for key in txdict:
                    #tmpobj = txdict[key].tolist() if isinstance(txdict[key], np.ndarray) else txdict[key]
                    if key not in xdict:
                        xdict[key] = txdict[key].tolist()
                    else:
                        xdict[key].extend(txdict[key].tolist())
    return (xdict,xobj)

def get_QLK_error_expansion_data(twdata, rho, kthetarhos=None, expand_dict=None, make_xpoint=False, use_imp_ions=False, set_rot_flag=1, pure_tor_flag=False, enforce_error=None):
    """
    
    """
    data = None
    here = None
    edict = dict()
    if isinstance(twdata, dict):
        data = twdata
    elif isinstance(twdata, (list, tuple)):
        data = twdata[0]
    if isinstance(rho, number_types):
        here = float(rho)
    elif isinstance(rho, array_types):
        here = float(rho[0])
    if here is not None and (here <= 0.0 or here >= 1.0):
        here = None
    if isinstance(expand_dict, dict):
        edict = copy.deepcopy(expand_dict)
    # This is here to accommodate native formatting of metadata inside QuaLiKiz-pythontools
    for key in list(edict.keys()):
        mm = re.match('^@(.+)\.(.+)$', key)
        if mm:
            edict[mm.group(1)] = copy.deepcopy(edict[key])

    xobj = None
    xdict = None
    if data is not None and here is not None:
        (txdict, xobj) = get_QLK_input_data(data, here, kthetarhos, make_xpoint=True,
                                            use_imp_ions=use_imp_ions,
                                            set_rot_flag=set_rot_flag,
                                            pure_tor_rot=pure_tor_flag,
                                            add_meta=True)
        if isinstance(txdict, dict):
            for key in txdict:
                mm = re.match(r'^error_(.+)$', key)
                nn = re.match(r'^mod_(.+)$', key)
                if mm and mm.group(1) in txdict and mm.group(1) in edict and isinstance(edict[mm.group(1)], array_types):
                    if xdict is None:
                        xdict = dict()
                    vkey = mm.group(1)
                    deltas = np.array(edict[vkey]).flatten()
                    xdict[vkey] = []
                    mean = float(txdict[vkey])
                    sigma = float(txdict[key]) if not np.all(txdict[key] == None) else None
                    if sigma is not None:
                        for jj in np.arange(0, len(deltas)):
                            gval = mean + sigma * deltas[jj]    # A proper sigma is always positive, but left this way to allow convenient tricks
                            xdict[vkey].append(gval)
                if nn and nn.group(1) in txdict and nn.group(1) not in edict:
                    vkey = nn.group(1)
                    xdict[vkey] = []
                    xdict[vkey].append(float(txdict[key]))
                if key == 'twlabel':
                    xdict[key] = txdict[key].tolist()
    return (xdict,xobj)

def generate_singular_run(twdata, rho, runname, kthetarhos=None, scantype=None, use_imp_ions=False, set_rot_flag=1, add_meta=False, rundir=None, execpath=None, submitfile=None):
    """
    Set up QuaLiKiz run diectory and associated batch submission scripts for
    a singular EX2GK database entry. Multiple radial points are allowed.

    :arg twdata: dict. EX2GK database entry from which QuaLiKiz parameters will be extracted.

    :arg rho: array. Radial points on which to generate QuaLiKiz input parameters.

    :arg runname: str. Name for actual QuaLiKiz run directory containing input and output files.

    :kwarg kthetarhos: array. Vector of kthetarhos for which QuaLiKiz will calculate linear growth rates, uses recommended vector if not specified.

    :kwarg scantype: str. Specification of the preset expansion scheme to use, choice of [hyperrect, hypercube, parallel].

    :kwarg use_imp_ions: bool. Specify use of impurity ions exactly as specified in EX2GK database entry.

    :kwarg set_rot_flag: int. Specify QuaLiKiz rotation flag - 0 = no rotation, 1 = rotation, 2 = rotation only applied for rho >= 0.5.

    :kwarg rundir: str. Base directory within which QuaLiKiz run directory will be created.

    :kwarg execpath: str. Full path to QuaLiKiz executable, useful for running with custom QuaLiKiz compilations.

    :kwarg submitfile: str. Optional full path to batch submission script, useful for debugging on various computing architectures.

    :returns: None.
    """
    rdir = './'
    rname = None
    xpath = None
    scan = 'parallel'
    if isinstance(rundir, str) and os.path.isdir(rundir):
        rdir = os.path.abspath(rundir)
    if not rdir.endswith('/'):
        rdir = rdir + '/'
    if isinstance(runname, str):
        rname = runname
    if isinstance(execpath, str) and os.path.isfile(execpath) and os.access(execpath, os.X_OK):
        xpath = os.path.abspath(execpath)
    if scantype in ('hypercube', 'hyperrect', 'hyperedge', 'parallel'):
        scan = scantype
    if scan == 'hypercube':
        scan = 'hyperrect'

    xdict = None
    xobj = None
    if scan == 'parallel':
        (xdict, xobj) = get_QLK_input_data(
            twdata, rho, kthetarhos,
            make_xpoint=True,
            use_imp_ions=use_imp_ions,
            set_rot_flag=set_rot_flag,
            add_meta=add_meta
        )
    elif scan in ['hyperrect', 'hyperedge']:
        (xdict, xobj) = get_QLK_error_expansion_data(
            twdata, rho, kthetarhos,
            expand_dict=None,
            make_xpoint=True,
            use_imp_ions=use_imp_ions,
            set_rot_flag=set_rot_flag,
            add_meta=add_meta
        )

    robj = None
    if isinstance(xobj, QuaLiKizXpoint):
        pobj = QuaLiKizPlan(scan_dict=xdict, scan_type=scan, xpoint_base=xobj)
        robj = QuaLiKizRun(rdir, rname, xpath, qualikiz_plan=pobj)
        robj.prepare()
        robj.generate_input()
        if isinstance(submitfile, str) and os.path.isfile(submitfile):
            targetdir = rundir
            if isinstance(rundir, str) and not targetdir.endswith('/'):
                targetdir = targetdir + '/'
            targetdir = targetdir + runname if isinstance(rundir, str) else runname
            if not targetdir.endswith('/'):
                targetdir = targetdir + '/'
            subprocess.run(['cp', submitfile, targetdir])

