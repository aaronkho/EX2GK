# Script with functions to converted standardized fitted profiles into QuaLiKiz input parameters
# Developer: Aaron Ho - 28/02/2018

# Required imports
import os
import sys
import copy
from operator import itemgetter
import numpy as np
import re
import datetime
import pickle
from scipy.interpolate import interp1d

# Internal package imports
from EX2GK.tools.general import classes, proctools as ptools, phystools as ytools


def calc_general_gk_inputs(basedata,coordinate=None,use_bias=False,fdebug=False):
    """
    Calculates the general dimensionless parameters
    needed for a gyrokinetic code. This function assumes
    the given data is in the rho toroidal radial
    coordinate system and will convert everything to the
    midplane-averaged minor radius coordinate system,
    provided that the conversion vectors are available.

    Note that this function calculates the midplane
    average of the LCFS as the R_zero value.

    :arg basedata: dict. Object containing standardized profile fits and dimensionless gyrokinetic parameters.

    :kwarg use_bias: bool. Flag to toggle calculation of inputs using the correction coordinate systems, if present.

    :returns: dict. Object with identical structure as input object, except with gyrokinetic inputs inserted in a standardized format.
    """
    twobj = None
    if isinstance(basedata,classes.EX2GKTimeWindow):
        twobj = copy.deepcopy(basedata)
    fdebug = False

    if isinstance(twobj,classes.EX2GKTimeWindow) and twobj.isFitted():
        # Housekeeping
        gcs = twobj["META"]["CSO"]
        gcp = twobj["META"]["CSOP"] if use_bias else ""     # It is NOT recommended to use ad-hoc corrected equilibria here
        numion = twobj["META"]["NUMI"]
        numimp = twobj["META"]["NUMIMP"]
        numz = twobj["META"]["NUMZ"]
        pddict = twobj["PD"].getPresentFields()

        ctag = "CMN"
        pcs = coordinate.upper() if isinstance(coordinate,str) and gcp+coordinate.upper() in twobj["CD"].coord_keys() else gcs
        xvec_in = twobj["PD"]["X"][""].copy()
        twobj["META"].setPostProcessingCodeMetaData("general",ctag,pcs)

        # Built-in class function from EX2GK/tools/general/classes.py: computes R_major_outer at z_midplane, d gcs / d R_major_outer, error from bias correction
        (rmajo,rmajoj,rmajoe) = twobj["CD"].convert(twobj["PD"]["X"][""],gcs,"RMAJORO",gcp,gcp,fdebug=fdebug)
        (rlcfso,rlcfsoj,rlcfsoe) = twobj["CD"].convert([1.0],"PSIPOLN","RMAJORO",gcp,gcp,fdebug=fdebug)

        # Built-in class function from EX2GK/tools/general/classes.py: computes R_major_inner at z_midplane, d gcs / d R_major_inner, error from bias correction
        (rmaji,rmajij,rmajie) = twobj["CD"].convert(twobj["PD"]["X"][""],gcs,"RMAJORI",gcp,gcp,fdebug=fdebug)
        (rlcfsi,rlcfsij,rlcfsie) = twobj["CD"].convert([1.0],"PSIPOLN","RMAJORI",gcp,gcp,fdebug=fdebug)

        # Filter to prevent divide by zero errors, however unlikely they are
        jfilt = np.all([np.abs(rmajoj) > 1.0e-6,np.abs(rmajij) > 1.0e-6],axis=0)

        # Built-in class function from EX2GK/tools/general/classes.py: computes desired coordinate base, d gcs / d desired coordinate, error from bias correction
        (userv,userj,usere) = twobj["CD"].convert(twobj["PD"]["X"][""],gcs,pcs,gcp,gcp,fdebug=fdebug)

        # Primary coordinate systems
        rr = (rmajo - rmaji) / 2.0
        rreb = np.sqrt(np.power(rmajoe,2.0) + np.power(rmajie,2.0)) / 2.0
        jrr = np.ones(xvec_in.shape)
        if np.any(jfilt):
            jrr[jfilt] = 2.0 * rmajoj[jfilt] * rmajij[jfilt] / (rmajij[jfilt] - rmajoj[jfilt])
        ifilt = np.isfinite(jrr)
        if np.any(np.invert(ifilt)):
            ifunc = interp1d(twobj["PD"]["X"][""][ifilt],jrr[ifilt],bounds_error=False,fill_value='extrapolate')
            jrr[np.invert(ifilt)] = ifunc(twobj["PD"]["X"][""][np.invert(ifilt)])
        twobj["PD"].addProfileData("OUT"+ctag+"_R",rr,rreb)
        twobj["PD"].addProfileDerivativeData("OUT"+ctag+"_R",jrr,None)
        twobj["PD"].addProfileData("OUT"+ctag+"_X",userv,usere)
        twobj["PD"].addProfileDerivativeData("OUT"+ctag+"_X",userj,None)

        rzero = float((rlcfso + rlcfsi) / 2.0)
        rzeroeb = float(np.sqrt(np.power(rlcfsoe,2.0) + np.power(rlcfsi,2.0)) / 2.0)
        rmin = float((rlcfso - rlcfsi) / 2.0)
        rmineb = float(np.sqrt(np.power(rlcfso,2.0) + np.power(rlcfsi,2.0)) / 2.0)
        twobj["ZD"].addData("OUT"+ctag+"_RZERO",rzero,rzeroeb)
        twobj["ZD"].addData("OUT"+ctag+"_A",rmin,rmineb)

        # Reference values
        bref = twobj["ZD"][gcp+"BMAG"][""] if gcp+"BMAG" in twobj["ZD"] else twobj["ZD"]["BMAG"][""]
        brefeb = twobj["ZD"][gcp+"BMAG"]["EB"] if gcp+"BMAG" in twobj["ZD"] else twobj["ZD"]["BMAG"]["EB"]
        twobj["ZD"].addData("OUT"+ctag+"_BO",bref,brefeb)

        if "N" in pddict:

            # Logarithmic electron density gradient
            qtag = "NE"
            if "E" in pddict["N"] and "GRAD" in twobj["PD"][qtag]:
                scale = 1.0
                val = copy.deepcopy(twobj["PD"][qtag][""])
                valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                norm = -1.0
                (ne,neeb,dlne,dlneeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlne,dlneeb)

            # Logarithmic main ion density gradient
            for idx in np.arange(0,numion):
                itag = "%d" % (idx+1)
                qtag = "NI"+itag
                if "I"+itag in pddict["N"] and "GRAD" in twobj["PD"][qtag]:
                    scale = 1.0
                    val = copy.deepcopy(twobj["PD"][qtag][""])
                    valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                    norm = -1.0
                    (ni,nieb,dlni,dlnieb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                    twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlni,dlnieb)

            # Logarithmic impurity ion density gradient
            for idx in np.arange(0,numimp):
                itag = "%d" % (idx+1)
                qtag = "NIMP"+itag
                if "IMP"+itag in pddict["N"] and "GRAD" in twobj["PD"][qtag]:
                    scale = 1.0
                    val = copy.deepcopy(twobj["PD"][qtag][""])
                    valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                    norm = -1.0
                    (nimp,nimpeb,dlnimp,dlnimpeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                    twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlnimp,dlnimpeb)

        if "T" in pddict:

            # Logarithmic electron temperature gradient
            qtag = "TE"
            if "E" in pddict["T"] and "GRAD" in twobj["PD"][qtag]:
                scale = 1.0
                val = copy.deepcopy(twobj["PD"][qtag][""])
                valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                norm = -1.0
                (te,teeb,dlte,dlteeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlte,dlteeb)

            # Logarithmic ion temperature gradient - set equal to impurity ion temperature data in main program if not directly measured
            for idx in np.arange(0,numion):
                itag = "%d" % (idx+1)
                qtag = "TI"+itag
                if "I"+itag in pddict["T"] and "GRAD" in twobj["PD"][qtag]:
                    scale = 1.0
                    val = copy.deepcopy(twobj["PD"][qtag][""])
                    valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                    norm = -1.0
                    (ti,tieb,dlti,dltieb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                    twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlti,dltieb)

            # Logarithmic impurity ion temperature gradient
            for idx in np.arange(0,numimp):
                itag = "%d" % (idx+1)
                qtag = "TIMP"+itag
                if "IMP"+itag in pddict["T"] and "GRAD" in twobj["PD"][qtag]:
                    scale = 1.0
                    val = copy.deepcopy(twobj["PD"][qtag][""])
                    valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                    norm = -1.0
                    (timp,timpeb,dltimp,dltimpeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                    twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dltimp,dltimpeb)

        ### The EX2GK main program allows up to two assumed impurities, used to match experimental Z_eff profiles if available

        if "N" in pddict and "T" in pddict:

            for idx in np.arange(0,numz):
                itag = "%d" % (idx+1)

                if "Z"+itag in pddict["N"] and "Z"+itag in pddict["T"]:
                    # Logarithmic impurity ion density gradient for assumed impurity species
                    qtag = "NZ"+itag
                    scale = 1.0
                    val = copy.deepcopy(twobj["PD"][qtag][""])
                    valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                    norm = -1.0
                    (nz,nzeb,dlnz,dlnzeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                    twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlnz,dlnzeb)

                    # Logarithmic impurity ion temperature gradient for assumed impurity species
                    qtag = "TZ"+itag
                    scale = 1.0
                    val = copy.deepcopy(twobj["PD"][qtag][""])
                    valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else None
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else None
                    norm = -1.0
                    (tz,tzeb,dltz,dltzeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
                    twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dltz,dltzeb)

        # Magnetic shear
        qtag = "Q" + gcp
        if "Q" in pddict and qtag in pddict["Q"]:
            zfilt = (twobj["PD"][qtag][""] != 0.0)
            val = copy.deepcopy(twobj["PD"][qtag][""])
            valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
            grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"]) if "GRAD" in twobj["PD"][qtag] else np.zeros(xvec_in.shape)
            gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"]) if "GRADEB" in twobj["PD"][qtag] else np.zeros(xvec_in.shape)
            norm = copy.deepcopy(userv)
            (qq,qqeb,smag,smageb) = ytools.calc_generic_normalized_gradient(twobj["PD"][qtag][""],1.0,grad,userj,norm,twobj["PD"][qtag]["EB"],gradeb,enforce_positive=True,default_der_err=0.2)
            if np.any(np.invert(zfilt)):
                smag[np.invert(zfilt)] = np.sign(smag[np.invert(zfilt)]) * np.Inf
                smageb[np.invert(zfilt)] = np.Inf
            twobj["PD"].addProfileData("OUT"+ctag+"_Q",qq,qqeb)
            twobj["PD"].addProfileData("OUT"+ctag+"_SMAG",smag,smageb)

            ptag = qtag
            # Alpha MHD
            if "P" in pddict and "E" in pddict["P"] and "GRAD" in twobj["PD"]["PE"] and np.abs(bref) > 0.0:
                sources = ["NBI","ICRH","ECRH","LH"]
                betac = 2.51327412e-6                    # (2 mu_0)  -- since p is already given in N m^-2
                qtag = "PE"
                scale = 1.0
                val = copy.deepcopy(twobj["PD"][qtag][""])
                valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
                norm = -betac / np.power(bref,2.0)
                (betae,betaeeb,alphae,alphaeeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.2)

                betai = np.zeros(twobj["PD"][qtag][""].shape)
                betaieb = np.zeros(twobj["PD"][qtag][""].shape)
                betaz = np.zeros(twobj["PD"][qtag][""].shape)
                betazeb = np.zeros(twobj["PD"][qtag][""].shape)
                betaf = np.zeros(twobj["PD"][qtag][""].shape)
                betafeb = np.zeros(twobj["PD"][qtag][""].shape)
                alphai = np.zeros(twobj["PD"][qtag][""].shape)
                alphaieb = np.zeros(twobj["PD"][qtag][""].shape)
                alphaz = np.zeros(twobj["PD"][qtag][""].shape)
                alphazeb = np.zeros(twobj["PD"][qtag][""].shape)
                alphaf = np.zeros(twobj["PD"][qtag][""].shape)
                alphafeb = np.zeros(twobj["PD"][qtag][""].shape)
                for idx in np.arange(0,numion):
                    itag = "%d" % (idx+1)
                    qtag = "PI"+itag
                    if "I"+itag in pddict["P"] and "GRAD" in twobj["PD"][qtag]:
                        scale = 1.0
                        val = copy.deepcopy(twobj["PD"][qtag][""])
                        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
                        norm = -betac / np.power(bref,2.0)
                        (beta,betaeb,alpha,alphaeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.2)
                        betai = betai + beta
                        betaieb = np.sqrt(np.power(betaieb,2.0) + np.pwoer(betaeb,2.0))
                        alphai = alphai + alpha
                        alphaieb = np.sqrt(np.power(alphaieb,2.0) + np.power(alphaeb,2.0))
                for idx in np.arange(0,numimp):
                    itag = "%d" % (idx+1)
                    qtag = "PIMP"+itag
                    if "IMP"+itag in pddict["P"] and "GRAD" in twobj["PD"][qtag]:
                        scale = 1.0
                        val = copy.deepcopy(twobj["PD"][qtag][""])
                        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
                        norm = -betac / np.power(bref,2.0)
                        (beta,betaeb,alpha,alphaeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.2)
                        betaz = betaz + beta
                        betazeb = np.sqrt(np.power(betazeb,2.0) + np.pwoer(betaeb,2.0))
                        alphaz = alphaz + alpha
                        alphazeb = np.sqrt(np.power(alphazeb,2.0) + np.power(alphaeb,2.0))
                for idx in np.arange(0,numion):
                    itag = "%d" % (idx+1)
                    qtag = "PZ"+itag
                    if "Z"+itag in pddict["P"] and "GRAD" in twobj["PD"][qtag]:
                        scale = 1.0
                        val = copy.deepcopy(twobj["PD"][qtag][""])
                        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
                        norm = -betac / np.power(bref,2.0)
                        (beta,betaeb,alpha,alphaeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.2)
                        betaz = betaz + beta
                        betazeb = np.sqrt(np.power(betazeb,2.0) + np.pwoer(betaeb,2.0))
                        alphaz = alphaz + alpha
                        alphazeb = np.sqrt(np.power(alphazeb,2.0) + np.power(alphaeb,2.0))
                for src in sources:                      # Factor 2/3 comes from the fact that W is energy density, not pressure
                    qtag = "WFI_"+src
                    if "W" in pddict and "FI_"+src in pddict["W"] and "GRAD" in twobj["PD"][qtag]:
                        scale = 1.0
                        val = copy.deepcopy(twobj["PD"][qtag][""])
                        valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                        grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                        gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
                        norm = -(2.0 / 3.0) * betac / np.power(bref,2.0)
                        (beta,betaeb,alpha,alphaeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.2)
                        betaf = betaf + beta
                        betafeb = np.sqrt(np.power(betafeb,2.0) + np.pwoer(betaeb,2.0))
                        alphaf = alphaf + alpha
                        alphafeb = np.sqrt(np.power(alphafeb,2.0) + np.power(alphaeb,2.0))

                # Combined errors not added in quadrature here, too hard to consider as completely independent from each other
                betaei = np.power(twobj["PD"][ptag][""],2.0) * (betae + betai)
                betaeieb = np.sqrt(2.0 * np.power(twobj["PD"][ptag]["EB"] * (betae + betai),2.0) + np.power(np.power(twobj["PD"][ptag]["EB"],2.0) * (betaeeb + betaieb),2.0))
                betath = np.power(twobj["PD"][ptag][""],2.0) * (betae + betai + betaz)
                betatheb = np.sqrt(2.0 * np.power(twobj["PD"][ptag]["EB"] * (betae + betai + betaz),2.0) + np.power(np.power(twobj["PD"][ptag]["EB"],2.0) * (betaeeb + betaieb + betazeb),2.0))
                betatot = np.power(twobj["PD"][ptag][""],2.0) * (betae + betai + betaz + betaf)
                betatoteb = np.sqrt(2.0 * np.power(twobj["PD"][ptag]["EB"] * (betae + betai + betaz + betaf),2.0) + np.power(np.power(twobj["PD"][ptag]["EB"],2.0) * (betaeeb + betaieb + betazeb + betafeb),2.0))
                twobj["PD"].addProfileData("OUT"+ctag+"_BETAEI",betaei,betaeieb)
                twobj["PD"].addProfileData("OUT"+ctag+"_BETATH",betath,betatheb)
                twobj["PD"].addProfileData("OUT"+ctag+"_BETATOT",betatot,betatoteb)
                alphaei = np.power(twobj["PD"][ptag][""],2.0) * (alphae + alphai)
                alphaeieb = np.sqrt(2.0 * np.power(twobj["PD"][ptag]["EB"] * (alphae + alphai),2.0) + np.power(np.power(twobj["PD"][ptag]["EB"],2.0) * (alphaeeb + alphaieb),2.0))
                alphath = np.power(twobj["PD"][ptag][""],2.0) * (alphae + alphai + alphaz)
                alphatheb = np.sqrt(2.0 * np.power(twobj["PD"][ptag]["EB"] * (alphae + alphai + alphaz),2.0) + np.power(np.power(twobj["PD"][ptag]["EB"],2.0) * (alphaeeb + alphaieb + alphazeb),2.0))
                alphatot = np.power(twobj["PD"][ptag][""],2.0) * (alphae + alphai + alphaz + alphaf)
                alphatoteb = np.sqrt(2.0 * np.power(twobj["PD"][ptag]["EB"] * (alphae + alphai + alphaz + alphaf),2.0) + np.power(np.power(twobj["PD"][ptag]["EB"],2.0) * (alphaeeb + alphaieb + alphazeb + alphafeb),2.0))
                twobj["PD"].addProfileData("OUT"+ctag+"_ALPHAEI",alphaei,alphaeieb)
                twobj["PD"].addProfileData("OUT"+ctag+"_ALPHATH",alphath,alphatheb)
                twobj["PD"].addProfileData("OUT"+ctag+"_ALPHATOT",alphatot,alphatoteb)


        tags = ["TOR", "POL"]
        for dtag in tags:
            # Logarithmic flow angular frequency gradient
            scale = 1.0
            val = np.zeros(xvec_in.shape)
            valeb = np.zeros(xvec_in.shape)
            grad = np.zeros(xvec_in.shape)
            gradeb = np.zeros(xvec_in.shape)
            qtag = "AF"+dtag
            if "AF" in pddict and dtag in pddict["AF"]:
                val = copy.deepcopy(twobj["PD"][qtag][""])
                valeb = copy.deepcopy(twobj["PD"][qtag]["EB"])
                if "GRAD" in twobj["PD"][qtag]:
                    grad = copy.deepcopy(twobj["PD"][qtag]["GRAD"])
                    gradeb = copy.deepcopy(twobj["PD"][qtag]["GRADEB"])
            (af,afeb,dlaf,dlafeb) = ytools.calc_generic_normalized_gradient(val,scale,grad,userj,norm,valeb,gradeb,enforce_positive=True,default_der_err=0.5)
            twobj["PD"].addProfileData("OUT"+ctag+"_DLOG"+qtag,dlaf,dlafeb)


    return twobj


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def clean_code_inputs(basedata):
    """
    This function provides a standard method to delete code-specific
    data generated by the output adapter. This allows for minimization
    of required storage space and reduces chance of clashes when
    re-processing data with the same or another adapter.

    :arg basedata: dict. Standardized object containing processed and fitted data, possibly post-processed by this adapter.

    :returns: dict. Object identical in structure to input object, except with all code-specific fields from this adapter removed.
    """
    outdict = None
    if isinstance(basedata,classes.EX2GKTimeWindow):
        outdict = copy.deepcopy(basedata)

    if isinstance(outdict,classes.EX2GKTimeWindow) and outdict.isProcessed():
        code = "general"
        ctag = "CMN"
        pdlist = outdict["PD"].getPresentFields('list')
        for item in pdlist:
            if item.endswith(ctag):
                del twobj["PD"][item]
        if re.match(r'^'+code+r'$',outdict["META"]["CODE"],flags=re.IGNORECASE):
            outdict["META"].resetPostProcessingCodeMetaData()

    return outdict


# This function should always exist in the adapter file with these arguments
#    All extra arguments needed for a given implementation should be input as a dict via namelist argument
def calc_code_inputs(basedata,sysopt=None,forceflag=False,biasflag=True,fdebug=False):
    """
    This function provides a single entry point into the output adapter
    scripts for various target codes. For ease of use, only this file
    should be imported into any user-defined script and only this
    function should be used to process the desired standardized data.

    :arg basedata: dict. Standardized object containing processed and fitted data from EX2GK main program.

    :kwarg sysopt: str. Name of radial specification to be used within the adapter, not always applicable.

    :kwarg forceflag: bool. Flag to force the calculation of code-specific quantities, even if other code-specific quantities are present.

    :kwarg biasflag: bool. Flag to toggle calculation of code inputs using the correction coordinate systems, if applicable and data is present.

    :returns: dict. Object identical in structure to input object, except containing code-specific quantities if appropriate data is present.
    """
    stddict = None
    if isinstance(basedata,classes.EX2GKTimeWindow):
        stddict = copy.deepcopy(basedata)

    outdata = None
    if stddict is not None:
        goflag = True
        if stddict["META"]["CODE"] is not None:
            if forceflag:
                print("This file has already been processed for use in %s. Deleting older values and re-processing." % (stddict["META"]["CODE"]))
                stddict = clean_code_inputs(stddict)
                goflag = True
            else:
                print("This file has already been processed for use in %s. Aborting..." % (stddict["META"]["CODE"]))
                goflag = False
        outdata = calc_general_gk_inputs(stddict,coordinate=sysopt,use_bias=biasflag,fdebug=fdebug) if goflag else stddict

    return outdata
