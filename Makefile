# Makefile for EX2GK tool and associated library

PROJECT = EX2GK

PYTHON = python3
.PHONY = help dist setup clean clean-build clean-pyc

.DEFAULT_GOAL = setup

help:
	@echo "clean - remove all build, test, and Python artifacts"
	@echo "clean-build - remove all build artifacts"
	@echo "clean-pyc - remove all Python artifacts"
	@echo "dist - build package for all distributions"
	@echo "sdist - build package for source distribution"
	@echo "bdist - build package for binary distribution"
	@echo "wheel - build package for binary wheel distribution"
	@echo "realclean - same as clean"
	@echo "setup - install the package to user site-packages for active Python"

dist: clean sdist bdist wheel

sdist:
	${PYTHON} setup.py sdist
	ls -l dist

bdist:
	${PYTHON} setup.py bdist
	ls -l dist

wheel:
	${PYTHON} setup.py bdist_wheel
	ls -l dist

setup: clean
	${PYTHON} -m pip install --user --editable .

clean: clean-build clean-pyc

clean-build:
	rm -rf build/
	rm -rf dist/
	find . -name '*.egg-info' -exec rm -rf {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +

realclean: clean
